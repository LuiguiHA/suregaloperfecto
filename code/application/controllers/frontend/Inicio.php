<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class inicio extends MY_Controller {
  public function __construct()
  {
      parent::__construct();

      $this->load->model("Categoria_model");
      $this->load->model("Producto_model");
      $this->load->model("Tipo_model");
      $this->load->model("Personalidad_model");
      $this->load->model("Tonos_model");
      $this->load->model("Edad_model");
      $this->load->helper('utils_helper');
      $this->load->helper('url');
      $this->load->library('minify');

  }
  public function index()
  {
   
   $this->minify->css('template2.css, service.ddlist.jquery.css, scrollBar.css');
   $this->minify->js("scrollBar.js, main2.js"); 
    $params = array(
        "select" => "*",
        "where" => "activo = ".ESTADO_ACTIVO,
        "order"=> "id"
    );
    
    //echo '$this->elementoPorPagina: '.$this->elementoPorPagina;
    $categorias = $this->Categoria_model->search_data($params, $start, $this->elementoPorPagina);
    
    $params = array(
        "select" => "*",
        "where" => "activo = ".ESTADO_ACTIVO,
        "order"=> "id"
    );
    
    $edades = $this->Edad_model->search($params);
    $personalidades = $this->Personalidad_model->search($params);
    $tipos = $this->Tipo_model->search($params);
    $tonos = $this->Tonos_model->search($params);
    $productos = $this->Producto_model->search($params);
    $total_productos = $this->Producto_model->total_records($params);
    
    $this->arrayVista['categorias'] = $categorias;
    $this->arrayVista['productos'] = $productos;
    $this->arrayVista['tonos'] = $tonos;
    $this->arrayVista['tipos'] = $tipos;
    $this->arrayVista['personalidades'] = $personalidades;
    $this->arrayVista['edades'] = $edades;
    $this->arrayVista['total_productos'] = $total_productos;
    $this->arrayVista['vista'] = 'frontend/site_view';
    $this->cargarVistaFrontend();
  }
}
