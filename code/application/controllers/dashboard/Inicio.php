<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Inicio extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

    }

    public function index()
    {

        if( $this->session->userdata('logged_in') == true )
        {
            redirect($this->config->item('path_dashboard').'/Principal');  // página de inicio
        }
        else
        {
            $url_local= url_servicios_dashoard."usuario/obtener_locales";
            $locales=$this->json_url(null,$url_local,"GET",null);
            $locales=$locales["locales"];
            $this->arrayVista['locales']=$locales;
            $this->arrayVista['vista'] = 'dashboard/login/login_view';
            $this->cargarVistaLoginDashboard();
        }

    }


    public function login()
    {

        $usuario = $this->input->post("txtuser",TRUE);
        $password = $this->input->post("txtpassword",TRUE);
        $local = $this->input->post("local",TRUE);
        $type_dashboard = DASHBOARD_ADMIN_COD;
        $data = json_encode(array(
            "correo"  => $usuario,
            "contrasenia" => $password,
            "id_local" => $local,
            "type_dashboard"=>$type_dashboard
        ));
        $url_login= url_servicios_dashoard."usuario/iniciar_sesion";

        $login=$this->json_url("",$url_login,"POST",$data);


        switch ($login["resultado"])
        {
            case "1" :
                $var_session = array(
                    'token'  => $login["token"],
                    'nom_local' => $login["detalle_local"]["nombre_local"],
                    'id_local' => $login["id_local"],
                    'nom_empresa' => $login["detalle_local"]["nombre_empresa"],
                    'logo' => $login["detalle_local"]["logo"],
                    'logged_in' => TRUE
                );
                $this->session->set_userdata($var_session);


                $error = array("msg"=>$login["mensaje"],"cod"=>1);

                echo json_encode($error);

                break;
            case "0" :  $error = array("msg"=>$login["mensaje"],"cod"=>0);echo json_encode($error);
                break;
        }


    }

    public function logout(){


        $token = $this->session->userdata('token');

        $url= url_servicios_dashoard."usuario/cerrar_sesion";

        $logout=$this->json_url($token,$url,"DELETE",null);


        $this->session->sess_destroy();
        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('token');
       redirect($this->config->item('path_dashboard'));
    }

}