<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Reserva extends MY_Controller
{
    private $horario_meses;

    public function __construct()
    {
        parent::__construct();

    }

    public function listar($start = 0)
    {
        if ($this->session->userdata('logged_in') == true)
        {
            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");
                $id_local= $this->session->userdata("id_local");

                 $data = json_encode(array(
                                         "id_local" => $id_local
                                     ));

                $url = url_servicios_dashoard . "/Reserva_solicitud/obtener_reservas/1";
                $reservas = $this->json_url($token, $url, "POST", $data);


                $total_reservas = $reservas["total_reservas"];

                $paginado = ($start / ELEMENTOS_POR_PAGINA) + 1;

                $url = url_servicios_dashoard . "/Reserva_solicitud/obtener_reservas/" . $paginado;

                $reservas = $this->json_url($token, $url, "POST", $data);


                //$this->session->set_userdata('token', $clientes["token"]);
                $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_dashboard') . '/Reserva'), $total_reservas);

                $arrayMigaPan = array(
                    array("nombre" => "Reservas", 'active' => true)
                );

                $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
                $this->arrayVista['reservas'] = $reservas["reservas"];
                $this->arrayVista['locales'] = $reservas["locales"];
                $this->arrayVista['vista'] = 'dashboard/reserva/listar_view';
                $this->cargarVistaDashboard();
            } else
            {
                redirect($this->config->item('path_dashboard'));
            }
        } else
        {
            redirect(site_url($this->config->item('path_dashboard') . '/logout'));
        }

    }

    public function buscar($start = 0)
    {

        if ($this->session->userdata('logged_in') == true)
        {

            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");


                $nombre = $this->input->post("nombre", TRUE);
                $apellido = $this->input->post("apellido", TRUE);
                $dni = $this->input->post("dni", TRUE);
                $dia = $this->input->post("dia", TRUE);
                $id_local = $this->input->post("local", TRUE);
                $estado = $this->input->post("estado", TRUE);

                if ($this->session->userdata('id_local')!= null ){
                    $id_local = $this->session->userdata('id_local');
                }


                $url = url_servicios_dashoard . "/Reserva_solicitud/filtrar_reservas/1";

                $data = json_encode(array(
                    "nombre" => $nombre, "apellido" => $apellido, "dni" => $dni, "dia" => $dia, "id_local" => $id_local, "estado" => $estado,
                ));

                $reservas = $this->json_url($token, $url, "POST", $data);

                $total_reservas = $reservas["total_reservas"];

                $paginado = ($start / ELEMENTOS_POR_PAGINA) + 1;

                $url = url_servicios_dashoard . "/Reserva_solicitud/filtrar_reservas/" . $paginado;

                $reservas = $this->json_url($token, $url, "POST", $data);

                //$this->session->set_userdata('token', $clientes["token"]);
                $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_dashboard') . '/Reserva/buscar/'), $total_reservas);

                $arrayMigaPan = array(
                    array("nombre" => "Reservas", 'active' => true)
                );

                $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
                $this->arrayVista['reservas'] = $reservas["reservas"];
                $this->arrayVista['locales'] = $reservas["locales"];
                $this->arrayVista['nombre'] = $nombre;
                $this->arrayVista['apellido'] = $apellido;
                $this->arrayVista['dni'] = $dni;
                $this->arrayVista['dia'] = $dia;
                $this->arrayVista['local_select'] = $id_local;
                $this->arrayVista['estado'] = $estado;
                $this->arrayVista['vista'] = 'dashboard/reserva/listar_view';
                $this->cargarVistaDashboard();
            } else
            {
                redirect($this->config->item('path_dashboard'));
            }
        } else
        {
            redirect(site_url($this->config->item('path_dashboard') . '/logout'));
        }

    }

    public function detalle($id_reserva)
    {

        if ($this->session->userdata('logged_in') == true)
        {

            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");

                $url = url_servicios_dashoard . "Reserva_solicitud/detalle_reserva/" . $id_reserva;

                $reserva = $this->json_url($token, $url, "GET", null);

                $arrayMigaPan = array(
                    array("nombre" => "Reserva", "url" => site_url($this->config->item('path_dashboard') . '/Reserva')), array("nombre" => "Detalle de la Reserva", 'active' => true)
                );

                $this->horario_meses = $reserva["reserva"]["meses"];

                $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
                $this->arrayVista['reserva'] = $reserva["reserva"];
                $this->arrayVista['ubicacion'] = $reserva["ubicacion"];
                $this->arrayVista['vista'] = 'dashboard/reserva/detalle_view';
                $this->cargarVistaDashboard();
            } else
            {
                redirect($this->config->item('path_dashboard'));
            }
        } else
        {
            redirect(site_url($this->config->item('path_dashboard') . '/logout'));
        }

    }

    public function editar_reserva($id_reserva)
    {

        if ($this->session->userdata('logged_in') == true)
        {
            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");

                $ubicacion = $this->input->post("ubicacion", TRUE);
                $estado = $this->input->post("estado", TRUE);


                $data = json_encode(array(
                    'id_reserva' => $id_reserva, 'ubicacion' => $ubicacion, 'estado' => $estado
                ));


                $url = url_servicios_dashoard . "Reserva_solicitud/editar_reserva/";

                $reserva = $this->json_url($token, $url, "POST", $data);

                if ($reserva["resultado"] = 1)
                {
                    $this->alert($reserva["mensaje"], site_url('dashboard/Reserva/listar'));
                } else
                {
                    $this->alert($reserva["mensaje"]);
                }

            } else
            {
                redirect($this->config->item('path_dashboard'));
            }

        } else
        {
            redirect(site_url($this->config->item('path_dashboard') . '/logout'));
        }

    }

    public function buscar_fechas()
    {

        if ($this->session->userdata('logged_in') == true)
        {
            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");

            $fecha = $this->input->post("fecha", TRUE);
            $id_local = $this->input->post("id_local", TRUE);

            $url = url_servicios_dashoard . "Reserva_solicitud/buscar_reserva_fecha";

            $data = json_encode(array(
                "fecha" => $fecha, "id_local" => $id_local,
            ));

            $reserva = $this->json_url($token, $url, "POST", $data);

            $arrayMigaPan = array(
                array("nombre" => "Reserva"), array("nombre" => "Detalle de la Reserva", 'active' => true)
            );

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['reserva'] = $reserva["reserva"];
            $this->arrayVista['vista'] = 'dashboard/reserva/detalle_view';
            $this->cargarVistaDashboard();
            } else
            {
                redirect($this->config->item('path_dashboard'));
            }
        } else
        {
            redirect(site_url($this->config->item('path_dashboard') . '/logout'));
        }

    }

    public function horarios_reserva($id_local, $meses, $id_reserva)
    {


        if ($this->session->userdata('logged_in') == true)
        {
            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");

            $arrayMigaPan = array(
                array("nombre" => "Reserva", "url" => site_url($this->config->item('path_dashboard') . '/Reserva')), array("nombre" => "Detalle de la Reserva", "url" => site_url($this->config->item('path_dashboard') . '/Reserva/detalle/' . $id_reserva)), array("nombre" => "Horarios ", 'active' => true)
            );

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['local'] = $id_local;
            $this->arrayVista['meses'] = $meses;
            $this->arrayVista['reserva'] = $id_reserva;
            $this->arrayVista['vista'] = 'dashboard/reserva/horarios_reserva_view';
            $this->cargarVistaDashboard();
            } else
            {
                redirect($this->config->item('path_dashboard'));
            }
        } else
        {
            redirect(site_url($this->config->item('path_dashboard') . '/logout'));
        }

    }

    public function editar_horario_reserva($id_reserva)
    {

        if ($this->session->userdata('logged_in') == true)
        {
            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");

            $fecha = $this->input->post("fecha", TRUE);
            $horario = $this->input->post("horario", TRUE);
            $ubicacion = $this->input->post("ubicacion", TRUE);
            $cantidad = $this->input->post("cantidad", TRUE);
            $estado = $this->input->post("estado", TRUE);

            $horario_array = explode("|", $horario);
            $id_horario = $horario_array[0];
            $hora = $horario_array[1];

            $data = json_encode(array(
                'id_reserva' => $id_reserva, 'fecha' => $fecha, 'id_horario' => $id_horario, 'ubicacion' => $ubicacion, 'hora' => $hora, 'cantidad' => $cantidad, 'estado' => $estado
            ));


            $url = url_servicios_dashoard . "Reserva_solicitud/editar_horario_reserva/";

            $reserva = $this->json_url($token, $url, "POST", $data);

            if ($reserva["resultado"] = 1)
            {
                $this->alert($reserva["mensaje"], site_url('dashboard/Reserva/listar'));
            } else
            {
                $this->alert($reserva["mensaje"]);
            }

            } else
            {
                redirect($this->config->item('path_dashboard'));
            }

        } else
        {
            redirect(site_url($this->config->item('path_dashboard') . '/logout'));
        }

    }

    public function revisar_fechas_reservas(){

        if ($this->session->userdata('logged_in') == true) {

            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");

                $local = $this->input->post("id_local", TRUE);
                $fecha = $this->input->post("fecha", TRUE);


                $data = json_encode(array(
                                        'id_local' => $local,
                                        'fecha' => $fecha,
                                    ));

                $url = url_servicios_dashoard . "Reserva_solicitud/buscar_reserva_fecha";

                $horario = $this->json_url( $token,$url, "POST", $data);

                if ($horario["resultado"] == "1") {

                    $error = array("cod" => $horario["resultado"] , "horario"=>$horario["reserva"],"ubicacion"=>$horario["ubicacion"]);

                } else {
                    $error = array("cod" => $horario["resultado"],"message"=>$horario["mensaje"]);
                }
                echo json_encode($error);

            } else
            {
                redirect($this->config->item('path_dashboard'));
            }
        } else {
            redirect(site_url($this->config->item('path_dashboard') . '/logout'));
        }
    }

}
