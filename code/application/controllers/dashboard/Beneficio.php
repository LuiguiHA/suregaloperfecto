<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Beneficio extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

    }

    public function listar($start = 0)
    {
        if ($this->session->userdata('logged_in') == true)
        {
            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");

                $url = url_servicios_dashoard . "/beneficio/obtener_beneficios/1";

                $beneficios = $this->json_url($token, $url, "GET", null);
                //$this->session->set_userdata('token', $beneficios["token"]);

                $total_beneficios = $beneficios["total_beneficios"];


                $paginado = ($start / ELEMENTOS_POR_PAGINA) + 1;

                $url = url_servicios_dashoard . "/beneficio/obtener_beneficios/" . $paginado;

                $beneficios = $this->json_url($token, $url, "GET", null);


                $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_dashboard') . '/Beneficio'), $total_beneficios);

                $arrayMigaPan = array(
                    array("nombre" => "Consulta"), array("nombre" => "Canjes", 'active' => true)
                );

                $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;

                $this->arrayVista['beneficios'] = $beneficios["beneficios"];
                $this->arrayVista['vista'] = 'dashboard/beneficio/listar_view';
                $this->cargarVistaDashboard();

            } else
            {
                redirect($this->config->item('path_dashboard'));
            }
        } else
        {
            redirect(site_url($this->config->item('path_dashboard') . '/logout'));
        }
    }

    public function buscar($start = 0)
    {

        if ($this->session->userdata('logged_in') == true)
        {

            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");

                $nombre = $this->input->post("nombre", TRUE);
                $estado = $this->input->post("estado", TRUE);
                $fecha_inicio = $this->input->post("fecha_inicio", TRUE);
                $fecha_fin = $this->input->post("fecha_fin", TRUE);

                $url = url_servicios_dashoard . "/beneficio/filtrar_beneficios/1";

                $data = json_encode(array(
                    "nombre" => $nombre, "estado" => $estado, "fecha_inicio" => $fecha_inicio, "fecha_fin" => $fecha_fin
                ));


                $beneficios = $this->json_url($token, $url, "POST", $data);

                //$this->session->set_userdata('token', $beneficios["token"]);
                $total_beneficios = $beneficios["total_beneficios"];

                $paginado = ($start / 2) + 1;
                $url = url_servicios_dashoard . "/beneficio/filtrar_beneficios/" . $paginado;

                $beneficios = $this->json_url($token, $url, "POST", $data);
                //$this->session->set_userdata('token', $beneficios["token"]);

                $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_dashboard') . '/Beneficio/buscar/'), $total_beneficios);

                $arrayMigaPan = array(
                    array("nombre" => "Consulta"), array("nombre" => "Promociones", 'active' => true)
                );

                $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
                $this->arrayVista['nombre'] = $nombre;
                $this->arrayVista['estado'] = $estado;
                $this->arrayVista['fecha_inicio'] = $fecha_inicio;
                $this->arrayVista['fecha_fin'] = $fecha_fin;
                $this->arrayVista['beneficios'] = $beneficios["beneficios"];
                $this->arrayVista['vista'] = 'dashboard/beneficio/listar_view';
                $this->cargarVistaDashboard();

            } else
            {
                redirect($this->config->item('path_dashboard'));
            }
        } else
        {
            redirect(site_url($this->config->item('path_dashboard') . '/logout'));
        }

    }

    public function detalle($id_beneficio)
    {

        if ($this->session->userdata('logged_in') == true)
        {
            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");

                $url = url_servicios_dashoard . "/beneficio/detalle/" . $id_beneficio;

                $beneficio = $this->json_url($token, $url, "GET", null);

                $arrayMigaPan = array(
                    array("nombre" => "Consulta"), array("nombre" => "Beneficios", "url" => site_url($this->config->item('path_dashboard') . '/Beneficio')), array("nombre" => "Detalle", 'active' => true)
                );


                $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;

                $this->arrayVista['locales'] = $beneficio["locales"];
                $this->arrayVista['foto_detalle'] = $beneficio["foto_detalle"];
                $this->arrayVista['informacion_adicional'] = $beneficio["informacion_adicional"];
                $this->arrayVista['contacto'] = $beneficio["contacto"];
                $this->arrayVista['descripcion_canje'] = $beneficio["descripcion_canje"];
                $this->arrayVista['beneficio'] = $beneficio["beneficio"];
                $this->arrayVista['vista'] = 'dashboard/beneficio/detalle_view';
                $this->cargarVistaDashboard();


            } else
            {
                redirect($this->config->item('path_dashboard'));
            }


        } else
        {
            redirect(site_url($this->config->item('path_dashboard') . '/logout'));
        }
    }
}
