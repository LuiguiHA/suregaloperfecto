<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Promocion extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

    }

    public function listar($start = 0)
    {
     

        if ($this->session->userdata('logged_in') == true)
        {
            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");

                $url = url_servicios_dashoard . "/promocion/obtener_promociones/1";

                $promociones = $this->json_url($token, $url, "GET", null);
                //$this->session->set_userdata('token', $promociones["token"]);

                $total_promociones = $promociones["total_promocion"];

                $paginado = ($start /ELEMENTOS_POR_PAGINA) + 1;
                
                $url = url_servicios_dashoard . "/promocion/obtener_promociones/" . $paginado;
     
                $promociones = $this->json_url($token, $url, "GET", null);
                //$this->session->set_userdata('token', $promociones["token"]);

                $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_dashboard') . '/Promocion'), $total_promociones);

                $arrayMigaPan = array(
                    array("nombre" => "Consulta"), array("nombre" => "Promociones", 'active' => true)
                );

                $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;

                $this->arrayVista['promociones'] = $promociones["promociones"];
                $this->arrayVista['vista'] = 'dashboard/promocion/listar_view';
                $this->cargarVistaDashboard();
            } else
            {
                redirect($this->config->item('path_dashboard'));
            }
            } else
            {
                redirect(site_url($this->config->item('path_dashboard') . '/logout'));
            }
        
    }

    public function buscar($start = 0)
    {
       
        if ($this->session->userdata('logged_in') == true)
        {
            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");

                $nombre = $this->input->post("nombre", TRUE);
                $estado = $this->input->post("estado", TRUE);
                $fecha_inicio = $this->input->post("fecha_inicio", TRUE);
                $fecha_fin = $this->input->post("fecha_fin", TRUE);

                $url = url_servicios_dashoard . "/promocion/filtrar_promocion/1";

                $data = json_encode(array(
                    "nombre" => $nombre, "estado" => $estado, "fecha_inicio" => $fecha_inicio, "fecha_fin" => $fecha_fin
                ));
            
                $promociones = $this->json_url( $token,$url, "POST", $data);

                //$this->session->set_userdata('token', $promociones["token"]);
                $total_promociones = $promociones["total_promocion"];;

                $paginado = ($start / ELEMENTOS_POR_PAGINA) + 1;
                $url = url_servicios_dashoard . "/promocion/filtrar_promocion/" . $paginado;
         
                $promociones = $this->json_url( $token,$url, "POST", $data);
                //$this->session->set_userdata('token', $promociones["token"]);

                $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_dashboard') . '/Promocion/buscar/'), $total_promociones);

                $arrayMigaPan = array(
                    array("nombre" => "Consulta"), array("nombre" => "Promociones", 'active' => true)
                );

                $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
                $this->arrayVista['nombre'] = $nombre;
                $this->arrayVista['estado'] = $estado;
                $this->arrayVista['fecha_inicio'] = $fecha_inicio;
                $this->arrayVista['fecha_fin'] = $fecha_fin;
                $this->arrayVista['promociones'] = $promociones["promociones"];
                $this->arrayVista['vista'] = 'dashboard/promocion/listar_view';
                $this->cargarVistaDashboard();
            } else
            {
                redirect($this->config->item('path_dashboard'));
            }
            } else
            {
                redirect(site_url($this->config->item('path_dashboard') . '/logout'));
            }
    }

    public function detalle($id_promocion)
    {
        
        if ($this->session->userdata('logged_in') == true)
        {
            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");

                $url = url_servicios_dashoard . "/promocion/detalle/".$id_promocion;
            
                $promocion = $this->json_url($token,$url, "GET", null);

                //$this->session->set_userdata('token', $promocion["token"]);
            
                $arrayMigaPan = array(
                    array("nombre" => "Consulta"),  array("nombre" => "Promociones","url"=>site_url($this->config->item('path_dashboard') . '/Promocion')), array("nombre" => "Detalle", 'active' => true)
                );


                $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
                $this->arrayVista['informacion_adicional'] = $promocion["informacion_adicional"];
                $this->arrayVista['foto_detalle'] = $promocion["foto_detalle"];
                $this->arrayVista['locales'] = $promocion["locales"];
                $this->arrayVista['promocion'] = $promocion["promocion"];
                $this->arrayVista['vista'] = 'dashboard/promocion/detalle_view';
                $this->cargarVistaDashboard();
            } else
            {
                redirect($this->config->item('path_dashboard'));
            }
            } else
            {
                redirect(site_url($this->config->item('path_dashboard') . '/logout'));
            }
        
    }

}
