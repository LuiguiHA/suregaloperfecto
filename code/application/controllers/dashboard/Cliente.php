<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Cliente extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

    }

    public function listar($start = 0)
    {

        if ($this->session->userdata('logged_in') == true)
        {

            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");

                $url = url_servicios_dashoard . "/cliente/obtener_clientes/1";


                $clientes = $this->json_url($token, $url, "GET", null);

                //$this->session->set_userdata('token', $clientes["token"]);

                $total_clientes = $clientes["total_usuarios"];
                $paginado = ($start / ELEMENTOS_POR_PAGINA) + 1;

                $url = url_servicios_dashoard . "/cliente/obtener_clientes/" . $paginado;

                $clientes = $this->json_url($token, $url, "GET", null);


                //$this->session->set_userdata('token', $clientes["token"]);


                $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_dashboard') . '/Cliente'), $total_clientes);

                $arrayMigaPan = array(
                    array("nombre" => "Clientes", 'active' => true)
                );

                $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
                $this->arrayVista['clientes'] = $clientes["usuarios"];
                $this->arrayVista['vista'] = 'dashboard/cliente/listar_view';
                $this->cargarVistaDashboard();
            } else
            {
                redirect($this->config->item('path_dashboard'));
            }

        } else
        {
            redirect(site_url($this->config->item('path_dashboard') . '/logout'));
        }
    }

    public function buscar($start = 0)
    {


        if ($this->session->userdata('logged_in') == true)
        {

            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");

                $nombre = $this->input->post("nombre", TRUE);
                $apellido = $this->input->post("apellido", TRUE);
                $dni = $this->input->post("dni", TRUE);
                $correo = $this->input->post("correo", TRUE);

                $url = url_servicios_dashoard . "/cliente/buscar_cliente/1";

                $data = json_encode(array(
                    "nombre" => $nombre, "apellido" => $apellido, "dni" => $dni, "correo" => $correo
                ));

                $clientes = $this->json_url($token, $url, "POST", $data);

                //$this->session->set_userdata('token', $clientes["token"]);

                $total_clientes = $clientes["total_usuarios"];
                $paginado = ($start / ELEMENTOS_POR_PAGINA) + 1;

                $url = url_servicios_dashoard . "/cliente/buscar_cliente/" . $paginado;

                $clientes = $this->json_url($token, $url, "POST", $data);


                //$this->session->set_userdata('token', $clientes["token"]);

                $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_dashboard') . '/Cliente/buscar/'), $total_clientes);


                $arrayMigaPan = array(
                    array("nombre" => "Clientes", 'active' => true)
                );

                $this->arrayVista['correo'] = $correo;
                $this->arrayVista['dni'] = $dni;
                $this->arrayVista['apellido'] = $apellido;
                $this->arrayVista['nombre'] = $nombre;
                $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
                $this->arrayVista['clientes'] = $clientes["usuarios"];
                $this->arrayVista['vista'] = 'dashboard/cliente/listar_view';
                $this->cargarVistaDashboard();
            } else
            {
                redirect($this->config->item('path_dashboard'));
            }

        } else
        {
            redirect(site_url($this->config->item('path_dashboard') . '/logout'));
        }

    }

    public function detalle($id_cliente)
    {
        if ($this->session->userdata('logged_in') == true)
        {
            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");

                $url = url_servicios_dashoard . "cliente/obtener_cliente/" . $id_cliente;

                $cliente = $this->json_url($token, $url, "GET", null);

                //$this->session->set_userdata('token', $cliente["token"]);

                $arrayMigaPan = array(
                    array("nombre" => "Clientes"), array("nombre" => "Detalle de Cliente", 'active' => true)
                );

                $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;

                $this->arrayVista['cliente'] = $cliente["cliente"];
                $this->arrayVista['vista'] = 'dashboard/cliente/detalle_view';
                $this->cargarVistaDashboard();

            } else
            {
                redirect($this->config->item('path_dashboard'));
            }

        } else
        {
            redirect(site_url($this->config->item('path_dashboard') . '/logout'));
        }
    }

    public function historial($id_cliente)
    {


        if ($this->session->userdata('logged_in') == true)
        {
            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");

                $url = url_servicios_dashoard . "cliente/obtener_historial/" . $id_cliente;

                $clientes = $this->json_url($token, $url, "GET", null);
                //$this->session->set_userdata('token', $clientes["token"]);

                $arrayMigaPan = array(
                    array("nombre" => "Cliente"), array("nombre" => "Histotial", 'active' => true)
                );

                $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
                $this->arrayVista['locales'] = $clientes["locales"];
                $this->arrayVista['detalle_cliente'] = $clientes["detalle_cliente"];
                $this->arrayVista['puntos'] = $clientes["puntos"];
                $this->arrayVista['canjes'] = $clientes["canjes"];
                $this->arrayVista['vista'] = 'dashboard/cliente/historial_view';
                $this->cargarVistaDashboard();
            } else
            {
                redirect($this->config->item('path_dashboard'));
            }

        } else
        {
            redirect(site_url($this->config->item('path_dashboard') . '/logout'));
        }

    }

    public function historial_buscar($id_cliente)
    {

        if ($this->session->userdata('logged_in') == true)
        {

            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");

                $local = $this->input->post("local", TRUE);
                $fecha_inicio = $this->input->post("fecha_inicio", TRUE);
                $fecha_fin = $this->input->post("fecha_fin", TRUE);


                $url = url_servicios_dashoard . "cliente/filtrar_historial";

                $data = json_encode(array(
                    "id_local" => $local, "id_usuario" => $id_cliente, "fecha_inicio" => $fecha_inicio, "fecha_fin" => $fecha_fin,
                ));

                $clientes = $this->json_url($token, $url, "POST", $data);

                //$this->session->set_userdata('token', $clientes["token"]);

                $arrayMigaPan = array(
                    array("nombre" => "Cliente"), array("nombre" => "Histotial", 'active' => true)
                );

                $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
                $this->arrayVista['local_select'] = $local;
                $this->arrayVista['fecha_inicio'] = $fecha_inicio;
                $this->arrayVista['fecha_fin'] = $fecha_fin;
                $this->arrayVista['locales'] = $clientes["locales"];
                $this->arrayVista['detalle_cliente'] = $clientes["detalle_cliente"];
                $this->arrayVista['puntos'] = $clientes["puntos"];
                $this->arrayVista['canjes'] = $clientes["canjes"];
                $this->arrayVista['vista'] = 'dashboard/cliente/historial_view';
                $this->cargarVistaDashboard();
            } else
            {
                redirect($this->config->item('path_dashboard'));
            }

        } else
        {
            redirect(site_url($this->config->item('path_dashboard') . '/logout'));
        }

    }

    public function descuento($id_cliente)
    {


        if ($this->session->userdata('logged_in') == true)
        {

            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");


                $url = url_servicios_dashoard . "cliente/detalle_descuento/" . $id_cliente;

                $descuentos = $this->json_url($token, $url, "GET", null);

                //$this->session->set_userdata('token', $descuentos["token"]);

                $arrayMigaPan = array(
                    array("nombre" => "Clientes"), array("nombre" => "Aplicar Canje", 'active' => true)
                );

                $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;

                $this->arrayVista['descuentos'] = $descuentos["datos_descuento"]["descuento"];
                $this->arrayVista['tipo_pagos'] = $descuentos["datos_descuento"]["tipo_pago"];
                $this->arrayVista['detalle_cliente'] = $descuentos["detalle_cliente"];
                $this->arrayVista['promociones'] = $descuentos["promociones_canjeadas"];
                $this->arrayVista['vista'] = 'dashboard/cliente/aplicar_descuento_view';
                $this->cargarVistaDashboard();
            } else
            {
                redirect($this->config->item('path_dashboard'));
            }

        } else
        {
            redirect(site_url($this->config->item('path_dashboard') . '/logout'));
        }

    }

    public function descuento_aplicar($id_cliente)
    {

        if ($this->session->userdata('logged_in') == true)
        {
            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");

                $descuento = $this->input->post("descuento", TRUE);
                $tipo_pago = $this->input->post("tipo_pago", TRUE);
                $monto = $this->input->post("monto", TRUE);

                $id_local = $this->session->userdata('id_local');


                $data = json_encode(array(
                    "id_usuario" => $id_cliente, "id_local" => $id_local, "id_promocion" => $descuento, "tipo_pago" => $tipo_pago, "monto" => $monto
                ));
                $url = url_servicios_dashoard . "cliente/realizar_descuento";

                $descuento = $this->json_url($token, $url, "POST", $data);

                //$this->session->set_userdata('token', $descuento["token"]);

                if ($descuento["resultado"] == "1")
                {
                    $this->session->set_userdata("tipo_mensaje", "success");
                    $this->session->set_userdata('mensaje', $descuento["mensaje"]);
                } else
                {
                    $this->session->set_userdata("tipo_mensaje", "Error");
                    $this->session->set_userdata('mensaje', $descuento["mensaje"]);
                }
                redirect($this->config->item('path_dashboard') . '/Cliente/descuento/' . $id_cliente);
            } else
            {
                redirect($this->config->item('path_dashboard'));
            }

        } else
        {
            redirect(site_url($this->config->item('path_dashboard') . '/logout'));
        }

    }

    public function detalle_canjear_puntos($id_cliente, $start = 0)
    {

        if ($this->session->userdata('logged_in') == true)
        {
            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");

            $url = url_servicios_dashoard . "cliente/detalle_canjear_puntos/" . $id_cliente . "/1";

            $canje_puntos = $this->json_url($token, $url, "GET", null);

            //$this->session->set_userdata('token', $canje_puntos["token"]);

            $total_canje_puntos = $canje_puntos["total_canje"];

            $paginado = ($start / ELEMENTOS_POR_PAGINA) + 1;

            $url = url_servicios_dashoard . "/cliente/detalle_canjear_puntos/" . $id_cliente . "/" . $paginado;

            $canje_puntos = $this->json_url($token, $url, "GET", null);
            //$this->session->set_userdata('token', $canje_puntos["token"]);

            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_dashboard') . '/Cliente/detalle_canjear_puntos/' . $id_cliente), $total_canje_puntos, 5);


            $arrayMigaPan = array(
                array("nombre" => "Clientes"), array("nombre" => "Canjear Puntos", 'active' => true)
            );

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;

            $this->arrayVista['canjes'] = $canje_puntos["canjes"];
            $this->arrayVista['detalle_cliente'] = $canje_puntos["detalle_cliente"];
            $this->arrayVista['beneficios'] = $canje_puntos["beneficios"];
            $this->arrayVista['vista'] = 'dashboard/cliente/canjear_puntos_view';
            $this->cargarVistaDashboard();
            } else
            {
                redirect($this->config->item('path_dashboard'));
            }

            } else
        {
            redirect(site_url($this->config->item('path_dashboard') . '/logout'));
        }
    }

    public function canjear_puntos($id_cliente)
    {

        if ($this->session->userdata('logged_in') == true)
        {

            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");

            $canje = $this->input->post("canje", TRUE);
            $punto = $this->input->post("punto", TRUE);
            $monto = $this->input->post("monto", TRUE);

            $id_local = $this->session->userdata('id_local');


            $data = json_encode(array(
                "id_usuario" => $id_cliente, "id_beneficio" => $canje, "id_local" => $id_local, "puntos" => $punto, "monto_adicional" => $monto

            ));

            $url = url_servicios_dashoard . "cliente/canjear_puntos";

            $canje = $this->json_url($token, $url, "POST", $data);

            //$this->session->set_userdata('token', $canje["token"]);

            if ($canje["resultado"] == "1")
            {
                $this->session->set_userdata("tipo_mensaje", "success");
                $this->session->set_userdata('mensaje', $canje["mensaje"]);
            } else
            {
                $this->session->set_userdata("tipo_mensaje", "error");
                $this->session->set_userdata('mensaje', $canje["mensaje"]);
            }
            redirect($this->config->item('path_dashboard') . '/Cliente/detalle_canjear_puntos/' . $id_cliente);
            } else
            {
                redirect($this->config->item('path_dashboard'));
            }

        } else
        {
            redirect(site_url($this->config->item('path_dashboard') . '/logout'));
        }

    }

    public function detalle_agregar_puntos($id_cliente, $start = 0)
    {


        if ($this->session->userdata('logged_in') == true)
        {
            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");

            $url = url_servicios_dashoard . "cliente/detalle_agregar_puntos/" . $id_cliente;

            $agregar_puntos = $this->json_url($token, $url, "GET", null);
            //$this->session->set_userdata('token', $agregar_puntos["token"]);

            $total_agregar_puntos = $agregar_puntos["total_puntos"];

            $paginado = ($start / ELEMENTOS_POR_PAGINA) + 1;

            $url = url_servicios_dashoard . "/cliente/detalle_agregar_puntos/" . $id_cliente . "/" . $paginado;

            $agregar_puntos = $this->json_url($token, $url, "GET", null);
            //$this->session->set_userdata('token', $agregar_puntos["token"]);

            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_dashboard') . '/Cliente/detalle_agregar_puntos/' . $id_cliente), $total_agregar_puntos, 5);


            $arrayMigaPan = array(
                array("nombre" => "Clientes"), array("nombre" => "Agregar Puntos", 'active' => true)
            );

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;


            $this->arrayVista['tipo_pagos'] = $agregar_puntos["tipo_pago"];
            $this->arrayVista['detalle_cliente'] = $agregar_puntos["detalle_cliente"];
            $this->arrayVista['puntos'] = $agregar_puntos["puntos"];
            $this->arrayVista['vista'] = 'dashboard/cliente/agregar_puntos_view';
            $this->cargarVistaDashboard();


            } else
            {
                redirect($this->config->item('path_dashboard'));
            }
            } else
        {
            redirect(site_url($this->config->item('path_dashboard') . '/logout'));
        }


    }

    public function agregar_puntos($id_cliente)
    {

        if ($this->session->userdata('logged_in') == true)
        {

            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");

                $tipo_pago = $this->input->post("tipo_pago", TRUE);
                $monto = $this->input->post("monto", TRUE);

                $id_local = $this->session->userdata('id_local');


                $data = json_encode(array(
                    "id_usuario" => $id_cliente, "id_local" => $id_local, "tipo_pago" => $tipo_pago, "monto" => $monto

                ));

                $url = url_servicios_dashoard . "cliente/agregar_puntos";

                $agregar = $this->json_url($token, $url, "POST", $data);;
                //$this->session->set_userdata('token', $agregar["token"]);

                if ($agregar["resultado"] == "1")
                {
                    $this->session->set_userdata("tipo_mensaje", "success");
                    $this->session->set_userdata('mensaje', $agregar["mensaje"]);
                } else
                {
                    $this->session->set_userdata("tipo_mensaje", "error");
                    $this->session->set_userdata('mensaje', $agregar["mensaje"]);
                }
                redirect($this->config->item('path_dashboard') . '/Cliente/detalle_agregar_puntos/' . $id_cliente);
            } else
            {
                redirect($this->config->item('path_dashboard'));
            }

             } else
        {
            redirect(site_url($this->config->item('path_dashboard') . '/logout'));
        }

    }

}
