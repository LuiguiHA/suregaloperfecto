<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Horario_exclusion extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function listar($start = 0)
    {
   

        if ($this->session->userdata('logged_in') == true) {

            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");
                $id_local= $this->session->userdata("id_local");

                $data = json_encode(array(
                                        "id_local" => $id_local
                                    ));
        
                $url = url_servicios_dashoard . "/Horario_exclusion/obtener_horarios_exclusion/1";
                $horarios = $this->json_url( $token,$url, "POST", $data);


                $total_horarios = $horarios["total_horarios"];

                $paginado = ($start / ELEMENTOS_POR_PAGINA) + 1;

                $url = url_servicios_dashoard . "/Horario_exclusion/obtener_horarios_exclusion/" . $paginado;
            
                $horarios = $this->json_url($token, $url, "POST", $data);

                $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_dashboard') . '/Horario_exclusion'), $total_horarios);

                $arrayMigaPan = array(
                    array("nombre" => "Horarios excluidos", 'active' => true)
                );


                $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
                $this->arrayVista['horarios'] = $horarios["horarios"];
                $this->arrayVista['locales'] = $horarios["locales"];
                $this->arrayVista['vista'] = 'dashboard/horario_exclusion/listar_view';
                $this->cargarVistaDashboard();
            } else
            {
                redirect($this->config->item('path_dashboard'));
            }

            } else {
                redirect(site_url($this->config->item('path_dashboard') . '/logout'));
            }
    }

    public function buscar($start = 0)
    {
      

        if ($this->session->userdata('logged_in') == true) {

            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");
            
                $tipo = $this->input->post("tipo", TRUE);
                $fecha = $this->input->post("fecha", TRUE);
                $local = $this->input->post("local", TRUE);
                $estado = $this->input->post("estado", TRUE);

                if ($this->session->userdata('id_local') != null ){
                    $local = $this->session->userdata('id_local');
                }

                $url = url_servicios_dashoard . "/Horario_exclusion/filtrar_horarios_exclusion/1";

                $data = json_encode(array(
                    "tipo" => $tipo,
                    "fecha" => $fecha,
                    "local" => $local,
                    "estado" => $estado,
                ));

                $horarios = $this->json_url($token,$url, "POST", $data);

                $total_horarios = $horarios["total_horarios"];

                $paginado = ($start / ELEMENTOS_POR_PAGINA) + 1;

                $url = url_servicios_dashoard . "/Horario_exclusion/filtrar_horarios_exclusion/" . $paginado;
  
                $horarios = $this->json_url( $token,$url, "POST", $data);
            
                $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_dashboard') . '/Horario_exclusion/buscar/'), $total_horarios);

                $arrayMigaPan = array(
                    array("nombre" => "Horarios excluidos", 'active' => true)
                );

                $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
                $this->arrayVista['horarios'] = $horarios["horarios"];
                $this->arrayVista['locales'] = $horarios["locales"];
                $this->arrayVista['tipo'] = $tipo;
                $this->arrayVista['fecha'] = $fecha;
                $this->arrayVista['local_select'] = $local;
                $this->arrayVista['estado'] = $estado;
                $this->arrayVista['vista'] = 'dashboard/horario_exclusion/listar_view';
                $this->cargarVistaDashboard();
            } else
            {
                redirect($this->config->item('path_dashboard'));
            }

            } else {
                redirect(site_url($this->config->item('path_dashboard') . '/logout'));
            }
    }

    public function detalle($id_horario_exclusion)
    {
        
        if ($this->session->userdata('logged_in') == true) {

            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");
            
                $url = url_servicios_dashoard . "Horario_exclusion/detalle_horario_exclusion/" . $id_horario_exclusion;

                $horario = $this->json_url($token,$url, "GET", null);

                $arrayMigaPan = array(
                    array("nombre" => "Horarios excluidos", "url" => site_url($this->config->item('path_dashboard') . '/Horario_exclusion')), array("nombre" => "Detalle del horario", 'active' => true)
                );

                $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
                $this->arrayVista['horario'] = $horario["horario"];
                $this->arrayVista['vista'] = 'dashboard/horario_exclusion/detalle_view';
                $this->cargarVistaDashboard();
            } else
            {
                redirect($this->config->item('path_dashboard'));
            }
            } else {
                redirect(site_url($this->config->item('path_dashboard') . '/logout'));
            }
        
    }

    public function agregar_view()
    {
        
        if ($this->session->userdata('logged_in') == true) {

            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");
            
                $url = url_servicios_dashoard . "Horario_exclusion/detalle_listado_locales/";

                $locales = $this->json_url($token,$url, "GET", null);
            
                $arrayMigaPan = array(
                    array("nombre" => "Horarios excluidos", "url" => site_url($this->config->item('path_dashboard') . '/Horario_exclusion')), array("nombre" => "Agregar horario excluido", 'active' => true)
                );

                $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
                $this->arrayVista['locales'] = $locales["locales"];
                $this->arrayVista['vista'] = 'dashboard/horario_exclusion/agregar_view';
                $this->cargarVistaDashboard();
            } else
            {
                redirect($this->config->item('path_dashboard'));
            }
            } else {
                redirect(site_url($this->config->item('path_dashboard') . '/logout'));
            }
        
    }

    public function editar($id_horario_exclusion)
    {
        
        if ($this->session->userdata('logged_in') == true) {

            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");

                $fecha = $this->input->post("fecha", TRUE);
                $tipo = $this->input->post("tipo", TRUE);
                $horario = $this->input->post("horario", TRUE);
                $hora_inicio = $this->input->post("hora_inicio", TRUE);
                $hora_fin = $this->input->post("hora_fin", TRUE);
                $estado = $this->input->post("estado", TRUE);

                if ($tipo == 0) {
                    $hora_inicio = "";
                    $hora_fin = "";
                } else {
                    $time_start = DateTime::createFromFormat('H:i A', $hora_inicio);
                    $hora_inicio = $time_start->format('H:i:s');

                    $time_end = DateTime::createFromFormat('H:i A', $hora_fin);
                    $hora_fin = $time_end->format('H:i:s');
                }

                $data = json_encode(array(
                    'id_horario_exclusion' => $id_horario_exclusion,
                    'fecha' => $fecha,
                    'tipo' => $tipo,
                    'horario' => $horario,
                    'hora_inicio' => $hora_inicio,
                    'hora_fin' => $hora_fin,
                    'estado' => $estado
                ));

                $url = url_servicios_dashoard . "Horario_exclusion/editar_horario_exclusion/";

                $reserva = $this->json_url( $token,$url, "POST", $data);

                if ($reserva["resultado"] = 1) {
                    $this->alert($reserva["mensaje"], site_url('dashboard/Horario_exclusion/listar'));
                } else {
                    $this->alert($reserva["mensaje"]);
                }
            } else
            {
                redirect($this->config->item('path_dashboard'));
            }

            } else {
                redirect(site_url($this->config->item('path_dashboard') . '/logout'));
            }
        
    }

    public function agregar()
    {
    

        if ($this->session->userdata('logged_in') == true) {

            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");
            
                $horario = $this->input->post("horario", TRUE);
                $local = $this->input->post("local", TRUE);
                $fecha = $this->input->post("fecha", TRUE);
                $tipo = $this->input->post("tipo", TRUE);
                $hora_inicio = $this->input->post("hora_inicio", TRUE);
                $hora_fin = $this->input->post("hora_fin", TRUE);
                $estado = $this->input->post("estado", TRUE);

                if ($this->session->userdata('id_local') != null ){
                    $local = $this->session->userdata('id_local');
                }

                if ($tipo == 0) {
                    $hora_inicio = "";
                    $hora_fin = "";
                } else {
                    $time_start = DateTime::createFromFormat('H:i A', $hora_inicio);
                    $hora_inicio = $time_start->format('H:i:s');

                    $time_end = DateTime::createFromFormat('H:i A', $hora_fin);
                    $hora_fin = $time_end->format('H:i:s');
                }


                $data = json_encode(array(
                    'id_horario' => $horario,
                    'id_local' => $local,
                    'fecha' => $fecha,
                    'tipo' => $tipo,
                    'hora_inicio' => $hora_inicio,
                    'hora_fin' => $hora_fin,
                    'estado' => $estado
                ));

                $url = url_servicios_dashoard . "Horario_exclusion/agregar_horarios_exclusion/";

                $reserva = $this->json_url( $token,$url, "POST", $data);

                if ($reserva["resultado"] = 1) {
                    $this->alert($reserva["mensaje"], site_url('dashboard/Horario_exclusion/listar'));
                } else {
                    $this->alert($reserva["mensaje"]);
                }

            } else
            {
                redirect($this->config->item('path_dashboard'));
            }

            } else {
                redirect(site_url($this->config->item('path_dashboard') . '/logout'));
            }
    }

    public function revisar_horario_exclusion()
    {

        if ($this->session->userdata('logged_in') == true) {

            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");

                $horario_exclusion = $this->input->post("id_horario_exclusion", TRUE);
                $horario = $this->input->post("id_horario", TRUE);
                $local = $this->input->post("id_local", TRUE);
                $fecha = $this->input->post("fecha", TRUE);
                $tipo = $this->input->post("tipo", TRUE);

                $data = json_encode(array(
                    'id_horario_exclusion'=>$horario_exclusion,
                    'id_horario' => $horario,
                    'id_local' => $local,
                    'fecha' => $fecha,
                    'tipo' => $tipo,

                ));

                $url = url_servicios_dashoard . "Horario_exclusion/revisar_horarios_exclusion/";

                $horario = $this->json_url( $token,$url, "POST", $data);

                if ($horario["resultado"] = 1) {
                    if (sizeof($horario["horarios"]) > 0) {
                        $error = array("cod" => 1);
                    } else {
                        $error = array("cod" => 0);
                    }
                } else {
                    $error = array("cod" => 0);
                }
                echo json_encode($error);

            } else
            {
                redirect($this->config->item('path_dashboard'));
            }
            } else {
                redirect(site_url($this->config->item('path_dashboard') . '/logout'));
            }
        
    }

    public function revisar_fechas_reservas(){

        if ($this->session->userdata('logged_in') == true) {

            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");

                $local = $this->input->post("id_local", TRUE);
                $fecha = $this->input->post("fecha", TRUE);


                $data = json_encode(array(
                                        'id_local' => $local,
                                        'fecha' => $fecha,
                                    ));

                $url = url_servicios_dashoard . "Horario_exclusion/buscar_reserva_fecha/";

                $horario = $this->json_url( $token,$url, "POST", $data);

                if ($horario["resultado"] = 1) {

                        $error = array("cod" => $horario["resultado"] , "horario"=>$horario["reserva"]);

                } else {
                    $error = array("cod" => 0);
                }
                echo json_encode($error);

            } else
            {
                redirect($this->config->item('path_dashboard'));
            }
        } else {
            redirect(site_url($this->config->item('path_dashboard') . '/logout'));
        }
    }

}
