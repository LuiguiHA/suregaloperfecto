<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Carta extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

    }

    public function listar($start = 0)
    {

        if ($this->session->userdata('logged_in') == true)
        {
            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");

                $url = url_servicios_dashoard . "/carta/obtener_cartas/1";

                $cartas = $this->json_url($token, $url, "GET", null);
                //$this->session->set_userdata('token', $cartas["token"]);

                $total_cartas = $cartas["total_cartas"];


                $paginado = ($start / ELEMENTOS_POR_PAGINA) + 1;

                $url = url_servicios_dashoard . "/carta/obtener_cartas/" . $paginado;

                $cartas = $this->json_url($token, $url, "GET", null);
                //$this->session->set_userdata('token', $cartas["token"]);

                $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_dashboard') . '/Carta'), $total_cartas);

                $arrayMigaPan = array(
                    array("nombre" => "Consulta"), array("nombre" => "Carta", 'active' => true)
                );

                $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
                $this->arrayVista['categorias'] = $cartas["categorias"];
                $this->arrayVista['cartas'] = $cartas["cartas"];
                $this->arrayVista['vista'] = 'dashboard/carta/listar_view';
                $this->cargarVistaDashboard();
            } else
            {
                redirect($this->config->item('path_dashboard'));
            }

        } else
        {
            redirect(site_url($this->config->item('path_dashboard') . '/logout'));
        }


    }

    public function buscar($start = 0)
    {


        if ($this->session->userdata('logged_in') == true)
        {
            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");

            $nombre = $this->input->post("nombre", TRUE);
            $estado = $this->input->post("estado", TRUE);
            $categoria = $this->input->post("categoria", TRUE);


            $url = url_servicios_dashoard . "/carta/buscar/1";

            $data = json_encode(array(
                "nombre" => $nombre, "estado" => $estado, "id_categoria" => $categoria
            ));


            $cartas = $this->json_url($token, $url, "POST", $data);

            $total_cartas = $cartas["total_cartas"];
            $paginado = ($start / ELEMENTOS_POR_PAGINA) + 1;
            $url = url_servicios_dashoard . "/carta/buscar/" . $paginado;
            $cartas = $this->json_url($token, $url, "POST", $data);


            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_dashboard') . '/Carta/buscar/'), $total_cartas);

            $arrayMigaPan = array(
                array("nombre" => "Consulta"), array("nombre" => "Carta", 'active' => true)
            );


            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['nombre'] = $nombre;
            $this->arrayVista['estado'] = $estado;
            $this->arrayVista['categoria_select'] = $categoria;
            $this->arrayVista['categorias'] = $cartas["categorias"];
            $this->arrayVista['cartas'] = $cartas["cartas"];
            $this->arrayVista['vista'] = 'dashboard/carta/listar_view';
            $this->cargarVistaDashboard();

            } else
            {
                redirect($this->config->item('path_dashboard'));
            }

            } else
        {
            redirect(site_url($this->config->item('path_dashboard') . '/logout'));
        }

    }

    public function detalle($id_carta)
    {


        if ($this->session->userdata('logged_in') == true)
        {

            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");

            $url = url_servicios_dashoard . "/carta/detalle/" . $id_carta;

            $carta = $this->json_url($token, $url, "GET", null);

            //$this->session->set_userdata('token', $carta["token"]);

            $arrayMigaPan = array(
                array("nombre" => "Consulta"), array("nombre" => "Cartas", "url" => site_url($this->config->item('path_dashboard') . '/Carta')), array("nombre" => "Detalle", 'active' => true)
            );


            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;

            $this->arrayVista['locales'] = $carta["locales"];
            $this->arrayVista['empresa'] = $carta["empresa"];
            $this->arrayVista['foto'] = $carta["foto"];
            $this->arrayVista['carta'] = $carta["carta"];
            $this->arrayVista['vista'] = 'dashboard/carta/detalle_view';
            $this->cargarVistaDashboard();
            } else
            {
                redirect($this->config->item('path_dashboard'));
            }
       
        } else
        {
            redirect(site_url($this->config->item('path_dashboard') . '/logout'));
        }

    }

}
