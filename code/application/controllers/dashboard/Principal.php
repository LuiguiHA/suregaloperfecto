<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Principal extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

    }

    public function index()
    {

        if ($this->session->userdata('logged_in') == true)
        {

            if ($this->session->userdata("token") != "" || $this->session->userdata("token") != null)
            {
                $token = $this->session->userdata("token");

            $url = url_servicios_dashoard . "principal/obtener";

            $id_local = $this->session->userdata("id_local");

                $data = json_encode(array(
                                        "id_local" => $id_local
                                    ));

            $dashboard = $this->json_url($token, $url, "POST", $data);
            //$this->session->set_userdata('token', $dashboard["token"]);

            ///////////Graficos
            $canjes_grafico = $dashboard["beneficios_grafico"];
            $reservas_grafico = $dashboard["reservas_grafico"];
            $acumulaciones_grafico = $dashboard["acumulacion_grafico"];

            /* FECHAS */

            $dias = array("Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sáb");
            $cad = ' "';
            $f = date("Y-m-d");
            $fecha = "";
            $fechas = array();
            for ($i = 6; $i > 0; $i--)
            {
                $fecha .= $cad . $dias[date('w', strtotime("$f   -$i day"))] . " " . date("d/m", strtotime("$f   -$i day")) . $cad . ",";
                $fechas[$i] = date("Y-m-d", strtotime("$f   -$i day"));
            }
            $fecha .= $cad . $dias[date('w')] . " " . date("d/m") . $cad;
            $fechas[] = $f;

            $fechas = array_values($fechas);

          /* CANJES */

            $total_canje = "";
            $grafico_canje = "";
            $total_max_canje = 1;


            for ($i = 0; $i < sizeof($canjes_grafico); $i++)
            {
                $contador = 1;
                for ($j = $i + 1; $j < sizeof($canjes_grafico); $j++)
                {
                    if ($canjes_grafico[$i]["nombre"] == $canjes_grafico[$j]["nombre"])
                    {
                        $contador++;
                    }
                }
                if ($contador == "1")
                {
                    $array_fecha = array();
                    $array_total = array();

                    for ($k = 0; $k < sizeof($canjes_grafico); $k++)
                    {
                        if ($canjes_grafico[$k]["nombre"] == $canjes_grafico[$i]["nombre"])
                        {
                            if (in_array($canjes_grafico[$k]["fecha"], $fechas))
                            {

                                $array_fecha[$k] = $canjes_grafico[$k]["fecha"];
                                $array_total[$k] = $canjes_grafico[$k]["total_canje"];

                            }
                        }
                    }

                    $string_bene_fecha = implode(' ', $array_fecha);
                    $array_fecha = array_values($array_fecha);
                    $array_total = array_values($array_total);
                    $total_max_canje =  max($array_total);
                    for ($j = 0; $j < sizeof($fechas); $j++)
                    {
                        if (substr_count($string_bene_fecha, $fechas[$j]) > 0)
                        {
                            for ($p = 0; $p < sizeof($array_fecha); $p++)
                            {
                                if ($array_fecha[$p] == $fechas[$j])
                                {
                                    $total_canje .= $array_total[$p] . ",";

                                }
                            }
                        } else
                        {
                            $total_canje .= "0" . ",";
                        }
                    }

                    $grafico_canje .= "{
                        label : '" . $canjes_grafico[$i]["nombre"] . "' , data :[
                       " . $total_canje . "
                     ]},";

                    $total_canje = null;
                }
            }

                /* RESERVAS */

                $total_reserva = "";
                $grafico_reserva = "";
                $total_max_reserva = 1;


                for ($i = 0; $i < sizeof($reservas_grafico); $i++)
                {
                    $contador = 1;
                    for ($j = $i + 1; $j < sizeof($reservas_grafico); $j++)
                    {
                        if ($reservas_grafico[$i]["nombre"] == $reservas_grafico[$j]["nombre"])
                        {
                            $contador++;
                        }
                    }
                    if ($contador == "1")
                    {
                        $array_fecha = array();
                        $array_total = array();

                        for ($k = 0; $k < sizeof($reservas_grafico); $k++)
                        {
                            if ($reservas_grafico[$k]["nombre"] == $reservas_grafico[$i]["nombre"])
                            {
                                if (in_array($reservas_grafico[$k]["fecha"], $fechas))
                                {

                                    $array_fecha[$k] = $reservas_grafico[$k]["fecha"];
                                    $array_total[$k] = $reservas_grafico[$k]["total_reserva"];

                                }
                            }
                        }

                        $string_reserva_fecha = implode(' ', $array_fecha);
                        $array_fecha = array_values($array_fecha);
                        $array_total = array_values($array_total);
                        $total_max_reserva =  max($array_total);
                        for ($j = 0; $j < sizeof($fechas); $j++)
                        {
                            if (substr_count($string_reserva_fecha, $fechas[$j]) > 0)
                            {
                                for ($p = 0; $p < sizeof($array_fecha); $p++)
                                {
                                    if ($array_fecha[$p] == $fechas[$j])
                                    {
                                        $total_reserva .= $array_total[$p] . ",";

                                    }
                                }
                            } else
                            {
                                $total_reserva .= "0" . ",";
                            }
                        }

                        $grafico_reserva .= "{
                        label : '" . $reservas_grafico[$i]["nombre"] . "' , data :[
                       " . $total_reserva . "
                     ]},";

                        $total_reserva = null;
                    }
                }


                /* ACUMULACION */

                $total_acumulacion = "";
                $grafico_acumulacion = "";
                $total_max_acumulacion = 1;


                for ($i = 0; $i < sizeof($acumulaciones_grafico); $i++)
                {
                    $contador = 1;
                    for ($j = $i + 1; $j < sizeof($acumulaciones_grafico); $j++)
                    {
                        if ($acumulaciones_grafico[$i]["nombre"] == $acumulaciones_grafico[$j]["nombre"])
                        {
                            $contador++;
                        }
                    }
                    if ($contador == "1")
                    {
                        $array_fecha = array();
                        $array_total = array();

                        for ($k = 0; $k < sizeof($acumulaciones_grafico); $k++)
                        {
                            if ($acumulaciones_grafico[$k]["nombre"] == $acumulaciones_grafico[$i]["nombre"])
                            {
                                if (in_array($acumulaciones_grafico[$k]["fecha"], $fechas))
                                {

                                    $array_fecha[$k] = $acumulaciones_grafico[$k]["fecha"];
                                    $array_total[$k] = $acumulaciones_grafico[$k]["total_acumulacion"];

                                }
                            }
                        }

                        $string_acumulacion_fecha = implode(' ', $array_fecha);
                        $array_fecha = array_values($array_fecha);
                        $array_total = array_values($array_total);
                        $total_max_acumulacion =  max($array_total);
                        for ($j = 0; $j < sizeof($fechas); $j++)
                        {
                            if (substr_count($string_acumulacion_fecha, $fechas[$j]) > 0)
                            {
                                for ($p = 0; $p < sizeof($array_fecha); $p++)
                                {
                                    if ($array_fecha[$p] == $fechas[$j])
                                    {
                                        $total_acumulacion .= $array_total[$p] . ",";

                                    }
                                }
                            } else
                            {
                                $total_acumulacion .= "0" . ",";
                            }
                        }

                        $grafico_acumulacion .= "{
                        label : '" . $acumulaciones_grafico[$i]["nombre"] . "' , data :[
                       " . $total_acumulacion . "
                     ]},";

                        $total_acumulacion = null;
                    }
                }


            $this->arrayVista['fecha'] = $fecha;
            $this->arrayVista['canjes'] = $dashboard["beneficios_listado"];
            $this->arrayVista['canjes_grafico'] = $grafico_canje;
            $this->arrayVista['canjes_total_max'] = $total_max_canje;
            $this->arrayVista['reservas'] = $dashboard["reservas_listado"];
            $this->arrayVista['reservas_grafico'] = $grafico_reserva;
            $this->arrayVista['reservas_total_max'] = $total_max_reserva;
            $this->arrayVista['acumulaciones'] = $dashboard["acumulacion_listado"];
            $this->arrayVista['acumulaciones_grafico'] = $grafico_acumulacion;
            $this->arrayVista['acumulaciones_total_max'] = $total_max_acumulacion;
            $this->arrayVista['datos_usuarios'] = $dashboard["datos_usuarios"];
            $this->arrayVista['vista'] = 'dashboard/principal/principal_view';
            $this->cargarVistaDashboard();
            } else
            {
                redirect($this->config->item('path_dashboard'));
            }

        } else
        {
            redirect(site_url($this->config->item('path_dashboard') . '/logout'));
        }

    }


}
