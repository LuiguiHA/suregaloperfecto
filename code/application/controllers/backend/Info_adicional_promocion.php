<?php
/**
 * Created by PhpStorm.
 * User: Mediabyte
 * Date: 05/05/2016
 * Time: 02:18 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Info_adicional_promocion extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
       
        $this->load->model("Informacion_adicional_promocion_model");
        $this->lang->load('backend_error_lang', 'spanish');
        $this->lang->load('backend_mensajes_lang', 'spanish');
        $this->load->helper('url');


    }


    public function listar( $start = 0)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            $id_promocion = $this->input->post('id_promocion');
            $params = array(
                "select" => "*",
                "where" => "id_promocion = '" . $id_promocion . "' and estado != '".ESTADO_ELIMINADO."'",
                "order"=> "fecha_registro desc"
            );
            $total_info_promocion = $this->Informacion_adicional_promocion_model->total_records($params);

            $info_promocion = $this->Informacion_adicional_promocion_model->search_data($params, $start, $this->elementoPorPagina);
            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Info_Adicional_Promocion'), $total_info_promocion);
            $this->arrayVista['tituloPagina'] = "Lista de Información Adicional";
            $this->arrayVista['promocion'] = $id_promocion;
            $this->arrayVista['info_promocion'] = $info_promocion;
            /* $this->arrayVista['vista'] = 'backend/foto_beneficio/listar_view';*/

            $this->cargarVistaListaAjax('backend/info_adicional_promocion/listar_view');

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function buscar($start = 0)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            $id_promocion = $this->input->post('id_promocion');
            $campo = $this->input->post('campo');
            $valor = $this->input->post('valor');
            $estado = $this->input->post('estado');
            $where = "";


            if ($campo != "") {
                $this->arrayVista['campo'] = $campo;
                if ($where != "")
                    $where .= " AND campo  LIKE '%" . $campo . "%'";

                else
                    $where .= "campo LIKE '%" . $campo . "%'";
            }

            if ($valor != "") {
                $this->arrayVista['valor'] = $valor;
                if ($where != "")
                    $where .= " AND valor LIKE '%" . $valor . "%'";
                else
                    $where .= "valor LIKE '%" . $valor . "%'";
            }


            if ($estado != "") {
                $this->arrayVista['estado'] = $estado;
                if ($where != "")
                    $where .= " AND estado = $estado";
                else
                    $where .= "estado = $estado";
            }else{
                if ($where != "")
                    $where .= " AND estado != '".ESTADO_ELIMINADO."'";
                else
                    $where .= "estado != '".ESTADO_ELIMINADO."'";
            }

            if ($where == "") {
                $params = array(
                    "select" => "*",
                    "where" => "id_promocion = '" . $id_promocion . "'",
                    "order"=> "fecha_registro desc"
                );
            } else {
                $params = array(
                    "select" => "*",
                    "where" => "id_promocion = '" . $id_promocion . "' AND " . $where,
                    "order"=> "fecha_registro desc"

                );
            }

            $total_info_promocion = $this->Informacion_adicional_promocion_model->total_records($params);

            $info_beneficio = $this->Informacion_adicional_promocion_model->search_data($params, $start, $this->elementoPorPagina);
            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Info_Adicional_Promocion/buscar'), $total_info_promocion);
            $this->arrayVista['tituloPagina'] = "Lista de Información Adicional";
            $this->arrayVista['promocion'] = $id_promocion;
            $this->arrayVista['info_promocion'] = $info_beneficio;

            $this->cargarVistaListaAjax('backend/info_adicional_promocion/listar_view');

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function agregar($id_promocion)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {
                $campo = $this->input->post("campo");
                $valor = $this->input->post("valor");
                $orden = $this->input->post("orden");
                $estado = $this->input->post("estado");

                $data_info_promocion = array(
                    'id_promocion' => $id_promocion,
                    'campo' => $campo,
                    'valor' => $valor,
                    'orden' => $orden,
                    'fecha_registro' => date("Y-m-d H:i:s"),
                    'estado' => $estado
                );

                if (isset($data_info_promocion)) {
                    try {
                        $this->Informacion_adicional_promocion_model->insert($data_info_promocion);
                        $this->alert($this->lang->line('mensaje_informacion_adicional_agregado'), site_url($this->config->item('path_backend') . '/Promocion/editar/' . $id_promocion . '/I'));
                    } catch (Exception $exception) {
                        $this->alert($this->lang->line('error_informacion_adicional_agregar'));
                    }
                }

            }


            $this->arrayVista['promocion'] = $id_promocion;
            $this->arrayVista['tituloPagina'] = "Agregar Infomación Adicional";
            $this->arrayVista['vista'] = 'backend/info_adicional_promocion/agregar_view';
            $this->cargarVistaBackend();
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }

    }

    public function editar($id_promocion, $id_info)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {
                $campo = $this->input->post("campo");
                $valor = $this->input->post("valor");
                $orden = $this->input->post("orden");
                $estado = $this->input->post("estado");

                $data_info_promccion = array(

                    'campo' => $campo,
                    'valor' => $valor,
                    'orden' => $orden,
                    'fecha_modificacion' => date("Y-m-d H:i:s"),
                    'estado' => $estado
                );

                if ($this->Informacion_adicional_promocion_model->update($id_info, $data_info_promccion)) {


                    $this->alert($this->lang->line('mensaje_informacion_adicional_editado'), site_url($this->config->item('path_backend') . '/Promocion/editar/' . $id_promocion . '/I'));

                }
            }

            $info = array(
                "select" => "*",
                "where" => "id = '" . $id_info . "'"
            );


            $info = $this->Informacion_adicional_promocion_model->get_search_row($info);
            $this->arrayVista['promocion'] = $id_promocion;
            $this->arrayVista['info'] = $info;
            $this->arrayVista['tituloPagina'] = "Editar Información Adicional";
            $this->arrayVista['vista'] = 'backend/info_adicional_promocion/editar_view';
            $this->cargarVistaBackend();


        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function eliminar(){
        if ($this->session->userdata('logged_in') === TRUE) {

            if ($this->input->post()) {
                $id_info_add = $this->input->post("id_info_add");

                $estado = ESTADO_ELIMINADO;

                $data = array(
                    'fecha_modificacion' => date("Y-m-d H:i:s"),
                    'estado' => $estado
                );

                if (  $this->Informacion_adicional_promocion_model->update($id_info_add, $data)){
                    $result = array("cod"=>"1",
                        "mensaje"=>$this->lang->line('mensaje_informacion_adicional_eliminado')
                    );
                }else{
                    $result = array("cod"=>"0",
                        "mensaje"=>$this->lang->line('error_informacion_adicional_eliminar')
                    );
                }
            }
        }else{
            $result = array("cod"=>"0",
                "mensaje"=>$this->lang->line('error_informacion_adicional_eliminar')
            );
        }
        echo json_encode($result);
    }

    public function export()
    {
        if ($this->session->userdata('logged_in') === TRUE) {

            $id_promocion = $this->input->get('id_promocion');
            $campo = $this->input->get('campo');
            $valor = $this->input->get('valor');
            $estado = $this->input->get('estado');


            $where = "";

            if ($campo != "") {
                $this->arrayVista['campo'] = $campo;
                if ($where != "")
                    $where .= " AND tbl_informacion_adicional_promocion.campo  LIKE '%" . $campo . "%'";

                else
                    $where .= "tbl_informacion_adicional_promocion.campo LIKE '%" . $campo . "%'";
            }

            if ($valor != "") {
                $this->arrayVista['valor'] = $valor;
                if ($where != "")
                    $where .= " AND tbl_informacion_adicional_promocion.valor LIKE '%" . $valor . "%'";
                else
                    $where .= "tbl_informacion_adicional_promocion.valor LIKE '%" . $valor . "%'";
            }


            if ($estado != "") {
                $this->arrayVista['estado'] = $estado;
                if ($where != "")
                    $where .= " AND tbl_informacion_adicional_promocion.estado = $estado";
                else
                    $where .= "tbl_informacion_adicional_promocion.estado = $estado";
            }else{
                if ($where != "")
                    $where .= " AND tbl_informacion_adicional_promocion.estado != '".ESTADO_ELIMINADO."'";
                else
                    $where .= "tbl_informacion_adicional_promocion.estado != '".ESTADO_ELIMINADO."'";
            }


            $join = array(
                'tbl_promocion as P, P.id = tbl_informacion_adicional_promocion.id_promocion'
            );
            if($where==""){
                $params = array(
                    "select" => "P.nombre,tbl_informacion_adicional_promocion.campo,tbl_informacion_adicional_promocion.valor,
                    tbl_informacion_adicional_promocion.orden,tbl_informacion_adicional_promocion.fecha_registro,
                    tbl_informacion_adicional_promocion.estado",
                    "join" => $join ,
                    "where" => "tbl_informacion_adicional_promocion.id_promocion = '" . $id_promocion . "'",
                    "order"=> "tbl_informacion_adicional_promocion.fecha_registro desc"
                );
            }
            else{
                $params =    array(
                    "select" => "P.nombre,tbl_informacion_adicional_promocion.campo,tbl_informacion_adicional_promocion.valor,
                    tbl_informacion_adicional_promocion.orden,tbl_informacion_adicional_promocion.fecha_registro,
                    tbl_informacion_adicional_promocion.estado",
                    "join" => $join ,
                    "where" => "tbl_informacion_adicional_promocion.id_promocion = '" . $id_promocion . "' AND ".$where,
                    "order"=> "tbl_informacion_adicional_promocion.fecha_registro desc"

                );
            }

            $informacion = $this->Informacion_adicional_promocion_model->search($params);

            
            //retrive contries table data
            $data = array();
            foreach ($informacion as $informacion) {



                if ($informacion->estado== 1){
                    $estado = "Activo";
                }else{
                    $estado = "Inactivo";
                }


                array_push(
                    $data, 
                    array(
                        utf8_decode("Promoción") => isset($informacion->nombre)?utf8_decode($informacion->nombre):"-",
                        utf8_decode("Campo") => isset($informacion->campo)?utf8_decode($informacion->campo):"-",
                        utf8_decode("Valor") => isset($informacion->valor)?utf8_decode($informacion->valor):"-",
                        utf8_decode("Orden") => isset($informacion->orden)?utf8_decode($informacion->orden):"-",
                        utf8_decode("Fecha de Registro") => isset($informacion->fecha_registro)?date("d/m/Y h:i:s a", strtotime($informacion->fecha_registro)):"-",
                        utf8_decode("Estado") => isset($estado)?$estado:"-",
                ));
            }
            //Fill data
            function filterData(&$str)
            {
                $str = preg_replace("/\t/", "\\t", $str);
                $str = preg_replace("/\r?\n/", "\\n", $str);
                if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
            }             
                
            $fileName = "Información Promoción-". date('YmdHis') . ".xls";
                
            header("Content-Disposition: attachment; filename=\"$fileName\"");
            header("Content-Type: application/vnd.ms-excel, charset=utf-8");
                
            $flag = false;

            foreach($data as $row) {
                // print_r( array_values($row))."<br>";
                if(!$flag) {
                    
                    echo implode("\t", array_keys($row)) . "\n";
                    $flag = true;
                }
                    
                array_walk($row, 'filterData');
                echo implode("\t", array_values($row)) . "\n";
            }
                
            exit;
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }
}
