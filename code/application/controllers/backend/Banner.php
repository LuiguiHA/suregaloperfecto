<?php
/**
 * Created by PhpStorm.
 * User: Mediabyte
 * Date: 10/05/2016
 * Time: 02:23 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
 
        $this->load->model("Banner_model");
        $this->load->helper('url');
        $this->lang->load('backend_error_lang', 'spanish');
        $this->lang->load('backend_mensajes_lang', 'spanish');
    }

    public function listar($start=0)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            $id_empresa =$this->input->post('id_empresa');
            $params = array(
                "select" => "*",
                "where" => "id_empresa = '" . $id_empresa . "'",
                "order"=> "fecha_registro desc"
            );
            $total_banner= $this->Banner_model->total_records($params);

            $banner = $this->Banner_model->search_data($params, $start, $this->elementoPorPagina);
            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend').'/Banner'), $total_banner);
            $this->arrayVista['tituloPagina'] = "Lista de Información Adicional";
            $this->arrayVista['empresa'] = $id_empresa;
            $this->arrayVista['banner'] = $banner;

            $this->cargarVistaListaAjax('backend/banner/listar_view');

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function buscar($start = 0)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            $id_empresa =$this->input->post('id_empresa');
            $nombre =$this->input->post('nombre');
            $tipo =$this->input->post('tipo');
            $estado = $this->input->post('estado');
            $where = "";


            if ($nombre != "") {
                $this->arrayVista['nombre'] = $nombre;
                if ($where != "")
                    $where .= " AND nombre  LIKE '%" . $nombre . "%'";

                else
                    $where .= "nombre LIKE '%" . $nombre . "%'";
            }

            if ($tipo != "") {
                $this->arrayVista['tipo'] = $tipo;
                if ($where != "")
                    $where .= " AND tipo = $tipo";
                else
                    $where .= "tipo = $tipo";
            }


            if ($estado != "") {
                $this->arrayVista['estado'] = $estado;
                if ($where != "")
                    $where .= " AND estado = $estado";
                else
                    $where .= "estado = $estado";
            }

            if($where==""){
                $params = array(
                    "select" => "*",
                    "where" => "id_empresa = '" . $id_empresa . "'",
                    "order"=> "fecha_registro desc"
                );
            }
            else{
                $params =    array(
                    "select" => "*",
                    "where" => "id_empresa = '" . $id_empresa . "' AND ".$where,
                    "order"=> "fecha_registro desc"

                );
            }

            $total_banner = $this->Banner_model->total_records($params);

            $banner= $this->Banner_model->search_data($params, $start, $this->elementoPorPagina);
            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend').'/Banner/buscar'), $total_banner);
            $this->arrayVista['tituloPagina'] = "Lista de Información Adicional";
            $this->arrayVista['empresa'] = $id_empresa;
            $this->arrayVista['banner'] = $banner;

            $this->cargarVistaListaAjax('backend/banner/listar_view');

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function agregar($id_empresa)
    {

        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {
                $recurso = $this->input->post("recurso");
                $nombre= $this->input->post("nombre");
                $descripcion = $this->input->post("descripcion");
                $enlace = $this->input->post("enlace");
                $target = $this->input->post("target");
                $tipo = $this->input->post("tipo");
                $estado = $this->input->post("estado");

                $data_banner = array(
                    'id_empresa' => $id_empresa,
                    'recurso' => $recurso,
                    'nombre' => $nombre,
                    'descripcion' => $descripcion,
                    'enlace' => $enlace,
                    'target' => $target,
                    'tipo' => $tipo,
                    'fecha_registro' => date("Y-m-d H:i:s"),
                    'estado' => $estado
                );

                if (isset($data_banner)) {
                    try {
                        $this->Banner_model->insert($data_banner);
                        $this->alert($this->lang->line('mensaje_banner_agregado'),  site_url($this->config->item('path_backend').'/Empresa/editar/'.$id_empresa.'/B'));
                    } catch (Exception $exception) {
                        $this->alert($this->lang->line('error_banner_agregar'));
                    }
                }

            }


            $this->arrayVista['empresa'] = $id_empresa;
            $this->arrayVista['tituloPagina'] = "Agregar Banner";
            $this->arrayVista['vista'] = 'backend/banner/agregar_view';
            $this->cargarVistaBackend();



        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function editar($id_empresa,$id_banner )
    {
        if ($this->session->userdata('logged_in') === TRUE) {

            if ($this->input->post()) {
                $recurso = $this->input->post("recurso");
                $nombre= $this->input->post("nombre");
                $descripcion = $this->input->post("descripcion");
                $enlace = $this->input->post("enlace");
                $target = $this->input->post("target");
                $tipo = $this->input->post("tipo");
                $estado = $this->input->post("estado");

                $data_banner = array(
                    
                    'recurso' => $recurso,
                    'nombre' => $nombre,
                    'descripcion' => $descripcion,
                    'enlace' => $enlace,
                    'target' => $target,
                    'tipo' => $tipo,
                    'fecha_modificacion' => date("Y-m-d H:i:s"),
                    'estado' => $estado
                );

                if ($this->Banner_model->update($id_banner, $data_banner)) {


                    $this->alert($this->lang->line('mensaje_banner_editado'),  site_url($this->config->item('path_backend').'/Empresa/editar/'.$id_empresa.'/B'));

                }
            }

            $banner = array(
                "select" => "*",
                "where" => "id = '" . $id_banner . "'"
            );


            $banner = $this->Banner_model->get_search_row($banner);
            $this->arrayVista['empresa'] =  $id_empresa ;
            $this->arrayVista['banner'] =  $banner ;
            $this->arrayVista['tituloPagina'] = "Editar Banner";
            $this->arrayVista['vista'] = 'backend/banner/editar_view';
            $this->cargarVistaBackend();



        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function export()
    {
        if ($this->session->userdata('logged_in') === TRUE) {

            $id_empresa = $this->input->get('id_empresa');
            $nombre = $this->input->get('nombre');
            $tipo = $this->input->get('tipo');
            $estado = $this->input->get('estado');


            $where = "";

            if ($nombre != "") {

                if ($where != "")
                    $where .= " AND tbl_banner.nombre  LIKE '%" . $nombre . "%'";

                else
                    $where .= "tbl_banner.nombre LIKE '%" . $nombre . "%'";
            }

            if ($tipo != "") {

                if ($where != "")
                    $where .= " AND tbl_banner.tipo = $tipo";
                else
                    $where .= "tbl_banner.tipo = $tipo";
            }

            if ($estado != "") {

                if ($where != "")
                    $where .= " AND tbl_banner.estado = $estado";
                else
                    $where .= "tbl_banner.estado = $estado";
            }

            $join=array(
                'tbl_empresa as E,E.id=tbl_banner.id_empresa'
            );
            if($where==""){

                $params = array(
                    "select" => "E.nombre as empresa,tbl_banner.nombre,tbl_banner.recurso,tbl_banner.descripcion,tbl_banner.enlace,
                    tbl_banner.target,tbl_banner.tipo,tbl_banner.fecha_registro,tbl_banner.estado",
                    "where" => "tbl_banner.id_empresa = '" . $id_empresa . "'",
                    "join" => $join,
                    "order"=> "tbl_banner.fecha_registro desc"
                );
            }
            else{
                $params =    array(
                    "select" => "E.nombre as empresa,tbl_banner.nombre,tbl_banner.recurso,tbl_banner.descripcion,tbl_banner.enlace,
                    tbl_banner.target,tbl_banner.tipo,tbl_banner.fecha_registro,tbl_banner.estado",
                    "where" => "tbl_banner.id_empresa = '" . $id_empresa . "' AND ".$where,
                    "join" => $join,
                    "order"=> "tbl_banner.fecha_registro desc"

                );
            }
            $banners = $this->Banner_model->search($params);

            

            //retrive contries table data
            $data = array();
            foreach ($banners as $banner) {


                if ($banner->tipo== 1){
                    $tipo= "Principal";
                }else{
                    $tipo = "Detalle";
                }

                if ($banner->estado== 1){
                    $estado = "Activo";
                }else{
                    $estado = "Inactivo";
                }


                array_push(
                    $data, 
                    array(
                        utf8_decode("Empresa") => isset($banner->empresa)?utf8_decode($banner->empresa):"-",
                        utf8_decode("Nombre") => isset($banner->nombre)?utf8_decode($banner->nombre):"-",
                        utf8_decode("Recurso") =>isset($banner->recurso)?utf8_decode($banner->recurso):"-",
                        utf8_decode("Descripción") => isset($banner->descripcion)?utf8_decode($banner->descripcion):"-",
                        utf8_decode("Enlace") => isset($banner->enlace)?utf8_decode($banner->enlace):"-",
                        utf8_decode("Target") => isset($banner->target)?utf8_decode($banner->target):"-",
                        utf8_decode("Tipo") => isset($tipo)?utf8_decode($tipo):"-",
                        utf8_decode("Fecha de Registro") => date("d/m/Y h:i:s a", strtotime($banner->fecha_registro)),
                        utf8_decode("Estado") => $estado,
                    )
                );
            }
            //Fill data
            function filterData(&$str)
            {
                $str = preg_replace("/\t/", "\\t", $str);
                $str = preg_replace("/\r?\n/", "\\n", $str);
                if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
            }             
                
            $fileName = "Banner de Empresa -". date('YmdHis') . ".xls";
                
            header("Content-Disposition: attachment; filename=\"$fileName\"");
            header("Content-Type: application/vnd.ms-excel, charset=utf-8");
                
            $flag = false;

            foreach($data as $row) {
                // print_r( array_values($row))."<br>";
                if(!$flag) {
                    
                    echo implode("\t", array_keys($row)) . "\n";
                    $flag = true;
                }
                    
                array_walk($row, 'filterData');
                echo implode("\t", array_values($row)) . "\n";
            }
                
            exit;
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }
}
