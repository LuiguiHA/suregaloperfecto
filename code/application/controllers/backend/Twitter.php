<?php
/**
 * Created by PhpStorm.
 * User: Mediabyte
 * Date: 28/04/2016
 * Time: 02:09 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Twitter extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model("Twitter_model");
        $this->load->model("Categoria_beneficio_model");
        $this->load->model("Informacion_adicional_beneficio_model");
        $this->load->model("Local_beneficio_model");
        $this->load->model("Foto_beneficio_model");
        $this->load->model("Empresa_model");
        $this->load->model("Local_model");
        $this->lang->load('backend_error_lang', 'spanish');
        $this->lang->load('backend_mensajes_lang', 'spanish');
        $this->lang->load('backend_constants_lang', 'spanish');
        $this->load->helper('utils_helper');
        $this->load->helper('url');

    }

    public function listar($start = 0)
    {

        if ($this->session->userdata('logged_in') === TRUE) {

    
            $params = array(
                "select" => "id, nombre, contenido, imagen, fecha_registro, activo",
                "order" => "fecha_registro desc"

            );
            $total_beneficios = $this->Twitter_model->total_records($params);

            $beneficio = $this->Twitter_model->search_data($params, $start, $this->elementoPorPagina);
            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Twitter'), $total_beneficios);

            $arrayMigaPan = array(
                array("nombre" => "Twitter"), array("nombre" => "Listar", 'active' => true));

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['tituloPagina'] = "Lista de Mensajes";
            $this->arrayVista['beneficios'] = $beneficio;
            $this->arrayVista['vista'] = 'backend/twitter/listar_view';
            $this->cargarVistaBackend();

         
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }
    
    public function buscar($start = 0)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            $nombre = $this->input->post('nombre');
            $estado = $this->input->post('estado');
            $where = "";


            if ($nombre != "" ) {

                $this->arrayVista['nombre'] = $nombre;
                if ($where != "") {
                    $where .= " AND vp_twitter.nombre LIKE '%" . $nombre . "%' ";
                } else {
                    $where = "vp_twitter.nombre LIKE '%" . $nombre . "%' ";
                }
            }


            
            if ($estado != "" ) {
                $this->arrayVista['estado'] = $estado;
                if ($where != "")
                    $where .= " AND vp_twitter.activo = $estado";
                else
                    $where .= "vp_twitter.activo = $estado";
            }


            $total_beneficios = $this->Twitter_model->total_records($params);

            $beneficio = $this->Twitter_model->search_data($params, $start, $this->elementoPorPagina);
            $arrayMigaPan = array(
                array("nombre" => "Twitter"), array("nombre" => "Listar", 'active' => true));

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Twitter/buscar'), $total_beneficios);
            $this->arrayVista['tituloPagina'] = "Lista de Mensajes";
            $this->arrayVista['beneficios'] = $beneficio;
            $this->arrayVista['vista'] = 'backend/twitter/listar_view';
            $this->cargarVistaBackend();


        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function agregar()
    {
        if ($this->session->userdata('logged_in') === TRUE) {
      

            if ($this->input->post()) {

                $nombre = $this->input->post("nombre");
                $contenido = $this->input->post("contenido");
                $foto = $this->input->post("foto");
                $estado = $this->input->post("activo");
              
                $pathImagen = "upload/beneficio/" . date("Y-m-d");
              
                if (!is_dir($pathImagen))
                    mkdir($pathImagen, 0775, true);
                if ($_FILES["foto"]["size"] > 0) {
                    $nombreArchivoImagen = "foto";
                    $formato = substr(strrchr($_FILES["foto"]["name"],'.'),1);
                    $nombre_imagen = pathinfo($_FILES['foto']['name'], PATHINFO_FILENAME);
                    $nombre_imagen =  $nombre_imagen."_".date("Y-m-d-h-i-s");
                    $resultadoFoto = $this->subirArchivo($nombreArchivoImagen, $pathImagen,$nombre_imagen, 'jpeg|jpg|png', 2024);
                    $foto = "";

                    if (array_key_exists("upload_data", $resultadoFoto)) {
                        if (isset($resultadoFoto["upload_data"]["file_name"]))
                            $foto = $resultadoFoto["upload_data"]["orig_name"];
                            //$foto = base_url() . "files/img/foto_beneficio/" . date("Y-m-d") . '/' . $resultadoFoto["upload_data"]["file_name"];
                    } else {
                        $this->session->set_flashdata('error', $this->lang->line('error_tamaño_imagen'));
                    }
                }
              
                $foto = $pathImagen."/".$foto;


                $data_beneficio = array(
                
                    'nombre' => $nombre,
                    'contenido' => $contenido,
                    'imagen' => $foto,
                    'fecha_registro' => date("Y-m-d H:i:s"),
                    'activo' => $estado
                );


                if (isset($data_beneficio)) {
                    try {
                        
                        $this->Twitter_model->insert($data_beneficio);
                        $this->alert($this->lang->line('mensaje_beneficio_agregado'), site_url($this->config->item('path_backend') . '/inicio'));
                      
                    } catch (Exception $exception) {
                        $this->alert($this->lang->line('error_beneficio_agregar'));
                    }
                }

            }

           

            $arrayMigaPan = array(
                array("nombre" => "Twitter", "url" => site_url($this->config->item('path_backend') . '/inicio')), array("nombre" => "Agregar", 'active' => true));

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['tituloPagina'] = "Agregar Mensaje";
            $this->arrayVista['vista'] = 'backend/twitter/agregar_view';
            $this->cargarVistaBackend();


        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function editar($id_beneficio)
    {
        
        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {

                $nombre = $this->input->post("nombre");
                $descripcion = $this->input->post("contenido");
                $estado = $this->input->post("activo");
                $foto_antiguo = $this->input->post("foto_antiguo");
              
                $pathImagen = "upload/beneficio/" . date("Y-m-d");

                if (!is_dir($pathImagen))
                    mkdir($pathImagen, 0775, true);
                if ($_FILES["foto"]["size"] > 0) {
                    $nombreArchivoImagen = "foto";
                    $formato = substr(strrchr($_FILES["foto"]["name"],'.'),1);
                    $nombre_imagen = pathinfo($_FILES['foto']['name'], PATHINFO_FILENAME);
                    $nombre_imagen =  $nombre_imagen."_".date("Y-m-d-h-i-s");

                    $resultadoFoto = $this->subirArchivo($nombreArchivoImagen, $pathImagen,$nombre_imagen ,'jpeg|jpg|png', 2024);
                    $foto = "";

                    if (array_key_exists("upload_data", $resultadoFoto)) {
                        if (isset($resultadoFoto["upload_data"]["file_name"]))
                            $foto = $resultadoFoto["upload_data"]["orig_name"];
                    } else {
                        $this->session->set_flashdata('error', $this->lang->line('error_tamaño_imagen'));
                    }
   
                    $foto = $pathImagen."/".$foto;
                   

                } else {
                    $foto = $foto_antiguo;
                }

      
                $data_beneficio = array(
                  
                    'nombre' => $nombre,
                    'contenido' => $descripcion,
                    'imagen' => $foto,
                    'activo' => $estado
                );

                if ($this->Twitter_model->update($id_beneficio, $data_beneficio)) {
                    
                    $this->alert($this->lang->line('mensaje_beneficio_editado'), site_url($this->config->item('path_backend') . '/inicio'));

                }
                $parametros = $data_beneficio;
                $accion = "editar beneficios";
                $usuario =  $this->session->userdata('correo');
                //guardar_log_cms($parametros,$accion,$usuario);
            }

            $beneficio = array(
                "select" => "*",
                "where" => "id = '" . $id_beneficio . "'"
            );
            
            $beneficio = $this->Twitter_model->get_search_row($beneficio);
            $arrayMigaPan = array(
                array("nombre" => "Twitter", "url" => site_url($this->config->item('path_backend') . '/inicio')), array("nombre" => "Editar", 'active' => true));

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['tituloPagina'] = "Editar Mensaje";
            $this->arrayVista['beneficio'] = $beneficio;
            $this->arrayVista['vista'] = 'backend/twitter/editar_view';
            $this->cargarVistaBackend();


        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function carga_imagenes()
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {

                $img = $this->input->post("img");
                $pathArchivos = "./upload/archivos/zip/beneficio/" . date("Y-m-d");
                $pathImagenes = "./upload/beneficio/" . date("Y-m-d");

                if (!is_dir($pathArchivos))
                    mkdir($pathArchivos, 0775, true);
                if ($_FILES["img"]["size"] > 0) {
                    $nombreArchivoImagenes = "img";
                    $resultadoImagenes = $this->subirArchivo($nombreArchivoImagenes, $pathArchivos,"", 'zip', 2024);
                    $img = "";

                    if (array_key_exists("upload_data", $resultadoImagenes)) {
                        if (isset($resultadoImagenes["upload_data"]["file_name"])) {
                            $zip = new ZipArchive;
                            $file = $resultadoImagenes['upload_data']['full_path'];
                            if ($zip->open($file) === TRUE) {
                                $zip->extractTo($pathImagenes);
                            }
                            $zip->close();
                        }
                    }
                }
                $this->alert($this->lang->line('mensaje_carga_fotos'), site_url($this->config->item('path_backend') . '/Beneficio/carga_masiva'));
            }
        }
    }

    public function export()
    {
        if ($this->session->userdata('logged_in') === TRUE) {

            $nombre = $this->input->get('nombre');
            $tipo = $this->input->get('tipo');
            $fecha_inicio = $this->input->get('fecha_inicio');
            $fecha_fin = $this->input->get('fecha_fin');
            $tipo = $this->input->get('tipo');
           // $empresa = $this->input->get('empresa');
            $categoria = $this->input->get('categoria');
            $estado = $this->input->get('estado');
          

            $where = "";


            if ($nombre != "" ) {

               $this->arrayVista['nombre'] = $nombre;
                if ($where != "") {
                    $where .= " AND tbl_beneficio.nombre LIKE '%" . $nombre . "%' ";
                } else {
                    $where = "tbl_beneficio.nombre LIKE '%" . $nombre . "%' ";
                }
            }


            if ($tipo != "" ) {
                $this->arrayVista['tipo'] = $tipo;
                if ($where != "")
                    $where .= " AND tbl_beneficio.tipo = $tipo";
                else
                    $where .= "tbl_beneficio.tipo = $tipo";
            }

           /* if ($empresa != "" ) {
                $this->arrayVista['empresa'] = $empresa;
                if ($where != "")
                    $where .= " AND E.id = $empresa";
                else
                    $where .= "E.id = $empresa";
            }*/

            if ($categoria != "" ) {
                $this->arrayVista['categoria'] = $categoria;
                if ($where != "")
                    $where .= " AND C.id = $categoria";
                else
                    $where .= "C.id = $categoria";
            }

            if ($fecha_inicio != "" ) {
                
                list($anio,$mes,$dia) = explode('-',$fecha_inicio);
                if ($where != "") {

                    $where .= " AND DATE(tbl_beneficio.fecha_inicio) >= '$anio-$mes-$dia'  ";
                } else {

                    $where = "DATE(tbl_beneficio.fecha_inicio)  >= '$anio-$mes-$dia' ";
                }
            }

            if ($fecha_fin != "" ) {

                list($anio,$mes,$dia) = explode('-',$fecha_fin);
                if ($where != "") {

                    $where .= " AND DATE(tbl_beneficio.fecha_fin) <= '$anio-$mes-$dia'  ";

                } else {
                    $where = "DATE(tbl_beneficio.fecha_fin)  <= '$anio-$mes-$dia' ";
                }
            }

            if ($estado != "" ) {
                $this->arrayVista['estado'] = $estado;
                if ($where != "")
                    $where .= " AND tbl_beneficio.estado = $estado";
                else
                    $where .= "tbl_beneficio.estado = $estado";
            }

            $join = array(
                'tbl_empresa as E, E.id = tbl_beneficio.id_empresa',
                'tbl_categoria_beneficio as  C, C.id = tbl_beneficio.id_beneficio_categoria'
            );

            if ($where == "") {
                $params = array(
                    "select" => "C.nombre as categoria ,E.nombre as empresa , tbl_beneficio.nombre, tbl_beneficio.descripcion,
                     tbl_beneficio.restriccion, tbl_beneficio.telefono, tbl_beneficio.correo, tbl_beneficio.direccion, tbl_beneficio.tipo,
                      tbl_beneficio.puntos, tbl_beneficio.monto_adicional_descripcion, tbl_beneficio.slug, tbl_beneficio.orden, tbl_beneficio.compartir_redes,
                       tbl_beneficio.fecha_inicio, tbl_beneficio.fecha_fin, tbl_beneficio.fecha_registro, tbl_beneficio.estado",
                    "join" => $join,
                    "order" => "tbl_beneficio.fecha_registro desc"
                );
            } else {
                $params = array(
                    "select" => "C.nombre as categoria ,E.nombre as empresa , tbl_beneficio.nombre, tbl_beneficio.descripcion,
                     tbl_beneficio.restriccion, tbl_beneficio.telefono, tbl_beneficio.correo, tbl_beneficio.direccion, tbl_beneficio.tipo,
                      tbl_beneficio.puntos, tbl_beneficio.monto_adicional_descripcion, tbl_beneficio.slug, tbl_beneficio.orden, tbl_beneficio.compartir_redes,
                       tbl_beneficio.fecha_inicio, tbl_beneficio.fecha_fin, tbl_beneficio.fecha_registro, tbl_beneficio.estado",
                    "where" => $where,
                    "join" => $join,
                    "order" => "tbl_beneficio.fecha_registro desc"
                );
            }

            $beneficios = $this->Beneficio_model->search($params);


            $data = array();
            foreach ($beneficios as $beneficio) {
                $tipo="";
                $redes="";
                $estado="";
                if ($beneficio->tipo == 1){
                    $tipo = "Canje por puntos";
                }else{
                    $tipo = "Mixto";
                }

                if ($beneficio->estado== 1){
                    $estado = "Activo";
                }else{
                    $estado = "Inactivo";
                }


                array_push($data, array(
                    utf8_decode("Categoría") => isset($beneficio->categoria)?utf8_decode($beneficio->categoria):"-",
                    utf8_decode("Empresa") => isset($beneficio->empresa)?utf8_decode($beneficio->empresa):"-",
                    utf8_decode("Nombre") => isset($beneficio->nombre)?utf8_decode($beneficio->nombre):"-",
                    utf8_decode("Descripción") => isset($beneficio->descripcion)?utf8_decode($beneficio->descripcion):"-",
                    utf8_decode("Restricción") => isset($beneficio->restriccion)?utf8_decode($beneficio->restriccion):"-",
                    utf8_decode("Teléfono") => isset($beneficio->telefono)?utf8_decode($beneficio->telefono):"-",
                    utf8_decode("Correo") => isset($beneficio->correo)?utf8_decode($beneficio->correo):"-",
                    utf8_decode("Dirección") => isset($beneficio->direccion)?utf8_decode($beneficio->direccion):"-",
                    utf8_decode("Tipo") => isset($tipo)?utf8_decode($tipo):"-",
                    utf8_decode("Puntos") => isset($beneficio->puntos)?utf8_decode($beneficio->puntos):"0",
                    utf8_decode("Monto Adicional") => isset($beneficio->monto_adicional_descripcion)?utf8_decode($beneficio->monto_adicional_descripcion):"0",
                    utf8_decode("Orden") => isset($beneficio->orden)?utf8_decode($beneficio->orden):"-",
                    utf8_decode("Fecha de Inicio") => date("d/m/Y", strtotime($beneficio->fecha_inicio)),
                    utf8_decode("Fecha de Finalización") => date("d/m/Y", strtotime($beneficio->fecha_fin)),
                    utf8_decode("Fecha de Registro") => date("d/m/Y h:i:s a", strtotime($beneficio->fecha_registro)),
                    utf8_decode("Estado") => $estado,
                ));
            }

            function filterData(&$str)
            {
                $str = preg_replace("/\t/", "\\t", $str);
                $str = preg_replace("/\r?\n/", "\\n", $str);
                if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
            }             
                
            $fileName = "beneficios-". date('YmdHis') . ".xls";
                
            header("Content-Disposition: attachment; filename=\"$fileName\"");
            header("Content-Type: application/vnd.ms-excel, charset=utf-8");
                
            $flag = false;

            foreach($data as $row) {
                // print_r( array_values($row))."<br>";
                if(!$flag) {
                    
                    echo implode("\t", array_keys($row)) . "\n";
                    $flag = true;
                }
                    
                array_walk($row, 'filterData');
                echo implode("\t", array_values($row)) . "\n";
            }
                
            exit;

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }
}
