<?php
/**
 * Created by PhpStorm.
 * User: Mediabyte
 * Date: 02/05/2016
 * Time: 03:03 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Foto_beneficio extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();


        $this->load->model("Foto_beneficio_model");
        $this->lang->load('backend_error_lang', 'spanish');
        $this->lang->load('backend_mensajes_lang', 'spanish');
        $this->load->helper('url');


    }

    public function listar( $start = 0)
    {

        if ($this->session->userdata('logged_in') === TRUE) {

            $id_beneficio = $this->input->post("id_beneficio");
            $params = array(
                "select" => "*",
                "where" => "id_beneficio = '" . $id_beneficio . "' and estado != '".ESTADO_ELIMINADO."'",
                "order"=> "fecha_registro desc"
            );
            $total_foto_beneficio = $this->Foto_beneficio_model->total_records($params);

            $foto_beneficio = $this->Foto_beneficio_model->search_data($params, $start, $this->elementoPorPagina);
            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Foto_Beneficio'), $total_foto_beneficio);

          
            $this->arrayVista['tituloPagina'] = "Lista de Fotos";
           $this->arrayVista['beneficio'] = $id_beneficio;
            $this->arrayVista['foto_beneficio'] = $foto_beneficio;

            
            $this->cargarVistaListaAjax('backend/foto_beneficio/listar_view');
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function buscar($start = 0)
    {
        if ($this->session->userdata('logged_in') === TRUE) {

            $id_beneficio = $this->input->post("id_beneficio");
            $tipo = $this->input->post('tipo');
            $plataforma = $this->input->post('plataforma');

            $estado = $this->input->post('estado');
            $where = "";


            if ($tipo != "") {
                $this->arrayVista['tipo'] = $tipo;
                if ($where != "")
                    $where .= " AND tipo = $tipo";
                else
                    $where .= "tipo = $tipo";
            }

            if ($plataforma != "") {
                $this->arrayVista['plataforma'] = $plataforma;
                if ($where != "")
                    $where .= " AND plataforma = $plataforma";
                else
                    $where .= "plataforma = $plataforma";
            }

            if ($estado != "") {
                $this->arrayVista['estado'] = $estado;
                if ($where != "")
                    $where .= " AND estado = $estado";
                else
                    $where .= "estado = $estado";
            }else{
                if ($where != "")
                    $where .= " AND estado != '".ESTADO_ELIMINADO."'";
                else
                    $where .= "estado != '".ESTADO_ELIMINADO."'";
            }

            if ($where == "") {
                $params = array(
                    "select" => "*",
                    "where" => "id_beneficio = '" . $id_beneficio . "'",
                    "order"=> "fecha_registro desc"
                );
            } else {
                $params = array(
                    "select" => "*",
                    "where" => "id_beneficio = '" . $id_beneficio . "' AND " . $where,
                    "order"=> "fecha_registro desc"

                );
            }

            $total_foto_beneficio = $this->Foto_beneficio_model->total_records($params);

            $foto_beneficio = $this->Foto_beneficio_model->search_data($params, $start, $this->elementoPorPagina);
            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Foto_Beneficio/buscar'), $total_foto_beneficio);
            $this->arrayVista['tituloPagina'] = "Lista de Fotos";
            $this->arrayVista['beneficio'] = $id_beneficio;
            $this->arrayVista['foto_beneficio'] = $foto_beneficio;

            $this->cargarVistaListaAjax('backend/foto_beneficio/listar_view');
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }

    }

    public function agregar($id_beneficio)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {


                $foto = $this->input->post("foto");
                $tipo = $this->input->post("tipo");
                $plataforma = $this->input->post("plataforma");
                $estado = $this->input->post("estado");


                $pathImagen = "upload/beneficio/" . date("Y-m-d");


                if (!is_dir($pathImagen))
                    mkdir($pathImagen, 0775, true);
                if ($_FILES["foto"]["size"] > 0) {
                    $nombreArchivoImagen = "foto";
                    $formato = substr(strrchr($_FILES["foto"]["name"],'.'),1);
                    $nombre_imagen = pathinfo($_FILES['foto']['name'], PATHINFO_FILENAME);
                    $nombre_imagen =  $nombre_imagen."_".$tipo."_".$plataforma.".".$formato;
                    $resultadoFoto = $this->subirArchivo($nombreArchivoImagen, $pathImagen,$nombre_imagen, 'jpeg|jpg|png', 2024);
                    $foto = "";

                    if (array_key_exists("upload_data", $resultadoFoto)) {
                        if (isset($resultadoFoto["upload_data"]["file_name"]))
                            $foto = $resultadoFoto["upload_data"]["orig_name"];
                            //$foto = base_url() . "files/img/foto_beneficio/" . date("Y-m-d") . '/' . $resultadoFoto["upload_data"]["file_name"];
                    } else {
                        $this->session->set_flashdata('error', $this->lang->line('error_tamaño_imagen'));
                    }
                }

                $dimension = $resultadoFoto["upload_data"]["image_width"] . ' x ' . $resultadoFoto["upload_data"]["image_height"];
                $foto = $pathImagen."/".$foto;
                $data_foto_beneficio = array(

                    'id_beneficio' => $id_beneficio,
                    'foto' => $foto,
                    'tipo' => $tipo,
                    'plataforma' => $plataforma,
                    'peso' => $resultadoFoto["upload_data"]["file_size"],
                    'dimension' => $dimension,
                    'fecha_registro' => date("Y-m-d H:i:s"),
                    'estado' => $estado
                );

                if (isset($data_foto_beneficio)) {
                    try {
                        if($estado == "1")
                        {
                            actualizar_foto($id_beneficio, $tipo, $plataforma, 2);
                        }

                        $this->Foto_beneficio_model->insert($data_foto_beneficio);
                        $this->alert($this->lang->line('mensaje_foto_agregado'), site_url($this->config->item('path_backend') . '/Beneficio/editar/' . $id_beneficio . '/F'));

                    } catch (Exception $exception) {
                        $this->alert($this->lang->line('error_foto_agregar'));
                    }
                }

            }


            $this->arrayVista['beneficio'] = $id_beneficio;
            $this->arrayVista['tituloPagina'] = "Agregar Foto ";

            $this->arrayVista['vista'] = 'backend/foto_beneficio/agregar_view';
            $this->cargarVistaBackend();

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function editar($id_beneficio, $id_foto)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {
                $foto = $this->input->post("foto");
                $foto_antiguo = $this->input->post("foto_antiguo");
                $tipo = $this->input->post("tipo");
                $plataforma = $this->input->post("plataforma");
                $estado = $this->input->post("estado");


                $pathImagen = "upload/beneficio/" . date("Y-m-d");

                if (!is_dir($pathImagen))
                    mkdir($pathImagen, 0775, true);
                if ($_FILES["foto"]["size"] > 0) {
                    $nombreArchivoImagen = "foto";
                    $formato = substr(strrchr($_FILES["foto"]["name"],'.'),1);
                    $nombre_imagen = pathinfo($_FILES['foto']['name'], PATHINFO_FILENAME);
                    $nombre_imagen =  $nombre_imagen."_".$tipo."_".$plataforma.".".$formato;

                    $resultadoFoto = $this->subirArchivo($nombreArchivoImagen, $pathImagen,$nombre_imagen ,'jpeg|jpg|png', 2024);
                    $foto = "";

                    if (array_key_exists("upload_data", $resultadoFoto)) {
                        if (isset($resultadoFoto["upload_data"]["file_name"]))


                           // $foto = base_url() . "files/img/foto_beneficio/" . date("Y-m-d") . '/' . $resultadoFoto["upload_data"]["file_name"];
                            $foto = $resultadoFoto["upload_data"]["orig_name"];
                    } else {
                        $this->session->set_flashdata('error', $this->lang->line('error_tamaño_imagen'));
                    }
                    $dimension = $resultadoFoto["upload_data"]["image_width"] . ' x ' . $resultadoFoto["upload_data"]["image_height"];
                    $foto = $pathImagen."/".$foto;
                    $data_foto_beneficio = array(
                        'foto' => $foto,
                        'tipo' => $tipo,
                        'plataforma' => $plataforma,
                        'peso' => $resultadoFoto["upload_data"]["file_size"],
                        'dimension' => $dimension,
                        'fecha_modificacion' => date("Y-m-d H:i:s"),
                        'estado' => $estado
                    );


                } else {
                    $foto = $foto_antiguo;

                    $data_foto_beneficio = array(
                        'foto' => $foto,
                        'tipo' => $tipo,
                        'plataforma' => $plataforma,
                        'fecha_modificacion' => date("Y-m-d H:i:s"),
                        'estado' => $estado
                    );

                }

                if($estado == "1" ){
                   actualizar_foto($id_beneficio, $tipo , $plataforma, 2);
                }

                if ($this->Foto_beneficio_model->update($id_foto, $data_foto_beneficio)) {
                    $this->alert($this->lang->line('mensaje_foto_editado'), site_url($this->config->item('path_backend') . '/Beneficio/editar/' . $id_beneficio . '/F'));
                }
            }

            $foto = array(
                "select" => "*",
                "where" => "id = '" . $id_foto . "'"
            );


            $foto = $this->Foto_beneficio_model->get_search_row($foto);
        
            $this->arrayVista['tituloPagina'] = "Editar Foto";
            $this->arrayVista['beneficio'] = $id_beneficio;
           // $this->arrayVista['fecha'] = $fecha_foto;
            $this->arrayVista['foto'] = $foto;
            $this->arrayVista['vista'] = 'backend/foto_beneficio/editar_view';
            $this->cargarVistaBackend();

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function eliminar(){
        if ($this->session->userdata('logged_in') === TRUE) {

            if ($this->input->post()) {
                $id_foto = $this->input->post("id_foto");

                $estado = ESTADO_ELIMINADO;

                $data_info_beneficio = array(
                    'fecha_modificacion' => date("Y-m-d H:i:s"),
                    'estado' => $estado
                );

                if (  $this->Foto_beneficio_model->update($id_foto, $data_info_beneficio)){
                    $result = array("cod"=>"1",
                        "mensaje"=>$this->lang->line('mensaje_foto_eliminado')
                    );
                }else{
                    $result = array("cod"=>"0",
                        "mensaje"=>$this->lang->line('error_foto_eliminar')
                    );
                }
            }
        }else{
            $result = array("cod"=>"0",
                "mensaje"=>$this->lang->line('error_foto_eliminar')
            );
        }
        echo json_encode($result);
    }

    public function export()
    {
        if ($this->session->userdata('logged_in') === TRUE) {

            $id_beneficio = $this->input->get('id_beneficio');
            $tipo = $this->input->get('tipo');
            $plataforma = $this->input->get('plataforma');
            $estado = $this->input->get('estado');


            $where = "";


            if ($tipo != "") {

                if ($where != "")
                    $where .= " AND tbl_foto_beneficio.tipo = $tipo";
                else
                    $where .= "tbl_foto_beneficio.tipo = $tipo";
            }

            if ($plataforma != "") {

                if ($where != "")
                    $where .= " AND tbl_foto_beneficio.plataforma = $plataforma";
                else
                    $where .= "tbl_foto_beneficio.plataforma = $plataforma";
            }



            if ($estado != "") {

                if ($where != "")
                    $where .= " AND tbl_foto_beneficio.estado = $estado";
                else
                    $where .= "tbl_foto_beneficio.estado = $estado";
            }else{
                if ($where != "")
                    $where .= " AND tbl_foto_beneficio.estado != '".ESTADO_ELIMINADO."'";
                else
                    $where .= "tbl_foto_beneficio.estado != '".ESTADO_ELIMINADO."'";
            }


            $join = array(
                'tbl_beneficio as B, B.id = tbl_foto_beneficio.id_beneficio'
            );
            if($where == ""){
                $params = array(
                    "select" => "B.nombre,tbl_foto_beneficio.foto,tbl_foto_beneficio.tipo,tbl_foto_beneficio.plataforma,
                    tbl_foto_beneficio.peso,tbl_foto_beneficio.dimension,tbl_foto_beneficio.fecha_registro,
                    tbl_foto_beneficio.estado",
                    "join" => $join ,
                    "where" => "tbl_foto_beneficio.id_beneficio = '" . $id_beneficio . "'",
                    "order"=> "tbl_foto_beneficio.fecha_registro desc"
                );
            }
            else{
                $params =    array(
                    "select" => "B.nombre,tbl_foto_beneficio.foto,tbl_foto_beneficio.tipo,tbl_foto_beneficio.plataforma,
                    tbl_foto_beneficio.peso,tbl_foto_beneficio.dimension,tbl_foto_beneficio.fecha_registro,
                    tbl_foto_beneficio.estado",
                    "join" => $join ,
                    "where" => "tbl_foto_beneficio.id_beneficio = '" . $id_beneficio . "' AND ".$where,
                    "order"=> "tbl_foto_beneficio.fecha_registro desc"

                );
            }

            $foto= $this->Foto_beneficio_model->search($params);

            //retrive contries table data
            $data = array();
            foreach ($foto as $foto) {



                if ($foto->estado== 1){
                    $estado = "Activo";
                }else{
                    $estado = "Inactivo";
                }

                if ($foto->tipo== 1){
                    $tipo = "Principal";
                }else{
                    $tipo = "Detalle";
                }

                if ($foto->plataforma == 1){
                    $plataforma = "Web";
                }else{
                    $plataforma = "Móvil";
                }


                array_push($data, array(
                    utf8_decode("Nombre") => isset($foto->foto)?base_url().substr($foto->foto,2):"-",
                    utf8_decode("Beneficio") => isset($foto->nombre)?utf8_decode($foto->nombre):"-",
                    utf8_decode("Tipo") => isset($tipo)?utf8_decode($tipo):"-",
                    utf8_decode("Plataforma") => isset($plataforma)?utf8_decode($plataforma):"-",
                    utf8_decode("Peso") => isset($foto->peso)?utf8_decode($foto->peso):"-",
                    utf8_decode("Dimensión") => isset($foto->dimension)?utf8_decode($foto->dimension):"-",
                    utf8_decode("Fecha de Registro") => isset($foto->fecha_registro)?date("d/m/Y h:i:s a", strtotime($foto->fecha_registro)):"-",
                    utf8_decode("Estado") => $estado,
                ));
            }
            function filterData(&$str)
            {
                $str = preg_replace("/\t/", "\\t", $str);
                $str = preg_replace("/\r?\n/", "\\n", $str);
                if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
            }             
                
            $fileName = "Foto_beneficios-". date('YmdHis') . ".xls";
                
            header("Content-Disposition: attachment; filename=\"$fileName\"");
            header("Content-Type: application/vnd.ms-excel, charset=utf-8");
                
            $flag = false;

            foreach($data as $row) {
                // print_r( array_values($row))."<br>";
                if(!$flag) {
                    
                    echo implode("\t", array_keys($row)) . "\n";
                    $flag = true;
                }
                    
                array_walk($row, 'filterData');
                echo implode("\t", array_values($row)) . "\n";
            }
                
            exit;
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function actualizar_foto($id_beneficio, $tipo)
    {

        $parametro_foto = array(
            "estado" => "0"
        );

        $where = "tbl_foto_promocion.id_beneficio = $id_beneficio
        and tbl_foto_promocion.tipo = $tipo";

        $filas_afectadas = $this->Foto_promocion_model->update_where($where, $parametro_foto);


        return $filas_afectadas;
    }
}
