<?php
/**
 * Created by PhpStorm.
 * User: Mediabyte
 * Date: 11/05/2016
 * Time: 12:08 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoria_carta extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();


        $this->load->model("Categoria_carta_model");
        $this->load->model("Carta_model");
        $this->load->model("Empresa_model");
        $this->load->helper('url');
        $this->lang->load('backend_error_lang', 'spanish');
        $this->lang->load('backend_mensajes_lang', 'spanish');


    }

    public function listar($start = 0)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            $params = array(
                "select" => "*",
                "order"=> "fecha_registro desc"
            );
            $total_categoria_carta = $this->Categoria_carta_model->total_records($params);

            $categoria_carta = $this->Categoria_carta_model->search_data($params, $start, $this->elementoPorPagina);
            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Categoria_Carta'), $total_categoria_carta);


            $arrayMigaPan = array(
                array("nombre" => "Carta" , "url" => site_url($this->config->item('path_backend') . '/Carta')),
                array("nombre" => "Categoría"),
                array("nombre" => "Listar", 'active' => true));

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['tituloPagina'] = "Lista de Categorías";
            $this->arrayVista['categoria_carta'] = $categoria_carta;
            $this->arrayVista['vista'] = 'backend/categoria_carta/listar_view';
            $this->cargarVistaBackend();
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function buscar($start = 0)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            $nombre = $this->input->post('nombre');

            $estado = $this->input->post('estado');
            $where = "";


            if ($nombre != "") {
                $this->arrayVista['nombre'] = $nombre;
                if ($where != "") {
                    $where .= " AND nombre LIKE '%" . $nombre . "%' ";
                } else {
                    $where = "nombre LIKE '%" . $nombre . "%' ";
                }
            }


            if ($estado != "") {
                $this->arrayVista['estado'] = $estado;
                if ($where != "")
                    $where .= " AND estado = $estado";
                else
                    $where .= "estado = $estado";
            }

            if ($where == "") {
                $params = array(
                    "select" => "*",
                    "order"=> "fecha_registro desc"
                );
            } else {
                $params = array(
                    "select" => "*",
                    "where" => $where,
                    "order"=> "fecha_registro desc"
                );
            }

            $total_categoria_carta = $this->Categoria_carta_model->total_records($params);

            $categoria_carta = $this->Categoria_carta_model->search_data($params, $start, $this->elementoPorPagina);
            $arrayMigaPan = array(
                array("nombre" => "Carta" , "url" => site_url($this->config->item('path_backend') . '/Carta/listar')),
                array("nombre" => "Categoría"),
                array("nombre" => "Listar", 'active' => true));

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Categoria_Carta/buscar'), $total_categoria_carta);
            $this->arrayVista['tituloPagina'] = "Lista de Categorías";
            $this->arrayVista['categoria_carta'] = $categoria_carta;
            $this->arrayVista['vista'] = 'backend/categoria_carta/listar_view';
            $this->cargarVistaBackend();

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function agregar()
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {

                $empresa = $this->input->post("empresa");
                $nombre = $this->input->post("nombre");
                $descripcion = $this->input->post("descripcion");

              //  $foto = $this->input->post("foto");

                $estado = $this->input->post("estado");


             //   $pathImagen = "./upload/categoria_carta/" . date("Y-m-d");

/*
                if (!is_dir($pathImagen))
                    mkdir($pathImagen, 0775, true);
                if ($_FILES["foto"]["size"] > 0) {
                    $nombreArchivoImagen = "foto";
                    $resultadoFoto = $this->subirArchivo($nombreArchivoImagen, $pathImagen, 'jpeg|jpg|png', 2024);
                    $foto = "";
                    if (array_key_exists("upload_data", $resultadoFoto)) {
                        if (isset($resultadoFoto["upload_data"]["file_name"]))

                            $foto = $resultadoFoto["upload_data"]["orig_name"];
                         //  $foto = base_url() . "files/img/categoria_carta/" . date("Y-m-d") . '/' . $resultadoFoto["upload_data"]["file_name"];
                    } else {
                        $this->session->set_flashdata('error', $this->lang->line('error_tamaño_imagen'));
                    }
                }

                $foto = $pathImagen."/".$foto;*/
                $data_categoria_carta = array(
                    'id_empresa' => $empresa,
                    'nombre' => $nombre,
                    'descripcion' => $descripcion,
                   // 'foto' => $foto,
                    'fecha_registro' => date("Y-m-d H:i:s"),
                    'estado' => $estado
                );

                if (isset($data_categoria_carta)) {
                    try {
                        $this->Categoria_carta_model->insert($data_categoria_carta);
                        $this->alert($this->lang->line('mensaje_categoria_agregado'), site_url($this->config->item('path_backend') . '/Categoria_Carta'));
                    } catch (Exception $exception) {
                        $this->alert($this->lang->line('mensaje_categoria_agregado'));
                    }
                }

            }


            $empresas = array(
                "select" => "*", "where" => "estado = 1"
            );
            $empresa = $this->Empresa_model->search($empresas);

            $arrayMigaPan = array(
                array("nombre" => "Carta" , "url" => site_url($this->config->item('path_backend') . '/Carta')),
                array("nombre" => "Categoría" ,"url" => site_url($this->config->item('path_backend') . '/Categoria_Carta')),
                array("nombre" => "Agregar", 'active' => true));

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['empresas'] = $empresa;
            $this->arrayVista['tituloPagina'] = "Agregar Categoría";

            $this->arrayVista['vista'] = 'backend/categoria_carta/agregar_view';
            $this->cargarVistaBackend();

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function editar($id_categoria)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {
                $empresa = $this->input->post("empresa");
                $nombre = $this->input->post("nombre");
                $descripcion = $this->input->post("descripcion");

              //  $foto = $this->input->post("foto");
             //   $foto_antiguo = $this->input->post("foto_antiguo");
                $estado = $this->input->post("estado");
             /*   $pathImagen = "./upload/categoria_carta/" . date("Y-m-d");


                if (!is_dir($pathImagen))
                    mkdir($pathImagen, 0775, true);
                if ($_FILES["foto"]["size"] > 0) {
                    $nombreArchivoImagen = "foto";
                    $resultadoFoto = $this->subirArchivo($nombreArchivoImagen, $pathImagen, 'jpeg|jpg|png', 2024);
                    $foto = "";
                    if (array_key_exists("upload_data", $resultadoFoto)) {
                        if (isset($resultadoFoto["upload_data"]["file_name"]))

                            $foto = $resultadoFoto["upload_data"]["orig_name"];
                           // $foto = $pathImagen."/".$foto;
                          //  $foto = base_url() . "files/img/categoria_carta/" . date("Y-m-d") . '/' . $resultadoFoto["upload_data"]["file_name"];
                    } else {
                        $this->session->set_flashdata('error', $this->lang->line('error_tamaño_imagen'));
                    }
                    $foto = $pathImagen."/".$foto;

                }else{
                    $foto= $foto_antiguo;
                }*/

                $data_categoria_carta = array(

                    'nombre' => $nombre,
                    'descripcion' => $descripcion,
                  //  'foto' => $foto,
                    'fecha_modificacion' => date("Y-m-d H:i:s"),
                    'estado' => $estado
                );



                if ($this->Categoria_carta_model->update($id_categoria, $data_categoria_carta)) {


                    $this->alert($this->lang->line('mensaje_categoria_editado'), site_url($this->config->item('path_backend') . '/Categoria_Carta'));

                }
            }

            $categoria = array(
                "select" => "*",
                "where" => "id = '" . $id_categoria . "'"
            );
            $empresas = array(
                "select" => "*", "where" => "estado = 1"
            );
            $empresa = $this->Empresa_model->search($empresas);


            $categoria = $this->Categoria_carta_model->get_search_row($categoria);

            $arrayMigaPan = array(
                array("nombre" => "Carta" , "url" => site_url($this->config->item('path_backend') . '/Carta')),
                array("nombre" => "Categoría" ,"url" => site_url($this->config->item('path_backend') . '/Categoria_Carta')),
                array("nombre" => "Editar", 'active' => true));
            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['tituloPagina'] = "Editar Categoría";
          //  $this->arrayVista['fecha'] = $fecha_foto;
            $this->arrayVista['empresas'] = $empresa;
            $this->arrayVista['categoria'] = $categoria;
            $this->arrayVista['vista'] = 'backend/categoria_carta/editar_view';
            $this->cargarVistaBackend();

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function export()
    {
        if ($this->session->userdata('logged_in') === TRUE) {

            $nombre = $this->input->get('nombre');
            $estado = $this->input->get('estado');


            $where = "";


            if ($nombre != "" ) {

                $this->arrayVista['nombre'] = $nombre;
                if ($where != "") {
                    $where .= " AND nombre LIKE '%" . $nombre . "%' ";
                } else {
                    $where = "nombre LIKE '%" . $nombre . "%' ";
                }
            }



            if ($estado != "" ) {
                $this->arrayVista['estado'] = $estado;
                if ($where != "")
                    $where .= " AND estado = $estado";
                else
                    $where .= "estado = $estado";
            }

            if ($where == "") {
                $params = array(
                    "select" => "*",
                    "order" => "fecha_registro desc"
                );
            } else {
                $params = array(
                    "select" => "*",
                    "where" => $where,
                    "order" => "fecha_registro desc"
                );
            }

            $categorias_carta= $this->Categoria_carta_model->search($params);

        
            //retrive contries table data
            $data = array();
            foreach ($categorias_carta as $categoria) {



                if ($categoria->estado== 1){
                    $estado = "Activo";
                }else{
                    $estado = "Inactivo";
                }

                
                array_push(
                    $data, 
                    array(
                        utf8_decode("Nombre") => isset($categoria->nombre)?utf8_decode($categoria->nombre):"-",
                        utf8_decode("Descripción") => isset($categoria->descripcion)?utf8_decode($categoria->descripcion):"-",
                      //  utf8_decode("Foto") =>  isset($categoria->foto)?substr(base_url(),0,-2)  .substr($categoria->foto,1):"-",
                        utf8_decode("Fecha de Registro") => date("d/m/Y h:i:s a", strtotime($categoria->fecha_registro)),
                        utf8_decode("Estado") => $estado,
                    )
                );
            }
            //Fill data
             function filterData(&$str)
            {
                $str = preg_replace("/\t/", "\\t", $str);
                $str = preg_replace("/\r?\n/", "\\n", $str);
                if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
            }             
                
            $fileName = "Categorías de carta -". date('YmdHis') . ".xls";
                
            header("Content-Disposition: attachment; filename=\"$fileName\"");
            header("Content-Type: application/vnd.ms-excel, charset=utf-8");
                
            $flag = false;

            foreach($data as $row) {
                // print_r( array_values($row))."<br>";
                if(!$flag) {
                    
                    echo implode("\t", array_keys($row)) . "\n";
                    $flag = true;
                }
                    
                array_walk($row, 'filterData');
                echo implode("\t", array_values($row)) . "\n";
            }
                
            exit;
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }
}
