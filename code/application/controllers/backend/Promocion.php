<?php
/**
 * Created by PhpStorm.
 * User: Mediabyte
 * Date: 28/04/2016
 * Time: 02:09 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Promocion extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
  
        $this->load->model("Promocion_model");
        $this->load->model("Foto_promocion_model");
        $this->load->model("Local_promocion_model");
        $this->load->model("Informacion_adicional_promocion_model");
        $this->load->model("Categoria_model");
        $this->load->model("Local_model");
        $this->load->model("Empresa_model");
        $this->lang->load('backend_error_lang', 'spanish');
        $this->lang->load('backend_mensajes_lang', 'spanish');
        $this->load->helper('url');


    }

    public function listar($start = 0)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            $params = array(
                "select" => "tbl_promocion.id,tbl_promocion.nombre,tbl_promocion.descuento,tbl_promocion.fecha_inicio,
                    tbl_promocion.fecha_fin,tbl_promocion.estado,categoria.nombre as nombre_categoria",
                "join" => array("tbl_categoria as categoria, categoria.id = tbl_promocion.id_categoria"),
                "order"=> "tbl_promocion.fecha_registro desc"
            );

            $total_promociones = $this->Promocion_model->total_records($params);

            $promocion = $this->Promocion_model->search_data($params, $start, $this->elementoPorPagina);
            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Promocion'), $total_promociones);


            $arrayMigaPan = array(
                array("nombre" => "Promocion"), array("nombre" => "Listar", 'active' => true));

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;

            $empresas = array(
                "select" => "id,nombre",
                "where"=> "estado =" .ESTADO_ACTIVO
            );

            $empresa = $this->Empresa_model->search($empresas);

            $categorias = array(
                "select" => "id,nombre",
                "where"=> "estado =" .ESTADO_ACTIVO
            );

            $categoria = $this->Categoria_model->search($categorias);



            $this->arrayVista['tituloPagina'] = "Lista de Promociones";
            $this->arrayVista['empresas'] = $empresa;
            $this->arrayVista['categorias'] = $categoria;
            $this->arrayVista['promociones'] = $promocion;
            $this->arrayVista['vista'] = 'backend/promocion/listar_view';
            $this->cargarVistaBackend();

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }
    
    public function buscar($start = 0)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            $nombre = $this->input->post('nombre');
            $categoria = $this->input->post('categoria');
            $empresa = $this->input->post('empresa');
            $fecha_inicio = $this->input->post('fecha_inicio');
            $fecha_fin = $this->input->post('fecha_fin');
            $estado = $this->input->post('estado');
            $where = "";


            if ($nombre != "") {
                $this->arrayVista['nombre'] = $nombre;
                if ($where != "") {
                    $where .= " AND tbl_promocion.nombre LIKE '%" . $nombre . "%' ";
                } else {
                    $where = "tbl_promocion.nombre LIKE '%" . $nombre . "%' ";
                }
            }

            if ($fecha_inicio != "" ) {

                $this->arrayVista['fecha_inicio'] = $fecha_inicio;
                list($anio,$mes,$dia) = explode('-',$fecha_inicio);
                if ($where != "") {

                    $where .= " AND DATE(tbl_promocion.fecha_inicio) >= '$anio-$mes-$dia'  ";
                } else {

                    $where = "DATE(tbl_promocion.fecha_inicio)  >= '$anio-$mes-$dia' ";
                }
            }

            if ($fecha_fin != "" ) {

                $this->arrayVista['fecha_fin'] = $fecha_fin;
                list($anio,$mes,$dia) = explode('-',$fecha_fin);
                if ($where != "") {

                    $where .= " AND DATE(tbl_promocion.fecha_fin) <= '$anio-$mes-$dia'  ";

                } else {
                    $where = "DATE(tbl_promocion.fecha_fin)  <= '$anio-$mes-$dia' ";
                }
            }


            if ($empresa != "") {
                $this->arrayVista['empresa'] = $empresa;
                if ($where != "")
                    $where .= " AND tbl_promocion.id_empresa = $empresa";
                else
                    $where .= "tbl_promocion.id_empresa = $empresa";
            }

            if ($categoria != "") {
                $this->arrayVista['categoria_select'] = $categoria;
                if ($where != "")
                    $where .= " AND tbl_promocion.id_categoria = $categoria";
                else
                    $where .= "tbl_promocion.id_categoria = $categoria";
            }

            if ($estado != "") {
                $this->arrayVista['estado'] = $estado;
                if ($where != "")
                    $where .= " AND tbl_promocion.estado = $estado";
                else
                    $where .= "tbl_promocion.estado = $estado";
            }

            if ($where == "") {
                $params = array(
                    "select" => "tbl_promocion.id,tbl_promocion.nombre,tbl_promocion.descuento,tbl_promocion.fecha_inicio,
                    tbl_promocion.fecha_fin,tbl_promocion.estado,categoria.nombre as nombre_categoria",
                    "join" => array("tbl_categoria as categoria, categoria.id = tbl_promocion.id_categoria"),
                    "order"=> "tbl_promocion.fecha_registro desc"
                );
            } else {
                $params = array(
                    "select" => "tbl_promocion.id,tbl_promocion.nombre,tbl_promocion.descuento,tbl_promocion.fecha_inicio,
                    tbl_promocion.fecha_fin,tbl_promocion.estado,categoria.nombre as nombre_categoria",
                    "join" => array("tbl_categoria as categoria, categoria.id = tbl_promocion.id_categoria"),
                    "where" => $where,
                    "order"=> "tbl_promocion.fecha_registro desc"


                );
            }

            $total_promociones = $this->Promocion_model->total_records($params);
            $empresas = array(
                "select" => "id,nombre",
                "where"=> "estado =" .ESTADO_ACTIVO
            );

            $categorias = array(
                "select" => "id,nombre",
                "where"=> "estado =" .ESTADO_ACTIVO
            );

            $categoria = $this->Categoria_model->search($categorias);

            $arrayMigaPan = array(
                array("nombre" => "Promocion"), array("nombre" => "Listar", 'active' => true));

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $promocion = $this->Promocion_model->search_data($params, $start, $this->elementoPorPagina);


            $empresa = $this->Empresa_model->search($empresas);

            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Promocion/buscar'), $total_promociones);
            $this->arrayVista['tituloPagina'] = "Lista de Promociones";
            $this->arrayVista['empresas'] = $empresa;
            $this->arrayVista['categorias'] = $categoria;
            $this->arrayVista['promociones'] = $promocion;
            $this->arrayVista['vista'] = 'backend/promocion/listar_view';
            $this->cargarVistaBackend();


        } else {
            redirect(site_url($this->config->item('path_backend')));
        }

    }

    public function agregar()
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            $empresas = array(
                "select" => "*", "where" => "estado = 1"
            );

            $empresa = $this->Empresa_model->search($empresas);

            if ($this->input->post()) {
                $id_categoria = $this->input->post("categoria");
                $id_empresa = $this->input->post("empresa");
                $tipo = $this->input->post("tipo");
                $nombre = $this->input->post("nombre");
                $descripcion = $this->input->post("descripcion");
                $descuento = $this->input->post("descuento");
                $restriccion = $this->input->post("restriccion");

                $tipo_locales = $this->input->post("tipo_locales");
                $locales = $this->input->post("locales");
                $direccion = $this->input->post("direccion");
                $telefono = $this->input->post("telefono");
                $correo = $this->input->post("correo");
                $latitud = $this->input->post("latitud");
                $longitud = $this->input->post("longitud");

                $orden = $this->input->post("orden");
                $slug = $this->input->post("slug");
                $compartir = $this->input->post("compartir");
                $fecha_inicio = $this->input->post("fecha_inicio");
                $fecha_fin = $this->input->post("fecha_fin");
                $estado = $this->input->post("estado");

                if ($empresa != null && sizeof($empresa) == 1){
                    $id_empresa =  $empresa[0]->id;
                }

                if ($tipo_locales != STATUS_DELIVERY){
                    $direccion = "";
                    $telefono = "";
                    $correo = "";
                    $latitud = "";
                    $longitud = "";
                }



                $data_promocion = array(
                    'id_categoria' => $id_categoria,
                    'id_empresa' => $id_empresa,
                    'tipo' => $tipo,
                    'nombre' => $nombre,
                    'descripcion' => $descripcion,
                    'restriccion' => $restriccion,
                    'descuento' => $descuento,
                    'slug' => $slug,
                    'orden' => $orden,
                    'compartir_redes' => $compartir,

                    'tipo_locales' => $tipo_locales,
                    'telefono' => $telefono,
                    'correo' => $correo,
                    'direccion' => $direccion,
                    'latitud'=>$latitud,
                    'longitud'=>$longitud,

                    'fecha_registro' => date("Y-m-d H:i:s"),
                    'fecha_inicio' => $fecha_inicio,
                    'fecha_fin' => $fecha_fin,
                    'estado' => $estado
                );


                if (isset($data_promocion)) {
                    try {

                        if ($tipo_locales == STATUS_ALL_LOCALS){

                            $locales_empresa= array(
                                "select" => "*", "where" => "estado = 1 and id_empresa = $id_empresa"
                            );
                            $locales_promocion = $this->Local_model->search($locales_empresa);

                            if ($locales_promocion != null && sizeof($locales_promocion) > 0){
                                $id_promocion =  $this->Promocion_model->insert($data_promocion);
                                $locales_array = array();
                                for($i=0 ;  $i<sizeof($locales_promocion) ; $i++){
                                    $local = array(
                                        'id_promocion' => $id_promocion,
                                        'id_local' => $locales_promocion[$i]->id,
                                        'fecha_registro' => date("Y-m-d H:i:s"),
                                        'estado' => ESTADO_ACTIVO);
                                    $locales_array[$i]= $local;
                                }
                                $this->Local_promocion_model->insert_lote($locales_array);
                            }else{
                                $this->alert($this->lang->line('error_local_empresa_empty'),site_url($this->config->item('path_backend') . '/beneficio/agregar'));
                            }
                        }elseif ($tipo_locales == STATUS_SOME_LOCALS){
                            $id_promocion =  $this->Promocion_model->insert($data_promocion);
                            $locales_array = array();
                            for($i = 0 ;  $i < sizeof($locales) ; $i++){
                                $local = array(
                                    'id_promocion' => $id_promocion,
                                    'id_local' => $locales[$i],
                                    'fecha_registro' => date("Y-m-d H:i:s"),
                                    'estado' => ESTADO_ACTIVO);
                                $locales_array[$i]= $local;
                            }
                            $this->Local_promocion_model->insert_lote($locales_array);
                        }else{
                            $this->Promocion_model->insert($data_promocion);
                        }

                        $this->alert($this->lang->line('mensaje_promocion_agregado'), site_url($this->config->item('path_backend') . '/Promocion'));
                    } catch (Exception $exception) {
                        $this->alert($this->lang->line('error_promocion_agregar'));
                    }
                }

            }

            $categorias = array(
                "select" => "*", "where" => "estado = 1"
            );


            $categoria = $this->Categoria_model->search($categorias);

            $arrayMigaPan = array(
                array("nombre" => "Promocion" ,"url"=>site_url($this->config->item('path_backend') . '/Promocion') ), array("nombre" => "Agregar", 'active' => true));

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['tituloPagina'] = "Agregar Promoción";
            $this->arrayVista['empresas'] = $empresa;
            $this->arrayVista['categorias'] = $categoria;
            $this->arrayVista['vista'] = 'backend/promocion/agregar_view';
            $this->cargarVistaBackend();

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }

    }

    public function editar($id_promocion)
    {
        if ($this->session->userdata('logged_in') === TRUE) {

            $empresas = array(
                "select" => "*", "where" => "estado = 1"
            );

            $empresa = $this->Empresa_model->search($empresas);

            if ($this->input->post()) {
                $id_categoria = $this->input->post("categoria");
                $id_empresa = $this->input->post("empresa");
                $tipo = $this->input->post("tipo");
                $nombre = $this->input->post("nombre");
                $descripcion = $this->input->post("descripcion");
                $descuento = $this->input->post("descuento");
                $restriccion = $this->input->post("restriccion");
                $orden = $this->input->post("orden");
                $slug = $this->input->post("slug");
                $locales = $this->input->post("locales");
                $compartir = $this->input->post("compartir");
                $fecha_inicio = $this->input->post("fecha_inicio");
                $fecha_fin = $this->input->post("fecha_fin");
                $estado = $this->input->post("estado");


                if ($empresa != null && sizeof($empresa) == 1){
                    $id_empresa =  $empresa[0]->id;
                }


                if ($locales == "2" || $locales == "1"){

                    $locales_promocion= array(
                        "select" => "*", "where" => "id_promocion = ".$id_promocion
                    );
                    $locales_promocion = $this->Local_promocion_model->search($locales_promocion);
                    if ($locales_promocion != null && sizeof($locales_promocion) > 0){
                        for($i=0 ;  $i<sizeof($locales_promocion) ; $i++){
                            $where = $locales_promocion[$i]->id;
                            $data = array(
                                'fecha_modificacion' => date("Y-m-d H:i:s"),
                                'estado' => ESTADO_ELIMINADO);
                            $this->Local_promocion_model->update($where,$data);
                        }
                    }
                }

                $data_promocion = array(
                    'id_categoria' => $id_categoria,
                    'id_empresa' => $id_empresa,
                    'tipo' => $tipo,
                    'nombre' => $nombre,
                    'descripcion' => $descripcion,
                    'restriccion' => $restriccion,
                    'descuento' => $descuento,
                    'slug' => $slug,
                    'orden' => $orden,
                    //'locales'=>$locales,
                    'compartir_redes' => $compartir,
                    'fecha_modificacion' => date("Y-m-d H:i:s"),
                    'fecha_inicio' => $fecha_inicio,
                    'fecha_fin' => $fecha_fin,
                    'estado' => $estado
                );


                if ($this->Promocion_model->update($id_promocion, $data_promocion)) {

                    if ($locales == "1"){
                        $locales_query = array(
                            "select" => "*", "where" => "estado = 1"
                        );
                        $locales_data = $this->Local_model->search($locales_query);
                        if ($locales_data != null && sizeof($locales_data) > 0){
                            $locales_array = array();
                            for($i=0 ;  $i<sizeof($locales_data) ; $i++){
                                $local = array(
                                    'id_promocion' => $id_promocion,
                                    'id_local' => $locales_data[$i]->id,
                                    'fecha_registro' => date("Y-m-d H:i:s"),
                                    'estado' => ESTADO_ACTIVO);
                                $locales_array[$i]= $local;
                            }
                            $this->Local_promocion_model->insert_lote($locales_array);
                        }
                    }

                    $this->alert($this->lang->line('mensaje_promocion_editado'), site_url($this->config->item('path_backend') . '/Promocion'));

                }
            }

            $promocion = array(
                "select" => "*",
                "where" => "id = '" . $id_promocion . "'"
            );
            $categorias = array(
                "select" => "*", "where" => "estado = 1"
            );



            $categoria = $this->Categoria_model->search($categorias);
            $promocion = $this->Promocion_model->get_search_row($promocion);

            $arrayMigaPan = array(
                array("nombre" => "Promocion" ,"url"=>site_url($this->config->item('path_backend') . '/Promocion') ), array("nombre" => "Editar", 'active' => true));

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;

            $this->arrayVista['tituloPagina'] = "Editar Promoción";
            $this->arrayVista['empresas'] = $empresa;
            $this->arrayVista['categorias'] = $categoria;
            $this->arrayVista['promocion'] = $promocion;
            $this->arrayVista['vista'] = 'backend/promocion/editar_view';
            $this->cargarVistaBackend();

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function carga_masiva()
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {
                $id_empresa = $this->input->post("empresa");
                $excel = $this->input->post("excel");
                $pathExcel = "./upload/archivos/excel/promocion/" . date("Y-m-d");


                if (!is_dir($pathExcel))
                    mkdir($pathExcel, 0775, true);
                if ($_FILES["excel"]["size"] > 0) {
                    $nombreArchivoExcel = "excel";
                    $resultadoExcel = $this->subirArchivo($nombreArchivoExcel, $pathExcel,"", 'xls|xlsx', 2024);
                    $excel = "";

                    if (array_key_exists("upload_data", $resultadoExcel)) {
                        if (isset($resultadoExcel["upload_data"]["file_name"])) {

                            $this->load->library('excel/PHPExcel');
                            $objPHPExcel = PHPExcel_IOFactory::load($resultadoExcel['upload_data']['full_path']);
                        }
                    }
                }

                $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
                foreach ($cell_collection as $cell) {

                    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                }

                for ($i = 0; $i <= 14; $i++) {
                    for ($j = 2; $j <= $row; $j++) {

                        $datos[$j][$i] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($i, $j)->getCalculatedValue();
                    }
                }

                $datos = array_values($datos);

                for ($i = 0; $i < sizeof($datos); $i++) {

                    $categoria = array("select" => "*",
                        "where" => "nombre = '" . trim($datos[$i][0]) . "'");

                    $categoria_promocion = $this->Categoria_model->get_search_row($categoria);

                    if (!count($categoria_promocion) > 0 && !isset($categoria_promocion->id)) {
                        $array_cat_promo = array(
                            "nombre" => $datos[$i][0],
                            "fecha_registro" => date("Y-m-d H:i:s"),
                            "estado" => '1');

                        $this->Categoria_model->insert($array_cat_promo);
                        $id_cate_promo = $this->db->insert_id();

                    } else {
                        $id_cate_promo = $categoria_promocion->id;
                    }

                    $fecha_ini = PHPExcel_Shared_Date::ExcelToPHPObject($datos[$i][5]);
                    $fecha_fin = PHPExcel_Shared_Date::ExcelToPHPObject($datos[$i][6]);

                    if (strnatcasecmp($datos[$i][9], "Nominal") == 0) {
                        $tipoPromocion = 2;
                    } else if (strnatcasecmp($datos[$i][9], "%") == 0) {
                        $tipoPromocion= 1;
                    }


                    $array_promo = array(
                        "id_categoria" => $id_cate_promo,
                        "id_empresa" => $id_empresa,
                        "tipo" => $tipoPromocion,
                        "nombre" => $datos[$i][1],
                        "descripcion" => $datos[$i][2],
                        "descuento" => $datos[$i][4]*100,
                        "restriccion" => $datos[$i][3],
                        "orden" => $datos[$i][10],
                        "fecha_inicio" => $fecha_ini->format('Y-m-d '),
                        "fecha_fin" => $fecha_fin->format('Y-m-d '),
                        "fecha_registro" => date("Y-m-d H:i:s"),
                        "estado" => "1",
                    );
                    $this->Promocion_model->insert($array_promo);
                    $id_promocion= $this->db->insert_id();

                    if ($datos[$i][7] != null) {
                        $info_promo = array();
                        $info_promo[] = explode("\n", $datos[$i][7]);
                        $sub = count($info_promo, COUNT_RECURSIVE);
                        $subarray = $sub - 1;
                        for ($j = 0; $j < $subarray; $j++) {
                            $informacion = array();
                            $informacion[] = explode("|", $info_promo[0][$j]);
                            if (isset($informacion)) {

                                $array_info_promo = array(
                                    "id_promocion" => $id_promocion,
                                    "campo" => $informacion[0][0],
                                    "valor" => $informacion[0][1],
                                    "fecha_registro" => date("Y-m-d H:i:s"),
                                    "estado" => "1",
                                );

                            }
                            $this->Informacion_adicional_promocion_model->insert($array_info_promo);
                        }
                    }

                    if ($datos[$i][8] != null) {
                        $local_promo = array();
                        $local_promo[] = explode("\n", $datos[$i][8]);
                        $sub = count($local_promo, COUNT_RECURSIVE);
                        $subarray = $sub - 1;
                        for ($j = 0; $j < $subarray; $j++) {
                            $local = array();
                            $local[] = explode("|", $local_promo[0][$j]);

                            $local_query = array("select" => "*",
                                "where" => "nombre = '" . trim($local[0][0]) . "'");

                            $locales = $this->Local_model->get_search_row($local_query);

                            if (!count($locales) > 0 && !isset($locales->id)) {
                                $array_local = array(
                                    "id_empresa" => $id_empresa,
                                    "nombre" => $local[0][0],
                                    "direccion" => $local[0][1],
                                    "latitud" => $local[0][2],
                                    "longitud" => $local[0][3],
                                    "correo" => $local[0][4],
                                    "telefono" => $local[0][5],
                                    "fecha_registro" => date("Y-m-d H:i:s"),
                                    "estado" => '1'
                                );
                                $this->Local_model->insert($array_local);
                                $id_local = $this->db->insert_id();

                            } else {
                                $id_local = $locales->id;
                            }

                            $array_local_promocion = array(
                                "id_local" => $id_local,
                                "id_promocion" => $id_promocion,
                                "fecha_registro" => date("Y-m-d H:i:s"),
                                "estado" => '1'
                            );
                            $this->Local_promocion_model->insert($array_local_promocion);

                        }
                    }

                    for ($p = 11; $p <= 14; $p++) {
                        if ($p == 11) {
                            $tipo = 1;
                            $plataforma = 1;
                        } elseif($p == 12) {
                            $tipo = 2;
                            $plataforma = 1;
                        } elseif($p == 13) {
                            $tipo = 1;
                            $plataforma= 2;
                        } elseif($p == 14) {
                            $tipo = 2;
                            $plataforma= 2;
                        }


                        $array_foto_promocion = array(
                            "id_promocion" => $id_promocion,
                            "foto" => 'upload/promocion/'.date("Y-m-d").'/'.$datos[$i][$p],
                            "tipo" => $tipo,
                            "plataforma" => $plataforma,
                            "fecha_registro" => date("Y-m-d H:i:s"),
                            "estado" => "1"

                        );
                        $this->Foto_promocion_model->insert($array_foto_promocion);
                    }

                }

                $this->alert(("Datos cargados correctamente "), site_url($this->config->item('path_backend') . '/Promocion/carga_masiva'));

            }

            $empresas = array(
                "select" => "*", "where" => "estado = 1"
            );

            $empresa = $this->Empresa_model->search($empresas);

            $arrayMigaPan = array(
                array("nombre" => "Promocion", "url" => site_url($this->config->item('path_backend') . '/Promocion')), array("nombre" => "Carga Masiva", 'active' => true));

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['tituloPagina'] = "Carga Masiva";
            $this->arrayVista['empresas'] = $empresa;

            $this->arrayVista['vista'] = 'backend/promocion/carga_masiva_view';
            $this->cargarVistaBackend();
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function carga_imagenes()
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {

                $img = $this->input->post("img");

                $pathArchivos = "./upload/archivos/zip/promocion/" . date("Y-m-d");
                $pathImagenes = "./upload/promocion/" . date("Y-m-d");

                if (!is_dir($pathArchivos))
                    mkdir($pathArchivos, 0775, true);
                if ($_FILES["img"]["size"] > 0) {
                    $nombreArchivoImagenes = "img";
                    $resultadoImagenes = $this->subirArchivo($nombreArchivoImagenes, $pathArchivos, 'zip', 2024);
                    $img = "";

                    if (array_key_exists("upload_data", $resultadoImagenes)) {
                        if (isset($resultadoImagenes["upload_data"]["file_name"])) {
                            $zip = new ZipArchive;
                            $file = $resultadoImagenes['upload_data']['full_path'];
                            if ($zip->open($file) === TRUE) {
                                $zip->extractTo($pathImagenes);
                            }
                            $zip->close();
                        }
                    }
                }
                $this->alert(("Archivo cargado correctamente "), site_url($this->config->item('path_backend') . '/Promocion/carga_masiva'));
            }
        }
    }

    public function export()
    {
        if ($this->session->userdata('logged_in') === TRUE) {

            $nombre = $this->input->get('nombre');
            $categoria = $this->input->get('categoria');
            $empresa = $this->input->get('empresa');
            $fecha_inicio = $this->input->get('fecha_inicio');
            $fecha_fin = $this->input->get('fecha_fin');
            $estado = $this->input->get('estado');


            $where = "";


            if ($nombre != "" ) {


                if ($where != "") {
                    $where .= " AND tbl_promocion.nombre LIKE '%" . $nombre . "%' ";
                } else {
                    $where = "tbl_promocion.nombre LIKE '%" . $nombre . "%' ";
                }
            }


            if ($empresa != "" ) {

                if ($where != "")
                    $where .= " AND tbl_promocion.id_empresa = $empresa";
                else
                    $where .= "tbl_promocion.id_empresa = $empresa";
            }

            if ($categoria != "" ) {

                if ($where != "")
                    $where .= " AND tbl_promocion.id_categoria = $categoria";
                else
                    $where .= "tbl_promocion.id_categoria = $categoria";
            }

            if ($fecha_inicio != "" ) {

                list($anio,$mes,$dia) = explode('-',$fecha_inicio);
                if ($where != "") {

                    $where .= " AND DATE(tbl_promocion.fecha_inicio) >= '$anio-$mes-$dia'  ";
                } else {

                    $where = "DATE(tbl_promocion.fecha_inicio)  >= '$anio-$mes-$dia' ";
                }
            }

            if ($fecha_fin != "" ) {
                
                list($anio,$mes,$dia) = explode('-',$fecha_fin);
                if ($where != "") {

                    $where .= " AND DATE(tbl_promocion.fecha_fin) <= '$anio-$mes-$dia'  ";

                } else {
                    $where = "DATE(tbl_promocion.fecha_fin)  <= '$anio-$mes-$dia' ";
                }
            }

            if ($estado != "" ) {

                if ($where != "")
                    $where .= " AND tbl_promocion.estado = $estado";
                else
                    $where .= "tbl_promocion.estado = $estado";
            }

            $join = array(
                'tbl_empresa as E, E.id = tbl_promocion.id_empresa', 'tbl_categoria as  C, C.id=tbl_promocion.id_categoria'
            );
            /*
            if ($where == "") {
                $params = array(
                    "select" => "E.nombre as empresa,C.nombre  as categoria,tbl_promocion.nombre,tbl_promocion.descripcion,
                    tbl_promocion.descuento,tbl_promocion.restriccion,tbl_promocion.orden,tbl_promocion.tipo,tbl_promocion.slug,
                    tbl_promocion.compartir_redes,tbl_promocion.fecha_inicio,tbl_promocion.fecha_fin,
                    tbl_promocion.fecha_registro,tbl_promocion.estado",
                    "join" => $join,
                    "order" => "tbl_promocion.fecha_registro desc"

                );
            } else {
                $params = array(
                    "select" => "E.nombre as empresa,C.nombre  as categoria,tbl_promocion.nombre,tbl_promocion.descripcion,
                    tbl_promocion.descuento,tbl_promocion.restriccion,tbl_promocion.orden,tbl_promocion.tipo,tbl_promocion.slug,
                    tbl_promocion.compartir_redes,tbl_promocion.fecha_inicio,tbl_promocion.fecha_fin,
                    tbl_promocion.fecha_registro,tbl_promocion.estado",
                    "join" => $join,
                    "where" => $where,
                    "order" => "tbl_promocion.fecha_registro desc"
                );
            }
            */

            $params = array(
                    "select" => "E.nombre as empresa,C.nombre  as categoria,tbl_promocion.nombre,tbl_promocion.descripcion,
                    tbl_promocion.descuento,tbl_promocion.restriccion,tbl_promocion.orden,tbl_promocion.tipo,tbl_promocion.slug,
                    tbl_promocion.compartir_redes,tbl_promocion.fecha_inicio,tbl_promocion.fecha_fin,
                    tbl_promocion.fecha_registro,tbl_promocion.estado",
                    "join" => $join,
                    "where" => $where,
                    "order" => "tbl_promocion.fecha_registro desc",
                    "group" => "C.nombre"
                );
            $promociones = $this->Promocion_model->search($params);

           /* $this->load->library('excel');
            $this->excel->setActiveSheetIndex(0);
            $this->excel->getActiveSheet()->setTitle("Promocion");

            $this->excel->getActiveSheet()->setCellValue('A1', 'Categoria');
            $this->excel->getActiveSheet()->setCellValue('B1', 'Empresa');
            $this->excel->getActiveSheet()->setCellValue('C1', 'Nombre');
            $this->excel->getActiveSheet()->setCellValue('D1', 'Descripción');
            $this->excel->getActiveSheet()->setCellValue('E1', 'Descuento');
            $this->excel->getActiveSheet()->setCellValue('F1', 'Restricción');
            $this->excel->getActiveSheet()->setCellValue('G1', 'Orden');
            $this->excel->getActiveSheet()->setCellValue('H1', 'Tipo');
            $this->excel->getActiveSheet()->setCellValue('I1', 'Slug');
            $this->excel->getActiveSheet()->setCellValue('J1', 'Compartir Redes');
            $this->excel->getActiveSheet()->setCellValue('K1', 'Fecha de Inicio');
            $this->excel->getActiveSheet()->setCellValue('L1', 'Fecha de Finalizacion');
            $this->excel->getActiveSheet()->setCellValue('M1', 'Fecha de Registro');
            $this->excel->getActiveSheet()->setCellValue('N1', 'Estado');


            for($i=65; $i<=78; $i++) {
                $letra = chr($i);
                $this->excel->getActiveSheet()->getStyle($letra.'1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }

            for($i=65; $i<=78; $i++) {
                $letra = chr($i);
                $this->excel->getActiveSheet()->getStyle($letra.'1')->getFont()->setBold(true);
            }



            for ($col = ord('A'); $col <= ord('N'); $col++) {

                $this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);

                $this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
                $this->excel->getActiveSheet()->getStyle(chr($col))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }*/
            //retrive contries table data
            $data = array();
            foreach ($promociones as $promocion) {

                if ($promocion->tipo == 1){
                    $tipo = " % ";
                }else{
                    $tipo = "Nominal";
                }


                if ($promocion->estado == 1){
                    $estado = "Activo";
                }else{
                    $estado = "Inactivo";
                }


                array_push($data, 
                    array(
                        utf8_decode("Categoría") => $promocion->categoria,
                        utf8_decode("Empresa") => isset($promocion->empresa)?utf8_decode($promocion->empresa):"-",
                        utf8_decode("Nombre") => isset($promocion->nombre)?utf8_decode($promocion->nombre):"-",
                        utf8_decode("Descripción") => isset($promocion->descripcion)?utf8_decode($promocion->descripcion):"-",
                        utf8_decode("Descuento") => isset($promocion->descuento)?utf8_decode($promocion->descuento):"-",
                        utf8_decode("Restricción") => isset($promocion->restriccion)?utf8_decode($promocion->restriccion):"-",
                        utf8_decode("Orden") => isset($promocion->orden)?utf8_decode($promocion->orden):"-",
                        utf8_decode("Tipo") => isset($tipo)?utf8_decode($tipo):"-",
                        utf8_decode("Fecha de Inicio") => isset($promocion->fecha_inicio)?date("d/m/Y", strtotime($promocion->fecha_inicio)):"-",
                        utf8_decode("Fecha de Registro") => isset($promocion->fecha_inicio)?date("d/m/Y h:i:s a", strtotime($promocion->fecha_registro)):"-",
                        utf8_decode("Estado") => isset($estado)?$estado:"-"
                ));
            }
            //Fill data
            function filterData(&$str)
            {
                $str = preg_replace("/\t/", "\\t", $str);
                $str = preg_replace("/\r?\n/", "\\n", $str);
                if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
            }             
                
            $fileName = "Promociones-". date('YmdHis') . ".xls";
                
            header("Content-Disposition: attachment; filename=\"$fileName\"");
            header("Content-Type: application/vnd.ms-excel, charset=utf-8");
                
            $flag = false;

            foreach($data as $row) {
                // print_r( array_values($row))."<br>";
                if(!$flag) {
                    
                    echo implode("\t", array_keys($row)) . "\n";
                    $flag = true;
                }
                    
                array_walk($row, 'filterData');
                echo implode("\t", array_values($row)) . "\n";
            }
                
            exit;
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }
}
