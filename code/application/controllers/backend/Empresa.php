<?php
/**
 * Created by PhpStorm.
 * User: Mediabyte
 * Date: 10/05/2016
 * Time: 10:14 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Empresa extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model("Beneficio_model");
        $this->load->model("Categoria_beneficio_model");
        $this->load->model("Empresa_model");
        $this->load->model("Local_model");
        $this->load->model("Formula_puntos_model");
        $this->load->model("Formula_puntos_empresa_model");
        $this->lang->load('backend_error_lang', 'spanish');
        $this->lang->load('backend_mensajes_lang', 'spanish');
        $this->load->helper('url');


    }

    public function listar($start = 0)
    {

        if ($this->session->userdata('logged_in') === TRUE) {
            $params = array(
                "select" => "*",
                "order"=> "fecha_registro desc"
            );
            $total_empresas = $this->Empresa_model->total_records($params);

            $empresa = $this->Empresa_model->search_data($params, $start, $this->elementoPorPagina);

            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Empresa'), $total_empresas);


           $arrayMigaPan = array(
                array("nombre" => "Empresa"), array("nombre" => "Listar", 'active' => true));

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['tituloPagina'] = "Lista de Empresas";

            $this->arrayVista['empresas'] = $empresa;

            $this->arrayVista['vista'] = 'backend/empresa/listar_view';
            $this->cargarVistaBackend();
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function buscar($start = 0)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            $nombre = $this->input->post('nombre');
            $codigo = $this->input->post('codigo');
            $ruc = $this->input->post('ruc');
            $estado = $this->input->post('estado');
            $where = "";


            if ($nombre != "") {
                $this->arrayVista['nombre'] = $nombre;
                if ($where != "") {
                    $where .= " AND nombre LIKE '%" . $nombre . "%' ";
                } else {
                    $where = "nombre LIKE '%" . $nombre . "%' ";
                }
            }

            if ($codigo != "") {
                $this->arrayVista['codigo'] = $codigo;
                if ($where != "") {
                    $where .= " AND codigo LIKE '%" . $codigo . "%' ";
                } else {
                    $where = "codigo LIKE '%" . $codigo . "%' ";
                }
            }

            if ($ruc != "") {
                $this->arrayVista['ruc'] = $ruc;
                if ($where != "") {
                    $where .= " AND ruc LIKE '%" . $ruc . "%' ";
                } else {
                    $where = "ruc LIKE '%" . $ruc . "%' ";
                }
            }



            if ($estado != "") {
                $this->arrayVista['estado'] = $estado;
                if ($where != "")
                    $where .= " AND estado = $estado";
                else
                    $where .= "estado = $estado";
            }

            if ($where == "") {
                $params = array(
                    "select" => "*",
                    "order"=> "fecha_registro desc"
                );
            } else {
                $params = array(
                    "select" => "*",
                    "where" => $where,
                    "order"=> "fecha_registro desc"
                );
            }

            $total_empresas = $this->Empresa_model->total_records($params);

            $empresa= $this->Empresa_model->search_data($params, $start, $this->elementoPorPagina);
            $arrayMigaPan = array(
                array("nombre" => "Empresa"), array("nombre" => "Listar", 'active' => true));

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend').'/Empresa/buscar'), $total_empresas);
            $this->arrayVista['tituloPagina'] = "Lista de Empresas";
            $this->arrayVista['empresas'] = $empresa;
            $this->arrayVista['vista'] = 'backend/empresa/listar_view';
            $this->cargarVistaBackend();
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function agregar()
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {

                $nombre = $this->input->post("nombre");
                $codigo = $this->input->post("codigo");
                $descripcion = $this->input->post("descripcion");
                $ruc = $this->input->post("ruc");
                $razon_social = $this->input->post("razon_social");
                $logo = $this->input->post("logo");
                $formula = $this->input->post("formula");
                $sitio_web= $this->input->post("sitio_web");
                $estado = $this->input->post("estado");

                $nombre_local = $this->input->post("nombre_local");
                $direccion = $this->input->post("direccion");
                $latitud = $this->input->post("latitud");
                $longitud = $this->input->post("longitud");
                $correo = $this->input->post("correo");
                $telefono = $this->input->post("telefono");
                $localidad= $this->input->post("localidad");
                $meses= $this->input->post("meses");


                $pathImagen = "upload/empresa/" . date("Y-m-d");


                if (!is_dir($pathImagen))
                    mkdir($pathImagen, 0775, true);
                if ($_FILES["logo"]["size"] > 0) {
                    $nombreArchivoImagen = "logo";
                    $resultadoFoto = $this->subirArchivo($nombreArchivoImagen, $pathImagen,"", 'jpeg|jpg|png', 2024);
                    $logo = "";
                    if (array_key_exists("upload_data", $resultadoFoto)) {
                        if (isset($resultadoFoto["upload_data"]["file_name"]))
                            $logo = $resultadoFoto["upload_data"]["orig_name"];
                            //$logo = base_url() . "files/img/empresa/" . date("Y-m-d") . '/' . $resultadoFoto["upload_data"]["file_name"];
                    } else {
                        $this->session->set_flashdata('error', $this->lang->line('error_tamaño_imagen'));
                    }
                }
                $logo = $pathImagen."/".$logo;
                $data_empresa = array(
                    'nombre' => $nombre,
                    'codigo' => $codigo,
                    'descripcion' => $descripcion,
                    'ruc' => $ruc,
                    'razon_social' => $razon_social,
                    'logo' => $logo,
                    'sitio_web' => $sitio_web,
                    'fecha_registro' => date("Y-m-d H:i:s"),
                    'estado' => $estado
                );



                if (isset($data_empresa)) {
                    try {
                        $this->Empresa_model->insert($data_empresa);
                        $id_empresa = $this->db->insert_id();

                        $data_local_empresa = array(
                            'id_empresa' =>$id_empresa,
                            'nombre' => $nombre_local,
                            'direccion' => $direccion,
                            'latitud' => $latitud,
                            'longitud' => $longitud,
                            'correo' => $correo,
                            'telefono' => $telefono,
                            'localidad' => $localidad,
                            'tipo' => LOCAL_DEFAULT,
                            'meses_reserva' => $meses,
                            'fecha_registro' => date("Y-m-d H:i:s"),
                            'estado' => ESTADO_ACTIVO
                        );

                        $data_formula_empresa = array(
                            'id_formula_puntos' => $formula,
                            'id_empresa' => $id_empresa,
                            'fecha_registro' => date("Y-m-d H:i:s"),
                            'estado' => "1"
                        );
                        $this->Formula_puntos_empresa_model->insert($data_formula_empresa);
                        $this->Local_model->insert($data_local_empresa);
                        $this->alert($this->lang->line('mensaje_empresa_agregado'), site_url($this->config->item('path_backend') . '/Empresa'));
                    } catch (Exception $exception) {
                        $this->alert($this->lang->line('error_empresa_agregar'));
                    }
                }

            }


            $formulas = array(
                "select" => "*", "where" => "estado = 1"
            );

            $formulas = $this->Formula_puntos_model->search($formulas);

            $arrayMigaPan = array(
                array("nombre" => "Empresa" ,"url" => site_url($this->config->item('path_backend') . '/Empresa')), array("nombre" => "Agregar", 'active' => true));

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['tituloPagina'] = "Agregar Empresa";
            $this->arrayVista['formulas'] = $formulas;
            $this->arrayVista['vista'] = 'backend/empresa/agregar_view';
            $this->cargarVistaBackend();

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function editar($id_empresa)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {
                $nombre = $this->input->post("nombre");
                $codigo = $this->input->post("codigo");
                $descripcion = $this->input->post("descripcion");
                $ruc = $this->input->post("ruc");
                $razon_social = $this->input->post("razon_social");
                $logo = $this->input->post("logo");
                $logo_antiguo = $this->input->post("logo_antiguo");
                $formula = $this->input->post("formula");
                $id_for_punt_emp = $this->input->post("id_for_punt_emp");
                $sitio_web= $this->input->post("sitio_web");
                $estado = $this->input->post("estado");

                $pathImagen = "                                                                                                                                                 upload/empresa/" . date("Y-m-d");

                if (!is_dir($pathImagen))
                    mkdir($pathImagen, 0775, true);
                if ($_FILES["logo"]["size"] > 0) {
                    $nombreArchivoImagen = "logo";
                    $resultadoFoto = $this->subirArchivo($nombreArchivoImagen, $pathImagen,"", 'jpeg|jpg|png', 2024);
                    $logo = "";
                    if (array_key_exists("upload_data", $resultadoFoto)) {
                        if (isset($resultadoFoto["upload_data"]["file_name"]))
                            $logo = $resultadoFoto["upload_data"]["orig_name"];
                            $logo = $pathImagen."/".$logo;
                           // $logo = base_url() . "files/img/empresa/" . date("Y-m-d") . '/' . $resultadoFoto["upload_data"]["file_name"];
                    } else {
                        $this->session->set_flashdata('error',$this->lang->line('error_tamaño_imagen'));
                    }

                }else{

                    $logo=$logo_antiguo;

                }
                $data_empresa = array(

                    'nombre' => $nombre,
                    'codigo' => $codigo,
                    'descripcion' => $descripcion,
                    'ruc' => $ruc,
                    'razon_social' => $razon_social,
                    'logo' => $logo,
                    'sitio_web' => $sitio_web,
                    'fecha_modificacion' => date("Y-m-d H:i:s"),
                    'estado' => $estado
                );

                $data_formula_empresa = array(

                    'id_formula_puntos' => $formula,
                    'fecha_modificacion' => date("Y-m-d H:i:s"),
                );



                if ($this->Empresa_model->update($id_empresa, $data_empresa)) {

                    $this->Formula_puntos_empresa_model->update($id_for_punt_emp, $data_formula_empresa);
                    $this->alert($this->lang->line('mensaje_empresa_editado'), site_url($this->config->item('path_backend') . '/Empresa'));

                }
            }

            $join = array("tbl_formula_puntos_empresa as P,P.id_empresa = tbl_empresa.id");

            $empresa = array(
                "select" => "tbl_empresa.id,tbl_empresa.nombre,tbl_empresa.codigo,tbl_empresa.descripcion,tbl_empresa.ruc,
                tbl_empresa.razon_social,tbl_empresa.logo,tbl_empresa.sitio_web,tbl_empresa.estado,P.id_formula_puntos AS formula,
                P.id AS id_for,tbl_empresa.fecha_registro",
                "join" => $join,
               "where" => "tbl_empresa.id = '" . $id_empresa . "'"

            );

            $formulas = array(
                "select" => "*", "where" => "estado = 1"
            );

            $formulas = $this->Formula_puntos_model->search($formulas);
            $empresa = $this->Empresa_model->get_search_row($empresa);
            //$fecha_logo = $this->fechaImagen($empresa->logo);
            $arrayMigaPan = array(
                array("nombre" => "Empresa" ,"url" => site_url($this->config->item('path_backend') . '/Empresa')), array("nombre" => "Editar", 'active' => true));
            $this->arrayVista['formulas'] = $formulas;
            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['tituloPagina'] = "Editar Empresa";
            $this->arrayVista['empresa'] = $empresa;
            $this->arrayVista['fecha'] = $fecha_logo;



            $this->arrayVista['vista'] = 'backend/empresa/editar_view';
            $this->cargarVistaBackend();

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function export()
    {
        if ($this->session->userdata('logged_in') === TRUE) {

            $nombre = $this->input->get('nombre');
            $codigo = $this->input->get('codigo');
            $ruc = $this->input->get('ruc');
            $estado = $this->input->get('estado');


            $where = "";


            if ($nombre != "" ) {

                $this->arrayVista['nombre'] = $nombre;
                if ($where != "") {
                    $where .= " AND nombre LIKE '%" . $nombre . "%' ";
                } else {
                    $where = "nombre LIKE '%" . $nombre . "%' ";
                }
            }


            if ($codigo != "") {
                $this->arrayVista['codigo'] = $codigo;
                if ($where != "") {
                    $where .= " AND codigo LIKE '%" . $codigo . "%' ";
                } else {
                    $where = "codigo LIKE '%" . $codigo . "%' ";
                }
            }

            if ($ruc != "") {
                $this->arrayVista['ruc'] = $ruc;
                if ($where != "") {
                    $where .= " AND ruc LIKE '%" . $ruc . "%' ";
                } else {
                    $where = "ruc LIKE '%" . $ruc . "%' ";
                }
            }


            if ($estado != "" ) {
                $this->arrayVista['estado'] = $estado;
                if ($where != "")
                    $where .= " AND estado = $estado";
                else
                    $where .= "estado = $estado";
            }

            if ($where == "") {
                $params = array(
                    "select" => "*",
                    "order" => "fecha_registro desc"
                );
            } else {
                $params = array(
                    "select" => "*",
                    "where" => $where,
                    "order" => "fecha_registro desc"
                );
            }

            $empresas= $this->Empresa_model->search($params);

            
            //retrive contries table data
            $data = array();
            foreach ($empresas as $empresa) {
                

                if ($empresa->estado == 1){
                    $estado = "Activo";
                }else{
                    $estado = "Inactivo";
                }
 
                
                array_push(
                    $data, 
                    array(
                        utf8_decode("Nombre") => isset($empresa->nombre)?utf8_decode($empresa->nombre):"-",
                        utf8_decode("Código") => isset($empresa->codigo)?utf8_decode($empresa->codigo):"-",
                        utf8_decode("Descripción") => isset($empresa->descripcion)?utf8_decode($empresa->descripcion):"-",
                        utf8_decode("Ruc") => isset($empresa->ruc)?utf8_decode($empresa->ruc):"-",
                        utf8_decode("Razón Social") => isset($empresa->razon_social)?utf8_decode($empresa->razon_social):"-",
                        utf8_decode("Logo") =>  isset($empresa->logo)?base_url().substr($empresa->logo,2):"-",
                        utf8_decode("Sitio Web") => isset($empresa->sitio_web)?utf8_decode($empresa->sitio_web):"-",
                        utf8_decode("Fecha de Registro") => date("d/m/Y h:i:s a", strtotime($empresa->fecha_registro)),
                        utf8_decode("Estado") => isset($estado)?utf8_decode($estado):"-",
                    )
                );
            }
            //Fill data
            function filterData(&$str)
            {
                $str = preg_replace("/\t/", "\\t", $str);
                $str = preg_replace("/\r?\n/", "\\n", $str);
                if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
            }             
                
            $fileName = "Empresa -". date('YmdHis') . ".xls";
                
            header("Content-Disposition: attachment; filename=\"$fileName\"");
            header("Content-Type: application/vnd.ms-excel, charset=utf-8");
                
            $flag = false;

            foreach($data as $row) {
                // print_r( array_values($row))."<br>";
                if(!$flag) {
                    
                    echo implode("\t", array_keys($row)) . "\n";
                    $flag = true;
                }
                    
                array_walk($row, 'filterData');
                echo implode("\t", array_values($row)) . "\n";
            }
                
            exit;
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }
}
