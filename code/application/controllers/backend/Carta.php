<?php
/**
 * Created by PhpStorm.
 * User: Mediabyte
 * Date: 11/05/2016
 * Time: 10:34 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Carta extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model("Carta_model");
        $this->load->model("Local_model");
        $this->load->model("Empresa_model");
        $this->load->model("Foto_carta_model");
        $this->load->model("Categoria_carta_model");
        $this->lang->load('backend_error_lang', 'spanish');
        $this->lang->load('backend_mensajes_lang', 'spanish');
        $this->load->helper('url');

    }

    public function listar($start = 0)
    {

        if ($this->session->userdata('logged_in') === TRUE) {

            $params = array(
                "select" => "tbl_carta.id,tbl_carta.nombre,tbl_carta.precio,tbl_carta.descripcion,tbl_carta.fecha_registro,
                tbl_carta.fecha_modificacion,tbl_carta.estado,categoria.id as id_categoria,categoria.nombre as nombre_categoria",
                "join"=>array("tbl_categoria_carta as categoria,categoria.id = tbl_carta.id_categoria_carta "),
                "order"=> "tbl_carta.fecha_registro desc"
            );
            $total_cartas = $this->Carta_model->total_records($params);

            $carta = $this->Carta_model->search_data($params, $start, $this->elementoPorPagina);
            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Carta'), $total_cartas);


            $arrayMigaPan = array(
                array("nombre" => "Carta"), array("nombre" => "Listar", 'active' => true));

            $categorias = array(
                "select" => "id,nombre"
            );
            $categoria = $this->Categoria_carta_model->search($categorias);
            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['tituloPagina'] = "Lista de Cartas";
            $this->arrayVista['cartas'] = $carta;
            $this->arrayVista['categorias'] = $categoria;
            $this->arrayVista['vista'] = 'backend/carta/listar_view';
            $this->cargarVistaBackend();
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function buscar($start = 0)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            $nombre = $this->input->post('nombre');
            $categoria = $this->input->post('categoria');
            $estado = $this->input->post('estado');
            $where = "";


            if ($nombre != "") {
                $this->arrayVista['nombre'] = $nombre;
                if ($where != "") {
                    $where .= " AND tbl_carta.nombre LIKE '%" . $nombre . "%' ";
                } else {
                    $where = "tbl_carta.nombre LIKE '%" . $nombre . "%' ";
                }
            }

            if ($categoria != "") {
                $this->arrayVista['categoria_select'] = $categoria;
                if ($where != "")
                    $where .= " AND tbl_carta.id_categoria_carta = $categoria";
                else
                    $where .= "tbl_carta.id_categoria_carta = $categoria";
            }

            if ($estado != "") {
                $this->arrayVista['estado'] = $estado;
                if ($where != "")
                    $where .= " AND tbl_carta.estado = $estado";
                else
                    $where .= "tbl_carta.estado = $estado";
            }

            if ($where == "") {
                $params = array(
                    "select" => "tbl_carta.id,tbl_carta.nombre,tbl_carta.precio,tbl_carta.descripcion,tbl_carta.fecha_registro,
                tbl_carta.fecha_modificacion,tbl_carta.estado,categoria.id as id_categoria,categoria.nombre as nombre_categoria",
                    "join"=>array("tbl_categoria_carta as categoria,categoria.id = tbl_carta.id_categoria_carta "),
                    "order"=> "tbl_carta.fecha_registro desc"
                );
            } else {
                $params = array(
                    "select" => "tbl_carta.id,tbl_carta.nombre,tbl_carta.precio,tbl_carta.descripcion,tbl_carta.fecha_registro,
                    tbl_carta.fecha_modificacion,tbl_carta.estado,categoria.id as id_categoria,categoria.nombre as nombre_categoria",
                    "join"=>array("tbl_categoria_carta as categoria,categoria.id = tbl_carta.id_categoria_carta "),
                    "where" => $where,
                    "order"=> "tbl_carta.fecha_registro desc"
                );
            }

            $total_cartas = $this->Carta_model->total_records($params);

            $carta = $this->Carta_model->search_data($params, $start, $this->elementoPorPagina);
            $arrayMigaPan = array(
                array("nombre" => "Carta"), array("nombre" => "Listar", 'active' => true));

            $categorias = array(
                "select" => "id,nombre"
            );

            $categoria = $this->Categoria_carta_model->search($categorias);
            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Beneficio/buscar'), $total_cartas);
            $this->arrayVista['tituloPagina'] = "Lista de Cartas";
            $this->arrayVista['categorias'] = $categoria;
            $this->arrayVista['cartas'] = $carta;
            $this->arrayVista['vista'] = 'backend/carta/listar_view';
            $this->cargarVistaBackend();
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function agregar()
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {
                $id_categoria_carta = $this->input->post("categoria");
                $id_local = $this->input->post("local");
                $nombre = $this->input->post("nombre");
                $precio = $this->input->post("precio");
                $descripcion = $this->input->post("descripcion");
                $estado = $this->input->post("estado");

                $data_carta = array(
                    'id_categoria_carta' => $id_categoria_carta,
                    'id_local' => $id_local,
                    'nombre' => $nombre,
                    'precio' => $precio,
                    'descripcion' => $descripcion,
                    'fecha_registro' => date("Y-m-d H:i:s"),
                    'estado' => $estado
                );

                if (isset($data_carta)) {
                    try {
                        $this->Carta_model->insert($data_carta);
                        $this->alert($this->lang->line('mensaje_carta_agregado'), site_url($this->config->item('path_backend') . '/Carta'));
                    } catch (Exception $exception) {
                        $this->alert($this->lang->line('error_carta_agregar'));
                    }
                }

            }

            $categorias = array(
                "select" => "*", "where" => "estado = 1"
            );

            $locales = array(
                "select" => "*", "where" => "estado = 1"
            );
            $locales = $this->Local_model->search($locales);
            $categoria = $this->Categoria_carta_model->search($categorias);

            $arrayMigaPan = array(
                array("nombre" => "Carta" , "url" => site_url($this->config->item('path_backend') . '/Carta')), array("nombre" => "Agregar", 'active' => true));

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['tituloPagina'] = "Agregar Carta";
            $this->arrayVista['local'] = $locales;
            $this->arrayVista['categorias'] = $categoria;
            $this->arrayVista['vista'] = 'backend/carta/agregar_view';
            $this->cargarVistaBackend();

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function editar($id_carta)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {
                $id_categoria_carta = $this->input->post("categoria");
                $id_local = $this->input->post("local");
                $nombre = $this->input->post("nombre");
                $precio = $this->input->post("precio");
                $descripcion = $this->input->post("descripcion");
                $estado = $this->input->post("estado");

                $data_carta = array(

                    'id_categoria_carta' => $id_categoria_carta,
                    'id_local' => $id_local,
                    'nombre' => $nombre,
                    'precio' => $precio,
                    'descripcion' => $descripcion,
                    'fecha_modificacion' => date("Y-m-d H:i:s"),
                    'estado' => $estado
                );

                if ($this->Carta_model->update($id_carta, $data_carta)) {


                    $this->alert($this->lang->line('mensaje_carta_editado'), site_url($this->config->item('path_backend') . '/Carta'));

                }
            }

            $carta = array(
                "select" => "*",
                "where" => "id = '" . $id_carta . "'"
            );

            $categorias = array(
                "select" => "*", "where" => "estado = 1"
            );

            $locales = array(
                "select" => "*", "where" => "estado = 1"
            );


            $categoria = $this->Categoria_carta_model->search($categorias);
            $local = $this->Local_model->search($locales);
            $carta = $this->Carta_model->get_search_row($carta);
            $arrayMigaPan = array(
                array("nombre" => "Carta" , "url" => site_url($this->config->item('path_backend') . '/Carta')), array("nombre" => "Editar", 'active' => true));

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['tituloPagina'] = "Editar Carta";
            $this->arrayVista['local'] = $local;
            $this->arrayVista['categorias'] = $categoria;
            $this->arrayVista['carta'] = $carta;
            $this->arrayVista['vista'] = 'backend/carta/editar_view';
            $this->cargarVistaBackend();

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function export()
    {

        if ($this->session->userdata('logged_in') === TRUE) {

            $nombre = $this->input->get('nombre');
            $categoria = $this->input->get('categoria');
            $estado = $this->input->get('estado');


            $where = "";


            if ($nombre != "" ) {


                if ($where != "") {
                    $where .= " AND nombre LIKE '%" . $nombre . "%' ";
                } else {
                    $where = "nombre LIKE '%" . $nombre . "%' ";
                }
            }

            if ($categoria != "") {

                if ($where != "")
                    $where .= " AND id_categoria_carta = $categoria";
                else
                    $where .= "id_categoria_carta = $categoria";
            }

            if ($estado != "" ) {

                if ($where != "")
                    $where .= " AND estado = $estado";
                else
                    $where .= "estado = $estado";
            }

            if ($where == "") {
                $params = array(
                    "select" => "*",
                    "order" => "fecha_registro desc"
                );
            } else {
                $params = array(
                    "select" => "*",
                    "where" => $where,
                    "order" => "fecha_registro desc"
                );
            }

            $cartas = $this->Carta_model->search($params);
            /*
            $this->load->library('excel');
            $this->excel->setActiveSheetIndex(0);
            $this->excel->getActiveSheet()->setTitle("Carta");


            $this->excel->getActiveSheet()->setCellValue('A1', 'Nombre');
            $this->excel->getActiveSheet()->setCellValue('B1', 'Precio');
            $this->excel->getActiveSheet()->setCellValue('C1', utf8_decode('Descripción'));
            $this->excel->getActiveSheet()->setCellValue('D1', 'Fecha de Registro');
            $this->excel->getActiveSheet()->setCellValue('E1', 'Estado');

            for($i=65; $i<=69; $i++) {
                $letra = chr($i);
                $this->excel->getActiveSheet()->getStyle($letra.'1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }

            for($i=65; $i<=69; $i++) {
                $letra = chr($i);
                $this->excel->getActiveSheet()->getStyle($letra.'1')->getFont()->setBold(true);
            }



            for ($col = ord('A'); $col <= ord('E'); $col++) {

                $this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);

                $this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
                $this->excel->getActiveSheet()->getStyle(chr($col))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }
            
            */
            //retrive contries table data
            $data = array();
            foreach ($cartas as $carta) {



                if ($carta->estado== 1){
                    $estado = "Activo";
                }else{
                    $estado = "Inactivo";
                }


                array_push($data, array(
                    utf8_decode("Nombre") => isset($carta->nombre)?utf8_decode($carta->nombre):"-",
                    utf8_decode("Precio") => isset($carta->precio)?utf8_decode($carta->precio):"-",
                    utf8_decode("Descripción") =>isset($carta->descripcion)?utf8_decode($carta->descripcion):"-",
                    utf8_decode("Fecha de Registro") => date("d/m/Y h:i:s a", strtotime($carta->fecha_registro)),
                    utf8_decode("Estado") => $estado,
                ));
            }
            

            function filterData(&$str)
            {
                $str = preg_replace("/\t/", "\\t", $str);
                $str = preg_replace("/\r?\n/", "\\n", $str);
                if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
            }             
                
            $fileName = "carta-". date('YmdHis') . ".xls";
                
            header("Content-Disposition: attachment; filename=\"$fileName\"");
            header("Content-Type: application/vnd.ms-excel, charset=utf-8");
                
            $flag = false;

            foreach($data as $row) {
                // print_r( array_values($row))."<br>";
                if(!$flag) {
                    
                    echo implode("\t", array_keys($row)) . "\n";
                    $flag = true;
                }
                    
                array_walk($row, 'filterData');
                echo implode("\t", array_values($row)) . "\n";
            }
                
            exit;

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function carga_masiva()
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {

                $excel = $this->input->post("excel");
                $id_empresa = $this->input->post("empresa");
                $pathExcel = "./upload/archivos/excel/carta/" . date("Y-m-d");

                if (!is_dir($pathExcel))
                    mkdir($pathExcel, 0775, true);
                if ($_FILES["excel"]["size"] > 0) {
                    $nombreArchivoExcel = "excel";
                    $resultadoExcel = $this->subirArchivo($nombreArchivoExcel, $pathExcel,"",'xls|xlsx', 2024);
                    $excel = "";

                    if (array_key_exists("upload_data", $resultadoExcel)) {
                        if (isset($resultadoExcel["upload_data"]["file_name"])) {

                            $this->load->library('excel/PHPExcel');
                            $objPHPExcel = PHPExcel_IOFactory::load($resultadoExcel['upload_data']['full_path']);
                        }
                    }
                }


                $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
                foreach ($cell_collection as $cell) {
                    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();

                }

                for ($i = 0; $i <= 8; $i++) {
                    for ($j = 2; $j <= $row; $j++) {
                        $datos[$j][$i] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($i, $j)->getCalculatedValue();
                    }
                }

                $datos = array_values($datos);

                for ($i = 0; $i < sizeof($datos); $i++) {

                    if ($datos[$i][0] != null || $datos[$i][0] != "") {

                        $categoria = array(
                            "select" => "*",
                            "where" => "nombre = '" . trim($datos[$i][0]) . "'"
                        );

                        $categoria_carta = $this->Categoria_carta_model->get_search_row($categoria);

                        if (!count($categoria_carta) > 0 && !isset($categoria_carta->id)) {
                            $array_cat_carta = array(
                                "id_empresa" => $id_empresa,
                                "nombre" => $datos[$i][0],
                                "fecha_registro" => date("Y-m-d H:i:s"),
                                "estado" => '1'
                            );

                            $this->Categoria_carta_model->insert($array_cat_carta);
                            $id_cate_carta = $this->db->insert_id();

                        } else {
                            $id_cate_carta = $categoria_carta->id;
                        }
                    }

                    if ($datos[$i][4] != null || $datos[$i][4] != "") {

                            $local = array();
                            $local[] = explode("|", $datos[$i][4]);

                            $local_query = array("select" => "*",
                                                 "where" => "nombre = '" . trim($local[0][0]) . "'");

                            $locales = $this->Local_model->get_search_row($local_query);

                            if (!count($locales) > 0 && !isset($locales->id)) {
                                $array_local = array(
                                    "id_empresa" => $id_empresa,
                                    "nombre" => trim($local[0][0]),
                                    "direccion" => trim($local[0][1]),
                                    "latitud" => trim($local[0][2]),
                                    "longitud" => trim($local[0][3]),
                                    "correo" => trim($local[0][4]),
                                    "telefono" => trim($local[0][5]),
                                    "fecha_registro" => date("Y-m-d H:i:s"),
                                    "estado" => '1'
                                );
                                $this->Local_model->insert($array_local);
                                $id_local = $this->db->insert_id();

                            } else {
                                $id_local = $locales->id;
                            }
                    }


                    if (($id_cate_carta != null || $id_cate_carta != "") && ($datos[$i][1] != null || $datos[$i][1] != "") &&
                        ($datos[$i][3] != null || $datos[$i][3] != "") && ($datos[$i][2] != null || $datos[$i][2] != "")){
                        $array_carta = array(
                            "id_categoria_carta" => $id_cate_carta,
                            "id_local" => $id_local,
                            "nombre" => trim($datos[$i][1]),
                            "precio" => trim($datos[$i][3]),
                            "descripcion" => trim($datos[$i][2]),
                            "fecha_registro" => date("Y-m-d H:i:s"),
                            "estado" => "1",
                        );

                        $this->Carta_model->insert($array_carta);
                        $id_carta = $this->db->insert_id();
                    }


                    if (($datos[$i][5] != null || $datos[$i][5] != "") && ($datos[$i][6] != null || $datos[$i][6] != "") ) {

                    for ($p = 5; $p <= 8; $p++) {
                        if ($p == 5) {
                            $tipo = 1;
                            $plataforma = 1;
                        } elseif($p == 6) {
                            $tipo = 2;
                            $plataforma = 1;
                        } elseif($p == 7) {
                            $tipo = 1;
                            $plataforma= 2;
                        } elseif($p == 8) {
                            $tipo = 2;
                            $plataforma= 2;
                        }

                        $array_foto_carta = array(
                            "id_carta" => $id_carta,
                            "foto" => 'upload/carta/' . date("Y-m-d").'/'.trim($datos[$i][$p]),
                            "tipo" => $tipo,
                            "plataforma" => $plataforma,
                            "fecha_registro" => date("Y-m-d H:i:s"),
                            "estado" => "1"

                        );
                        $this->Foto_carta_model->insert($array_foto_carta);
                     }
                    }

                }

                $this->alert($this->lang->line('mensaje_carta_carga_masiva'), site_url($this->config->item('path_backend') . '/Carta/carga_masiva'));

            }

            $empresas = array(
                "select" => "*", "where" => "estado = 1"
            );

            $empresa = $this->Empresa_model->search($empresas);


            $arrayMigaPan = array(
                array("nombre" => "Carta", "url" => site_url($this->config->item('path_backend') . '/Carta')), array("nombre" => "Carga Masiva", 'active' => true));

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['tituloPagina'] = "Carga Masiva";
            $this->arrayVista['empresas'] = $empresa;

            $this->arrayVista['vista'] = 'backend/carta/carga_masiva_view';
            $this->cargarVistaBackend();
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function carga_imagenes()
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {

                $img = $this->input->post("img");
                $pathArchivos = "upload/archivos/zip/carta/" . date("Y-m-d");
                $pathImagenes = "upload/carta/" . date("Y-m-d");

                if (!is_dir($pathArchivos))
                    mkdir($pathArchivos, 0775, true);
                if ($_FILES["img"]["size"] > 0) {
                    $nombreArchivoImagenes = "img";
                    $resultadoImagenes = $this->subirArchivo($nombreArchivoImagenes, $pathArchivos,"",'zip', 2024);
                    $img = "";

                    if (array_key_exists("upload_data", $resultadoImagenes)) {
                        if (isset($resultadoImagenes["upload_data"]["file_name"])) {
                            $zip = new ZipArchive;
                            $file = $resultadoImagenes['upload_data']['full_path'];
                            if ($zip->open($file) === TRUE) {
                                $zip->extractTo($pathImagenes);
                            }
                            $zip->close();
                        }
                    }
                }
                $this->alert($this->lang->line('mensaje_carga_fotos'), site_url($this->config->item('path_backend') . '/Carta/carga_masiva'));
            }
        }
    }
}
