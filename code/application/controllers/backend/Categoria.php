<?php
/**
 * Created by PhpStorm.
 * User: Mediabyte
 * Date: 02/05/2016
 * Time: 03:39 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoria extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();


        $this->load->model("Categoria_model");
        $this->lang->load('backend_error_lang', 'spanish');
        $this->lang->load('backend_mensajes_lang', 'spanish');
        $this->load->helper('url');


    }

    public function listar($start = 0)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            $params = array(
                "select" => "*",
                "order"=> "fecha_registro desc"
            );
            $total_categoria = $this->Categoria_model->total_records($params);

            $categoria = $this->Categoria_model->search_data($params, $start, $this->elementoPorPagina);
            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Categoria'), $total_categoria);


            $arrayMigaPan = array(
                array("nombre" => "Promocion" ,"url"=>site_url($this->config->item('path_backend') . '/Promocion/listar') ),
                array("nombre" => "Categoría" ),
                array("nombre" => "Listar", 'active' => true));

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['tituloPagina'] = "Lista de Categorías";
            $this->arrayVista['categorias'] = $categoria;
            $this->arrayVista['vista'] = 'backend/categoria/listar_view';
            $this->cargarVistaBackend();
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function buscar($start = 0)
    {
        if ($this->session->userdata('logged_in') === TRUE) {

            $nombre = $this->input->post('nombre');

            $estado = $this->input->post('estado');
            $where = "";


            if ($nombre != "") {
                $this->arrayVista['nombre'] = $nombre;
                if ($where != "") {
                    $where .= " AND nombre LIKE '%" . $nombre . "%' ";
                } else {
                    $where = "nombre LIKE '%" . $nombre . "%' ";
                }
            }


            if ($estado != "") {
                $this->arrayVista['estado'] = $estado;
                if ($where != "")
                    $where .= " AND estado = $estado";
                else
                    $where .= "estado = $estado";
            }

            if ($where == "") {
                $params = array(
                    "select" => "*",
                    "order"=> "fecha_registro desc"
                );
            } else {
                $params = array(
                    "select" => "*",
                    "where" => $where,
                    "order"=> "fecha_registro desc"
                );
            }

            $total_categoria = $this->Categoria_model->total_records($params);

            $arrayMigaPan = array(
                array("nombre" => "Promocion" ,"url"=>site_url($this->config->item('path_backend') . '/Promocion/listar') ),
                array("nombre" => "Categoría" ),
                array("nombre" => "Listar", 'active' => true));

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $categoria = $this->Categoria_model->search_data($params, $start, $this->elementoPorPagina);
            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Categoria/buscar'), $total_categoria);
            $this->arrayVista['tituloPagina'] = "Lista de Categorías";
            $this->arrayVista['categorias'] = $categoria;
            $this->arrayVista['vista'] = 'backend/categoria/listar_view';
            $this->cargarVistaBackend();

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function agregar()
    {
        if ($this->session->userdata('logged_in') === TRUE) {

            if ($this->input->post()) {

                $nombre = $this->input->post("nombre");
                $descripcion = $this->input->post("descripcion");

              //  $foto = $this->input->post("foto");

                $estado = $this->input->post("estado");


               /* $pathImagen = "./upload/categoria_promocion/" . date("Y-m-d");


                if (!is_dir($pathImagen))
                    mkdir($pathImagen, 0775, true);
                if ($_FILES["foto"]["size"] > 0) {
                    $nombreArchivoImagen = "foto";
                    $resultadoFoto = $this->subirArchivo($nombreArchivoImagen, $pathImagen, 'jpeg|jpg|png', 2024);
                    $foto = "";
                    if (array_key_exists("upload_data", $resultadoFoto)) {
                        if (isset($resultadoFoto["upload_data"]["file_name"]))

                            $foto = $resultadoFoto["upload_data"]["orig_name"];
                            $foto = $pathImagen."/".$foto;
                           // $foto = base_url() . "files/img/categoria/" . date("Y-m-d") . '/' . $resultadoFoto["upload_data"]["file_name"];
                    } else {
                        $this->session->set_flashdata('error', $this->lang->line('error_tamaño_imagen'));
                    }
                }*/

                $data_categoria = array(

                    'nombre' => $nombre,
                    'descripcion' => $descripcion,
                  //  'foto' => $foto,
                    'fecha_registro' => date("Y-m-d H:i:s"),
                    'estado' => $estado
                );

                if (isset($data_categoria)) {
                    try {
                        $this->Categoria_model->insert($data_categoria);
                        $this->alert($this->lang->line('mensaje_categoria_agregado'), site_url($this->config->item('path_backend') . '/Categoria'));
                    } catch (Exception $exception) {
                        $this->alert($this->lang->line('error_categoria_agregar'));
                    }
                }

            }

            $arrayMigaPan = array(
                array("nombre" => "Promocion" ,"url"=>site_url($this->config->item('path_backend') . '/Promocion') ),
                array("nombre" => "Categoria" ,"url"=>site_url($this->config->item('path_backend') . '/Categoría') ),
                array("nombre" => "Agregar", 'active' => true));

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['tituloPagina'] = "Agregar Categoría";

            $this->arrayVista['vista'] = 'backend/categoria/agregar_view';
            $this->cargarVistaBackend();
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function editar($id_categoria)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {
                $nombre = $this->input->post("nombre");
                $descripcion = $this->input->post("descripcion");

           //     $foto = $this->input->post("foto");
              //  $foto_antiguo= $this->input->post("foto_antiguo");
                $estado = $this->input->post("estado");
             /*   $pathImagen = "./upload/categoria_promocion/" . date("Y-m-d");


                if (!is_dir($pathImagen))
                    mkdir($pathImagen, 0775, true);
                if ($_FILES["foto"]["size"] > 0) {
                    $nombreArchivoImagen = "foto";
                    $resultadoFoto = $this->subirArchivo($nombreArchivoImagen, $pathImagen, 'jpeg|jpg|png', 2024);
                    $foto = "";
                    if (array_key_exists("upload_data", $resultadoFoto)) {
                        if (isset($resultadoFoto["upload_data"]["file_name"]))

                            $foto = $resultadoFoto["upload_data"]["orig_name"];
                            $foto = $pathImagen."/".$foto;
                          //  $foto = base_url() . "files/img/categoria/" . date("Y-m-d") . '/' . $resultadoFoto["upload_data"]["file_name"];
                    } else {
                        $this->session->set_flashdata('error', $this->lang->line('error_tamaño_imagen'));
                    }



                }else{
                    $foto =  $foto_antiguo;

                }*/
                $data_categoria = array(

                    'nombre' => $nombre,
                    'descripcion' => $descripcion,
                   // 'foto' => $foto,
                    'fecha_modificacion' => date("Y-m-d H:i:s"),
                    'estado' => $estado
                );


                if ($this->Categoria_model->update($id_categoria, $data_categoria)) {


                    $this->alert($this->lang->line('mensaje_categoria_editado'), site_url($this->config->item('path_backend') . '/Categoria'));

                }
            }

            $categoria = array(
                "select" => "*",
                "where" => "id = '" . $id_categoria . "'"
            );


            $categoria = $this->Categoria_model->get_search_row($categoria);


            $arrayMigaPan = array(
                array("nombre" => "Promocion" ,"url"=>site_url($this->config->item('path_backend') . '/Promocion') ),
                array("nombre" => "Categoria" ,"url"=>site_url($this->config->item('path_backend') . '/Categoría') ),
                array("nombre" => "Editar", 'active' => true));
            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['tituloPagina'] = "Editar Categoría";
         //   $this->arrayVista['fecha'] = $fecha_foto;
            $this->arrayVista['categoria'] = $categoria;
            $this->arrayVista['vista'] = 'backend/categoria/editar_view';
            $this->cargarVistaBackend();
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }

    }

    public function export()
    {
        if ($this->session->userdata('logged_in') === TRUE) {

            $nombre = $this->input->get('nombre');
            $estado = $this->input->get('estado');


            $where = "";


            if ($nombre != "" ) {

                $this->arrayVista['nombre'] = $nombre;
                if ($where != "") {
                    $where .= " AND nombre LIKE '%" . $nombre . "%' ";
                } else {
                    $where = "nombre LIKE '%" . $nombre . "%' ";
                }
            }



            if ($estado != "" ) {
                $this->arrayVista['estado'] = $estado;
                if ($where != "")
                    $where .= " AND estado = $estado";
                else
                    $where .= "estado = $estado";
            }

            if ($where == "") {
                $params = array(
                    "select" => "*",
                    "order" => "fecha_registro desc"
                );
            } else {
                $params = array(
                    "select" => "*",
                    "where" => $where,
                    "order" => "fecha_registro desc"
                );
            }

            $categorias = $this->Categoria_model->search($params);

          /*  $this->load->library('excel');
            $this->excel->setActiveSheetIndex(0);
            $this->excel->getActiveSheet()->setTitle("Categoria");


            $this->excel->getActiveSheet()->setCellValue('A1', 'Nombre');
            $this->excel->getActiveSheet()->setCellValue('B1', 'Descripción');
            $this->excel->getActiveSheet()->setCellValue('C1', 'Foto');
            $this->excel->getActiveSheet()->setCellValue('D1', 'Fecha de Registro');
            $this->excel->getActiveSheet()->setCellValue('E1', 'Estado');

            for($i=65; $i<=69; $i++) {
                $letra = chr($i);
                $this->excel->getActiveSheet()->getStyle($letra.'1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }

            for($i=65; $i<=69; $i++) {
                $letra = chr($i);
                $this->excel->getActiveSheet()->getStyle($letra.'1')->getFont()->setBold(true);
            }



            for ($col = ord('A'); $col <= ord('E'); $col++) {

                $this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);

                $this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
                $this->excel->getActiveSheet()->getStyle(chr($col))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }*/
            //retrive contries table data
            $data = array();
            foreach ($categorias as $categoria) {

                $estado="";
                $fecha="";

                if ($categoria->estado== 1){
                    $estado = "Activo";
                }else{
                    $estado = "Inactivo";
                }

                if (isset($categoria->foto)){
                    //$fecha = $this->fechaImagen($categoria->foto);
                }

              /*  if ($fecha = null){
                    $fecha = "---";
                }*/


                array_push($data, array(
                    utf8_decode("Nombre") => isset($categoria->nombre)?utf8_decode($categoria->nombre):"-",
                    utf8_decode("Descripción") => isset($categoria->descripcion)?utf8_decode($categoria->descripcion):"-",
                   // utf8_decode("Foto") =>  isset($categoria->foto)?substr(base_url(),0,-2)  .substr($categoria->foto,1):"-",
                    utf8_decode("Fecha de Registro") => date("d/m/Y h:i:s a", strtotime($categoria->fecha_registro)),
                    utf8_decode("Estado") => $estado,
                ));
            }

            function filterData(&$str)
            {
                $str = preg_replace("/\t/", "\\t", $str);
                $str = preg_replace("/\r?\n/", "\\n", $str);
                if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
            }

            $fileName = "Categoria de Promociones-". date('YmdHis') . ".xls";

            header("Content-Disposition: attachment; filename=\"$fileName\"");
            header("Content-Type: application/vnd.ms-excel, charset=utf-8");

            $flag = false;

            foreach($data as $row) {
                // print_r( array_values($row))."<br>";
                if(!$flag) {

                    echo implode("\t", array_keys($row)) . "\n";
                    $flag = true;
                }

                array_walk($row, 'filterData');
                echo implode("\t", array_values($row)) . "\n";
            }

            exit;

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }
}
