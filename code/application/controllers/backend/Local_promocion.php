<?php
/**
 * Created by PhpStorm.
 * User: Mediabyte
 * Date: 05/05/2016
 * Time: 02:18 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Local_promocion extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
  
        $this->load->model("Local_promocion_model");
        $this->load->model("Local_model");
        $this->lang->load('backend_error_lang', 'spanish');
        $this->lang->load('backend_mensajes_lang', 'spanish');
        $this->load->helper('url');


    }

    public function listar( $start = 0)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            $id_promocion = $this->input->post('id_promocion');
            $join = array(
                'tbl_local as L,L.id=tbl_local_promocion.id_local'
            );
            $params = array(
                "select" => "tbl_local_promocion.id,tbl_local_promocion.fecha_registro,tbl_local_promocion.estado,L.nombre",
                "where" => "tbl_local_promocion.id_promocion = '" . $id_promocion . "' and tbl_local_promocion.estado != ".ESTADO_ELIMINADO,
                "join" => $join,
                "order"=> "tbl_local_promocion.fecha_registro desc"
            );
            $total_local_promocion = $this->Local_promocion_model->total_records($params);

            $local_promocion = $this->Local_promocion_model->search_data($params, $start, $this->elementoPorPagina);

            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Local_Promocion'), $total_local_promocion);
            $this->arrayVista['tituloPagina'] = "Lista de Locales";
            $this->arrayVista['promocion'] = $id_promocion;
            $this->arrayVista['local_promocion'] = $local_promocion;
            /* $this->arrayVista['vista'] = 'backend/foto_beneficio/listar_view';*/

            $this->cargarVistaListaAjax('backend/local_promocion/listar_view');

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function buscar($start = 0)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            $id_promocion = $this->input->post('id_promocion');
            $local = $this->input->post('local');

            $estado = $this->input->post('estado');
            $where = "";


            if ($local != "") {
                $this->arrayVista['local'] = $local;
                if ($where != "")
                    $where .= " AND L.nombre  LIKE '%" . $local . "%'";

                else
                    $where .= "L.nombre LIKE '%" . $local . "%'";
            }

            if ($estado != "") {
                $this->arrayVista['estado'] = $estado;
                if ($where != "")
                    $where .= " AND tbl_local_promocion.estado = $estado";
                else
                    $where .= "tbl_local_promocion.estado = $estado";
            }else{
                if ($where != "")
                    $where .= " AND tbl_local_promocion.estado !=".ESTADO_ELIMINADO;
                else
                    $where .= "tbl_local_promocion.estado !=".ESTADO_ELIMINADO;
            }

            $join = array(
                'tbl_local as L,L.id=tbl_local_promocion.id_local'
            );
            if ($where == "") {

                $params = array(
                    "select" => "tbl_local_promocion.id,tbl_local_promocion.fecha_registro,tbl_local_promocion.estado,L.nombre",
                    "where" => "tbl_local_promocion.id_promocion = '" . $id_promocion . "'",
                    "join" => $join,
                    "order"=> "tbl_local_promocion.fecha_registro desc"
                );
            } else {
                $params = array(
                    "select" => "tbl_local_promocion.id,tbl_local_promocion.fecha_registro,tbl_local_promocion.estado,L.nombre",
                    "where" => "tbl_local_promocion.id_promocion = '" . $id_promocion . "' AND " . $where,
                    "join" => $join,
                    "order"=> "tbl_local_promocion.fecha_registro desc"

                );
            }

            $total_local_promocion = $this->Local_promocion_model->total_records($params);

            $local_promocion = $this->Local_promocion_model->search_data($params, $start, $this->elementoPorPagina);
            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Local_Promocion/buscar'), $total_local_promocion);
            $this->arrayVista['tituloPagina'] = "Lista de Locales";
            $this->arrayVista['promocion'] = $id_promocion;
            $this->arrayVista['local_promocion'] = $local_promocion;

            $this->cargarVistaListaAjax('backend/local_promocion/listar_view');


        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function agregar($id_promocion)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {
                $id_local = $this->input->post("local");

                $data_local_promocion = array(
                    'id_promocion' => $id_promocion,
                    'id_local' => $id_local,
                    'fecha_registro' => date("Y-m-d H:i:s"),
                    'estado' => ESTADO_ACTIVO
                );

                if (isset($data_local_promocion)) {
                    try {
                        $this->Local_promocion_model->insert($data_local_promocion);
                        $this->alert($this->lang->line('mensaje_local_agregado'), site_url($this->config->item('path_backend') . '/Promocion/editar/' . $id_promocion . '/L'));
                    } catch (Exception $exception) {
                        $this->alert($this->lang->line('error_local_agregar'));
                    }
                }

            }

            $local = array(
                "select" => "*",
                "where" => "estado = 1"
            );
            $local = $this->Local_model->search($local);
            $this->arrayVista['promocion'] = $id_promocion;
            $this->arrayVista['local'] = $local;
            $this->arrayVista['tituloPagina'] = "Agregar Local";
            $this->arrayVista['vista'] = 'backend/local_promocion/agregar_view';
            $this->cargarVistaBackend();


        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function editar($id_promocion, $id_local_promocion)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {
                $id_local = $this->input->post("local");
                $estado = $this->input->post("estado");

                $data_local_promocion = array(
                    'id_local' => $id_local,
                    'fecha_modificacion' => date("Y-m-d H:i:s"),
                    'estado' => $estado
                );

                if ($this->Local_promocion_model->update($id_local_promocion, $data_local_promocion)) {


                    $this->alert($this->lang->line('mensaje_local_editado'), site_url($this->config->item('path_backend') . '/Promocion/editar/' . $id_promocion . '/L'));

                }
            }

            $local = array(
                "select" => "*",
                "where" => "estado = 1"
            );


            $local_promocion = array(
                "select" => "*",
                "where" => "id = '" . $id_local_promocion . "'"
            );


            $local = $this->Local_model->search($local);

            $local_promocion = $this->Local_promocion_model->get_search_row($local_promocion);
            $this->arrayVista['promocion'] = $id_promocion;
            $this->arrayVista['locales'] = $local;
            $this->arrayVista['local_promocion'] = $local_promocion;
            $this->arrayVista['tituloPagina'] = "Editar Local";
            $this->arrayVista['vista'] = 'backend/local_promocion/editar_view';
            $this->cargarVistaBackend();


        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function editar_estado(){
        if ($this->session->userdata('logged_in') === TRUE) {

            if ($this->input->post()) {
                $id_local = $this->input->post("id_local");
                $estado = $this->input->post("estado");


                $data = array(
                    'fecha_modificacion' => date("Y-m-d H:i:s"),
                    'estado' => $estado
                );

                if (  $this->Local_promocion_model->update($id_local, $data)){
                    $result = array("cod"=>"1",
                        "mensaje"=>$this->lang->line('mensaje_local_promocion_eliminado')
                    );
                }else{
                    $result = array("cod"=>"0",
                        "mensaje"=>$this->lang->line('error_local_promocion_eliminar')
                    );
                }
            }
        }else{
            $result = array("cod"=>"0",
                "mensaje"=>$this->lang->line('error_local_promocion_eliminar')
            );
        }
        echo json_encode($result);
    }

    public function export()
    {
        if ($this->session->userdata('logged_in') === TRUE) {

            $id_promocion = $this->input->get('id_promocion');
            $local = $this->input->get('local');
            $estado = $this->input->get('estado');


            $where = "";

            if ($local != "") {
                $this->arrayVista['local'] = $local;
                if ($where != "")
                    $where .= " AND L.nombre  LIKE '%" . $local . "%'";

                else
                    $where .= "L.nombre LIKE '%" . $local . "%'";
            }

            if ($estado != "") {
                $this->arrayVista['estado'] = $estado;
                if ($where != "")
                    $where .= " AND tbl_local_promocion.estado = $estado";
                else
                    $where .= "tbl_local_promocion.estado = $estado";
            }else{
                if ($where != "")
                    $where .= " AND tbl_local_promocion.estado !=".ESTADO_ELIMINADO;
                else
                    $where .= "tbl_local_promocion.estado !=".ESTADO_ELIMINADO;
            }

            $join=array(
                'tbl_local as L,L.id=tbl_local_promocion.id_local','tbl_promocion as P,P.id=tbl_local_promocion.id_promocion'
            );
            if($where==""){

                $params = array(
                    "select" => "tbl_local_promocion.fecha_registro,tbl_local_promocion.estado,L.nombre,
                    P.nombre as promocion",
                    "where" => "tbl_local_promocion.id_promocion = '" . $id_promocion . "'",
                    "join" => $join,
                    "order"=> "tbl_local_promocion.fecha_registro desc"
                );
            }
            else{
                $params =    array(
                    "select" => "tbl_local_promocion.fecha_registro,tbl_local_promocion.estado,L.nombre,
                    P.nombre as promocion",
                    "where" => "tbl_local_promocion.id_promocion = '" . $id_promocion . "' AND ".$where,
                    "join" => $join,
                    "order"=> "tbl_local_promocion.fecha_registro desc"

                );
            }
            $locales = $this->Local_promocion_model->search($params);

            
            //retrive contries table data
            $data = array();
            foreach ($locales as $local) {



                if ($local->estado== 1){
                    $estado = "Seleccionar";
                }else{
                    $estado = "Quitar";
                }


                array_push(
                    $data,
                    array(
                        utf8_decode("Promoción") => isset($local->promocion)?utf8_decode($local->promocion):"-",
                        utf8_decode("Local") => isset($local->nombre)?utf8_decode($local->nombre):"-",
                        utf8_decode("Fecha de Registro") => isset($local->fecha_registro)?date("d/m/Y h:i:s a", strtotime($local->fecha_registro)):"-",
                        utf8_decode("Estado") => isset($estado)?$estado:"-",
                    )
                );
            }
            //Fill data
            function filterData(&$str)
            {
                $str = preg_replace("/\t/", "\\t", $str);
                $str = preg_replace("/\r?\n/", "\\n", $str);
                if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
            }             
                
            $fileName = "Local promoción-". date('YmdHis') . ".xls";
                
            header("Content-Disposition: attachment; filename=\"$fileName\"");
            header("Content-Type: application/vnd.ms-excel, charset=utf-8");
                
            $flag = false;

            foreach($data as $row) {
                // print_r( array_values($row))."<br>";
                if(!$flag) {
                    
                    echo implode("\t", array_keys($row)) . "\n";
                    $flag = true;
                }
                    
                array_walk($row, 'filterData');
                echo implode("\t", array_values($row)) . "\n";
            }
                
            exit;
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }
}
