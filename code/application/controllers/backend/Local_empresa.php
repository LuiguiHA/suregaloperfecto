<?php
/**
 * Created by PhpStorm.
 * User: Mediabyte
 * Date: 05/05/2016
 * Time: 02:18 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Local_empresa extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model("Local_model");
        $this->lang->load('backend_error_lang', 'spanish');
        $this->lang->load('backend_mensajes_lang', 'spanish');
        $this->load->helper('url');


    }

    public function listar($start = 0)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            $id_empresa =$this->input->post('id_empresa');
            $params = array(
                "select" => "*",
                "where" => "id_empresa = '" . $id_empresa . "'",
                "order"=> "fecha_registro desc"

            );
            $total_local_empresa = $this->Local_model->total_records($params);

            $local_empresa = $this->Local_model->search_data($params, $start, $this->elementoPorPagina);

            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Local_Empresa'), $total_local_empresa);
            $this->arrayVista['tituloPagina'] = "Lista de Locales";
            $this->arrayVista['empresa'] = $id_empresa;
            $this->arrayVista['local_empresa'] = $local_empresa;
            /* $this->arrayVista['vista'] = 'backend/foto_beneficio/listar_view';*/

            $this->cargarVistaListaAjax('backend/local_empresa/listar_view');

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function buscar($start = 0)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            $id_empresa =$this->input->post('id_empresa');
            $nombre = $this->input->post('nombre');
            $direccion = $this->input->post('direccion');
            $estado = $this->input->post('estado');
            $where = "";


            if ($nombre != "") {
                $this->arrayVista['nombre'] = $nombre;
                if ($where != "")
                    $where .= " AND nombre  LIKE '%" . $nombre . "%'";

                else
                    $where .= "nombre LIKE '%" . $nombre . "%'";
            }

            if ($direccion != "") {
                $this->arrayVista['direccion'] = $direccion;
                if ($where != "")
                    $where .= " AND direccion  LIKE '%" . $direccion . "%'";

                else
                    $where .= "direccion LIKE '%" . $direccion . "%'";
            }

            if ($estado != "") {
                $this->arrayVista['estado'] = $estado;
                if ($where != "")
                    $where .= " AND estado = $estado";
                else
                    $where .= "estado = $estado";
            }


            if ($where == "") {

                $params = array(
                    "select" => "*",
                    "where" => "id_empresa = '" . $id_empresa . "'",
                    "order"=> "fecha_registro desc"

                );
            } else {
                $params = array(
                    "select" => "*",
                    "where" => "id_empresa = '" . $id_empresa . "' AND " . $where,
                    "order"=> "fecha_registro desc"
                );
            }

            $total_local_empresa = $this->Local_model->total_records($params);

            $local_empresa = $this->Local_model->search_data($params, $start, $this->elementoPorPagina);
            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Local_Empresa/buscar'), $total_local_empresa);
            $this->arrayVista['tituloPagina'] = "Lista de Locales";
            $this->arrayVista['empresa'] = $id_empresa;
            $this->arrayVista['local_empresa'] = $local_empresa;

            $this->cargarVistaListaAjax('backend/local_empresa/listar_view');


        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function agregar($id_empresa)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {
                $nombre = $this->input->post("nombre");
                $direccion = $this->input->post("direccion");
                $latitud = $this->input->post("latitud");
                $longitud = $this->input->post("longitud");
                $correo = $this->input->post("correo");
                $telefono = $this->input->post("telefono");
                $localidad= $this->input->post("localidad");
                $meses= $this->input->post("meses");
                $estado = $this->input->post("estado");

                $data_local_empresa = array(
                    'id_empresa' =>$id_empresa,
                    'nombre' => $nombre,
                    'direccion' => $direccion,
                    'latitud' => $latitud,
                    'longitud' => $longitud,
                    'correo' => $correo,
                    'telefono' => $telefono,
                    'localidad' => $localidad,
                    'meses_reserva' => $meses,
                    'fecha_registro' => date("Y-m-d H:i:s"),
                    'estado' => $estado
                );

                if (isset($data_local_empresa)) {
                    try {
                        $this->Local_model->insert($data_local_empresa);
                        $this->alert($this->lang->line('mensaje_local_agregado'), site_url($this->config->item('path_backend') . '/Empresa/editar/' . $id_empresa . '/L'));
                    } catch (Exception $exception) {
                        $this->alert($this->lang->line('error_local_agregar'));
                    }
                }

            }


            $this->arrayVista['empresa'] = $id_empresa;

            $this->arrayVista['tituloPagina'] = "Agregar Local";
            $this->arrayVista['vista'] = 'backend/local_empresa/agregar_view';
            $this->cargarVistaBackend();


        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function editar($id_empresa, $id_local)
    {

        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {
                $nombre = $this->input->post("nombre");
                $direccion = $this->input->post("direccion");
                $latitud = $this->input->post("latitud");
                $longitud = $this->input->post("longitud");
                $correo = $this->input->post("correo");
                $telefono = $this->input->post("telefono");
                $localidad= $this->input->post("localidad");
                $meses= $this->input->post("meses");
                $estado = $this->input->post("estado");

                $data_local_empresa = array(

                    'nombre' => $nombre,
                    'direccion' => $direccion,
                    'latitud' => $latitud,
                    'longitud' => $longitud,
                    'correo' => $correo,
                    'telefono' => $telefono,
                    'localidad' => $localidad,
                    'meses_reserva' => $meses,
                    'fecha_modificacion' => date("Y-m-d H:i:s"),
                    'estado' => $estado
                );

                if ($this->Local_model->update($id_local, $data_local_empresa)) {


                    $this->alert($this->lang->line('mensaje_local_editado'), site_url($this->config->item('path_backend') . '/Empresa/editar/' . $id_empresa . '/L'));

                }
            }

            $local = array(
                "select" => "*",
                "where" => "id = '" . $id_local . "'"
            );

            $local_empresa = $this->Local_model->get_search_row($local);

            $this->arrayVista['empresa'] = $id_empresa;
            $this->arrayVista['local_empresa'] = $local_empresa;
            $this->arrayVista['tituloPagina'] = "Editar Local";
            $this->arrayVista['vista'] = 'backend/local_empresa/editar_view';
            $this->cargarVistaBackend();


        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function export()
    {
        if ($this->session->userdata('logged_in') === TRUE) {

            $id_empresa = $this->input->get('id_empresa');
            $nombre = $this->input->get('nombre');
            $direccion = $this->input->get('direccion');
            $estado = $this->input->get('estado');


            $where = "";

            if ($nombre != "") {

                if ($where != "")
                    $where .= " AND tbl_local.nombre  LIKE '%" . $nombre . "%'";

                else
                    $where .= "tbl_local.nombre LIKE '%" . $nombre . "%'";
            }

            if ($direccion != "") {

                if ($where != "")
                    $where .= " AND tbl_local.direccion  LIKE '%" . $direccion . "%'";

                else
                    $where .= "tbl_local.direccion LIKE '%" . $direccion . "%'";
            }

            if ($estado != "") {

                if ($where != "")
                    $where .= " AND tbl_local.estado = $estado";
                else
                    $where .= "tbl_local.estado = $estado";
            }

            $join=array(
                'tbl_empresa as E,E.id=tbl_local.id_empresa'
            );
            if($where==""){

                $params = array(
                    "select" => "E.nombre as empresa,tbl_local.nombre,tbl_local.direccion,tbl_local.latitud,tbl_local.longitud,
                    tbl_local.correo,tbl_local.telefono,tbl_local.localidad,tbl_local.fecha_registro,tbl_local.estado",
                    "where" => "tbl_local.id_empresa = '" . $id_empresa . "'",
                    "join" => $join,
                    "order"=> "tbl_local.fecha_registro desc"
                );
            }
            else{
                $params =    array(
                    "select" => "E.nombre as empresa,tbl_local.nombre,tbl_local.direccion,tbl_local.latitud,tbl_local.longitud,
                    tbl_local.correo,tbl_local.telefono,tbl_local.localidad,tbl_local.fecha_registro,tbl_local.estado",
                    "where" => "tbl_local.id_empresa = '" . $id_empresa . "' AND ".$where,
                    "join" => $join,
                    "order"=> "tbl_local.fecha_registro desc"

                );
            }
            $locales = $this->Local_model->search($params);

            $this->load->library('excel');
            $this->excel->setActiveSheetIndex(0);
            $this->excel->getActiveSheet()->setTitle("Locales");

            $this->excel->getActiveSheet()->setCellValue('A1', 'Empresa');
            $this->excel->getActiveSheet()->setCellValue('B1', 'Nombre');
            $this->excel->getActiveSheet()->setCellValue('C1', 'Dirección');
            $this->excel->getActiveSheet()->setCellValue('D1', 'Latitud');
            $this->excel->getActiveSheet()->setCellValue('E1', 'Longitud');
            $this->excel->getActiveSheet()->setCellValue('F1', 'Correo');
            $this->excel->getActiveSheet()->setCellValue('G1', 'Telefono');
            $this->excel->getActiveSheet()->setCellValue('H1', 'Localidad');
            $this->excel->getActiveSheet()->setCellValue('I1', 'Fecha de Registro');
            $this->excel->getActiveSheet()->setCellValue('J1', 'Estado');


            for($i=65; $i<=74; $i++) {
                $letra = chr($i);
                $this->excel->getActiveSheet()->getStyle($letra.'1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }

            for($i=65; $i<=74; $i++) {
                $letra = chr($i);
                $this->excel->getActiveSheet()->getStyle($letra.'1')->getFont()->setBold(true);
            }



            for ($col = ord('A'); $col <= ord('J'); $col++) {

                $this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);

                $this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
                $this->excel->getActiveSheet()->getStyle(chr($col))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }
            //retrive contries table data
            $data = array();
            foreach ($locales as $local) {



                if ($local->estado== 1){
                    $estado = "Activo";
                }else{
                    $estado = "Inactivo";
                }


                array_push(
                    $data, 
                    array(
                        utf8_decode("Empresa") => isset($local->empresa)?utf8_decode($local->empresa):"-",
                        utf8_decode("Nombre") => isset($local->nombre)?utf8_decode($local->nombre):"-",
                        utf8_decode("Dirección") => isset($local->direccion)?utf8_decode($local->direccion):"-",
                        utf8_decode("Latitud") => isset($local->latitud)?utf8_decode($local->latitud):"-",
                        utf8_decode("Longitud") => isset($local->longitud)?utf8_decode($local->longitud):"-",
                        utf8_decode("Correo") => isset($local->correo)?utf8_decode($local->correo):"-",
                        utf8_decode("Teléfono") => isset($local->telefono)?utf8_decode($local->telefono):"-",
                        utf8_decode("Localidad") => isset($local->localidad)?utf8_decode($local->localidad):"-",
                        utf8_decode("Fecha de Registro") => isset($local->fecha_registro)?date("d/m/Y h:i:s a", strtotime($local->fecha_registro)):"-",
                        utf8_decode("Estado") => isset($estado)?$estado:"-",
                ));
            }
            //Fill data
            function filterData(&$str)
            {
                $str = preg_replace("/\t/", "\\t", $str);
                $str = preg_replace("/\r?\n/", "\\n", $str);
                if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
            }             
                
            $fileName = "Locales de Empresa -". date('YmdHis') . ".xls";
                
            header("Content-Disposition: attachment; filename=\"$fileName\"");
            header("Content-Type: application/vnd.ms-excel, charset=utf-8");
                
            $flag = false;

            foreach($data as $row) {
                // print_r( array_values($row))."<br>";
                if(!$flag) {
                    
                    echo implode("\t", array_keys($row)) . "\n";
                    $flag = true;
                }
                    
                array_walk($row, 'filterData');
                echo implode("\t", array_values($row)) . "\n";
            }
                
            exit;
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }
}
