<?php
/**
 * Created by PhpStorm.
 * User: Mediabyte
 * Date: 05/05/2016
 * Time: 02:18 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Local_ubicacion extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model("Local_model");
        $this->load->model("Local_ubicacion_model", "Local_ubicacion_model");
        $this->lang->load('backend_error_lang', 'spanish');
        $this->lang->load('backend_mensajes_lang', 'spanish');
        $this->load->helper('url');

    }

    public function listar($start = 0)
    {
        if ($this->session->userdata('logged_in') === TRUE)
        {
            $id_local = $this->input->post('id_local');
            $id_empresa = $this->input->post('id_empresa');

            $params = array(
                "select" => "id,nombre,estado,fecha_registro", "where" => "id_local = '" . $id_local . "'", "order" => "fecha_registro desc"
            );

            $total_local_ubicacion = $this->Local_ubicacion_model->total_records($params);

            $local_ubicacion = $this->Local_ubicacion_model->search_data($params, $start, $this->elementoPorPagina);
            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Local_Ubicacion'), $total_local_ubicacion);
            $this->arrayVista['local'] = $id_local;
            $this->arrayVista['empresa'] = $id_empresa;
            $this->arrayVista['local_ubicacion'] = $local_ubicacion;
            $this->cargarVistaListaAjax('backend/local_ubicacion/listar_view');
        } else
        {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function buscar($start = 0)
    {

        $id_local = $this->input->post('id_local');
        $id_empresa = $this->input->post('id_empresa');
        $nombre = $this->input->post('nombre');
        $estado = $this->input->post('estado');
        $where = "";

        if ($nombre != "")
        {
            $this->arrayVista['nombre'] = $nombre;
            if ($where != "") $where .= " AND nombre  LIKE '%" . $nombre . "%'";

            else
                $where .= "nombre LIKE '%" . $nombre . "%'";
        }

        if ($estado != "")
        {
            $this->arrayVista['estado'] = $estado;
            if ($where != "") $where .= " AND estado = $estado"; else
                $where .= "estado = $estado";
        }


        if ($where == "")
        {

            $params = array(
                "select" => "id,nombre,estado,fecha_registro", "where" => "id_local = '" . $id_local . "'", "order" => "fecha_registro desc"

            );
        } else
        {
            $params = array(
                "select" => "id,nombre,estado,fecha_registro", "where" => "id_local = '" . $id_local . "' AND " . $where, "order" => "fecha_registro desc"
            );
        }

        $total_local_ubicacion = $this->Local_ubicacion_model->total_records($params);

        $local_ubicacion = $this->Local_ubicacion_model->search_data($params, $start, $this->elementoPorPagina);
        $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Local_Ubicacion/buscar'), $total_local_ubicacion);
        $this->arrayVista['tituloPagina'] = "Lista de Ubicación de locales";
        $this->arrayVista['local'] = $id_local;
        $this->arrayVista['empresa'] = $id_empresa;
        $this->arrayVista['local_ubicacion'] = $local_ubicacion;

        $this->cargarVistaListaAjax('backend/local_ubicacion/listar_view');

    }

    public function agregar_view($id_empresa, $id_local)
    {
        if ($this->session->userdata('logged_in') === TRUE)
        {
            $this->arrayVista['empresa'] = $id_empresa;
            $this->arrayVista['local'] = $id_local;
            $this->arrayVista['tituloPagina'] = "Agregar ubicación en locales ";
            $this->arrayVista['vista'] = 'backend/local_ubicacion/agregar_view';
            $this->cargarVistaBackend();

        } else
        {

            redirect(site_url($this->config->item('path_backend')));

        }
    }

    public function agregar($id_empresa, $id_local)
    {
        if ($this->session->userdata('logged_in') === TRUE)
        {
            $nombre = $this->input->post("nombre");
            $estado = $this->input->post("estado");

            $data_local_ubicacion = array(
                'id_local' => $id_local, 'nombre' => $nombre, 'estado' => $estado, 'fecha_registro' => date("Y-m-d H:i:s")
            );

            if (isset($data_local_ubicacion))
            {
                try
                {
                    $this->Local_ubicacion_model->insert($data_local_ubicacion);
                    $this->alert($this->lang->line('mensaje_local_ubicacion_agregado'), site_url($this->config->item('path_backend') . '/Local_Empresa/editar/' . $id_empresa . '/' . $id_local . '/U'));
                } catch (Exception $exception)
                {
                    $this->alert($this->lang->line('error_local_ubicacion_agregar'));
                }
            }

        } else
        {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function editar_view($id_empresa, $id_local, $id_local_ubicacion)
    {
        if ($this->session->userdata('logged_in') === TRUE)
        {
            $local_ubicacion = array(
                "select" => "id,nombre,estado", "where" => "id = '" . $id_local_ubicacion . "'"
            );

            $local_ubicacion = $this->Local_ubicacion_model->get_search_row($local_ubicacion);

            $this->arrayVista['local'] = $id_local;
            $this->arrayVista['empresa'] = $id_empresa;
            $this->arrayVista['id_local_ubicacion'] = $id_local_ubicacion;
            $this->arrayVista['local_ubicacion'] = $local_ubicacion;
            $this->arrayVista['tituloPagina'] = "Editar Ubicación del local";
            $this->arrayVista['vista'] = 'backend/local_ubicacion/editar_view';
            $this->cargarVistaBackend();

        } else
        {

            redirect(site_url($this->config->item('path_backend')));

        }
    }

    public function editar($id_empresa, $id_local, $id_local_ubicacion)
    {

        if ($this->session->userdata('logged_in') === TRUE)
        {
            $nombre = $this->input->post("nombre");
            $estado = $this->input->post("estado");

            $data_local_ubicacion = array(
                'nombre' => $nombre, 'estado' => $estado, 'fecha_modificacion' => date("Y-m-d H:i:s")
            );

            if ($this->Local_ubicacion_model->update($id_local_ubicacion, $data_local_ubicacion))
            {

                $this->alert($this->lang->line('mensaje_local_ubicacion_editado'), site_url($this->config->item('path_backend') . '/Local_Empresa/editar/'.$id_empresa.'/'.$id_local.'/U'));
            }

        } else
        {
            redirect(site_url($this->config->item('path_backend')));

        }
    }

    public function export()

    {
        if ($this->session->userdata('logged_in') === TRUE) {

            $id_local = $this->input->get('id_local');
            $id_empresa = $this->input->get('id_empresa');
            $nombre = $this->input->get('nombre');
            $estado = $this->input->get('estado');



            $where = "";

            if ($nombre != "") {
                $this->arrayVista['nombre'] = $nombre;
                if ($where != "")
                    $where .= " AND tbl_local_ubicacion.nombre  LIKE '%" . $nombre . "%'";

                else
                    $where .= "tbl_local_ubicacion.nombre LIKE '%" . $nombre . "%'";
            }

            if ($estado != "") {
                $this->arrayVista['estado'] = $estado;
                if ($where != "")
                    $where .= " AND tbl_local_ubicacion.estado = $estado";
                else
                    $where .= "tbl_local_ubicacion.estado = $estado";
            }


            if ($where == "")
            {
                $params = array(
                    "select" => "tbl_local_ubicacion.id,tbl_local_ubicacion.nombre,tbl_local_ubicacion.estado,tbl_local_ubicacion.fecha_registro,local.nombre  as nombre_local",
                    "join"  =>  array("tbl_local local, local.id = tbl_local_ubicacion.id_local"),
                    "where" => "tbl_local_ubicacion.id_local = '" . $id_local . "'",
                    "order" => "tbl_local_ubicacion.fecha_registro desc"

                );
            } else
            {
                $params = array(
                    "select" => "tbl_local_ubicacion.id,tbl_local_ubicacion.nombre,tbl_local_ubicacion.estado,tbl_local_ubicacion.fecha_registro,local.nombre  as nombre_local",
                    "join"  =>  array("tbl_local local, local.id = tbl_local_ubicacion.id_local"),
                    "where" => "tbl_local_ubicacion.id_local = '" . $id_local . "' AND " .$where,
                    "order" => "tbl_local_ubicacion.fecha_registro desc"
                );



            }

            $local_ubicacion = $this->Local_ubicacion_model->search($params);

            //retrive contries table data
            $data = array();
            foreach ($local_ubicacion as $ubicacion) {

                if ($ubicacion->estado== 1){
                    $estado = "Activo";
                }else{
                    $estado = "Inactivo";
                }


                array_push($data, array(
                    utf8_decode("Local") => isset($ubicacion->nombre_local)?utf8_decode($ubicacion->nombre_local):"-",
                    utf8_decode("Nombre") => isset($ubicacion->nombre)?utf8_decode($ubicacion->nombre):"-",
                    utf8_decode("Estado") => isset($estado)?$estado:"-",
                    utf8_decode("Fecha de Registro") => isset($ubicacion->fecha_registro)?date("d/m/Y h:i:s a", strtotime($ubicacion->fecha_registro)):"-",
                ));
            }
            //Fill data
            function filterData(&$str)
            {
                $str = preg_replace("/\t/", "\\t", $str);
                $str = preg_replace("/\r?\n/", "\\n", $str);
                if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
            }

            $fileName = "Ubicación de local-". date('YmdHis') . ".xls";

            header("Content-Disposition: attachment; filename=\"$fileName\"");
            header("Content-Type: application/vnd.ms-excel, charset=utf-8");

            $flag = false;

            foreach($data as $row) {
                // print_r( array_values($row))."<br>";
                if(!$flag) {

                    echo implode("\t", array_keys($row)) . "\n";
                    $flag = true;
                }

                array_walk($row, 'filterData');
                echo implode("\t", array_values($row)) . "\n";
            }

            exit;
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }


}
