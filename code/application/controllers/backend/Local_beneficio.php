<?php
/**
 * Created by PhpStorm.
 * User: Mediabyte
 * Date: 05/05/2016
 * Time: 02:18 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Local_beneficio extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
   
        $this->load->model("Local_beneficio_model");
        $this->load->model("Local_model");
        $this->load->model("Beneficio_model");
        $this->lang->load('backend_error_lang', 'spanish');
        $this->lang->load('backend_mensajes_lang', 'spanish');
        $this->load->helper('url');


    }

    public function listar($start=0)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            $id_beneficio = $this->input->post("id_beneficio");

            $beneficio = array(
                "select" => "tbl_beneficio.id,tbl_beneficio.tipo_locales",
                "where" => "id = '" . $id_beneficio . "'"
            );

            $beneficio = $this->Beneficio_model->get_search_row($beneficio);

            $join=array(
            'tbl_local as L,L.id=tbl_local_beneficio.id_local'
        );
        $params = array(
            "select" => "tbl_local_beneficio.id,tbl_local_beneficio.fecha_registro,tbl_local_beneficio.estado,L.nombre,tbl_local_beneficio.id_beneficio",
            "where" => "tbl_local_beneficio.id_beneficio = '" . $id_beneficio . "'",
            "join" =>$join,
            "order"=> "tbl_local_beneficio.fecha_registro desc"
        );
        $total_local_beneficio = $this->Local_beneficio_model->total_records($params);

        $local_beneficio = $this->Local_beneficio_model->search_data($params, $start, $this->elementoPorPagina);
        $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend').'/Local_Beneficio'), $total_local_beneficio);
        $this->arrayVista['tituloPagina'] = "Lista de Locales";
        $this->arrayVista['beneficio'] = $beneficio;
        $this->arrayVista['local_beneficio'] = $local_beneficio;
        /* $this->arrayVista['vista'] = 'backend/foto_beneficio/listar_view';*/

        $this->cargarVistaListaAjax('backend/local_beneficio/listar_view');
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function buscar($start = 0)
    {
        if ($this->session->userdata('logged_in') === TRUE) {

            $id_beneficio = $this->input->post("id_beneficio");
        $local =$this->input->post('local');

        $estado = $this->input->post('estado');
        $where = "";

            $beneficio = array(
                "select" => "tbl_beneficio.id,tbl_beneficio.tipo_locales",
                "where" => "id = '" . $id_beneficio . "'"
            );

            $beneficio = $this->Beneficio_model->get_search_row($beneficio);


        if ($local != "") {
            $this->arrayVista['local'] = $local;
            if ($where != "")
                $where .= " AND L.nombre  LIKE '%" . $local . "%'";

            else
                $where .= "L.nombre LIKE '%" . $local . "%'";
        }

        if ($estado != "") {
            $this->arrayVista['estado'] = $estado;
            if ($where != "")
                $where .= " AND tbl_local_beneficio.estado = $estado";
            else
                $where .= "tbl_local_beneficio.estado = $estado";
        }

        $join=array(
            'tbl_local as L,L.id=tbl_local_beneficio.id_local'
        );
        if($where==""){

            $params = array(
                "select" => "tbl_local_beneficio.id,tbl_local_beneficio.fecha_registro,tbl_local_beneficio.estado,L.nombre,tbl_local_beneficio.id_beneficio",
                "where" => "id_beneficio = '" . $id_beneficio . "'",
                "join" => $join,
                "order"=> "tbl_local_beneficio.fecha_registro desc"
            );
        }
        else{
            $params =    array(
                "select" => "tbl_local_beneficio.id,tbl_local_beneficio.fecha_registro,tbl_local_beneficio.estado,L.nombre,tbl_local_beneficio.id_beneficio",
                "where" => "id_beneficio = '" . $id_beneficio . "' AND ".$where,
                "join" => $join,
                "order"=> "tbl_local_beneficio.fecha_registro desc"

            );
        }

        $total_local_beneficio = $this->Local_beneficio_model->total_records($params);

        $local_beneficio = $this->Local_beneficio_model->search_data($params, $start, $this->elementoPorPagina);
        $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend').'/Local_Beneficio/buscar'), $total_local_beneficio);
        $this->arrayVista['tituloPagina'] = "Lista de Locales";
        $this->arrayVista['beneficio'] = $beneficio;
        $this->arrayVista['local_beneficio'] = $local_beneficio;

        $this->cargarVistaListaAjax('backend/local_beneficio/listar_view');


        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function agregar($id_beneficio)
    {



        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {
                $id_local = $this->input->post("local");


            $data_local_beneficio = array(
                'id_beneficio' => $id_beneficio,
                'id_local' => $id_local,
                'fecha_registro' => date("Y-m-d H:i:s"),
                'estado' => ESTADO_ACTIVO
            );

            if (isset($data_local_beneficio)) {
                try {
                    $this->Local_beneficio_model->insert($data_local_beneficio);
                    $this->alert($this->lang->line('mensaje_local_agregado'),  site_url($this->config->item('path_backend').'/Beneficio/editar/'.$id_beneficio.'/L'));
                } catch (Exception $exception) {
                    $this->alert($this->lang->line('error_local_agregar'));
                }
            }

        }

        $local = array(
            "select" => "tbl_local.id,tbl_local.nombre",
            "where" => "tbl_local.estado = 1 and tbl_local.id not in
        (select tbl_local_beneficio.id_local from tbl_local_beneficio where tbl_local_beneficio.id_beneficio = $id_beneficio )"

        );

        $local = $this->Local_model->search($local);
        $this->arrayVista['local'] = $local;
        $this->arrayVista['beneficio'] = $id_beneficio;
        $this->arrayVista['tituloPagina'] = "Agregar Local";
        $this->arrayVista['vista'] = 'backend/local_beneficio/agregar_view';
        $this->cargarVistaBackend();

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }

    }

    public function editar($id_beneficio,$id_local_beneficio)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
        if ($this->input->post()) {
            $id_local = $this->input->post("local");
            $estado = $this->input->post("estado");

            $data_local_beneficio = array(
                'id_local'=> $id_local,
                'fecha_modificacion' => date("Y-m-d H:i:s"),
                'estado' => $estado
            );

            if ($this->Local_beneficio_model->update($id_local_beneficio, $data_local_beneficio)) {

                $this->alert($this->lang->line('mensaje_local_editado'),  site_url($this->config->item('path_backend').'/Beneficio/editar/'.$id_beneficio.'/L'));

            }
        }

        $local = array(
            "select" => "*",
            "where" => "estado = 1"
        );

        $local_beneficio = array(
            "select" => "*",
            "where" => "id = '" . $id_local_beneficio . "'"
        );

        $local = $this->Local_model->search($local);

        $local_beneficio = $this->Local_beneficio_model->get_search_row($local_beneficio);
        $this->arrayVista['beneficio'] = $id_beneficio;
        $this->arrayVista['local'] = $local;
        $this->arrayVista['local_beneficio'] = $local_beneficio;
        $this->arrayVista['tituloPagina'] = "Editar Local";
        $this->arrayVista['vista'] = 'backend/local_beneficio/editar_view';
        $this->cargarVistaBackend();

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }

    }

    public function editar_estado(){
        if ($this->session->userdata('logged_in') === TRUE) {

            if ($this->input->post()) {
                $id_local = $this->input->post("id_local");
                $estado = $this->input->post("estado");
                $id_beneficio = $this->input->post("id_beneficio");

                $local_beneficio = array(
                    "select" => "tbl_local_beneficio.id",
                    "where" => "tbl_local_beneficio.id_beneficio = '" . $id_beneficio . "' and tbl_local_beneficio.estado = ".ESTADO_ACTIVO,
                );

                $local_beneficio_estado = $this->Local_beneficio_model->total_records($local_beneficio);

                if ($local_beneficio_estado > 1){
                    $data = array(
                        'fecha_modificacion' => date("Y-m-d H:i:s"),
                        'estado' => $estado
                    );

                    if ( $this->Local_beneficio_model->update($id_local, $data)){
                        $result = array("cod"=>"1",
                            "mensaje"=>$this->lang->line('mensaje_local_promocion_eliminado')
                        );
                    }else{
                        $result = array("cod"=>"0",
                            "mensaje"=>$this->lang->line('error_local_promocion_eliminar')
                        );
                    }
                }else{
                    $result = array("cod"=>"0",
                        "mensaje"=>$this->lang->line('error_local_beneficio_minimo')
                    );
                }
            }
        }else{
            $result = array("cod"=>"0",
                "mensaje"=>$this->lang->line('error_local_promocion_eliminar')
            );
        }
        echo json_encode($result);
    }

    public function export()
    {
        if ($this->session->userdata('logged_in') === TRUE) {

            $id_beneficio = $this->input->get('id_beneficio');
            $local = $this->input->get('local');
            $estado = $this->input->get('estado');


            $where = "";

            if ($local != "") {
                $this->arrayVista['local'] = $local;
                if ($where != "")
                    $where .= " AND L.nombre  LIKE '%" . $local . "%'";

                else
                    $where .= "L.nombre LIKE '%" . $local . "%'";
            }

            if ($estado != "") {
                $this->arrayVista['estado'] = $estado;
                if ($where != "")
                    $where .= " AND tbl_local_beneficio.estado = $estado";
                else
                    $where .= "tbl_local_beneficio.estado = $estado";
            }

            $join=array(
                'tbl_local as L,L.id=tbl_local_beneficio.id_local','tbl_beneficio as B,B.id=tbl_local_beneficio.id_beneficio'
            );
            if($where==""){

                $params = array(
                    "select" => "tbl_local_beneficio.id,tbl_local_beneficio.fecha_registro,tbl_local_beneficio.estado,L.nombre,
                    B.nombre as beneficio",
                    "where" => "tbl_local_beneficio.id_beneficio = '" . $id_beneficio . "'",
                    "join" => $join,
                    "order"=> "tbl_local_beneficio.fecha_registro desc"
                );
            }
            else{
                $params =    array(
                    "select" => "tbl_local_beneficio.id,tbl_local_beneficio.fecha_registro,tbl_local_beneficio.estado,L.nombre,
                    B.nombre as beneficio",
                    "where" => "tbl_local_beneficio.id_beneficio = '" . $id_beneficio . "' AND ".$where,
                    "join" => $join,
                    "order"=> "tbl_local_beneficio.fecha_registro desc"

                );
            }
            $locales = $this->Local_beneficio_model->search($params);

            
            //retrive contries table data
            $data = array();
            foreach ($locales as $local) {



                if ($local->estado== 1){
                    $estado = "Activo";
                }else{
                    $estado = "Inactivo";
                }


                array_push($data, array("Beneficio" => $local->beneficio,
                    utf8_decode("Local") => isset($local->nombre)?utf8_decode($local->nombre):"-",
                    utf8_decode("Fecha de Registro") => isset($local->fecha_registro)?date("d/m/Y h:i:s a", strtotime($local->fecha_registro)):"-",
                    utf8_decode("Estado") => isset($estado)?$estado:"-",
                ));
            }
            //Fill data
            function filterData(&$str)
            {
                $str = preg_replace("/\t/", "\\t", $str);
                $str = preg_replace("/\r?\n/", "\\n", $str);
                if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
            }             
                
            $fileName = "Locales de beneficios-". date('YmdHis') . ".xls";
                
            header("Content-Disposition: attachment; filename=\"$fileName\"");
            header("Content-Type: application/vnd.ms-excel, charset=utf-8");
                
            $flag = false;

            foreach($data as $row) {
                // print_r( array_values($row))."<br>";
                if(!$flag) {
                    
                    echo implode("\t", array_keys($row)) . "\n";
                    $flag = true;
                }
                    
                array_walk($row, 'filterData');
                echo implode("\t", array_values($row)) . "\n";
            }
                
            exit;
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }
}
