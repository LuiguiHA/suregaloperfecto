<?php
/**
 * Created by PhpStorm.
 * User: Mediabyte
 * Date: 11/05/2016
 * Time: 06:15 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model("Usuario_model");
        $this->load->model("Persona_model");
        $this->load->model("Empresa_model");
        $this->lang->load('backend_error_lang', 'spanish');
        $this->lang->load('backend_mensajes_lang', 'spanish');
        $this->load->helper('url');


    }

    public function listar($start = 0)
    {


        if ($this->session->userdata('logged_in') === TRUE) {
            $join=array(
                'tbl_persona as P,P.id=tbl_usuario.id_persona'
            );

            $params = array(
                "select" => "P.dni,P.nombre,P.apellido,P.sexo,tbl_usuario.estado,tbl_usuario.id",
                "join" => $join,
                "order" => "tbl_usuario.fecha_registro desc"
            );
            $total_usuarios = $this->Usuario_model->total_records($params);

            $usuario= $this->Usuario_model->search_data($params, $start, $this->elementoPorPagina);

            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Usuario'), $total_usuarios);

            $arrayMigaPan = array(
                array("nombre" => "Usuario"), array("nombre" => "Listar", 'active' => true));

            $empresas = array(
                "select" => "*"
            );

            $empresa = $this->Empresa_model->search($empresas);

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['tituloPagina'] = "Lista de Usuarios";

            $this->arrayVista['usuarios'] = $usuario;
            $this->arrayVista['empresas'] = $empresa;

           $this->arrayVista['vista'] = 'backend/usuario/listar_view';
            $this->cargarVistaBackend();
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    //Listar empresas por persona
    public function listar_empresa($start=0)
    {

        if ($this->session->userdata('logged_in') === TRUE) {
            $id_usuario = $this->input->post('id_usuario');
            
            $join2=array(
                'tbl_persona as P,P.id=tbl_usuario.id_persona'
            );

            $params1 = array(
                "select" => "P.id",
                "join" => $join2,
                "where" => "tbl_usuario.id = '".$id_usuario."'"
            );

            $id_persona = $this->Usuario_model->get_search_row($params1);



            $join=array(
                'tbl_usuario as U, U.id_empresa=tbl_empresa.id', 'tbl_persona as  P, P.id=U.id_persona '
            );


            $params = array(
                "select" => "tbl_empresa.nombre,U.correo,U.punto_acumulado,U.fecha_registro",
                "join" => $join,
               // "join" => $join2,
                "where" => "P.id = '".$id_persona->id."'",
                "order" => "U.fecha_registro desc"
            );




            $total_persona_empresa = $this->Empresa_model->total_records($params);

            $empresa_persona= $this->Empresa_model->search_data($params, $start, $this->elementoPorPagina);

            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend').'/Usuario/listar_empresa'), $total_persona_empresa);
            $this->arrayVista['tituloPagina'] = "Lista de Empresas";
            $this->arrayVista['usuario'] = $id_usuario;
            $this->arrayVista['empresa_persona'] = $empresa_persona;


            $this->cargarVistaListaAjax('backend/empresa_persona/listar_view');
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function buscar_empresa($start=0)
    {

        if ($this->session->userdata('logged_in') === TRUE) {
            $id_usuario = $this->input->post('id_usuario');
            $nombre  = $this->input->post('nombre');

            $where = "";


            if ($nombre != "") {
                $this->arrayVista['nombre'] = $nombre;
                if ($where != "") {
                    $where .= " AND tbl_empresa.nombre LIKE '%" . $nombre . "%' ";
                } else {
                    $where = "tbl_empresa.nombre LIKE '%" . $nombre . "%' ";
                }
            }


            $join2=array(
                'tbl_persona as P,P.id=tbl_usuario.id_persona'
            );


            $params1 = array(
                "select" => "P.id",
                "join" => $join2,
                "where" => "tbl_usuario.id = '".$id_usuario."'"
            );

            $id_persona = $this->Usuario_model->get_search_row($params1);



            $join=array(
                'tbl_usuario as U, U.id_empresa=tbl_empresa.id', 'tbl_persona as  P, P.id=U.id_persona '
            );

            if ( $where == "") {
                $params = array(
                    "select" => "tbl_empresa.nombre,U.correo,U.punto_acumulado,U.fecha_registro",
                    "join" => $join,
                    // "join" => $join2,
                    "where" => "P.id = '" . $id_persona->id . "'",
                    "order" => "U.fecha_registro desc"
                );
            }else{
                $params = array(
                    "select" => "tbl_empresa.nombre,U.correo,U.punto_acumulado,U.fecha_registro",
                    "join" => $join,
                    // "join" => $join2,
                    "where" => "P.id = '" . $id_persona->id . "' AND ".$where,
                    "order" => "U.fecha_registro desc"
                );
            }




            $total_persona_empresa = $this->Empresa_model->total_records($params);

            $empresa_persona= $this->Empresa_model->search_data($params, $start, $this->elementoPorPagina);

            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend').'/Usuario/buscar_empresa'), $total_persona_empresa);
            $this->arrayVista['tituloPagina'] = "Lista de Empresas";
            $this->arrayVista['usuario'] = $id_usuario;
            $this->arrayVista['empresa_persona'] = $empresa_persona;


            $this->cargarVistaListaAjax('backend/empresa_persona/listar_view');
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }
    
    public function buscar($start = 0)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            $nombre = $this->input->post('nombre');
            $apellido = $this->input->post('apellido');
            $dni= $this->input->post('dni');
            $empresa= $this->input->post('empresa');
            $estado = $this->input->post('estado');
            $where = "";


            if ($nombre != "") {
                $this->arrayVista['nombre'] = $nombre;
                if ($where != "") {
                    $where .= " AND P.nombre LIKE '%" . $nombre . "%' ";
                } else {
                    $where = "P.nombre LIKE '%" . $nombre . "%' ";
                }
            }

            if ($apellido != "") {
                $this->arrayVista['apellido'] = $apellido;
                if ($where != "") {
                    $where .= " AND P.apellido LIKE '%" . $apellido . "%' ";
                } else {
                    $where = "P.apellido LIKE '%" . $apellido . "%' ";
                }
            }

            if ($dni != "") {
                $this->arrayVista['dni'] = $dni;
                if ($where != "") {
                    $where .= " AND P.dni LIKE '%" . $dni . "%' ";
                } else {
                    $where = "P.dni LIKE '%" . $dni . "%' ";
                }
            }


            if ($empresa != "") {
                $this->arrayVista['empresa'] = $empresa;
                if ($where != "")
                    $where .= " AND tbl_usuario.id_empresa = $empresa";
                else
                    $where .= "tbl_usuario.id_empresa = $empresa";
            }

            if ($estado != "") {
                $this->arrayVista['estado'] = $estado;
                if ($where != "")
                    $where .= " AND tbl_usuario.estado = $estado";
                else
                    $where .= "tbl_usuario.estado = $estado";
            }

            $join=array(
                'tbl_persona as P,P.id=tbl_usuario.id_persona'
            );

            if ($where == "") {

                $params = array(
                    "select" => "P.dni,P.nombre,P.apellido,P.sexo,tbl_usuario.estado,tbl_usuario.id",
                    "join" => $join,
                    "order" => "tbl_usuario.fecha_registro desc"
                );
            } else {
                $params = array(
                    "select" => "P.dni,P.nombre,P.apellido,P.sexo,tbl_usuario.estado,tbl_usuario.id",
                    "where" => $where,
                    "join" => $join,
                    "order" => "tbl_usuario.fecha_registro desc"
                );
            }

            $total_usuarios = $this->Usuario_model->total_records($params);

            $usuario = $this->Usuario_model->search_data($params, $start, $this->elementoPorPagina);
            $arrayMigaPan = array(
                array("nombre" => "Usuario"), array("nombre" => "Listar", 'active' => true));

            $empresas = array(
                "select" => "*"
            );

            $empresa = $this->Empresa_model->search($empresas);
            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Usuario/buscar'), $total_usuarios);
            $this->arrayVista['tituloPagina'] = "Lista de Usuarios";
            $this->arrayVista['usuarios'] = $usuario;
            $this->arrayVista['empresas'] = $empresa;
            $this->arrayVista['vista'] = 'backend/usuario/listar_view';
            $this->cargarVistaBackend();
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function agregar()
    {

        if ($this->session->userdata('logged_in') === TRUE) {

            if ($this->input->post()) {
                $dni = $this->input->post("dni");
                $nombre= $this->input->post("nombre");
                $apellido = $this->input->post("apellido");
                $sexo = $this->input->post("sexo");
                $fecha_nacimiento = $this->input->post("fecha_nacimiento");
                $empresa = $this->input->post("empresa");
                $contrasenia = $this->input->post("contrasenia");
                $correo = $this->input->post("correo");
                $puntos = $this->input->post("puntos");
                $estado = $this->input->post("estado");

                $data_persona = array(

                    'dni' => $dni,
                    'nombre' => $nombre,
                    'apellido' => $apellido,
                    'sexo' => strtolower($sexo),
                    'fecha_nacimiento' => $fecha_nacimiento,
                    'fecha_registro' => date("Y-m-d H:i:s"),
                    'estado' => $estado
                );


                if (isset($data_persona)) {
                    try {
                       $this->Persona_model->insert($data_persona);
                        $id_persona = $this->db->insert_id();
                        $data_usuario = array(
                            'id_persona' => $id_persona,
                            'id_empresa' => $empresa,
                            'correo' => $correo,
                            'contrasenia' => md5($contrasenia),
                            'punto_acumulado' => $puntos,
                            'fecha_registro' => date("Y-m-d H:i:s"),
                            'estado' => $estado
                        );
                        $this->Usuario_model->insert($data_usuario);
                        $this->alert($this->lang->line('mensaje_usuario_agregado'),  site_url($this->config->item('path_backend').'/Usuario'));
                    } catch (Exception $exception) {
                        $this->alert($this->lang->line('error_usuario_agregar'));
                    }
                }

            }

            $empresas = array(
                "select" => "*", "where" => "estado = 1"
            );
            $arrayMigaPan = array(
                array("nombre" => "Usuario" , 'url' => site_url($this->config->item('path_backend').'/Usuario')), array("nombre" => "Agregar", 'active' => true));

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;

            $empresa = $this->Empresa_model->search($empresas);


            $this->arrayVista['empresas'] = $empresa;
            $this->arrayVista['tituloPagina'] = "Agregar Usuario";
            $this->arrayVista['vista'] = 'backend/usuario/agregar_view';
            $this->cargarVistaBackend();



        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function editar($id_usuario)
    {
        if ($this->session->userdata('logged_in') === TRUE) {

            if ($this->input->post()) {
                $dni = $this->input->post("dni");
                $nombre= $this->input->post("nombre");
                $apellido = $this->input->post("apellido");
                $sexo = $this->input->post("sexo");
                $fecha_nacimiento = $this->input->post("fecha_nacimiento");
                $estado = $this->input->post("estado");

                $data_persona = array(

                    'dni' => $dni,
                    'nombre' => $nombre,
                    'apellido' => $apellido,
                    'sexo' => $sexo,
                    'fecha_nacimiento' => $fecha_nacimiento,
                    'fecha_modificacion' => date("Y-m-d H:i:s"),
                    'estado' => $estado
                );

                $join=array(
                    'tbl_persona as P,P.id=tbl_usuario.id_persona'
                );

                $params = array(
                    "select" => "P.id",
                    "join" => $join,
                    "where" => "tbl_usuario.id = '".$id_usuario."'"
                );

                $id_persona = $this->Usuario_model->get_search_row($params);

                $data_usuario = array('estado'=>$estado);


                if ($this->Persona_model->update($id_persona->id, $data_persona)) {

                    $this->Usuario_model->update($id_usuario, $data_usuario);

                    $this->alert($this->lang->line('mensaje_usuario_editado'),  site_url($this->config->item('path_backend').'/Usuario'));

                }
            }

            $join=array(
                'tbl_persona as P,P.id=tbl_usuario.id_persona'
            );

            $persona = array(
                "select" => "tbl_usuario.id,P.nombre,P.apellido,P.dni,P.sexo,P.fecha_nacimiento,P.estado",
                "join" => $join,
                "where" => "tbl_usuario.id = '".$id_usuario."'"
            );

            $usuario = $this->Usuario_model->get_search_row($persona);
            $arrayMigaPan = array(
                array("nombre" => "Usuario" , 'url' => site_url($this->config->item('path_backend').'/Usuario')), array("nombre" => "Editar", 'active' => true));

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;

            $this->arrayVista['usuario'] =  $usuario ;
            $this->arrayVista['tituloPagina'] = "Editar Usuario";
            $this->arrayVista['vista'] = 'backend/usuario/editar_view';
            $this->cargarVistaBackend();



        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function export()
    {
        if ($this->session->userdata('logged_in') === TRUE) {

            $nombre = $this->input->get('nombre');
            $apellido = $this->input->get('apellido');
            $dni = $this->input->get('dni');
            $empresa = $this->input->get('empresa');
            $estado = $this->input->get('estado');


            $where = "";


            if ($nombre != "" ) {


                if ($where != "") {
                    $where .= " AND P.nombre LIKE '%" . $nombre . "%' ";
                } else {
                    $where = "P.nombre LIKE '%" . $nombre . "%' ";
                }
            }


            if ($apellido != "" ) {

                if ($where != "") {
                    $where .= " AND P.apellido LIKE '%" . $apellido . "%' ";
                } else {
                    $where = "P.apellido LIKE '%" . $apellido . "%' ";
                }
            }

            if ($dni != "" ) {


                if ($where != "") {
                    $where .= " AND P.dni LIKE '%" . $dni . "%' ";
                } else {
                    $where = "P.dni LIKE '%" . $dni . "%' ";
                }
            }

            if ($empresa != "" ) {

                if ($where != "")
                    $where .= " AND tbl_usuario.id_[empresa = $empresa";
                else
                    $where .= "tbl_usuario.id_empresa = $empresa";
            }

            if ($estado != "" ) {

                if ($where != "")
                    $where .= " AND tbl_usuario.estado = $estado";
                else
                    $where .= "tbl_usuario.estado = $estado";
            }

            $join=array(
                'tbl_persona as P,P.id=tbl_usuario.id_persona', 'tbl_empresa as E,E.id=tbl_usuario.id_empresa'
            );


            if ($where == "") {
                $params = array(
                    "select" => "E.nombre as empresa,P.dni,P.nombre,P.apellido,P.sexo,P.fecha_nacimiento,tbl_usuario.correo,
                    tbl_usuario.punto_acumulado,tbl_usuario.fecha_registro,tbl_usuario.estado",
                    "join" => $join,
                    "order" => "tbl_usuario.fecha_registro desc"
                );
            } else {
                $params = array(
                    "select" => "E.nombre as empresa,P.dni,P.nombre,P.apellido,P.sexo,P.fecha_nacimiento,tbl_usuario.correo,
                    tbl_usuario.punto_acumulado,tbl_usuario.fecha_registro,tbl_usuario.estado",
                    "join" => $join,
                    "where" => $where,
                    "order" => "tbl_usuario.fecha_registro desc"
                );
            }

            $usuarios = $this->Usuario_model->search($params);

            //retrive contries table data
            $data = array();
            foreach ($usuarios as $usuario) {


                if ($usuario->estado == 1){
                    $estado = "Activo";
                }else{
                    $estado = "Inactivo";
                }

                if (strcasecmp($usuario->sexo,"M") == 0){
                    $sexo = "Masculino";
                }else{
                    $sexo = "Femenino";
                }


                array_push(
                    $data,
                    array(
                        utf8_decode("Empresa") => isset($usuario->empresa)?utf8_decode($usuario->empresa):"-",
                        utf8_decode("DNI") => isset($usuario->dni)?utf8_decode($usuario->dni):"-",
                        utf8_decode("Nombre") => isset($usuario->nombre)?utf8_decode($usuario->nombre):"-",
                        utf8_decode("Apellido") => isset($usuario->apellido)?utf8_decode($usuario->apellido):"-",
                        utf8_decode("Sexo") => isset($sexo)?utf8_decode($sexo):"-",
                        utf8_decode("Fecha de Nacimiento") => date("d/m/Y", strtotime($usuario->fecha_nacimiento)),
                        utf8_decode("Correo") => isset($usuario->correo)?utf8_decode($usuario->correo):"-",
                        utf8_decode("Puntos Acumulados") => isset($usuario->punto_acumulado)?utf8_decode($usuario->punto_acumulado):"-",
                        utf8_decode("Fecha de Registro") => date("d/m/Y h:i:s a", strtotime($usuario->fecha_registro)),
                        utf8_decode("Estado") => $estado,
                ));
            }
            //Fill data
            
            function filterData(&$str)
            {
                $str = preg_replace("/\t/", "\\t", $str);
                $str = preg_replace("/\r?\n/", "\\n", $str);
                if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
            }             
                
            $fileName = "Usuarios -". date('YmdHis') . ".xls";
                
            header("Content-Disposition: attachment; filename=\"$fileName\"");
            header("Content-Type: application/vnd.ms-excel, charset=utf-8");
                
            $flag = false;

            foreach($data as $row) {
                // print_r( array_values($row))."<br>";
                if(!$flag) {
                    
                    echo implode("\t", array_keys($row)) . "\n";
                    $flag = true;
                }
                    
                array_walk($row, 'filterData');
                echo implode("\t", array_values($row)) . "\n";
            }
                
            exit;
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function export_user_empresa()
    {
        if ($this->session->userdata('logged_in') === TRUE) {

            $nombre = $this->input->get('nombre');
            $id_usuario = $this->input->get('id_usuario');


            $where = "";

            if ($nombre != "") {
                $this->arrayVista['nombre'] = $nombre;
                if ($where != "") {
                    $where .= " AND tbl_empresa.nombre LIKE '%" . $nombre . "%' ";
                } else {
                    $where = "tbl_empresa.nombre LIKE '%" . $nombre . "%' ";
                }
            }




            $join2=array(
                'tbl_persona as P,P.id=tbl_usuario.id_persona'
            );


            $params1 = array(
                "select" => "P.id",
                "join" => $join2,
                "where" => "tbl_usuario.id = '".$id_usuario."'"
            );

            $id_persona = $this->Usuario_model->get_search_row($params1);



            $join=array(
                'tbl_usuario as U, U.id_empresa=tbl_empresa.id', 'tbl_persona as  P, P.id=U.id_persona '
            );

            if ( $where == "") {
                $params = array(
                    "select" => "tbl_empresa.nombre,U.correo,U.punto_acumulado,U.fecha_registro",
                    "join" => $join,
                    // "join" => $join2,
                    "where" => "P.id = '" . $id_persona->id . "'",
                    "order" => "U.fecha_registro desc"
                );
            }else{
                $params = array(
                    "select" => "tbl_empresa.nombre,U.correo,U.punto_acumulado,U.fecha_registro",
                    "join" => $join,
                    // "join" => $join2,
                    "where" => "P.id = '" . $id_persona->id . "' AND ".$where,
                    "order" => "U.fecha_registro desc"
                );
            }



            $empresas = $this->Empresa_model->search($params);

           
            $data = array();
            foreach ($empresas as $empresa) {



                array_push(
                    $data,
                    array(
                        utf8_decode("Empresa") => isset($empresa->nombre)?utf8_decode($empresa->nombre):"-",
                        utf8_decode("Correo") => isset($empresa->correo)?utf8_decode($empresa->correo):"-",
                        utf8_decode("Puntos Acumulados") => isset($empresa->punto_acumulado)?utf8_decode($empresa->punto_acumulado):"-",
                        utf8_decode("Fecha de Registro") => isset($empresa->fecha_registro)?date("d/m/Y h:i:s a", strtotime($empresa->fecha_registro)):"-",

                ));
            }
            //Fill data
             function filterData(&$str)
            {
                $str = preg_replace("/\t/", "\\t", $str);
                $str = preg_replace("/\r?\n/", "\\n", $str);
                if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
            }             
                
            $fileName = "Usuarios -". date('YmdHis') . ".xls";
                
            header("Content-Disposition: attachment; filename=\"$fileName\"");
            header("Content-Type: application/vnd.ms-excel, charset=utf-8");
                
            $flag = false;

            foreach($data as $row) {
                // print_r( array_values($row))."<br>";
                if(!$flag) {
                    
                    echo implode("\t", array_keys($row)) . "\n";
                    $flag = true;
                }
                    
                array_walk($row, 'filterData');
                echo implode("\t", array_values($row)) . "\n";
            }
                
            exit;
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

}