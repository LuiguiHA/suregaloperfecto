<?php
/**
 * Created by PhpStorm.
 * User: warri
 * Date: 18/05/2016
 * Time: 10:26 AM
 */

class Formula_puntos extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model("Formula_puntos_model");
        $this->lang->load('backend_error_lang', 'spanish');
        $this->lang->load('backend_mensajes_lang', 'spanish');
        $this->load->helper('url');


    }

    public function listar($start = 0)
    {

        if ($this->session->userdata('logged_in') === TRUE) {
            $params = array(
                "select" => "*",
                "order"=> "fecha_registro desc"
            );
            $total_formulas = $this->Formula_puntos_model->total_records($params);

            $formulas = $this->Formula_puntos_model->search_data($params, $start, $this->elementoPorPagina);

            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Formula_Puntos'), $total_formulas);


            $arrayMigaPan = array(
                array("nombre" => "Empresa" ,"url" => site_url($this->config->item('path_backend') . '/Empresa')),
                array("nombre" => "Fórmula"),
                array("nombre" => "Listar", 'active' => true));

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['tituloPagina'] = "Lista de Fórmulas de Puntos";

            $this->arrayVista['formulas'] = $formulas;

            $this->arrayVista['vista'] = 'backend/formula_puntos/listar_view';
            $this->cargarVistaBackend();
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function buscar($start = 0)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            $nombre = $this->input->post('nombre');
            $estado = $this->input->post('estado');
            $where = "";


            if ($nombre != "") {
                $this->arrayVista['nombre'] = $nombre;
                if ($where != "") {
                    $where .= " AND nombre LIKE '%" . $nombre . "%' ";
                } else {
                    $where = "nombre LIKE '%" . $nombre . "%' ";
                }
            }



            if ($estado != "") {
                $this->arrayVista['estado'] = $estado;
                if ($where != "")
                    $where .= " AND estado = $estado";
                else
                    $where .= "estado = $estado";
            }

            if ($where == "") {
                $params = array(
                    "select" => "*",
                    "order"=> "fecha_registro desc"
                );
            } else {
                $params = array(
                    "select" => "*",
                    "where" => $where,
                    "order"=> "fecha_registro desc"
                );
            }

            $total_formulas = $this->Formula_puntos_model->total_records($params);

            $formulas= $this->Formula_puntos_model->search_data($params, $start, $this->elementoPorPagina);
            $arrayMigaPan = array(
                array("nombre" => "Empresa" ,"url" => site_url($this->config->item('path_backend') . '/Empresa')),
                array("nombre" => "Fórmula"),
                array("nombre" => "Listar", 'active' => true));
            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend').'/Formula_Puntos/buscar'), $total_formulas);
            $this->arrayVista['tituloPagina'] = "Lista de Fórmulas de Puntos";
            $this->arrayVista['formulas'] = $formulas;
            $this->arrayVista['vista'] = 'backend/formula_puntos/listar_view';
            $this->cargarVistaBackend();
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function agregar()
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {

                $nombre = $this->input->post("nombre");
                $monto= $this->input->post("monto");
                $punto = $this->input->post("punto");
                $estado = $this->input->post("estado");


                $data_formula = array(

                    'nombre' => $nombre,
                    'monto' => $monto,
                    'punto' => $punto,
                    'fecha_registro' => date("Y-m-d H:i:s"),
                    'estado' => $estado
                );



                if (isset($data_formula)) {
                    try {
                        $this->Formula_puntos_model->insert($data_formula);
                        $this->alert($this->lang->line('mensaje_formula_agregado'), site_url($this->config->item('path_backend') . '/Formula_Puntos'));
                    } catch (Exception $exception) {
                        $this->alert($this->lang->line('error_formula_agregar'));
                    }
                }

            }



            $arrayMigaPan = array(
                array("nombre" => "Empresa" ,"url" => site_url($this->config->item('path_backend') . '/Empresa')),
                array("nombre" => "Fórmula"  ,"url" => site_url($this->config->item('path_backend') . '/Formula_Puntos')),
                array("nombre" => "Agregar", 'active' => true));
            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['tituloPagina'] = "Agregar Fórmula";
            $this->arrayVista['vista'] = 'backend/formula_puntos/agregar_view';
            $this->cargarVistaBackend();

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function editar($id_formula)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {
                $nombre = $this->input->post("nombre");
                $monto= $this->input->post("monto");
                $punto = $this->input->post("punto");
                $estado = $this->input->post("estado");


                $data_formula = array(

                    'nombre' => $nombre,
                    'monto' => $monto,
                    'punto' => $punto,
                    'fecha_modificacion' => date("Y-m-d H:i:s"),
                    'estado' => $estado
                );




                if ($this->Formula_puntos_model->update($id_formula, $data_formula)) {

                    $this->alert($this->lang->line('mensaje_formula_editado'), site_url($this->config->item('path_backend') . '/Formula_Puntos'));

                }
            }



            $formula = array(
                "select" => "*", "where" => "id = '".$id_formula."'"
            );


            $formula = $this->Formula_puntos_model->get_search_row($formula);

            $arrayMigaPan = array(
                array("nombre" => "Empresa" ,"url" => site_url($this->config->item('path_backend') . '/Empresa')),
                array("nombre" => "Fórmula"  ,"url" => site_url($this->config->item('path_backend') . '/Formula_Puntos')),
                array("nombre" => "Editar", 'active' => true));
            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['tituloPagina'] = "Editar Fórmula";
            $this->arrayVista['formula'] = $formula;

            $this->arrayVista['vista'] = 'backend/formula_puntos/editar_view';
            $this->cargarVistaBackend();

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function revisar_formulas()
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {

                $nombre = $this->input->post("nombre");
                $monto= $this->input->post("monto");
                $punto = $this->input->post("punto");
                $estado = $this->input->post("estado");


                $data_formula = array(

                    'nombre' => $nombre,
                    'monto' => $monto,
                    'punto' => $punto,
                    'fecha_registro' => date("Y-m-d H:i:s"),
                    'estado' => $estado
                );



                if (isset($data_formula)) {
                    try {
                        $this->Formula_puntos_model->insert($data_formula);
                        $this->alert($this->lang->line('mensaje_formula_agregado'), site_url($this->config->item('path_backend') . '/Formula_Puntos'));
                    } catch (Exception $exception) {
                        $this->alert($this->lang->line('error_formula_agregar'));
                    }
                }

            }



            $arrayMigaPan = array(
                array("nombre" => "Empresa" ,"url" => site_url($this->config->item('path_backend') . '/Empresa')),
                array("nombre" => "Fórmula"  ,"url" => site_url($this->config->item('path_backend') . '/Formula_Puntos')),
                array("nombre" => "Agregar", 'active' => true));
            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['tituloPagina'] = "Agregar Fórmula";
            $this->arrayVista['vista'] = 'backend/formula_puntos/agregar_view';
            $this->cargarVistaBackend();

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }


    public function export()
    {
        if ($this->session->userdata('logged_in') === TRUE) {

            $nombre = $this->input->get('nombre');
            $estado = $this->input->get('estado');


            $where = "";


            if ($nombre != "" ) {

                $this->arrayVista['nombre'] = $nombre;
                if ($where != "") {
                    $where .= " AND nombre LIKE '%" . $nombre . "%' ";
                } else {
                    $where = "nombre LIKE '%" . $nombre . "%' ";
                }
            }



            if ($estado != "" ) {
                $this->arrayVista['estado'] = $estado;
                if ($where != "")
                    $where .= " AND estado = $estado";
                else
                    $where .= "estado = $estado";
            }

            if ($where == "") {
                $params = array(
                    "select" => "*",
                    "order" => "fecha_registro desc"
                );
            } else {
                $params = array(
                    "select" => "*",
                    "where" => $where,
                    "order" => "fecha_registro desc"
                );
            }

            $formulas = $this->Formula_puntos_model->search($params);

           /* $this->load->library('excel');
            $this->excel->setActiveSheetIndex(0);
            $this->excel->getActiveSheet()->setTitle("Formula de Puntos");


            $this->excel->getActiveSheet()->setCellValue('A1', 'Nombre');
            $this->excel->getActiveSheet()->setCellValue('B1', 'Monto');
            $this->excel->getActiveSheet()->setCellValue('C1', 'Punto');
            $this->excel->getActiveSheet()->setCellValue('D1', 'Fecha de Registro');
            $this->excel->getActiveSheet()->setCellValue('E1', 'Estado');

            for($i=65; $i<=69; $i++) {
                $letra = chr($i);
                $this->excel->getActiveSheet()->getStyle($letra.'1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }

            for($i=65; $i<=69; $i++) {
                $letra = chr($i);
                $this->excel->getActiveSheet()->getStyle($letra.'1')->getFont()->setBold(true);
            }



            for ($col = ord('A'); $col <= ord('E'); $col++) {

                $this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);

                $this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
                $this->excel->getActiveSheet()->getStyle(chr($col))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }*/
            //retrive contries table data
            $data = array();
            foreach ($formulas as $formula) {



                if ($formula->estado== 1){
                    $estado = "Activo";
                }else{
                    $estado = "Inactivo";
                }


                array_push($data, array(
                    utf8_decode("Nombre") => isset($formula->nombre)?utf8_decode($formula->nombre):"-",
                    utf8_decode("Monto") => isset($formula->monto)?utf8_decode($formula->monto):"-",
                    utf8_decode("Punto") => isset($formula->punto)?utf8_decode($formula->punto):"-",
                    utf8_decode("Fecha de Registro") => isset($formula->fecha_registro)?date("d/m/Y h:i:s a", strtotime($formula->fecha_registro)):"-",
                    utf8_decode("Estado") => isset($estado)?$estado:"-",
                ));
            }
            //Fill data
            function filterData(&$str)
            {
                $str = preg_replace("/\t/", "\\t", $str);
                $str = preg_replace("/\r?\n/", "\\n", $str);
                if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
            }

            $fileName = "Formula de puntos -". date('YmdHis') . ".xls";

            header("Content-Disposition: attachment; filename=\"$fileName\"");
            header("Content-Type: application/vnd.ms-excel, charset=utf-8");

            $flag = false;

            foreach($data as $row) {
                // print_r( array_values($row))."<br>";
                if(!$flag) {

                    echo implode("\t", array_keys($row)) . "\n";
                    $flag = true;
                }

                array_walk($row, 'filterData');
                echo implode("\t", array_values($row)) . "\n";
            }

            exit;
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }
}
