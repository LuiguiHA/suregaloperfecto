<?php
/**
 * Created by PhpStorm.
 * User: Mediabyte
 * Date: 02/05/2016
 * Time: 03:39 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoria_beneficio extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();


        $this->load->model("Categoria_beneficio_model");
        $this->load->model("Beneficio_model");
        $this->load->helper('url');
        $this->lang->load('backend_error_lang', 'spanish');
        $this->lang->load('backend_mensajes_lang', 'spanish');


    }

    public function listar($start = 0)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            $params = array(
                "select" => "*",
                "order"=> "fecha_registro desc"
            );
            $total_categoria_beneficio = $this->Categoria_beneficio_model->total_records($params);

            $categoria_beneficio = $this->Categoria_beneficio_model->search_data($params, $start, $this->elementoPorPagina);
            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Categoria_Beneficio'), $total_categoria_beneficio);


            $arrayMigaPan = array(array("nombre" => "Beneficio" ,"url" => site_url($this->config->item('path_backend') . '/inicio') ),
                array("nombre" => "Categoría"), array("nombre" => "Listar", 'active' => true));



            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;

            $this->arrayVista['tituloPagina'] = "Lista de Categorías";
            $this->arrayVista['categoria_beneficio'] = $categoria_beneficio;
            $this->arrayVista['vista'] = 'backend/categoria_beneficio/listar_view';
            $this->cargarVistaBackend();
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }


    public function buscar($start = 0)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            $nombre = $this->input->post('nombre');

            $estado = $this->input->post('estado');
            $where = "";


            if ($nombre != "") {
                $this->arrayVista['nombre'] = $nombre;
                if ($where != "") {
                    $where .= " AND nombre LIKE '%" . $nombre . "%' ";
                } else {
                    $where = "nombre LIKE '%" . $nombre . "%' ";
                }
            }


            if ($estado != "") {
                $this->arrayVista['estado'] = $estado;
                if ($where != "")
                    $where .= " AND estado = $estado";
                else
                    $where .= "estado = $estado";
            }

            if ($where == "") {
                $params = array(
                    "select" => "*",
                    "order"=> "fecha_registro desc"
                );
            } else {
                $params = array(
                    "select" => "*",
                    "where" => $where,
                    "order"=> "fecha_registro desc"
                );
            }

            $total_categoria_beneficio = $this->Categoria_beneficio_model->total_records($params);
            $arrayMigaPan = array(array("nombre" => "Beneficio" ,"url" => site_url($this->config->item('path_backend') . '/inicio') ),
                array("nombre" => "Categoría"), array("nombre" => "Listar", 'active' => true));


            $categoria_beneficio = $this->Categoria_beneficio_model->search_data($params, $start, $this->elementoPorPagina);
            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Categoria_Beneficio/buscar'), $total_categoria_beneficio);
            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['tituloPagina'] = "Lista de Categorías";
            $this->arrayVista['categoria_beneficio'] = $categoria_beneficio;
            $this->arrayVista['vista'] = 'backend/categoria_beneficio/listar_view';
            $this->cargarVistaBackend();

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function agregar()
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {

                $nombre = $this->input->post("nombre");
                $descripcion = $this->input->post("descripcion");

              //  $foto = $this->input->post("foto");

                $estado = $this->input->post("estado");

/*
                $pathImagen = "./upload/categoria_beneficio/" . date("Y-m-d");


                if (!is_dir($pathImagen))
                    mkdir($pathImagen, 0775, true);
                if ($_FILES["foto"]["size"] > 0) {
                    $nombreArchivoImagen = "foto";
                    $resultadoFoto = $this->subirArchivo($nombreArchivoImagen, $pathImagen, 'jpeg|jpg|png', 2024);
                    $foto = "";
                    if (array_key_exists("upload_data", $resultadoFoto)) {
                        if (isset($resultadoFoto["upload_data"]["file_name"]))

                            $foto = $resultadoFoto["upload_data"]["orig_name"];
                        //    $foto = base_url() . "files/img/categoria_beneficio/" . date("Y-m-d") . '/' . $resultadoFoto["upload_data"]["file_name"];
                    } else {
                        $this->session->set_flashdata('error', $this->lang->line('error_tamaño_imagen'));
                    }
                }
                $foto = $pathImagen."/".$foto;*/
                $data_categoria_beneficio = array(

                    'nombre' => $nombre,
                    'descripcion' => $descripcion,
                    //'foto' => $foto,
                    'fecha_registro' => date("Y-m-d H:i:s"),
                    'estado' => $estado
                );

                if (isset($data_categoria_beneficio)) {
                    try {
                        $this->Categoria_beneficio_model->insert($data_categoria_beneficio);
                        $this->alert($this->lang->line('mensaje_categoria_agregado'), site_url($this->config->item('path_backend') . '/Categoria_Beneficio'));
                    } catch (Exception $exception) {
                        $this->alert($this->lang->line('error_categoria_agregar'));
                    }
                }

            }

            $arrayMigaPan = array(array("nombre" => "Beneficio" , "url" => site_url($this->config->item('path_backend') . '/inicio')),
                array("nombre" => "Categoría", "url" => site_url($this->config->item('path_backend') . '/Categoria_Beneficio')), array("nombre" => "Agregar", 'active' => true));

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['tituloPagina'] = "Agregar Categoría";

            $this->arrayVista['vista'] = 'backend/categoria_beneficio/agregar_view';
            $this->cargarVistaBackend();

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function editar($id_categoria)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {
                $nombre = $this->input->post("nombre");
                $descripcion = $this->input->post("descripcion");

              //  $foto = $this->input->post("foto");
              //  $foto_antiguo = $this->input->post("foto_antiguo");
                $estado = $this->input->post("estado");
            //    $pathImagen = "./upload/categoria_beneficio/" . date("Y-m-d");


             /*   if (!is_dir($pathImagen))
                    mkdir($pathImagen, 0775, true);
                if ($_FILES["foto"]["size"] > 0) {
                    $nombreArchivoImagen = "foto";
                    $resultadoFoto = $this->subirArchivo($nombreArchivoImagen, $pathImagen, 'jpeg|jpg|png', 2024);
                    $foto = "";
                    if (array_key_exists("upload_data", $resultadoFoto)) {
                        if (isset($resultadoFoto["upload_data"]["file_name"]))

                            $foto = $resultadoFoto["upload_data"]["orig_name"];
                            $foto = $pathImagen."/".$foto;
                    } else {
                        $this->session->set_flashdata('error',$this->lang->line('error_tamaño_imagen'));
                    }

                }else{

                    $foto = $foto_antiguo;
                   
                }*/
                $data_categoria_beneficio = array(

                    'nombre' => $nombre,
                    'descripcion' => $descripcion,
                   // 'foto' => $foto,
                    'fecha_modificacion' => date("Y-m-d H:i:s"),
                    'estado' => $estado
                );



                if ($this->Categoria_beneficio_model->update($id_categoria, $data_categoria_beneficio)) {


                    $this->alert($this->lang->line('mensaje_categoria_editado'), site_url($this->config->item('path_backend') . '/Categoria_Beneficio'));

                }
            }

            $categoria = array(
                "select" => "*",
                "where" => "id = '" . $id_categoria . "'"
            );


            $categoria = $this->Categoria_beneficio_model->get_search_row($categoria);


            $arrayMigaPan = array(array("nombre" => "Beneficio" , "url" => site_url($this->config->item('path_backend') . '/inicio')),
                array("nombre" => "Categoría", "url" => site_url($this->config->item('path_backend') . '/Categoria_Beneficio')), array("nombre" => "Editar", 'active' => true));
            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['tituloPagina'] = "Editar Categoría";
         //   $this->arrayVista['fecha'] = $fecha_foto;
            $this->arrayVista['categoria'] = $categoria;
            $this->arrayVista['vista'] = 'backend/categoria_beneficio/editar_view';
            $this->cargarVistaBackend();

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function export()
    {
        if ($this->session->userdata('logged_in') === TRUE) {

            $nombre = $this->input->get('nombre');
            $estado = $this->input->get('estado');


            $where = "";


            if ($nombre != "" ) {

                $this->arrayVista['nombre'] = $nombre;
                if ($where != "") {
                    $where .= " AND nombre LIKE '%" . $nombre . "%' ";
                } else {
                    $where = "nombre LIKE '%" . $nombre . "%' ";
                }
            }



            if ($estado != "" ) {
                $this->arrayVista['estado'] = $estado;
                if ($where != "")
                    $where .= " AND estado = $estado";
                else
                    $where .= "estado = $estado";
            }

            if ($where == "") {
                $params = array(
                    "select" => "*",
                    "order" => "fecha_registro desc"
                );
            } else {
                $params = array(
                    "select" => "*",
                    "where" => $where,
                    "order" => "fecha_registro desc"
                );
            }

            $categorias_beneficio = $this->Categoria_beneficio_model->search($params);

            
            //retrive contries table data
            $data = array();
            foreach ($categorias_beneficio as $categoria) {

            $estado="";

                if ($categoria->estado== 1){
                    $estado = "Activo";
                }else{
                    $estado = "Inactivo";
                }

                array_push(
                    $data, 
                    array(
                        utf8_decode("Nombre") => isset($categoria->nombre)?utf8_decode($categoria->nombre):"-",
                        utf8_decode("Descripción") => isset($categoria->descripcion)?utf8_decode($categoria->descripcion):"-",
                       // utf8_decode("Foto") =>  isset($categoria->foto)?substr(base_url(),0,-2)  .substr($categoria->foto,1):"-",
                        utf8_decode("Fecha de Registro") => date("d/m/Y h:i:s a", strtotime($categoria->fecha_registro)),
                        utf8_decode("Estado") => $estado,
                ));
            }
            //Fill data
            function filterData(&$str)
            {
                $str = preg_replace("/\t/", "\\t", $str);
                $str = preg_replace("/\r?\n/", "\\n", $str);
                if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
            }             
                
            $fileName = "Categorias de beneficios-". date('YmdHis') . ".xls";
                
            header("Content-Disposition: attachment; filename=\"$fileName\"");
            header("Content-Type: application/vnd.ms-excel, charset=utf-8");
                
            $flag = false;

            foreach($data as $row) {
                // print_r( array_values($row))."<br>";
                if(!$flag) {
                    
                    echo implode("\t", array_keys($row)) . "\n";
                    $flag = true;
                }
                    
                array_walk($row, 'filterData');
                echo implode("\t", array_values($row)) . "\n";
            }
                
            exit;
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }
}
