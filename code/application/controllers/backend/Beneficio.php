<?php
/**
 * Created by PhpStorm.
 * User: Mediabyte
 * Date: 28/04/2016
 * Time: 02:09 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Beneficio extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model("Beneficio_model");
        $this->load->model("Categoria_beneficio_model");
        $this->load->model("Informacion_adicional_beneficio_model");
        $this->load->model("Local_beneficio_model");
        $this->load->model("Foto_beneficio_model");
        $this->load->model("Empresa_model");
        $this->load->model("Local_model");
        $this->lang->load('backend_error_lang', 'spanish');
        $this->lang->load('backend_mensajes_lang', 'spanish');
        $this->lang->load('backend_constants_lang', 'spanish');
        $this->load->helper('utils_helper');
        $this->load->helper('url');

    }

    public function listar($start = 0)
    {

        if ($this->session->userdata('logged_in') === TRUE) {

            $join = array(
                'tbl_empresa as E, E.id = tbl_beneficio.id_empresa', 'tbl_categoria_beneficio as  C, C.id=tbl_beneficio.id_beneficio_categoria '
            );
            $params = array(
                "select" => "tbl_beneficio.id,tbl_beneficio.nombre, tbl_beneficio.descripcion,
                    tbl_beneficio.telefono, tbl_beneficio.correo,  tbl_beneficio.estado,C.nombre as nombre_categoria,tbl_beneficio.tipo,
                    tbl_beneficio.fecha_registro,tbl_beneficio.fecha_inicio,tbl_beneficio.fecha_fin",
                "join" => $join,
                "order" => "tbl_beneficio.fecha_registro desc"

            );
            $total_beneficios = $this->Beneficio_model->total_records($params);

            $beneficio = $this->Beneficio_model->search_data($params, $start, $this->elementoPorPagina);
            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Beneficio'), $total_beneficios);

            $categorias = array(
                "select" => "*"
            );

            $empresas = array(
                "select" => "*"
            );

            $empresa = $this->Empresa_model->search($empresas);
            $categoria = $this->Categoria_beneficio_model->search($categorias);

            $arrayMigaPan = array(
                array("nombre" => "Beneficio"), array("nombre" => "Listar", 'active' => true));

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['tituloPagina'] = "Lista de Beneficios";
            $this->arrayVista['beneficios'] = $beneficio;
            $this->arrayVista['categorias'] = $categoria;
            $this->arrayVista['empresas'] = $empresa;
            $this->arrayVista['vista'] = 'backend/beneficio/listar_view';
            $this->cargarVistaBackend();

         
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }
    
    public function buscar($start = 0)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            $nombre = $this->input->post('nombre');
            $tipo = $this->input->post('tipo');
            $empresa = $this->input->post('empresa');
            $categoria = $this->input->post('categoria');
            $fecha_inicio = $this->input->post('fecha_inicio');
            $fecha_fin = $this->input->post('fecha_fin');
            $estado = $this->input->post('estado');
            $where = "";


            if ($nombre != "" ) {

                $this->arrayVista['nombre'] = $nombre;
                if ($where != "") {
                    $where .= " AND tbl_beneficio.nombre LIKE '%" . $nombre . "%' ";
                } else {
                    $where = "tbl_beneficio.nombre LIKE '%" . $nombre . "%' ";
                }
            }


            if ($tipo != "" ) {
                $this->arrayVista['tipo'] = $tipo;
                if ($where != "")
                    $where .= " AND tbl_beneficio.tipo = $tipo";
                else
                    $where .= "tbl_beneficio.tipo = $tipo";
            }

            if ($empresa != "" ) {
                $this->arrayVista['empresa_select'] = $empresa;
                if ($where != "")
                    $where .= " AND E.id = $empresa";
                else
                    $where .= "E.id = $empresa";
            }

            if ($categoria != "" ) {
                $this->arrayVista['categoria_select'] = $categoria;
                if ($where != "")
                    $where .= " AND C.id = $categoria";
                else
                    $where .= "C.id = $categoria";
            }

            if ($fecha_inicio != "" ) {

                $this->arrayVista['fecha_inicio'] = $fecha_inicio;
                list($anio,$mes,$dia) = explode('-',$fecha_inicio);
                if ($where != "") {

                    $where .= " AND DATE(tbl_beneficio.fecha_inicio) >= '$anio-$mes-$dia'  ";
                } else {

                    $where = "DATE(tbl_beneficio.fecha_inicio)  >= '$anio-$mes-$dia' ";
                }
            }

            if ($fecha_fin != "" ) {

                $this->arrayVista['fecha_fin'] = $fecha_fin;
                list($anio,$mes,$dia) = explode('-',$fecha_fin);
                if ($where != "") {

                    $where .= " AND DATE(tbl_beneficio.fecha_fin) <= '$anio-$mes-$dia'  ";

                } else {
                    $where = "DATE(tbl_beneficio.fecha_fin)  <= '$anio-$mes-$dia' ";
                }
            }

            if ($estado != "" ) {
                $this->arrayVista['estado'] = $estado;
                if ($where != "")
                    $where .= " AND tbl_beneficio.estado = $estado";
                else
                    $where .= "tbl_beneficio.estado = $estado";
            }

            $join = array(
                'tbl_empresa as E, E.id = tbl_beneficio.id_empresa', 'tbl_categoria_beneficio as  C, C.id=tbl_beneficio.id_beneficio_categoria '
            );
            if ($where == "") {
                $params = array(
                    "select" => "tbl_beneficio.id,tbl_beneficio.nombre, tbl_beneficio.descripcion,
                    tbl_beneficio.telefono, tbl_beneficio.correo,  tbl_beneficio.estado,C.nombre as nombre_categoria,tbl_beneficio.tipo,tbl_beneficio.fecha_inicio,tbl_beneficio.fecha_fin",
                    "join" => $join,
                    "order" => "tbl_beneficio.fecha_registro desc"
                );
            } else {
                $params = array(
                    "select" => "tbl_beneficio.id,tbl_beneficio.nombre, tbl_beneficio.descripcion,
                    tbl_beneficio.telefono, tbl_beneficio.correo,  tbl_beneficio.estado,C.nombre as nombre_categoria,tbl_beneficio.tipo,tbl_beneficio.fecha_inicio,tbl_beneficio.fecha_fin",
                    "where" => $where,
                    "join" => $join,
                    "order" => "tbl_beneficio.fecha_registro desc"
                );
            }

            $categorias = array(
                "select" => "*"
            );

            $empresas = array(
                "select" => "*"
            );

            $empresa = $this->Empresa_model->search($empresas);
            $categoria = $this->Categoria_beneficio_model->search($categorias);

            $total_beneficios = $this->Beneficio_model->total_records($params);

            $beneficio = $this->Beneficio_model->search_data($params, $start, $this->elementoPorPagina);
            $arrayMigaPan = array(
                array("nombre" => "Beneficio"), array("nombre" => "Listar", 'active' => true));

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Beneficio/buscar'), $total_beneficios);
            $this->arrayVista['tituloPagina'] = "Lista de Beneficios";
            $this->arrayVista['empresas'] = $empresa;
            $this->arrayVista['categorias'] = $categoria;
            $this->arrayVista['beneficios'] = $beneficio;
            $this->arrayVista['vista'] = 'backend/beneficio/listar_view';
            $this->cargarVistaBackend();


        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function agregar()
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            $empresas = array(
                "select" => "*", "where" => "estado = 1"
            );

            $empresa = $this->Empresa_model->search($empresas);

            if ($this->input->post()) {
                $id_categoria = $this->input->post("categoria");
                $id_empresa = $this->input->post("empresa");
                $nombre = $this->input->post("nombre");
                $descripcion = $this->input->post("descripcion");
                $restriccion = $this->input->post("restriccion");

                $tipo = $this->input->post("tipo");
                $tipo_locales = $this->input->post("tipo_locales");
                $locales = $this->input->post("locales");
                $direccion = $this->input->post("direccion");
                $telefono = $this->input->post("telefono");
                $correo = $this->input->post("correo");
                $latitud = $this->input->post("latitud");
                $longitud = $this->input->post("longitud");
                $puntos = $this->input->post("puntos");
                $monto_adicional = $this->input->post("monto_adicional");
                $slug = $this->input->post("slug");
                $orden = $this->input->post("orden");
                $compartir = $this->input->post("compartir");
                $fecha_inicio = $this->input->post("fecha_inicio");
                $fecha_fin = $this->input->post("fecha_fin");
                $estado = $this->input->post("estado");

                if ($empresa != null && sizeof($empresa) == 1){
                    $id_empresa =  $empresa[0]->id;
                }

                if ($tipo_locales != STATUS_DELIVERY){
                    $direccion = "";
                    $telefono = "";
                    $correo = "";
                    $latitud = "";
                    $longitud = "";
                }


                $data_beneficio = array(
                    'id_beneficio_categoria' => $id_categoria,
                    'id_empresa' => $id_empresa,
                    'nombre' => $nombre,
                    'descripcion' => $descripcion,
                    'restriccion' => $restriccion,
                    'tipo' => $tipo,
                    'puntos' => $puntos,
                    'puntos_descripcion' => $this->lang->line('constant_points'),
                    'monto_adicional_descripcion' => $monto_adicional,
                    'tipo_locales' => $tipo_locales,
                    'telefono' => $telefono,
                    'correo' => $correo,
                    'direccion' => $direccion,
                    'latitud'=>$latitud,
                    'longitud'=>$longitud,
                    'slug' => "",
                    'orden' => $orden,
                    'compartir_redes' => $compartir,
                    'fecha_registro' => date("Y-m-d H:i:s"),
                    'fecha_inicio' => $fecha_inicio,
                    'fecha_fin' => $fecha_fin,
                    'estado' => $estado
                );


                if (isset($data_beneficio)) {
                    try {
                        if ($tipo_locales == STATUS_ALL_LOCALS){

                            $locales_empresa= array(
                                "select" => "*", "where" => "estado = 1 and id_empresa = $id_empresa"
                            );
                            $locales_beneficio = $this->Local_model->search($locales_empresa);

                            if ($locales_beneficio != null && sizeof($locales_beneficio) > 0){
                                $id_beneficio = $this->Beneficio_model->insert($data_beneficio);
                                $locales_array = array();
                                for($i=0 ;  $i<sizeof($locales_beneficio) ; $i++){
                                    $local = array(
                                        'id_beneficio' => $id_beneficio,
                                        'id_local' => $locales_beneficio[$i]->id,
                                        'fecha_registro' => date("Y-m-d H:i:s"),
                                        'estado' => ESTADO_ACTIVO);
                                    $locales_array[$i]= $local;
                                }
                                $this->Local_beneficio_model->insert_lote($locales_array);
                            }else{
                                $this->alert($this->lang->line('error_local_empresa_empty'),site_url($this->config->item('path_backend') . '/beneficio/agregar'));
                            }
                        }elseif ($tipo_locales == STATUS_SOME_LOCALS){
                            $id_beneficio = $this->Beneficio_model->insert($data_beneficio);
                            $locales_array = array();
                            for($i = 0 ;  $i < sizeof($locales) ; $i++){
                                $local = array(
                                    'id_beneficio' => $id_beneficio,
                                    'id_local' => $locales[$i],
                                    'fecha_registro' => date("Y-m-d H:i:s"),
                                    'estado' => ESTADO_ACTIVO);
                                $locales_array[$i]= $local;
                            }
                            $this->Local_beneficio_model->insert_lote($locales_array);
                        }else{
                            $this->Beneficio_model->insert($data_beneficio);
                        }

                        $this->alert($this->lang->line('mensaje_beneficio_agregado'), site_url($this->config->item('path_backend') . '/inicio'));
                    } catch (Exception $exception) {
                        $this->alert($this->lang->line('error_beneficio_agregar'));
                    }
                }

            }

            $categorias = array(
                "select" => "*", "where" => "estado = 1"
            );

            $locales = array(
                "select" => "id,nombre,tipo,id_empresa", "where" => "estado = 1"
            );

            $categoria = $this->Categoria_beneficio_model->search($categorias);
            $locales_empresa = $this->Local_model->search($locales);

            $arrayMigaPan = array(
                array("nombre" => "Beneficio", "url" => site_url($this->config->item('path_backend') . '/inicio')), array("nombre" => "Agregar", 'active' => true));

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['tituloPagina'] = "Agregar Beneficio";
            $this->arrayVista['empresas'] = $empresa;
            $this->arrayVista['categorias'] = $categoria;
            $this->arrayVista['locales'] = $locales_empresa;
            $this->arrayVista['vista'] = 'backend/beneficio/agregar_view';
            $this->cargarVistaBackend();


        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function editar($id_beneficio)
    {
        
        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {

                $id_categoria = $this->input->post("categoria");
                $id_empresa = $this->input->post("empresa");
                $nombre = $this->input->post("nombre");
                $descripcion = $this->input->post("descripcion");
                $restriccion = $this->input->post("restriccion");

                $tipo = $this->input->post("tipo");
                $tipo_locales = $this->input->post("tipo_locales");
                $locales = $this->input->post("locales");
                $direccion = $this->input->post("direccion");
                $telefono = $this->input->post("telefono");
                $correo = $this->input->post("correo");
                $latitud = $this->input->post("latitud");
                $longitud = $this->input->post("longitud");
                $puntos = $this->input->post("puntos");
                $monto_adicional = $this->input->post("monto_adicional");
                $slug = $this->input->post("slug");
                $orden = $this->input->post("orden");
                $compartir = $this->input->post("compartir");
                $fecha_inicio = $this->input->post("fecha_inicio");
                $fecha_fin = $this->input->post("fecha_fin");
                $estado = $this->input->post("estado");

                if ($tipo_locales != STATUS_DELIVERY){
                    $direccion = "";
                    $telefono = "";
                    $correo = "";
                    $latitud = "";
                    $longitud = "";
                }

                $data_beneficio = array(
                    'id_beneficio_categoria' => $id_categoria,
                    'id_empresa' => $id_empresa,
                    'nombre' => $nombre,
                    'descripcion' => $descripcion,
                    'restriccion' => $restriccion,
                    'tipo' => $tipo,
                    'puntos' => $puntos,
                    'puntos_descripcion' => $this->lang->line('constant_points'),
                    'monto_adicional_descripcion' => $monto_adicional,
                    'tipo_locales' => $tipo_locales,
                    'telefono' => $telefono,
                    'correo' => $correo,
                    'direccion' => $direccion,
                    'latitud'=>$latitud,
                    'longitud'=>$longitud,
                    'slug' => $slug,
                    'orden' => $orden,
                    'compartir_redes' => $compartir,
                    'fecha_modificacion' => date("Y-m-d H:i:s"),
                    'fecha_inicio' => $fecha_inicio,
                    'fecha_fin' => $fecha_fin,
                    'estado' => $estado
                );

                if ($this->Beneficio_model->update($id_beneficio, $data_beneficio)) {
                    $where_string = "id_beneficio =".$id_beneficio;
                    $this->Local_beneficio_model->delete_where($where_string);
                    if ($tipo_locales == STATUS_ALL_LOCALS){

                        $locales_empresa= array(
                            "select" => "*", "where" => "estado = 1 and id_empresa = $id_empresa"
                        );
                        $locales_beneficio = $this->Local_model->search($locales_empresa);

                        if ($locales_beneficio != null && sizeof($locales_beneficio) > 0){

                            $locales_array = array();
                            for($i=0 ;  $i<sizeof($locales_beneficio) ; $i++){
                                $local = array(
                                    'id_beneficio' => $id_beneficio,
                                    'id_local' => $locales_beneficio[$i]->id,
                                    'fecha_registro' => date("Y-m-d H:i:s"),
                                    'estado' => ESTADO_ACTIVO);
                                $locales_array[$i]= $local;
                            }
                            $this->Local_beneficio_model->insert_lote($locales_array);
                        }else{
                            $this->alert($this->lang->line('error_local_empresa_empty'),site_url($this->config->item('path_backend') . '/beneficio/agregar'));
                        }
                    }elseif ($tipo_locales == STATUS_SOME_LOCALS){
                        $locales_array = array();
                        for($i = 0 ;  $i < sizeof($locales) ; $i++){
                            $local = array(
                                'id_beneficio' => $id_beneficio,
                                'id_local' => $locales[$i],
                                'fecha_registro' => date("Y-m-d H:i:s"),
                                'estado' => ESTADO_ACTIVO);
                            $locales_array[$i]= $local;
                        }
                        $this->Local_beneficio_model->insert_lote($locales_array);
                    }

                    $this->alert($this->lang->line('mensaje_beneficio_editado'), site_url($this->config->item('path_backend') . '/inicio'));

                }
                $parametros = $data_beneficio;
                $accion = "editar beneficios";
                $usuario =  $this->session->userdata('correo');
                guardar_log_cms($parametros,$accion,$usuario);
            }

            $beneficio = array(
                "select" => "*",
                "where" => "id = '" . $id_beneficio . "'"
            );
            $categorias = array(
                "select" => "*", "where" => "estado = 1"
            );
            $empresas = array(
                "select" => "*", "where" => "estado = 1"
            );

            $locales = array(
                "select" => "id,nombre,tipo,id_empresa", "where" => "estado = 1"
            );

            $join=array(
                'tbl_local as L,L.id=tbl_local_beneficio.id_local'
            );
            $paramsLocalBeneficio = array(
                "select" => "tbl_local_beneficio.id,tbl_local_beneficio.fecha_registro,tbl_local_beneficio.estado,tbl_local_beneficio.id_local, L.nombre,tbl_local_beneficio.id_beneficio",
                "where" => "tbl_local_beneficio.id_beneficio = '" . $id_beneficio . "'",
                "join" =>$join,
                "order"=> "tbl_local_beneficio.fecha_registro desc"
            );
        

            $local_beneficio = $this->Local_beneficio_model->search($paramsLocalBeneficio);

           

            $locales_empresa = $this->Local_model->search($locales);

            $empresa = $this->Empresa_model->search($empresas);
            $categoria = $this->Categoria_beneficio_model->search($categorias);
            $beneficio = $this->Beneficio_model->get_search_row($beneficio);
            $arrayMigaPan = array(
                array("nombre" => "Beneficio", "url" => site_url($this->config->item('path_backend') . '/inicio')), array("nombre" => "Editar", 'active' => true));

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['tituloPagina'] = "Editar Beneficio";
            $this->arrayVista['empresas'] = $empresa;
            $this->arrayVista['categorias'] = $categoria;
            $this->arrayVista['beneficio'] = $beneficio;
            $this->arrayVista['locales'] = $locales_empresa;
            $this->arrayVista['local_beneficio'] = $local_beneficio;
            $this->arrayVista['vista'] = 'backend/beneficio/editar_view';
            $this->cargarVistaBackend();


        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function carga_masiva()
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {
                $id_empresa = $this->input->post("empresa");
                $excel = $this->input->post("excel");
                $img = $this->input->post("img");
                $pathExcel = "./upload/archivos/excel/beneficio/" . date("Y-m-d");

                if (!is_dir($pathExcel))
                    mkdir($pathExcel, 0775, true);
                if ($_FILES["excel"]["size"] > 0) {
                    $nombreArchivoExcel = "excel";
                    $resultadoExcel = $this->subirArchivo($nombreArchivoExcel, $pathExcel,"", 'xls|xlsx', 2024);
                    $excel = "";

                    if (array_key_exists("upload_data", $resultadoExcel)) {
                        if (isset($resultadoExcel["upload_data"]["file_name"])) {

                            $this->load->library('excel/PHPExcel');
                            $objPHPExcel = PHPExcel_IOFactory::load($resultadoExcel['upload_data']['full_path']);
                        }
                    }
                }

                $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
                foreach ($cell_collection as $cell) {

                    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                }

                for ($i = 0; $i <= 18; $i++) {
                    for ($j = 2; $j <= $row; $j++) {

                        $datos[$j][$i] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($i, $j)->getCalculatedValue();
                    }
                }

                $datos = array_values($datos);

                for ($i = 0; $i < sizeof($datos); $i++) {

                    $categoria = array("select" => "*",
                        "where" => "nombre = '" . trim($datos[$i][0]) . "'");

                    $categoria_beneficio = $this->Categoria_beneficio_model->get_search_row($categoria);

                    if (!count($categoria_beneficio) > 0 && !isset($categoria_beneficio->id)) {
                        $array_cat_benef = array(
                            "nombre" => $datos[$i][0],
                            "fecha_registro" => date("Y-m-d H:i:s"),
                            "estado" => '1');

                        $this->Categoria_beneficio_model->insert($array_cat_benef);
                        $id_cate_benef = $this->db->insert_id();

                    } else {
                        $id_cate_benef = $categoria_beneficio->id;
                    }

                    $fecha_ini = PHPExcel_Shared_Date::ExcelToPHPObject($datos[$i][4]);
                    $fecha_fin = PHPExcel_Shared_Date::ExcelToPHPObject($datos[$i][5]);

                    if (strnatcasecmp($datos[$i][11], "Mixto") == 0) {
                        $tipoBeneficio = 2;
                    } else if (strnatcasecmp($datos[$i][11], "Canje por puntos") == 0) {
                        $tipoBeneficio = 1;
                    }


                    $array_benef = array(
                        "id_beneficio_categoria" => $id_cate_benef,
                        "id_empresa" => $id_empresa,
                        "nombre" => $datos[$i][1],
                        "descripcion" => $datos[$i][2],
                        "restriccion" => $datos[$i][3],
                        "telefono" => $datos[$i][8],
                        "correo" => $datos[$i][9],
                        "direccion" => $datos[$i][10],
                        "tipo" => $tipoBeneficio,
                        "puntos" => $datos[$i][12],
                        "monto_adicional" => $datos[$i][13],
                        "orden" => $datos[$i][14],
                        "fecha_inicio" => $fecha_ini->format('Y-m-d'),
                        "fecha_fin" => $fecha_fin->format('Y-m-d'),
                        "fecha_registro" => date("Y-m-d H:i:s"),
                        "estado" => "1",
                    );
                    $this->Beneficio_model->insert($array_benef);
                    $id_beneficio = $this->db->insert_id();

                    if ($datos[$i][6] != null) {
                        $info_bene = array();
                        $info_bene[] = explode("\n", $datos[$i][6]);
                        $sub = count($info_bene, COUNT_RECURSIVE);
                        $subarray = $sub - 1;
                        for ($j = 0; $j < $subarray; $j++) {
                            $informacion = array();
                            $informacion[] = explode("|", $info_bene[0][$j]);
                            if (isset($informacion)) {

                                $array_info_benef = array(
                                    "id_beneficio" => $id_beneficio,
                                    "campo" => $informacion[0][0],
                                    "valor" => $informacion[0][1],
                                    "fecha_registro" => date("Y-m-d H:i:s"),
                                    "estado" => "1",
                                );

                            }
                            $this->Informacion_adicional_beneficio_model->insert($array_info_benef);
                        }
                    }

                    if ($datos[$i][7] != null) {
                        $local_bene = array();
                        $local_bene[] = explode("\n", $datos[$i][7]);
                        $sub = count($local_bene, COUNT_RECURSIVE);
                        $subarray = $sub - 1;
                        for ($j = 0; $j < $subarray; $j++) {
                            $local = array();
                            $local[] = explode("|", $local_bene[0][$j]);

                            $local_query = array("select" => "*",
                                "where" => "nombre = '" . trim($local[0][0]) . "'");

                            $locales = $this->Local_model->get_search_row($local_query);

                            if (!count($locales) > 0 && !isset($locales->id)) {
                                $array_local = array(
                                    "id_empresa" => $id_empresa,
                                    "nombre" => $local[0][0],
                                    "direccion" => $local[0][1],
                                    "latitud" => $local[0][2],
                                    "longitud" => $local[0][3],
                                    "correo" => $local[0][4],
                                    "telefono" => $local[0][5],
                                    "fecha_registro" => date("Y-m-d H:i:s"),
                                    "estado" => '1'
                                );
                                $this->Local_model->insert($array_local);
                                $id_local = $this->db->insert_id();

                            } else {
                                $id_local = $locales->id;
                            }

                            $array_local_beneficio = array(
                                "id_local" => $id_local,
                                "id_beneficio" => $id_beneficio,
                                "fecha_registro" => date("Y-m-d H:i:s"),
                                "estado" => '1'
                            );
                            $this->Local_beneficio_model->insert($array_local_beneficio);

                        }
                    }

                    for ($p = 15; $p <= 18; $p++) {

                        if ($p == 15) {
                            $tipo = 1;
                            $plataforma = 1;
                        } elseif($p == 16) {
                            $tipo = 2;
                            $plataforma = 1;
                        } elseif($p == 17) {
                            $tipo = 1;
                            $plataforma= 2;
                        } elseif($p == 18) {
                            $tipo = 2;
                            $plataforma= 2;
                        }

                        $array_foto_beneficio = array(
                            "id_beneficio" => $id_beneficio,
                            "foto" => 'upload/beneficio/' . date("Y-m-d").'/'.$datos[$i][$p],
                            "tipo" => $tipo,
                            "plataforma" => $plataforma,
                            "fecha_registro" => date("Y-m-d H:i:s"),
                            "estado" => "1"

                        );
                        $this->Foto_beneficio_model->insert($array_foto_beneficio);
                    }

                }

                $this->alert($this->lang->line('mensaje_beneficio_carga_masiva'), site_url($this->config->item('path_backend') . '/Beneficio/carga_masiva'));

            }

            $empresas = array(
                "select" => "*", "where" => "estado = 1"
            );

            $empresa = $this->Empresa_model->search($empresas);

            $arrayMigaPan = array(
                array("nombre" => "Beneficio", "url" => site_url($this->config->item('path_backend') . '/inicio')), array("nombre" => "Carga Masiva", 'active' => true));

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;
            $this->arrayVista['tituloPagina'] = "Carga Masiva";
            $this->arrayVista['empresas'] = $empresa;

            $this->arrayVista['vista'] = 'backend/beneficio/carga_masiva_view';
            $this->cargarVistaBackend();
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function carga_imagenes()
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {

                $img = $this->input->post("img");
                $pathArchivos = "./upload/archivos/zip/beneficio/" . date("Y-m-d");
                $pathImagenes = "./upload/beneficio/" . date("Y-m-d");

                if (!is_dir($pathArchivos))
                    mkdir($pathArchivos, 0775, true);
                if ($_FILES["img"]["size"] > 0) {
                    $nombreArchivoImagenes = "img";
                    $resultadoImagenes = $this->subirArchivo($nombreArchivoImagenes, $pathArchivos,"", 'zip', 2024);
                    $img = "";

                    if (array_key_exists("upload_data", $resultadoImagenes)) {
                        if (isset($resultadoImagenes["upload_data"]["file_name"])) {
                            $zip = new ZipArchive;
                            $file = $resultadoImagenes['upload_data']['full_path'];
                            if ($zip->open($file) === TRUE) {
                                $zip->extractTo($pathImagenes);
                            }
                            $zip->close();
                        }
                    }
                }
                $this->alert($this->lang->line('mensaje_carga_fotos'), site_url($this->config->item('path_backend') . '/Beneficio/carga_masiva'));
            }
        }
    }

    public function export()
    {
        if ($this->session->userdata('logged_in') === TRUE) {

            $nombre = $this->input->get('nombre');
            $tipo = $this->input->get('tipo');
            $fecha_inicio = $this->input->get('fecha_inicio');
            $fecha_fin = $this->input->get('fecha_fin');
            $tipo = $this->input->get('tipo');
           // $empresa = $this->input->get('empresa');
            $categoria = $this->input->get('categoria');
            $estado = $this->input->get('estado');
          

            $where = "";


            if ($nombre != "" ) {

               $this->arrayVista['nombre'] = $nombre;
                if ($where != "") {
                    $where .= " AND tbl_beneficio.nombre LIKE '%" . $nombre . "%' ";
                } else {
                    $where = "tbl_beneficio.nombre LIKE '%" . $nombre . "%' ";
                }
            }


            if ($tipo != "" ) {
                $this->arrayVista['tipo'] = $tipo;
                if ($where != "")
                    $where .= " AND tbl_beneficio.tipo = $tipo";
                else
                    $where .= "tbl_beneficio.tipo = $tipo";
            }

           /* if ($empresa != "" ) {
                $this->arrayVista['empresa'] = $empresa;
                if ($where != "")
                    $where .= " AND E.id = $empresa";
                else
                    $where .= "E.id = $empresa";
            }*/

            if ($categoria != "" ) {
                $this->arrayVista['categoria'] = $categoria;
                if ($where != "")
                    $where .= " AND C.id = $categoria";
                else
                    $where .= "C.id = $categoria";
            }

            if ($fecha_inicio != "" ) {
                
                list($anio,$mes,$dia) = explode('-',$fecha_inicio);
                if ($where != "") {

                    $where .= " AND DATE(tbl_beneficio.fecha_inicio) >= '$anio-$mes-$dia'  ";
                } else {

                    $where = "DATE(tbl_beneficio.fecha_inicio)  >= '$anio-$mes-$dia' ";
                }
            }

            if ($fecha_fin != "" ) {

                list($anio,$mes,$dia) = explode('-',$fecha_fin);
                if ($where != "") {

                    $where .= " AND DATE(tbl_beneficio.fecha_fin) <= '$anio-$mes-$dia'  ";

                } else {
                    $where = "DATE(tbl_beneficio.fecha_fin)  <= '$anio-$mes-$dia' ";
                }
            }

            if ($estado != "" ) {
                $this->arrayVista['estado'] = $estado;
                if ($where != "")
                    $where .= " AND tbl_beneficio.estado = $estado";
                else
                    $where .= "tbl_beneficio.estado = $estado";
            }

            $join = array(
                'tbl_empresa as E, E.id = tbl_beneficio.id_empresa',
                'tbl_categoria_beneficio as  C, C.id = tbl_beneficio.id_beneficio_categoria'
            );

            if ($where == "") {
                $params = array(
                    "select" => "C.nombre as categoria ,E.nombre as empresa , tbl_beneficio.nombre, tbl_beneficio.descripcion,
                     tbl_beneficio.restriccion, tbl_beneficio.telefono, tbl_beneficio.correo, tbl_beneficio.direccion, tbl_beneficio.tipo,
                      tbl_beneficio.puntos, tbl_beneficio.monto_adicional_descripcion, tbl_beneficio.slug, tbl_beneficio.orden, tbl_beneficio.compartir_redes,
                       tbl_beneficio.fecha_inicio, tbl_beneficio.fecha_fin, tbl_beneficio.fecha_registro, tbl_beneficio.estado",
                    "join" => $join,
                    "order" => "tbl_beneficio.fecha_registro desc"
                );
            } else {
                $params = array(
                    "select" => "C.nombre as categoria ,E.nombre as empresa , tbl_beneficio.nombre, tbl_beneficio.descripcion,
                     tbl_beneficio.restriccion, tbl_beneficio.telefono, tbl_beneficio.correo, tbl_beneficio.direccion, tbl_beneficio.tipo,
                      tbl_beneficio.puntos, tbl_beneficio.monto_adicional_descripcion, tbl_beneficio.slug, tbl_beneficio.orden, tbl_beneficio.compartir_redes,
                       tbl_beneficio.fecha_inicio, tbl_beneficio.fecha_fin, tbl_beneficio.fecha_registro, tbl_beneficio.estado",
                    "where" => $where,
                    "join" => $join,
                    "order" => "tbl_beneficio.fecha_registro desc"
                );
            }

            $beneficios = $this->Beneficio_model->search($params);


            $data = array();
            foreach ($beneficios as $beneficio) {
                $tipo="";
                $redes="";
                $estado="";
                if ($beneficio->tipo == 1){
                    $tipo = "Canje por puntos";
                }else{
                    $tipo = "Mixto";
                }

                if ($beneficio->estado== 1){
                    $estado = "Activo";
                }else{
                    $estado = "Inactivo";
                }


                array_push($data, array(
                    utf8_decode("Categoría") => isset($beneficio->categoria)?utf8_decode($beneficio->categoria):"-",
                    utf8_decode("Empresa") => isset($beneficio->empresa)?utf8_decode($beneficio->empresa):"-",
                    utf8_decode("Nombre") => isset($beneficio->nombre)?utf8_decode($beneficio->nombre):"-",
                    utf8_decode("Descripción") => isset($beneficio->descripcion)?utf8_decode($beneficio->descripcion):"-",
                    utf8_decode("Restricción") => isset($beneficio->restriccion)?utf8_decode($beneficio->restriccion):"-",
                    utf8_decode("Teléfono") => isset($beneficio->telefono)?utf8_decode($beneficio->telefono):"-",
                    utf8_decode("Correo") => isset($beneficio->correo)?utf8_decode($beneficio->correo):"-",
                    utf8_decode("Dirección") => isset($beneficio->direccion)?utf8_decode($beneficio->direccion):"-",
                    utf8_decode("Tipo") => isset($tipo)?utf8_decode($tipo):"-",
                    utf8_decode("Puntos") => isset($beneficio->puntos)?utf8_decode($beneficio->puntos):"0",
                    utf8_decode("Monto Adicional") => isset($beneficio->monto_adicional_descripcion)?utf8_decode($beneficio->monto_adicional_descripcion):"0",
                    utf8_decode("Orden") => isset($beneficio->orden)?utf8_decode($beneficio->orden):"-",
                    utf8_decode("Fecha de Inicio") => date("d/m/Y", strtotime($beneficio->fecha_inicio)),
                    utf8_decode("Fecha de Finalización") => date("d/m/Y", strtotime($beneficio->fecha_fin)),
                    utf8_decode("Fecha de Registro") => date("d/m/Y h:i:s a", strtotime($beneficio->fecha_registro)),
                    utf8_decode("Estado") => $estado,
                ));
            }

            function filterData(&$str)
            {
                $str = preg_replace("/\t/", "\\t", $str);
                $str = preg_replace("/\r?\n/", "\\n", $str);
                if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
            }             
                
            $fileName = "beneficios-". date('YmdHis') . ".xls";
                
            header("Content-Disposition: attachment; filename=\"$fileName\"");
            header("Content-Type: application/vnd.ms-excel, charset=utf-8");
                
            $flag = false;

            foreach($data as $row) {
                // print_r( array_values($row))."<br>";
                if(!$flag) {
                    
                    echo implode("\t", array_keys($row)) . "\n";
                    $flag = true;
                }
                    
                array_walk($row, 'filterData');
                echo implode("\t", array_values($row)) . "\n";
            }
                
            exit;

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }
}
