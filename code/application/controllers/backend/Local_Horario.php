<?php
/**
 * Created by PhpStorm.
 * User: Mediabyte
 * Date: 05/05/2016
 * Time: 02:18 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Local_horario extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model("Local_model");
        $this->load->model("horario_reserva_model", "horario_reserva_model");
        $this->lang->load('backend_error_lang', 'spanish');
        $this->lang->load('backend_mensajes_lang', 'spanish');
        $this->load->helper('url');

    }

    public function listar($start = 0)
    {
        if ($this->session->userdata('logged_in') === TRUE)
        {
            $id_local = $this->input->post('id_local');
            $id_empresa = $this->input->post('id_empresa');

            $params = array(
                "select" => "id,dia,stock,hora_inicio,hora_fin,estado,fecha_registro",
                "where" => "id_local = '" . $id_local . "'",
                "order" => "fecha_registro desc"
            );

            $total_local_horario = $this->horario_reserva_model->total_records($params);

            $local_horario = $this->horario_reserva_model->search_data($params, $start, $this->elementoPorPagina);
            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Local_Horario'), $total_local_horario);
            $this->arrayVista['local'] = $id_local;
            $this->arrayVista['empresa'] = $id_empresa;
            $this->arrayVista['local_horario'] = $local_horario;
            $this->cargarVistaListaAjax('backend/local_horario/listar_view');
        } else
        {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function buscar($start = 0)
    {

        $id_local = $this->input->post('id_local');
        $id_empresa = $this->input->post('id_empresa');
        $dia = $this->input->post('dia');
        $estado = $this->input->post('estado');
        $where = "";

        if ($dia != "")
        {
            $this->arrayVista['dia'] = $dia;
            if ($where != "") $where .= " AND dia = $dia"; else
                $where .= "dia = $dia";
        }

        if ($estado != "")
        {
            $this->arrayVista['estado'] = $estado;
            if ($where != "") $where .= " AND estado = $estado"; else
                $where .= "estado = $estado";
        }


        if ($where == "")
        {

            $params = array(
                "select" => "id,dia,stock,hora_inicio,hora_fin,estado,fecha_registro",
                "where" => "id_local = '" . $id_local . "'",
                "order" => "fecha_registro desc"
            );
            
        } else
        {

            $params = array(
                "select" => "id,dia,stock,hora_inicio,hora_fin,estado,fecha_registro",
                "where" => "id_local = '" . $id_local . "' AND " . $where,
                "order" => "fecha_registro desc"
            );
            
        }

        $total_local_horario = $this->horario_reserva_model->total_records($params);

        $local_horario = $this->horario_reserva_model->search_data($params, $start, $this->elementoPorPagina);
        $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Local_Horario/buscar'), $total_local_horario);
        $this->arrayVista['tituloPagina'] = "Lista de Horario de locales";
        $this->arrayVista['local'] = $id_local;
        $this->arrayVista['empresa'] = $id_empresa;
        $this->arrayVista['local_horario'] = $local_horario;

        $this->cargarVistaListaAjax('backend/local_horario/listar_view');

    }

    public function agregar_view($id_empresa, $id_local)
    {
        if ($this->session->userdata('logged_in') === TRUE)
        {
            $this->arrayVista['empresa'] = $id_empresa;
            $this->arrayVista['local'] = $id_local;
            $this->arrayVista['tituloPagina'] = "Agregar horario de local ";
            $this->arrayVista['vista'] = 'backend/local_horario/agregar_view';
            $this->cargarVistaBackend();

        } else
        {

            redirect(site_url($this->config->item('path_backend')));

        }
    }

    public function agregar($id_empresa, $id_local)
    {
        if ($this->session->userdata('logged_in') === TRUE)
        {
            $dia = $this->input->post("dia");
            $stock = $this->input->post("stock");
            $hora_inicio = $this->input->post("hora_inicio");
            $hora_fin = $this->input->post("hora_fin");
            $estado = $this->input->post("estado");

            $time_start= DateTime::createFromFormat( 'H:i A', $hora_inicio);
            $hora_inicio = $time_start->format( 'H:i:s');

            $time_end= DateTime::createFromFormat( 'H:i A', $hora_fin);
            $hora_fin = $time_end->format( 'H:i:s');

           

            $data_local_horario = array(
                'id_local' => $id_local,
                'dia' => $dia,
                'stock' => $stock,
                'hora_inicio' => $hora_inicio,
                'hora_fin' => $hora_fin,
                'estado' => $estado,
                'fecha_registro' => date("Y-m-d H:i:s")
            );

            if (isset($data_local_horario))
            {
                try
                {
                    $this->horario_reserva_model->insert($data_local_horario);
                    $this->alert($this->lang->line('mensaje_local_horario_agregado'), site_url($this->config->item('path_backend') . '/Local_Empresa/editar/' . $id_empresa . '/' . $id_local . '/H'));
                } catch (Exception $exception)
                {
                    $this->alert($this->lang->line('error_local_horario_agregar'));
                }
            }

        } else
        {
            redirect(site_url($this->config->item('path_backend')));
        }
    }
    
    public function check_hour(){

        $hora_inicio = $this->input->post("hora_inicio",TRUE);
        $hora_fin = $this->input->post("hora_fin",TRUE);
        $id_local = $this->input->post("id_local",TRUE);
        $id_dia = $this->input->post("id_dia",TRUE);
        $id_horario = $this->input->post("id_horario",TRUE);
        $estado =  $this->input->post("estado",TRUE);

        if (isset($id_dia)  && $id_dia != ""){

            if ($estado == ESTADO_ACTIVO){

                $where_add="";
                $where_id_horario ="";
                if ($id_horario !=null || $id_horario != ""){
                    $where_id_horario = " and id != ".$id_horario."";
                }

                $where_add =  "id_local = '" .$id_local."' and estado = '".ESTADO_ACTIVO ."'";


                if ($hora_inicio < $hora_fin){
                    $params_inicio = array(
                        "select" => "hora_inicio,hora_fin",
                        "where" => "dia = '".$id_dia."' and hora_inicio >= '".$hora_inicio."' and hora_inicio < '".$hora_fin ."' and " . $where_add . $where_id_horario
                    );

                    $params_fin = array(
                        "select" => "hora_inicio,hora_fin",
                        "where" => "dia = '".$id_dia."' and hora_fin > '".$hora_inicio."' and hora_fin <= '" . $hora_fin ."' and " . $where_add . $where_id_horario
                    );
                }else{
                    if ($id_dia != 7){
                        $params_inicio = array(
                            "select" => "hora_inicio,hora_fin",
                            "where" => "((dia = '".$id_dia."' and hora_inicio >= '".$hora_inicio."' and hora_inicio < '23:59')  or 
                            (dia = '".($id_dia+1)."' and hora_inicio >= '00:00' and hora_inicio < '".$hora_fin."')) and " . $where_add . $where_id_horario
                        );

                        $params_fin = array(
                            "select" => "hora_inicio,hora_fin",
                            "where" => "((dia = '".$id_dia."' and hora_fin > '".$hora_inicio."' and hora_fin <= '23:59')  or 
                            (dia = '".($id_dia+1)."' and hora_fin > '00:00' and hora_fin <= '".$hora_fin."')) and " . $where_add . $where_id_horario
                        );
                    }else{
                        $params_inicio = array(
                            "select" => "hora_inicio,hora_fin",
                            "where" => "((dia = '".$id_dia."' and hora_inicio >= '".$hora_inicio."' and hora_inicio < '23:59')  or 
                            (dia = '1' and hora_inicio >= '00:00' and hora_inicio < '".$hora_fin."')) and " . $where_add . $where_id_horario
                        );

                        $params_fin = array(
                            "select" => "hora_inicio,hora_fin",
                            "where" => "((dia = '".$id_dia."' and hora_fin > '".$hora_inicio."' and hora_fin <= '23:59')  or 
                            (dia = '1' and hora_fin > '00:00' and hora_fin <= '".$hora_fin."')) and " . $where_add . $where_id_horario
                        );
                    }
                }

                $horario_inicio = $this->horario_reserva_model->total_records($params_inicio);
                $horario_fin = $this->horario_reserva_model->total_records($params_fin);


                if ($horario_inicio > 0  || $horario_fin > 0 ){

                    $error = array("cod"=>1);
                }else{
                    $error = array("cod"=>0);
                }
            }else{
                $error = array("cod"=>0);

            }
        }else{
            $error = array("cod"=>0);
        }


        echo json_encode($error);

    }

    public function editar_view($id_empresa, $id_local, $id_local_horario)

    {
        if ($this->session->userdata('logged_in') === TRUE)
        {
            $local_horario = array(
                "select" => "id,dia,stock,hora_inicio,hora_fin,estado",
                "where" => "id = '" . $id_local_horario . "'"
            );

            $local_horario = $this->horario_reserva_model->get_search_row($local_horario);

            $this->arrayVista['local'] = $id_local;
            $this->arrayVista['empresa'] = $id_empresa;
            $this->arrayVista['id_local_horario'] = $id_local_horario;
            $this->arrayVista['local_horario'] = $local_horario;
            $this->arrayVista['tituloPagina'] = "Editar Horario del local";
            $this->arrayVista['vista'] = 'backend/local_horario/editar_view';
            $this->cargarVistaBackend();

        } else
        {

            redirect(site_url($this->config->item('path_backend')));

        }
    }

    public function editar($id_empresa, $id_local, $id_local_horario)
    {

        if ($this->session->userdata('logged_in') === TRUE)
        {

            $dia = $this->input->post("dia");
            $stock = $this->input->post("stock");
            $hora_inicio = $this->input->post("hora_inicio");
            $hora_fin = $this->input->post("hora_fin");
            $estado = $this->input->post("estado");

            $time_start= DateTime::createFromFormat( 'H:i A', $hora_inicio);
            $hora_inicio = $time_start->format( 'H:i:s');

            $time_end= DateTime::createFromFormat( 'H:i A', $hora_fin);
            $hora_fin = $time_end->format( 'H:i:s');


            $data_local_horario = array(
                'dia' => $dia,
                'stock' => $stock,
                'hora_inicio' => $hora_inicio,
                'hora_fin' => $hora_fin,
                'estado' => $estado,
                'fecha_modificacion' => date("Y-m-d H:i:s")
            );



            if ($this->horario_reserva_model->update($id_local_horario, $data_local_horario))
            {

                $this->alert($this->lang->line('mensaje_local_horario_editado'),site_url($this->config->item('path_backend') . '/Local_Empresa/editar/' . $id_empresa . '/' . $id_local . '/H'));
            }

        } else
        {
            redirect(site_url($this->config->item('path_backend')));

        }
    }

    public function export()

    {
        if ($this->session->userdata('logged_in') === TRUE) {

            $id_local = $this->input->get('id_local');
            $id_empresa = $this->input->get('id_empresa');
            $dia = $this->input->get('dia');
            $estado = $this->input->get('estado');


            $where = "";

            if ($dia != "") {
                $this->arrayVista['dia'] = $dia;
                if ($where != "")
                    $where .= " AND tbl_horario_reserva.dia = $dia";
                else
                    $where .= "tbl_horario_reserva.dia = $dia";
            }

            if ($estado != "") {
                $this->arrayVista['estado'] = $estado;
                if ($where != "")
                    $where .= " AND tbl_horario_reserva.estado = $estado";
                else
                    $where .= "tbl_horario_reserva.estado = $estado";
            }

            if ($where == "")
            {

                $params = array(
                    "select" => "tbl_horario_reserva.dia,tbl_horario_reserva.stock,tbl_horario_reserva.hora_inicio,tbl_horario_reserva.hora_fin,tbl_horario_reserva.estado,tbl_horario_reserva.fecha_registro,local.nombre as nombre_local",
                    "join"  =>  array("tbl_local local, local.id = tbl_horario_reserva.id_local"),
                    "where" => "tbl_horario_reserva.id_local = '" . $id_local . "'",
                    "order" => "tbl_horario_reserva.fecha_registro desc"
                );

            } else
            {

                $params = array(
                    "select" => "tbl_horario_reserva.dia,tbl_horario_reserva.stock,tbl_horario_reserva.hora_inicio,tbl_horario_reserva.hora_fin,tbl_horario_reserva.estado,tbl_horario_reserva.fecha_registro,local.nombre as nombre_local",
                    "join"  =>  array("tbl_local local, local.id = tbl_horario_reserva.id_local"),
                    "where" => "tbl_horario_reserva.id_local = '" . $id_local . "' AND " . $where,
                    "order" => "tbl_horario_reserva.fecha_registro desc"
                );



            }

            $local_horario = $this->horario_reserva_model->search($params);

            //retrive contries table data
            $data = array();
            foreach ($local_horario as $horario) {

                if ($horario->estado== 1){
                    $estado = "Activo";
                }else{
                    $estado = "Inactivo";
                }

                $day_week = unserialize (DAY_WEEK);

                for ($i=0 ; $i<sizeof($day_week) ; $i++ ){
                    if ($horario->dia == $day_week[$i]["id"]){
                        $dia = $day_week[$i]["dia"];
                    }
                }



                array_push($data, array(
                    utf8_decode("Local") => isset($horario->nombre_local)?utf8_decode($horario->nombre_local):"-",
                    utf8_decode("Dia") => isset($dia)?utf8_decode($dia):"-",
                    utf8_decode("Stock") => isset($horario->stock)?utf8_decode($horario->stock):"-",
                    utf8_decode("Hora Inicio") => isset($horario->hora_inicio)?utf8_decode($horario->hora_inicio):"-",
                    utf8_decode("Hora Fin") => isset($horario->hora_fin)?utf8_decode($horario->hora_fin):"-",
                    utf8_decode("Estado") => isset($estado)?$estado:"-",
                    utf8_decode("Fecha de Registro") => isset($horario->fecha_registro)?date("d/m/Y h:i:s a", strtotime($horario->fecha_registro)):"-",
                ));
            }
            //Fill data
            function filterData(&$str)
            {
                $str = preg_replace("/\t/", "\\t", $str);
                $str = preg_replace("/\r?\n/", "\\n", $str);
                if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
            }

            $fileName = "Horario de local-". date('YmdHis') . ".xls";

            header("Content-Disposition: attachment; filename=\"$fileName\"");
            header("Content-Type: application/vnd.ms-excel, charset=utf-8");

            $flag = false;

            foreach($data as $row) {
                // print_r( array_values($row))."<br>";
                if(!$flag) {

                    echo implode("\t", array_keys($row)) . "\n";
                    $flag = true;
                }

                array_walk($row, 'filterData');
                echo implode("\t", array_values($row)) . "\n";
            }

            exit;
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }


}
