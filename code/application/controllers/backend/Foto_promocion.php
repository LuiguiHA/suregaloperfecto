<?php
/**
 * Created by PhpStorm.
 * User: Mediabyte
 * Date: 02/05/2016
 * Time: 03:03 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Foto_promocion extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();


        $this->load->model("Foto_promocion_model");
        $this->lang->load('backend_error_lang', 'spanish');
        $this->lang->load('backend_mensajes_lang', 'spanish');
        $this->load->helper('url');


    }

    public function listar( $start = 0)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            $id_promocion = $this->input->post('id_promocion');
            
            $params = array(
                "select" => "*",
                "where" => "id_promocion = '" . $id_promocion . "' and estado != '".ESTADO_ELIMINADO."'",
                "order"=> "fecha_registro desc"
            );
            $total_foto_promocion = $this->Foto_promocion_model->total_records($params);

            $foto_promocion = $this->Foto_promocion_model->search_data($params, $start, $this->elementoPorPagina);
            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Foto_Promocion'), $total_foto_promocion);
            $this->arrayVista['tituloPagina'] = "Lista de Fotos";
            $this->arrayVista['promocion'] = $id_promocion;
            $this->arrayVista['foto_promocion'] = $foto_promocion;
            $this->cargarVistaListaAjax('backend/foto_promocion/listar_view');
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function buscar( $start = 0)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            $id_promocion = $this->input->post('id_promocion');
            $tipo = $this->input->post('tipo');
            $plataforma = $this->input->post('plataforma');

            $estado = $this->input->post('estado');
            $where = "";


            if ($tipo != "") {
                $this->arrayVista['tipo'] = $tipo;
                if ($where != "")
                    $where .= " AND tipo = $tipo";
                else
                    $where .= "tipo = $tipo";
            }


            if ($plataforma != "") {
                $this->arrayVista['plataforma'] = $plataforma;
                if ($where != "")
                    $where .= " AND plataforma = $plataforma";
                else
                    $where .= "plataforma = $plataforma";
            }

            if ($estado != "") {
                $this->arrayVista['estado'] = $estado;
                if ($where != "")
                    $where .= " AND estado = $estado";
                else
                    $where .= "estado = $estado";
            }else{
                if ($where != "")
                    $where .= " AND estado != '".ESTADO_ELIMINADO."'";
                else
                    $where .= "estado != '".ESTADO_ELIMINADO."'";
            }

            if ($where == "") {
                $params = array(
                    "select" => "*",
                    "where" => "id_promocion = '" . $id_promocion . "'",
                    "order"=> "fecha_registro desc"
                );
            } else {
                $params = array(
                    "select" => "*",
                    "where" => "id_promocion = '" . $id_promocion . "' AND " . $where,
                    "order"=> "fecha_registro desc"

                );
            }

            $total_foto_promocion = $this->Foto_promocion_model->total_records($params);

            $foto_promocion = $this->Foto_promocion_model->search_data($params, $start, $this->elementoPorPagina);
            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Foto_Promocion/buscar'), $total_foto_promocion);
            $this->arrayVista['tituloPagina'] = "Lista de Fotos";
            $this->arrayVista['promocion'] = $id_promocion;
            $this->arrayVista['foto_promocion'] = $foto_promocion;

            $this->cargarVistaListaAjax('backend/foto_promocion/listar_view');


        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function agregar($id_promocion)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {


                $foto = $this->input->post("foto");
                $tipo = $this->input->post("tipo");
                $plataforma = $this->input->post("plataforma");
                $estado = $this->input->post("estado");


                $pathImagen = "upload/promocion/" . date("Y-m-d");


                if (!is_dir($pathImagen))
                    mkdir($pathImagen, 0775, true);
                if ($_FILES["foto"]["size"] > 0) {
                    $nombreArchivoImagen = "foto";
                    $formato = substr(strrchr($_FILES["foto"]["name"],'.'),1);
                    $nombre_imagen = pathinfo($_FILES['foto']['name'], PATHINFO_FILENAME);
                    $nombre_imagen =  $nombre_imagen."_".$tipo."_".$plataforma.".".$formato;
                    $resultadoFoto = $this->subirArchivo($nombreArchivoImagen, $pathImagen,$nombre_imagen, 'jpeg|jpg|png', 2024);
                    $foto = "";

                    if (array_key_exists("upload_data", $resultadoFoto)) {
                        if (isset($resultadoFoto["upload_data"]["file_name"]))


                            $foto = $resultadoFoto["upload_data"]["orig_name"];
                    } else {
                        $this->session->set_flashdata('error', $this->lang->line('error_tamaño_imagen'));
                    }
                }

                $dimension = $resultadoFoto["upload_data"]["image_width"] . ' x ' . $resultadoFoto["upload_data"]["image_height"];
                $foto = $pathImagen."/".$foto;
                $data_foto_promocion = array(

                    'id_promocion' => $id_promocion,
                    'foto' => $foto,
                    'tipo' => $tipo,
                    'plataforma' => $plataforma,
                    'peso' => $resultadoFoto["upload_data"]["file_size"],
                    'dimension' => $dimension,
                    'fecha_registro' => date("Y-m-d H:i:s"),
                    'estado' => $estado
                );

                if (isset($data_foto_promocion)) {
                    try {
                        if($estado == "1") {
                            actualizar_foto($id_promocion, $tipo,$plataforma, 1);
                        }
                        //$this->actualizar_foto($id_promocion,$tipo);
                        $this->Foto_promocion_model->insert($data_foto_promocion);
                        $this->alert($this->lang->line('mensaje_foto_agregado'), site_url($this->config->item('path_backend') . '/Promocion/editar/' . $id_promocion . '/F'));
                    } catch (Exception $exception) {
                        $this->alert($this->lang->line('error_foto_agregar'));
                    }
                }

            }


            $this->arrayVista['tituloPagina'] = "Agregar Foto";
            $this->arrayVista['promocion'] = $id_promocion;
            $this->arrayVista['vista'] = 'backend/foto_promocion/agregar_view';
            $this->cargarVistaBackend();


        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function editar($id_promocion, $id_foto)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {
                $foto = $this->input->post("foto");
                $tipo = $this->input->post("tipo");
                $plataforma = $this->input->post("plataforma");
                $estado = $this->input->post("estado");
                $foto_antigua = $this->input->post("foto_antigua");

                $pathImagen = "upload/promocion/" . date("Y-m-d");

                if (!is_dir($pathImagen))
                    mkdir($pathImagen, 0775, true);
                if ($_FILES["foto"]["size"] > 0) {
                    $nombreArchivoImagen = "foto";
                    $formato = substr(strrchr($_FILES["foto"]["name"],'.'),1);
                    $nombre_imagen = pathinfo($_FILES['foto']['name'], PATHINFO_FILENAME);
                    $nombre_imagen =  $nombre_imagen."_".$tipo."_".$plataforma.".".$formato;
                    $resultadoFoto = $this->subirArchivo($nombreArchivoImagen, $pathImagen,$nombre_imagen, 'jpeg|jpg|png', 2024);
                    

                    $foto = "";

                    if (array_key_exists("upload_data", $resultadoFoto)) {
                        if (isset($resultadoFoto["upload_data"]["file_name"]))

                            $foto = $resultadoFoto["upload_data"]["orig_name"];
                    } else {
                        $this->session->set_flashdata('error', $this->lang->line('error_tamaño_imagen'));
                    }


                    $dimension = $resultadoFoto["upload_data"]["image_width"] . ' x ' . $resultadoFoto["upload_data"]["image_height"];

                    $foto = $pathImagen."/".$foto;

                    $data_foto_promocion = array(
                        'foto' => $foto,
                        'tipo' => $tipo,
                        'plataforma' => $plataforma,
                        'peso' => $resultadoFoto["upload_data"]["file_size"],
                        'dimension' => $dimension,
                        'fecha_modificacion' => date("Y-m-d H:i:s"),
                        'estado' => $estado
                    );
                    
                } else 
                {
                    $foto = $foto_antigua;
                    $data_foto_promocion = array(
                        
                        'foto' => $foto,
                        'tipo' => $tipo,
                        'plataforma' => $plataforma,
                        'fecha_modificacion' => date("Y-m-d H:i:s"),
                        'estado' => $estado
                    );
                }

                if($estado == "1") {
                    //$this->actualizar_foto($id_promocion, $tipo);
                  //  $filas_afectadas =
                        actualizar_foto($id_promocion, $tipo,$plataforma, 1);
                }

                if ($this->Foto_promocion_model->update($id_foto, $data_foto_promocion)) {


                    $this->alert($this->lang->line('mensaje_foto_editado'), site_url($this->config->item('path_backend') . '/Promocion/editar/' . $id_promocion . '/F'));

                }
            }

            $foto = array(
                "select" => "*",
                "where" => "id = '" . $id_foto . "'"
            );


            $foto = $this->Foto_promocion_model->get_search_row($foto);
            $this->arrayVista['tituloPagina'] = "Editar Foto";
            $this->arrayVista['promocion'] = $id_promocion;
            $this->arrayVista['fecha'] = $fecha_foto;
            $this->arrayVista['foto'] = $foto;
            $this->arrayVista['vista'] = 'backend/foto_promocion/editar_view';
            $this->cargarVistaBackend();

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function eliminar(){
        if ($this->session->userdata('logged_in') === TRUE) {

            if ($this->input->post()) {
                $id_foto = $this->input->post("id_foto");

                $estado = ESTADO_ELIMINADO;

                $data = array(
                    'fecha_modificacion' => date("Y-m-d H:i:s"),
                    'estado' => $estado
                );

                if (  $this->Foto_promocion_model->update($id_foto, $data)){
                    $result = array("cod"=>"1",
                        "mensaje"=>$this->lang->line('mensaje_foto_eliminado')
                    );
                }else{
                    $result = array("cod"=>"0",
                        "mensaje"=>$this->lang->line('error_foto_eliminar')
                    );
                }
            }
        }else{
            $result = array("cod"=>"0",
                "mensaje"=>$this->lang->line('error_foto_eliminar')
            );
        }
        echo json_encode($result);
    }


    public function export()
    {
        if ($this->session->userdata('logged_in') === TRUE) {

            $id_promocion = $this->input->get('id_promocion');
            $tipo = $this->input->get('tipo');
            $plataforma = $this->input->get('plataforma');
            $estado = $this->input->get('estado');


            $where = "";


            if ($tipo != "") {

                if ($where != "")
                    $where .= " AND tbl_foto_promocion.tipo = $tipo";
                else
                    $where .= "tbl_foto_promocion.tipo = $tipo";
            }

            if ($plataforma != "") {

                if ($where != "")
                    $where .= " AND tbl_foto_promocion.plataforma = $plataforma";
                else
                    $where .= "tbl_foto_promocion.plataforma = $plataforma";
            }




            if ($estado != "") {

                if ($where != "")
                    $where .= " AND tbl_foto_promocion.estado = $estado";
                else
                    $where .= "tbl_foto_promocion.estado = $estado";
            }else{
                if ($where != "")
                    $where .= " AND tbl_foto_promocion.estado != '".ESTADO_ELIMINADO."'";
                else
                    $where .= "tbl_foto_promocion.estado != '".ESTADO_ELIMINADO."'";
            }


            $join = array(
                'tbl_promocion as P, P.id = tbl_foto_promocion.id_promocion'
            );
            if($where==""){
                $params = array(
                    "select" => "P.nombre,tbl_foto_promocion.foto,tbl_foto_promocion.tipo,tbl_foto_promocion.plataforma,
                    tbl_foto_promocion.peso,tbl_foto_promocion.dimension,tbl_foto_promocion.fecha_registro,
                    tbl_foto_promocion.estado",
                    "join" => $join ,
                    "where" => "tbl_foto_promocion.id_promocion = '" . $id_promocion . "'",
                    "order"=> "tbl_foto_promocion.fecha_registro desc"
                );
            }
            else{
                $params =    array(
                    "select" => "P.nombre,tbl_foto_promocion.foto,tbl_foto_promocion.tipo,tbl_foto_promocion.plataforma,
                    tbl_foto_promocion.peso,tbl_foto_promocion.dimension,tbl_foto_promocion.fecha_registro,
                    tbl_foto_promocion.estado",
                    "join" => $join ,
                    "where" => "tbl_foto_promocion.id_promocion = '" . $id_promocion . "' AND ".$where,
                    "order"=> "tbl_foto_promocion.fecha_registro desc"

                );
            }

            $foto= $this->Foto_promocion_model->search($params);

          
            //retrive contries table data
            $data = array();
            foreach ($foto as $foto) {



                if ($foto->estado== 1){
                    $estado = "Activo";
                }else{
                    $estado = "Inactivo";
                }

                if ($foto->tipo== 1){
                    $tipo = "Principal";
                }else{
                    $tipo = "Detalle";
                }

                if ($foto->plataforma == 1){
                    $plataforma = "Web";
                }else{
                    $plataforma = "Móvil";
                }

                //$fecha = $this->fechaImagen($foto->foto);
                array_push($data, 
                    array(
                        utf8_decode("Promoción") => isset($foto->nombre)?$foto->nombre:"-",
                        "Foto" => isset($foto->foto)?base_url().substr($foto->foto,2):"-",
                        "Tipo" => isset($tipo)?$tipo:"-",
                        "Tipo" => isset($tipo)?$tipo:"-",
                        "Plataforma" => isset($plataforma)?utf8_decode($plataforma):"-",
                        utf8_decode("Dimensión") => isset($foto->dimension)?$foto->dimension:"-",
                        "Fecha de Registro" => isset($foto->fecha_registro)?date("d/m/Y h:i:s a", strtotime($foto->fecha_registro)):"-",
                        "Estado" => isset($estado)?$estado:"-",
                ));
            }
            //Fill data
            function filterData(&$str)
            {
                $str = preg_replace("/\t/", "\\t", $str);
                $str = preg_replace("/\r?\n/", "\\n", $str);
                if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
            }             
                
            $fileName = "Foto promocion-". date('YmdHis') . ".xls";
                
            header("Content-Disposition: attachment; filename=\"$fileName\"");
            header("Content-Type: application/vnd.ms-excel, charset=utf-8");
                
            $flag = false;

            foreach($data as $row) {
                // print_r( array_values($row))."<br>";
                if(!$flag) {
                    
                    echo implode("\t", array_keys($row)) . "\n";
                    $flag = true;
                }
                    
                array_walk($row, 'filterData');
                echo implode("\t", array_values($row)) . "\n";
            }
                
            exit;
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }


    public function actualizar_foto($id_promocion, $tipo)
    {

        $parametro_foto = array(
            "estado" => "0"
        );

        $where = "tbl_foto_promocion.id_promocion = $id_promocion
        and tbl_foto_promocion.tipo = $tipo";

        $filas_afectadas = $this->Foto_promocion_model->update_where($where, $parametro_foto);


        return $filas_afectadas;
    }
}
