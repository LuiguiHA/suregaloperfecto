<?php
/**
 * Created by PhpStorm.
 * User: Mediabyte
 * Date: 10/05/2016
 * Time: 03:17 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Contacto extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model("Contacto_model");
        $this->lang->load('backend_error_lang', 'spanish');
        $this->lang->load('backend_mensajes_lang', 'spanish');
        $this->load->helper('url');


    }

    public function listar($start = 0)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            $id_empresa = $this->input->post('id_empresa');
            $params = array(
                "select" => "*",
                "where" => "id_empresa = '" . $id_empresa . "'",
                "order" => "fecha_registro desc"
            );
            $total_contacto = $this->Contacto_model->total_records($params);

            $contacto = $this->Contacto_model->search_data($params, $start, $this->elementoPorPagina);
            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Contacto'), $total_contacto);
            $this->arrayVista['tituloPagina'] = "Lista de Contacto";
            $this->arrayVista['empresa'] = $id_empresa;
            $this->arrayVista['contacto'] = $contacto;

            $this->cargarVistaListaAjax('backend/contacto/listar_view');

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function buscar($start = 0)
    {
        if ($this->session->userdata('logged_in') === TRUE) {
            $id_empresa = $this->input->post('id_empresa');
            $nombre = $this->input->post('nombre');
            $apellido = $this->input->post('apellido');
            $dni = $this->input->post('dni');
            $estado = $this->input->post('estado');
            $where = "";


            if ($nombre != "") {
                $this->arrayVista['nombre'] = $nombre;
                if ($where != "")
                    $where .= " AND nombre  LIKE '%" . $nombre . "%'";

                else
                    $where .= "nombre LIKE '%" . $nombre . "%'";
            }

            if ($apellido != "") {
                $this->arrayVista['apellido'] = $apellido;
                if ($where != "")
                    $where .= " AND apellido  LIKE '%" . $apellido . "%'";

                else
                    $where .= "apellido LIKE '%" . $apellido . "%'";
            }

            if ($dni != "") {
                $this->arrayVista['dni'] = $dni;
                if ($where != "")
                    $where .= " AND dni  LIKE '%" . $dni . "%'";

                else
                    $where .= "dni LIKE '%" . $dni . "%'";
            }


            if ($estado != "") {
                $this->arrayVista['estado'] = $estado;
                if ($where != "")
                    $where .= " AND estado = $estado";
                else
                    $where .= "estado = $estado";
            }

            if ($where == "") {
                $params = array(
                    "select" => "*",
                    "where" => "id_empresa = '" . $id_empresa . "'",
                    "order" => "fecha_registro desc"
                );
            } else {
                $params = array(
                    "select" => "*",
                    "where" => "id_empresa = '" . $id_empresa . "' AND " . $where,
                    "order" => "fecha_registro desc"

                );
            }

            $total_contacto = $this->Contacto_model->total_records($params);

            $contacto = $this->Contacto_model->search_data($params, $start, $this->elementoPorPagina);
            $this->arrayVista['paginador'] = $this->obtenerPaginadoListado(site_url($this->config->item('path_backend') . '/Contacto/buscar'), $total_contacto);
            $this->arrayVista['tituloPagina'] = "Lista de Información Adicional";
            $this->arrayVista['empresa'] = $id_empresa;
            $this->arrayVista['contacto'] = $contacto;

            $this->cargarVistaListaAjax('backend/contacto/listar_view');

        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function agregar($id_empresa)
    {

        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->post()) {


                $dni = $this->input->post("dni");
                $nombre = $this->input->post("nombre");
                $apellido = $this->input->post("apellido");
                $sexo = $this->input->post("sexo");
                $fecha_nacimiento = $this->input->post("fecha_nacimiento");
                $email = $this->input->post("email");
                $roles = $this->input->post("roles");
                $promociones = $this->input->post("promociones");
                $canje = $this->input->post("canje");
                $estado = $this->input->post("estado");


                for ($i = 0; $i < sizeof($roles); $i++) {
                    if ($roles[$i] == 'api-dashboard') {
                        $rol1 = array("api-dashboard" => "si");
                    }
                }

                for ($i = 0; $i < sizeof($roles); $i++) {
                    if ($roles[$i] == 'clientes') {
                        $rol2 = array("clientes" => "si");
                    }
                }

                for ($i = 0; $i < sizeof($roles); $i++) {
                    if ($roles[$i] == 'reportes') {
                        $rol3 = array("reportes" => "si");
                    }
                }

                for ($i = 0; $i < sizeof($roles); $i++) {
                    if ($roles[$i] == 'consultas') {
                        if ($promociones == 'on') {
                            $rol4 = array('consultas' => array("promociones"));
                        }
                        if ($canje == 'on') {
                            $rol4 = array('consultas' => array("canje"));
                        }

                        if ($canje == 'on' && $promociones == 'on') {
                            $rol4 = array('consultas' => array("promociones,canje"));
                        }
                    }
                }


                $roles = array($rol1, $rol2, $rol3, $rol4);
                $roles = array_values(array_diff($roles, array('')));

                $tipos = json_encode($roles);

                $data_contacto = array(
                    'id_empresa' => $id_empresa,
                    'dni' => $dni,
                    'nombre' => $nombre,
                    'apellido' => $apellido,
                    'sexo' => $sexo,
                    'perfil' => $tipos,
                    'fecha_nacimiento' => $fecha_nacimiento,
                    'correo' => $email,
                    'fecha_registro' => date("Y-m-d H:i:s"),
                    'estado' => $estado
                );

                if (isset($data_contacto)) {
                    try {
                        $this->Contacto_model->insert($data_contacto);
                        $this->alert($this->lang->line('mensaje_contacto_agregado'), site_url($this->config->item('path_backend') . '/Empresa/editar/' . $id_empresa . '/C'));
                    } catch (Exception $exception) {
                        $this->alert($this->lang->line('error_contacto_agregar'));
                    }
                }

            }

            $arrayMigaPan = array(
                array("nombre" => "Empresa", "url" => site_url($this->config->item('path_backend') . '/Empresa')),
                array("nombre" => "Contacto", "url" => site_url($this->config->item('path_backend') . '/Contacto/agregar')),
                array("nombre" => "Agregar", 'active' => true));

            $this->arrayVista['arrayMigaPan'] = $arrayMigaPan;

            $this->arrayVista['empresa'] = $id_empresa;
            $this->arrayVista['tituloPagina'] = "Agregar Contacto";
            $this->arrayVista['vista'] = 'backend/contacto/agregar_view';
            $this->cargarVistaBackend();


        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function editar($id_empresa, $id_contacto)
    {
        if ($this->session->userdata('logged_in') === TRUE) {

            if ($this->input->post()) {
                $dni = $this->input->post("dni");
                $nombre = $this->input->post("nombre");
                $apellido = $this->input->post("apellido");
                $sexo = $this->input->post("sexo");
                $contraseña = $this->input->post("contraseña");
                $roles = $this->input->post("roles");
                $promociones = $this->input->post("promociones");
                $canje = $this->input->post("canje");
                $fecha_nacimiento = $this->input->post("fecha_nacimiento");
                $email = $this->input->post("email");
                $estado = $this->input->post("estado");

                for ($i = 0; $i < sizeof($roles); $i++) {
                    if ($roles[$i] == 'api-dashboard') {
                        $rol1 = array("api-dashboard" => "si");
                    }
                }

                for ($i = 0; $i < sizeof($roles); $i++) {
                    if ($roles[$i] == 'clientes') {
                        $rol2 = array("clientes" => "si");
                    }
                }

                for ($i = 0; $i < sizeof($roles); $i++) {
                    if ($roles[$i] == 'reportes') {
                        $rol3 = array("reportes" => "si");
                    }
                }

                for ($i = 0; $i < sizeof($roles); $i++) {
                    if ($roles[$i] == 'consultas') {
                        if ($promociones == 'on') {
                            $rol4 = array('consultas' => array("promociones"));
                        }
                        if ($canje == 'on') {
                            $rol4 = array('consultas' => array("canje"));
                        }

                        if ($canje == 'on' && $promociones == 'on') {
                            $rol4 = array('consultas' => array("promociones,canje"));
                        }
                    }
                }


                $roles = array($rol1, $rol2, $rol3, $rol4);
                $roles = array_values(array_diff($roles, array('')));

                $tipos = json_encode($roles);


                if ($contraseña == '1') {

                    $caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
                    $numerodeletras = 10;
                    $password = "";
                    for ($i = 0; $i < $numerodeletras; $i++) {
                        $password .= substr($caracteres, rand(0, strlen($caracteres)), 1);
                    }
                    $data_contacto = array(

                        'dni' => $dni,
                        'nombre' => $nombre,
                        'apellido' => $apellido,
                        'sexo' => $sexo,
                        'contrasenia' => $password,
                        'perfil'=>$tipos,
                        'fecha_nacimiento' => $fecha_nacimiento,
                        'correo' => $email,
                        'fecha_modificacion' => date("Y-m-d H:i:s"),
                        'estado' => $estado
                    );

                } else {
                    $data_contacto = array(

                        'dni' => $dni,
                        'nombre' => $nombre,
                        'apellido' => $apellido,
                        'sexo' => $sexo,
                        'perfil'=> $tipos,
                        'fecha_nacimiento' => $fecha_nacimiento,
                        'correo' => $email,
                        'fecha_modificacion' => date("Y-m-d H:i:s"),
                        'estado' => $estado
                    );
                }


                if ($this->Contacto_model->update($id_contacto, $data_contacto)) {


                    $this->alert($this->lang->line('mensaje_contacto_editado'), site_url($this->config->item('path_backend') . '/Empresa/editar/' . $id_empresa . '/C'));

                }
            }

            $contacto = array(
                "select" => "*",
                "where" => "id = '" . $id_contacto . "'"
            );


            $contacto = $this->Contacto_model->get_search_row($contacto);
            $this->arrayVista['empresa'] = $id_empresa;
            $this->arrayVista['contacto'] = $contacto;
            $this->arrayVista['tituloPagina'] = "Editar Contacto";
            $this->arrayVista['vista'] = 'backend/contacto/editar_view';
            $this->cargarVistaBackend();


        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }

    public function export()
    {
        if ($this->session->userdata('logged_in') === TRUE) {

            $id_empresa = $this->input->get('id_empresa');
            $nombre = $this->input->get('nombre');
            $apellido = $this->input->get('apellido');
            $dni = $this->input->get('dni');
            $estado = $this->input->get('estado');


            $where = "";

            if ($nombre != "") {

                if ($where != "")
                    $where .= " AND tbl_contacto.nombre  LIKE '%" . $nombre . "%'";

                else
                    $where .= "tbl_contacto.nombre LIKE '%" . $nombre . "%'";
            }

            if ($apellido != "") {

                if ($where != "")
                    $where .= " AND tbl_contacto.apellido  LIKE '%" . $apellido . "%'";

                else
                    $where .= "tbl_contacto.apellido LIKE '%" . $apellido . "%'";
            }

            if ($dni != "") {

                if ($where != "")
                    $where .= " AND tbl_contacto.dni  LIKE '%" . $dni . "%'";

                else
                    $where .= "tbl_contacto.dni LIKE '%" . $dni . "%'";
            }


            if ($estado != "") {

                if ($where != "")
                    $where .= " AND tbl_contacto.estado = $estado";
                else
                    $where .= "tbl_contacto.estado = $estado";
            }

            $join = array(
                'tbl_empresa as E,E.id=tbl_contacto.id_empresa'
            );
            if ($where == "") {

                $params = array(
                    "select" => "E.nombre as empresa,tbl_contacto.dni,tbl_contacto.nombre,tbl_contacto.apellido,tbl_contacto.sexo,
                    tbl_contacto.fecha_nacimiento,tbl_contacto.correo,tbl_contacto.fecha_registro,tbl_contacto.estado",
                    "where" => "tbl_contacto.id_empresa = '" . $id_empresa . "'",
                    "join" => $join,
                    "order" => "tbl_contacto.fecha_registro desc"
                );
            } else {
                $params = array(
                    "select" => "E.nombre as empresa,tbl_contacto.dni,tbl_contacto.nombre,tbl_contacto.apellido,tbl_contacto.sexo,
                    tbl_contacto.fecha_nacimiento,tbl_contacto.correo,tbl_contacto.fecha_registro,tbl_contacto.estado",
                    "where" => "tbl_contacto.id_empresa = '" . $id_empresa . "' AND " . $where,
                    "join" => $join,
                    "order" => "tbl_contacto.fecha_registro desc"

                );
            }
            $contactos = $this->Contacto_model->search($params);

            //retrive contries table data
            $data = array();
            foreach ($contactos as $contacto) {


                if (strcasecmp($contacto->sexo, "M") == 0) {
                    $sexo = "Masculino";
                } else {
                    $sexo = "Femenino";
                }

                if ($contacto->estado == 1) {
                    $estado = "Activo";
                } else {
                    $estado = "Inactivo";
                }


                array_push(
                    $data, 
                    array(
                        utf8_decode("Empresa") => isset($contacto->empresa)?utf8_decode($contacto->empresa):"-",
                        utf8_decode("DNI") => isset($contacto->dni)?utf8_decode($contacto->dni):"-",
                        utf8_decode("Nombre") => isset($contacto->nombre)?utf8_decode($contacto->nombre):"-",
                        utf8_decode("Apellido") => isset($contacto->apellido)?utf8_decode($contacto->apellido):"-",
                        utf8_decode("Sexo") => isset($sexo)?utf8_decode($sexo):"-",
                        utf8_decode("Fecha de Nacimiento") => date("d/m/Y", strtotime($contacto->fecha_nacimiento)),
                        utf8_decode("Correo") => isset($contacto->correo)?utf8_decode($contacto->correo):"-",
                        utf8_decode("Fecha de Registro") => date("d/m/Y h:i:s a", strtotime($contacto->fecha_registro)),
                        utf8_decode("Estado") => isset($estado)?utf8_decode($estado):"-",
                ));
            }
            //Fill data
            function filterData(&$str)
            {
                $str = preg_replace("/\t/", "\\t", $str);
                $str = preg_replace("/\r?\n/", "\\n", $str);
                if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
            }             
                
            $fileName = "Contacto-". date('YmdHis') . ".xls";
                
            header("Content-Disposition: attachment; filename=\"$fileName\"");
            header("Content-Type: application/vnd.ms-excel, charset=utf-8");
                
            $flag = false;

            foreach($data as $row) {
                // print_r( array_values($row))."<br>";
                if(!$flag) {
                    
                    echo implode("\t", array_keys($row)) . "\n";
                    $flag = true;
                }
                    
                array_walk($row, 'filterData');
                echo implode("\t", array_values($row)) . "\n";
            }
                
            exit;
        } else {
            redirect(site_url($this->config->item('path_backend')));
        }
    }
}
