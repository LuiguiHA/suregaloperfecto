<?php
/**
 * Created by PhpStorm.
 * User: Luis Alberto Rosas Arce
 * Date: 04/05/2016
 * Time: 09:49 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
require APPPATH."/libraries/Uuid.php";

class Catalogo extends REST_Controller{
  public  $contacto= "";
  public $empresa;
  public function __construct(){
      parent::__construct();
      $this->load->helper('utils_helper');
      $this->load->model("Producto_model","producto_model");
      //$this->load->model("token_model","token_model");
      $this->lang->load("api_mensajes_lang", "spanish");
      $this->lang->load("api_error_lang", "spanish");
      //$this->empresa = $this->config->item('empresa');
  }

  public function obtener_categoria_get($categoria){

      $productos = $this->listar_productos($categoria);
      if(isset($productos)){
          if (sizeof($productos['data_productos'])>0 ){
              $resultado = array(
                  "resultado"=>ESTADO_RESPUESTA_OK,
                  "mensaje"=>$this->lang->line('productos_listar'),
                  "productos" => $productos['data_productos'],
                  "cantidad" => $productos['cantidad']
              );
          }else{
              $resultado = array(
                  "resultado"=>ESTADO_RESPUESTA_ERROR,
                  "mensaje"=>'no se encontraron productos',
                  "productos" => $productos['data_productos'],
                  "cantidad" => $productos['cantidad']
              );
          }
      }else{
          $resultado = array(
              "resultado"=>ESTADO_RESPUESTA_ERROR,
              "mensaje"=>'no se encontraron productos 2'
          );
      }

      $this->response($resultado);
  }


  public function listar_productos($categoria){

    if($categoria != 0){
      $params = array(
          "select" => "*",
          "where" => "activo = ".ESTADO_ACTIVO." AND categoria_id = ".$categoria,
          "order"=> "id"
      );

    }else{
      $params = array(
          "select" => "*",
          "where" => "activo = ".ESTADO_ACTIVO,
          "order"=> "id"
      );
    }


    $data_productos = $this->producto_model->search_array($params);


    $cantidad = $this->producto_model->total_records($params);

    $productos =array(
        "data_productos" => $data_productos,
        "paginacion_siguiente" => $paginacion_siguiente,
        "cantidad" => $cantidad
    );

    return $productos;
  }

  public function detalle_producto_get($id){

      $producto = $this->detalle_producto($id);
      if(isset($producto)){

          $resultado = array(
              "resultado"=>ESTADO_RESPUESTA_OK,
              "mensaje"=>'producto',
              "producto" => $producto
          );


      }else{
          $resultado = array(
              "resultado"=>ESTADO_RESPUESTA_ERROR,
              "mensaje"=>'no se encontró el producto'
          );
      }

      $this->response($resultado);
  }

  public function detalle_producto($id){

    $params = array(
        "select" => "*",
        "where" => "activo = ".ESTADO_ACTIVO." AND id = ".$id
    );

    $data_producto = $this->producto_model->get_search_row($params);

    return $data_producto;
  }
  
  public function obtener_regalo_post(){
    
    $sexo = $this->post('sexo', TRUE);
    $edad = $this->post('edad', TRUE);
    $tipo = $this->post('tipo', TRUE);
    $personalidad = $this->post('personalidad', TRUE);
    $tono = $this->post('tono', TRUE);
    
    $producto = $this->obtener_regalo($sexo, $edad, $tipo, $personalidad, $tono);
    if(isset($producto)){

        $resultado = array(
            "resultado"=>ESTADO_RESPUESTA_OK,
            "mensaje"=>'producto',
            "producto" => $producto
        );


    }else{
        $resultado = array(
            "resultado"=>ESTADO_RESPUESTA_ERROR,
            "mensaje"=>'no se encontró el producto'
        );
    }

    $this->response($resultado);
  }
  
  public function obtener_regalo($sexo, $edad, $tipo, $personalidad, $tono){
    
    $params = array(
        "select"=>"producto.id, producto.categoria_id, producto.sku, producto.sap, producto.nombre, producto.descripcion, producto.url_saga",
        "join"=>array(
            "combinacion com, com.sap = producto.sap"
        ),
        "where" => "producto.activo = ".ESTADO_ACTIVO.
        " and com.sexo = '$sexo' 
        and com.edad_id = $edad 
        and com.tipo_id = $tipo 
        and com.personalidad_id = $personalidad 
        and com.tono_id = $tono" 
    );

    $data_producto = $this->producto_model->get_search_row($params);

    return $data_producto;
  }


}