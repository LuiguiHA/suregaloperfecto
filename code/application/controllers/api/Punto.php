<?php
/**
 * Created by PhpStorm.
 * User: Luis Alberto Rosas Arce
 * Date: 04/05/2016
 * Time: 09:49 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
require APPPATH."/libraries/Uuid.php";

class Punto extends REST_Controller{
  public  $contacto= "";
  public $empresa;
  public function __construct(){
      parent::__construct();
      $this->load->helper('utils_helper');
      $this->load->model("Lugar_model","lugar_model");
      $this->load->model("Punto_model","punto_model");
      $this->lang->load("api_mensajes_lang", "spanish");
      $this->lang->load("api_error_lang", "spanish");
      //$this->empresa = $this->config->item('empresa');
  }

  public function obtener_puntos_venta_get(){

      $puntos = $this->listar_lugares();
      if(isset($puntos)){
          if (sizeof($puntos['data_lugares'])>0 ){
              $resultado = array(
                  "resultado"=>ESTADO_RESPUESTA_OK,
                  "mensaje" => "lugares encontrados",
                  "lugares" => $puntos['data_lugares'],
                  "puntos" => $puntos['data_puntos']
              );
          }else{
              $resultado = array(
                  "resultado"=>ESTADO_RESPUESTA_ERROR,
                  "mensaje"=>'No se econtrarons puntos.'
              );
          }
      }else{
          $resultado = array(
              "resultado"=>ESTADO_RESPUESTA_ERROR,
              "mensaje"=>'Ocurrió un error.'
          );
      }

      $this->response($resultado);
  }


  public function listar_lugares(){

  
    $params = array(
        "select" => "*",
        "where" => "activo = ".ESTADO_ACTIVO,
        "order"=> "id"
    );

    $data_lugares = $this->lugar_model->search_array($params);
    $data_puntos = $this->punto_model->search_array($params);


    $puntos =array(
        "data_lugares" => $data_lugares,
        "data_puntos" => $data_puntos
    );

    return $puntos;
  }

  


}