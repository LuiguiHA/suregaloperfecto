<?php
/**
 * Created by PhpStorm.
 * User: Luis Alberto Rosas Arce
 * Date: 04/05/2016
 * Time: 09:49 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
require APPPATH."/libraries/Uuid.php";

class Contacto extends REST_Controller{

  public function __construct(){
      parent::__construct();
      
      $this->load->model("Contacto_model","contacto_model");
      $this->lang->load("api_mensajes_lang", "spanish");
      $this->lang->load("api_error_lang", "spanish");
      $this->load->library('email');
  }

  
  
  public function registro_contacto_post(){
    
    $names = $this->post('names', TRUE);
    $email = $this->post('email', TRUE);
    $message = $this->post('message', TRUE);
    $terminos = $this->post('terminos', TRUE);
    $info = $this->post('info', TRUE);
    $fecha = date('Y-m-d h:i:s');
    
    
    if(isset($names) && $names != ""){

        if(isset($email) && $email != ""){
          
          if(isset($message) && $message != ""){
            
            if(isset($terminos) && $terminos != ""){
              
              $data = array(
                'nombres' => $names,
                'email' => $email,
                'mensaje' => $message,
                'acepta_terminos' => $terminos,
                'acepta_info' => $info,
                'fecha_registro' => $fecha
              );
              
              $id_contacto = $this->contacto_model->insert($data);
              
              if($id_contacto){
              
                
                
              
                $configuracionGmail = array(
                    'protocol' => CORREO_PROTOCOL,
                    'smtp_host' => CORREO_HOST,
                    'smtp_port' => CORREO_PUERTO,
                    'smtp_user' => CORREO_NOMBRE,
                    'smtp_pass' => CORREO_CONTRASENIA,
                    'mailtype' => 'html',
                    'charset' => 'utf-8',
                    'newline' => "\r\n"
                );

                $CI = &get_instance();
                $this->load->helper('utils_helper');
                
                $data['nombre'] = $names;
                $data['correo'] = $email;
                $data['mensaje'] = $message;
                
                $mensaje = $CI->load->view("mailing/message_notification",$data,TRUE);

                $CI->email->initialize($configuracionGmail);
                $CI->email->from(CORREO_NOMBRE, REMITENTE_NOMBRE);
                $CI->email->to(CORREO_DESTINO);
                $CI->email->subject(CORREO_SUBJECT);
                $CI->email->message($mensaje);
                $envio = $CI->email->send();
                
                $resultado = array(
                    "resultado"=>ESTADO_RESPUESTA_OK,
                    "mensaje"=>'La información se almacenó correctamente.'
                );
                
                
              }else{
                $resultado = array(
                    "resultado"=>ESTADO_RESPUESTA_ERROR,
                    "mensaje"=>'No fue posible almacenar la información.'
                );
              }
              
            }else{
              $resultado = array(
                  "resultado"=>ESTADO_RESPUESTA_ERROR,
                  "mensaje"=>'No fue posible almacenar la información. Los datos no están completos.'
              );
            }
            
          }else{
            $resultado = array(
                "resultado"=>ESTADO_RESPUESTA_ERROR,
                "mensaje"=>'No fue posible almacenar la información. Los datos no están completos.'
            );
          }
          
        }else{
          $resultado = array(
              "resultado"=>ESTADO_RESPUESTA_ERROR,
              "mensaje"=>'No fue posible almacenar la información. Los datos no están completos.'
          );
        }


    }else{
        $resultado = array(
            "resultado"=>ESTADO_RESPUESTA_ERROR,
            "mensaje"=>'No fue posible almacenar la información. Los datos no están completos.'
        );
    }

    $this->response($resultado);
  }
  



}