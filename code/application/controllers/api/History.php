<?php
/**
 * Created by PhpStorm.
 * User: Luis Alberto Rosas Arce
 * Date: 04/05/2016
 * Time: 09:49 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
require APPPATH."/libraries/Uuid.php";

class History extends REST_Controller{

  public function __construct(){
      parent::__construct();
      
      $this->load->model("History_model","history_model");
      $this->lang->load("api_mensajes_lang", "spanish");
      $this->lang->load("api_error_lang", "spanish");
      $this->load->library('email');
  }

  
  
  public function registro_historia_post(){
    $name = $this->post('name', TRUE);
    $last_name = $this->post('last_name', TRUE);
    $email = $this->post('email', TRUE);
    $history = $this->post('history', TRUE);
    $dni = $this->post('dni', TRUE);
    $phone = $this->post('phone', TRUE);
    $info = $this->post('info', TRUE);
    $fecha = date('Y-m-d h:i:s');
    
    $data = array(
        'nombres' => $name,
        'apellidos' => $last_name,
        'email' => $email,
        'historia' => $history,
        'dni' => $dni,
        'phone' => $phone,
        'info' => $info,
        'fecha_registro' => $fecha
    );
    $id_history = $this->history_model->insert($data);

    if($id_history){
        $resultado = array(
            "resultado"=>ESTADO_RESPUESTA_OK,
            "mensaje"=>'La información se almacenó correctamente.'
        );
    } else{
        $resultado = array(
            "resultado"=>ESTADO_RESPUESTA_ERROR,
            "mensaje"=>'No fue posible almacenar la información.'
        );
    }

    $this->response($resultado);
  }
  



}