<?php
/**
 * Created by PhpStorm.
 * User: Luis Alberto Rosas Arce
 * Date: 04/05/2016
 * Time: 09:49 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
require APPPATH."/libraries/Uuid.php";

class Admin extends REST_Controller{

  public function __construct(){
      parent::__construct();
      
      $this->load->model("Admin_model","admin_model");
      $this->lang->load("api_mensajes_lang", "spanish");
      $this->lang->load("api_error_lang", "spanish");
      $this->load->library('email');
  }

  
  
  public function login_post(){
    $email = $this->post('email', TRUE);
    $password = $this->post('password', TRUE);
    $fecha = date('Y-m-d h:i:s');
    
    $data = array(
        'email' => $email,
        'password' => md5($password),
    );
    $id_history = $this->admin_model->insert($data);
    $login_id = $this->admin_model->login_valid($email, $password);
    if ($login_id) {
        $this->session->set_userdata('user_id', $login_id);
        $resultado = array(
            "resultado"=>TRUE,
        );
    } else {

        $resultado = array(
            "resultado"=>FALSE,
        );
    }
    $this->response($resultado);
  }

  public function logout_post(){
    $this->session->unset_userdata('user_id');
    $resultado = array(
        "resultado"=>TRUE,
    );
    $this->response($resultado);
  }
  



}