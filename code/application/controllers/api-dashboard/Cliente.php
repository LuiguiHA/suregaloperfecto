<?php
/*
 * Created by Luis Alberto Rosas Arce
 */
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
require APPPATH . "/libraries/Uuid.php";

class Cliente extends REST_Controller
{
    public $id_empresa;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('utils_helper');
        $this->load->model("token_dashboard_model", "token_dashboard_model");
        $this->load->model("usuario_model", "usuario_model");
        $this->load->model("persona_model", "persona_model");
        $this->load->model("tipo_pago_empresa_model", "tipo_pago_empresa_model");
        $this->load->model("puntos_model", "puntos_model");
        $this->load->model("canje_model", "canje_model");
        $this->load->model("beneficio_model", "beneficio_model");
        $this->load->model("local_model", "local_model");
        $this->load->model("promocion_model", "promocion_model");
        $this->load->model("promocion_canje_model", "promocion_canje_model");
        $this->load->model("empresa_model", "empresa_model");
        $this->lang->load('dashboard_error_lang', 'spanish');
        $this->lang->load('dashboard_mensajes_lang', 'spanish');
        $this->id_empresa = $this->config->item('empresa');
        $this->load->library('form_validation');
        $this->load->model('contrasenia_model', 'contrasenia_model');

    }

    public function obtener_clientes_get($paginacion = "")
    {
        $token_recibido = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_OBTENER_CLIENTES;
        $nombre_metodo = NOMBRE_METODO_GET;
        $operacion ="obtener clientes";
        $parametros = array(
            "token" => $token_recibido
        );

        $id_empresa = $this->id_empresa;
        $usuario = "";

        if (isset($token_recibido) && $token_recibido != "") {
            $objeto_token = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);
            if ($objeto_token != "" && isset($objeto_token)) {
                $datos_usuarios = $this->listar_usuarios($id_empresa, $paginacion);
                $usuario = $objeto_token->usuario;

                if (isset($datos_usuarios)) {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_OK,
                        "mensaje" => $this->lang->line('cliente_obtener_cliente'),
                        "total_usuarios" => $datos_usuarios["total_usuarios"],
                        "usuarios" => $datos_usuarios["usuarios"]
                    );

                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_obtener_usuarios')
                    );
                }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }


        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo,$token_recibido, $usuario, $operacion);
        $this->response($resultado);
    }

    public function buscar_cliente_post($paginacion = "")
    {
        $token_recibido = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_BUSCAR_CLIENTE;
        $nombre_metodo = NOMBRE_METODO_POST;
        $operacion = "obtener clientes";

        $nombre = $this->post("nombre", TRUE);
        $apellido = $this->post("apellido", TRUE);
        $dni = $this->post("dni", TRUE);
        $correo = $this->post("correo", TRUE);


        $parametros = array(
            "token" => $token_recibido,
            "nombre" => $nombre,
            "apellido" => $apellido,
            "dni" => $dni,
            "correo" => $correo
        );

        
        if (isset($token_recibido) && $token_recibido != "") {
            $objeto_token = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);
            if ($objeto_token != "" && isset($objeto_token)) {

                $datos_usuario = $this->buscar_usuarios($this->id_empresa, $paginacion);
                $usuario = $objeto_token->usuario;

                if (isset($datos_usuario)) {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_OK,
                        "mensaje" => $this->lang->line('cliente_busqueda_usuario'),
                        "total_usuarios" => $datos_usuario["total_usuarios"],
                        "usuarios" => $datos_usuario["usuarios"]
                    );

                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_obtener_usuarios')
                    );
                }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo,$token_recibido,$usuario, $operacion);
        $this->response($resultado);
    }

    public function detalle_agregar_puntos_get($id_usuario, $paginacion = "")
    {
        $token_recibido = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_DETALLE_AGREGAR_PUNTOS;
        $nombre_metodo = NOMBRE_METODO_GET;

        $operacion = "detalle para agregar puntos";

        $parametros = array(
            "token" => $token_recibido,
            "id_usuario" => $id_usuario
        );

        if (isset($token_recibido) && $token_recibido != "") {
            $objeto_token = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);
            $usuario = $objeto_token->usuario;
            if ($objeto_token != "" && isset($objeto_token)) {
                if (isset($id_usuario) && $id_usuario != "") {

                    $tipos_pago = $this->buscar_tipos_pago($id_empresa = $this->id_empresa);

                    if (isset($tipos_pago)) {
                        $detalle_cliente = $this->buscar_detalle_cliente($id_usuario);

                        if (isset($detalle_cliente)) {
                            $datos_usuario = $this->buscar_puntos_usuario($id_usuario, $paginacion);

                            if (isset($datos_usuario)) {
                                $resultado = array(
                                    "resultado" => ESTADO_RESPUESTA_OK, "mensaje" => $this->lang->line('cliente_detalle_agregar_puntos'), "tipo_pago" => $tipos_pago, "detalle_cliente" => $detalle_cliente, "total_puntos" => $datos_usuario["total_puntos"], "puntos" => $datos_usuario["puntos"]
                                );

                            } else {
                                $resultado = array(
                                    "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_obtener_detalle_puntps')
                                );
                            }

                        } else {
                            $resultado = array(
                                "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_obtener_detalle_cliente')
                            );
                        }
                    } else {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_obtener_tipo_pago')
                        );
                    }
                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_parametro_url')
                    );
                }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo,$token_recibido, $usuario, $operacion);
        $this->response($resultado);
    }

    public function agregar_puntos_post()
    {
        $token_recibido = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_AGREGAR_PUNTOS;
        $nombre_metodo = NOMBRE_METODO_POST;
        $operacion = "agregar puntos";

        $id_usuario = $this->post("id_usuario", TRUE);
        $id_local = $this->post("id_local", TRUE);
        $tipo_pago = $this->post("tipo_pago", TRUE);
        $monto = $this->post("monto", TRUE);
        $mozo = $this->post("mozo", TRUE);

        $parametros = array(
            "token" => $token_recibido,
            "id_usuario" => $id_usuario,
            "id_local" => $id_local,
            "tipo_pago" => $tipo_pago,
            "monto" => $monto,
            "mozo" => $mozo
        );

        if (isset($token_recibido) && $token_recibido != "") {
            $objeto_token = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);
            if ($objeto_token != "" && isset($objeto_token)) {
                $usuario = $objeto_token->usuario;

                if (isset($id_local) && $id_local != "" && isset($tipo_pago) && $tipo_pago != "" && isset($monto) && $monto != "" && isset($id_usuario) && $id_usuario != "") {
                    $id_empresa = $this->id_empresa;
                    $data_usuario = $this->buscar_usuario($id_usuario);
                    $data_puntos = $this->guardar_puntos($data_usuario, $id_empresa);

                    $id_puntos = $data_puntos['id_puntos'];
                    $puntos_ganados = $data_puntos['puntos'];
                    $mensaje = $this->lang->line('cliente_agregar_puntos') . $puntos_ganados . " puntos.";

                    if (isset($id_puntos) && $id_puntos > 0) {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_OK,
							"puntos"=>$puntos_ganados,
                            "mensaje" => $mensaje
                        );
                    } else {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR,
                            "mensaje" => $this->lang->line('error_guardar_puntos')
                        );
                    }

                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_parametros')
                    );
                }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo,$token_recibido, $usuario, $operacion);
        $this->response($resultado);
    }

    public function detalle_canjear_puntos_get($id_usuario, $paginacion = "")
    {
        $token_recibido = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_DETALLE_CANJEAR_PUNTOS;
        $nombre_metodo = NOMBRE_METODO_GET;
        $operacion = "detalle para canjear puntos";

        $parametros = array(
            "token" => $token_recibido,
            "id_usuario" => $id_usuario
        );

        if (isset($token_recibido) && $token_recibido != "") {
            $objeto_token = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);
            if ($objeto_token != "" && isset($objeto_token)) {
                $usuario = $objeto_token->usuario;
                if (isset($id_usuario) && $id_usuario != "") {
                    $beneficios = $this->buscar_beneficios_empresa($this->id_empresa);
                    if (isset($beneficios)) {
                        $cliente = $this->buscar_detalle_cliente($id_usuario);
                        if (isset($cliente)) {
                            $datos_canje = $this->buscar_canje_usuario($id_usuario, $paginacion);

                            if (isset($datos_canje)) {
                                $resultado = array(
                                    "resultado" => ESTADO_RESPUESTA_OK,
                                    "mensaje" => $this->lang->line('cliente_detalle_canjear_puntos'),
                                    "beneficios" => $beneficios,
                                    "detalle_cliente" => $cliente,
                                    "total_canje" => $datos_canje["total_canje"],
                                    "canjes" => $datos_canje["canjes"]
                                );
                            } else {
                                $resultado = array(
                                    "resultado" => ESTADO_RESPUESTA_ERROR,
                                    "mensaje" => $this->lang->line('error_obtener_detalle_canjes')
                                );
                            }
                        } else {
                            $resultado = array(
                                "resultado" => ESTADO_RESPUESTA_ERROR,
                                "mensaje" => $this->lang->line('error_obtener_detalle_cliente')
                            );
                        }

                    } else {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR,
                            "mensaje" => $this->lang->line('error_obtener_beneficios')
                        );
                    }
                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_parametro_url')
                    );
                }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }


        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token_recibido, $usuario, $operacion);

        $this->response($resultado);
    }

    public function canjear_puntos_post()
    {
        $token_recibido = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_CANJEAR_PUNTOS;
        $nombre_metodo = NOMBRE_METODO_POST;
        $operacion = "canjear puntos";

        $id_usuario = $this->post("id_usuario", TRUE);
        $id_beneficio = $this->post("id_beneficio", TRUE);
        $id_local = $this->post("id_local", TRUE);
        $puntos = $this->post("puntos", TRUE);
        $monto_adicional = $this->post("monto_adicional");

        if (isset($monto_adicional) && $monto_adicional != "") {
        }
        {
            $monto_adicional = "0";
        }

        $parametros = array(
            "token" => $token_recibido,
            "id_usuario" => $id_usuario,
            "id_canje" => $id_beneficio,
            "id_local" => $id_local,
            "puntos" => $puntos,
            "monto_adicional" => $monto_adicional
        );


        if (isset($token_recibido) && $token_recibido != "") {
            $objeto_token = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);
            if ($objeto_token != "" && isset($objeto_token)) {
                $usuario = $objeto_token->usuario;
                if (isset($id_usuario) && $id_usuario != "" && isset($id_beneficio) && $id_beneficio != "" && isset($id_local) && $id_local != "" && isset($puntos) && $puntos != "") {
                    $data_usuario = $this->buscar_usuario($id_usuario);
                    if ($puntos <= $data_usuario->punto_acumulado) {
                        $id_canje = $this->guardar_canje($data_usuario);
                        if (isset($id_canje) && $id_canje != "") {
                            $resultado = array(
                                "resultado" => ESTADO_RESPUESTA_OK, "mensaje" => $this->lang->line('cliente_canjear_puntos') . " " . $puntos . " puntos por un"
                            );
                        } else {
                            $resultado = array(
                                "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_guardar_canje')
                            );
                        }
                    } else {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_canje_insuficiente')
                        );
                    }
                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_parametros')
                    );
                }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token_recibido, $usuario, $operacion);

        $this->response($resultado);
    }

    public function obtener_historial_get($id_usuario, $paginacion = "")
    {
        $token_recibido = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_OBTENER_HISTORIAL;
        $nombre_metodo = NOMBRE_METODO_GET;
        $operacion = "obtener historial";

        $parametros = array(
            "token" => $token_recibido,
            "id_usuario" => $id_usuario
        );

        if (isset($token_recibido) && $token_recibido != "") {
            $objeto_token = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);
            if ($objeto_token != "" && isset($objeto_token)) {
                $usuario = $objeto_token->usuario;
                if (isset($id_usuario) && $id_usuario != "") {

                    $id_empresa = $this->id_empresa;

                    $detalle_cliente = $this->buscar_detalle_cliente($id_usuario);

                    if (isset($detalle_cliente)) {
                        $locales = $this->obtener_locales_empresa($id_empresa);

                        if (isset($locales)) {
                            $datos_puntos = $this->buscar_puntos_usuario($id_usuario, $paginacion);

                            if (isset($datos_puntos)) {
                                $datos_canje = $this->buscar_canje_usuario($id_usuario, $paginacion);
                                if (isset($datos_canje)) {
                                    $resultado = array(
                                        "resultado" => ESTADO_RESPUESTA_OK,
                                        "mensaje" => $this->lang->line('cliente_obtener_historial'),
                                        "locales" => $locales,
                                        "detalle_cliente" => $detalle_cliente,
                                        "total_puntos" => $datos_puntos["total_puntos"],
                                        "puntos" => $datos_puntos["puntos"],
                                        "total_canje" => $datos_canje["total_canje"],
                                        "canjes" => $datos_canje["canjes"]
                                    );
                                } else {
                                    $resultado = array(
                                        "resultado" => ESTADO_RESPUESTA_ERROR,
                                        "mensaje" => $this->lang->line('error_obtener_detalle_canjes')
                                    );
                                }

                            } else {
                                $resultado = array(
                                    "resultado" => ESTADO_RESPUESTA_ERROR,
                                    "mensaje" => $this->lang->line('error_obtener_detalle_puntps')
                                );
                            }

                        } else {
                            $resultado = array(
                                "resultado" => ESTADO_RESPUESTA_ERROR,
                                "mensaje" => $this->lang->line('error_obtener_detalle_locales')
                            );
                        }
                    } else {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR,
                            "mensaje" => $this->lang->line('error_obtener_detalle_puntps')
                        );
                    }

                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_parametro_url')
                    );
                }

            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo,$token_recibido,$usuario,$operacion);
        $this->response($resultado);
    }

    public function filtrar_historial_post()
    {
        $token_recibido = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_FILTRAR_HISTORIAL;
        $nombre_metodo = NOMBRE_METODO_POST;
        $operacion = "filtrar historial";

        $id_usuario = $this->post("id_usuario", TRUE);
        $local = $this->post("id_local", TRUE);
        $fecha_inicio = $this->post("fecha_inicio", TRUE);
        $fecha_fin = $this->post("fecha_fin", TRUE);

        $parametros = array(
            "token" => $token_recibido,
            "id_usuario" => $id_usuario,
            "id_local" => $local,
            "fecha_inicio" => $fecha_inicio,
            "fehca_fin" => $fecha_fin
        );


        if (isset($token_recibido) && $token_recibido != "") {
            $objeto_token = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);
            if ($objeto_token != "" && isset($objeto_token)) {
                $usuario = $objeto_token->usuario;
                if (isset($id_usuario) && $id_usuario != "") {

                    $id_empresa = $this->id_empresa;

                    $detalle_cliente = $this->buscar_detalle_cliente($id_usuario);

                    if (isset($detalle_cliente)) {
                        $locales = $this->obtener_locales_empresa($id_empresa);

                        if (isset($locales)) {
                            $puntos = $this->filtrar_puntos_usuario($id_usuario);

                            if (isset($puntos)) {
                                $canjes = $this->filtrar_canje_usuario($id_usuario);

                                if (isset($canjes)) {
                                    $resultado = array(
                                        "resultado" => ESTADO_RESPUESTA_OK, "mensaje" => $this->lang->line('cliente_filtar_historial'), "locales" => $locales, "detalle_cliente" => $detalle_cliente, "puntos" => $puntos, "canjes" => $canjes
                                    );
                                } else {
                                    $resultado = array(
                                        "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_obtener_detalle_canjes')
                                    );
                                }
                            } else {
                                $resultado = array(
                                    "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_parametro_url')
                                );
                            }

                        } else {
                            $resultado = array(
                                "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_obtener_detalle_locales')
                            );
                        }
                    } else {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_detalle_cliente')
                        );
                    }

                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_parametro_url')
                    );
                }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token_recibido, $usuario, $operacion);

        $this->response($resultado);
    }

    public function detalle_descuento_get($id_usuario, $paginacion = "")
    {
        $token_recibido = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_DETALLE_DESCUENTO;
        $nombre_metodo = NOMBRE_METODO_GET;
        $operacion = "detalle descuento";

        $parametros = array(
            "token" => $token_recibido,
            "id_usuario" => $id_usuario

        );


        if (isset($token_recibido) && $token_recibido != "") {
            $objeto_token = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);
            if ($objeto_token != "" && isset($objeto_token)) {
                $usuario = $objeto_token->usuario;

                if (isset($id_usuario) && $id_usuario != "") {

                    $id_empresa = $this->id_empresa;

                    $promociones = $this->obtener_promociones($id_empresa);

                    if (isset($promociones)) {
                        $tipo_pago = $this->buscar_tipos_pago($id_empresa);

                        if (isset($tipo_pago)) {
                            $data_usuario = $this->buscar_detalle_cliente($id_usuario);

                            if (isset($data_usuario)) {
                                $detalle_promocion_canje = $this->obtener_promociones_canjeadas($id_usuario, $paginacion);

                                if (isset($detalle_promocion_canje)) {
                                    $resultado = array(
                                        "resultado" => ESTADO_RESPUESTA_OK,
                                        "mensaje" => $this->lang->line('cliente_detalle_descuento'),
                                        "datos_descuento" => array(
                                            "descuento" => $promociones,
                                            "tipo_pago" => $tipo_pago
                                        ),
                                        "detalle_cliente" => $data_usuario,
                                        "total_promocion" => $detalle_promocion_canje["total_promocion"],
                                        "promocion_canje" => $detalle_promocion_canje["promocion_canje"]
                                    );
                                } else {
                                    $resultado = array(
                                        "resultado" => ESTADO_RESPUESTA_ERROR,
                                        "mensaje" => $this->lang->line('error_obtener_promociones_canjeadas')
                                    );
                                }
                            } else {
                                $resultado = array(
                                    "resultado" => ESTADO_RESPUESTA_ERROR,
                                    "mensaje" => $this->lang->line('error_obtener_detalle_cliente')
                                );
                            }

                        } else {
                            $resultado = array(
                                "resultado" => ESTADO_RESPUESTA_ERROR,
                                "mensaje" => $this->lang->line('error_obtener_tipo_pago')
                            );
                        }
                    } else {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR,
                            "mensaje" => $this->lang->line('error_obtener_promociones')
                        );
                    }

                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_parametro_url')
                    );
                }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }


        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token_recibido, $usuario, $operacion);

        $this->response($resultado);
    }

    public function realizar_descuento_post()
    {
        $token_recibido = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_REALIZAR_DESCUENTO;
        $nombre_metodo = NOMBRE_METODO_POST;
        $operacion = "realizar descuento";


        $id_usuario = $this->post("id_usuario", TRUE);
        $id_local = $this->post("id_local", TRUE);
        $id_promocion = $this->post("id_promocion", TRUE);
        $tipo_pago = $this->post("tipo_pago", TRUE);
        $monto = $this->post("monto", TRUE);



        $parametros = array(
            "token" => $token_recibido,
            "id_usuario" => $id_usuario,
            "id_local" => $id_local,
            "id_promocion" => $id_promocion,
            "tipo_pago" => $tipo_pago,
            "monto" => $monto
        );

        if (isset($token_recibido) && $token_recibido != "") {
            $objeto_token = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);
            if ($objeto_token != "" && isset($objeto_token)) {
                $usuario = $objeto_token->usuario;
                if (isset($id_local) && $id_local != "" && isset($id_promocion) && $id_promocion != "" && isset($tipo_pago) && $tipo_pago != "" && isset($id_usuario) && $id_usuario != "") {

                    $id_promocion_canje = $this->guardar_descuento();
                    if (isset($id_promocion_canje) && $id_promocion_canje != "") {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_OK,
                            "mensaje" => $this->lang->line('cliente_realizar_descuento')
                        );
                    } else {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR,
                            "mensaje" => $this->lang->line('error_guardar_descuento')
                        );
                    }

                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_parametros')
                    );
                }

            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token_recibido, $usuario, $operacion);
        $this->response($resultado);
    }

    public function obtener_cliente_get($id_cliente)
    {
        $token_recibido = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_OBTENER_CLIENTE;
        $nombre_metodo = NOMBRE_METODO_GET;
        $operacion ="obtener cliente";

        $parametros = array(
            "token" => $token_recibido,
            "id_cliente" => $id_cliente

        );
        $usuario="";
        if (isset($token_recibido) && $token_recibido != "") {
            $objeto_token = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);
            if ($objeto_token != "" && isset($objeto_token)) {
                $usuario = $objeto_token->usuario;
                if (isset($id_cliente) && $id_cliente != "") {

                    $cliente = $this->buscar_cliente_detalle($id_cliente);

                    if (isset($cliente)) {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_OK, "mensaje" => $this->lang->line('cliente_buscar_detalle_cliente'), "cliente" => $cliente
                        );

                    } else {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_obtener_detalle_cliente')
                        );
                    }
                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_parametros')
                    );
                }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }


        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo,$token_recibido,$usuario,$operacion);
        $this->response($resultado);
    }

    public function obtener_puntos_get($id_usuario, $paginacion = "")
    {
        $token_recibido = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_OBTENER_PUNTOS;
        $nombre_metodo = NOMBRE_METODO_GET;

        $operacion = "obtener puntos de usuario";

        $parametros = array(
            "token" => $token_recibido,
            "id_usuario" => $id_usuario,
            "paginacion" => $paginacion
        );
        $usuario ="";
        if (isset($token_recibido) && $token_recibido != "") {
            $objeto_token = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);
            if ($objeto_token != "" && isset($objeto_token)) {
                $usuario = $objeto_token->usuario;
                if (isset($id_usuario) && $id_usuario != "") {
                    $detalle_puntos = $this->buscar_puntos_usuario($id_usuario, $paginacion);
                    if ($detalle_puntos) {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_OK,
                            "mensaje" => $this->lang->line('cliente_obtener_puntos'),
                            "total_puntos" => $detalle_puntos["total_puntos"],
                            "puntos" => $detalle_puntos["puntos"]
                        );
                    } else {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR,
                            "mensaje" => $this->lang->line('error_obtener_puntos'),
                        );
                    }
                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_parametro_url')
                    );
                }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo,$token_recibido, $usuario, $operacion);

        $this->response($resultado);
    }

    public function obtener_canje_get($id_usuario, $paginacion = "")
    {
        $token_recibido = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_OBTENER_CANJES;
        $nombre_metodo = NOMBRE_METODO_GET;
        $operacion = "obtener canje de usuario";

        $parametros = array(
            "token" => $token_recibido,
            "id_usuario" => $id_usuario,
            "paginacion" => $paginacion
        );

        $usuario = "";
        if (isset($token_recibido) && $token_recibido != "") {
            $objeto_token = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);
            if ($objeto_token != "" && isset($objeto_token)) {
                $usuario = $objeto_token->usuario;
                if (isset($id_usuario) && $id_usuario != "") {

                    $datos_canje = $this->buscar_canje_usuario($id_usuario, $paginacion);

                    if ($datos_canje) {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_OK, "mensaje" => $this->lang->line('cliente_obtener_puntos'), "total_canje" => $datos_canje["total_canje"], "canjes" => $datos_canje["canjes"]
                        );
                    } else {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_obtener_puntos'),

                        );
                    }

                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_parametro_url')
                    );
                }

            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token_recibido, $usuario, $operacion);

        $this->response($resultado);
    }

    public function buscar_cliente_dni_post()
    {
        $token_recibido = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_BUSCAR_CLIENTE_DNI;
        $nombre_metodo = NOMBRE_METODO_POST;
        $operacion = "buscar cliente por dni";


        $dni = $this->post("dni", TRUE);
        $id_local = $this->post("id_local", TRUE);

        $parametros = array(
            "token" => $token_recibido,
            "dni" => $dni,
            "id_local" => $id_local,
        );

        $usuario = "";
        if (isset($token_recibido) && $token_recibido != "") {
            $objeto_token = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);
            if ($objeto_token != "" && isset($objeto_token)) {
                $usuario = $objeto_token->usuario;
                if (isset($id_local) && $id_local != "" && isset($dni) && $dni != "") {

                    $datos_usuario = $this->buscar_cliente_dni_detalle($id_local, $dni);

                    if (!is_null($datos_usuario)) {

                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_OK,
                            "mensaje" => $this->lang->line('cliente_busqueda_dni_usuario'),
                            "usuario" => $datos_usuario
                        );
                    } else {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR,
                            "mensaje" => $this->lang->line('error_buscar_dni_usuario')
                        );
                    }

                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_parametros')
                    );
                }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }


        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo,$token_recibido, $usuario, $operacion);
        $this->response($resultado);
    }

    public function buscar_cliente_tarjeta_post()
    {
        $token_recibido = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_BUSCAR_CLIENTE_TARJETA;
        $nombre_metodo = NOMBRE_METODO_POST;
        $operacion = "buscar cliente por tarjeta";


        $tarjeta = $this->post("tarjeta", TRUE);
        $id_local = $this->post("id_local", TRUE);

        $parametros = array(
            "token" => $token_recibido,
            "tarjeta" => $tarjeta,
            "id_local" => $id_local,
        );

        $usuario = "";
        if (isset($token_recibido) && $token_recibido != "") {
            $objeto_token = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);
            if ($objeto_token != "" && isset($objeto_token)) {
                $usuario = $objeto_token->usuario;
                if (isset($id_local) && $id_local != "" && isset($tarjeta) && $tarjeta != "") {

                    $datos_usuario = $this->buscar_cliente_tarjeta_detalle($id_local, $tarjeta);

                    if (!is_null($datos_usuario)) {

                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_OK,
                            "mensaje" => $this->lang->line('cliente_busqueda_dni_usuario'),
                            "usuario" => $datos_usuario
                        );
                    } else {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR,
                            "mensaje" => $this->lang->line('error_buscar_tarjeta_usuario')
                        );
                    }

                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_parametros')
                    );
                }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }


        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo,$token_recibido, $usuario, $operacion);
        $this->response($resultado);
    }

    public function obtener_beneficios_post()
    {
        $token_recibido = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_OBTENER_BENEFICIOS;
        $nombre_metodo = NOMBRE_METODO_POST;
        $operacion = "obtener beneficios";


        $parametros = array(
            "token" => $token_recibido,
        );
        $usuario = "";
        if (isset($token_recibido) && $token_recibido != "") {
            $objeto_token = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);
            if ($objeto_token != "" && isset($objeto_token)) {
                $usuario = $objeto_token->usuario;
                $beneficios = $this->buscar_beneficios_empresa($this->id_empresa);
                if (!is_null($beneficios)) {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_OK, "mensaje" => $this->lang->line('cliente_obtener_beneficios'), "beneficios" => $beneficios
                    );

                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_obtener_beneficios')
                    );
                }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token_recibido, $usuario, $operacion);
        $this->response($resultado);
    }

    public function obtener_tipo_pagos_post()
    {
        $token_recibido = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_OBTENER_TIPO_PAGOS;
        $nombre_metodo = NOMBRE_METODO_POST;
        $operacion = "obtener tipos de pago";

        $parametros = array(
            "token" => $token_recibido,
        );

        $usuario = "";
        if (isset($token_recibido) && $token_recibido != "") {
            $objeto_token = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);
            if ($objeto_token != "" && isset($objeto_token)) {
                $usuario = $objeto_token->usuario;
                $pagos = $this->buscar_tipos_pago($this->id_empresa);

                if (!is_null($pagos)) {

                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_OK, "mensaje" => $this->lang->line('cliente_tipo_pagos'), "tipo" => $pagos
                    );

                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_tipo_pagos')
                    );
                }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token_recibido, $usuario, $operacion);
        $this->response($resultado);
    }

    public function registrar_cliente_post()
    {
        $token = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_REGISTRAR_CLIENTE;
        $nombre_metodo = NOMBRE_METODO_POST;
        $usuario = "";
        $operacion = "regitrar cliente";

        $nombre = $this->post("nombre", TRUE);
        $apellido = $this->post("apellido", TRUE);
        $sexo = $this->post("sexo", TRUE);
        $fecha_nacimiento = $this->post("fecha_nacimiento", TRUE);
        $dni = $this->post("dni", TRUE);
        $celular = $this->post("celular", TRUE);
        $correo = $this->post("correo", TRUE);
        $empresa = $this->id_empresa;

        $parametros = array(
            "token" => $token,
            "nombre" => $nombre,
            "apellido" => $apellido,
            "sexo" => $sexo,
            "fecha_nacimiento" => $fecha_nacimiento,
            "dni" => $dni,
            "celular" => $celular,
            "correo" => $correo
        );

        $id_red_social = "";
        $contrasenia = $this->generar_contrasenia_aleatoria();

        $objeto_token = validar_token_seguridad_dashboard($token, $this->id_empresa);
        $mensaje = "";
        $formulario_validado = TRUE;

        if ($nombre == "") {
            $mensaje = "Error en el campo nombre";
            $formulario_validado = FALSE;
        } else {
            if ($apellido == "") {
                $mensaje = "Error en el campo apellido";
                $formulario_validado = FALSE;
            } else {
                if ($sexo == "") {
                    $mensaje = "Error en el campo sexo";
                    $formulario_validado = FALSE;
                } else {
                    if ($fecha_nacimiento == "") {
                        $mensaje = "Error en el campo fecha de nacimiento";
                        $formulario_validado = FALSE;
                    } else {
                        if ($dni == "") {
                            $mensaje = "Error en el campo dni";
                            $formulario_validado = FALSE;
                        } else {
                            if ($celular == "") {
                                $mensaje = "Error en el campo celular";
                                $formulario_validado = FALSE;
                            } else {
                                if ($correo == "") {
                                    $mensaje = "Error en el campo correo";
                                    $formulario_validado = FALSE;
                                }
                            }
                        }
                    }
                }
            }
        }


        if ($objeto_token != "" && isset($objeto_token)) {
            $usuario = $objeto_token->usuario;

            if ($formulario_validado === TRUE) {

                $cliente_dni = $this->buscar_cliente_dni($dni);

                if ($cliente_dni == 0) {
                    $cliente_email = $this->buscar_cliente_correo($correo);

                    if ($cliente_email == 0) {

                        $id_persona = $this->guardar_persona($nombre, $apellido, $sexo, $dni, $celular, $fecha_nacimiento);

                        if ($id_persona > 0) {
                            $id_usuario = $this->guardar_usuario($id_persona, $empresa, $correo, $contrasenia, $id_red_social);

                            if ($id_usuario > 0) {
                                $resultado = array(
                                    "resultado" => ESTADO_RESPUESTA_OK, "mensaje" => $this->lang->line('usuario_registro'), "usuario" => array(
                                        "id" => $id_usuario, "nombre" => $nombre, "apellido" => $apellido, "puntos" => PUNTOS_INICIALES_REGISTRO
                                    )
                                );
                            } else {
                                $resultado = array(
                                    "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_usuario_registro')
                                );

                            }

                        } else {
                            $resultado = array(
                                "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_persona_registro')
                            );
                        }
                    } else {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_persona_email')
                        );
                    }
                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_persona_dni')
                    );
                }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $mensaje
                );

            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_token_valido')
            );
        }


        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token, $usuario, $operacion);
        $this->response($resultado);
    }

    public function resetar_contrasenia_get($id_usuario)
    {
        $token = $this->getHeaderRC('token');
        $usuario = "";
        $accion_realizada = ACCION_DASHBOARD_RESETEAR_CONTRASENIA;
        $operacion = "resetar contraseña";
        $nombre_metodo = NOMBRE_METODO_GET;

        $parametros = array(
            "token" => $token,
            "id_usuario" => $id_usuario
        );

        if ($id_usuario != "") {
            $objeto_token = validar_token_seguridad_dashboard($token, $this->id_empresa);

            if ($objeto_token != "" && isset($objeto_token)) {
                $usuario = $objeto_token->usuario;
                $cliente_creado = $this->buscar_usuario($id_usuario);
                $correo = $cliente_creado->correo;
                $data_usuario= array('nombre'=>$cliente_creado->nombre);
                $data_token_contrasenia = $this->contrasenia_model->registrar_token($id_usuario);

                if ($data_token_contrasenia['id_token_contrasenia'] > 0) {
                    $mensaje_enviado = $this->contrasenia_model->enviar_correo_usuario($correo, $data_usuario,$data_token_contrasenia['token_contrasenia']);

                    if ($mensaje_enviado == TRUE) {

                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_OK, "mensaje" => $this->lang->line('contrasena_restablecida')
                        );

                    } else {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_token_contrasenia_correo')
                        );
                    }
                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_token_contrasenia')
                    );
                }

            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_token_valido')
                );
            }

        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_parametros')
            );
        }
        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token, $usuario, $operacion);
        $this->response($resultado);
    }

    public function asignar_contrasenia_post()
    {
        $token = $this->getHeaderRC('token');
        $contrasenia = $this->post('contrasenia', TRUE);

        $usuario = "";
        $accion_realizada = ACCION_DASHBOARD_ASIGNAR_CONTRASENIA;
        $operacion = "asignar contraseña";
        $nombre_metodo = NOMBRE_METODO_POST;

        $parametros = array(
            "token" => $token, "contrasenia" => $contrasenia,
        );


        $objeto_token_contrasenia = $this->contrasenia_model->verificar_token($token);

        if (isset($objeto_token_contrasenia)) {
            $id_usuario = $objeto_token_contrasenia->id_usuario;
            $contrasenia = md5($contrasenia);
            $contrasenia_asignada = $this->usuario_model->asignar_contrasenia($id_usuario, $contrasenia);

            $usuario = $objeto_token_contrasenia->correo;

            if ($contrasenia_asignada) {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_OK, "mensaje" => "Contrasenia asignada de manera exitosa"
                );
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => "Ocurrió un error al momento de actualizar"
                );
            }


        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => "Token vencido"
            );
        }
        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token, $usuario, $operacion);
        $this->response($resultado);
    }

    public function listar_clientes_get($paginacion = "1")
    {
        $token = $this->getHeaderRC('token', TRUE);
        $accion_realizada = ACCION_DASHBOARD_LISTAR_CLIENTE;
        $nombre_metodo = NOMBRE_METODO_GET;
        $operacion = "listar clientes";
        $usuario = "";

        $parametros = array(
            "token" => $token, "paginacion" => $paginacion
        );

        $objeto_token = validar_token_seguridad_dashboard($token, $this->id_empresa);


        if ($objeto_token != "" && isset($objeto_token)) {
            $usuario = $objeto_token->usuario;
            $data_cliente = $this->usuario_model->obtener_usuarios($paginacion);

            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_OK, "mensaje" => $this->lang->line('cliente_listado'), "total_clientes" => $data_cliente['total'], "clientes" => $data_cliente['usuarios']
            );

      } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_token_valido')
            );
        }

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token, $usuario, $operacion);
        $this->response($resultado);
    }

    public function detalle_cliente_get($id_cliente)
    {
        $token = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_DETALLE_CLIENTE;
        $nombre_metodo = NOMBRE_METODO_GET;
        $operacion = "detalle cliente";
        $usuario = "";

        $parametros = array(
            "token" => $token, "id_cliente" => $id_cliente
        );

        if ($id_cliente != "") {

            $objeto_token = validar_token_seguridad_dashboard($token, $this->id_empresa);

            if ($objeto_token != "" && isset($objeto_token)) {
                $usuario = $objeto_token->usuario;
                $cliente = $this->usuario_model->obtener_detalle($id_cliente);

                if (isset($cliente->id)) {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_OK, "mensaje" => $this->lang->line('cliente_detalle'), "cliente" => $cliente
                    );
                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_parametros')
                    );
                }

            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_token_valido')
                );
            }

        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_parametros')
            );
        }


        $objeto_token = validar_token_seguridad_dashboard($token, $this->id_empresa);
        $usuario = $objeto_token->usuario;


        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token, $usuario, $operacion);
        $this->response($resultado);
    }

    public function filtrar_clientes_post($paginacion = "1")
    {
        $dni = $this->post('dni', TRUE);
        $nombre = $this->post('nombre', TRUE);
        $apellido = $this->post('apellido', TRUE);
        $sexo = $this->post('sexo', TRUE);
        $email = $this->post('email', TRUE);
        $estado = $this->post('estado', TRUE);

        $token = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_FILTRAR_CLIENTE;
        $nombre_metodo = NOMBRE_METODO_POST;
        $operacion = "filtrar clientes";
        $usuario = "";

        $parametros = array(
            "token" => $token,
            "dni" => $dni,
            "nombre" => $nombre,
            "apellido" => $apellido,
            "sexo" => $sexo,
            "email" => $email,
            "estado" => $estado
        );


        $objeto_token = validar_token_seguridad_dashboard($token, $this->id_empresa);

        if ($objeto_token != "" && isset($objeto_token)) {
            $usuario = $objeto_token->usuario;
            $data_cliente = $this->usuario_model->filtrar_clientes($paginacion, $dni, $nombre, $apellido, $sexo, $estado,$email);
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_OK,
                "mensaje" => $this->lang->line('cliente_filtro'),
                "total_clientes" => $data_cliente['total'],
                "clientes" => $data_cliente['usuarios']
            );

        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_token_valido')
            );
        }


        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token, $usuario, $operacion);
        $this->response($resultado);

    }

    public function obtener_clientes_notificacion_get(){
        $token = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_OBTENER_LISTA_CLIENTES_NOTIFICACION;
        $nombre_metodo = NOMBRE_METODO_GET;
        $operacion = "Obtener clientes notificacion";


        $parametros = array(
            "token" => $token
        );
        $id_empresa = $this->id_empresa;
        $datos_clientes = $this->obtener_lista_clientes_notificacion($id_empresa);

        $objeto_token = validar_token_seguridad_dashboard($token,$id_empresa);
        $usuario = $objeto_token->usuario;

        if ($objeto_token!="" && isset($objeto_token)) {
            if (isset($datos_clientes))
            {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_OK,
                    "mensaje" => $this->lang->line('mensaje_operacion_exito'),
                    "clientes" => $datos_clientes
                );
            } else
            {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_obtener_clientes')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_valido')
            );
        }
        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo,$token, $usuario, $operacion);
        $this->response($resultado);
    }

    public function generar_excel_post(){

        $dni = $this->post('dni', TRUE);
        $nombre = $this->post('nombre', TRUE);
        $apellido = $this->post('apellido', TRUE);
        $sexo = $this->post('sexo', TRUE);
        $email = $this->post('email', TRUE);
        $estado = $this->post('estado', TRUE);

        $token = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_GENERAR_EXCEL_CLIENTE;
        $nombre_metodo = NOMBRE_METODO_POST;
        $operacion = "generar excel listado";
        $usuario = "";

        $parametros = array(
            "token" => $token,
            "dni" => $dni,
            "nombre" => $nombre,
            "apellido" => $apellido,
            "sexo" => $sexo,
            "email" => $email,
            "estado" => $estado
        );


        $objeto_token = validar_token_seguridad_dashboard($token, $this->id_empresa);

        if ($objeto_token != "" && isset($objeto_token)) {
            $usuario = $objeto_token->usuario;
            $data_cliente = $this->usuario_model->generar_excel_clientes($dni, $nombre, $apellido, $sexo, $estado,$email);
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_OK,
                "mensaje" => $this->lang->line('cliente_filtro'),
                "clientes" => $data_cliente
            );

        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_token_valido')
            );
        }


        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token, $usuario, $operacion);
        $this->response($resultado);

    }

    public function asignar_tarjeta_post()
    {
        $token = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_ASIGNAR_TARJETA;
        $nombre_metodo = NOMBRE_METODO_POST;

        $operacion = "asignar tarjeta";

        $tarjeta= $this->post("tarjeta", TRUE);
        $id_usuario = $this->post("id_usuario", TRUE);
        $empresa = $this->id_empresa;
        $usuario = "";
        $parametros = array(
            "token" => $token,
            "id_usuario"=>$id_usuario,
            "tarjeta" => $tarjeta
        );


        $objeto_token = validar_token_seguridad_dashboard($token, $this->id_empresa);
        $mensaje = "";

        if ($objeto_token != "" && isset($objeto_token)) {
            $usuario = $objeto_token->usuario;
                if ($tarjeta != "" || $tarjeta != null ) {
                    if (is_numeric($tarjeta)) {
                        $tarjeta_dni_cliente = $this->buscar_tarjeta_dni_cliente($id_usuario);
                        if ($tarjeta_dni_cliente == null) {

                        $tarjeta_cliente = $this->buscar_tarjeta_cliente($id_usuario, $tarjeta);
                        if ($tarjeta_cliente == null) {

                            $usuario_tarjeta = $this->actualizar_tarjeta($id_usuario, $tarjeta);

                            if ($usuario_tarjeta != null) {
                                $resultado = array(
                                    "resultado" => ESTADO_RESPUESTA_OK,
                                    "mensaje" => $this->lang->line('registro_tarjeta_cliente')
                                );
                            } else {
                                $resultado = array(
                                    "resultado" => ESTADO_RESPUESTA_ERROR,
                                    "mensaje" => $this->lang->line('error_asignar_tarjeta')
                                );
                            }

                        } else {
                            $resultado = array(
                                "resultado" => ESTADO_RESPUESTA_ERROR,
                                "mensaje" => $this->lang->line('error_asignar_tarjeta_repetido')
                            );
                        }
                    }else{
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR,
                            "mensaje" => $this->lang->line('error_asignar_tarjeta_repetido_dni')
                        );

                    }

                    }else{
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR,
                            "mensaje" => $this->lang->line('error_asignar_tarjeta_numerico')
                        );
                    }

                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_asignar_tarjeta_vacio')
                    );
                }

        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_token_valido')
            );
        }


        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token, $usuario, $operacion);
        $this->response($resultado);
    }

    public function eliminar_tarjeta_post()
    {
        $token = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_ELIMINAR_TARJETA;
        $nombre_metodo = NOMBRE_METODO_POST;
        $usuario = "";
        $operacion = "eliminar cliente";

        $id_usuario = $this->post("id_usuario", TRUE);

        $parametros = array(
            "token" => $token,
            "id_usuario"=>$id_usuario,
        );


        $objeto_token = validar_token_seguridad_dashboard($token, $this->id_empresa);

        if ($objeto_token != "" && isset($objeto_token)) {

            $tarjeta = "";
            $usuario = $this->actualizar_tarjeta($id_usuario,$tarjeta);

            if ($usuario != null){
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_OK,
                    "mensaje" => $this->lang->line('registro_tarjeta_cliente_eliminar')
                );
            }else{
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_asignar_tarjeta_eliminar')
                );
            }

        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_token_valido')
            );
        }


        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token, $usuario, $operacion);
        $this->response($resultado);
    }



    /****************METODOS*******************/

    public function generar_contrasenia_aleatoria()
    {
        $tamanio = 10;
        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $tamanio);
    }

    public function buscar_cliente_dni($dni)
    {
        $parametros_busqueda_cliente = array(
            "select" => "tbl_usuario.id as id,
            persona.nombre as nombre,
            persona.apellido as apellido,
            persona.sexo as sexo,
            persona.dni as dni",

            "join" => array(
                "tbl_persona persona, persona.id = tbl_usuario.id_persona", "tbl_empresa empresa, empresa.id = tbl_usuario.id_empresa", "tbl_local local, local.id_empresa = empresa.id",
            ), "where" => "tbl_usuario.estado = " . ESTADO_ACTIVO .
                " and persona.estado = " . ESTADO_ACTIVO .
                " and tbl_usuario.id_empresa = " . $this->id_empresa .
                " and persona.dni = '" . $dni . "'"
        );

        $cliente = $this->usuario_model->total_records($parametros_busqueda_cliente);

        return $cliente;
    }

    public function buscar_cliente_correo($correo)
    {
        $parametros_busqueda_cliente = array(
            "select" => "tbl_usuario.id as id,
            persona.nombre as nombre,
            persona.apellido as apellido,
            persona.sexo as sexo,
            persona.dni as dni",

            "join" => array(
                "tbl_persona persona, persona.id = tbl_usuario.id_persona",
                "tbl_empresa empresa, empresa.id = tbl_usuario.id_empresa",
                "tbl_local local, local.id_empresa = empresa.id",
            ),
            "where" => "tbl_usuario.estado = " . ESTADO_ACTIVO .
                " and persona.estado = " . ESTADO_ACTIVO .
                " and tbl_usuario.id_empresa = " . $this->id_empresa .
                " and tbl_usuario.correo = '" . $correo . "'"
        );

        $cliente = $this->usuario_model->total_records($parametros_busqueda_cliente);

        return $cliente;
    }


    private function guardar_persona($nombre, $apellido, $sexo, $dni, $celular, $fecha_nacimiento)
    {
        $parametros = array(
            "nombre" => $nombre, "apellido" => $apellido, "sexo" => $sexo, "dni" => $dni, "telefono" => $celular, "fecha_nacimiento" => $fecha_nacimiento, "estado" => "1"
        );

        $persona_existe = $this->buscar_persona($dni);
        if (!isset($persona_existe)) {
            $parametros["fecha_registro"] = date("Y-m-d H:i:s");
            $id_persona = $this->persona_model->insert($parametros);
        } else {
            /** Se actulizan los datos de la persona**/
            $parametros["fecha_modificacion"] = date("Y-m-d H:i:s");
            $id_persona = $persona_existe->id;
            $fila_afectada = $this->persona_model->update($id_persona, $parametros);
            if ($fila_afectada === FALSE) {
                $id_persona = 0;
            }
        }
        return $id_persona;
    }

    private function buscar_persona($dni)
    {
        $parametros = array(
            "select" => "*", "where" => "tbl_persona.dni='" . $dni . "'" . " and tbl_persona.estado = 1"
        );
        $existe_persona = $this->persona_model->get_search_row($parametros);
        return $existe_persona;
    }

    private function guardar_usuario($id_persona, $empresa, $correo, $contrasenia, $id_red_social)
    {
        $parametros = array(
            "correo" => $correo, "contrasenia" => $contrasenia, "estado" => "1"
        );

        $existe_usuario = $this->buscar_usuario_por_correo_empresa($correo, $empresa);

        if (!isset($existe_usuario)) {
            $parametros["id_empresa"] = $empresa;
            $parametros["id_persona"] = $id_persona;
            $parametros["id_red_social"] = $id_red_social;
            $parametros["punto_acumulado"] = "0";
            $parametros["fecha_registro"] = date("Y-m-d H:i:s");
            $id_usuario = $this->usuario_model->insert($parametros, TRUE);
        } else {
            $parametros["fecha_modificacion"] = date("Y-m-d H:i:s");
            $id_usuario = $existe_usuario->id;
            $fila_afectada = $this->usuario_model->update($id_usuario, $parametros);
            if ($fila_afectada === FALSE) {
                $id_usuario = 0;
            }
        }
        return $id_usuario;
    }

    private function buscar_usuario_por_correo_empresa($correo, $id_empresa)
    {
        $parametros = array(
            "select" => "*", "where" => "tbl_usuario.correo = '" . $correo . "'" . " and tbl_usuario.id_empresa = " . $id_empresa . " and tbl_usuario.estado = 1"
        );
        $existe_usuario = $this->usuario_model->get_search_row($parametros);
        return $existe_usuario;
    }

    public function buscar_cliente_detalle($id_cliente)
    {
        $parametros_busqueda_cliente = array(
            "select" => "tbl_usuario.id as id,
            persona.nombre as nombre,
            persona.apellido as apellido,
            persona.sexo as sexo,
            persona.fecha_nacimiento as fecha_nacimiento,
            tbl_usuario.correo as correo,
            tbl_usuario.fecha_registro as fecha_registro,
            ifnull(persona.telefono ,'') as telefono,
            tbl_usuario.punto_acumulado as punto_acumulado",

            "join" => array(
                "tbl_persona persona, persona.id = tbl_usuario.id_persona"
            ), "where" => "tbl_usuario.estado = " . ESTADO_ACTIVO . " and persona.estado = " . ESTADO_ACTIVO . " and tbl_usuario.id_empresa = " . $this->id_empresa . " and tbl_usuario.id = $id_cliente"
        );

        $cliente = $this->usuario_model->get_search_row($parametros_busqueda_cliente);


        $puntos = $this->buscar_puntos_por_vencer($id_cliente);
        $puntos_usados = $this->buscar_puntos_usados($id_cliente);

        $visita = $this->obtener_visitas($id_cliente);
        $ultima_visita = $visita->fecha_canje;

        if ($ultima_visita < $visita->fecha_puntos) {
            $ultima_visita = $visita->fecha_puntos;
        }


        $cliente->puntos_por_vencer = $puntos->puntos_por_vencer;
        $cliente->puntos_usado = $puntos_usados->puntos_usados;
        $cliente->ultima_visita = $ultima_visita;


        return $cliente;
    }

    public function obtener_visitas($id_cliente)
    {
        $parametros_obtener_ultima_visita = array(
            "select" => "max(canje.fecha_registro) as fecha_canje,
            max(puntos.fecha_registro) as fecha_puntos", "join" => array(
                "tbl_puntos puntos, puntos.id_usuario = tbl_usuario.id", "tbl_canje canje, canje.id_usuario = tbl_usuario.id"
            ), "where" => "tbl_usuario.id = $id_cliente
            and puntos.estado = " . ESTADO_ACTIVO . "
            and canje.estado = " . ESTADO_ACTIVO
        );

        $visitas = $this->usuario_model->get_search_row($parametros_obtener_ultima_visita);
        return $visitas;
    }

    public function buscar_cliente_detalle_test_get()
    {
        $id_cliente = "1";

        $cliente = $this->buscar_cliente_detalle($id_cliente);
        $this->response($cliente);
    }

    public function buscar_empresa_token($token)
    {
        $parametros_busqueda_empresa = array(
            "select" => "empresa.*", "join" => array(
                "tbl_contacto contacto, contacto.id = tbl_token_dashboard.id_contacto", "tbl_empresa empresa, empresa.id = contacto.id_empresa"
            ), "where" => "tbl_token_dashboard.token = '$token'
            and tbl_token_dashboard.estado = " . ESTADO_ACTIVO . "
            and contacto.estado = " . ESTADO_ACTIVO . "
            and empresa.estado = " . ESTADO_ACTIVO
        );

        $empresa_encontrada = $this->token_dashboard_model->get_search_row($parametros_busqueda_empresa);
        return $empresa_encontrada;
    }

    public function listar_usuarios($empresa, $paginacion = "")
    {
        $parametros_listado_usuario = array(
            "select" => "tbl_usuario.id as id,
            persona.nombre as nombre,
            persona.apellido as apellido,
            persona.dni as dni,
            tbl_usuario.correo as correo,
            tbl_usuario.punto_acumulado as puntos",

            "join" => array(
                "tbl_persona persona, persona.id = tbl_usuario.id_persona"
            ), "where" => "tbl_usuario.estado = " . ESTADO_ACTIVO . "
            and tbl_usuario.id_empresa = $empresa
            and persona.estado = " . ESTADO_ACTIVO
        );
        if (is_numeric($paginacion) && $paginacion > 0) {
            $inicio = ELEMENTOS_POR_PAGINA * ($paginacion - 1);

            $usuarios = $this->usuario_model->search_data_array($parametros_listado_usuario, $inicio, ELEMENTOS_POR_PAGINA);

        } else {
            $usuarios = $this->usuario_model->search_array($parametros_listado_usuario);
        }

        $total_usuarios = $this->usuario_model->total_records($parametros_listado_usuario);

        $datos_usuarios = array(
            "total_usuarios" => $total_usuarios, "usuarios" => $usuarios
        );

        return $datos_usuarios;
    }

    public function buscar_usuarios($empresa, $paginacion = "")
    {
        $nombre = $this->post("nombre", TRUE);
        $apellido = $this->post("apellido", TRUE);
        $dni = $this->post("dni", TRUE);
        $correo = $this->post("correo", TRUE);

        $filtro_nombre = "";
        $filtro_apellido = "";
        $filtro_dni = "";
        $filtro_correo = "";
        if ($nombre != "") {
            $filtro_nombre = " and persona.nombre like '%$nombre%'";
        }
        if ($apellido != "") {
            $filtro_apellido = " and persona.apellido like '%$apellido%'";
        }
        if ($dni != "") {
            $filtro_dni = " and persona.dni like '%$dni%'";
        }
        if ($correo != "") {
            $filtro_correo = " and persona.correo like '%$correo%'";
        }


        $parametros_listado_usuario = array(
            "select" => "tbl_usuario.id as id,
            persona.nombre as nombre,
            persona.apellido as apellido,
            persona.dni as dni,
            tbl_usuario.correo as correo,
            tbl_usuario.punto_acumulado as puntos",

            "join" => array(
                "tbl_persona persona, persona.id = tbl_usuario.id_persona",
            ), "where" => "tbl_usuario.estado = " . ESTADO_ACTIVO . "
            and tbl_usuario.id_empresa = $empresa
            and persona.estado = " . ESTADO_ACTIVO . $filtro_nombre . $filtro_apellido . $filtro_correo . $filtro_dni
        );

        if (is_numeric($paginacion) && $paginacion > 0) {
            $inicio = ELEMENTOS_POR_PAGINA * ($paginacion - 1);
            $usuarios = $this->usuario_model->search_data_array($parametros_listado_usuario, $inicio, ELEMENTOS_POR_PAGINA);

        } else {
            $usuarios = $this->usuario_model->search_array($parametros_listado_usuario);
        }

        $total_usuarios = $this->usuario_model->total_records($parametros_listado_usuario);

        $datos_usuarios = array(
            "total_usuarios" => $total_usuarios, "usuarios" => $usuarios
        );

        return $datos_usuarios;
    }

    public function buscar_usuario($id_usuario)
    {
        $parametros_listado_usuario = array(
            "select" => "tbl_usuario.id,tbl_usuario.id_empresa,tbl_usuario.id_persona,tbl_usuario.id_red_social,tbl_usuario.correo,
            tbl_usuario.punto_acumulado,tbl_usuario.fecha_registro,tbl_usuario.estado,persona.nombre",
            "join" => array("tbl_persona persona, persona.id = tbl_usuario.id_persona"),
            "where" => "tbl_usuario.estado = " . ESTADO_ACTIVO . "
            and tbl_usuario.id = $id_usuario"
        );

        $usuario = $this->usuario_model->get_search_row($parametros_listado_usuario);
        return $usuario;
    }

    public function buscar_tipos_pago($empresa)
    {
        $parametros_buscar_tipo_pago = array(
            "select" => "pago.id as id,
            pago.nombre as nombre", "join" => array(
                "tbl_tipo_pago pago, pago.id = tbl_tipo_pago_empresa.id_tipo_pago"
            ), "where" => "pago.estado = " . ESTADO_ACTIVO . "
            and tbl_tipo_pago_empresa.estado = " . ESTADO_ACTIVO . "
            and tbl_tipo_pago_empresa.id_empresa = $empresa"
        );
        $tipo_pago = $this->tipo_pago_empresa_model->search_array($parametros_buscar_tipo_pago);

        return $tipo_pago;
    }

    public function buscar_detalle_cliente($usuario)
    {

        $parametros_detalle_cliente = array(
            "select" => "tbl_usuario.id as id,
            persona.nombre as nombre,
            persona.apellido as apellido,
            persona.sexo as sexo,
            tbl_usuario.correo as correo,
            persona.telefono as telefono,
            persona.dni as dni,
            tbl_usuario.punto_acumulado as puntos_acumulados",

            "join" => array(
                "tbl_persona persona, persona.id = tbl_usuario.id_persona"
            ), "where" => "tbl_usuario.estado = " . ESTADO_ACTIVO . "
            and tbl_usuario.id = $usuario
            and persona.estado = " . ESTADO_ACTIVO
        );


        $detalle_cliente = $this->usuario_model->get_search_row($parametros_detalle_cliente);

        $puntos = $this->buscar_puntos_por_vencer($usuario);
        $puntos_usados = $this->buscar_puntos_usados($usuario);
        $detalle_cliente->puntos_por_vencer = $puntos->puntos_por_vencer;
        $detalle_cliente->puntos_usado = $puntos_usados->puntos_usados;

        return $detalle_cliente;
    }

    public function buscar_puntos_usuario($usuario, $paginacion = "")
    {
        $fecha_actual = date("Y-m-d H:i:s");
        $parametros_busqueda_puntos_usuario = array(
            "select" => "tbl_puntos.id as id,
            tbl_puntos.fecha_registro as fecha_registro,
            tipo_pago.nombre as tipo_pago,
            tbl_puntos.monto as monto,
            tbl_puntos.puntos as puntos_ganados,
            tbl_puntos.puntos_acumulado as puntos_totales,
            tbl_puntos.fecha_fin as fecha_caducidad",

            "join" => array(
                "tbl_tipo_pago tipo_pago, tipo_pago.id = tbl_puntos.id_tipo_pago"
            ), "where" => "tbl_puntos.id_usuario = $usuario
            and tbl_puntos.estado = " . ESTADO_ACTIVO . "
            and tbl_puntos.fecha_fin > '$fecha_actual'"
        );

        if (is_numeric($paginacion) && $paginacion > 0) {
            $inicio = ELEMENTOS_POR_PAGINA * ($paginacion - 1);

            $puntos_usuario = $this->puntos_model->search_data_array($parametros_busqueda_puntos_usuario, $inicio, ELEMENTOS_POR_PAGINA);
        } else {
            $puntos_usuario = $this->puntos_model->search_array($parametros_busqueda_puntos_usuario);
        }

        $total_puntos = $this->puntos_model->total_records($parametros_busqueda_puntos_usuario);
        $datos_puntos = array(
            "total_puntos" => $total_puntos, "puntos" => $puntos_usuario
        );

        return $datos_puntos;
    }

    public function filtrar_puntos_usuario($usuario)
    {

        $local = $this->post("id_local", TRUE);
        $fecha_inicio = $this->post("fecha_inicio", TRUE);
        $fecha_fin = $this->post("fecha_fin", TRUE);

        $filtro_local = "";
        $filtro_fecha_inicio = "";
        $filtro_fecha_fin = "";

        if ($local != "") {
            $filtro_local = " and tbl_puntos.id_local = $local";
        }
        if ($fecha_inicio != "") {
            $filtro_fecha_inicio = " and tbl_puntos.fecha_registro > '$fecha_inicio'";
        }
        if ($fecha_fin != "") {
            $filtro_fecha_fin = " and tbl_puntos.fecha_registro < '$fecha_fin'";
        }

        $parametros_busqueda_puntos_usuario = array(
            "select" => "tbl_puntos.id as id,
            tbl_puntos.fecha_registro as fecha_registro,
            tbl_puntos.id_tipo_pago as tipo_pago,
            tbl_puntos.monto as monto,
            tbl_puntos.puntos as puntos_ganados,
            tbl_puntos.puntos_acumulado as puntos_totales,
            tbl_puntos.fecha_fin as fecha_caducidad",

            "where" => "tbl_puntos.id_usuario = $usuario
            and tbl_puntos.estado < 2" . $filtro_local . $filtro_fecha_inicio . $filtro_fecha_fin
        );

        $puntos_usuario = $this->puntos_model->search_array($parametros_busqueda_puntos_usuario);

        return $puntos_usuario;
    }

    public function buscar_puntos_por_vencer($usuario)
    {
        $fecha_actual = date("Y-m-d H:i:s");
        $fecha_vencimiento = strtotime('+1 month', strtotime($fecha_actual));
        $fecha_por_vencer = date("Y-m-d H:i:s", $fecha_vencimiento);
        $parametros_busqueda_puntos_por_vencer = array(
            "select" => "SUM(tbl_puntos.puntos) as puntos_por_vencer",

            "where" => "tbl_puntos.id_usuario = $usuario
            and tbl_puntos.estado = " . ESTADO_ACTIVO . "
            and tbl_puntos.fecha_fin < '$fecha_por_vencer'
            and tbl_puntos.fecha_fin > '$fecha_actual'"
        );

        $puntos_usuario = $this->puntos_model->get_search_row($parametros_busqueda_puntos_por_vencer);

        if (!isset($puntos_usuario->puntos_por_vencer)) {
            $puntos_usuario->puntos_por_vencer = "0";
        }

        return $puntos_usuario;
    }

    public function buscar_puntos_usados($usuario)
    {
        $parametros_busqueda_puntos_usados = array(
            "select" => "SUM(tbl_canje.puntos) as puntos_usados", "where" => "tbl_canje.estado = " . ESTADO_ACTIVO . "
            and tbl_canje.id_usuario = $usuario"
        );

        $puntos_canjeados = $this->canje_model->get_search_row($parametros_busqueda_puntos_usados);
        return $puntos_canjeados;
    }

    public function buscar_cliente_dni_detalle($id_local, $dni)
    {
        $parametros_busqueda_cliente = array(
            "select" => "tbl_usuario.id as id,
            persona.nombre as nombre,
            persona.apellido as apellido,
            persona.sexo as sexo,
            persona.dni as dni,
            persona.fecha_nacimiento as fecha_nacimiento,
            tbl_usuario.correo as correo,
            tbl_usuario.fecha_registro as fecha_registro,
            tbl_usuario.tarjeta as tarjeta,
            ifnull(persona.telefono ,'') as telefono,
            tbl_usuario.punto_acumulado as punto_acumulado",

            "join" => array(
                "tbl_persona persona, persona.id = tbl_usuario.id_persona", "tbl_empresa empresa, empresa.id = tbl_usuario.id_empresa", "tbl_local local, local.id_empresa = empresa.id",
            ), "where" => "tbl_usuario.estado = " . ESTADO_ACTIVO . " and persona.estado = " . ESTADO_ACTIVO . " and tbl_usuario.id_empresa = " . $this->id_empresa . " and local.id = '" . $id_local . "'
                 and local.estado = " . ESTADO_ACTIVO . " and persona.dni = '" . $dni . "'"
        );

        $cliente = $this->usuario_model->get_search_row($parametros_busqueda_cliente);

        if (isset($cliente)) {

            $id_cliente = $cliente->id;
            $puntos = $this->buscar_puntos_por_vencer($id_cliente);
            $puntos_usados = $this->buscar_puntos_usados($id_cliente);

            $cliente->puntos_por_vencer = $puntos->puntos_por_vencer;
            $cliente->puntos_usado = $puntos_usados->puntos_usados;
        }

        return $cliente;
    }

    public function buscar_cliente_tarjeta_detalle($id_local, $tarjeta)
    {
        $parametros_busqueda_cliente = array(
            "select" => "tbl_usuario.id as id,
            persona.nombre as nombre,
            persona.apellido as apellido,
            persona.sexo as sexo,
            persona.dni as dni,
            persona.fecha_nacimiento as fecha_nacimiento,
            tbl_usuario.correo as correo,
            tbl_usuario.fecha_registro as fecha_registro,
            ifnull(persona.telefono ,'') as telefono,
            tbl_usuario.punto_acumulado as punto_acumulado",

            "join" => array(
                "tbl_persona persona, persona.id = tbl_usuario.id_persona", "tbl_empresa empresa, empresa.id = tbl_usuario.id_empresa", "tbl_local local, local.id_empresa = empresa.id",
            ), "where" => "tbl_usuario.estado = " . ESTADO_ACTIVO . " and persona.estado = " . ESTADO_ACTIVO . " and tbl_usuario.id_empresa = " . $this->id_empresa . " and local.id = '" . $id_local . "'
                 and local.estado = " . ESTADO_ACTIVO . " and tbl_usuario.tarjeta = '" . $tarjeta . "'"
        );

        $cliente = $this->usuario_model->get_search_row($parametros_busqueda_cliente);

        if (isset($cliente)) {

            $id_cliente = $cliente->id;
            $puntos = $this->buscar_puntos_por_vencer($id_cliente);
            $puntos_usados = $this->buscar_puntos_usados($id_cliente);

            $cliente->puntos_por_vencer = $puntos->puntos_por_vencer;
            $cliente->puntos_usado = $puntos_usados->puntos_usados;
        }

        return $cliente;
    }

    /***** ********/

    public function buscar_beneficios_empresa($id_empresa)
    {

        $tiempo_actual = date('Y-m-d H:i:s');

        $parametros_beneficios_empresa = array(
            "select" => "tbl_beneficio.id as id,
            tbl_beneficio.nombre as nombre,
            tbl_beneficio.puntos as puntos,
            tbl_beneficio.puntos_descripcion as puntos_descripcion,
            ",

            "where" => "tbl_beneficio.estado = " . ESTADO_ACTIVO . " 
                        and tbl_beneficio.id_empresa = $id_empresa"
        );


        $beneficios = $this->beneficio_model->search_array($parametros_beneficios_empresa);


        return $beneficios;
    }

    public function buscar_canje_usuario($usuario, $paginacion = "")
    {
        $parametros_busqueda_canje_usuario = array(
            "select" => "tbl_canje.id as id,
            tbl_canje.fecha_registro as fecha_registro,
            beneficio.nombre as beneficio,
            local.nombre as local,
            tbl_canje.puntos as puntos_usados,
            tbl_canje.puntos_acumulado as puntos_totales",

            "join" => array(
                "tbl_beneficio beneficio, beneficio.id = tbl_canje.id_beneficio", "tbl_local local, local.id = tbl_canje.id_local"
            ), "where" => "tbl_canje.estado < 2
            and tbl_canje.id_usuario = $usuario"
        );

        if (is_numeric($paginacion) && $paginacion > 0) {
            $inicio = ELEMENTOS_POR_PAGINA * ($paginacion - 1);
            $canjes = $this->canje_model->search_data_array($parametros_busqueda_canje_usuario, $inicio, ELEMENTOS_POR_PAGINA);
        } else {
            $canjes = $this->canje_model->search_array($parametros_busqueda_canje_usuario);
        }

        $total_canje = $this->canje_model->total_records($parametros_busqueda_canje_usuario);

        $detalle_canje = array(
            "total_canje" => $total_canje, "canjes" => $canjes
        );

        return $detalle_canje;
    }

    public function filtrar_canje_usuario($usuario)
    {
        $local = $this->post("id_local", TRUE);
        $fecha_inicio = $this->post("fecha_inicio", TRUE);
        $fecha_fin = $this->post("fecha_fin", TRUE);

        $filtro_local = "";
        $filtro_fecha_inicio = "";
        $filtro_fecha_fin = "";

        if ($local != "") {
            $filtro_local = " and tbl_canje.id_local = $local";
        }
        if ($fecha_inicio != "") {
            $filtro_fecha_inicio = " and tbl_canje.fecha_registro > '$fecha_inicio'";
        }

        if ($fecha_fin != "") {
            $filtro_fecha_fin = " and tbl_canje.fecha_registro < '$fecha_fin'";
        }

        $parametros_busqueda_canje_usuario = array(
            "select" => "tbl_canje.id as id,
            tbl_canje.fecha_registro as fecha_registro,
            beneficio.nombre as beneficio,
            local.nombre as local,
            tbl_canje.puntos as puntos_usados,
            tbl_canje.puntos_acumulado as puntos_totales",

            "join" => array(
                "tbl_beneficio beneficio, beneficio.id = tbl_canje.id_beneficio", "tbl_local local, local.id = tbl_canje.id_local"
            ), "where" => "tbl_canje.estado < 2
            and tbl_canje.id_usuario = $usuario" . $filtro_local . $filtro_fecha_inicio . $filtro_fecha_fin
        );

        $canjes = $this->canje_model->search_array($parametros_busqueda_canje_usuario);

        return $canjes;
    }

    public function obtener_locales_empresa($empresa)
    {
        $parametros_busqueda_locales_empresa = array(
            "select" => "tbl_local.id as id,
            tbl_local.nombre as nombre",

            "where" => "tbl_local.id_empresa = $empresa
            and tbl_local.estado < 2"
        );
        $locales = $this->local_model->search_array($parametros_busqueda_locales_empresa);
        return $locales;
    }

    public function obtener_promociones($id_empresa)
    {
        $parametros_promociones_empresa = array(
            "select" => "tbl_promocion.id,
            tbl_promocion.nombre", "where" => "tbl_promocion.estado = " . ESTADO_ACTIVO . "
            and tbl_promocion.id_empresa = $id_empresa"
        );

        $promociones = $this->promocion_model->search_array($parametros_promociones_empresa);
        return $promociones;
    }

    public function obtener_promociones_canjeadas($usuario, $paginacion = "")
    {
        $parametro_promociones_canjeadas = array(
            "select" => "tbl_promocion_canje.id as id,
            tbl_promocion_canje.fecha_registro as fecha_registro,
            promocion.descuento as descuento,
            tbl_promocion_canje.monto as monto,
            tbl_promocion_canje.tipo_pago as tipo_pago", "join" => array(
                "tbl_promocion as promocion, promocion.id = tbl_promocion_canje.id_promocion"
            ), "where" => "tbl_promocion_canje.estado = " . ESTADO_ACTIVO . "
            and tbl_promocion_canje.id_usuario = $usuario
            and promocion.id_empresa = $this->id_empresa"
        );

        if (is_numeric($paginacion) && $paginacion > 0) {
            $inicio = ELEMENTOS_POR_PAGINA * ($paginacion - 1);
            $promocion_canje = $this->promocion_canje_model->search_data_array($parametro_promociones_canjeadas, $inicio, ELEMENTOS_POR_PAGINA);
        } else {
            $promocion_canje = $this->promocion_canje_model->search_array($parametro_promociones_canjeadas);
        }

        $total_promocion = $this->promocion_canje_model->total_records($parametro_promociones_canjeadas);

        $detalle_promocion_canje = array(
            "total_promocion" => $total_promocion, "promocion_canje" => $promocion_canje
        );
        return $detalle_promocion_canje;
    }

    public function calcular_puntos($monto, $id_empresa)
    {
        $puntos_totales = "-1";

        $parametros_calcular_puntos = array(
            "select" => "formula_puntos.monto as monto_formula,
            formula_puntos.punto as puntos_formula", "join" => array(
                "tbl_formula_puntos_empresa puntos_empresa, puntos_empresa.id_empresa = tbl_empresa.id", "tbl_formula_puntos formula_puntos, formula_puntos.id = puntos_empresa.id_formula_puntos"
            ), "where" => "tbl_empresa.id = $id_empresa"
        );


        $formula = $this->empresa_model->get_search_row($parametros_calcular_puntos);

        if (isset($formula)) {
            $puntos_totales = ($formula->puntos_formula * $monto) / $formula->monto_formula;
            $puntos_totales = floor($puntos_totales);
        }


        return $puntos_totales;
    }

    public function guardar_puntos($usuario, $id_empresa)
    {
        $fecha_actual = date("Y-m-d H:i:s");
        $fecha_vencimiento = strtotime('+1 year', strtotime($fecha_actual));
        $fecha_fin = date("Y-m-d H:i:s", $fecha_vencimiento);

        $id_usuario = $this->post("id_usuario", TRUE);
        $id_local = $this->post("id_local", TRUE);
        $id_mozo = $this->post("mozo", TRUE);
        $tipo_pago = $this->post("tipo_pago", TRUE);
        $monto = $this->post("monto", TRUE);

        $puntos_totales_usuario = $usuario->punto_acumulado;
        $puntos_ganados = $this->calcular_puntos($monto, $id_empresa);

        if (isset($puntos_ganados) && $puntos_ganados > -1) {
            if ($puntos_ganados == 0) {
                $puntos_ganados = "0";
            }

            $parametros_guardar_puntos = array(
                "id_usuario" => $id_usuario, "id_local" => $id_local, "id_mozo" => $id_mozo, "monto" => $monto, "id_tipo_pago" => $tipo_pago, "puntos" => $puntos_ganados, "puntos_acumulado" => $puntos_totales_usuario, "fecha_inicio" => $fecha_actual, "fecha_fin" => $fecha_fin, "fecha_puntos" => $fecha_actual, "fecha_registro" => $fecha_actual, "estado" => ESTADO_ACTIVO

            );

            $id_puntos = $this->puntos_model->insert($parametros_guardar_puntos, TRUE);

            $parametros_actualizar_usuario = array(
                "fecha_modificacion" => $fecha_actual, "punto_acumulado" => $puntos_totales_usuario + $puntos_ganados
            );

            $this->usuario_model->update($id_usuario, $parametros_actualizar_usuario);
        } else {
            $id_puntos = null;
        }

        $datos_puntos = [
            'id_puntos' => $id_puntos, 'puntos' => $puntos_ganados
        ];


        return $datos_puntos;
    }

    public function guardar_canje($usuario)
    {
        $fecha_actual = date("Y-m-d H:i:s");


        $id_usuario = $this->post("id_usuario", TRUE);
        $id_beneficio = $this->post("id_beneficio", TRUE);
        $id_local = $this->post("id_local", TRUE);
        $puntos = $this->post("puntos", TRUE);
        $monto_adicional = $this->post("monto_adicional", TRUE);
        $tipo_pago = $this->post("tipo_pago", TRUE);
        $puntos_totales_usuario = $usuario->punto_acumulado;

        $parametros_canje = array(
            "id_usuario" => $id_usuario, "id_beneficio" => $id_beneficio, "id_local" => $id_local, "puntos" => $puntos, "puntos_acumulado" => $puntos_totales_usuario, "tipo_pago" => $tipo_pago, "monto" => $monto_adicional, "fecha_canje" => $fecha_actual, "fecha_registro" => $fecha_actual, "estado" => ESTADO_ACTIVO

        );

        /* if ($monto_adicional != "" || $monto_adicional != null){

             $monto = array(  "monto" => $monto_adicional);
             $parametros_canje =  array_merge($parametros_canje,$monto);
         }*/

        $id_canje = $this->canje_model->insert($parametros_canje, TRUE);


        $parametros_actualizar_cliente = array(
            "fecha_modificacion" => $fecha_actual, "punto_acumulado" => $usuario->punto_acumulado - $puntos
        );
        $this->usuario_model->update($usuario->id, $parametros_actualizar_cliente);
        return $id_canje;
    }

    public function guardar_descuento()
    {
        $fecha_actual = date("Y-m-d H:i:s");


        $id_usuario = $this->post("id_usuario", TRUE);
        $id_local = $this->post("id_local", TRUE);
        $id_promocion = $this->post("id_promocion", TRUE);
        $tipo_pago = $this->post("tipo_pago", TRUE);
        $monto = $this->post("monto", TRUE);

        $parametros_descuento = array(
            "id_usuario" => $id_usuario, "id_promocion" => $id_promocion, "id_local" => $id_local, "monto" => $monto, "tipo_pago" => $tipo_pago, "fecha_registro" => $fecha_actual, "estado" => ESTADO_ACTIVO
        );

        $id_promocion_canje = $this->promocion_canje_model->insert($parametros_descuento);

        return $id_promocion_canje;
    }

    public function buscar_usuario_token($token)
    {
        $parametros_busqueda_usuario = array(
            "select" => "contacto.perfil", "join" => array(
                "tbl_contacto contacto, contacto.id = tbl_token_dashboard.id_contacto"
            ), "where" => "tbl_token_dashboard.token = '$token'
            and tbl_token_dashboard.estado = " . ESTADO_ACTIVO . "
            and contacto.id = tbl_token_dashboard.id_contacto"
        );

        $empresa_encontrada = $this->token_dashboard_model->get_search_row($parametros_busqueda_usuario);
        return $empresa_encontrada;
    }

    public function validar_perfil($token)
    {
        $perfil_asignado = FALSE;
        $perfil_usuario = $this->buscar_usuario_token($token);

        $perfil = json_decode($perfil_usuario->perfil, true);

        foreach ($perfil as $arreglo) {
            if (isset($arreglo["clientes"])) {
                if ($arreglo["clientes"] == "si") {
                    $perfil_asignado = TRUE;
                }
            }
        }
        return $perfil_asignado;
    }

    public function obtener_lista_clientes_notificacion($id_empresa){
        $parametros_listado_usuario = array(
            "select" => "tbl_usuario.id as id,
            persona.nombre as nombre,
            persona.apellido as apellido,
            persona.dni as dni,
            tbl_usuario.correo as correo",

            "join" => array(
                "tbl_persona persona, persona.id = tbl_usuario.id_persona"
            ),
            "where" => "tbl_usuario.estado = " . ESTADO_ACTIVO . "
            and tbl_usuario.id_empresa = $id_empresa
            and persona.estado = " . ESTADO_ACTIVO .""
        );

        $usuarios = $this->usuario_model->search_array($parametros_listado_usuario);

        return $usuarios;

    }

    public function actualizar_tarjeta($id_usuario,$tarjeta)
    {
        $fecha_actual = date("Y-m-d H:i:s");

        $parametros_actualizar_tarjeta= array(
            "fecha_modificacion" => $fecha_actual, "tarjeta" => $tarjeta
        );

        $usuario = $this->usuario_model->update($id_usuario, $parametros_actualizar_tarjeta);

        return $usuario;
    }

    public function buscar_tarjeta_cliente($id_usuario,$tarjeta)
    {

        $parametros_busqueda_tarjeta = array(
            "select" => "tbl_usuario.tarjeta",
            "where" => "tbl_usuario.id != '$id_usuario'
            and tbl_usuario.tarjeta = '$tarjeta'"
        );

        $tarjeta_encontrada = $this->usuario_model->get_search_row($parametros_busqueda_tarjeta);

        return $tarjeta_encontrada;

    }

    public function buscar_tarjeta_dni_cliente($id_usuario)
    {

        $parametros_busqueda_tarjeta = array(
            "select" => "tbl_usuario.tarjeta",
            "where" => "tbl_usuario.id = '$id_usuario'
            and tbl_usuario.tarjeta != ''"
        );

        $tarjeta_encontrada = $this->usuario_model->get_search_row($parametros_busqueda_tarjeta);

        return $tarjeta_encontrada;

    }





}