<?php
/*
 * Created by Luis Alberto Rosas Arce
 */

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
require APPPATH . "/libraries/Uuid.php";

class Promocion extends REST_Controller
{
    public $id_empresa;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('utils_helper');
        $this->load->model("promocion_model", "promocion_model");
        $this->load->model("token_dashboard_model", "token_dashboard_model");
        $this->lang->load('dashboard_error_lang', 'spanish');
        $this->lang->load('dashboard_mensajes_lang', 'spanish');
        $this->id_empresa = $this->config->item('empresa');
    }

    public function obtener_promociones_get($paginacion = "")
    {
        $token_recibido = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_LISTAR_PROMOCION;
        $nombre_metodo = NOMBRE_METODO_GET;
        $operacion = "listar promociones";

        $parametros = array(
            "token" => $token_recibido,
            "paginacion" => $paginacion
        );

        $id_empresa = $this->id_empresa;
        $usuario ="";
        if (isset($token_recibido) && $token_recibido != "") {

            $objeto_token = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);

            if ($objeto_token != "" && isset($objeto_token)) {
                $usuario = $objeto_token->usuario;
                $datos_promocion = $this->obtener_listado_promociones($id_empresa, $paginacion);
                if (isset($datos_promocion)) {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_OK,
                        "mensaje" => $this->lang->line('mensaje_operacion_exito'),
                        "total_promocion" => $datos_promocion["total_promocion"],
                        "promociones" => $datos_promocion["promociones"]
                    );
                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_obtener_promociones')
                    );
                }

            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }


        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token_recibido, $usuario, $operacion);
        $this->response($resultado);
    }

    public function filtrar_promocion_post($paginacion = "")
    {
        $token_recibido = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_FILTRAR_PROMOCION;
        $nombre_metodo = NOMBRE_METODO_GET;
        $operacion = "filtrar promocion";

        $nombre = $this->post("nombre", TRUE);
        $fecha_inicio = $this->post("fecha_inicio", TRUE);
        $fecha_fin = $this->post("fecha_fin", TRUE);
        $estado = $this->post("estado", TRUE);

        $parametros = array(
            "token" => $token_recibido,
            "paginacion" => $paginacion,
            "nombre" => $nombre,
            "fecha_inicio" => $fecha_inicio,
            "fecha_fin" => $fecha_fin,
            "estado" => $estado
        );


        $id_empresa = $this->id_empresa;
        $usuario = "";

        if (isset($token_recibido) && $token_recibido != "") {
            $objeto_token = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);
            if ($objeto_token != "" && isset($objeto_token)) {
                $usuario = $objeto_token->usuario;
                $datos_promocion = $this->obtener_filtro_promociones($id_empresa, $paginacion);
                if (isset($datos_promocion)) {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_OK,
                        "mensaje" => $this->lang->line('mensaje_operacion_exito'),
                        "total_promocion" => $datos_promocion["total_promocion"],
                        "promociones" => $datos_promocion["promociones"]
                    );
                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_obtener_promociones')
                    );
                }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }


        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token_recibido, $usuario, $operacion);

        $this->response($resultado);
    }

    public function detalle_get($id_promocion)
    {
        $token_recibido = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_DETALLE_PROMOCION;
        $nombre_metodo = NOMBRE_METODO_GET;
        $operacion = "detalle de promoción";

        $parametros = array(
            "token" => $token_recibido,
            "id_promocion" => $id_promocion
        );
        $usuario ="";
        if (isset($token_recibido) && $token_recibido != "") {
            $objeto_token = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);
            if ($objeto_token != "" && isset($objeto_token)) {
                $usuario = $objeto_token->usuario;
                if (isset($id_promocion) && $id_promocion != "") {
                    $promocion_detalle = $this->detalle_promocion_promocion($id_promocion);
                    if (isset($promocion_detalle)) {
                        $detalle_informacion_adicional = $this->detalle_promocion_informacion_adicional($id_promocion);
                        if (isset($detalle_informacion_adicional)) {
                            $foto_detalle = $this->detalle_promocion_foto_detalle($id_promocion);
                            if (isset($foto_detalle)) {
                                $locales = $this->detalle_promocion_local($id_promocion);

                                if (isset($locales)) {
                                    $resultado = array(
                                        "resultado" => ESTADO_RESPUESTA_OK,
                                        "mensaje" => $this->lang->line('mensaje_operacion_exito'),
                                        "promocion" => $promocion_detalle,
                                        "informacion_adicional" => $detalle_informacion_adicional,
                                        "foto_detalle" => $foto_detalle,
                                        "locales" => $locales
                                    );
                                } else {
                                    $resultado = array(
                                        "resultado" => ESTADO_RESPUESTA_ERROR,
                                        "mensaje" => $this->lang->line('error_obtener_detalle_locales')
                                    );
                                }
                            } else {
                                $resultado = array(
                                    "resultado" => ESTADO_RESPUESTA_ERROR,
                                    "mensaje" => $this->lang->line('error_obtener_detalle_foto')
                                );
                            }

                        } else {
                            $resultado = array(
                                "resultado" => ESTADO_RESPUESTA_ERROR,
                                "mensaje" => $this->lang->line('error_obtener_detalle_informacion_adicional')
                            );
                        }
                    } else {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR,
                            "mensaje" => $this->lang->line('error_obtener_detalle_promocion')
                        );
                    }
                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_parametro_url')
                    );
                }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token_recibido, $usuario, $operacion);
        $this->response($resultado);
    }

    public function eliminar_delete($id_promocion)
    {
        $token_recibido = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_ELIMINAR_PROMOCION;
        $nombre_metodo = NOMBRE_METODO_DELETE;
        $operacion = "eliminar promocion";

        $parametros = array(
            "token" => $token_recibido,
            "id_promocion" => $id_promocion
        );

        $usuario = "";
        if (isset($token_recibido) && $token_recibido != "") {
            $objeto_token = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);
            if ($objeto_token != "" && isset($objeto_token)) {
                $usuario = $objeto_token->usuario;
                if (isset($id_promocion) && $id_promocion != "") {

                    $datos_actualizados = $this->eliminar_promocion($id_promocion);

                    if ($datos_actualizados) {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_OK,
                            "mensaje" => $this->lang->line('mensaje_operacion_exito')
                        );
                    } else {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR,
                            "mensaje" => $this->lang->line('error_eliminar_promocion')
                        );
                    }

                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_parametro_url')
                    );
                }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token_recibido, $usuario, $operacion);

        $this->response($resultado);
    }

    public function obtener_promociones_notificacion_get()
    {
        $token = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_LISTAR_PROMOCION_NOTIFICACION;
        $nombre_metodo = NOMBRE_METODO_GET;
        $operacion = "Obtener promociones notificacion";


        $parametros = array(
            "token" => $token
        );
        $id_empresa = $this->id_empresa;
        $datos_promociones = $this->obtener_lista_promociones_notificacion($id_empresa);

        $objeto_token = validar_token_seguridad_dashboard($token,$id_empresa);
        $usuario = $objeto_token->usuario;

        if ($objeto_token!="" && isset($objeto_token)) {
            if (isset($datos_promociones))
            {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_OK,
                    "mensaje" => $this->lang->line('mensaje_operacion_exito'),
                    "promociones" => $datos_promociones
                );
            } else
            {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_obtener_promociones')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_valido')
            );
        }
        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo,$token, $usuario, $operacion);
        $this->response($resultado);
    }
    
    public function buscar_usuario_token($token)
    {
        $parametros_busqueda_usuario = array(
            "select" => "contacto.perfil",
            "join" => array(
                "tbl_contacto contacto, contacto.id = tbl_token_dashboard.id_contacto"
            ),
            "where" => "tbl_token_dashboard.token = '$token'
            and tbl_token_dashboard.estado = " . ESTADO_ACTIVO . "
            and contacto.id = tbl_token_dashboard.id_contacto"
        );

        $empresa_encontrada = $this->token_dashboard_model->get_search_row($parametros_busqueda_usuario);
        return $empresa_encontrada;
    }

    public function validar_perfil($token)
    {
        $perfil_asignado = FALSE;
        $perfil_usuario = $this->buscar_usuario_token($token);

        $perfil = json_decode($perfil_usuario->perfil, true);

        foreach ($perfil as $arreglo) {
            if (isset($arreglo["consultas"])) {
                if ($arreglo["consultas"] == "si") {
                    $perfil_asignado = TRUE;
                } else {
                    if (is_array($arreglo["consultas"])) {
                        $arreglo_temporal = $arreglo["consultas"];
                        $credencial = array_search("promociones", $arreglo_temporal);
                        if ($credencial !== FALSE) {
                            $perfil_asignado = TRUE;
                        }
                    }
                }
            }
        }
        return $perfil_asignado;
    }

    public function buscar_empresa_token($token)
    {
        $parametros_busqueda_empresa = array(
            "select" => "empresa.*",
            "join" => array(
                "tbl_contacto contacto, contacto.id = tbl_token_dashboard.id_contacto",
                "tbl_empresa empresa, empresa.id = contacto.id_empresa"
            ),
            "where" => "tbl_token_dashboard.token = '$token'
            and tbl_token_dashboard.estado = " . ESTADO_ACTIVO . "
            and contacto.estado = " . ESTADO_ACTIVO . "
            and empresa.estado = " . ESTADO_ACTIVO
        );

        $empresa_encontrada = $this->token_dashboard_model->get_search_row($parametros_busqueda_empresa);
        return $empresa_encontrada;
    }

    public function obtener_listado_promociones($id_empresa, $paginacion = "")
    {
        $fecha_actual = date("Y-m-d H:i:s");
        $parametros_listado_promociones = array(
            "select" => "tbl_promocion.id as id,
            tbl_promocion.nombre as nombre,
            tbl_promocion.fecha_inicio as fecha_inicio,
            tbl_promocion.fecha_fin as fecha_fin,
            tbl_promocion.estado",
            "where" => "tbl_promocion.estado <" . ESTADO_ELIMINADO . "
            and tbl_promocion.id_empresa = $id_empresa"
        );
        if (is_numeric($paginacion) && $paginacion > 0) {
            $inicio = ELEMENTOS_POR_PAGINA * ($paginacion - 1);
            $promociones = $this->promocion_model->search_data_array($parametros_listado_promociones, $inicio, ELEMENTOS_POR_PAGINA);
        } else {
            $promociones = $this->promocion_model->search_array($parametros_listado_promociones);
        }
        $total_promocion = $this->promocion_model->total_records($parametros_listado_promociones);
        $datos_promocion = array(
            "total_promocion" => $total_promocion,
            "promociones" => $promociones
        );
        return $datos_promocion;
    }

    public function obtener_filtro_promociones($id_empresa, $paginacion = "")
    {
        $nombre = $this->post("nombre", TRUE);
        $fecha_inicio = $this->post("fecha_inicio", TRUE);
        $fecha_fin = $this->post("fecha_fin", TRUE);
        $estado = $this->post("estado", TRUE);

        $filtro_nombre = "";
        $filtro_fecha_inicio = "";
        $filtro_fecha_fin = "";
        $filtro_estado = "";

        if ($nombre != "") {
            $filtro_nombre = " and tbl_promocion.nombre like '%$nombre%'";
        }
        if ($fecha_inicio != "") {
            $filtro_fecha_inicio = " and tbl_promocion.fecha_registro >= '$fecha_inicio'";
        }
        if ($fecha_fin != "") {
            $filtro_fecha_fin = " and tbl_promocion.fecha_registro <= '$fecha_fin'";
        }
        if ($estado != "" && is_numeric($estado)) {
            $filtro_estado = " and tbl_promocion.estado = $estado";
        }
        $parametros_listado_promociones = array(
            "select" => "tbl_promocion.id as id,
            tbl_promocion.nombre as nombre,
            tbl_promocion.fecha_inicio as fecha_inicio,
            tbl_promocion.fecha_fin as fecha_fin,
            tbl_promocion.estado",
            "where" => "tbl_promocion.estado <" . ESTADO_ELIMINADO . "
            and tbl_promocion.id_empresa = $id_empresa" . $filtro_nombre . $filtro_fecha_inicio . $filtro_fecha_fin . $filtro_estado
        );
        if (is_numeric($paginacion) && $paginacion > 0) {
            $inicio = ELEMENTOS_POR_PAGINA * ($paginacion - 1);
            $promociones = $this->promocion_model->search_data_array($parametros_listado_promociones, $inicio, ELEMENTOS_POR_PAGINA);
        } else {
            $promociones = $this->promocion_model->search_array($parametros_listado_promociones);
        }

        $total_promocion = $this->promocion_model->total_records($parametros_listado_promociones);
        $datos_promocion = array(
            "total_promocion" => $total_promocion,
            "promociones" => $promociones
        );
        return $datos_promocion;
    }

    public function detalle_promocion_promocion($id_promocion)
    {
        $tiempo_actual = date("Y-m-d H:i:s");
        $parametro_detalle_promocion = array(
            "select" => " tbl_promocion.id as id,
            tbl_promocion.nombre as nombre,
            tbl_promocion.descripcion as descripcion,
            tbl_promocion.fecha_inicio as fecha_inicio,
            tbl_promocion.fecha_fin as fecha_fin,
            tbl_promocion.restriccion as restricciones,
            tbl_promocion.compartir_redes as compartir_redes",
            "where" => "tbl_promocion.id = $id_promocion
            and tbl_promocion.id_empresa = $this->id_empresa"
        );

        $promocion = $this->promocion_model->get_search_row($parametro_detalle_promocion);
        return $promocion;
    }

    public function detalle_promocion_informacion_adicional($id_promocion)
    {
        $parametro_detalle_informacion_adicional = array(
            "select" => " info_adicional.campo as campo,
            info_adicional.valor as valor,
            info_adicional.orden",
            "join" => array(
                "tbl_informacion_adicional_promocion info_adicional, info_adicional.id_promocion = tbl_promocion.id",
            ),

            "where" => " tbl_promocion.id = $id_promocion
            and tbl_promocion.estado = " . ESTADO_ACTIVO . "
            and info_adicional.estado = " . ESTADO_ACTIVO,
            "order" => "info_adicional.orden asc"

        );
        $informacion_adicional = $this->promocion_model->search_array($parametro_detalle_informacion_adicional);
        if (!isset($informacion_adicional)) {
            $informacion_adicional = (object)$informacion_adicional;
        }
        return $informacion_adicional;
    }

    public function detalle_promocion_foto($id_promocion)
    {
        $parametro_detalle_foto = array(
            "select" => "foto.id as id,
            foto.dimension as dimension,
            foto.foto as foto",
            "join" => array(
                "tbl_foto_promocion foto, foto.id_promocion = tbl_promocion.id"
            ),
            "where" => "tbl_promocion.estado = " . ESTADO_ACTIVO . "
            and tbl_promocion.id =  $id_promocion
            and foto.tipo = 1
            and foto.estado = " . ESTADO_ACTIVO
        );

        $foto = $this->promocion_model->get_search_row($parametro_detalle_foto);
        if (!isset($foto)) {
            $foto = (object)$foto;
        }
        return $foto;
    }

    public function detalle_promocion_foto_detalle($id_promocion)
    {

        $parametro_detalle_foto = array(
            "select" => "foto.id as id,
            foto.dimension as dimension,
            foto.foto as foto",
            "join" => array(
                "tbl_foto_promocion foto, foto.id_promocion = tbl_promocion.id "
            ),

            "where" => "tbl_promocion.estado = " . ESTADO_ACTIVO . "
            and tbl_promocion.id =  $id_promocion
            and foto.tipo = " . FOTO_DETALLE . "
            and foto.estado = " . ESTADO_ACTIVO

        );

        $foto = $this->promocion_model->get_search_row($parametro_detalle_foto);
        if (!isset($foto)) {
            $foto = (object)$foto;
        }
        return $foto;
    }

    public function detalle_promocion_local($id_promocion)
    {
        $parametros_detalle_local = array(
            "select" => "local.id as id,
            local.nombre as nombre,
            local.direccion as direccion,
            local.latitud as latitud,
            local.longitud as longitud,
            local.correo as correo,
            local.telefono as telefono",
            "join" => array(
                "tbl_local_promocion local_promocion, local_promocion.id_promocion = tbl_promocion.id",
                "tbl_local local, local.id = local_promocion.id_local",
                "tbl_empresa emp, emp.id = local.id_empresa"
            ),
            "where" => "tbl_promocion.id = $id_promocion
            and local_promocion.estado = " . ESTADO_ACTIVO . "
            and local.estado = " . ESTADO_ACTIVO . "
            and emp.estado = " . ESTADO_ACTIVO . "",
            "group" => "local.id"
        );
        $locales = $this->promocion_model->search_array($parametros_detalle_local);
        return $locales;

    }

    public function eliminar_promocion($id_promocion)
    {
        $fecha_actual = date("Y-m-d H:i:s");
        $parametros_promocion = array(
            "estado" => ESTADO_ELIMINADO,
            "fecha_modificacion" => $fecha_actual
        );

        $filas_afectadas = $this->promocion_model->update($id_promocion, $parametros_promocion);

        return $filas_afectadas;

    }

    public function eliminar_test_get()
    {
        $id_promocion = "1";

        $filas_afectadas = $this->eliminar_promocion($id_promocion);

        var_dump($filas_afectadas);

    }

    public function validar_perfil_test_get()
    {
        $token_recibido = $this->getHeaderRC('token');
        $perfil_asignado = FALSE;
        $perfil_usuario = $this->buscar_usuario_token($token_recibido);

        $perfil = json_decode($perfil_usuario->perfil, true);

        foreach ($perfil as $arreglo) {
            if (isset($arreglo["consultas"])) {
                if ($arreglo["consultas"] == "si") {
                    $perfil_asignado = TRUE;
                } else {
                    if (is_array($arreglo["consultas"])) {
                        $arreglo_temporal = $arreglo["consultas"];
                        $credencial = array_search("promociones", $arreglo_temporal);

                        if (isset($credencial)) {
                            $perfil_asignado = TRUE;
                        }
                    }
                }
            }
        }
        return $perfil_asignado;
    }

    public function obtener_lista_promociones_notificacion($id_empresa){
        $fecha_actual = date("Y-m-d H:i:s");

        $parametros_listado_promociones = array(
            "select" => "tbl_promocion.id as id,
                tbl_promocion.nombre as nombre,
                tbl_promocion.estado as estado",

            "where" => "tbl_promocion.estado < " . ESTADO_ELIMINADO . "
                and tbl_promocion.id_empresa = $id_empresa
                and tbl_promocion.fecha_fin > '$fecha_actual'"
        );


        $promociones = $this->promocion_model->search_array($parametros_listado_promociones);

        return $promociones;
    }


}