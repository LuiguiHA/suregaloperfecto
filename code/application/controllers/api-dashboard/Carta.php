<?php
/**
 * Created by PhpStorm.
 * User: Mediabyte
 * Date: 06/05/2016
 * Time: 03:33 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
require APPPATH . "/libraries/Uuid.php";

class Carta extends REST_Controller
{
    public $id_empresa;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('utils_helper');
        $this->load->model("carta_model", "carta_model");
        $this->load->model("categoria_carta_model", "categoria_carta_model");
        $this->load->model("token_dashboard_model", "token_dashboard_model");
        $this->lang->load('dashboard_error_lang', 'spanish');
        $this->lang->load('dashboard_mensajes_lang', 'spanish');
        $this->id_empresa = $this->config->item('empresa');
    }

    public function obtener_cartas_get($paginacion = "1")
    {

        $accion_realizada = ACCION_DASHBOARD_CARTA_OBTENER;
        $nombre_metodo = NOMBRE_METODO_GET;
        $token = $this->getHeaderRC('token');
        $usuario  = "";
        $operacion = "obtener cartas";

        $parametros = array(
            "token" => $token,
            "paginacion" => $paginacion
        );

        $objeto_token = validar_token_seguridad_dashboard($token,$this->id_empresa);

        if ($objeto_token!="" && isset($objeto_token)) {
           $usuario = $objeto_token->usuario;
           $categoria = $this->listar_categorias();

            if (isset($categoria))
            {
                $cartas = $this->listar_cartas($paginacion);
                if (isset($cartas))
                {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_OK,
                        "mensaje" => $this->lang->line('carta_listar'),
                        "categorias" => $categoria,
                        "total_cartas" => $cartas["total_cartas"],
                        "cartas" => $cartas["cartas"]
                    );
                } else
                {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_carta_obtener')
                    );
                }
            } else
            {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_carta_obtener_categoria')
                );
            }
        } else{
             $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_valido')
            );
        }

                

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo,$token, $usuario,$operacion);

        $this->response($resultado);
    }
    
    public function detalle_get($id_carta)
    {

        $accion_realizada = ACCION_DASHBOARD_CARTA_DETALLE;
        $nombre_metodo = NOMBRE_METODO_GET;
        $operacion = "detalle carta";
        $token = $this->getHeaderRC('token');
        $usuario="";
        $parametros = array(
            "token" => $token,
            "id_carta" => $id_carta
        );
        
        if (isset($id_carta) && $id_carta != "")
        {
            $objeto_token = validar_token_seguridad_dashboard($token,$this->id_empresa);

            if ($objeto_token!="" && isset($objeto_token)) {
                $usuario = $objeto_token->usuario;
                $carta = $this->detalle_carta_carta($id_carta);

                if (isset($carta))
                {
                    $foto = $this->detalle_carta_foto($id_carta);

                    if (isset($foto))
                    {
                        $empresa = $this->detalle_carta_empresa($id_carta);

                        if (isset($empresa))
                        {
                            $locales = $this->detalle_carta_locales($id_carta);

                            if (isset($locales))
                            {

                                $resultado = array(
                                    "resultado" => ESTADO_RESPUESTA_OK,
                                    "mensaje" => $this->lang->line('carta_detalle'),
                                    "carta" => $carta,
                                    "foto" => $foto,
                                    "empresa" => $empresa,
                                    "locales" => $locales
                                );
                            } else
                            {
                                $resultado = array(
                                    "resultado" => ESTADO_RESPUESTA_ERROR,
                                    "mensaje" => $this->lang->line('error_carta_detalle_local')
                                );
                            }
                        } else
                        {
                            $resultado = array(
                                "resultado" => ESTADO_RESPUESTA_ERROR,
                                "mensaje" => $this->lang->line('error_carta_detalle_empresa')
                            );
                        }

                    } else
                    {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR,
                            "mensaje" => $this->lang->line('error_carta_detalle_foto')
                        );
                    }
                } else
                {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_carta_detalle')
                    );
                }

            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_OK,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }


        } else
        {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_carta_detalle_parametro')
            );
        }

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token, $usuario, $operacion);
        $this->response($resultado);
    }

    public function buscar_post($paginacion = "1")
    {

        $accion_realizada = ACCION_DASHBOARD_CARTA_BUSCAR;
        $nombre_metodo = NOMBRE_METODO_POST;
        $token = $this->getHeaderRC('token');
        $usuario = "";
        $operacion = "buscar carta";

        $carta = $this->post("nombre", TRUE);
        $categoria = $this->post("id_categoria", TRUE);
        $estado = $this->post("estado", TRUE);

        $parametros = array(
            "token" => $token,
            "nombre" => $carta,
            "id_categoria" => $categoria,
            "estado" => $estado,
            "paginacion" => $paginacion
        );


        $objeto_token = validar_token_seguridad_dashboard($token, $this->id_empresa);

        if (is_numeric($categoria)) {
            if ($objeto_token!="" && isset($objeto_token)) 
            {
                $usuario = $objeto_token->usuario;
                $categorias = $this->listar_categorias();

                if (isset($categorias))
                {
                    $cartas = $this->buscar($paginacion);
                    if (isset($cartas))
                    {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_OK,
                            "mensaje" => $this->lang->line('carta_busqueda'),
                            "categorias" => $categorias,
                            "total_cartas" => $cartas["total_cartas"],
                            "cartas" => $cartas["cartas"]
                        );
                    } else
                    {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR,
                            "mensaje" => $this->lang->line('error_carta_busqueda')
                        );
                    }
                } else
                {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_carta_obtener_categoria')
                    );
                }
            } else
            {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_OK,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else
        {
            $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_OK,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
        }
        
                

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token, $usuario, $operacion);
        $this->response($resultado);
    }

    public function obtener_cartas_notificacion_post()
    {
        $token = $this->getHeaderRC('token');
        $id_local = $this->post('id_local', TRUE);
        $accion_realizada = ACCION_DASHBOARD_CARTA_OBTENER_LISTA_NOTIFICACION;
        $nombre_metodo = NOMBRE_METODO_POST;
        $operacion = "Obtener cartas notificacion";


        $parametros = array(
            "token" => $token,
            "id_local"=>$id_local
        );
        $id_empresa = $this->id_empresa;
        $datos_cartas = $this->obtener_lista_cartas_notificacion($id_local);

        $objeto_token = validar_token_seguridad_dashboard($token,$id_empresa);
        $usuario = $objeto_token->usuario;

        if ($objeto_token!="" && isset($objeto_token)) {
            if (isset($datos_cartas))
            {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_OK,
                    "mensaje" => $this->lang->line('mensaje_operacion_exito'),
                    "cartas" => $datos_cartas
                );
            } else
            {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_carta_obtener')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_valido')
            );
        }
        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo,$token, $usuario, $operacion);
        $this->response($resultado);
    }


    public function listar_categorias()
    {
        $parametros_listado_categorias = array(
            "select" => "id, nombre, foto",
            "where" => "tbl_categoria_carta.estado = " . ESTADO_ACTIVO
        );
        $categorias = $this->categoria_carta_model->search_array($parametros_listado_categorias);
        return $categorias;
    }

    public function listar_cartas($paginacion)
    {
        $parametros_listado_cartas = array(
            "select" => "tbl_carta.id as id,
            tbl_carta.nombre as nombre,
            cat.nombre as categoria,
            tbl_carta.precio as precio,
            tbl_carta.descripcion as descripcion,
            ifnull(foto.foto,'') as foto,
            ifnull(foto.dimension,'') as foto_dimension,
            tbl_carta.estado as estado",
            "join" => array(
                "tbl_categoria_carta cat, cat.id = tbl_carta.id_categoria_carta"

            ),
            "left" => array(
                "tbl_foto_carta foto, foto.id_carta = tbl_carta.id  and foto.tipo= 1 ,left"
            ),
            "where" => "cat.estado = " . ESTADO_ACTIVO
        );

        if (is_numeric($paginacion) && $paginacion > 0)
        {
            $inicio = ELEMENTOS_POR_PAGINA * ($paginacion - 1);

            $cartas = $this->carta_model->search_data_array($parametros_listado_cartas, $inicio, ELEMENTOS_POR_PAGINA);
        } else
        {
            $cartas = $this->carta_model->search_array($parametros_listado_cartas);
        }
        $total_cartas = $this->carta_model->total_records($parametros_listado_cartas);
        $datos_cartas = array(
            "total_cartas" => $total_cartas,
            "cartas" => $cartas
        );

        return $datos_cartas;
    }

    public function listar_cartas_categoria($categoria, $paginacion = "")
    {
        $parametros_listado_cartas_categoria = array(
            "select" => "tbl_carta.id as id,
            tbl_carta.nombre as nombre,
            tbl_carta.id_categoria_carta as id_categoria,
            cat.nombre as nombre_categoria,
            tbl_carta.precio as precio,
            tbl_carta.descripcion as descripcion,
            foto.foto as foto,
            foto.dimension as foto_dimension",
            "join" => array(
                "tbl_categoria_carta cat, cat.id = tbl_carta.id_categoria_carta",
                "tbl_foto_carta foto, foto.id_carta = tbl_carta.id"
            ),
            "where" => "tbl_carta.estado = 1
            and cat.estado = 1
            and foto.estado = 1
            and foto.tipo = 1
            and cat.id = $categoria"
        );

        if (is_numeric($paginacion) && $paginacion > 1)
        {
            $inicio = PAGINACION * ($paginacion - 1);

            $hasta = $paginacion * PAGINACION;

            $cartas = $this->carta_model->search_data_array($parametros_listado_cartas_categoria, $inicio, $hasta);
        } else
        {
            $cartas = $this->carta_model->search_array($parametros_listado_cartas_categoria);
        }

        return $cartas;
    }

    public function detalle_carta_carta($id_carta)
    {
        $parametro_detalle_carta = array(
            "select" => "tbl_carta.id as id,
            tbl_carta.nombre as nombre,
            tbl_carta.id_categoria_carta as id_categoria,
            cat.nombre as nombre_categoria,
            tbl_carta.precio as precio,
            tbl_carta.descripcion as descripcion",
            "join" => array(
                "tbl_categoria_carta cat, cat.id = tbl_carta.id_categoria_carta",
            ),
            "where" => "cat.estado = 1
            and tbl_carta.id = $id_carta"
        );


        $carta = $this->carta_model->get_search_row($parametro_detalle_carta);

        return $carta;
    }

    public function detalle_carta_foto($id_carta)
    {
        $parametro_detalle_foto = array(
            "select" => "foto.foto as nombre,
            foto.dimension as dimension",
            "join" => array(
                "tbl_foto_carta foto, foto.id_carta = tbl_carta.id"
            ),
            "where" => "tbl_carta.estado = 1
            and foto.estado = 1
            and foto.tipo = 2
            and tbl_carta.id = $id_carta"
        );


        $foto = $this->carta_model->get_search_row($parametro_detalle_foto);
        if (!isset($foto))
        {
            $foto = (object)$foto;
        }
        return $foto;

    }

    public function detalle_carta_empresa($id_carta)
    {
        $parametro_detalle_empresa = array(
            "select" => "emp.nombre as nombre",
            "join" => array(
                "tbl_categoria_carta categoria, categoria.id = tbl_carta.id_categoria_carta",
                "tbl_empresa emp, emp.id = categoria.id_empresa"
            ),
            "where" => "categoria.estado = " . ESTADO_ACTIVO . "
            and emp.estado = " . ESTADO_ACTIVO . "
            and emp.id = $this->id_empresa
            and tbl_carta.id = $id_carta"
        );
        $empresa = $this->carta_model->get_search_row($parametro_detalle_empresa);
        return $empresa;
    }

    public function detalle_carta_locales($id_carta)
    {
        $parametro_detalle_local = array(
            "select" => "local.id as id,
            local.nombre as nombre,
            local.direccion as direccion,
            local.latitud as latitud,
            local.longitud as longitud,
            local.correo as correo,
            local.telefono as telefono",

            "join" => array(
                "tbl_local local, local.id = tbl_carta.id_local"
            ),
            "where" => "local.estado = " . ESTADO_ACTIVO .
                " and tbl_carta.estado = " . ESTADO_ACTIVO .
                " and tbl_carta.id = $id_carta"
        );
        $locales = $this->carta_model->search_array($parametro_detalle_local);

        return $locales;

    }

    public function buscar($paginacion)
    {
        $nombre = $this->post("nombre", TRUE);
        $categoria = $this->post("id_categoria", TRUE);
        $estado = $this->post("estado", TRUE);


        $filtro_nombre = "";
        $filtro_categoria = "";
        $filtro_estado = "";
        if ($nombre != "")
        {
            $filtro_nombre = " and (tbl_carta.descripcion like '%$nombre%'
            or tbl_carta.nombre like '%$nombre%'
            or local.nombre like '%$nombre%' )";
        }

        if ($categoria != "")
        {
            $filtro_categoria = " and categoria.id = $categoria";
        }

        if ($estado != "")
        {
            $filtro_estado = " and tbl_carta.estado = $estado";
        }


        $parametros_busqueda_carta = array(
            "select" => "tbl_carta.id as id,
            tbl_carta.nombre as nombre,
            tbl_carta.id_categoria_carta as id_categoria,
            categoria.nombre as categoria,
            tbl_carta.precio as precio,
            ifnull(tbl_carta.descripcion,'') as descripcion,
            ifnull(foto.foto,'') as foto,
            ifnull(foto.dimension,'') as foto_dimension,
            tbl_carta.estado as estado",
            "join" => array(
                "tbl_categoria_carta categoria, categoria.id = tbl_carta.id_categoria_carta",
                "tbl_local local, local.id = tbl_carta.id_local"
            ),
            "left" => array(
                "tbl_foto_carta foto, foto.id_carta = tbl_carta.id and foto.tipo =1, left",
            ),

            "where" => "categoria.estado = " . ESTADO_ACTIVO .
                " and tbl_carta.estado < " . ESTADO_ELIMINADO .
                " and local.estado = " . ESTADO_ACTIVO . $filtro_nombre . $filtro_estado . $filtro_categoria
        );


        if (is_numeric($paginacion) && $paginacion > 0)
        {
            $inicio = ELEMENTOS_POR_PAGINA * ($paginacion - 1);


            $cartas = $this->carta_model->search_data_array($parametros_busqueda_carta, $inicio, ELEMENTOS_POR_PAGINA);
        } else
        {
            $cartas = $this->carta_model->search_array($parametros_busqueda_carta);
        }
        $total_cartas = $this->carta_model->total_records($parametros_busqueda_carta);
        $datos_cartas = array(
            "total_cartas" => $total_cartas,
            "cartas" => $cartas
        );
        return $datos_cartas;
    }

    public function listar_cartas_notificacion($paginacion)
    {
        $parametros_listado_cartas = array(
            "select" => "tbl_carta.id as id,
            tbl_carta.nombre as nombre,
            cat.nombre as categoria,
            tbl_carta.precio as precio,
            tbl_carta.descripcion as descripcion,
            ifnull(foto.foto,'') as foto,
            ifnull(foto.dimension,'') as foto_dimension,
            tbl_carta.estado as estado",
            "join" => array(
                "tbl_categoria_carta cat, cat.id = tbl_carta.id_categoria_carta"

            ),
            "left" => array(
                "tbl_foto_carta foto, foto.id_carta = tbl_carta.id  and foto.tipo= 1 ,left"
            ),
            "where" => "cat.estado = " . ESTADO_ACTIVO
        );

        if (is_numeric($paginacion) && $paginacion > 0)
        {
            $inicio = ELEMENTOS_POR_PAGINA * ($paginacion - 1);

            $cartas = $this->carta_model->search_data_array($parametros_listado_cartas, $inicio, ELEMENTOS_POR_PAGINA);
        } else
        {
            $cartas = $this->carta_model->search_array($parametros_listado_cartas);
        }
        $total_cartas = $this->carta_model->total_records($parametros_listado_cartas);
        $datos_cartas = array(
            "total_cartas" => $total_cartas,
            "cartas" => $cartas
        );

        return $datos_cartas;
    }

    public function obtener_lista_cartas_notificacion($id_local){

        $parametros_listado_cartas = array(
            "select" => "tbl_carta.id as id,
                tbl_carta.nombre as nombre,
                tbl_carta.estado as estado",

            "where" => "tbl_carta.estado =" . ESTADO_ACTIVO . "
                and tbl_carta.id_local = ".$id_local
        );

        $cartas = $this->carta_model->search_array($parametros_listado_cartas);

        return $cartas;
    }
}