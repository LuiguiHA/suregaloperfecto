<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";

class Sendgrid extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('utils_helper');
        $this->load->model('estado_envio_email_model', 'estado_envio_email_model');
    }

    public function callback_post()
    {
        $arrayEvents = json_decode( file_get_contents("php://input") );

        log_message('debug',"Respuesta SendGrid: \n".print_r($arrayEvents,TRUE));

        if(is_array($arrayEvents))
        {
            foreach ($arrayEvents as $event) {

                if(isset($event->detalle_email_id))
                {
                    $detalle_email_id = "";

                    if(isset($event->detalle_email_id))
                        $detalle_email_id = $event->detalle_email_id;

                    if($detalle_email_id != "")
                    {
                        switch ($event->event) {
                            case EVENT_SENDGRID_TYPE_STATUS_DELETE:
                                $event_value = VALUE_EVENT_SENDGRID_TYPE_STATUS_FAILED;
                                $event_value_code = VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_FAILED;
                                log_message('debug',"Evento: Dropped \n".print_r($event,TRUE));
                                break;
                            case EVENT_SENDGRID_TYPE_STATUS_PROCESSED:
                                $event_value = VALUE_EVENT_SENDGRID_TYPE_STATUS_PROCESSED;
                                $event_value_code = VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_PROCESSED;
                                log_message('debug',"Evento: Processed \n".print_r($event,TRUE));
                                break;
                            case EVENT_SENDGRID_TYPE_STATUS_REBOUND:
                                $event_value = VALUE_EVENT_SENDGRID_TYPE_STATUS_FAILED;
                                $event_value_code = VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_FAILED;
                                log_message('debug',"Evento: Bounce \n".print_r($event,TRUE));
                                break;
                            case EVENT_SENDGRID_TYPE_STATUS_DELIVERED:
                                $event_value = VALUE_EVENT_SENDGRID_TYPE_STATUS_DELIVERED;
                                $event_value_code = VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_DELIVERED;
                                log_message('debug',"Evento: Delivered \n".print_r($event,TRUE));
                                break;
                            case EVENT_SENDGRID_TYPE_STATUS_OPEN:
                                $event_value = VALUE_EVENT_SENDGRID_TYPE_STATUS_OPEN;
                                $event_value_code = VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_OPEN;
                                log_message('debug',"Evento: Open \n".print_r($event,TRUE));
                                break;
                            case EVENT_SENDGRID_TYPE_STATUS_CLICK:
                                $event_value = VALUE_EVENT_SENDGRID_TYPE_STATUS_CLICK;
                                $event_value_code = VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_CLICK;
                                log_message('debug',"Evento: Click \n".print_r($event,TRUE));
                                break;
                            case EVENT_SENDGRID_TYPE_STATUS_UNSUBSCRIBE:
                                $event_value = VALUE_EVENT_SENDGRID_TYPE_STATUS_UNSUBSCRIBE;
                                $event_value_code =VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_UNSUBSCRIBE;
                                log_message('debug',"Evento: Unsubscribe \n".print_r($event,TRUE));
                                break;
                            case EVENT_SENDGRID_TYPE_STATUS_SPAM:
                                $event_value = VALUE_EVENT_SENDGRID_TYPE_STATUS_SPAM;
                                $event_value_code = VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_SPAM;
                                log_message('debug',"Evento: Spam Report \n".print_r($event,TRUE));
                                break;
                            case EVENT_SENDGRID_TYPE_STATUS_DEFERRED:
                                $event_value = VALUE_EVENT_SENDGRID_TYPE_STATUS_FAILED;
                                $event_value_code = VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_FAILED;
                                log_message('debug',"Evento: deferred \n".print_r($event,TRUE));
                                break;
                            default:
                                $event_value = VALUE_EVENT_SENDGRID_TYPE_STATUS_UNIDENTIFIED;
                                $event_value_code = VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_UNIDENTIFIED;
                                log_message('debug',"Evento: Other \n".print_r($event,TRUE));
                                break;
                        }
                            $this->registrar_estado_envio_email($detalle_email_id,$event_value,$event_value_code);
                    }
                }
            }
        }
    }

    public function obtener_estado_envio_email($detalle_email_id){

        $parametros = array(
         'select'=>"id",
         'where'=>"id_detalle_envio_email = ".$detalle_email_id
        );
        $estado_mensaje_email= $this->estado_envio_email_model->get_search_row($parametros);

        return $estado_mensaje_email;
    }


    public function registrar_estado_envio_email($detalle_email_id,$status_email,$status_code_email){
        $tiempo_actual = date('Y-m-d H:i:s');
        $parametros = array(
            'id_detalle_envio_email'=>$detalle_email_id,
            'descripcion'=>$status_email,
            'estado'=>$status_code_email,
            'fecha_registro'=>$tiempo_actual,

        );
        $estado_mensaje_email= $this->estado_envio_email_model->insert($parametros);

        return $estado_mensaje_email;
    }

    public function actualizar_estado_envio_email($detalle_email_id,$status_email,$status_code_email){
        $parametros = array(
            'descripcion'=>$status_email,
            'estado'=>$status_code_email,
        );
        $where = "id_detalle_envio_email = ".$detalle_email_id;
        $estado_mensaje_email= $this->estado_envio_email_model->update_where($where,$parametros);

        return $estado_mensaje_email;
    }

}