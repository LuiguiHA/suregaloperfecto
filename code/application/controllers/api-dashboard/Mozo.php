<?php
/*
 * Created by Luis Alberto Rosas Arce
 */

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
require APPPATH . "/libraries/Uuid.php";

class Mozo extends REST_Controller
{
    public $id_empresa;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('utils_helper');
        $this->load->model("mozo_model", "mozo_model");
        $this->lang->load('dashboard_error_lang', 'spanish');
        $this->lang->load('dashboard_mensajes_lang', 'spanish');
        $this->id_empresa = $this->config->item('empresa');

    }

    public function obtener_mozos_post()
    {
        $token_recibido = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_OBTENER_MOZOS;
        $nombre_metodo = NOMBRE_METODO_POST;
        $operacion = "obtener mozos";

        $id_local = $this->post("id_local", TRUE);

        $parametros = array(
            "token" => $token_recibido,
            "id_local" => $id_local
        );

        $usuario = "";

        if (isset($token_recibido) && $token_recibido != "") {
            $objeto_token = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);
            if ($objeto_token != "" && isset($objeto_token)) {
                $usuario = $objeto_token->usuario;
                if (isset($id_local) && $id_local != "") {
                $mozos = $this->obtener_listado_mozos($id_local);
                if (isset($mozos)) {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_OK,
                        "mensaje" => $this->lang->line('cliente_obtener_mozos'),
                        "mozos" => $mozos,
                    );
                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_obtener_mozos')
                    );
                }
                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_parametro_url')
                    );
                }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }


        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token_recibido, $usuario, $operacion);

        $this->response($resultado);
    }

    /****** METODOS ******/


    public function obtener_listado_mozos($id_local)
    {

        $parametros_listado_mozos = array(
            "select" => "tbl_mozo.id as id,
            tbl_mozo.nombre as nombre,
            tbl_mozo.apellido as apellido,
            tbl_mozo.estado as estado",

            "where" => "tbl_mozo.id_local = " . $id_local . "
           and  tbl_mozo.estado = " . ESTADO_ACTIVO
        );

        $mozos = $this->mozo_model->search_array($parametros_listado_mozos);

        return $mozos;
    }


}