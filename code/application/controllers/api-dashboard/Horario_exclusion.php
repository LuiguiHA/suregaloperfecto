<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
require APPPATH . "/libraries/Uuid.php";

class Horario_exclusion extends REST_Controller
{
    public $id_empresa;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('utils_helper');
        $this->load->model("token_dashboard_model", "token_dashboard_model");
        $this->load->model("usuario_model", "usuario_model");
        $this->load->model("reserva_solicitud_model", "reserva_solicitud_model");
        $this->load->model("tipo_pago_empresa_model", "tipo_pago_empresa_model");
        $this->load->model("horario_reserva_model", "horario_reserva_model");
        $this->load->model("horario_exclusion_model", "horario_exclusion_model");
        $this->load->model("local_model", "local_model");
        $this->load->model("empresa_model", "empresa_model");
        $this->lang->load('dashboard_error_lang', 'spanish');
        $this->lang->load('dashboard_mensajes_lang', 'spanish');
        $this->id_empresa = $this->config->item('empresa');
    }

    public function obtener_horarios_exclusion_post($paginacion = "")
    {
        $token_recibido = $this->getHeaderRC('token');
        $id_local = $this->post("id_local", TRUE);
        $accion_realizada = ACCION_DASHBOARD_OBTENER_HORARIO_EXCLUSION;
        $nombre_metodo = NOMBRE_METODO_POST;
        $operacion = "obtener horarios";


        $parametros = array(
            "token" => $token_recibido,
            'id_local'=>$id_local
        );

        $usuario = "";
        if (isset($token_recibido) && $token_recibido != "") {
            $objeto_token = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);
            if ($objeto_token != "" && isset($objeto_token)) {
                $usuario = $objeto_token->usuario;
                $datos_locales = $this->obtener_listado_locales($this->id_empresa);

                if (isset($datos_locales)) {
                    $datos_horarios = $this->obtener_listado_horarios_exclusion($this->id_empresa, $paginacion,$id_local);

                    if (isset($datos_horarios)) {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_OK,
                            "mensaje" => $this->lang->line('cliente_obtener_horarios'),
                            "total_horarios" => $datos_horarios["total_horarios"],
                            "horarios" => $datos_horarios["horarios"],
                            "locales" => $datos_locales
                        );

                    } else {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR,
                            "mensaje" => $this->lang->line('error_obtener_horarios')
                        );
                    }
                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_obtener_locales')
                    );
                }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }


        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo,$token_recibido, $usuario, $operacion);
        $this->response($resultado);
    }

    public function filtrar_horarios_exclusion_post($paginacion = "")
    {
        $token_recibido = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_BUSCAR_HORARIO_EXCLUSION;
        $nombre_metodo = NOMBRE_METODO_POST;
        $operacion ="filtra horarios";

        $tipo = $this->post("tipo", TRUE);
        $fecha = $this->post("fecha", TRUE);
        $local = $this->post("local", TRUE);
        $estado = $this->post("estado", TRUE);

        $parametros = array(
            "token" => $token_recibido,
            "paginacion" => $paginacion,
            "tipo" => $tipo,
            "fecha" => $fecha,
            "local" => $local,
            "estado" => $estado
        );
        $usuario ="";
        $id_empresa = $this->id_empresa;
        if (isset($token_recibido) && $token_recibido != "") {
            $objeto_token = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);
            if ($objeto_token != "" && isset($objeto_token)) {
                $usuario = $objeto_token->usuario;
                $datos_locales = $this->obtener_listado_locales($id_empresa);

                if (isset($datos_locales)) {
                    $datos_horarios = $this->obtener_buscar_horarios_exclusion($id_empresa, $paginacion);

                    if (isset($datos_horarios)) {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_OK,
                            "mensaje" => $this->lang->line('cliente_obtener_horarios'),
                            "total_horarios" => $datos_horarios["total_horarios"],
                            "horarios" => $datos_horarios["horarios"],
                            "locales" => $datos_locales
                        );
                    } else {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR,
                            "mensaje" => $this->lang->line('error_obtener_horarios')
                        );
                    }
                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_obtener_locales')
                    );
                }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo,$token_recibido, $usuario, $operacion);

        $this->response($resultado);
    }

    public function detalle_horario_exclusion_get($id_horario_exclusion)
    {
        $token_recibido = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_DETALLE_HORARIO_EXCLUSION;
        $nombre_metodo = NOMBRE_METODO_GET;
        $operacion = "detalle de horario";

        $parametros = array(
            "token" => $token_recibido,
            "id_horario_exclusion" => $id_horario_exclusion
        );
        $usuario = "";

        if (isset($token_recibido) && $token_recibido != "") {
            $objeto_token = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);
            if ($objeto_token != "" && isset($objeto_token)) {
                $usuario = $objeto_token->usuario;
                if (isset($id_horario_exclusion) && $id_horario_exclusion != "") {

                    $detalle_horario_exclusion = $this->detalle_horario_exclusion($id_horario_exclusion);

                    if (isset($detalle_horario_exclusion)) {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_OK,
                            "mensaje" => $this->lang->line('mensaje_operacion_exito'),
                            "horario" => $detalle_horario_exclusion

                        );

                    } else {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR,
                            "mensaje" => $this->lang->line('error_obtener_detalle_horario_exclusion')
                        );
                    }

                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_parametro_url')
                    );
                }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo,$token_recibido, $usuario, $operacion);
        $this->response($resultado);
    }

    public function detalle_listado_locales_get()
    {
        $token_recibido = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_DETALLE_LISTADO_LOCALES;
        $nombre_metodo = NOMBRE_METODO_GET;
        $operacion = "detalle listado de locales";

        $parametros = array(
            "token" => $token_recibido
        );

        $usuario ="";
        $id_empresa = $this->id_empresa;
        if (isset($token_recibido) && $token_recibido != "") {
            $objeto_token = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);
            $usuario = $objeto_token->usuario;
            if ($objeto_token != "" && isset($objeto_token)) {
                $locales = $this->obtener_listado_locales($id_empresa);
                if (isset($locales)) {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_OK,
                        "mensaje" => $this->lang->line('mensaje_operacion_exito'),
                        "locales" => $locales

                    );

                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_obtener_locales')
                    );
                }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }


        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token_recibido, $usuario, $operacion);
        $this->response($resultado);
    }

    public function editar_horario_exclusion_post()
    {
        $token_recibido = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_EDITAR_HORARIO_EXCLUSION;
        $nombre_metodo = NOMBRE_METODO_POST;
        $operacion = "editar horario exclusion";

        $id_horario_exclusion = $this->post("id_horario_exclusion", TRUE);
        $fecha = $this->post("fecha", TRUE);
        $tipo = $this->post("tipo", TRUE);
        $horario = $this->post("horario", TRUE);
        $hora_inicio = $this->post("hora_inicio", TRUE);
        $hora_fin = $this->post("hora_fin", TRUE);
        $estado = $this->post("estado", TRUE);

        $parametros = array(
            "token" => $token_recibido,
            "id_horario_exclusion" => $id_horario_exclusion,
            "fecha" => $fecha,
            "tipo" => $tipo,
            "horario" => $horario,
            "hora_inicio" => $hora_inicio,
            "hora_fin" => $hora_fin,
            "estado" => $estado
        );
        $usuario = "";
        if (isset($token_recibido) && $token_recibido != "") {
            $objeto_token = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);
            if ($objeto_token != "" && isset($objeto_token)) {
                $usuario = $objeto_token->usuario;

                if (isset($id_horario_exclusion) && $id_horario_exclusion != ""
                    && isset($fecha) && $fecha != ""
                    && isset($tipo) && $tipo != ""
                    && isset($estado) && $estado != ""
                ) {

                    $horario = $this->editar_horario_exclusion();

                    if (isset($horario)) {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_OK,
                            "mensaje" => $this->lang->line('cliente_editar_horario_exclusion')
                        );
                    } else {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR,
                            "mensaje" => $this->lang->line('error_editar_horario_exclusion')
                        );
                    }

                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_parametros')
                    );
                }

            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token_recibido, $usuario, $operacion);
        $this->response($resultado);
    }

    public function agregar_horarios_exclusion_post()
    {
        $token_recibido = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_AGREGAR_HORARIO_EXCLUSION;
        $nombre_metodo = NOMBRE_METODO_POST;
        $operacion = "agregar horario";

        $id_horario = $this->post("id_horario", TRUE);
        $id_local = $this->post("id_local", TRUE);
        $fecha = $this->post("fecha", TRUE);
        $tipo = $this->post("tipo", TRUE);
        $hora_inicio = $this->post("hora_inicio", TRUE);
        $hora_fin = $this->post("hora_fin", TRUE);
        $estado = $this->post("estado", TRUE);

        $parametros = array(
            "token" => $token_recibido,
            "id_horario" => $id_horario,
            "id_local" => $id_local,
            "fecha" => $fecha,
            "tipo" => $tipo,
            "hora_inicio" => $hora_inicio,
            "hora_fin" => $hora_fin,
            "estado" => $estado
        );
        $usuario = "";
        if (isset($token_recibido) && $token_recibido != "") {
            $objeto_token = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);
            if ($objeto_token != "" && isset($objeto_token)) {
                if (isset($fecha) && $fecha != ""
                    && isset($tipo) && $tipo != ""
                    && isset($estado) && $estado != ""
                    && isset($id_local) && $id_local != ""
                ) {

                    $horario = $this->agregar_horario_exclusion();

                    if (isset($horario)) {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_OK,
                            "mensaje" => $this->lang->line('cliente_agregar_horario_exclusion')
                        );
                    } else {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR,
                            "mensaje" => $this->lang->line('error_agregar_horario_exclusion')
                        );
                    }

                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_parametros')
                    );
                }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token_recibido, $usuario, $operacion);
        $this->response($resultado);
    }

    public function revisar_horarios_exclusion_post()
    {
        $token_recibido = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_REVISAR_HORARIO_EXCLUSION;
        $nombre_metodo = NOMBRE_METODO_POST;
        $operacion = "revisar horario exclusion";

        $id_horario_exclusion = $this->post("id_horario_exclusion", TRUE);
        $id_horario = $this->post("id_horario", TRUE);
        $tipo = $this->post("tipo", TRUE);
        $fecha = $this->post("fecha", TRUE);
        $id_local = $this->post("id_local", TRUE);


        $parametros = array(
            "token" => $token_recibido,
           "id_horario_exclusion" => $id_horario_exclusion,
            "id_horario" => $id_horario,
            "id_local" => $id_local,
            "fecha" => $fecha,
            "tipo" => $tipo,
        );
        $usuario ="";

        if (isset($token_recibido) && $token_recibido != "") {
            $objeto_token = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);
            if ($objeto_token != "" && isset($objeto_token)) {
                $usuario = $objeto_token->usuario;
                if (isset($fecha) && $fecha != ""
                    && isset($tipo) && $tipo != ""
                    && isset($id_local) && $id_local != ""
                ) {

                    $horario = $this->revisar_horario();

                    if (isset($horario)) {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_OK,
                            "mensaje" => $this->lang->line('cliente_revisar_horario_exclusion'),
                            "horarios" => $horario
                        );
                    } else {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR,
                            "mensaje" => $this->lang->line('error_revisar_horario_exclusion')
                        );
                    }
                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_parametros')
                    );
                }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo,$token_recibido, $usuario, $operacion);
        $this->response($resultado);
    }

    public function buscar_reserva_fecha_post()
    {
        $token_recibido = $this->getHeaderRC('token');
        $fecha = $this->post("fecha", TRUE);
        $id_local = $this->post("id_local", TRUE);
        $operacion = "buscar fecha reserva";

        $accion_realizada = ACCION_DASHBOARD_FECHA_RESERVA;
        $nombre_metodo = NOMBRE_METODO_POST;

        $parametros = array(
            "token" => $token_recibido,
            "fecha" => $fecha,
            "id_local"=>$id_local
        );

        $usuario = "";
        if (isset($token_recibido) && $token_recibido != "") {
            $objeto_token = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);
            if ($objeto_token != "" && isset($objeto_token)) {
                $usuario = $objeto_token->usuario;
                if (isset($fecha) && $fecha != "" && isset($id_local) && $id_local != "") {
                    $dia = date('N', strtotime($fecha));
                    $fechas = $this->revisar_horarios($fecha, $dia, $id_local);
                    if (isset($fechas)) {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_OK,
                            "mensaje" => $this->lang->line('mensaje_operacion_exito'),
                            "reserva" => $fechas
                        );
                    } else {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR,
                            "mensaje" => $this->lang->line('error_obtener_detalle_reserva'),
                        );
                    }
                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_parametro_url')
                    );
                }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token_recibido, $usuario, $operacion);
        $this->response($resultado);
    }



    public function obtener_listado_horarios_exclusion($id_empresa, $paginacion = "",$id_local)
    {
        $where = "";
        if ($id_local != null){
            $where = " AND local.id=".$id_local;
        }


        $parametros_listado_horario = array(

            "select" => "tbl_horario_exclusion.id as id,
            tbl_horario_exclusion.id_horario  as id_horario,
            tbl_horario_exclusion.fecha as fecha,
            tbl_horario_exclusion.hora_inicio as hora_inicio,
            tbl_horario_exclusion.hora_fin as hora_fin,
            tbl_horario_exclusion.tipo as tipo,
            tbl_horario_exclusion.fecha_registro as fecha_registro,
            tbl_horario_exclusion.estado as estado,
            local.id as id_local,
            local.nombre as nombre_local",

            "join" => array(
                "tbl_local local, local.id = tbl_horario_exclusion.id_local",
            ),
            "where" => "local.id_empresa = " . $id_empresa . $where,
            "order" => "tbl_horario_exclusion.fecha_registro desc"
        );

        if (is_numeric($paginacion) && $paginacion > 0) {
            $inicio = ELEMENTOS_POR_PAGINA * ($paginacion - 1);

            $horarios = $this->horario_exclusion_model->search_data_array($parametros_listado_horario, $inicio, ELEMENTOS_POR_PAGINA);

        } else {
            $horarios = $this->horario_exclusion_model->search_array($parametros_listado_horario);
        }

        $total_horarios = $this->horario_exclusion_model->total_records($parametros_listado_horario);

        $datos_horarios = array(
            "total_horarios" => $total_horarios, "horarios" => $horarios
        );

        return $datos_horarios;

    }

    public function obtener_buscar_horarios_exclusion($id_empresa, $paginacion = "")
    {

        $tipo = $this->post("tipo", TRUE);
        $fecha = $this->post("fecha", TRUE);
        $local = $this->post("local", TRUE);
        $estado = $this->post("estado", TRUE);

        $filtro_tipo = "";
        $filtro_fecha = "";
        $filtro_local = "";
        $filtro_estado = "";

        if ($tipo != "") {
            $filtro_tipo = " and tbl_horario_exclusion.tipo  = $tipo ";
        }
        if ($fecha != "") {
            $filtro_fecha = " and tbl_horario_exclusion.fecha = '$fecha'";
        }
        if ($local != "") {
            $filtro_local = " and local.id = '$local'";
        }
        if ($estado != "" && is_numeric($estado)) {
            $filtro_estado = " and tbl_horario_exclusion.estado = $estado";
        }


        $parametros_listado_horario = array(

            "select" => "tbl_horario_exclusion.id as id,
            tbl_horario_exclusion.id_horario  as id_horario,
            tbl_horario_exclusion.fecha as fecha,
            tbl_horario_exclusion.hora_inicio as hora_inicio,
            tbl_horario_exclusion.hora_fin as hora_fin,
            tbl_horario_exclusion.tipo as tipo,
            tbl_horario_exclusion.fecha_registro as fecha_registro,
            tbl_horario_exclusion.estado as estado,
            local.id as id_local,
            local.nombre as nombre_local",

            "join" => array(
                "tbl_local local, local.id = tbl_horario_exclusion.id_local",
            ),
            "where" => "local.id_empresa = " . $id_empresa . $filtro_tipo . $filtro_fecha . $filtro_local . $filtro_estado,
            "order" => "tbl_horario_exclusion.fecha_registro desc"
        );

        if (is_numeric($paginacion) && $paginacion > 0) {
            $inicio = ELEMENTOS_POR_PAGINA * ($paginacion - 1);

            $horarios = $this->horario_exclusion_model->search_data_array($parametros_listado_horario, $inicio, ELEMENTOS_POR_PAGINA);

        } else {
            $horarios = $this->horario_exclusion_model->search_array($parametros_listado_horario);
        }

        $total_horarios = $this->horario_exclusion_model->total_records($parametros_listado_horario);

        $datos_horarios = array(
            "total_horarios" => $total_horarios, "horarios" => $horarios
        );

        return $datos_horarios;
    }

    public function detalle_horario_exclusion($id_horario_exclusion)
    {

        $parametros_detalle_horario_exclusion = array(
            "select" => "tbl_horario_exclusion.id as id,
                         tbl_horario_exclusion.fecha as fecha,
                         tbl_horario_exclusion.hora_inicio as hora_inicio,
                         tbl_horario_exclusion.hora_fin as hora_fin,
                         tbl_horario_exclusion.tipo as tipo,
                         tbl_horario_exclusion.id_horario as id_horario,
                        tbl_horario_exclusion.estado as estado,
                        local.id as id_local,
                        local.nombre as nombre_local",
            "join" => array(
                "tbl_local local, local.id = tbl_horario_exclusion.id_local",
            ),
            "where" => "tbl_horario_exclusion.id = " . $id_horario_exclusion
        );

        $horario = $this->horario_exclusion_model->get_search_row($parametros_detalle_horario_exclusion);

        return $horario;

    }

    public function obtener_listado_locales($empresa)
    {

        $parametros_listado_local = array(
            "select" => "tbl_local.id as id,
            tbl_local.nombre  as nombre",
            "join" => array(
                "tbl_empresa empresa, empresa.id =  tbl_local.id_empresa",
            ), "where" => "tbl_local.id_empresa = $empresa
             and tbl_local.estado = " . ESTADO_ACTIVO
        );

        $locales = $this->local_model->search_array($parametros_listado_local);

        return $locales;
    }

    public function validar_perfil($token)
    {
        $perfil_asignado = FALSE;
        $perfil_usuario = $this->buscar_usuario_token($token);
        $perfil = json_decode($perfil_usuario->perfil, true);
        foreach ($perfil as $arreglo) {
            if (isset($arreglo["clientes"])) {
                if ($arreglo["clientes"] == "si") {
                    $perfil_asignado = TRUE;
                }
            }
        }
        return $perfil_asignado;
    }

    public function buscar_usuario_token($token)
    {
        $parametros_busqueda_usuario = array(
            "select" => "contacto.perfil", "join" => array(
                "tbl_contacto contacto, contacto.id = tbl_token_dashboard.id_contacto"
            ), "where" => "tbl_token_dashboard.token = '$token'
            and tbl_token_dashboard.estado = " . ESTADO_ACTIVO . "
            and contacto.id = tbl_token_dashboard.id_contacto"
        );
        $empresa_encontrada = $this->token_dashboard_model->get_search_row($parametros_busqueda_usuario);
        return $empresa_encontrada;
    }

    public function agregar_horario_exclusion()
    {

        $id_horario = $this->post("id_horario", TRUE);
        $id_local = $this->post("id_local", TRUE);
        $fecha = $this->post("fecha", TRUE);
        $tipo = $this->post("tipo", TRUE);
        $hora_inicio = $this->post("hora_inicio", TRUE);
        $hora_fin = $this->post("hora_fin", TRUE);
        $estado = $this->post("estado", TRUE);


        $parametros_horario = array(
            'id_horario' => $id_horario,
            'id_local' => $id_local,
            'fecha' => $fecha,
            'tipo' => $tipo,
            'hora_inicio' => $hora_inicio,
            'hora_fin' => $hora_fin,
            'estado' => $estado,
            'fecha_registro' => date("Y-m-d H:i:s"),

        );

        $horario = $this->horario_exclusion_model->insert($parametros_horario);

        return $horario;

    }

    public function revisar_horarios($fecha, $dia, $local)
    {

        $obtener_fecha_reserva = array(
            "select" => "tbl_horario_reserva.id as id_horario,DATE_FORMAT(tbl_horario_reserva.hora_inicio,'%h:%i %p') as hora_inicio,
                DATE_FORMAT(tbl_horario_reserva.hora_fin,'%h:%i %p') as hora_fin ,tbl_horario_reserva.stock as stock_total,
                tbl_horario_reserva.stock  as stock_final",
            "join" => array(
                "tbl_local local, local.id = tbl_horario_reserva.id_local"
            ),
            "where" => "tbl_horario_reserva.id_local = " . $local . " 
                        and local.estado = " . ESTADO_ACTIVO . "
                        and tbl_horario_reserva.dia = " . $dia . "
                        and tbl_horario_reserva.estado = " . ESTADO_ACTIVO . ""
        );

        $horarios = $this->horario_reserva_model->search_array($obtener_fecha_reserva);

        return $horarios;
    }

    public function editar_horario_exclusion()
    {

        $id_horario_exclusion = $this->post("id_horario_exclusion", TRUE);
        $fecha = $this->post("fecha", TRUE);
        $tipo = $this->post("tipo", TRUE);
        $horario = $this->post("horario", TRUE);
        $hora_inicio = $this->post("hora_inicio", TRUE);
        $hora_fin = $this->post("hora_fin", TRUE);
        $estado = $this->post("estado", TRUE);


        $parametros_horario = array(
            'fecha' => $fecha,
            'tipo' => $tipo,
            'id_horario' => $horario,
            'hora_inicio' => $hora_inicio,
            'hora_fin' => $hora_fin,
            'estado' => $estado,
            'fecha_modificacion' => date("Y-m-d H:i:s")

        );

        $horario = $this->horario_exclusion_model->update($id_horario_exclusion, $parametros_horario);

        return $horario;

    }

    public function revisar_horario()
    {
        $id_horario_exclusion = $this->post("id_horario_exclusion", TRUE);
        $id_horario = $this->post("id_horario", TRUE);
        $tipo = $this->post("tipo", TRUE);
        $fecha = $this->post("fecha", TRUE);
        $id_local = $this->post("id_local", TRUE);

        $filtro_horario = "";

        if ($id_horario_exclusion != "" || $id_horario_exclusion != null) {
            $filtro_horario = " and id  != $id_horario_exclusion ";
        }
        if ($id_horario != "" || $id_horario != null) {
            $filtro_horario_tipo = " and id_horario = $id_horario ";
        }



        $params_horario = array(
            "select" => "id_horario,tipo,fecha,estado",
            "where" => "fecha = '$fecha' 
                        and id_local = " . $id_local . " 
                        and estado = " . ESTADO_ACTIVO . $filtro_horario .$filtro_horario_tipo
        );

        $horarios = $this->horario_exclusion_model->search_array($params_horario);

        return $horarios;
    }

    public function horarios_disponibles($fecha_data, $fecha, $local, $dia, $type)
    {
        if ($fecha_data == 1) {


            $obtener_fecha_reserva = array(
                "select" => "tbl_reserva_solicitud.id_horario,horario.hora_inicio,horario.hora_fin,
                horario.stock as stock_total,horario.stock - sum(tbl_reserva_solicitud.cantidad) as stock_final",

                "join" => array(
                    "tbl_horario_reserva horario, horario.id =  tbl_reserva_solicitud.id_horario",
                    "tbl_local local, local.id = tbl_reserva_solicitud.id_local",
                ),

                "where" => "tbl_reserva_solicitud.fecha = '$fecha' 
                        and tbl_reserva_solicitud.id_local = " . $local . " 
                        and local.estado = " . ESTADO_ACTIVO . "
                        and horario.estado = " . ESTADO_ACTIVO . "
                        and tbl_reserva_solicitud.estado != " . ESTADO_RECHAZADO . "
                        and tbl_reserva_solicitud.estado != " . ESTADO_INACTIVO . "",
                "group" => "tbl_reserva_solicitud.id_horario"

            );

            $reserva_fecha_data = $this->reserva_solicitud_model->search_array($obtener_fecha_reserva);


            $where = "";
            if ($type) {
                $where = " and tbl_horario_reserva.id not in  (select id_horario from tbl_horario_exclusion 
                        where fecha ='$fecha' and estado = 1)";
            }

            $where_horario = " and tbl_horario_reserva.id not in  (select id_horario from tbl_reserva_solicitud
                        where fecha ='$fecha' and estado != 0  and estado != 2 and id_local = " . $local . ")";

            $obtener_fecha_reserva = array(
                "select" => "tbl_horario_reserva.id as id_horario,tbl_horario_reserva.hora_inicio,tbl_horario_reserva.hora_fin,tbl_horario_reserva.stock as stock_total,tbl_horario_reserva.stock  as stock_final",

                "join" => array(
                    "tbl_local local, local.id = tbl_horario_reserva.id_local",


                ), "where" => "tbl_horario_reserva.id_local = " . $local . " 
                        and local.estado = " . ESTADO_ACTIVO . "
                        and tbl_horario_reserva.dia = " . $dia . "
                        and tbl_horario_reserva.estado = " . ESTADO_ACTIVO . $where_horario . $where,
            );

            $horarios_excluidos = $this->horario_reserva_model->search_array($obtener_fecha_reserva);

            $horario_total = array_merge($reserva_fecha_data, $horarios_excluidos);

            if ($type) {
                $obtener_fecha_reserva_excluido = array(
                    "select" => "tbl_horario_exclusion.id_horario as id_horario,tbl_horario_exclusion.hora_inicio as hora_inicio,
                tbl_horario_exclusion.hora_fin as hora_fin,horario_reserva.stock as stock_total,horario_reserva.stock  as stock_final",

                    "join" => array(
                        "tbl_local local, local.id = tbl_horario_exclusion.id_local",
                        "tbl_horario_reserva horario_reserva, tbl_horario_exclusion.id_horario = horario_reserva.id"

                    ),
                    "where" => "tbl_horario_exclusion.estado = " . ESTADO_ACTIVO . " 
                        and tbl_horario_exclusion.fecha = '$fecha '
                        and tbl_horario_exclusion.tipo = 1 
                        and horario_reserva.estado = " . ESTADO_ACTIVO . " 
                        and horario_reserva.id_local = " . $local . " 
                        and local.estado = " . ESTADO_ACTIVO
                );


                $horarios_excluidos = $this->horario_exclusion_model->search_array($obtener_fecha_reserva_excluido);

                $horario_total = array_merge($horario_total, $horarios_excluidos);
            }

            $horarios[] = array();

            for ($i = 0; $i < sizeof($horario_total); $i++) {
                if ($horario_total[$i]["stock_final"] > 0) {
                    $horarios[$i] = $horario_total[$i];
                }
            }

        } else {


            $where = "";
            if ($type) {
                $where = " and tbl_horario_reserva.id not in  (select id_horario from tbl_horario_exclusion 
                        where fecha ='$fecha' and estado = 1)";
            }

            $obtener_fecha_reserva = array(
                "select" => "tbl_horario_reserva.id as id_horario,tbl_horario_reserva.hora_inicio,tbl_horario_reserva.hora_fin,tbl_horario_reserva.stock as stock_total,tbl_horario_reserva.stock  as stock_final",

                "join" => array(
                    "tbl_local local, local.id = tbl_horario_reserva.id_local",


                ), "where" => "tbl_horario_reserva.id_local = " . $local . " 
                        and local.estado = " . ESTADO_ACTIVO . "
                        and tbl_horario_reserva.dia = " . $dia . "
                        and tbl_horario_reserva.estado = " . ESTADO_ACTIVO . $where,
            );

            $horarios = $this->horario_reserva_model->search_array($obtener_fecha_reserva);

            if ($type) {
                $obtener_fecha_reserva_excluido = array(
                    "select" => "tbl_horario_exclusion.id_horario as id_horario,tbl_horario_exclusion.hora_inicio as hora_inicio,
                tbl_horario_exclusion.hora_fin as hora_fin,horario_reserva.stock as stock_total,horario_reserva.stock  as stock_final",

                    "join" => array(
                        "tbl_local local, local.id = tbl_horario_exclusion.id_local",
                        "tbl_horario_reserva horario_reserva, tbl_horario_exclusion.id_horario = horario_reserva.id"

                    ), "where" => "tbl_horario_exclusion.estado = " . ESTADO_ACTIVO . " 
                        and tbl_horario_exclusion.fecha = '$fecha '
                        and tbl_horario_exclusion.tipo = 1 
                        and horario_reserva.estado = " . ESTADO_ACTIVO . " 
                        and horario_reserva.id_local = " . $local . " 
                        and local.estado = " . ESTADO_ACTIVO
                );


                $horarios_excluidos = $this->horario_exclusion_model->search_array($obtener_fecha_reserva_excluido);

                $horarios = array_merge($horarios, $horarios_excluidos);
            }

        }
        return $horarios;
    }


}