<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
require APPPATH . "/libraries/Uuid.php";
require APPPATH.'/libraries/Pusher.php';

class Reserva_solicitud extends REST_Controller
{
    public $id_empresa;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('utils_helper');
        $this->load->model("token_dashboard_model", "token_dashboard_model");
        $this->load->model("usuario_model", "usuario_model");
        $this->load->model("horario_exclusion_model", "horario_exclusion_model");
        $this->load->model("reserva_solicitud_model", "reserva_solicitud_model");
        $this->load->model("tipo_pago_empresa_model", "tipo_pago_empresa_model");
        $this->load->model("puntos_model", "puntos_model");
        $this->load->model("horario_reserva_model", "horario_reserva_model");
        $this->load->model("canje_model", "canje_model");
        $this->load->model("beneficio_model", "beneficio_model");
        $this->load->model("local_model", "local_model");
        $this->load->model("local_ubicacion_model", "local_ubicacion_model");
        $this->load->model("promocion_model", "promocion_model");
        $this->load->model("promocion_canje_model", "promocion_canje_model");
        $this->load->model("empresa_model", "empresa_model");
        $this->lang->load('dashboard_error_lang', 'spanish');
        $this->lang->load('dashboard_mensajes_lang', 'spanish');
        $this->id_empresa = $this->config->item('empresa');
        $this->origen = "reserva";

    }

    public function obtener_reservas_post($paginacion = "") 
    {

        $accion_realizada = ACCION_DASHBOARD_OBTENER_RESERVAS;
        $nombre_metodo = NOMBRE_METODO_POST;
        $id_local = $this->post("id_local", TRUE);
        $date = $this->post("date", TRUE);
        $token_recibido = $this->getHeaderRC('token');


        $operacion = "Listado reservas";
        $usuario = "";


        $parametros = array(
            "token" => $token_recibido,
            "id_local" => $id_local,
            "date" => $date
        );

        $id_empresa = $this->id_empresa;
        $datos_locales = $this->obtener_listado_locales($id_empresa);

        $objeto_token = validar_token_seguridad_dashboard($token_recibido,$this->id_empresa);

        if($objeto_token!="" && isset($objeto_token)){
            if (isset($datos_locales)) {
                $datos_reservas = $this->obtener_listado_reservas($id_empresa,$id_local,$date, $paginacion);
                $usuario = $objeto_token->usuario;
                if (isset($datos_reservas)) {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_OK, "mensaje" => $this->lang->line('cliente_obtener_reserva'), "total_reservas" => $datos_reservas["total_reservas"], "reservas" => $datos_reservas["reservas"], "locales" => $datos_locales
                    );

                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_obtener_reservas')
                    );
                }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_obtener_locales')
                );
            }
        } else {
            $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_token_valido')
                );
        }

            

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo,$token_recibido,$usuario,$operacion);
        $this->response($resultado);
    }

    public function filtrar_reservas_post($paginacion = "")
    {

        $accion_realizada = ACCION_DASHBOARD_BUSCAR_RESERVA;
        $nombre_metodo = NOMBRE_METODO_POST;
        $token_recibido = $this->getHeaderRC('token');
        $usuario = "";
        $operacion ="Filtrar reservas";


        $id_local = $this->post("id_local", TRUE);
        $nombre = $this->post("nombre", TRUE);
        $apellido = $this->post("apellido", TRUE);
        $dni = $this->post("dni", TRUE);
        $dia = $this->post("dia", TRUE);
        $local = $this->post("local", TRUE);
        $estado = $this->post("estado", TRUE);
        $fecha_actual = $this->post("fecha_actual", TRUE);
        $fecha_inicio= $this->post("fecha_inicio", TRUE);
        $fecha_fin = $this->post("fecha_fin", TRUE);

        $parametros = array(
            "token" => $token_recibido,
            "id_local" => $id_local,
            "paginacion" => $paginacion,
            "nombre" => $nombre,
            "apellido" => $apellido,
            "dni" => $dni,
            "dia" => $dia,
            "local" => $local,
            "fecha_actual" => $fecha_actual,
            "fecha_inicio" => $fecha_inicio,
            "fecha_fin" => $fecha_fin,
            "estado" => $estado
        );


        $id_empresa = $this->id_empresa;

        $datos_locales = $this->obtener_listado_locales($id_empresa);
        $objeto_token = validar_token_seguridad_dashboard($token_recibido,$this->id_empresa);
        if ($objeto_token!="" && isset($objeto_token)) {
            $usuario = $objeto_token->usuario;
            if (isset($datos_locales)) {
                $datos_reservas = $this->obtener_buscar_reservas($id_empresa, $paginacion);

                if (isset($datos_reservas)) {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_OK, "mensaje" => $this->lang->line('cliente_obtener_reserva'), "total_reservas" => $datos_reservas["total_reservas"], "reservas" => $datos_reservas["reservas"], "locales" => $datos_locales
                    );
                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_obtener_reservas')
                    );
                }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_obtener_locales')
                );
            }
        } else {
             $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_token_recibido')
                );

        }

        

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token_recibido, $usuario, $operacion);

        $this->response($resultado);
    }

    public function detalle_reserva_get($id_reserva)
    {

        $accion_realizada = ACCION_DASHBOARD_DETALLE_RESERVA;
        $nombre_metodo = NOMBRE_METODO_GET;
        $token_recibido = $this->getHeaderRC('token');
        $objeto_token = validar_token_seguridad_dashboard($token_recibido,$this->id_empresa);
        $usuario ="";
        $operacion = "detalle reserva";

        $parametros = array(
            "token" => $token_recibido,
            "id_reserva" => $id_reserva
        );


        if ($objeto_token!="" && isset($objeto_token)) {
            $usuario = $objeto_token->usuario;
            if (isset($id_reserva) && $id_reserva != "") {

                $detalle_reserva = $this->detalle_reserva($id_reserva);
                $id_local = $detalle_reserva->id_local;

                if (isset($detalle_reserva)) {
                    $listado_ubicacion = $this->obtener_listado_ubicacion($id_local);

                    if (isset($listado_ubicacion)) {

                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_OK, "mensaje" => $this->lang->line('mensaje_operacion_exito'), "reserva" => $detalle_reserva, "ubicacion" => $listado_ubicacion
                        );
                    } else {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_obtener_listado_ubicacion')
                        );
                    }

                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_obtener_detalle_reserva')
                    );
                }

            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_parametro_url')
                );
            }
        } else {
             $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_token_recibido')
                );
        }

        

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token_recibido, $usuario, $operacion );
        $this->response($resultado);
    }

    public function buscar_reserva_fecha_post()
    {

        $fecha = $this->post("fecha", TRUE);
        $id_local = $this->post("id_local", TRUE);
        $token_recibido = $this->getHeaderRC('token');
        $usuario = "";
        $operacion = "buscar fecha reserva";

        $accion_realizada = ACCION_DASHBOARD_FECHA_RESERVA;
        $nombre_metodo = NOMBRE_METODO_POST;

        $parametros = array(
            "token" => $token_recibido,
            "fecha" => $fecha,
            "id_local" => $id_local
        );

        $objeto_token = validar_token_seguridad_dashboard($token_recibido,$this->id_empresa);

        if ($objeto_token!="" && isset($objeto_token)) {
            $usuario = $objeto_token->usuario;
                if (isset($fecha) && $fecha != "" && isset($id_local) && $id_local != "") {

                    if (validar_formato_fecha($fecha))
                    {
                        $dia = date('N', strtotime($fecha));

                        $fechas = $this->revisar_horarios($fecha, $dia, $id_local);

                        if (isset($fechas) && sizeof($fechas) > 0) {

                            $name = 'hora_inicio';
                            usort($fechas, function ($a, $b) use (&$name) {
                                return $a[$name] - $b[$name];
                            });

                            $ubicaciones = $this->obtener_listado_ubicacion($id_local);
                            if ($ubicaciones) {
                                $resultado = array(
                                    "resultado" => ESTADO_RESPUESTA_OK,
                                    "mensaje" => $this->lang->line('mensaje_operacion_exito'),
                                    "reserva" => $fechas,
                                    "ubicacion" => $ubicaciones
                                );

                            } else {
                                $resultado = array(
                                    "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_ubicacion_listado')
                                );
                            }

                        } else {
                            $resultado = array(

                                "resultado" => ESTADO_RESPUESTA_ERROR,
                                "mensaje" => $this->lang->line('error_obtener_detalle_reserva')
                            );
                        }
                    } else
                    {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR,
                            "mensaje" => $this->lang->line('error_formato_fecha')

                        );
                    }

                } else {
                     $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_parametro_url')
                        );
                }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_token_recibido')
            );
        }

        

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo,$token_recibido, $usuario, $operacion);
        $this->response($resultado);
    }

    public function editar_reserva_post()
    {

        $accion_realizada = ACCION_DASHBOARD_EDITAR_RESERVA;
        $nombre_metodo = NOMBRE_METODO_POST;

        $id_reserva = $this->post("id_reserva", TRUE);
        $ubicacion = $this->post("ubicacion", TRUE);
        $estado = $this->post("estado", TRUE);

        $token_recibido = $this->getHeaderRC('token');
        $usuario = "";
        $operacion = "editar reserva";

        $parametros = array(
            "token"=>$token_recibido,
            "id_reserva" => $id_reserva,
            "ubicacion" => $ubicacion,
            "estado" => $estado
        );
        $objeto_token = validar_token_seguridad_dashboard($token_recibido,$this->id_empresa);
        if ($objeto_token!="" && isset($objeto_token)) {
            $usuario = $objeto_token->usuario;
            if (isset($id_reserva) && $id_reserva != "" && isset($ubicacion) && $ubicacion != "" && isset($estado) && $estado != "") {
                $reserva = $this->update_reserva();
                    if (isset($reserva)) {
                        if ($estado  == "1" || $estado == "2"){
                            $envio = $this->enviar_correo_reserva_estado($id_reserva,$estado);
                            if ($envio["envio"]){
                                $local = $envio["datos"]["local_id"];
                                $resultado = array(
                                    "resultado" => ESTADO_RESPUESTA_OK, "mensaje" => $this->lang->line('cliente_actualizar_reserva')
                                );

                                $this->actualizar_listado_reservas($local);
                            }else{
                                $resultado = array(
                                    "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_envio_mail')
                                );
                            }
                        }else{
                            $resultado = array(
                                "resultado" => ESTADO_RESPUESTA_OK, "mensaje" => $this->lang->line('cliente_actualizar_reserva')
                            );
                        }

                    } else {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_actualizar_reserva')
                        );
                    }

            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_parametros')
                );
            }
        } else{
            $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_token_recibido')
                );
        }
        

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token_recibido, $usuario, $operacion);
        $this->response($resultado);


    }

    public function editar_horario_reserva_post()
    {

        $accion_realizada = ACCION_DASHBOARD_EDITAR_HORARIO_RESERVA;
        $nombre_metodo = NOMBRE_METODO_POST;
        $id_local = $this->post("id_local", TRUE);
        $id_reserva = $this->post("id_reserva", TRUE);
        $id_horario = $this->post("id_horario", TRUE);
        $fecha = $this->post("fecha", TRUE);
        $hora = $this->post("hora", TRUE);
        $ubicacion = $this->post("ubicacion", TRUE);
        $cantidad = $this->post("cantidad", TRUE);
        $estado = $this->post("estado", TRUE);
        $notificacion = $this->post("notificacion", TRUE);
        $token_recibido = $this->getHeaderRC('token');
        $usuario ="";
        $operacion = "editar horario reserva";

        $parametros = array(
            "token" => $token_recibido,
            "id_local"  => $id_local,
            "id_reserva" => $id_reserva,
            "id_horario" => $id_horario,
            "fecha" => $fecha,
            "hora" => $hora,
            "ubicacion" => $ubicacion,
            "cantidad" => $cantidad,
            "estado" => $estado,
            "notificacion" => $notificacion,
        );
        $objeto_token = validar_token_seguridad_dashboard($token_recibido,$this->id_empresa);

        if($objeto_token!="" && isset($objeto_token)){
            $usuario = $objeto_token->usuario;
            if (isset($id_reserva) && $id_reserva != "" && isset($id_horario) && $id_horario != ""
            && isset($fecha) && $fecha != "" && isset($hora) && $hora != ""
            && isset($ubicacion) && $ubicacion != ""
            && isset($cantidad) && $cantidad != ""
            && isset($estado) && $estado != ""
            ) {
                        $reserva = $this->update_horario_reserva();

                        $enviar = true;

                         if ($notificacion  == "1"){
                            $enviar = $this->enviar_correo_reserva_notificacion($id_reserva);
                        }

                        if (isset($reserva) ) {

                             if ($enviar){
                                 $resultado = array(
                                     "resultado" => ESTADO_RESPUESTA_OK,
                                     "mensaje" => $this->lang->line('cliente_actualizar_reserva')
                                 );
                             }else{
                                     $resultado = array(
                                         "resultado" => ESTADO_RESPUESTA_ERROR,
                                         "mensaje" => $this->lang->line('error_envio_mail')
                                     );

                             }
                        } else {
                            $resultado = array(
                                "resultado" => ESTADO_RESPUESTA_ERROR,
                                "mensaje" => $this->lang->line('error_actualizar_reserva')
                            );
                        }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_parametros')
                );
            }
        }else {
             $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_token_recibido')
                );
        }
        

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token_recibido, $usuario, $operacion);
        $this->response($resultado);


    }

    public function recepcion_reserva_post($id_reserva){
        $accion_realizada = ACCION_DASHBOARD_RECEPCION_RESERVA;
        $nombre_metodo = NOMBRE_METODO_POST;
        $token_recibido = $this->getHeaderRC('token');
        $objeto_token = validar_token_seguridad_dashboard($token_recibido,$this->id_empresa);
        $usuario ="";
        $operacion = "recepcion reserva";

        $parametros = array(
            "token" => $token_recibido,
            "id_reserva" => $id_reserva
        );


        if ($objeto_token!="" && isset($objeto_token)) {
            $usuario = $objeto_token->usuario;
            if (isset($id_reserva) && $id_reserva != "") {

                $recepcion_reserva = $this->detalle_recepcion_reserva($id_reserva);
                if (isset($recepcion_reserva)) {
                    $recepcion = $recepcion_reserva->recepcion;
                    if ($recepcion == '1'){
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_OK,
                            'recepcion' => "1",
                            "mensaje" => $this->lang->line('cliente_recepcion_reserva_check')
                        );
                    }else{
                        $recepcion_reserva_update = $this->update_recepcion_reserva($id_reserva);
                        if (isset($recepcion_reserva_update)){
                            $resultado = array(
                                "resultado" => ESTADO_RESPUESTA_OK,
                                'recepcion' => "0",
                                "mensaje" => $this->lang->line('cliente_recepcion_reserva_actualizar')
                            );
                        }else{
                            $resultado = array(
                                "resultado" => ESTADO_RESPUESTA_ERROR,
                                'recepcion' => "0",
                                "mensaje" => $this->lang->line('error_recepcion_reserva_actualizar')
                            );
                        }
                    }
                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_obtener_detalle_reserva')
                    );
                }

            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_parametro_url')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_token_recibido')
            );
        }



        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token_recibido, $usuario, $operacion );
        $this->response($resultado);
    }

    public function generar_excel_post(){

        $accion_realizada = ACCION_DASHBOARD_GENERAR_EXCEL_RESERVA;
        $nombre_metodo = NOMBRE_METODO_POST;
        $token_recibido = $this->getHeaderRC('token');
        $usuario = "";
        $operacion ="generar excel reservas";


        $id_local = $this->post("id_local", TRUE);
        $nombre = $this->post("nombre", TRUE);
        $apellido = $this->post("apellido", TRUE);
        $dni = $this->post("dni", TRUE);
        $dia = $this->post("dia", TRUE);
        $local = $this->post("local", TRUE);
        $estado = $this->post("estado", TRUE);
        $fecha_actual = $this->post("fecha_actual", TRUE);
        $fecha_inicio= $this->post("fecha_inicio", TRUE);
        $fecha_fin = $this->post("fecha_fin", TRUE);

        $parametros = array(
            "token" => $token_recibido,
            "id_local" => $id_local,
            "nombre" => $nombre,
            "apellido" => $apellido,
            "dni" => $dni,
            "dia" => $dia,
            "local" => $local,
            "fecha_actual" => $fecha_actual,
            "fecha_inicio" => $fecha_inicio,
            "fecha_fin" => $fecha_fin,
            "estado" => $estado
        );


        $id_empresa = $this->id_empresa;

        $objeto_token = validar_token_seguridad_dashboard($token_recibido,$this->id_empresa);
        if ($objeto_token!="" && isset($objeto_token)) {
            $usuario = $objeto_token->usuario;
                $datos_reservas = $this->obtener_excel_reservas($id_empresa);
                if (isset($datos_reservas)) {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_OK,
                        "mensaje" => $this->lang->line('cliente_obtener_reserva'),
                        "reservas" => $datos_reservas

                    );
                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_obtener_reservas')
                    );
                }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_token_recibido')
            );

        }


        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token_recibido, $usuario, $operacion);

        $this->response($resultado);
    }


    /****************METODOS******************/



    public function buscar_token_dashboard($token)
    {
        $parametros_busqueda_token = array(
            "select" => "tbl_token_dashboard.id,
            tbl_token_dashboard.token", "join" => array(
                "tbl_contacto contacto, contacto.id = tbl_token_dashboard.id_contacto"
            ), "where" => "tbl_token_dashboard.estado = " . ESTADO_ACTIVO . "
            and contacto.estado = " . ESTADO_ACTIVO . "
            and contacto.id_empresa = $this->id_empresa
            and tbl_token_dashboard.token = '$token'"
        );
        $token_existe = $this->token_dashboard_model->get_search_row($parametros_busqueda_token);
        return $token_existe;
    }

    public function obtener_listado_reservas($id_empresa, $id_local,$date,$paginacion = "")
    {
        $where_local="";

        $where_date = "";

        if ($id_local != "" || $id_local != null){
            $where_local =  " and local.id = ".$id_local;
        }

        if($date != "" || $date != null ){
            $where_date = " and tbl_reserva_solicitud.fecha = '".$date."'";
        }


        $parametros_listado_reserva = array(
            "select" => "tbl_reserva_solicitud.id as id,
            local.nombre  as local_nombre,
            local.id  as local_id,
            persona.nombre as nombre,
            persona.apellido as apellido,
            persona.dni as dni,
            tbl_reserva_solicitud.cantidad as cantidad,
            tbl_reserva_solicitud.hora as hora,
            horario.dia as dia,
            tbl_reserva_solicitud.estado as estado,
            tbl_reserva_solicitud.fecha as fecha",

            "join" => array(
                "tbl_local_ubicacion ubicacion, ubicacion.id =  tbl_reserva_solicitud.id_local_ubicacion",
                "tbl_usuario usuario, usuario.id = tbl_reserva_solicitud.id_usuario",
                "tbl_persona persona, persona.id = usuario.id_persona",
                "tbl_local local, local.id= tbl_reserva_solicitud.id_local",
                "tbl_horario_reserva horario, horario.id = tbl_reserva_solicitud.id_horario" .$where_local . $where_date
            ), "where" => "usuario.estado = " . ESTADO_ACTIVO . "
            and local.id_empresa = $id_empresa
            and persona.estado = " . ESTADO_ACTIVO,
            "order" => "tbl_reserva_solicitud.fecha_registro desc"
        );

        if (is_numeric($paginacion) && $paginacion > 0) {
            $inicio = ELEMENTOS_POR_PAGINA * ($paginacion - 1);

            $reservas = $this->reserva_solicitud_model->search_data_array($parametros_listado_reserva, $inicio, ELEMENTOS_POR_PAGINA);

        } else {
            $reservas = $this->reserva_solicitud_model->search_array($parametros_listado_reserva);
        }

        $total_reservas = $this->reserva_solicitud_model->total_records($parametros_listado_reserva);

        $datos_reservas = array(
            "total_reservas" => $total_reservas, "reservas" => $reservas
        );

        return $datos_reservas;

    }

    public function obtener_buscar_reservas($id_empresa, $paginacion = "")
    {


        $nombre = $this->post("nombre", TRUE);
        $apellido = $this->post("apellido", TRUE);
        $dni = $this->post("dni", TRUE);
        $dia = $this->post("dia", TRUE);
        $local = $this->post("id_local", TRUE);
        $fecha_actual = $this->post("fecha_actual", TRUE);
        $fecha_inicio= $this->post("fecha_inicio", TRUE);
        $fecha_fin = $this->post("fecha_fin", TRUE);
        $estado = $this->post("estado", TRUE);

        $filtro_nombre = "";
        $filtro_apellido = "";
        $filtro_dni = "";
        $filtro_dia = "";
        $filtro_local = "";
        $filtro_fecha_actual = "";
        $filtro_fecha_inicio = "";
        $filtro_fecha_fin = "";
        $filtro_estado = "";


        if ($nombre != "") {
            $filtro_nombre = " and persona.nombre like '%$nombre%'";
        }
        if ($apellido != "") {
            $filtro_apellido = " and persona.apellido like '%$apellido%'";
        }
        if ($dni != "" && is_numeric($dni)) {
            $filtro_dni = " and persona.dni like '%$dni%'";
        }
        if ($dia != "") {
            $filtro_dia = " and horario.dia = '$dia'";
        }
        if ($local != "") {
            $filtro_local = " and tbl_reserva_solicitud.id_local = '$local'";
        }
        if ($estado != "" && is_numeric($estado)) {
            $filtro_estado = " and tbl_reserva_solicitud.estado = $estado";
        }

        if ($fecha_inicio != "" ) {
            list($year, $month, $day) = explode('-', $fecha_inicio);
            $filtro_fecha_inicio = " and tbl_reserva_solicitud.fecha >= '$year-$month-$day' ";
        }

        if ($fecha_fin != "" ) {
            list($year, $month, $day) = explode('-', $fecha_fin);
            $filtro_fecha_fin = " and tbl_reserva_solicitud.fecha  <= '$year-$month-$day' ";
        }

        if($fecha_actual != "" || $fecha_actual != null ){
            $filtro_fecha_actual = " and tbl_reserva_solicitud.fecha = '".$fecha_actual."'";
        }

        $parametros_listado_reserva = array(
            "select" => "tbl_reserva_solicitud.id as id,
            local.nombre  as local_nombre,
            persona.nombre as nombre,
            persona.apellido as apellido,
            persona.dni as dni,
            tbl_reserva_solicitud.cantidad as cantidad,
            tbl_reserva_solicitud.hora as hora,
            horario.dia as dia,
            tbl_reserva_solicitud.estado as estado,
            tbl_reserva_solicitud.fecha as fecha",

            "join" => array(
                "tbl_local_ubicacion ubicacion, ubicacion.id =  tbl_reserva_solicitud.id_local_ubicacion", "tbl_usuario usuario, usuario.id = tbl_reserva_solicitud.id_usuario", "tbl_persona persona, persona.id = usuario.id_persona", "tbl_local local, local.id= tbl_reserva_solicitud.id_local", "tbl_horario_reserva horario, horario.id = tbl_reserva_solicitud.id_horario"
            ), "where" => "usuario.estado = " . ESTADO_ACTIVO . "
            and local.id_empresa = $id_empresa
            and persona.estado = " . ESTADO_ACTIVO . $filtro_nombre . $filtro_apellido . $filtro_dni . $filtro_dia . $filtro_local . $filtro_estado  . $filtro_fecha_inicio . $filtro_fecha_fin . $filtro_fecha_actual,
            "order" => "tbl_reserva_solicitud.fecha_registro desc"
        );

        if (is_numeric($paginacion) && $paginacion > 0) {
            $inicio = ELEMENTOS_POR_PAGINA * ($paginacion - 1);
            $reservas = $this->reserva_solicitud_model->search_data_array($parametros_listado_reserva, $inicio, ELEMENTOS_POR_PAGINA);
        } else {
            $reservas = $this->reserva_solicitud_model->search_array($parametros_listado_reserva);
        }

        $total_reservas = $this->reserva_solicitud_model->total_records($parametros_listado_reserva);

        $datos_reservas = array(
            "total_reservas" => $total_reservas, "reservas" => $reservas
        );
        return $datos_reservas;
    }

    public function detalle_reserva($id_reserva)
{

    $parametros_detalle_reserva = array(
        "select" => "persona.nombre as nombre,
                         persona.apellido as apellido,
                         persona.dni as dni,
                         persona.telefono as telefono,
                         usuario.correo as correo,
                         usuario.id as id_usuario,
                         horario.dia as dia,
                         local.nombre as nombre_local,
                         local.id as id_local,
                         local.meses_reserva as meses,
                         ubicacion.nombre  as nombre_local_ubicacion,
                         ubicacion.id  as id_local_ubicacion,
                         tbl_reserva_solicitud.fecha as fecha,
                         tbl_reserva_solicitud.hora as hora,
                         tbl_reserva_solicitud.cantidad as cantidad,
                         tbl_reserva_solicitud.estado as estado,
                         tbl_reserva_solicitud.comentario as comentario,
                         tbl_reserva_solicitud.id as id,
                         tbl_reserva_solicitud.id_horario  as id_horario,
                         DATE_FORMAT(tbl_reserva_solicitud.fecha_registro,'%Y-%m-%d')  as fecha_registro,
                         DATE_FORMAT(tbl_reserva_solicitud.fecha_registro,'%l:%i %p')  as hora_registro,
                         DATE_FORMAT(tbl_reserva_solicitud.fecha_confirmacion,'%Y-%m-%d')  as fecha_confirmacion,
                         DATE_FORMAT(tbl_reserva_solicitud.fecha_confirmacion,'%l:%i %p')  as hora_confirmacion,",

        "join" => array(
            "tbl_local_ubicacion ubicacion, ubicacion.id =  tbl_reserva_solicitud.id_local_ubicacion",
            "tbl_usuario usuario, usuario.id = tbl_reserva_solicitud.id_usuario",
            "tbl_persona persona, persona.id = usuario.id_persona",
            "tbl_local local, local.id= tbl_reserva_solicitud.id_local",
            "tbl_horario_reserva horario, horario.id = tbl_reserva_solicitud.id_horario"
        ),
        "where" => "tbl_reserva_solicitud.id = " . $id_reserva
    );

    $reserva = $this->reserva_solicitud_model->get_search_row($parametros_detalle_reserva);

    return $reserva;

}

    public function obtener_listado_locales($empresa)
    {


        $parametros_listado_local = array(
            "select" => "tbl_local.id as id,
            tbl_local.nombre  as nombre", "join" => array(
                "tbl_empresa empresa, empresa.id =  tbl_local.id_empresa",
            ), "where" => "tbl_local.id_empresa = $empresa
             and tbl_local.estado = " . ESTADO_ACTIVO
        );

        $locales = $this->local_model->search_array($parametros_listado_local);

        return $locales;
    }

    public function detalle_recepcion_reserva($id_reserva)
{

    $parametros_detalle_reserva = array(
        "select" => "tbl_reserva_solicitud.recepcion as recepcion",
        "where" => "tbl_reserva_solicitud.id = " . $id_reserva
    );

    $reserva = $this->reserva_solicitud_model->get_search_row($parametros_detalle_reserva);

    return $reserva;

}

    public function actualizar_listado_reservas($id_local){

        $options = array(
            //'encrypted' => true
        );

        $pusher = new Pusher("d5143f48561b03e9c95d", "65b9bd0cd937380f5c2c", "277661", $options);

        $data["update"]="1";

        $pusher->trigger('channel_'.$id_local, 'update_list_reserve', $data);
    }

    public function update_recepcion_reserva($id_reserva)
    {

        $parametros_reserva = array(
            'recepcion' => '1'
        );

        $reserva = $this->reserva_solicitud_model->update($id_reserva, $parametros_reserva);

        return $reserva;

    }

    public function validar_perfil($token)
    {
        $perfil_asignado = FALSE;
        $perfil_usuario = $this->buscar_usuario_token($token);
        $perfil = json_decode($perfil_usuario->perfil, true);
        foreach ($perfil as $arreglo) {
            if (isset($arreglo["clientes"])) {
                if ($arreglo["clientes"] == "si") {
                    $perfil_asignado = TRUE;
                }
            }
        }
        return $perfil_asignado;
    }

    public function buscar_usuario_token($token)
    {
        $parametros_busqueda_usuario = array(
            "select" => "contacto.perfil", "join" => array(
                "tbl_contacto contacto, contacto.id = tbl_token_dashboard.id_contacto"
            ), "where" => "tbl_token_dashboard.token = '$token'
            and tbl_token_dashboard.estado = " . ESTADO_ACTIVO . "
            and contacto.id = tbl_token_dashboard.id_contacto"
        );
        $empresa_encontrada = $this->token_dashboard_model->get_search_row($parametros_busqueda_usuario);
        return $empresa_encontrada;
    }

    public function revisar_horarios($fecha, $dia, $local)
    {


        $params_fecha = array(
            "select" => "fecha",
            "where" => "fecha = '$fecha'",
            "group" => "fecha"
        );

        $params_horario_exclusion = array(
            "select" => "id_local,id_horario,fecha,tipo,estado",
            "where" => "fecha = '$fecha'
             and estado =" . ESTADO_ACTIVO
        );

        $horario_exclusion = $this->horario_exclusion_model->search_array($params_horario_exclusion);

        $id_horario_excluidos = array();
        for ($j = 0; $j < sizeof($horario_exclusion); $j++) {

            if ($horario_exclusion[$j]["tipo"] == 0) {
                $type = 0;
            } else {
                $type = 1;
            }
        }

        $fecha_data = $this->reserva_solicitud_model->total_records($params_fecha);

        if (sizeof($horario_exclusion) == 0){
            $horarios = $this->horarios_disponibles($fecha_data,$fecha,$local,$dia,false);

        }else{
            if ($type == 1){
                $horarios = $this->horarios_disponibles($fecha_data,$fecha,$local,$dia,true);
            }
        }

        return $horarios;
    }

    public function obtener_listado_ubicacion($id_local)
    {

        $parametros_ubicacion = array(
            "select" => "tbl_local_ubicacion.id as id , tbl_local_ubicacion.nombre as nombre",
            "join" => array(
                "tbl_local local, local.id = tbl_local_ubicacion.id_local"
            ),
            "where" => "tbl_local_ubicacion.estado = '" . ESTADO_ACTIVO . "' 
                    and tbl_local_ubicacion.id_local = " . $id_local
        );


        $ubicacion = $this->local_ubicacion_model->search_array($parametros_ubicacion);

        return $ubicacion;
    }

    public function update_reserva()
    {

        $fecha_confirmacion = null;

        $id_reserva = $this->post("id_reserva", TRUE);
        $ubicacion = $this->post("ubicacion", TRUE);
        $estado = $this->post("estado", TRUE);

        $parametros_reserva = array(
            'id_local_ubicacion' => $ubicacion,
            'estado' => $estado,
            'fecha_modificacion' => date("Y-m-d H:i:s")
        );

        $parametro_estado = array();
        if($estado == STATUS_ACCEPTED){
           $parametro_estado = array("fecha_aceptado"=>date("Y-m-d H:i:s"));
        }elseif ($estado == STATUS_DENIED){
            $parametro_estado = array("fecha_rechazado"=>date("Y-m-d H:i:s"));
        }elseif ($estado == STATUS_ASSISTED){
            $parametro_estado = array("fecha_confirmacion"=>date("Y-m-d H:i:s"));
        }

        $parametros = array_merge($parametros_reserva,$parametro_estado);

        $reserva = $this->reserva_solicitud_model->update($id_reserva, $parametros);

        return $reserva;

    }

    public function update_horario_reserva()
    {

        $fecha_confirmacion = null;
        $id_local = $this->post("id_local", TRUE);
        $id_reserva = $this->post("id_reserva", TRUE);
        $id_horario = $this->post("id_horario", TRUE);
        $fecha = $this->post("fecha", TRUE);
        $hora = $this->post("hora", TRUE);
        $ubicacion = $this->post("ubicacion", TRUE);
        $cantidad = $this->post("cantidad", TRUE);
        $estado = $this->post("estado", TRUE);

        $hora  = date("H:i:s", strtotime($hora));



        if ($id_local != "" || $id_local!= null){
            $parametros_reserva = array(
                'id_local'=> $id_local,
                'id_local_ubicacion' => $ubicacion,
                'id_horario' => $id_horario,
                'hora' => $hora,
                'fecha' => $fecha,
                'cantidad' => $cantidad,
                'estado' => $estado,
                'fecha_modificacion' => date("Y-m-d H:i:s"),
            );
        }else{
            $parametros_reserva = array(
                'id_local_ubicacion' => $ubicacion,
                'id_horario' => $id_horario,
                'hora' => $hora,
                'fecha' => $fecha,
                'cantidad' => $cantidad,
                'estado' => $estado,
                'fecha_modificacion' => date("Y-m-d H:i:s"),
            );
        }

        $parametro_estado = array();
        if($estado == STATUS_ACCEPTED){
            $parametro_estado = array("fecha_aceptado"=>date("Y-m-d H:i:s"));
        }elseif ($estado == STATUS_DENIED){
            $parametro_estado = array("fecha_rechazado"=>date("Y-m-d H:i:s"));
        }elseif ($estado == STATUS_ASSISTED){
            $parametro_estado = array("fecha_confirmacion"=>date("Y-m-d H:i:s"));
        }

        $parametros = array_merge($parametros_reserva,$parametro_estado);

        $reserva = $this->reserva_solicitud_model->update($id_reserva, $parametros);

        return $reserva;

    }

    public function horarios_disponibles($fecha_data,$fecha,$local,$dia,$type){

        if ($fecha_data > 0) {

            $reserva_fecha_data = $this->obtener_horarios_solicitud_reserva_fecha($fecha , $local);
            $horarios_excluidos = $this->obtener_horarios_reserva_local($type,$fecha,$local,$dia,true);

            $horario_total = array_merge($reserva_fecha_data,$horarios_excluidos);

            if ($type){

                $horarios_excluidos_total = $this->obtener_horarios_excluidos_fecha($fecha,$local,true);
                $horarios_excluidos_reserva = $this->obtener_horarios_excluidos_reserva($fecha,$local);

                $horario_total = array_merge($horario_total, $horarios_excluidos_total);
                $horario_total = array_merge($horario_total, $horarios_excluidos_reserva);
            }

           // $horarios[] = array();
            $lista_horas = array();

            for ($i = 0; $i < sizeof($horario_total); $i++) {
             //   if ($horario_total[$i]["stock_final"] > 0) {
                    $lista_horas[$i] = array('lista_horas'=>$this->create_time_range($horario_total[$i]["hora_inicio"],$horario_total[$i]["hora_fin"],'30 mins'));
                    $horarios[$i] = array_merge($horario_total[$i], $lista_horas[$i]);

//                }
            }

        } else {

            $horarios = $this->obtener_horarios_reserva_local($type,$fecha,$local,$dia,false);

            if ($type){
                $horarios_excluidos = $this->obtener_horarios_excluidos_fecha($fecha,$local,false);
                $horarios = array_merge($horarios, $horarios_excluidos);
            }

            $horarios_total = array();

            for ($i = 0; $i < sizeof($horarios); $i++) {
                $lista_horas[$i] = array('lista_horas'=>$this->create_time_range($horarios[$i]["hora_inicio"],$horarios[$i]["hora_fin"],'30 mins'));
                $horarios_total[$i] = array_merge($horarios[$i], $lista_horas[$i]);
            }
            $horarios = $horarios_total;
        }
        return $horarios;
    }

    public function obtener_horarios_solicitud_reserva_fecha($fecha , $local){

        $where = " and tbl_reserva_solicitud.id_horario not in  (select id_horario from tbl_horario_exclusion 
                        where fecha ='$fecha' and estado = 1)";

        $obtener_fecha_reserva = array(
            "select" => "tbl_reserva_solicitud.id_horario,horario.hora_inicio,horario.hora_fin,
                horario.stock as stock_total,horario.stock - sum(tbl_reserva_solicitud.cantidad) as stock_final",

            "join" => array(
                "tbl_horario_reserva horario, horario.id =  tbl_reserva_solicitud.id_horario",
                "tbl_local local, local.id = tbl_reserva_solicitud.id_local",
            ),

            "where" => "tbl_reserva_solicitud.fecha = '$fecha' 
                        and tbl_reserva_solicitud.id_local = " . $local . " 
                        and local.estado = " . ESTADO_ACTIVO . "
                        and horario.estado = " . ESTADO_ACTIVO . "
                        and tbl_reserva_solicitud.estado != " . ESTADO_RECHAZADO . $where,
            "group" => "tbl_reserva_solicitud.id_horario"

        );

        $reserva_fecha_data = $this->reserva_solicitud_model->search_array($obtener_fecha_reserva);

        return $reserva_fecha_data;
    }

    public function obtener_horarios_reserva_local($type,$fecha,$local,$dia,$type_date){
        $where = "";
        $where_horario = "";

        if ($type){
            $where = " and tbl_horario_reserva.id not in  (select id_horario from tbl_horario_exclusion 
                        where fecha ='$fecha' and estado = 1)";
        }
        if ($type_date){
            $where_horario = " and tbl_horario_reserva.id not in  (select id_horario from tbl_reserva_solicitud
                        where fecha ='$fecha' and estado != 2  and id_local = ".$local.")";
        }


        $obtener_fecha_reserva = array(
            "select" => "tbl_horario_reserva.id as id_horario,tbl_horario_reserva.hora_inicio,tbl_horario_reserva.hora_fin,tbl_horario_reserva.stock as stock_total,tbl_horario_reserva.stock  as stock_final",

            "join" => array(
                "tbl_local local, local.id = tbl_horario_reserva.id_local",


            ), "where" => "tbl_horario_reserva.id_local = " . $local . " 
                        and local.estado = " . ESTADO_ACTIVO . "
                        and tbl_horario_reserva.dia = " . $dia . "
                        and tbl_horario_reserva.estado = " . ESTADO_ACTIVO  . $where_horario .$where,
        );

        $horarios = $this->horario_reserva_model->search_array($obtener_fecha_reserva);

        return $horarios;
    }

    public function obtener_horarios_excluidos_fecha($fecha,$local,$type_date)
    {

        $where_horario="";

        if ($type_date){
            $where_horario = " and horario_reserva.id not in  (select id_horario from tbl_reserva_solicitud
                        where fecha ='$fecha' and estado != 0  and estado != 2 and id_local = ".$local.")";
        }

        $obtener_fecha_reserva_excluido = array(
            "select" => "tbl_horario_exclusion.id_horario as id_horario,tbl_horario_exclusion.hora_inicio as hora_inicio,
                tbl_horario_exclusion.hora_fin as hora_fin,horario_reserva.stock as stock_total,horario_reserva.stock  as stock_final",

            "join" => array(
                "tbl_local local, local.id = tbl_horario_exclusion.id_local",
                "tbl_horario_reserva horario_reserva, tbl_horario_exclusion.id_horario = horario_reserva.id"

            ), "where" => "tbl_horario_exclusion.estado = " . ESTADO_ACTIVO . " 
                        and tbl_horario_exclusion.fecha = '$fecha '
                        and tbl_horario_exclusion.tipo = 1 
                        and horario_reserva.estado = " . ESTADO_ACTIVO . " 
                        and horario_reserva.id_local = ".$local." 
                        and local.estado = " . ESTADO_ACTIVO . $where_horario
        );


        $horarios_excluidos = $this->horario_exclusion_model->search_array($obtener_fecha_reserva_excluido);
        return $horarios_excluidos;
    }

    public function create_time_range($start, $end, $by='30 mins') {

        $start_time = strtotime($start);
        $end_time   = strtotime($end);

        $current    = time();
        $add_time   = strtotime('+'.$by, $current);
        $diff       = $add_time-$current;

        $times = array();
        while ($start_time < $end_time) {
            $times[] = $start_time;
            $start_time += $diff;
        }
        $times[] = $start_time;

        foreach ($times as $key => $time) {
            $times[$key] = date('g:i a', $time);
        }
        return $times;
    }

    public function obtener_horarios_excluidos_reserva($fecha,$local){


        $where = " and tbl_reserva_solicitud.id_horario in (select id_horario from tbl_horario_exclusion 
                        where fecha ='$fecha' and estado = 1)";


        $obtener_fecha_reserva = array(
            "select" => "tbl_reserva_solicitud.id_horario,exclusion.hora_inicio,exclusion.hora_fin,
                horario.stock as stock_total,horario.stock - sum(tbl_reserva_solicitud.cantidad) as stock_final",

            "join" => array(
                "tbl_horario_reserva horario, horario.id =  tbl_reserva_solicitud.id_horario",
                "tbl_local local, local.id = tbl_reserva_solicitud.id_local",
                "tbl_horario_exclusion exclusion, exclusion.id_horario =  horario.id"
            ),

            "where" => "tbl_reserva_solicitud.fecha = '$fecha' 
                        and tbl_reserva_solicitud.id_local = " . $local . " 
                        and local.estado = " . ESTADO_ACTIVO . "
                        and horario.estado = " . ESTADO_ACTIVO . "
                        and tbl_reserva_solicitud.estado != " . ESTADO_RECHAZADO . "
                        and tbl_reserva_solicitud.estado != " . ESTADO_INACTIVO . $where,
            "group" => "tbl_reserva_solicitud.id_horario"

        );

        $horarios = $this->reserva_solicitud_model->search_array($obtener_fecha_reserva);

        return $horarios;

    }

    public function enviar_correo_reserva_estado($id_reserva,$estado){

        $data = $this->obtener_datos_reserva_mailing($id_reserva);
        $correo = $data["correo"];
            if ($estado == "1"){
                $type = "3";
            }else {
                $type = "4";
            }

        $envio = enviar_correo_usuario($correo,$type,$data,"");

        $data_send = array(
            "datos"=>$data,
            "envio"=>$envio
        );

        return $data_send;
    }

    public function enviar_correo_reserva_notificacion($id_reserva){

        $data = $this->obtener_datos_reserva_mailing($id_reserva);
        $correo = $data["correo"];
        $type = "5";

        $envio = enviar_correo_usuario($correo,$type,$data,"");

        return $envio;
    }

    public function obtener_datos_reserva_mailing($id_reserva)
    {

        $reserva = $this->detalle_reserva($id_reserva);

        $local = $reserva->id_local;
        $ubicacion = $reserva->nombre_local_ubicacion;
        $fecha = $reserva->fecha;
        $cantidad = $reserva->cantidad;
        $hora = $reserva->hora;
        $id_usuario = $reserva->id_usuario;
        $reserva_estado = $reserva->estado;

        $estado = "";
        if ($reserva_estado == ESTADO_PENDIENTE) {
            $estado = "Pendiente";
        } else {
            if ($reserva_estado == ESTADO_ACEPTADO) {
                $estado = "Aceptado";
            } else {
                if ($reserva_estado == ESTADO_RECHAZADO) {
                    $estado = "Rechazado";
                } else {
                    $estado = "Confirmado";
                }
            }
        }

        $time_start= DateTime::createFromFormat( 'H:i:s', $hora);
        $hora = $time_start->format( 'h:i A');

        $usuario = $this->buscar_usuario($id_usuario);
        $correo = $usuario->correo;
        $nombre = $usuario->nombre;

        $local_datos = $this->local_model->buscar_local($local);

        $data = array(
            'nombre'=>$nombre,
            'local_id'=>$local,
            'local'=>$local_datos->nombre,
            'local_correo'=>$local_datos->correo,
            'local_telefono'=>$local_datos->telefono,
            'fecha'=>$fecha,
            'hora'=>$hora,
            'cantidad'=>$cantidad,
            'ambiente'=>$ubicacion,
            'estado'=>$estado,
            'correo'=>$correo
        );

        return $data;
    }

    public function buscar_usuario($id_usuario)
    {
        $parametros_listado_usuario = array(
            "select" => "tbl_usuario.correo,persona.nombre",
            "join" => array("tbl_persona persona, persona.id = tbl_usuario.id_persona"),
            "where" => "tbl_usuario.estado = " . ESTADO_ACTIVO . "
            and tbl_usuario.id = $id_usuario"
        );

        $usuario = $this->usuario_model->get_search_row($parametros_listado_usuario);
        return $usuario;
    }

    public function obtener_excel_reservas($id_empresa)
    {


        $nombre = $this->post("nombre", TRUE);
        $apellido = $this->post("apellido", TRUE);
        $dni = $this->post("dni", TRUE);
        $dia = $this->post("dia", TRUE);
        $local = $this->post("id_local", TRUE);
        $fecha_actual = $this->post("fecha_actual", TRUE);
        $fecha_inicio= $this->post("fecha_inicio", TRUE);
        $fecha_fin = $this->post("fecha_fin", TRUE);
        $estado = $this->post("estado", TRUE);

        $filtro_nombre = "";
        $filtro_apellido = "";
        $filtro_dni = "";
        $filtro_dia = "";
        $filtro_local = "";
        $filtro_fecha_actual = "";
        $filtro_fecha_inicio = "";
        $filtro_fecha_fin = "";
        $filtro_estado = "";


        if ($nombre != "") {
            $filtro_nombre = " and persona.nombre like '%$nombre%'";
        }
        if ($apellido != "") {
            $filtro_apellido = " and persona.apellido like '%$apellido%'";
        }
        if ($dni != "" && is_numeric($dni)) {
            $filtro_dni = " and persona.dni like '%$dni%'";
        }
        if ($dia != "") {
            $filtro_dia = " and horario.dia = '$dia'";
        }
        if ($local != "") {
            $filtro_local = " and tbl_reserva_solicitud.id_local = '$local'";
        }
        if ($estado != "" && is_numeric($estado)) {
            $filtro_estado = " and tbl_reserva_solicitud.estado = $estado";
        }

        if ($fecha_inicio != "" ) {
            list($year, $month, $day) = explode('-', $fecha_inicio);
            $filtro_fecha_inicio = " and tbl_reserva_solicitud.fecha >= '$year-$month-$day' ";
        }

        if ($fecha_fin != "" ) {
            list($year, $month, $day) = explode('-', $fecha_fin);
            $filtro_fecha_fin = " and tbl_reserva_solicitud.fecha  <= '$year-$month-$day' ";
        }

        if($fecha_actual != "" || $fecha_actual != null ){
            $filtro_fecha_actual = " and tbl_reserva_solicitud.fecha = '".$fecha_actual."'";
        }

        $parametros_listado_reserva = array(
            "select" => "tbl_reserva_solicitud.id as id,
            local.nombre  as local_nombre,
            persona.nombre as nombre,
            persona.apellido as apellido,
            persona.dni as dni,
            tbl_reserva_solicitud.cantidad as cantidad,
            tbl_reserva_solicitud.hora as hora,
            horario.dia as dia,
            tbl_reserva_solicitud.estado as estado,
            tbl_reserva_solicitud.fecha as fecha",

            "join" => array(
                "tbl_local_ubicacion ubicacion, ubicacion.id =  tbl_reserva_solicitud.id_local_ubicacion", "tbl_usuario usuario, usuario.id = tbl_reserva_solicitud.id_usuario", "tbl_persona persona, persona.id = usuario.id_persona", "tbl_local local, local.id= tbl_reserva_solicitud.id_local", "tbl_horario_reserva horario, horario.id = tbl_reserva_solicitud.id_horario"
            ), "where" => "usuario.estado = " . ESTADO_ACTIVO . "
            and local.id_empresa = $id_empresa
            and persona.estado = " . ESTADO_ACTIVO . $filtro_nombre . $filtro_apellido . $filtro_dni . $filtro_dia . $filtro_local . $filtro_estado  . $filtro_fecha_inicio . $filtro_fecha_fin . $filtro_fecha_actual,
            "order" => "tbl_reserva_solicitud.fecha_registro desc"
        );


        $datos_reservas = $this->reserva_solicitud_model->search($parametros_listado_reserva);

        return $datos_reservas;
    }

}