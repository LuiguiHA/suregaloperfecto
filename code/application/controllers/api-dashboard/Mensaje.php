<?php
/**
 * Created by PhpStorm.
 * User: Mediabyte
 * Date: 06/05/2016
 * Time: 03:33 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
require APPPATH . "/libraries/Uuid.php";

class Mensaje extends REST_Controller
{
    public $id_empresa;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('utils_helper');
        $this->load->model("usuario_model", "usuario_model");
        $this->load->model("beneficio_model", "beneficio_model");
        $this->load->model("promocion_model", "promocion_model");
        $this->load->model("carta_model", "carta_model");
        $this->load->model("usuario_dispositivo_model", "usuario_dispositivo_model");
        $this->load->model("mensaje_model", "mensaje_model");
        $this->load->model("envio_email_model", "envio_email_model");
        $this->load->model("envio_push_model", "envio_push_model");
        $this->load->model("detalle_envio_email_model", "detalle_envio_email_model");
        $this->load->model("estado_envio_email_model", "estado_envio_email_model");
        $this->load->model("envio_push_model", "envio_push_model");
        $this->load->model("detalle_envio_push_model", "detalle_envio_push_model");
        $this->load->model("estado_envio_push_model", "estado_envio_push_model");
        $this->load->model("token_dashboard_model", "token_dashboard_model");
        $this->lang->load('dashboard_error_lang', 'spanish');
        $this->lang->load('dashboard_mensajes_lang', 'spanish');
        $this->id_empresa = $this->config->item('empresa');
    }

    public function obtener_mensajes_post($paginacion = 1)
    {

        $accion_realizada = ACCION_DASHBOARD_MENSAJE_LISTADO;
        $nombre_metodo = NOMBRE_METODO_POST;
        $id_local = $this->post("id_local", TRUE);
        $token_recibido = $this->getHeaderRC('token');

        $operacion = "Listado mensajes";
        $usuario = "";


        $parametros = array(
            "token" => $token_recibido,
            "id_local" => $id_local,
        );

        $id_empresa = $this->id_empresa;
        $objeto_token = validar_token_seguridad_dashboard($token_recibido,$this->id_empresa);

        if($objeto_token!="" && isset($objeto_token)){
                $datos_mensajes = $this->obtener_listado_mensajes($id_empresa,$id_local, $paginacion);
               $usuario = $objeto_token->usuario;
                if (isset($datos_mensajes)) {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_OK,
                        "total_mensajes" => $datos_mensajes["total_mensajes"],
                        "mensajes" => $datos_mensajes["mensajes"]
                    );

                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_obtener_mensajes')
                    );
                }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_token_valido')
            );
        }

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo,$token_recibido,$usuario,$operacion);
        $this->response($resultado);
    }

    public function filtros_mensajes_post($paginacion = 1){

        $accion_realizada = ACCION_DASHBOARD_MENSAJE_BUSQUEDA;
        $nombre_metodo = NOMBRE_METODO_POST;
        $id_local = $this->post("id_local", TRUE);
        $titulo = $this->post("titulo", TRUE);
        $mensaje = $this->post("mensaje", TRUE);
        $tipo_envio = $this->post("tipo_envio", TRUE);
        $tipo_mensaje = $this->post("tipo_mensaje", TRUE);
        $estado = $this->post("estado", TRUE);

        $token_recibido = $this->getHeaderRC('token');

        $operacion = "busqueda mensajes";
        $usuario = "";

        $parametros = array(
            "token" => $token_recibido,
            "id_local" => $id_local,
            "titulo" => $titulo,
            "mensaje" => $mensaje,
            "tipo_envio" => $tipo_envio,
            "tipo_mensaje" => $tipo_mensaje,
            "estado" => $estado
        );

        $id_empresa = $this->id_empresa;
       $objeto_token = validar_token_seguridad_dashboard($token_recibido,$this->id_empresa);

       if($objeto_token!="" && isset($objeto_token)){
            $datos_mensajes = $this->obtener_filtro_mensajes($id_empresa,$id_local, $paginacion,$titulo,$mensaje,$tipo_envio,$tipo_mensaje,$estado);
            $usuario = $objeto_token->usuario;
            if (isset($datos_mensajes)) {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_OK,
                    "total_mensajes" => $datos_mensajes["total_mensajes"],
                    "mensajes" => $datos_mensajes["mensajes"]
                );

            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_obtener_mensajes')
                );
            }
       } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_token_valido')
            );
        }

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo,$token_recibido,$usuario,$operacion);
        $this->response($resultado);

    }

    public function registrar_post()
    {

        $accion_realizada = ACCION_DASHBOARD_MENSAJE_REGISTRAR;
        $nombre_metodo = NOMBRE_METODO_POST;
        $token = $this->getHeaderRC('token');
        $id_local = $this->post("id_local", TRUE);
        $id_empresa =   $this->id_empresa;

        $notificacion_titulo = $this->post("notificacion_titulo", TRUE);
        $notificacion_mensaje = $this->post("notificacion_mensaje", TRUE);
        $notificacion_alias= $this->post("notificacion_alias", TRUE);
        $email_notification = $this->post("email_notification", TRUE);
        $push_notification = $this->post("push_notification", TRUE);
        $notificacion_tipo_envio = $this->post("notificacion_tipo_envio", TRUE);
        $notificacion_tipo_envio_nombre = $this->post("notificacion_tipo_envio_nombre", TRUE);
        $notificacion_clientes = $this->post("notificacion_clientes", TRUE);

        $usuario  = "";
        $operacion = "registrar mensaje";

        $parametros = array(
            "token" => $token,
            "id_local" => $id_local,
            'notificacion_titulo'=> $notificacion_titulo,
            'notificacion_mensaje' => $notificacion_mensaje,
            'notificacion_alias' => $notificacion_alias,
            'notificacion_email'=>$email_notification,
            'notificacion_push'=>$push_notification,
            'notificacion_tipo_envio' => $notificacion_tipo_envio,
            'notificacion_tipo_envio_nombre' => $notificacion_tipo_envio_nombre,
            'notificacion_clientes' => $notificacion_clientes
        );


        $objeto_token = validar_token_seguridad_dashboard($token,$this->id_empresa);

       if ($objeto_token!="" && isset($objeto_token)) {
           $usuario = $objeto_token->usuario;
            $email = "0";
           $config_email_json="";
           $config_push_json="";
            if ($email_notification != "" && sizeof($email_notification) > 0){
                $email = TYPE_NOTIFICATION_EMAIL_ACTIVE;
                $config_email = $this->obtener_configuracion($email_notification,true);
                $config_email_json = $config_email['config'] ;
                $adjunto = $config_email['adjunto'] ;
            }

           $push = "0";
           if ($push_notification != "" && sizeof($push_notification) > 0){
               $push = TYPE_NOTIFICATION_PUSH_ACTIVE;
               $config_push = $this->obtener_configuracion($push_notification,false);
               $config_push_json = $config_push['config'] ;
               $parametros_tipo = $config_push['parametros_tipo'] ;
               $tipo_mensaje = $config_push['tipo_mensaje'] ;
           }

            $id_mensaje = $this->registrar_notificacion($id_local,$notificacion_tipo_envio,$notificacion_alias,$notificacion_titulo,$notificacion_mensaje,$email,$push,$config_email_json,$config_push_json);

            if (isset($id_mensaje)) {
                if ($email == TYPE_NOTIFICATION_EMAIL_ACTIVE) {
                    $result_email = $this->enviar_email($id_mensaje,$notificacion_titulo,$notificacion_mensaje,$config_email_json,$notificacion_tipo_envio,$id_empresa,$adjunto,$notificacion_clientes);
                }

                if ($push == TYPE_NOTIFICATION_PUSH_ACTIVE) {
                    $result_push = $this->enviar_push($id_mensaje,$notificacion_titulo,$notificacion_mensaje,$config_push_json,$tipo_mensaje,$parametros_tipo,$notificacion_tipo_envio,$id_empresa,$notificacion_clientes);
                }

                if (isset($result_email) && isset($result_push)){
                    if (($result_email['resultado']== ESTADO_RESPUESTA_OK && $result_push['resultado']== ESTADO_RESPUESTA_OK) || ($result_email['resultado']== ESTADO_RESPUESTA_ERROR && $result_push['resultado']== ESTADO_RESPUESTA_ERROR)){
                        $mensaje_push = $result_push['push'];
                        $result_email['push']=$mensaje_push;
                        $resultado = $result_email;
                    }else{
                        if ($result_email['resultado'] == ESTADO_RESPUESTA_OK){
                            $mensaje_push = $result_push['push'];
                            $result_email['push']=$mensaje_push;
                            $resultado = $result_email;
                        }else{
                            $mensaje_email = $result_email['push'];
                            $result_push['push']=$mensaje_email;
                            $resultado = $result_push;
                        }
                    }
                }else{
                    if (isset($result_email)){
                        $resultado = $result_email;
                    }else{
                        $resultado = $result_push;
                    }
                }
            }else{
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_register_message')
                );
            }
        }else{
             $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => "error token"
            );
        }
        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo,$token, $usuario,$operacion);
        $this->response($resultado);
    }

    public function general_excel_post(){

        $accion_realizada = ACCION_DASHBOARD_MENSAJE_GENERAR_EXCEL;
        $nombre_metodo = NOMBRE_METODO_POST;
        $id_local = $this->post("id_local", TRUE);
        $titulo = $this->post("titulo", TRUE);
        $mensaje = $this->post("mensaje", TRUE);
        $tipo_envio = $this->post("tipo_envio", TRUE);
        $tipo_mensaje = $this->post("tipo_mensaje", TRUE);
        $estado = $this->post("estado", TRUE);

        $token_recibido = $this->getHeaderRC('token');

        $operacion = "generar excel mensajes";
        $usuario = "";

        $parametros = array(
            "token" => $token_recibido,
            "id_local" => $id_local,
            "titulo" => $titulo,
            "mensaje" => $mensaje,
            "tipo_envio" => $tipo_envio,
            "tipo_mensaje" => $tipo_mensaje,
            "estado" => $estado
        );

        $id_empresa = $this->id_empresa;
        $objeto_token = validar_token_seguridad_dashboard($token_recibido,$this->id_empresa);

        if($objeto_token!="" && isset($objeto_token)){
            $datos_mensajes = $this->generar_excel_mensajes($id_empresa,$id_local,$titulo,$mensaje,$tipo_envio,$tipo_mensaje,$estado);
            $usuario = $objeto_token->usuario;
            if (isset($datos_mensajes)) {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_OK,
                    "mensajes" => $datos_mensajes
                );

            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_obtener_mensajes')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR, "mensaje" => $this->lang->line('error_token_valido')
            );
        }

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo,$token_recibido,$usuario,$operacion);
        $this->response($resultado);

    }

    public function estadistica_mensaje_post(){

        $accion_realizada = ACCION_DASHBOARD_MENSAJE_ESTADISTICA;
        $nombre_metodo = NOMBRE_METODO_POST;
        $token = $this->getHeaderRC('token');
        $id_mensaje = $this->post("id_mensaje", TRUE);
        $id_local = $this->post("id_local", TRUE);
        $id_empresa =   $this->id_empresa;

        $usuario  = "";
        $operacion = "estadistica mensaje";

        $parametros = array(
            "token" => $token,
            "id_local" => $id_local,
            "id_mensaje" => $id_mensaje,
        );

      $objeto_token = validar_token_seguridad_dashboard($token,$this->id_empresa);

       if ($objeto_token!="" && isset($objeto_token)) {

        $mensaje= $this->obtener_detalle_mensaje($id_mensaje,$id_local);

        if (isset($mensaje)){
            $obtener_estados = true;
            $mensaje_procesado_terminado = true;
            if ($mensaje->es_email == TYPE_NOTIFICATION_EMAIL){


               $mensaje_procesado = $this->obtener_estados_email($id_mensaje,$id_local);

               if ($mensaje_procesado != null){

                   $email_fallidos = $this->obtener_estados_email_pie($id_mensaje, $id_local, VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_FAILED);
                   $email_entregados = $this->obtener_estados_email_pie($id_mensaje, $id_local, VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_DELIVERED);

                   $pieChart = array(
                       array(VALUE_EVENT_SENDGRID_TYPE_STATUS_DELIVERED,VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_DELIVERED,$email_entregados),
                       array(VALUE_EVENT_SENDGRID_TYPE_STATUS_FAILED,VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_FAILED,$email_fallidos)
                   );

                   $email_click = $this->obtener_estados_email_bar($id_mensaje, $id_local, VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_CLICK);
                   $email_abiertos = $this->obtener_estados_email_bar($id_mensaje, $id_local, VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_OPEN);

                   $fechas_abiertos = $this->obtener_estado_abiertos_fecha($id_mensaje, $id_local);

                   $email_pendientes = array();
                   for ($i = 0 ;  $i<sizeof($fechas_abiertos);$i++){
                       $pendientes =  $this->obtener_estado_pendiente_email($id_mensaje, $id_local,$email_entregados,$fechas_abiertos[$i]["fecha"]);
                       $email_pendientes[$i] = $pendientes;
                       $email_entregados = $pendientes->total_mensaje;
                   }

                   $barChart = array(
                       $email_click,
                       $email_abiertos,
                       $email_pendientes
                   );
               }else{
                   $mensaje_procesado_terminado = false;
               }
            }

            if ($mensaje->es_push == TYPE_NOTIFICATION_PUSH_ACTIVE) {

                $mensaje_procesado = $this->obtener_estados_push($id_mensaje,$id_local);

                if ($mensaje_procesado != null) {

                    $push_fallidos = $this->obtener_estados_push_pie($id_mensaje, $id_local, CODE_ERROR_SEND_PUSH);
                    $push_entregados = $this->obtener_estados_push_pie($id_mensaje, $id_local, CODE_SUCCESS_SEND_PUSH);

                    $pieChartPush = array(
                        array(SUCCESS_SEND_PUSH, VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_DELIVERED, $push_entregados),
                        array(ERROR_SEND_PUSH, VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_FAILED, $push_fallidos)
                    );

                    $push_abiertos = $this->obtener_estados_push_bar($id_mensaje, $id_local, CODE_SEEN_SEND_PUSH);

                    $fechas_abiertos = $this->obtener_estado_abiertos_fecha_push($id_mensaje, $id_local);

                    $push_pendientes = array();
                    for ($i = 0 ;  $i<sizeof($fechas_abiertos);$i++){
                        $pendientes =  $this->obtener_estado_pendiente_push($id_mensaje, $id_local,$push_entregados,$fechas_abiertos[$i]["fecha"]);
                        $push_pendientes[$i] = $pendientes;
                        $push_entregados = $pendientes->total_mensaje;
                    }

                    $barChartPush = array(
                        $push_abiertos,
                        $push_pendientes
                    );
                }else{
                    $mensaje_procesado_terminado = false;
                }
            }

            if ($obtener_estados){
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_OK
                );
                if ($mensaje_procesado_terminado){
                    if ($mensaje->es_email == TYPE_NOTIFICATION_EMAIL){
                        $email_resultado = array(
                            'fecha'=>$mensaje->fecha_registro,
                            'pie'=>$pieChart,
                            'bar'=>$barChart);
                        $resultado_email = array("email" => $email_resultado);
                        $resultado = array_merge($resultado,$resultado_email);
                    }
                    if ($mensaje->es_push == TYPE_NOTIFICATION_PUSH_ACTIVE) {
                        $push_resultado = array(
                            'fecha'=>$mensaje->fecha_registro,
                            'pie'=>$pieChartPush,
                            'bar'=>$barChartPush
                        );
                        $reaultado_push = array( "push" => $push_resultado);
                        $resultado = array_merge($resultado,$reaultado_push);
                    }
                }else{
                    $reaultado_mensaje = array("mensaje" => $this->lang->line('error_obtener_detalle_mensaje_procesando'));
                    $resultado = array_merge($resultado,$reaultado_mensaje);
                }

            }else{
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_obtener_estadistica_mensaje')
                );
            }

        }else{
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_obtener_detalle_mensaje')
            );
        }
        }else{
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => "error token"
            );
        }
       guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo,$token, $usuario,$operacion);
        $this->response($resultado);
    }

    private function obtener_configuracion($notification,$type_notification){
        $config = array();
        $adjunto = array();
        $tipo_mensaje = $notification[0][0];
        $tipo_mensaje_nombre = $notification[0][1];
        if ($tipo_mensaje == TYPE_NOTIFICATION_INFORMATIVE){
            if (!$type_notification){
                $parametros_tipo =  "{}";
            }
            $config[0] = [
                'cod_tipo'=>$tipo_mensaje,
                'tipo' =>$tipo_mensaje_nombre
            ];
        }else if ($tipo_mensaje == TYPE_NOTIFICATION_LINKED_APP){
            if ($type_notification){
                $beneficios = array();
                $promociones = array();
                $cartas = array();
                if ($notification[1]!=""){
                    $beneficios = $notification[1][2];
                }
                if ($notification[2]!=""){
                    $promociones = $notification[2][2];
                }
                if ($notification[3]!=""){
                    $cartas = $notification[3][2];
                }
                $config[0] = [
                    'cod_tipo'=>$tipo_mensaje,
                    'tipo' =>$tipo_mensaje_nombre,
                    'beneficios' => $beneficios,
                    'promociones' =>$promociones,
                    'cartas' =>$cartas,
                ];

                $unique = true;

                if (sizeof(array_merge($beneficios,$promociones,$cartas))>1){
                    $unique = false;
                }
                for ($i=0;$i<sizeof($beneficios);$i++){
                    $adjunto["beneficios"][$i] = $this->obtener_detalle_beneficio_notificacion($beneficios[$i],$unique);
                }
                for ($i=0;$i<sizeof($promociones);$i++){
                    $adjunto["promociones"][$i] = $this->obtener_detalle_promocion_notificacion($promociones[$i],$unique);
                }
                for ($i=0;$i<sizeof($cartas);$i++){
                    $adjunto["cartas"][$i] = $this->obtener_detalle_carta_notificacion($cartas[$i],$unique);
                }
            }else{
                $config[0] = [
                    'cod_tipo'=>$tipo_mensaje,
                    'tipo' =>$tipo_mensaje_nombre,
                ];

                $detalle = 0;
                $opcion = 0;

                $adjunto_push = $notification[1];
                $opcion = $adjunto_push[0];
                if ( $adjunto_push[2] != ""){
                    $detalle = $adjunto_push[2];
                }else{
                    $detalle = "0";
                }
                $config_push[0][$adjunto_push[1]] = $detalle;

                $parametros_tipo = array('opcion'=>$opcion,
                                         'detalle'=>$detalle,
                );
                $parametros_tipo = json_encode($parametros_tipo);
            }
        }else if ($tipo_mensaje == TYPE_NOTIFICATION_LINKED_EXTERNAL){
            $url = $notification[1][0];
            $config[0] = [
                'cod_tipo'=>$tipo_mensaje,
                'tipo' =>$tipo_mensaje_nombre,
                'url' =>$url
            ];
            if (true){
                $adjunto["url"] = $url;
            }else{
                $parametros_tipo = array('url'=>$url);
                $parametros_tipo = json_encode($parametros_tipo);
            }

        }
        $config_json = json_encode($config);

        if ($type_notification){
            $config = array(
                'config'=>$config_json,
                'adjunto'=>$adjunto
            );

        }else{
            $config = array(
                'config'=>$config_json,
                'parametros_tipo'=>$parametros_tipo,
                'tipo_mensaje'=>$tipo_mensaje
            );
        }
        return $config;
    }

    private function enviar_email($id_mensaje,$notificacion_titulo,$notificacion_mensaje,$config_email_json,$notificacion_tipo_envio,$id_empresa,$adjunto,$notificacion_clientes){
        $envio_mensaje_email = false;
        $envio_mensaje_push = false;

        $error_detalle_email = false;
        $error_estado_detalle_email = false;
        $id_email = $this->registrar_email($id_mensaje, $notificacion_titulo, $notificacion_mensaje, $config_email_json);
        if (isset($id_email)) {
            if ($notificacion_tipo_envio == TYPE_SEND_MASSIVE) {
                $clientes = $this->obtener_clientes_empresa($id_empresa);
                for ($i = 0; $i < sizeof($clientes); $i++) {

                    $id_detalle_email = $this->registrar_detalle_email($id_email, $clientes[$i]["id"], $notificacion_titulo, $notificacion_mensaje);
                    if (isset($id_detalle_email)){
                        $error_detalle_email =  true;

                        $jsonHeader = json_encode(array("unique_args"=>array("detalle_email_id" => $id_detalle_email)));
                        $arrayHeadersSendgrid = array(
                            "X-SMTPAPI" =>  $jsonHeader
                        );
                        $envio = enviar_correo_mensaje($clientes[$i]["correo"],$clientes[$i]["nombre"],$notificacion_titulo,$notificacion_mensaje, $adjunto,$arrayHeadersSendgrid);
                        if (!$envio){

                        }
                    }else{
                        break;
                    }
                }
            } else {
                $clientes = $notificacion_clientes;
                for ($i = 0; $i < sizeof($clientes); $i++) {
                    $cliente = $this->obtener_cliente($clientes[$i]);
                    $id_detalle_email = $this->registrar_detalle_email($id_email, $cliente->id, $notificacion_titulo, $notificacion_mensaje);
                    if (isset($id_detalle_email)){
                        $error_detalle_email =  true;
                        $jsonHeader = json_encode(array("unique_args"=>array("detalle_email_id" => $id_detalle_email)));
                        $arrayHeadersSendgrid = array(
                            "X-SMTPAPI" =>  $jsonHeader
                        );
                        $envio = enviar_correo_mensaje($cliente->correo,$cliente->nombre,$notificacion_titulo,$notificacion_mensaje,$adjunto,$arrayHeadersSendgrid);

                    }else{
                        break;
                    }
                }
            }

            if ($error_estado_detalle_email || $error_detalle_email){
                $detalle_email_fecha_fin = $this->update_fecha_fin_email($id_email);
                if (isset($detalle_email_fecha_fin)){
                    $envio_mensaje_email =  true;
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_OK,
                        "email" => $this->lang->line('registro_mensaje_email')
                    );
                }else{
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "email" => $this->lang->line('error_register_message_update_date_end')
                    );
                }
            }else{
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "email" => $this->lang->line('error_register_message_detail')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "email" => $this->lang->line('error_register_message_email')
            );
        }

        return $resultado;
    }

    private function enviar_push($id_mensaje,$notificacion_titulo,$notificacion_mensaje,$config_push_json,$notificacion_tipo_push,$parametros_tipo,$notificacion_tipo_envio,$id_empresa,$notificacion_clientes){
        $id_push = $this->registrar_push($id_mensaje, $notificacion_titulo, $notificacion_mensaje, $config_push_json);
        if (isset($id_push)) {

            $parametrosEnvioPush =  [
                "id"=>$id_push,
                "titulo"=>$notificacion_titulo,
                "mensaje"=>$notificacion_mensaje,
                "tipo"=>$notificacion_tipo_push,
                "parametros"=>$parametros_tipo,
            ];
            $parametrosEnvioPush =  json_encode($parametrosEnvioPush);
            if ($notificacion_tipo_envio == TYPE_SEND_MASSIVE) {
                $arrayTokenPushAndroid = array();
                $arrayUsuarioDispositivoAndroid= array();
                $arrayTokenPushIOS = array();
                $arrayUsuarioDispositivoIOS= array();
                $clientes_push = $this->obtener_clientes_empresa_push($id_empresa);
                for ($i=0;$i<sizeof($clientes_push);$i++){
                    if ($clientes_push[$i]->id_aplicacion ==  NOTIFICATION_ANDROID){
                        $arrayTokenPushAndroid[$i] = $clientes_push[$i]->token_push;
                        $arrayUsuarioDispositivoAndroid[$i] = $clientes_push[$i]->id_usuario_dispositivo;
                    }elseif($clientes_push[$i]->id_aplicacion ==  NOTIFICATION_IOS){
                        $arrayTokenPushIOS[$i] = $clientes_push[$i]->token_push;
                        $arrayUsuarioDispositivoIOS[$i] = $clientes_push[$i]->id_usuario_dispositivo;
                    }
                }

                $arrayTokenPushAndroid = array_values($arrayTokenPushAndroid);
                $arrayUsuarioDispositivoAndroid = array_values($arrayUsuarioDispositivoAndroid);
                $arrayTokenPushIOS = array_values($arrayTokenPushIOS);
                $arrayUsuarioDispositivoIOS = array_values($arrayUsuarioDispositivoIOS);
                if (isset($arrayTokenPushAndroid) &&  sizeof($arrayTokenPushAndroid)>0) {
                    $this->envio_push($id_push, $notificacion_titulo, $notificacion_mensaje, $arrayUsuarioDispositivoAndroid, $arrayTokenPushAndroid, $parametrosEnvioPush, true);
                }
                if (isset($arrayTokenPushIOS) &&  sizeof($arrayTokenPushIOS)>0) {
                    $this->envio_push($id_push, $notificacion_titulo, $notificacion_mensaje, $arrayUsuarioDispositivoIOS, $arrayTokenPushIOS, $parametrosEnvioPush, false);
                }
                    $envio_mensaje_push =  true;

                $push_total = array_merge($arrayTokenPushIOS,$arrayTokenPushAndroid);


                if (isset($push_total) &&  sizeof($push_total) <= 0){
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "push" => $this->lang->line('error_usuarios_token_push')
                    );
                }else{
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_OK,
                        "push" => $this->lang->line('registro_mensaje_push')
                    );
                }
            } else {
                $arrayTokenPushAndroid = array();
                $arrayUsuarioDispositivoAndroid= array();
                $arrayTokenPushIOS = array();
                $arrayUsuarioDispositivoIOS= array();
                $clientes = $notificacion_clientes;

                for ($i = 0; $i < sizeof($clientes); $i++) {
                    $cliente_push = $this->obtener_cliente_push($clientes[$i]);

                    for ($j=0;$j<sizeof($cliente_push);$j++){
                        if ($cliente_push[$j]->id_aplicacion ==  NOTIFICATION_ANDROID){
                            $arrayTokenPushAndroid[$i + $j] = $cliente_push[$j]->token_push;
                            $arrayUsuarioDispositivoAndroid[$i + $j] = $cliente_push[$j]->id_usuario_dispositivo;
                        }elseif($cliente_push[$j]->id_aplicacion ==  NOTIFICATION_IOS){
                            $arrayTokenPushIOS[$i + $j] = $cliente_push[$j]->token_push;
                            $arrayUsuarioDispositivoIOS[$i + $j] = $cliente_push[$j]->id_usuario_dispositivo;
                        }
                    }
                }
                $arrayTokenPushAndroid = array_values($arrayTokenPushAndroid);
                $arrayUsuarioDispositivoAndroid = array_values($arrayUsuarioDispositivoAndroid);
                $arrayTokenPushIOS = array_values($arrayTokenPushIOS);
                $arrayUsuarioDispositivoIOS = array_values($arrayUsuarioDispositivoIOS);

                if (isset($arrayTokenPushAndroid) &&  sizeof($arrayTokenPushAndroid)>0){
                    $this->envio_push($id_push,$notificacion_titulo,$notificacion_mensaje,$arrayUsuarioDispositivoAndroid,$arrayTokenPushAndroid,$parametrosEnvioPush,true);
                }

                if (isset($arrayTokenPushIOS) &&  sizeof($arrayTokenPushIOS)>0){
                    $this->envio_push($id_push,$notificacion_titulo,$notificacion_mensaje,$arrayUsuarioDispositivoIOS,$arrayTokenPushIOS,$parametrosEnvioPush,false);
                }


                $push_total = array_merge($arrayTokenPushIOS,$arrayTokenPushAndroid);


                if (isset($push_total) &&  sizeof($push_total) <= 0){
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "push" => $this->lang->line('error_usuarios_token_push')
                        );
                }else{
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_OK,
                        "push" => $this->lang->line('registro_mensaje_push')
                    );
                }
            }

            $this->update_fecha_fin_push($id_push);
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "push" => $this->lang->line('error_register_message_push')
            );
        }

        return $resultado;
    }

    public function registrar_notificacion($id_local,$tipo_envio,$alias,$titulo,$mensaje,$email,$push,$config_email,$config_push){
        $tiempo_actual = date('Y-m-d H:i:s');
        $parametros = array(
            'id_local'=>$id_local,
            'tipo_envio'=>$tipo_envio,
            'alias'=>$alias,
            'titulo'=>$titulo,
            'mensaje'=>$mensaje,
            'es_email'=>$email,
            'es_push'=>$push,
            'configuracion_email'=>$config_email,
            'configuracion_push'=>$config_push,
            'fecha_registro'=>$tiempo_actual,
            'estado'=>ESTADO_ACTIVO,
        );

        $mensaje = $this->mensaje_model->insert($parametros);

        return $mensaje;
    }

    public function registrar_email($id_mensaje,$titulo,$mensaje,$config){
        $tiempo_actual = date('Y-m-d H:i:s');
        $parametros = array(
            'id_mensaje'=>$id_mensaje,
            'titulo'=>$titulo,
            'mensaje'=>$mensaje,
            'fecha_inicio'=>$tiempo_actual,
            'configuracion_envio'=>$config,
            'estado'=>ESTADO_ACTIVO,
        );

        $mensaje_email = $this->envio_email_model->insert($parametros);

        return $mensaje_email;
    }

    public function registrar_push($id_mensaje,$titulo,$mensaje,$config){
        $tiempo_actual = date('Y-m-d H:i:s');
        $parametros = array(
            'id_mensaje'=>$id_mensaje,
            'titulo'=>$titulo,
            'mensaje'=>$mensaje,
            'fecha_inicio'=>$tiempo_actual,
            'configuracion_envio'=>$config,
            'estado'=>ESTADO_ACTIVO,
        );

        $mensaje_push = $this->envio_push_model->insert($parametros);

        return $mensaje_push;
    }

    public function registrar_detalle_email($id_mensaje_envio,$id_usuario,$titulo,$mensaje){
        $tiempo_actual = date('Y-m-d H:i:s');
        $parametros = array(
            'id_mensaje_envio'=>$id_mensaje_envio,
            'id_usuario'=>$id_usuario,
            'titulo'=>$titulo,
            'mensaje'=>$mensaje,
            'fecha_registro'=>$tiempo_actual,
        );

        $detalle_email = $this->detalle_envio_email_model->insert($parametros);

        return $detalle_email;
    }

    public function obtener_clientes_empresa($id_empresa){

        $parametros_listado_usuario = array(
            "select" => "tbl_usuario.id as id,
            persona.nombre as nombre,
            persona.apellido as apellido,
            persona.dni as dni,
            tbl_usuario.correo as correo",

            "join" => array(
                "tbl_persona persona, persona.id = tbl_usuario.id_persona"
            ),
            "where" => "tbl_usuario.estado = " . ESTADO_ACTIVO . "
            and tbl_usuario.id_empresa = $id_empresa
            and persona.estado = " . ESTADO_ACTIVO
        );

        $clientes = $this->usuario_model->search_array($parametros_listado_usuario);

        return $clientes;

    }

    public function obtener_cliente($id_usuario){

        $parametros_listado_usuario = array(
            "select" => "tbl_usuario.id as id,
            persona.nombre as nombre,
            persona.apellido as apellido,
            persona.dni as dni,
            tbl_usuario.correo as correo",

            "join" => array(
                "tbl_persona persona, persona.id = tbl_usuario.id_persona"
            ),
            "where" => "tbl_usuario.estado = " . ESTADO_ACTIVO . "
            and tbl_usuario.id = $id_usuario
            and persona.estado = " . ESTADO_ACTIVO
        );

        $cliente = $this->usuario_model->get_search_row($parametros_listado_usuario);

        return $cliente;
    }

    public function update_fecha_fin_email($id_email){
        $tiempo_actual = date('Y-m-d H:i:s');
        $parametros = array(
            'fecha_fin'=>$tiempo_actual,
        );

        $mensaje_email = $this->envio_email_model->update($id_email,$parametros);

        return $mensaje_email;
    }

    public function update_fecha_fin_push($id_push){
        $tiempo_actual = date('Y-m-d H:i:s');
        $parametros = array(
            'fecha_fin'=>$tiempo_actual,
        );

        $mensaje_push = $this->envio_push_model->update($id_push,$parametros);

        return $mensaje_push;
    }

    public function registrar_estado_envio_email($id_detalle_envio_email,$descripcion,$estado){
        $tiempo_actual = date('Y-m-d H:i:s');
        $parametros = array(
            'id_detalle_envio_email'=>$id_detalle_envio_email,
            'descripcion'=>$descripcion,
            'estado'=>$estado,
            'fecha_registro'=>$tiempo_actual,

        );
        $esado_mensaje_email= $this->estado_envio_email_model->insert($parametros);

        return $esado_mensaje_email;
    }

    public function obtener_cliente_push($id_usuario){

        $parametros_listado_usuario = array(
            "select" => "distinct usuario_push.token_push,
            usuario_dispositivo.id_aplicacion,
            usuario_push.id_usuario_dispositivo",

            "join" => array(
                "tbl_persona persona, persona.id = tbl_usuario.id_persona",
                "tbl_usuario_dispositivo usuario_dispositivo, usuario_dispositivo.id_usuario = tbl_usuario.id",
                "tbl_dispositivo_token_push usuario_push, usuario_push.id_usuario_dispositivo = usuario_dispositivo.id",
            ),
            "where" => "tbl_usuario.estado = " . ESTADO_ACTIVO . "
            and tbl_usuario.id = $id_usuario
            and persona.estado = " . ESTADO_ACTIVO ." 
              and usuario_push.estado = " . ESTADO_ACTIVO ." 
            and usuario_push.estado =".ESTADO_ACTIVO
        );


        $cliente_push = $this->usuario_model->search($parametros_listado_usuario);

        return $cliente_push;
    }

    public function obtener_clientes_empresa_push($id_empresa){

        $parametros_listado_usuario = array(
            "select" => "distinct usuario_push.token_push,
            usuario_push.id_usuario_dispositivo,
            usuario_dispositivo.id_aplicacion",

            "join" => array(
                "tbl_persona persona, persona.id = tbl_usuario.id_persona",
                "tbl_usuario_dispositivo usuario_dispositivo, usuario_dispositivo.id_usuario = tbl_usuario.id",
                "tbl_dispositivo_token_push usuario_push, usuario_push.id_usuario_dispositivo = usuario_dispositivo.id",
            ),
            "where" => "tbl_usuario.estado = " . ESTADO_ACTIVO . "
           and tbl_usuario.id_empresa = $id_empresa
            and persona.estado = " . ESTADO_ACTIVO ." 
             and usuario_push.estado = " . ESTADO_ACTIVO ." 
            and usuario_push.estado =".ESTADO_ACTIVO
        );

        $clientes_push = $this->usuario_model->search($parametros_listado_usuario);

        return $clientes_push;
    }

    public function envio_push($id_push,$notificacion_titulo,$notificacion_mensaje,$arrayUsuarioDispositivo,$arrayTokenPush,$parametrosEnvioPush,$isIOS){
        $objConfiguracion = '{"apiKey":'.API_KEY_FIREBASE.'}';

        $this->procesarNotificacionesAndroid($id_push,$notificacion_titulo,$notificacion_mensaje, $objConfiguracion,$arrayUsuarioDispositivo ,$arrayTokenPush, $parametrosEnvioPush,$isIOS);
    }

    private function procesarNotificacionesAndroid($id_push= "0",$notificacion_titulo,$notificacion_mensaje, $parametrosConfiguracion = "", $arrayUsuarioDispositivo =  array(),$arrayTokenPush = array(), $parametrosEnvioPush,$isIOS)
    {
        $this->load->library("gcm");
        if($parametrosConfiguracion != "")
            $this->gcm->loadConfiguration($parametrosConfiguracion);
        $totalTokenPush = count($arrayTokenPush);
        if( $totalTokenPush > MAX_TOKEN_PUSH_POR_ENVIO_ANDROID)
        {
            $arrayPush = array();
            $arrayDataInsertado = array();
            $indiceTotalEnviado = 0;
            for ($i=0; $i < $totalTokenPush; $i++){
                $arrayPush[] = $arrayTokenPush[$i];
                $arrayDataInsertado[] = array(
                    "id_envio_push" => $id_push,
                    "id_usuario_dispositivo" => $arrayUsuarioDispositivo[$i],
                    "titulo" => $notificacion_titulo,
                    "mensaje" => $notificacion_mensaje,
                    "fecha_registro" => date("Y-m-d H:i:s"),
                );
                $indiceTotalEnviado++;
                if($indiceTotalEnviado == MAX_TOKEN_PUSH_POR_ENVIO_ANDROID)
                {
                    $id_detalle_push = $this->detalle_envio_push_model->insert_lote($arrayDataInsertado);

                    $this->enviarNotificacionAndroid($arrayPush, $parametrosEnvioPush, $id_detalle_push,$isIOS);
                    $indiceTotalEnviado = 0;
                    $arrayPush = array();
                    $arrayDataInsertado = array();

                }
            }

            if(count($arrayPush) > 0)
            {
                $id_detalle_push =  $this->detalle_envio_push_model->insert_lote($arrayDataInsertado);

                $this->enviarNotificacionAndroid($arrayPush, $parametrosEnvioPush, $id_detalle_push,$isIOS);
            }
        }
        else
        {

            $arrayDataInsertado = array();
            for ($i=0; $i < $totalTokenPush; $i++){
                $arrayDataInsertado[] = array(
                    "id_envio_push" => $id_push,
                    "id_usuario_dispositivo" => $arrayUsuarioDispositivo[$i],
                    "titulo" => $notificacion_titulo,
                    "mensaje" => $notificacion_mensaje,
                    "fecha_registro" => date("Y-m-d H:i:s"),
                );
            }
            $id_detalle_push =  $this->detalle_envio_push_model->insert_lote($arrayDataInsertado);

            $this->enviarNotificacionAndroid($arrayTokenPush, $parametrosEnvioPush, $id_detalle_push,$isIOS);
        }

    }

    private function enviarNotificacionAndroid($arrayTokenPush = array(), $jsonParametrosPush="", $id_detalle_push= array(),$isIOS)
    {

        $i=0;
        foreach ($arrayTokenPush as &$tokenPush) {
            $arrayTokenPush[$i] = str_replace(array("\r", "\n", "\n\r", "\n \r"), '', $tokenPush);
            $i++;
            # code...
        }
        $payloadJson = $this->gcm->buildJsonPush($arrayTokenPush, json_decode($jsonParametrosPush),$isIOS);
        $objResultado = $this->gcm->sendMessage($payloadJson);
        //$this->response($objResultado);

        if((isset($objResultado->failure) && $objResultado->failure > 0) || (isset($objResultado->canonical_ids) && $objResultado->canonical_ids > 0) || (isset($objResultado->success) && $objResultado->success > 0))
        {
            if(isset($objResultado->results) && count($objResultado->results) > 0)
            {
                $indice = 0;
                foreach($objResultado->results as $item)
                {

                    $id_detalle  =  $id_detalle_push[$indice];
                    if(isset($item->error))
                    {
                        $arrayDataInsert = array(
                            "id_detalle_envio_push" => $id_detalle,
                            "descripcion" => ERROR_SEND_PUSH,
                            "estado" => CODE_ERROR_SEND_PUSH,
                            "fecha_registro" =>  date("Y-m-d H:i:s"),
                        );
                        $this->estado_envio_push_model->insert($arrayDataInsert);
                    }
                    elseif(isset($item->message_id))
                    {
                        $arrayDataInsert = array(
                            "id_detalle_envio_push" => $id_detalle,
                            "descripcion" => SUCCESS_SEND_PUSH,
                            "estado" => CODE_SUCCESS_SEND_PUSH,
                            "fecha_registro" =>  date("Y-m-d H:i:s"),
                        );
                        $this->estado_envio_push_model->insert($arrayDataInsert);
                    }
                    $indice++;
                }
            }
        }
    }

    public function obtener_detalle_beneficio_notificacion($id_beneficio,$unique)
    {
        $foto_tipo = FOTO_LISTADO;
        if ($unique){
            $foto_tipo = FOTO_DETALLE;
        }

        $parametros_beneficio = array(

            "select" => "tbl_beneficio.id,
                        tbl_beneficio.nombre,
                        tbl_beneficio.descripcion,
                        tbl_beneficio.puntos,
                        tbl_beneficio.tipo,
                        tbl_beneficio.monto_adicional,
                         foto.foto as foto",
            "join" => array(
                "tbl_foto_beneficio foto, foto.id_beneficio = tbl_beneficio.id"
            ),
            "where" => "foto.estado = " . ESTADO_ACTIVO . "
            and tbl_beneficio.estado = " . ESTADO_ACTIVO . "
            and foto.tipo = " . $foto_tipo . "
            and foto.plataforma = " . PLATAFORMA_WEB . "
            and tbl_beneficio.id = $id_beneficio"

        );


        $beneficio = $this->beneficio_model->get_search_row($parametros_beneficio);

        return $beneficio;
    }

    public function obtener_detalle_promocion_notificacion($id_promocion,$unique)
    {
        $foto_tipo = FOTO_LISTADO;
        if ($unique){
            $foto_tipo = FOTO_DETALLE;
        }

        $parametros_promocion = array(

            "select" => "tbl_promocion.id,
                        tbl_promocion.nombre,
                        tbl_promocion.descripcion,
                         foto.foto as foto",
            "join" => array(
                "tbl_foto_promocion foto, foto.id_promocion = tbl_promocion.id"
            ),
            "where" => "foto.estado = " . ESTADO_ACTIVO . "
            and tbl_promocion.estado = " . ESTADO_ACTIVO . "
            and foto.tipo = " . $foto_tipo . "
            and foto.plataforma = " . PLATAFORMA_WEB . "
            and tbl_promocion.id = $id_promocion"

        );


        $promocion = $this->promocion_model->get_search_row($parametros_promocion);

        return $promocion;
    }

    public function obtener_detalle_carta_notificacion($id_carta,$unique)
    {
        $foto_tipo = FOTO_LISTADO;
        if ($unique){
            $foto_tipo = FOTO_DETALLE;
        }

        $parametros_carta = array(

            "select" => "tbl_carta.id,
                        tbl_carta.nombre,
                        tbl_carta.descripcion,
                        tbl_carta.precio,
                         foto.foto as foto",
            "join" => array(
                "tbl_foto_carta foto, foto.id_carta = tbl_carta.id"
            ),
            "where" => "foto.estado = " . ESTADO_ACTIVO . "
            and tbl_carta.estado = " . ESTADO_ACTIVO . "
            and foto.tipo = " . $foto_tipo . "
            and foto.plataforma = " . PLATAFORMA_WEB . "
            and tbl_carta.id = $id_carta"

        );


        $carta = $this->carta_model->get_search_row($parametros_carta);

        return $carta;
    }

    public function obtener_listado_mensajes($id_empresa, $id_local,$paginacion = 1)
    {
        $parametros_listado_mensaje = array(
            "select" => "tbl_mensaje.id as id,
            tbl_mensaje.tipo_envio,
            tbl_mensaje.alias,
            tbl_mensaje.titulo,
            tbl_mensaje.mensaje,
            tbl_mensaje.es_email,
            tbl_mensaje.es_push,
            tbl_mensaje.configuracion_push,
            tbl_mensaje.configuracion_email,
            tbl_mensaje.fecha_registro,
            tbl_mensaje.estado",
            "join" => array(
                "tbl_local local, local.id= tbl_mensaje.id_local"
            ), "where" => "local.id_empresa = $id_empresa
            and tbl_mensaje.id_local = $id_local
            and local.estado = " . ESTADO_ACTIVO,
            "order" => "tbl_mensaje.fecha_registro desc"
        );

        if (is_numeric($paginacion) && $paginacion > 0) {
            $inicio = ELEMENTOS_POR_PAGINA * ($paginacion - 1);

            $mensajes = $this->mensaje_model->search_data_array($parametros_listado_mensaje, $inicio, ELEMENTOS_POR_PAGINA);

        } else {
            $mensajes = $this->mensaje_model->search_array($parametros_listado_mensaje);
        }

        $total_mensajes = $this->mensaje_model->total_records($parametros_listado_mensaje);

        $datos_mensajes = array(
            "total_mensajes" => $total_mensajes, "mensajes" => $mensajes
        );

        return $datos_mensajes;

    }

    public function obtener_filtro_mensajes($id_empresa, $id_local,$paginacion = 1,$titulo,$mensaje,$tipo_envio,$tipo_mensaje,$estado)
    {
        $where = "";

        if ($titulo !=""){
            $where .= " and tbl_mensaje.titulo like '%$titulo%'";
        }

        if ($mensaje !=""){
            $where .= " and tbl_mensaje.mensaje like '%$mensaje%'";
        }

        if ($tipo_envio !=""){
            $where .= " and tbl_mensaje.tipo_envio = $tipo_envio";
        }

        if ($tipo_mensaje !=""){
            if ($tipo_mensaje == "1"){
                $where .= " and tbl_mensaje.es_email  = 1";
            }else{
                $where .= " and tbl_mensaje.es_push  = 1";
            }
        }

        if ($estado !=""){
            $where .= " and tbl_mensaje.estado = $estado";
        }


        $parametros_listado_mensaje = array(
            "select" => "tbl_mensaje.id as id,
            tbl_mensaje.tipo_envio,
            tbl_mensaje.alias,
            tbl_mensaje.titulo,
            tbl_mensaje.mensaje,
            tbl_mensaje.es_email,
            tbl_mensaje.es_push,
            tbl_mensaje.configuracion_push,
            tbl_mensaje.configuracion_email,
            tbl_mensaje.fecha_registro,
            tbl_mensaje.estado",
            "join" => array(
                "tbl_local local, local.id= tbl_mensaje.id_local"
            ), "where" => "local.id_empresa = $id_empresa 
            and tbl_mensaje.id_local = $id_local
            and local.estado = " . ESTADO_ACTIVO . $where ,
            "order" => "tbl_mensaje.fecha_registro desc"
        );

        if (is_numeric($paginacion) && $paginacion > 0) {
            $inicio = ELEMENTOS_POR_PAGINA * ($paginacion - 1);

            $mensajes = $this->mensaje_model->search_data_array($parametros_listado_mensaje, $inicio, ELEMENTOS_POR_PAGINA);

        } else {
            $mensajes = $this->mensaje_model->search_array($parametros_listado_mensaje);
        }

        $total_mensajes = $this->mensaje_model->total_records($parametros_listado_mensaje);

        $datos_mensajes = array(
            "total_mensajes" => $total_mensajes, "mensajes" => $mensajes
        );

        return $datos_mensajes;

    }

    public function generar_excel_mensajes($id_empresa, $id_local,$titulo,$mensaje,$tipo_envio,$tipo_mensaje,$estado)
    {
        $where = "";

        if ($titulo !=""){
            $where .= " and tbl_mensaje.titulo like '%$titulo%'";
        }

        if ($mensaje !=""){
            $where .= " and tbl_mensaje.mensaje like '%$mensaje%'";
        }

        if ($tipo_envio !=""){
            $where .= " and tbl_mensaje.tipo_envio = $tipo_envio";
        }

        if ($tipo_mensaje !=""){
            if ($tipo_mensaje == "1"){
                $where .= " and tbl_mensaje.es_email  = 1";
            }else{
                $where .= " and tbl_mensaje.es_push  = 1";
            }
        }

        if ($estado !=""){
            $where .= " and tbl_mensaje.estado = $estado";
        }


        $parametros_listado_mensaje = array(
            "select" => "tbl_mensaje.id as id,
            tbl_mensaje.tipo_envio,
            tbl_mensaje.alias,
            tbl_mensaje.titulo,
            tbl_mensaje.mensaje,
            tbl_mensaje.es_email,
            tbl_mensaje.es_push,
            tbl_mensaje.configuracion_push,
            tbl_mensaje.configuracion_email,
            tbl_mensaje.fecha_registro,
            tbl_mensaje.estado",
            "join" => array(
                "tbl_local local, local.id= tbl_mensaje.id_local"
            ), "where" => "local.id_empresa = $id_empresa 
            and tbl_mensaje.id_local = $id_local
            and local.estado = " . ESTADO_ACTIVO . $where ,
            "order" => "tbl_mensaje.fecha_registro desc"
        );

        $datos_mensajes = $this->mensaje_model->search($parametros_listado_mensaje);

        return $datos_mensajes;

    }


    public function obtener_detalle_mensaje($id_mensaje,$id_local){
        $parametros_carta = array(

            "select" => "tbl_mensaje.es_email,tbl_mensaje.es_push,DATE_FORMAT(tbl_mensaje.fecha_registro,'%Y-%m-%d') as fecha_registro",
            "join" => array(
                "tbl_local local, local.id = tbl_mensaje.id_local",
            ),
            "where" => "local.estado = " . ESTADO_ACTIVO . "
            and local.id = " . $id_local . "
            and tbl_mensaje.id = $id_mensaje"
        );
        $mensaje = $this->mensaje_model->get_search_row($parametros_carta);

        return $mensaje;
    }

    public function obtener_estados_email($id_mensaje,$id_local){
        $parametros = array(
            "select" => "estado_email.estado",
            "join" => array(
                "tbl_envio_email envio_email, envio_email.id_mensaje = tbl_mensaje.id",
                "tbl_detalle_envio_email detalle_email, detalle_email.id_mensaje_envio = envio_email.id",
                "tbl_estado_envio_email estado_email, estado_email.id_detalle_envio_email = detalle_email.id",
                "tbl_local local, local.id = tbl_mensaje.id_local",
            ),
            "where" => "local.estado = " . ESTADO_ACTIVO . "
            and local.id = " . $id_local . "
            and tbl_mensaje.id = $id_mensaje"

        );

        $estados_mensajes = $this->mensaje_model->search($parametros);
        return $estados_mensajes;
    }


    public function obtener_estados_email_pie($id_mensaje,$id_local,$estado_email){
        $parametros = array(
            "select" => "estado_email.estado",
            "join" => array(
                "tbl_envio_email envio_email, envio_email.id_mensaje = tbl_mensaje.id",
                "tbl_detalle_envio_email detalle_email, detalle_email.id_mensaje_envio = envio_email.id",
                "tbl_estado_envio_email estado_email, estado_email.id_detalle_envio_email = detalle_email.id",
                "tbl_local local, local.id = tbl_mensaje.id_local",
            ),
            "where" => "local.estado = " . ESTADO_ACTIVO . "
            and local.id = " . $id_local . "
            and estado_email.estado = " . $estado_email . "
            and tbl_mensaje.id = $id_mensaje"

        );

        $estados_mensajes = $this->mensaje_model->total_records($parametros);
        return $estados_mensajes;
    }

    public function obtener_estados_email_bar($id_mensaje,$id_local,$estado_email){

        $parametros = array(
            "select" => "estado_email.estado,DATE_FORMAT(estado_email.fecha_registro,'%Y-%m-%d') as fecha,count(estado_email.fecha_registro) as total_mensaje",
            "join" => array(
                "tbl_envio_email envio_email, envio_email.id_mensaje = tbl_mensaje.id",
                "tbl_detalle_envio_email detalle_email, detalle_email.id_mensaje_envio = envio_email.id",
                "tbl_estado_envio_email estado_email, estado_email.id_detalle_envio_email = detalle_email.id",
                "tbl_local local, local.id = tbl_mensaje.id_local",
            ),
            "where" => "local.estado = " . ESTADO_ACTIVO . "
            and local.id = " . $id_local . "
            and estado_email.estado = " . $estado_email . "
            and tbl_mensaje.id = $id_mensaje",
            "group"=>"estado_email.fecha_registro"

        );

        $estados_mensajes = $this->mensaje_model->search_array($parametros);
        return $estados_mensajes;

    }

    public function obtener_estado_abiertos_fecha($id_mensaje,$id_local){

        $parametros_carta = array(
            "select" => "DATE_FORMAT(estado_email.fecha_registro,'%Y-%m-%d') as fecha",
            "join" => array(
                "tbl_envio_email envio_email, envio_email.id_mensaje = tbl_mensaje.id",
                "tbl_detalle_envio_email detalle_email, detalle_email.id_mensaje_envio = envio_email.id",
                "tbl_estado_envio_email estado_email, estado_email.id_detalle_envio_email = detalle_email.id",
                "tbl_local local, local.id = tbl_mensaje.id_local",
            ),
            "where" => "local.estado = " . ESTADO_ACTIVO . "
            and local.id = " . $id_local . "
            and estado_email.estado = ".VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_OPEN."
            and tbl_mensaje.id = $id_mensaje",

        );

        $estados_mensajes = $this->mensaje_model->search_array($parametros_carta);
        return $estados_mensajes;

    }

    public function obtener_estado_pendiente_email($id_mensaje,$id_local,$entregados,$fecha){


        $parametros = array(
            "select" => "DATE_FORMAT(estado_email.fecha_registro,'%Y-%m-%d') as fecha,
            ($entregados - count(estado_email.fecha_registro)) as total_mensaje",
            "join" => array(
                "tbl_envio_email envio_email, envio_email.id_mensaje = tbl_mensaje.id",
                "tbl_detalle_envio_email detalle_email, detalle_email.id_mensaje_envio = envio_email.id",
                "tbl_estado_envio_email estado_email, estado_email.id_detalle_envio_email = detalle_email.id",
                "tbl_local local, local.id = tbl_mensaje.id_local",
            ),
            "where" => "local.estado = " . ESTADO_ACTIVO . "
            and local.id = " . $id_local . "
            and estado_email.estado = ".VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_OPEN."
            and DATE_FORMAT(estado_email.fecha_registro,'%Y-%m-%d') = '$fecha'
            and tbl_mensaje.id = $id_mensaje",
            "group"=>"estado_email.fecha_registro"

        );

        $estados_mensajes = $this->mensaje_model->get_search_row($parametros);
        return $estados_mensajes;

    }

    public function obtener_estados_push($id_mensaje,$id_local){


        $parametros = array(

            "select" => "estado_push.estado",
            "join" => array(
                "tbl_envio_push envio_push, envio_push.id_mensaje = tbl_mensaje.id",
                "tbl_detalle_envio_push detalle_push, detalle_push.id_envio_push = envio_push.id",
                "tbl_estado_envio_push estado_push, estado_push.id_detalle_envio_push = detalle_push.id",
                "tbl_local local, local.id = tbl_mensaje.id_local",
            ),
            "where" => "local.estado = " . ESTADO_ACTIVO . "
            and local.id = " . $id_local . "
            and tbl_mensaje.id = $id_mensaje"

        );

        $estados_mensajes = $this->mensaje_model->search($parametros);

        return $estados_mensajes;
    }

    public function obtener_estados_push_pie($id_mensaje,$id_local,$estado_push){


        $parametros = array(

            "select" => "estado_push.estado",
            "join" => array(
                "tbl_envio_push envio_push, envio_push.id_mensaje = tbl_mensaje.id",
                "tbl_detalle_envio_push detalle_push, detalle_push.id_envio_push = envio_push.id",
                "tbl_estado_envio_push estado_push, estado_push.id_detalle_envio_push = detalle_push.id",
                "tbl_local local, local.id = tbl_mensaje.id_local",
            ),
            "where" => "local.estado = " . ESTADO_ACTIVO . "
            and local.id = " . $id_local . "
            and estado_push.estado = " . $estado_push . "
            and tbl_mensaje.id = $id_mensaje"

        );

        $estados_mensajes = $this->mensaje_model->total_records($parametros);

        return $estados_mensajes;
    }

    public function obtener_estado_abiertos_fecha_push($id_mensaje,$id_local){

        $parametros_carta = array(
            "select" => "DATE_FORMAT(estado_push.fecha_registro,'%Y-%m-%d') as fecha",
            "join" => array(
                "tbl_envio_push envio_push, envio_push.id_mensaje = tbl_mensaje.id",
                "tbl_detalle_envio_push detalle_push, detalle_push.id_envio_push = envio_push.id",
                "tbl_estado_envio_push estado_push, estado_push.id_detalle_envio_push = detalle_push.id",
                "tbl_local local, local.id = tbl_mensaje.id_local",
            ),
            "where" => "local.estado = " . ESTADO_ACTIVO . "
            and local.id = " . $id_local . "
            and estado_push.estado = ".CODE_SEEN_SEND_PUSH."
            and tbl_mensaje.id = $id_mensaje",

        );

        $estados_mensajes = $this->mensaje_model->search_array($parametros_carta);
        return $estados_mensajes;

    }

    public function obtener_estados_push_bar($id_mensaje,$id_local,$estado_push){


        $parametros = array(

            "select" => "estado_push.estado,DATE_FORMAT(estado_push.fecha_registro,'%Y-%m-%d') as fecha,count(estado_push.fecha_registro) as total_mensaje",
            "join" => array(
                "tbl_envio_push envio_push, envio_push.id_mensaje = tbl_mensaje.id",
                "tbl_detalle_envio_push detalle_push, detalle_push.id_envio_push = envio_push.id",
                "tbl_estado_envio_push estado_push, estado_push.id_detalle_envio_push = detalle_push.id",
                "tbl_local local, local.id = tbl_mensaje.id_local",
            ),
            "where" => "local.estado = " . ESTADO_ACTIVO . "
            and local.id = " . $id_local . "
            and estado_push.estado = " . $estado_push . "
            and tbl_mensaje.id = $id_mensaje",
            "group"=>"estado_push.fecha_registro"

        );

        $estados_mensajes = $this->mensaje_model->search_array($parametros);

        return $estados_mensajes;
    }

    public function obtener_estado_pendiente_push($id_mensaje,$id_local,$entregados,$fecha){


        $parametros = array(
            "select" => "DATE_FORMAT(estado_push.fecha_registro,'%Y-%m-%d') as fecha,
            ($entregados - count(estado_push.fecha_registro)) as total_mensaje",
            "join" => array(
                "tbl_envio_push envio_push, envio_push.id_mensaje = tbl_mensaje.id",
                "tbl_detalle_envio_push detalle_push, detalle_push.id_envio_push = envio_push.id",
                "tbl_estado_envio_push estado_push, estado_push.id_detalle_envio_push = detalle_push.id",
                "tbl_local local, local.id = tbl_mensaje.id_local",
            ),
            "where" => "local.estado = " . ESTADO_ACTIVO . "
            and local.id = " . $id_local . "
             and estado_push.estado = ".CODE_SEEN_SEND_PUSH."
            and DATE_FORMAT(estado_push.fecha_registro,'%Y-%m-%d') = '$fecha'
            and tbl_mensaje.id = $id_mensaje",
            "group"=>"estado_push.fecha_registro"
        );

        $estados_mensajes = $this->mensaje_model->get_search_row($parametros);
        return $estados_mensajes;

    }
}