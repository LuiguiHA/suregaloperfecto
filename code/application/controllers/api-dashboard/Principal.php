<?php
/*
 * Created by Luis Alberto Rosas Arce
 */

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
require APPPATH . "/libraries/Uuid.php";

class Principal extends REST_Controller
{
    public $id_empresa;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('utils_helper');
        $this->load->model("token_dashboard_model", "token_dashboard_model");
        $this->load->model("usuario_model", "usuario_model");
        $this->load->model("promocion_model", "promocion_model");
        $this->load->model("beneficio_model", "beneficio_model");
        $this->load->model("puntos_model", "puntos_model");
        $this->load->model("reserva_solicitud_model", "reserva_solicitud_model");
        $this->lang->load('dashboard_error_lang', 'spanish');
        $this->lang->load('dashboard_mensajes_lang', 'spanish');
        $this->id_empresa = $this->config->item('empresa');
    }

    public function obtener_post()
    {
        $token_recibido = $this->getHeaderRC('token');
        $id_local = $this->post('id_local', TRUE);
        $accion_realizada = ACCION_DASHBOARD_OBTENER_PRINCIPAL;
        $nombre_metodo = NOMBRE_METODO_POST;
        $operacion = "obtener principal";

        $parametros = array(
            "token" => $token_recibido,
            "id_local"=> $id_local
        );
        $id_empresa = $this->id_empresa;
        $usuario = "";
        if (isset($token_recibido) && $token_recibido != "") {
            $objeto_token = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);
            if ($objeto_token != "" && isset($objeto_token)) {
                $usuario = $objeto_token->usuario;
                if (isset($id_empresa)) {

                    $datos_usuario = $this->buscar_datos_usuario($id_empresa,$id_local);

                    if (isset($datos_usuario)) {

                        $reservas_grafico =  $this->reservas_grafico($id_local);

                        $reservas = $this->listado_reservas_grafico($id_local);

                        if (isset($reservas_grafico) && isset($reservas)){

                            $acumulacion_grafico = $this->acumulacion_puntos_grafico($id_local);

                            $acumulacion = $this->listado_acumulacion_puntos_grafico($id_local);

                            if (isset($acumulacion_grafico) && isset($acumulacion)) {

                                $beneficios_usados = $this->buscar_beneficios_usados($id_local);

                                $beneficios_usados_grafico = $this->beneficios_usados_grafico($id_local);


                                if (isset($beneficios_usados) && isset($beneficios_usados_grafico)) {
                                    $resultado = array(
                                        "resultado" => ESTADO_RESPUESTA_OK,
                                        "mensaje" => $this->lang->line('mensaje_operacion_exito'),
                                        "datos_usuarios" => $datos_usuario,
                                        "beneficios_listado" => $beneficios_usados,
                                        "beneficios_grafico" => $beneficios_usados_grafico,
                                        "reservas_listado" => $reservas,
                                        "reservas_grafico" => $reservas_grafico,
                                        "acumulacion_listado" => $acumulacion,
                                        "acumulacion_grafico" => $acumulacion_grafico
                                    );

                                } else {
                                    $resultado = array(
                                        "resultado" => ESTADO_RESPUESTA_ERROR,
                                        "mensaje" => $this->lang->line('error_obtener_beneficios')
                                    );
                                }
                            } else {
                                $resultado = array(
                                    "resultado" => ESTADO_RESPUESTA_ERROR,
                                    "mensaje" => $this->lang->line('error_obtener_acumulacion_grafico')

                                );
                            }

                        } else {
                              $resultado = array(
                                  "resultado" => ESTADO_RESPUESTA_ERROR,
                                  "mensaje" => $this->lang->line('error_obtener_reservas_grafico')

                              );
                          }

                    } else {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR,
                            "mensaje" => $this->lang->line('error_estadisticas_usuarios')
                        );
                    }


                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_empresa_encontrada')
                    );
                }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token_recibido, $usuario, $operacion);
        $this->response($resultado);

    }


    public function buscar_empresa_token($token)
    {
        $parametros_busqueda_empresa = array(
            "select" => "empresa.*", "join" => array(
                "tbl_contacto contacto, contacto.id = tbl_token_dashboard.id_contacto", "tbl_empresa empresa, empresa.id = contacto.id_empresa"
            ), "where" => "tbl_token_dashboard.token = '$token'
            and tbl_token_dashboard.estado = " . ESTADO_ACTIVO . "
            and contacto.estado = " . ESTADO_ACTIVO . "
            and empresa.estado = " . ESTADO_ACTIVO
        );

        $empresa_encontrada = $this->token_dashboard_model->get_search_row($parametros_busqueda_empresa);
        return $empresa_encontrada;
    }

    public function buscar_datos_usuario($id_empresa,$id_local)
    {

        $numero_usuarios_totales = count($this->buscar_usuarios_empresa($id_empresa));
        $numero_usuarios_canje = count($this->buscar_usuarios_canje($id_empresa,$id_local));
        $numero_usuarios_puntos = count($this->buscar_usuarios_puntos($id_empresa,$id_local));
        $numero_usuarios_nuevos = count($this->buscar_usuarios_nuevos($id_empresa));
        $numero_usuarios_recurrentes = count($this->buscar_usuarios_recurrentes($id_empresa,$id_local));


        $datos_usuario = array(
            "usuarios_totales" => $numero_usuarios_totales, "usuarios_recurrentes" => $numero_usuarios_recurrentes, "usuarios_nuevos" => $numero_usuarios_nuevos, "usuarios_canje" => $numero_usuarios_canje, "usuarios_puntos" => $numero_usuarios_puntos
        );

        return $datos_usuario;
    }

    public function buscar_usuarios_empresa($id_empresa)
    {
        $parametros_busqueda_usuario = array(
            "select" => "tbl_usuario.*", "join" => array(
                "tbl_empresa empresa, empresa.id = tbl_usuario.id_empresa"
            ), "where" => "empresa.estado = " . ESTADO_ACTIVO . "
            and empresa.id = $id_empresa
            and tbl_usuario.estado  = " . ESTADO_ACTIVO
        );
        $usuarios = $this->usuario_model->search_array($parametros_busqueda_usuario);
        return $usuarios;
    }

    public function buscar_usuarios_canje($id_empresa,$id_local)
    {
        if ($id_local != null || $id_local != ""){
            $where_local = " and canje.id_local = ".$id_local;
        }

        $parametros_busquda_usuario_canje = array(
            "select" => "tbl_usuario.*",
            "join" => array(
                "tbl_canje canje, canje.id_usuario = tbl_usuario.id"
            ),
            "where" => "tbl_usuario.estado = " . ESTADO_ACTIVO . "
            and tbl_usuario.id_empresa = $id_empresa
            and canje.estado = " . ESTADO_ACTIVO.$where_local,
            "group" => "tbl_usuario.id"
        );
        $usuarios_canje = $this->usuario_model->search_array($parametros_busquda_usuario_canje);
        return $usuarios_canje;
    }

    public function buscar_usuarios_puntos($id_empresa,$id_local)
    {
        if ($id_local != null || $id_local != ""){
            $where_local = " and puntos.id_local = ".$id_local;
        }

        $parametros_busqueda_usuario_puntos = array(
            "select" => "tbl_usuario.*",
            "join" => array(
                "tbl_puntos puntos, puntos.id_usuario = tbl_usuario.id"
            ),
            "where" => "tbl_usuario.estado = " . ESTADO_ACTIVO . "
            and puntos.estado = " . ESTADO_ACTIVO . "
            and tbl_usuario.id_empresa = $id_empresa".$where_local,
            "group" => "tbl_usuario.id"
        );
        $usuarios_puntos = $this->usuario_model->search_array($parametros_busqueda_usuario_puntos);

        return $usuarios_puntos;
    }

    public function buscar_usuarios_nuevos($id_empresa)
    {
        //Criterio será de una semana atrás
        $fecha_actual = date("Y-m-d H:i:s");
        $fecha_semana_pasada = strtotime("-7 day", strtotime($fecha_actual));
        $fecha_semana_pasada = date("Y-m-d H:i:s", $fecha_semana_pasada);

        $parametros_busqueda_usuario_nuevo = array(
            "select" => "tbl_usuario.*", "where" => "tbl_usuario.estado = " . ESTADO_ACTIVO . "
            and tbl_usuario.id_empresa = $id_empresa
            and tbl_usuario.fecha_registro > '$fecha_semana_pasada'"
        );

        $usuarios = $this->usuario_model->search_array($parametros_busqueda_usuario_nuevo);
        return $usuarios;
    }

    public function buscar_usuarios_recurrentes($id_empresa,$id_local)
    {
        if ($id_local != null || $id_local != ""){

            $parametros_busqueda_usuario_nuevo = array(
            "select" => "tbl_usuario.*",
            "left"=>array(
                "tbl_canje canje,canje.id_usuario =  tbl_usuario.id,left",
                "tbl_puntos puntos,puntos.id_usuario =  tbl_usuario.id,left",
                "tbl_reserva_solicitud reserva,reserva.id_usuario =  tbl_usuario.id,left"
            ),
            "where" => "tbl_usuario.estado = " . ESTADO_ACTIVO . "
            and canje.id_local =". $id_local ."
            and puntos.id_local = ".$id_local."
            and reserva.id_local = ".$id_local."
            and reserva.estado = ".ESTADO_ASISTIDO,
            "group"=> "tbl_usuario.id"

            );

            $usuarios = $this->usuario_model->search_array($parametros_busqueda_usuario_nuevo);

        }else{
            //Criterio será de una semana atrás
            $fecha_actual = date("Y-m-d H:i:s");
            $fecha_semana_pasada = strtotime("-7 day", strtotime($fecha_actual));
            $fecha_semana_pasada = date("Y-m-d H:i:s", $fecha_semana_pasada);

            $parametros_busqueda_usuario_nuevo = array(
                "select" => "tbl_usuario.*", "where" => "tbl_usuario.estado = " . ESTADO_ACTIVO . "
            and tbl_usuario.id_empresa = $id_empresa
            and tbl_usuario.fecha_modificacion > '$fecha_semana_pasada'"
            );

            $usuarios = $this->usuario_model->search_array($parametros_busqueda_usuario_nuevo);
        }

        return $usuarios;
    }

    public function buscar_promociones_usadas()
    {

        $parametros_busqueda_promociones_usadas = array(
            "select" => "promo_canje.id as id,
            concat(persona.nombre,' ',persona.apellido) as usuario_nombre,
            tbl_promocion.nombre as descripcion,
            promo_canje.fecha_registro as fecha", "join" => array(
                "tbl_promocion_canje promo_canje, promo_canje.id_promocion = tbl_promocion.id", "tbl_usuario usuario, usuario.id = promo_canje.id_usuario", "tbl_persona persona, persona.id = usuario.id_persona", "tbl_local local, local.id = promo_canje.id_local"
            ),

            "where" => "local.id_empresa = $this->id_empresa
            and promo_canje.estado = " . ESTADO_ACTIVO . "
            and local.estado = " . ESTADO_ACTIVO, "limit" => ELEMENTOS_POR_PAGINA

        );
        $promociones_usadas = $this->promocion_model->search_array($parametros_busqueda_promociones_usadas);
        return $promociones_usadas;

    }

    public function buscar_beneficios_usados($id_local)
    {

        $where = "";
        if ($id_local !=null || $id_local!= ""){
            $where =  " AND local.id  = ".$id_local;
        }

        $parametros_beneficios_usados = array(
            "select" => "usuario.id,concat(persona.nombre,' ',persona.apellido) as usuario_nombre,local.nombre as local_nombre,
            canje.puntos as puntos_usuados,tbl_beneficio.nombre as beneficio,
           canje.fecha_registro as fecha",

            "left" => array(
                "tbl_canje canje, canje.id_beneficio = tbl_beneficio.id,left",
                "tbl_usuario usuario, usuario.id = canje.id_usuario,left",
                "tbl_persona persona, persona.id = usuario.id_persona,left",
                "tbl_local local, local.id = canje.id_local,left"),
            "where" => "canje.fecha_registro BETWEEN (CURDATE() - INTERVAL 6 DAY) AND CURDATE() + 1".$where,
            "order"=> "canje.fecha_registro desc"

        );
        $beneficios_usados = $this->beneficio_model->search_array($parametros_beneficios_usados);
        return $beneficios_usados;
    }

    public function buscar_usuario_token($token)
    {
        $parametros_busqueda_usuario = array(
            "select" => "contacto.perfil", "join" => array(
                "tbl_contacto contacto, contacto.id = tbl_token_dashboard.id_contacto"
            ), "where" => "tbl_token_dashboard.token = '$token'
            and tbl_token_dashboard.estado = " . ESTADO_ACTIVO . "
            and contacto.id = tbl_token_dashboard.id_contacto"
        );

        $empresa_encontrada = $this->token_dashboard_model->get_search_row($parametros_busqueda_usuario);
        return $empresa_encontrada;
    }

    public function validar_perfil($token)
    {
        $perfil_asignado = FALSE;
        $perfil_usuario = $this->buscar_usuario_token($token);

        $perfil = json_decode($perfil_usuario->perfil, true);

        foreach ($perfil as $arreglo) {
            if (isset($arreglo["dashboard"])) {
                if ($arreglo["dashboard"] == "si") {
                    $perfil_asignado = TRUE;
                }
            }
        }
        return $perfil_asignado;
    }

    public function beneficios_usados_grafico($id_local)
    {
        $where = "";
        if ($id_local !=null || $id_local!= ""){
            $where =  " AND local.id  = ".$id_local;
        }

        $parametros_beneficios_usados_grafico = array(
            "select" => "canje.id_local,local.nombre , count(canje.id_local) as total_canje,
              DATE_FORMAT(canje.fecha_registro,'%Y-%m-%d') as fecha",

            "left" => array("tbl_canje canje, canje.id_beneficio = tbl_beneficio.id,left",
                            "tbl_local local, local.id = canje.id_local"),
            "where"=> "canje.fecha_registro BETWEEN (CURDATE() - INTERVAL 6 DAY) AND CURDATE() + 1 ".$where,
            "group" => array("DATE_FORMAT(canje.fecha_registro,'%m-%d-%Y')", "canje.id_local")

        );

        $beneficios_usados_grafico = $this->beneficio_model->search_array($parametros_beneficios_usados_grafico);
        return $beneficios_usados_grafico;
    }

    public function promociones_usados_grafico()
    {
        $parametros_promociones_usados_grafico = array(
            "select" => "tbl_promocion.id,tbl_promocion.nombre , count(canje.id_promocion) as total_canje,
            DATE_FORMAT(canje.fecha_registro,'%Y-%m-%d') as fecha",

            "left" => array("tbl_promocion_canje canje, canje.id_promocion = tbl_promocion.id,left"),

            "group" => array("DATE_FORMAT(canje.fecha_registro,'%m-%d-%Y')", "tbl_promocion.id")

        );

        $promociones_usados_grafico = $this->promocion_model->search_array($parametros_promociones_usados_grafico);
        return $promociones_usados_grafico;
    }

    public function reservas_grafico($id_local)
    {
        $where = "";
        if ($id_local !=null || $id_local!= ""){
            $where =  " AND local.id  = ".$id_local;
        }

        $parametros_reservas_grafico = array(
            "select" => "tbl_reserva_solicitud.id,local.nombre , count(tbl_reserva_solicitud.fecha_registro) as total_reserva,
             CASE tbl_reserva_solicitud.estado when '".STATUS_PENDING."' then DATE_FORMAT(tbl_reserva_solicitud.fecha_registro,'%Y-%m-%d')
           when '".STATUS_ACCEPTED."' then DATE_FORMAT(tbl_reserva_solicitud.fecha_aceptado,'%Y-%m-%d')
            when '".STATUS_DENIED."' then DATE_FORMAT(tbl_reserva_solicitud.fecha_rechazado,'%Y-%m-%d')
            when '".STATUS_ACCEPTED."' then DATE_FORMAT(tbl_reserva_solicitud.fecha_confirmacion,'%Y-%m-%d')
            END
            as fecha",

            "left" => array("tbl_local local, local.id = tbl_reserva_solicitud.id_local,left"),
            "where" =>
                "CASE tbl_reserva_solicitud.estado when '".STATUS_PENDING."' then tbl_reserva_solicitud.fecha_registro  BETWEEN (CURDATE() - INTERVAL 6 DAY) AND CURDATE() + 1
                when '".STATUS_ACCEPTED."' then tbl_reserva_solicitud.fecha_aceptado  BETWEEN (CURDATE() - INTERVAL 6 DAY) AND CURDATE() + 1
                when '".STATUS_DENIED."' then tbl_reserva_solicitud.fecha_rechazado  BETWEEN (CURDATE() - INTERVAL 6 DAY) AND CURDATE() + 1
                when '".STATUS_ACCEPTED."' then tbl_reserva_solicitud.fecha_confirmacion  BETWEEN (CURDATE() - INTERVAL 6 DAY) AND CURDATE() + 1
                END ".$where,
            "group" => array("CASE tbl_reserva_solicitud.estado 
                when '".STATUS_PENDING."' then tbl_reserva_solicitud.fecha_registro
                when '".STATUS_ACCEPTED."' then tbl_reserva_solicitud.fecha_aceptado
                when '".STATUS_DENIED."' then tbl_reserva_solicitud.fecha_rechazado
                when '".STATUS_ACCEPTED."' then tbl_reserva_solicitud.fecha_confirmacion
                END", "tbl_reserva_solicitud.id_local")

        );

        $reservas_grafico = $this->reserva_solicitud_model->search_array($parametros_reservas_grafico);
        return $reservas_grafico;
    }

    public function listado_reservas_grafico($id_local)
    {

        $where = "";
        if ($id_local !=null || $id_local!= ""){
            $where =  " AND local.id  = ".$id_local;
        }

        $parametros_reservas = array(
            "select" => "usuario.id,concat(persona.nombre,' ',persona.apellido) as usuario_nombre,tbl_reserva_solicitud.cantidad,
           local.nombre as local_nombre,tbl_reserva_solicitud.estado,
           CASE tbl_reserva_solicitud.estado when '".STATUS_PENDING."' then tbl_reserva_solicitud.fecha_registro
           when '".STATUS_ACCEPTED."' then tbl_reserva_solicitud.fecha_aceptado
            when '".STATUS_DENIED."' then tbl_reserva_solicitud.fecha_rechazado
            when '".STATUS_ACCEPTED."' then tbl_reserva_solicitud.fecha_confirmacion
            END
            as fecha ",

            "left" => array("tbl_local local, local.id = tbl_reserva_solicitud.id_local,left",
                            "tbl_usuario usuario,usuario.id = tbl_reserva_solicitud.id_usuario,left",
                            "tbl_persona persona,persona.id = usuario.id_persona,left"),
            "where" =>
                "CASE tbl_reserva_solicitud.estado when '".STATUS_PENDING."' then tbl_reserva_solicitud.fecha_registro  BETWEEN (CURDATE() - INTERVAL 6 DAY) AND CURDATE() + 1
                when '".STATUS_ACCEPTED."' then tbl_reserva_solicitud.fecha_aceptado  BETWEEN (CURDATE() - INTERVAL 6 DAY) AND CURDATE() + 1
                when '".STATUS_DENIED."' then tbl_reserva_solicitud.fecha_rechazado  BETWEEN (CURDATE() - INTERVAL 6 DAY) AND CURDATE() + 1
                when '".STATUS_ACCEPTED."' then tbl_reserva_solicitud.fecha_confirmacion  BETWEEN (CURDATE() - INTERVAL 6 DAY) AND CURDATE() + 1
                END ".$where,
            "order" =>
                "CASE tbl_reserva_solicitud.estado 
                when '".STATUS_PENDING."' then tbl_reserva_solicitud.fecha_registro
                when '".STATUS_ACCEPTED."' then tbl_reserva_solicitud.fecha_aceptado
                when '".STATUS_DENIED."' then tbl_reserva_solicitud.fecha_rechazado
                when '".STATUS_ACCEPTED."' then tbl_reserva_solicitud.fecha_confirmacion
                END desc"

        );

        $reservas = $this->reserva_solicitud_model->search_array($parametros_reservas);

        return $reservas;
    }

    public function acumulacion_puntos_grafico($id_local)
    {
        $where = "";
        if ($id_local !=null || $id_local!= ""){
            $where =  " AND local.id  = ".$id_local;
        }

        $parametros_acumulacion_grafico = array(
            "select" => "tbl_puntos.id_local,local.nombre , count(tbl_puntos.id_local) as total_acumulacion,
            DATE_FORMAT(tbl_puntos.fecha_registro,'%Y-%m-%d') as fecha",

            "left" => array("tbl_local local, local.id = tbl_puntos.id_local,left"),
            "where" => "tbl_puntos.fecha_registro BETWEEN (CURDATE() - INTERVAL 6 DAY) AND CURDATE() + 1".$where,
            "group" => array("DATE_FORMAT(tbl_puntos.fecha_registro,'%m-%d-%Y')", "tbl_puntos.id_local")

        );

        $acumulacion_grafico = $this->puntos_model->search_array($parametros_acumulacion_grafico);
        return $acumulacion_grafico;
    }

    public function listado_acumulacion_puntos_grafico($id_local)
    {
        $where = "";
        if ($id_local !=null || $id_local!= ""){
            $where =  " AND local.id  = ".$id_local;
        }

        $parametros_acumulacion = array(
            "select" => "usuario.id,concat(persona.nombre,' ',persona.apellido) as usuario_nombre ,tbl_puntos.puntos ,
            local.nombre as local_nombre,tipo_pago.nombre as tipo_pago,tbl_puntos.fecha_registro as fecha",

            "left" => array("tbl_local local, local.id = tbl_puntos.id_local,left",
                            "tbl_usuario usuario, usuario.id = tbl_puntos.id_usuario,left",
                            "tbl_tipo_pago tipo_pago, tipo_pago.id = tbl_puntos.id_tipo_pago,left",
                            "tbl_persona persona, persona.id = usuario.id_persona"),
            "where" => "tbl_puntos.fecha_registro BETWEEN (CURDATE() - INTERVAL 6 DAY) AND CURDATE() + 1".$where,
            "order" => "DATE_FORMAT(tbl_puntos.fecha_registro,'%m-%d-%Y') desc"

        );

        $acumulacion = $this->puntos_model->search_array($parametros_acumulacion);
        return $acumulacion;
    }


    /************TEST****************/
    /* public function buscar_datos_usuario_test_get()
     {
         $id_empresa = "1";
         $usuarios = $this->buscar_datos_usuario($id_empresa);
         $this->response($usuarios);
     }
 
     public function buscar_usuarios_empresa_test_get()
     {
         $id_empresa = "1";
         $usuario_totales = $this->buscar_usuarios_empresa($id_empresa);
         $this->response($usuario_totales);
     }
 
     public function buscar_beneficios_usados_test_get()
     {
         $id_empresa = "1";
         $beneficios_usados = $this->buscar_beneficios_usados($id_empresa);
         $this->response($beneficios_usados);
     }
 
     public function guardar_log_dashboard_test_get()
     {
         $token = (string)Uuid::generate();
 
         $parametros = array(
             "token" => $token
         );
 
         $accion_realizada = "guardar_log_dashboard_test";
         $nombre_metodo = "guardar_log_dashboard_test_get";
 
 
         $this->db->trans_begin();
         guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token);
 
 
         if ($this->db->trans_status() === FALSE)
         {
             $this->db->trans_rollback();
         } else
         {
             $this->db->trans_commit();
         }
     }
 
     public function validar_token_seguridad_dashboard_test_get()
     {
         $token_recibido = "a1e45e80-1886-11e6-89f1-1f04225015b1";
         $accion = "Accion";
         $token = (string)Uuid::generate();
         validar_token_seguridad_dashboard($token_recibido, $accion, $token);
     }
 
     public function obtener_perfil_test_get()
     {
         $perfil_asignado = FALSE;
         $token_recibido = $this->getHeaderRC('token');
         $perfil_usuario = $this->buscar_usuario_token($token_recibido);
 
         $perfil = json_decode($perfil_usuario->perfil, true);
 
         foreach ($perfil as $arreglo)
         {
             if (isset($arreglo["dashboard"]))
             {
                 if ($arreglo["dashboard"] == "si")
                 {
                     $perfil_asignado = TRUE;
                 }
             }
 
         }
         return $perfil_asignado;
     }*/


}