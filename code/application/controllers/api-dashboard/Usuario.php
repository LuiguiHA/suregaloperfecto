<?php
/*
 * Created by Luis Alberto Rosas Arce
 */
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
require APPPATH . "/libraries/Uuid.php";

class Usuario extends REST_Controller
{
    public $id_empresa;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('utils_helper');
        $this->load->model("token_dashboard_model", "token_dashboard_model");
        $this->load->model("contacto_model", "contacto_model");
        $this->load->model("local_model", "local_model");
        $this->load->model("beneficio_model", "beneficio_model");
        $this->lang->load('dashboard_error_lang', 'spanish');
        $this->lang->load('dashboard_mensajes_lang', 'spanish');
        $this->id_empresa = $this->config->item('empresa');

    }

    public function iniciar_sesion_post()
    {

        $accion_realizada = ACCION_DASHBOARD_INICIAR_SESION;
        $nombre_metodo = NOMBRE_METODO_POST;
        $token = "";
        $id_usuario = "";
        $operacion = "iniciar sesión";

        $correo = $this->post("correo", TRUE);
        $contrasenia = $this->post("contrasenia", TRUE);
      //  $id_local = $this->post("id_local", TRUE);
        $dashboard = $this->post("type_dashboard", TRUE);

       /* if ($dashboard == DASHBOARD_ADMIN_COD){
            $type_dashboard = DASHBOARD_ADMIN_NOMBRE;
        }else{
            $type_dashboard = DASHBOARD_MOZO_NOMBRE;
        }*/

        $usuario = $correo;

        $parametros = array(
            "correo" => $correo,
            "contrasenia" => $contrasenia,
          //  "id_local" => $id_local,
           // "type_dashboard"=>$type_dashboard
        );

        if (isset($correo) && $correo != "") {
            if (isset($contrasenia) && $contrasenia != "") {

              /*  if ($dashboard == DASHBOARD_ADMIN_COD){
                    $id_local = "local_dashboard";
                }*/

            //    if (isset($id_local) && $id_local != "") {

                    $contacto = $this->buscar_contacto($correo);

                    if (isset($contacto)) {

                       // if ($dashboard == DASHBOARD_ADMIN_COD) {
                            if ($contacto->id_local != null || $contacto->id_local != "") {
                                $id_local = $contacto->id_local;
                            }else{
                                $id_local = null;
                            }
                    //    }

                        $id_contacto = $contacto->id;

                        $contrasenia_validada = $this->validar_contrasenia($id_contacto, $contrasenia);

                        if ($contrasenia_validada == TRUE) {

                       //     if ($dashboard == DASHBOARD_ADMIN_COD) {
                                if ($id_local == null) {
                                    $local_validado = true;
                                }else{
                                    $local_validado = $this->validar_local($id_contacto, $id_local);
                                }
                          /*  }else {
                                $local_validado = $this->validar_local($id_contacto, $id_local);
                            }*/

                            if ($local_validado == TRUE) {

                            //    if ($dashboard == DASHBOARD_ADMIN_COD) {
                                    if ($id_local == null) {
                                        $detalle_local = array();
                                        $super_admin = "1";
                                    }else{
                                        $detalle_local = $this->buscar_detalle_local($id_local);
                                        $super_admin = "0";
                                    }
                               /* }else{
                                    $detalle_local = $this->buscar_detalle_local($id_local);
                                }*/

                                if (isset($detalle_local)) {
                                    $token = (string)Uuid::generate();
                                    $this->actualizar_sesion($id_usuario);
                                    guardar_token_dashboard($token, $id_contacto, $accion_realizada);
                                    $resultado = array(
                                        "resultado" => ESTADO_RESPUESTA_OK,
                                        "token" => $token,
                                        "mensaje" => $this->lang->line('mensaje_operacion_exito'),
                                        "id_usuario" => $id_contacto,
                                        "id_local" => $id_local,
                                        "detalle_local" => $detalle_local,
                                        "super_admin" => $super_admin
                                    );

                                } else {
                                    $resultado = array(
                                        "resultado" => ESTADO_RESPUESTA_ERROR,
                                        "mensaje" => $this->lang->line('error_obtener_detalle_local')
                                    );
                                }
                            } else {
                                $resultado = array(
                                    "resultado" => ESTADO_RESPUESTA_ERROR,
                                    "mensaje" => $this->lang->line('error_local_no_encontrado')
                                );
                            }
                        } else {
                            $resultado = array(
                                "resultado" => ESTADO_RESPUESTA_ERROR,
                                "mensaje" => $this->lang->line('error_contrasenia_incorrecta')
                            );
                        }

                    } else {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR,
                            "mensaje" => $this->lang->line('error_usurio_no_registrado')
                        );
                    }
             /*   } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_local_vacío')
                    );
                }*/
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_contrasenia_vacio')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_correo_vacio')

            );
        }
        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token, $usuario, $operacion);


        $this->response($resultado);
    }

    public function cerrar_sesion_delete()
    {
        $token_recibido = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_CERRAR_SESION;
        $nombre_metodo = NOMBRE_METODO_DELETE;
        $operacion = "cerrar sesión";

        $parametros = array(
            "token" => $token_recibido,
        );

        if (isset($token_recibido)) {
            $token_encontrado = validar_token_seguridad_dashboard($token_recibido, $this->id_empresa);

            if (isset($token_encontrado)) {

                $id_contacto = $token_encontrado->id_contacto;
                $id_token = $token_encontrado->id;
                $usuario = $token_encontrado->usuario;
                $cerrar_sesion = $this->cerrar_sesion($id_contacto, $id_token);
                if ($cerrar_sesion) {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_OK,
                        "mensaje" => $this->lang->line('mensaje_operacion_exito')
                    );
                } else {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_cerrar_sesion')
                    );
                }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_recibido')
            );
        }
        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token_recibido, $usuario, $operacion);
        $this->response($resultado);
    }

    public function obtener_locales_get()
    {
        $locales = $this->obtener_locales($this->id_empresa);
        $parametros = "";
        $acccion_realizada = "api-dashboard/usuario/obtener_locales";
        $nombre_metodo = NOMBRE_METODO_GET;
        $usuario = "";
        $operacion = "obtener locales";

        if (isset($locales)) {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_OK,
                "mensaje" => $this->lang->line('inicio_obtener_locales'),
                "locales" => $locales
            );
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_local_listado')
            );
        }
        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token_recibido, $usuario, $operacion);
        $this->response($resultado);
    }

    public function obtener_usuarios_get()
    {
        $token = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_LISTAR_USUARIO;
        $nombre_metodo = NOMBRE_METODO_GET;
        $operacion = "Obtener beneficios";
    }

    public function configurar_local_super_admin_post(){

        $id_local = $this->post("id_local", TRUE);
        $token_recibido = $this->getHeaderRC('token');
        $usuario = "";
        $operacion = "configurar local super admin";

        $accion_realizada = ACCION_DASHBOARD_CONFIGURAR_SUPER_ADMIN;
        $nombre_metodo = NOMBRE_METODO_POST;

        $parametros = array(
            "token" => $token_recibido,
            "id_local" => $id_local
        );

        $objeto_token = validar_token_seguridad_dashboard($token_recibido,$this->id_empresa);

        if ($objeto_token!="" && isset($objeto_token)) {
            $usuario = $objeto_token->usuario;
            if (isset($id_local) && $id_local != "") {

                        $local_detalle = $this->buscar_detalle_local($id_local);
                        if ($local_detalle) {
                            $resultado = array(
                                "resultado" => ESTADO_RESPUESTA_OK,
                                "mensaje" => $this->lang->line('mensaje_operacion_exito'),
                                "local" => $local_detalle
                            );

                        } else {
                            $resultado = array(
                                "resultado" => ESTADO_RESPUESTA_ERROR,
                                "mensaje" => $this->lang->line('error_obtener_detalle_local')
                            );
                        }

                    } else {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR,
                            "mensaje" => $this->lang->line('error_local_vacío')
                        );
                    }
                } else
                {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_token_recibido')

                    );
                }


        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo,$token_recibido, $usuario, $operacion);
        $this->response($resultado);
    }

    public function buscar_empresa_token($token)
    {
        $parametros_busqueda_empresa = array(
            "select" => "empresa.id",
            "join" => array(
                "tbl_contacto contacto, contacto.id = tbl_token_dashboard.id_contacto",
                "tbl_empresa empresa, empresa.id = contacto.id_empresa"
            ),
            "where" => "tbl_token_dashboard.token = '$token'
            and tbl_token_dashboard.estado = " . ESTADO_ACTIVO . "
            and contacto.estado = " . ESTADO_ACTIVO . "
            and empresa.estado = " . ESTADO_ACTIVO
        );

        $empresa_encontrada = $this->token_dashboard_model->get_search_row($parametros_busqueda_empresa);
        return $empresa_encontrada;
    }

    public function cerrar_sesion($id_contacto, $id_token)
    {

        $cerro_sesion = false;
        $fecha_actual = date("Y-m-d H:i:s");
        $parametros = array(
            "fecha_modificacion" => $fecha_actual
        );

        $actualizo_contacto = $this->contacto_model->update($id_contacto, $parametros);

        $parametros["estado"] = ESTADO_INACTIVO;
        $parametros["accion_uso"] = "Cerrar sesión";

        $actualizo_token_dashboard = $this->token_dashboard_model->update($id_token, $parametros);

        if ($actualizo_contacto && $actualizo_token_dashboard) {
            $cerro_sesion = true;
        }
        return $cerro_sesion;

    }

    public function validar_token_usuario($token_recibido, $id_contacto)
    {
        $parametros_validacion = array(
            "select" => "contacto.id",
            "join" => array(
                "tbl_contacto contacto, contacto.id = tbl_token_dashboard.id_contacto"
            ),
            "where" => "contacto.id_empresa = $this->id_empresa
            and tbl_token_dashboard.token = '$token_recibido'
            and contacto.id = $id_contacto"
        );

        $validacion = $this->token_dashboard_model->total_record($parametros_validacion);
        return $validacion;
    }

    public function cerrar_sesion_test_get()
    {
        $id_contacto = "1";
        $id_token = "76";

        $cerrar_sesion = $this->cerrar_sesion($id_contacto, $id_token);
        var_dump($cerrar_sesion);
    }

    public function buscar_contacto($correo)
    {
        $usuario = null;

        $parametros_buscar_usuario = array(
            "select" => "id,id_local",
            "where" => "tbl_contacto.estado =" . ESTADO_ACTIVO . "
            and tbl_contacto.correo = '$correo'"
        );

        $usuario = $this->contacto_model->get_search_row($parametros_buscar_usuario);

        return $usuario;
    }

    public function validar_contrasenia($id_contacto, $contrasenia)
    {
        $validacion = FALSE;

        $contrasenia_encriptada = md5($contrasenia);
        $parametros_validacion_contrasenia = array(
            "select" => "*",
            "where" => "tbl_contacto.estado = " . ESTADO_ACTIVO . "
            and tbl_contacto.id = $id_contacto
            and tbl_contacto.contrasenia = '$contrasenia_encriptada'"
        );
        $usuario_validado = $this->contacto_model->get_search_row($parametros_validacion_contrasenia);

        if (isset($usuario_validado)) {
            $validacion = TRUE;
        }

        return $validacion;
    }

    public function buscar_detalle_local($id_local)
    {
        $parametros_busqueda_local = array(
            "select" => "tbl_local.nombre as nombre_local,
                         empresa.nombre as nombre_empresa,
                         meses_reserva as meses,
                         empresa.logo as logo",
            "join" => array(
                "tbl_empresa empresa, empresa.id = tbl_local.id_empresa"
            ),
            "where" => "tbl_local.estado = " . ESTADO_ACTIVO . "
            and empresa.estado = " . ESTADO_ACTIVO . "
            and tbl_local.id = $id_local"
        );

        $local = $this->local_model->get_search_row($parametros_busqueda_local);

        return $local;
    }

    public function validar_local($id_usuario, $id_local)
    {

        $validacion_local = FALSE;

        $parametros_validacion_local = array(
            "select" => "local.id",
            "join" => array(
                "tbl_empresa empresa, empresa.id = tbl_contacto.id_empresa",
                "tbl_local local, local.id_empresa = empresa.id"
            ),
            "where" => "tbl_contacto.estado = " . ESTADO_ACTIVO . "
            and tbl_contacto.id = $id_usuario
            and local.id = $id_local"
        );

        $local_validado = $this->contacto_model->get_search_row($parametros_validacion_local);

        if (isset($local_validado)) {
            $validacion_local = TRUE;
        }


        return $validacion_local;
    }

    public function actualizar_sesion($id_usuario)
    {
        $fecha_actual = date("Y-m-d H:i:s");
        $parametros_actualizar_sesion = array(
            "fecha_modificacion" => $fecha_actual
        );
        $this->contacto_model->update($id_usuario, $parametros_actualizar_sesion);
    }

    public function obtener_locales($id_empresa)
    {
        $parametro_listado_locales = array(
            "select" => "tbl_local.id as id,
            tbl_local.nombre as nombre,tbl_local.meses_reserva as meses_reserva",
            "where" => "tbl_local.estado = " . ESTADO_ACTIVO . "
            and tbl_local.id_empresa = $id_empresa"
        );

        $locales = $this->local_model->search_array($parametro_listado_locales);
        return $locales;
    }

    public function validar_local_dashboard()
    {

    }

    public function buscar_contacto_test_get()
    {
        $correo = "contacto1@gmail.com";
        $contacto = $this->buscar_contacto($correo);

        $this->response($contacto);

    }

    public function validar_contrasenia_test_get($id_contacto, $contrasenia)
    {

        $validado = $this->validar_contrasenia($id_contacto, $contrasenia);
        $this->response($validado);

    }



}
