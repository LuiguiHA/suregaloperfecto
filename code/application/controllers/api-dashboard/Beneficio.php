<?php
/*
 * Created by Luis Alberto Rosas Arce
 */

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "/libraries/REST_Controller.php";
require APPPATH . "/libraries/Uuid.php";

class Beneficio extends REST_Controller
{
    public $descripcion_canje = "";
    public $contacto = "";
    public $id_empresa;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('utils_helper');
        $this->load->model("promocion_model", "promocion_model");
        $this->load->model("token_dashboard_model", "token_dashboard_model");
        $this->load->model("beneficio_model", "beneficio_model");
        $this->lang->load('dashboard_error_lang', 'spanish');
        $this->lang->load('dashboard_mensajes_lang', 'spanish');
        $this->id_empresa = $this->config->item('empresa');

    }

    public function obtener_beneficios_get($paginacion = "")
    {
        $token = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_LISTAR_BENEFICIO;
        $nombre_metodo = NOMBRE_METODO_GET;
        $operacion = "Obtener beneficios";
        

        $parametros = array(
            "token" => $token,
            "paginacion" => $paginacion
        );
        $id_empresa = $this->id_empresa;
        $datos_beneficios = $this->obtener_listado_beneficios($id_empresa, $paginacion);

        $objeto_token = validar_token_seguridad_dashboard($token,$id_empresa);
        $usuario = $objeto_token->usuario;

        if ($objeto_token!="" && isset($objeto_token)) {
            if (isset($datos_beneficios))
            {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_OK,
                    "mensaje" => $this->lang->line('mensaje_operacion_exito'),
                    "total_beneficios" => $datos_beneficios["total_beneficios"],
                    "beneficios" => $datos_beneficios["beneficios"]
                );
            } else
            {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_obtener_beneficios')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_valido')
            );
        }
        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo,$token, $usuario, $operacion);
        $this->response($resultado);
    }

    public function filtrar_beneficios_post($paginacion = "")
    {

        $accion_realizada = ACCION_DASHBOARD_FILTRAR_BENEFICIO;
        $nombre_metodo = NOMBRE_METODO_POST;
        $token = $this->getHeaderRC('token');
        $usuario = "";
        $operacion = "filtrar beneficios";

        $nombre = $this->post("nombre", TRUE);
        $fecha_inicio = $this->post("fecha_inicio", TRUE);
        $fecha_fin = $this->post("fecha_fin", TRUE);
        $estado = $this->post("estado", TRUE);


        $parametros = array(
            "token" => $token,
            "nombre" => $nombre,
            "fecha_inicio" => $fecha_inicio,
            "fecha_fin" => $fecha_fin,
            "estado" => $estado,
            "paginacion" => $paginacion
        );

        $objeto_token = validar_token_seguridad_dashboard($token, $this->id_empresa);

        if($objeto_token!="" && isset($objeto_token))
        {
            $usuario = $objeto_token->usuario;
            $id_empresa = $this->id_empresa;
            $datos_beneficios = $this->obtener_filtro_beneficios($id_empresa, $paginacion);
            
            if (isset($datos_beneficios))
            {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_OK,
                    "mensaje" => $this->lang->line('mensaje_operacion_exito'),
                    "total_beneficios" => $datos_beneficios["total_beneficios"],
                    "beneficios" => $datos_beneficios["beneficios"]
                );
            } else
            {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_obtener_beneficios')
                );
            }
        } else{
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_valido')
            );
        }
               
        
        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo,$token,$usuario,$operacion);
        $this->response($resultado);
    }

    public function detalle_get($id_beneficio)
    {
  
        $accion_realizada = ACCION_DASHBOARD_DETALLE_BENEFICIO;
        $nombre_metodo = NOMBRE_METODO_GET;
        $token = $this->getHeaderRC('token');
        $usuario ="";
        $operacion ="detalle beneficio";

        $parametros = array(
            "token" => $token,
            "id_promocion" => $id_beneficio
        );

        if (isset($id_beneficio) && $id_beneficio != "")
        {
            $objeto_token = validar_token_seguridad_dashboard($token,$this->id_empresa);

            if ($objeto_token!="" && isset($objeto_token)) {
               $detalle_beneficio = $this->obtener_detalle_beneficio($id_beneficio);
               $usuario = $objeto_token->usuario;
                if (isset($detalle_beneficio))
                {
                    $detalle_descripcion_canje = $this->descripcion_canje;
                    if (isset($detalle_descripcion_canje))
                    {
                        $detalle_contacto = $this->contacto;
                        if (isset($detalle_contacto))
                        {
                            $detalle_informacion_adicional = $this->obtener_detalle_informacion_adicional($id_beneficio);
                            if (isset($detalle_informacion_adicional))
                            {
                                $detalle_foto = $this->obtener_detalle_foto($id_beneficio);
                                if (isset($detalle_foto))
                                {
                                    $detalle_locales = $this->obtener_detalle_locales($id_beneficio);
                                    if (isset($detalle_locales))
                                    {
                                        $resultado = array(
                                            "resultado" => ESTADO_RESPUESTA_OK,
                                            "mensaje" => $this->lang->line('mensaje_operacion_exito'),
                                            "beneficio" => $detalle_beneficio,
                                            "descripcion_canje" => $detalle_descripcion_canje,
                                            "contacto" => $detalle_contacto,
                                            "informacion_adicional" => $detalle_informacion_adicional,
                                            "foto_detalle" => $detalle_foto,
                                            "locales" => $detalle_locales
                                        );
                                    } else
                                    {
                                        $resultado = array(
                                            "resultado" => ESTADO_RESPUESTA_ERROR,
                                            "mensaje" => $this->lang->line('error_obtener_detalle_locales')
                                        );
                                    }
                                } else
                                {
                                    $resultado = array(
                                        "resultado" => ESTADO_RESPUESTA_ERROR,
                                        "mensaje" => $this->lang->line('error_obtener_detalle_foto')
                                    );
                                }

                            } else
                            {
                                $resultado = array(
                                    "resultado" => ESTADO_RESPUESTA_ERROR,
                                    "mensaje" => $this->lang->line('error_obtener_detalle_informacion_adicional')
                                );
                            }

                        } else
                        {
                            $resultado = array(
                                "resultado" => ESTADO_RESPUESTA_ERROR,
                                "mensaje" => $this->lang->line('error_obtener_detalle_contacto')
                            );
                        }
                    } else
                    {
                        $resultado = array(
                            "resultado" => ESTADO_RESPUESTA_ERROR,
                            "mensaje" => $this->lang->line('error_obtener_detalle_descripcion_canje')
                        );
                    }

                } else
                {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_obtener_beneficio')
                     );
                }
            } else {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_token_valido')
                );
            }

                
        } else
        {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_parametro_url')
            );
        }
        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo,$token, $usuario, $operacion);
        $this->response($resultado);


    }

    public function eliminar_delete($id_beneficio)
    {
       
        $accion_realizada = ACCION_DASHBOARD_ELIMINAR_BENEFICIO;
        $nombre_metodo = NOMBRE_METODO_DELETE;
        $token = $this->getHeaderRC('token');

        $operacion = "deshabilitar beneficio";
        $usuario="";

        $parametros = array(
            "token" => $token,
            "id_beneficio" => $id_beneficio
        );
        $objeto_token = validar_token_seguridad_dashboard($token, $this->id_empresa);

        if ($objeto_token!="" && isset($objeto_token)) {
            $usuario = $objeto_token->usuario;
            if (isset($id_beneficio) && $id_beneficio != "")
            {
                
                $datos_actualizados = $this->eliminar_beneficio($id_beneficio);
                if ($datos_actualizados == true)
                {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_OK,
                        "mensaje" => $this->lang->line('mensaje_operacion_exito')
                    );
                } else
                {
                    $resultado = array(
                        "resultado" => ESTADO_RESPUESTA_ERROR,
                        "mensaje" => $this->lang->line('error_eliminar_beneficio')
                    );
                }
                
            } else
            {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_parametro_url')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_valido')
            );
        }

        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo, $token, $usuario, $operacion);
        $this->response($resultado);
    }

    public function obtener_beneficios_notificacion_get()
    {
        $token = $this->getHeaderRC('token');
        $accion_realizada = ACCION_DASHBOARD_LISTAR_BENEFICIOS_NOTIFICACION;
        $nombre_metodo = NOMBRE_METODO_GET;
        $operacion = "Obtener beneficios notificacion";


        $parametros = array(
            "token" => $token
        );
        $id_empresa = $this->id_empresa;
        $datos_beneficios = $this->obtener_lista_beneficios_notificacion($id_empresa);

        $objeto_token = validar_token_seguridad_dashboard($token,$id_empresa);
        $usuario = $objeto_token->usuario;

        if ($objeto_token!="" && isset($objeto_token)) {
            if (isset($datos_beneficios))
            {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_OK,
                    "mensaje" => $this->lang->line('mensaje_operacion_exito'),
                    "beneficios" => $datos_beneficios
                );
            } else
            {
                $resultado = array(
                    "resultado" => ESTADO_RESPUESTA_ERROR,
                    "mensaje" => $this->lang->line('error_obtener_beneficios')
                );
            }
        } else {
            $resultado = array(
                "resultado" => ESTADO_RESPUESTA_ERROR,
                "mensaje" => $this->lang->line('error_token_valido')
            );
        }
        guardar_log_dashboard($parametros, $accion_realizada, $nombre_metodo,$token, $usuario, $operacion);
        $this->response($resultado);
    }

    /*METODOS*/

    public function obtener_listado_beneficios($id_empresa, $paginacion)
    {
        $fecha_actual = date("Y-m-d H:i:s");
        $parametros_listado_beneficios = array(
            "select" => "tbl_beneficio.id as id,
            tbl_beneficio.nombre as nombre,
            tbl_beneficio.puntos as puntos,
            tbl_beneficio.fecha_inicio as fecha_inicio,
            tbl_beneficio.fecha_fin as fecha_fin,
            tbl_beneficio.estado as estado",

            "where" => "tbl_beneficio.estado < " . ESTADO_ELIMINADO . "
            and tbl_beneficio.id_empresa = $id_empresa
            and tbl_beneficio.fecha_fin > '$fecha_actual'"
        );
        if (is_numeric($paginacion) && $paginacion > 0)
        {
            $inicio = ELEMENTOS_POR_PAGINA * ($paginacion - 1);
            $beneficios = $this->beneficio_model->search_data_array($parametros_listado_beneficios, $inicio, ELEMENTOS_POR_PAGINA);
        } else
        {
            $beneficios = $this->beneficio_model->search_array($parametros_listado_beneficios);
        }

        $total_beneficios = $this->beneficio_model->total_records($parametros_listado_beneficios);

        $datos_beneficios = array(
            "total_beneficios" => $total_beneficios,
            "beneficios" => $beneficios
        );
        return $datos_beneficios;
    }

    public function obtener_filtro_beneficios($id_empresa, $paginacion)
    {

        $nombre = $this->post("nombre", TRUE);
        $fecha_inicio = $this->post("fecha_inicio", TRUE);
        $fecha_fin = $this->post("fecha_fin", TRUE);
        $estado = $this->post("estado", TRUE);

        $filtro_nombre = "";
        $filtro_fecha_inicio = "";
        $filtro_fecha_fin = "";
        $filtro_estado = "";

        if (isset($nombre) && $nombre != "")
        {
            $filtro_nombre = " and tbl_beneficio.nombre like '%$nombre%'";
        }
        if (isset($fecha_inicio) && $fecha_inicio != "")
        {
            $filtro_fecha_inicio = " and tbl_beneficio.fecha_inicio >= '$fecha_inicio'";
        }
        if (isset($fecha_fin) && $fecha_fin != "")
        {
            $filtro_fecha_fin = " and tbl_beneficio.fecha_inicio <= '$fecha_fin'";
        }
        if ($estado != "" && is_numeric($estado))
        {
            $filtro_estado = " and tbl_beneficio.estado = $estado";
        }

        $fecha_actual = date("Y-m-d H:i:s");
        $parametros_listado_beneficios = array(
            "select" => "tbl_beneficio.id as id,
            tbl_beneficio.nombre as nombre,
            tbl_beneficio.puntos as puntos,
            tbl_beneficio.fecha_inicio as fecha_inicio,
            tbl_beneficio.fecha_fin as fecha_fin,
            tbl_beneficio.estado as estado",

            "where" => "tbl_beneficio.estado < 2
            and tbl_beneficio.id_empresa = $id_empresa
            and tbl_beneficio.fecha_fin > '$fecha_actual'" . $filtro_nombre . $filtro_fecha_inicio . $filtro_fecha_fin . $filtro_estado
        );
        if (is_numeric($paginacion) && $paginacion > 0)
        {
            $inicio = ELEMENTOS_POR_PAGINA * ($paginacion - 1);
            $beneficios = $this->beneficio_model->search_data_array($parametros_listado_beneficios, $inicio, ELEMENTOS_POR_PAGINA);
        } else
        {
            $beneficios = $this->beneficio_model->search_array($parametros_listado_beneficios);
        }
        $total_beneficios = $this->beneficio_model->total_records($parametros_listado_beneficios);

        $datos_beneficios = array(
            "total_beneficios" => $total_beneficios,
            "beneficios" => $beneficios
        );

        return $datos_beneficios;

    }

    public function obtener_detalle_beneficio($id_beneficio)
    {
        $fecha_actual = date("Y-m-d H:i:s");
        $parametros_listado_beneficios = array(
            "select" => "tbl_beneficio.id as id,
            tbl_beneficio.nombre as nombre,
            tbl_beneficio.descripcion as descripcion,
            tbl_beneficio.restriccion as restriccion,
            tbl_beneficio.direccion as direccion,
            tbl_beneficio.compartir_redes as compartir_redes,
            tbl_beneficio.tipo as tipo,
            tbl_beneficio.orden as orden,
            tbl_beneficio.puntos as puntos,
            tbl_beneficio.monto_adicional as monto_adicional,
            tbl_beneficio.telefono as telefono,
            tbl_beneficio.correo as correo",

            "where" => "tbl_beneficio.estado < 2
            and tbl_beneficio.id = $id_beneficio
            and tbl_beneficio.id_empresa = $this->id_empresa
            and tbl_beneficio.fecha_fin > '$fecha_actual'"
        );

        $beneficios = $this->beneficio_model->get_search_row($parametros_listado_beneficios);
        $beneficios = $this->filtrar_detalle_canje($beneficios);
        $beneficios = $this->filtrar_contacto($beneficios);

        return $beneficios;
    }

    public function filtrar_detalle_canje($beneficios)
    {
        $lista = $beneficios;
        if (isset($lista))
        {
            $descripcion_canje = array(
                "puntos" => $lista->puntos,
                "monto_adicional" => $lista->monto_adicional
            );
            $this->descripcion_canje = $descripcion_canje;

            unset($lista->puntos);
            unset($lista->monto_adicional);

        }
        return $lista;
    }

    public function filtrar_contacto($beneficios)
    {
        $lista = $beneficios;
        if (isset($lista))
        {
            $contacto = array(
                "correo" => $lista->correo,
                "telefono" => $lista->telefono
            );
            $this->contacto = $contacto;

            unset($lista->correo);
            unset($lista->telefono);

        }

        return $lista;
    }

    public function obtener_detalle_informacion_adicional($id_beneficio)
    {
        $parametros_detalle_beneficio = array(
            "select" => "info.campo as campo,
            info.valor as valor,
            info.orden as orden",
            "join" => array(
                "tbl_informacion_adicional_beneficio info, info.id_beneficio = tbl_beneficio.id"
            ),
            "where" => "tbl_beneficio.estado = " . ESTADO_ACTIVO . "
            and tbl_beneficio.id= $id_beneficio
            and info.estado = " . ESTADO_ACTIVO
        );
        $informacion_adicional = $this->beneficio_model->search_array($parametros_detalle_beneficio);

        return $informacion_adicional;
    }

    public function obtener_detalle_foto($id_beneficio)
    {
        $parametros_detalle_beneficio = array(
            "select" => "foto.id as id,
            foto.dimension as dimension,
            foto.foto as foto",
            "join" => array(
                "tbl_foto_beneficio foto, foto.id_beneficio = tbl_beneficio.id"
            ),
            "where" => "foto.estado = " . ESTADO_ACTIVO . "
            and tbl_beneficio.estado = " . ESTADO_ACTIVO . "
            and foto.tipo = " . FOTO_DETALLE . "
            and tbl_beneficio.id = $id_beneficio"
        );
        $foto = $this->beneficio_model->search_array($parametros_detalle_beneficio);

        return $foto;
    }

    public function obtener_detalle_locales($id_beneficio)
    {
        $parametros_detalle_beneficio = array(
            "select" => "local.id as id,
            local.nombre as nombre,
            local.latitud as latitud,
            local.longitud as longitud,
            local.correo as correo,
            local.telefono as telefono,
            local.direccion as direccion",
            "join" => array(
                "tbl_local_beneficio local_beneficio, local_beneficio.id_beneficio = tbl_beneficio.id",
                "tbl_local local, local.id = local_beneficio.id_local",
                "tbl_empresa emp, emp.id = local.id_empresa"
            ),
            "where" => "tbl_beneficio.id = $id_beneficio
            and tbl_beneficio.estado = " . ESTADO_ACTIVO . "
            and local_beneficio.estado = " . ESTADO_ACTIVO . "
            and local.estado = " . ESTADO_ACTIVO . "
            and emp.estado = " . ESTADO_ACTIVO
        );
        $locales = $this->beneficio_model->search_array($parametros_detalle_beneficio);

        return $locales;

    }

    public function eliminar_beneficio($id_beneficio)
    {
        $fecha_actual = date("Y-m-d H:i:s");
        $parametros_beneficio = array(
            "estado" => ESTADO_ELIMINADO,
            "fecha_modificacion" => $fecha_actual
        );

        $filas_afectadas = $this->beneficio_model->update($id_beneficio, $parametros_beneficio);
        return $filas_afectadas;
    }

    public function eliminar_beneficio_test_get()
    {
        $id_beneficio = "1";
        $filas_afectadas = $this->eliminar_beneficio($id_beneficio);
        var_dump($filas_afectadas);
    }

    public function buscar_usuario_token($token)
    {
        $parametros_busqueda_usuario = array(
            "select" => "contacto.perfil",
            "join" => array(
                "tbl_contacto contacto, contacto.id = tbl_token_dashboard.id_contacto"
            ),
            "where" => "tbl_token_dashboard.token = '$token'
            and tbl_token_dashboard.estado = " . ESTADO_ACTIVO . "
            and contacto.id = tbl_token_dashboard.id_contacto"
        );

        $empresa_encontrada = $this->token_dashboard_model->get_search_row($parametros_busqueda_usuario);
        return $empresa_encontrada;
    }

    public function validar_perfil($token)
    {
        $perfil_asignado = FALSE;
        $perfil_usuario = $this->buscar_usuario_token($token);

        $perfil = json_decode($perfil_usuario->perfil, true);

        foreach ($perfil as $arreglo)
        {
            if (isset($arreglo["consultas"]))
            {
                if ($arreglo["consultas"] == "si")
                {
                    $perfil_asignado = TRUE;
                } else
                {
                    if (is_array($arreglo["consultas"]))
                    {
                        $arreglo_temporal = $arreglo["consultas"];
                        $credencial = array_search("canjes", $arreglo_temporal);
                        if ($credencial !== FALSE)
                        {

                            $perfil_asignado = TRUE;
                        }

                    }
                }
            }
        }

        return $perfil_asignado;
    }

    public function obtener_lista_beneficios_notificacion($id_empresa)
    {
        $fecha_actual = date("Y-m-d H:i:s");

        $parametros_listado_beneficios = array(
            "select" => "tbl_beneficio.id as id,
                tbl_beneficio.nombre as nombre,
                tbl_beneficio.estado as estado",

            "where" => "tbl_beneficio.estado < " . ESTADO_ELIMINADO . "
                and tbl_beneficio.id_empresa = $id_empresa
                and tbl_beneficio.fecha_fin > '$fecha_actual'"
        );


        $beneficios = $this->beneficio_model->search_array($parametros_listado_beneficios);

        return $beneficios;
    }

    public function obtener_detalle_beneficio_notificacion($id_beneficio,$unique)
    {
        $foto_tipo = FOTO_LISTADO;
        if ($unique){
            $foto_tipo = FOTO_DETALLE;
        }

        $parametros_beneficio = array(
            "select" => "tbl_beneficio.nombre,
                        tbl_beneficio.descripcion,
                        tbl_beneficio.puntos,
                        tbl_beneficio.monto_adicional,
                         foto.foto as foto",
                "join" => array(
                    "tbl_foto_beneficio foto, foto.id_beneficio = tbl_beneficio.id"
                ),
            "where" => "foto.estado = " . ESTADO_ACTIVO . "
            and tbl_beneficio.estado = " . ESTADO_ACTIVO . "
            and foto.tipo = " . $foto_tipo . "
            and foto.plataforma = " . PLATAFORMA_WEB . "
            and tbl_beneficio.id = $id_beneficio"

        );


        $beneficio = $this->beneficio_model->get_search_row($parametros_beneficio);

        return $beneficio;
    }
}