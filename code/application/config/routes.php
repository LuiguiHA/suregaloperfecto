<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'frontend/inicio/index';
$route['404_override'] = '';
//$route['translate_uri_dashes'] = FALSE;

################### backend #############################

#inicio
$route[$this->config->item('path_backend').'/inicio'] = "backend/Twitter/listar"; // dashboard (cambiar esto por la página de inicio o página que debe de aparecer despues del login)
$route[$this->config->item('path_backend')] = "backend/inicio/index";

#login
$route[$this->config->item('path_backend').'/login'] = "backend/inicio/login";
$route[$this->config->item('path_backend').'/logout'] = "backend/inicio/logout";

#Beneficio
$route[$this->config->item('path_backend').'/Twitter'] = "backend/Twitter/listar";
$route[$this->config->item('path_backend').'/Beneficio/(:num)'] = "backend/Beneficio/listar/$1";
$route[$this->config->item('path_backend').'/Beneficio/buscar'] = "backend/Beneficio/buscar";
$route[$this->config->item('path_backend').'/Beneficio/buscar/(:num)'] = "backend/Beneficio/buscar/$1";
$route[$this->config->item('path_backend').'/Beneficio/agregar'] = "backend/Beneficio/agregar";
$route[$this->config->item('path_backend').'/Beneficio/carga_masiva'] = "backend/Beneficio/carga_masiva";
$route[$this->config->item('path_backend').'/Beneficio/carga_imagenes'] = "backend/Beneficio/carga_imagenes";
$route[$this->config->item('path_backend').'/Beneficio/export'] = "backend/Beneficio/export";
$route[$this->config->item('path_backend').'/Beneficio/editar/(:num)'] = "backend/Beneficio/editar/$1";
$route[$this->config->item('path_backend').'/Beneficio/editar/(:num)/F'] = "backend/Beneficio/editar/$1/F";
$route[$this->config->item('path_backend').'/Beneficio/editar/(:num)/I'] = "backend/Beneficio/editar/$1/I";
$route[$this->config->item('path_backend').'/Beneficio/editar/(:num)/L'] = "backend/Beneficio/editar/$1/L";

#Categoria_Beneficio

$route[$this->config->item('path_backend').'/Categoria_Beneficio'] = "backend/Categoria_beneficio/listar";
$route[$this->config->item('path_backend').'/Categoria_Beneficio/(:num)'] = "backend/Categoria_beneficio/listar/$1";
$route[$this->config->item('path_backend').'/Categoria_Beneficio/buscar'] = "backend/Categoria_beneficio/buscar";
$route[$this->config->item('path_backend').'/Categoria_Beneficio/buscar/(:num)'] = "backend/Categoria_beneficio/buscar/$1";
$route[$this->config->item('path_backend').'/Categoria_Beneficio/agregar'] = "backend/Categoria_beneficio/agregar";
$route[$this->config->item('path_backend').'/Categoria_Beneficio/export'] = "backend/Categoria_beneficio/export";
$route[$this->config->item('path_backend').'/Categoria_Beneficio/editar/(:num)'] = "backend/Categoria_beneficio/editar/$1";


#Foto_Beneficio
$route[$this->config->item('path_backend').'/Foto_Beneficio/(:num)'] = "backend/Foto_beneficio/listar/$1";
$route[$this->config->item('path_backend').'/Foto_Beneficio'] = "backend/Foto_beneficio/listar";
$route[$this->config->item('path_backend').'/Foto_Beneficio/buscar/(:num)'] = "backend/Foto_beneficio/buscar/$1";
$route[$this->config->item('path_backend').'/Foto_Beneficio/buscar'] = "backend/Foto_beneficio/buscar";
$route[$this->config->item('path_backend').'/Foto_Beneficio/export'] = "backend/Foto_beneficio/export";
$route[$this->config->item('path_backend').'/Foto_Beneficio/agregar/(:num)'] = "backend/Foto_beneficio/agregar/$1";
$route[$this->config->item('path_backend').'/Foto_Beneficio/editar/(:num)/(:num)'] = "backend/Foto_beneficio/editar/$1/$2";

#Info_Adicional_Beneficio
$route[$this->config->item('path_backend').'/Info_Adicional_Beneficio/(:num)'] = "backend/Info_adicional_beneficio/listar/$1";
$route[$this->config->item('path_backend').'/Info_Adicional_Beneficio'] = "backend/Info_adicional_beneficio/listar";
$route[$this->config->item('path_backend').'/Info_Adicional_Beneficio/buscar/(:num)'] = "backend/Info_adicional_beneficio/buscar/$1";
$route[$this->config->item('path_backend').'/Info_Adicional_Beneficio/buscar'] = "backend/Info_adicional_beneficio/buscar";
$route[$this->config->item('path_backend').'/Info_Adicional_Beneficio/export'] = "backend/Info_adicional_beneficio/export";
$route[$this->config->item('path_backend').'/Info_Adicional_Beneficio/agregar/(:num)'] = "backend/Info_adicional_beneficio/agregar/$1";
$route[$this->config->item('path_backend').'/Info_Adicional_Beneficio/editar/(:num)/(:num)'] = "backend/Info_adicional_beneficio/editar/$1/$2";


#Local_Beneficio
$route[$this->config->item('path_backend').'/Local_Beneficio/(:num)'] = "backend/Local_beneficio/listar/$1";
$route[$this->config->item('path_backend').'/Local_Beneficio'] = "backend/Local_beneficio/listar";
$route[$this->config->item('path_backend').'/Local_Beneficio/buscar/(:num)'] = "backend/Local_beneficio/buscar/$1";
$route[$this->config->item('path_backend').'/Local_Beneficio/buscar'] = "backend/Local_beneficio/buscar";
$route[$this->config->item('path_backend').'/Local_Beneficio/export'] = "backend/Local_beneficio/export";
$route[$this->config->item('path_backend').'/Local_Beneficio/agregar/(:num)'] = "backend/Local_beneficio/agregar/$1";
$route[$this->config->item('path_backend').'/Local_Beneficio/editar/(:num)/(:num)'] = "backend/Local_beneficio/editar/$1/$2";

#Promocion
$route[$this->config->item('path_backend').'/Promocion'] = "backend/Promocion/listar";
$route[$this->config->item('path_backend').'/Promocion/(:num)'] = "backend/Promocion/listar/$1";
$route[$this->config->item('path_backend').'/Promocion/buscar'] = "backend/Promocion/buscar";
$route[$this->config->item('path_backend').'/Promocion/buscar/(:num)'] = "backend/Promocion/buscar/$1";
$route[$this->config->item('path_backend').'/Promocion/agregar'] = "backend/Promocion/agregar";
$route[$this->config->item('path_backend').'/Promocion/carga_masiva'] = "backend/Promocion/carga_masiva";
$route[$this->config->item('path_backend').'/Promocion/carga_imagenes'] = "backend/Promocion/carga_imagenes";
$route[$this->config->item('path_backend').'/Promocion/export'] = "backend/Promocion/export";
$route[$this->config->item('path_backend').'/Promocion/editar/(:num)'] = "backend/Promocion/editar/$1";
$route[$this->config->item('path_backend').'/Promocion/editar/(:num)/F'] = "backend/Promocion/editar/$1/F";
$route[$this->config->item('path_backend').'/Promocion/editar/(:num)/I'] = "backend/Promocion/editar/$1/I";
$route[$this->config->item('path_backend').'/Promocion/editar/(:num)/L'] = "backend/Promocion/editar/$1/L";

#Categoria
$route[$this->config->item('path_backend').'/Categoria'] = "backend/Categoria/listar";
$route[$this->config->item('path_backend').'/Categoria/(:num)'] = "backend/Categoria/listar/$1";
$route[$this->config->item('path_backend').'/Categoria/buscar'] = "backend/Categoria/buscar";
$route[$this->config->item('path_backend').'/Categoria/buscar/(:num)'] = "backend/Categoria/buscar/$1";
$route[$this->config->item('path_backend').'/Categoria/agregar'] = "backend/Categoria/agregar";
$route[$this->config->item('path_backend').'/Categoria/export'] = "backend/Categoria/export";
$route[$this->config->item('path_backend').'/Categoria/editar/(:num)'] = "backend/Categoria/editar/$1";

#Foto_Promocion
$route[$this->config->item('path_backend').'/Foto_Promocion/(:num)'] = "backend/Foto_promocion/listar/$1";
$route[$this->config->item('path_backend').'/Foto_Promocion'] = "backend/Foto_promocion/listar";
$route[$this->config->item('path_backend').'/Foto_Promocion/buscar/(:num)'] = "backend/Foto_promocion/buscar/$1/";
$route[$this->config->item('path_backend').'/Foto_Promocion/buscar'] = "backend/Foto_promocion/buscar";
$route[$this->config->item('path_backend').'/Foto_Promocion/export'] = "backend/Foto_Promocion/export";
$route[$this->config->item('path_backend').'/Foto_Promocion/agregar/(:num)'] = "backend/Foto_promocion/agregar/$1";
$route[$this->config->item('path_backend').'/Foto_Promocion/editar/(:num)/(:num)'] = "backend/Foto_promocion/editar/$1/$2";

#Info_Adicional_Promocion
$route[$this->config->item('path_backend').'/Info_Adicional_Promocion/(:num)'] = "backend/Info_adicional_promocion/listar/$1/";
$route[$this->config->item('path_backend').'/Info_Adicional_Promocion'] = "backend/Info_adicional_promocion/listar";
$route[$this->config->item('path_backend').'/Info_Adicional_Promocion/buscar/(:num)'] = "backend/Info_adicional_promocion/buscar/$1";
$route[$this->config->item('path_backend').'/Info_Adicional_Promocion/buscar'] = "backend/Info_adicional_promocion/buscar";
$route[$this->config->item('path_backend').'/Info_Adicional_Promocion/export'] = "backend/Info_adicional_promocion/export";
$route[$this->config->item('path_backend').'/Info_Adicional_Promocion/agregar/(:num)'] = "backend/Info_adicional_promocion/agregar/$1";
$route[$this->config->item('path_backend').'/Info_Adicional_Promocion/editar/(:num)/(:num)'] = "backend/Info_adicional_promocion/editar/$1/$2";


#Local_Promocion
$route[$this->config->item('path_backend').'/Local_Promocion/(:num)'] = "backend/Local_promocion/listar/$1";
$route[$this->config->item('path_backend').'/Local_Promocion'] = "backend/Local_promocion/listar";
$route[$this->config->item('path_backend').'/Local_Promocion/buscar/(:num)'] = "backend/Local_promocion/buscar/$1";
$route[$this->config->item('path_backend').'/Local_Promocion/buscar'] = "backend/Local_promocion/buscar";
$route[$this->config->item('path_backend').'/Local_Promocion/export'] = "backend/Local_promocion/export";
$route[$this->config->item('path_backend').'/Local_Promocion/agregar/(:num)'] = "backend/Local_promocion/agregar/$1";
$route[$this->config->item('path_backend').'/Local_Promocion/editar/(:num)/(:num)'] = "backend/Local_promocion/editar/$1/$2";

#Empresa
$route[$this->config->item('path_backend').'/Empresa'] = "backend/Empresa/listar";
$route[$this->config->item('path_backend').'/Empresa/(:num)'] = "backend/Empresa/listar/$1";
$route[$this->config->item('path_backend').'/Empresa/buscar'] = "backend/Empresa/buscar";
$route[$this->config->item('path_backend').'/Empresa/buscar/(:num)'] = "backend/Empresa/buscar/$1";
$route[$this->config->item('path_backend').'/Empresa/agregar'] = "backend/Empresa/agregar";
$route[$this->config->item('path_backend').'/Empresa/export'] = "backend/Empresa/export";
$route[$this->config->item('path_backend').'/Empresa/editar/(:num)'] = "backend/Empresa/editar/$1";
$route[$this->config->item('path_backend').'/Empresa/editar/(:num)/C'] = "backend/Empresa/editar/$1/C";
$route[$this->config->item('path_backend').'/Empresa/editar/(:num)/B'] = "backend/Empresa/editar/$1/B";
$route[$this->config->item('path_backend').'/Empresa/editar/(:num)/L'] = "backend/Empresa/editar/$1/L";


#Banner
$route[$this->config->item('path_backend').'/Banner/(:num)'] = "backend/Banner/listar/$1";
$route[$this->config->item('path_backend').'/Banner'] = "backend/Banner/listar";
$route[$this->config->item('path_backend').'/Banner/buscar/(:num)'] = "backend/Banner/buscar/$1";
$route[$this->config->item('path_backend').'/Banner/buscar'] = "backend/Banner/buscar";
$route[$this->config->item('path_backend').'/Banner/export'] = "backend/Banner/export";
$route[$this->config->item('path_backend').'/Banner/agregar/(:num)'] = "backend/Banner/agregar/$1";
$route[$this->config->item('path_backend').'/Banner/editar/(:num)/(:num)'] = "backend/Banner/editar/$1/$2";

#Contacto
$route[$this->config->item('path_backend').'/Contacto/(:num)'] = "backend/Contacto/listar/$1";
$route[$this->config->item('path_backend').'/Contacto'] = "backend/Contacto/listar";
$route[$this->config->item('path_backend').'/Contacto/buscar/(:num)'] = "backend/Contacto/buscar/$1";
$route[$this->config->item('path_backend').'/Contacto/buscar'] = "backend/Contacto/buscar";
$route[$this->config->item('path_backend').'/Contacto/export'] = "backend/Contacto/export";
$route[$this->config->item('path_backend').'/Contacto/agregar/(:num)'] = "backend/Contacto/agregar/$1";
$route[$this->config->item('path_backend').'/Contacto/editar/(:num)/(:num)'] = "backend/Contacto/editar/$1/$2";

#Local_Empresa
$route[$this->config->item('path_backend').'/Local_Empresa/(:num)'] = "backend/Local_empresa/listar/$1";
$route[$this->config->item('path_backend').'/Local_Empresa'] = "backend/Local_empresa/listar";
$route[$this->config->item('path_backend').'/Local_Empresa/buscar/(:num)'] = "backend/Local_empresa/buscar/$1";
$route[$this->config->item('path_backend').'/Local_Empresa/buscar'] = "backend/Local_empresa/buscar";
$route[$this->config->item('path_backend').'/Local_Empresa/export'] = "backend/Local_empresa/export";
$route[$this->config->item('path_backend').'/Local_Empresa/agregar/(:num)'] = "backend/Local_empresa/agregar/$1";
$route[$this->config->item('path_backend').'/Local_Empresa/editar/(:num)/(:num)'] = "backend/Local_empresa/editar/$1/$2";
$route[$this->config->item('path_backend').'/Local_Empresa/editar/(:num)/(:num)/U'] = "backend/Local_empresa/editar/$1/$2/U";
$route[$this->config->item('path_backend').'/Local_Empresa/editar/(:num)/(:num)/H'] = "backend/Local_empresa/editar/$1/$2/H";

#Carta
$route[$this->config->item('path_backend').'/Carta'] = "backend/Carta/listar";
$route[$this->config->item('path_backend').'/Carta/(:num)'] = "backend/Carta/listar/$1";
$route[$this->config->item('path_backend').'/Carta/buscar'] = "backend/Carta/buscar";
$route[$this->config->item('path_backend').'/Carta/buscar/(:num)'] = "backend/Carta/buscar/$1";
$route[$this->config->item('path_backend').'/Carta/agregar'] = "backend/Carta/agregar";
$route[$this->config->item('path_backend').'/Carta/export'] = "backend/Carta/export";
$route[$this->config->item('path_backend').'/Carta/carga_masiva'] = "backend/Carta/carga_masiva";
$route[$this->config->item('path_backend').'/Carta/carga_imagenes'] = "backend/Carta/carga_imagenes";
$route[$this->config->item('path_backend').'/Carta/editar/(:num)'] = "backend/Carta/editar/$1";
$route[$this->config->item('path_backend').'/Carta/editar/(:num)/F'] = "backend/Carta/editar/$1/F";

#Categoria_Carta
$route[$this->config->item('path_backend').'/Categoria_Carta'] = "backend/Categoria_carta/listar";
$route[$this->config->item('path_backend').'/Categoria_Carta/(:num)'] = "backend/Categoria_carta/listar/$1";
$route[$this->config->item('path_backend').'/Categoria_Carta/buscar'] = "backend/Categoria_carta/buscar";
$route[$this->config->item('path_backend').'/Categoria_Carta/buscar/(:num)'] = "backend/Categoria_carta/buscar/$1";
$route[$this->config->item('path_backend').'/Categoria_Carta/agregar'] = "backend/Categoria_carta/agregar";
$route[$this->config->item('path_backend').'/Categoria_Carta/export'] = "backend/Categoria_carta/export";
$route[$this->config->item('path_backend').'/Categoria_Carta/editar/(:num)'] = "backend/Categoria_carta/editar/$1";

#Foto_Carta
$route[$this->config->item('path_backend').'/Foto_Carta/(:num)'] = "backend/Foto_carta/listar/$1";
$route[$this->config->item('path_backend').'/Foto_Carta'] = "backend/Foto_carta/listar";
$route[$this->config->item('path_backend').'/Foto_Carta/buscar/(:num)'] = "backend/Foto_carta/buscar/$1";
$route[$this->config->item('path_backend').'/Foto_Carta/buscar'] = "backend/Foto_carta/buscar";
$route[$this->config->item('path_backend').'/Foto_Carta/export'] = "backend/Foto_carta/export";
$route[$this->config->item('path_backend').'/Foto_Carta/agregar/(:num)'] = "backend/Foto_carta/agregar/$1";
$route[$this->config->item('path_backend').'/Foto_Carta/editar/(:num)/(:num)'] = "backend/Foto_carta/editar/$1/$2";


#Usuario
$route[$this->config->item('path_backend').'/Usuario'] = "backend/Usuario/listar";
$route[$this->config->item('path_backend').'/Usuario/(:num)'] = "backend/Usuario/listar/$1";
$route[$this->config->item('path_backend').'/Usuario/buscar'] = "backend/Usuario/buscar";
$route[$this->config->item('path_backend').'/Usuario/buscar/(:num)'] = "backend/Usuario/buscar/$1";
$route[$this->config->item('path_backend').'/Usuario/agregar'] = "backend/Usuario/agregar";
$route[$this->config->item('path_backend').'/Usuario/export'] = "backend/Usuario/export";
$route[$this->config->item('path_backend').'/Usuario/editar/(:num)'] = "backend/Usuario/editar/$1";
$route[$this->config->item('path_backend').'/Usuario/listar_empresa'] = "backend/Usuario/listar_empresa";
$route[$this->config->item('path_backend').'/Usuario/export_user_empresa'] = "backend/Usuario/export_user_empresa";
$route[$this->config->item('path_backend').'/Usuario/listar_empresa/(:num)'] = "backend/Usuario/listar_empresa/$1";
$route[$this->config->item('path_backend').'/Usuario/buscar_empresa'] = "backend/Usuario/buscar_empresa";
$route[$this->config->item('path_backend').'/Usuario/buscar_empresa/(:num)'] = "backend/Usuario/buscar_empresa/$1";

#Formula_Puntos
$route[$this->config->item('path_backend').'/Formula_Puntos'] = "backend/Formula_puntos/listar";
$route[$this->config->item('path_backend').'/Formula_Puntos/(:num)'] = "backend/Formula_puntos/listar/$1";
$route[$this->config->item('path_backend').'/Formula_Puntos/buscar'] = "backend/Formula_puntos/buscar";
$route[$this->config->item('path_backend').'/Formula_Puntos/buscar/(:num)'] = "backend/Formula_puntos/buscar/$1";
$route[$this->config->item('path_backend').'/Formula_Puntos/agregar'] = "backend/Formula_puntos/agregar";
$route[$this->config->item('path_backend').'/Formula_Puntos/export'] = "backend/Formula_puntos/export";
$route[$this->config->item('path_backend').'/Formula_Puntos/editar/(:num)'] = "backend/Formula_puntos/editar/$1";

#Ubicacion_Local
$route[$this->config->item('path_backend').'/Local_Ubicacion/(:num)'] = "backend/Local_ubicacion/listar/$1";
$route[$this->config->item('path_backend').'/Local_Ubicacion'] = "backend/Local_ubicacion/listar";
$route[$this->config->item('path_backend').'/Local_Ubicacion/buscar/(:num)'] = "backend/Local_ubicacion/buscar/$1";
$route[$this->config->item('path_backend').'/Local_Ubicacion/buscar'] = "backend/Local_ubicacion/buscar";
$route[$this->config->item('path_backend').'/Local_Ubicacion/export'] = "backend/Local_ubicacion/export";
$route[$this->config->item('path_backend').'/Local_Ubicacion/agregar/(:num)/(:num)'] = "backend/Local_ubicacion/agregar/$1/$2";
$route[$this->config->item('path_backend').'/Local_Ubicacion/editar/(:num)/(:num)/(:num)'] = "backend/Local_ubicacion/editar/$1/$2/$3";

#Horario_Local
$route[$this->config->item('path_backend').'/Local_Horario/(:num)'] = "backend/Local_horario/listar/$1";
$route[$this->config->item('path_backend').'/Local_Horario'] = "backend/Local_horario/listar";
$route[$this->config->item('path_backend').'/Local_Horario/buscar/(:num)'] = "backend/Local_horario/buscar/$1";
$route[$this->config->item('path_backend').'/Local_Horario/buscar'] = "backend/Local_horario/buscar";
$route[$this->config->item('path_backend').'/Local_Horario/export'] = "backend/Local_horario/export";
$route[$this->config->item('path_backend').'/Local_Horario/agregar/(:num)/(:num)'] = "backend/Local_horario/agregar/$1/$2";
$route[$this->config->item('path_backend').'/Local_Horario/editar/(:num)/(:num)/(:num)'] = "backend/Local_horario/editar/$1/$2/$3";


##########################################################

################### api #############################
//$route[$this->config->item('path_api')] = "inicio";
$route[$this->config->item('path_api').'/tracking'] = "api/tracking/index";
$route[$this->config->item('path_api').'/usuario'] = "api/usuario/index";


##########################################################

################### api-dashboard #############################

$route[$this->config->item('path_dashboard')] = "api-dashboard/inicio/index";


##########################################################

################### dashboard #############################

$route[$this->config->item('path_dashboard').'/logout'] = "dashboard/Inicio/logout";


#Inicio
$route[$this->config->item('path_dashboard')] = "dashboard/Inicio";
$route[$this->config->item('path_dashboard').'/Principal'] = "dashboard/Principal";
$route[$this->config->item('path_dashboard').'/Cliente'] = "dashboard/Cliente/listar";
$route[$this->config->item('path_dashboard').'/Cliente/(:num)'] = "dashboard/Cliente/listar/$1";
$route[$this->config->item('path_dashboard').'/Cliente/buscar'] = "dashboard/Cliente/buscar";
$route[$this->config->item('path_dashboard').'/Cliente/buscar/(:num)'] = "dashboard/Cliente/buscar/$1";
$route[$this->config->item('path_dashboard').'/Cliente/detalle/(:num)'] = "dashboard/Cliente/detalle/$1";
$route[$this->config->item('path_dashboard').'/Cliente/historial/(:num)'] = "dashboard/Cliente/historial/$1";
$route[$this->config->item('path_dashboard').'/Cliente/historial_buscar/(:num)'] = "dashboard/Cliente/historial_buscar/1";
$route[$this->config->item('path_dashboard').'/Cliente/descuento/(:num)'] = "dashboard/Cliente/descuento/$1";
$route[$this->config->item('path_dashboard').'/Cliente/descuento/(:num)'] = "dashboard/Cliente/descuento/$1";
$route[$this->config->item('path_dashboard').'/Cliente/detalle_agregar_puntos/(:num)'] = "dashboard/Cliente/detalle_agregar_puntos/$1";
$route[$this->config->item('path_dashboard').'/Cliente/detalle_agregar_puntos/(:num)/(:num)'] = "dashboard/Cliente/detalle_agregar_puntos/$1/$2";
$route[$this->config->item('path_dashboard').'/Cliente/detalle_canjear_puntos/(:num)'] = "dashboard/Cliente/detalle_canjear_puntos/$1";
$route[$this->config->item('path_dashboard').'/Cliente/detalle_canjear_puntos/(:num)/(:num)'] = "dashboard/Cliente/detalle_canjear_puntos/$1/$2";
$route[$this->config->item('path_dashboard').'/Cliente/canjear_puntos/(:num)'] = "dashboard/Cliente/canjear_puntos/$1";
$route[$this->config->item('path_dashboard').'/Cliente/agregar_puntos/(:num)'] = "dashboard/Cliente/agregar_puntos/$1";

$route[$this->config->item('path_dashboard').'/Promocion'] = "dashboard/Promocion/listar";
$route[$this->config->item('path_dashboard').'/Promocion/(:num)'] = "dashboard/Promocion/listar/$1";
$route[$this->config->item('path_dashboard').'/Promocion/buscar/(:num)'] = "dashboard/Promocion/buscar/$1";
$route[$this->config->item('path_dashboard').'/Promocion/detalle/(:num)'] = "dashboard/Promocion/detalle/$1";
$route[$this->config->item('path_dashboard').'/Beneficio'] = "dashboard/Beneficio/listar";
$route[$this->config->item('path_dashboard').'/Beneficio/(:num)'] = "dashboard/Beneficio/listar/$1";
$route[$this->config->item('path_dashboard').'/Beneficio/buscar/(:num)'] = "dashboard/Beneficio/buscar/$1";
$route[$this->config->item('path_dashboard').'/Beneficio/detalle/(:num)'] = "dashboard/Beneficio/detalle/$1";

$route[$this->config->item('path_dashboard').'/Carta'] = "dashboard/Carta/listar";
$route[$this->config->item('path_dashboard').'/Carta/(:num)'] = "dashboard/Carta/listar/$1";
$route[$this->config->item('path_dashboard').'/Carta/buscar/(:num)'] = "dashboard/Carta/buscar/$1";
$route[$this->config->item('path_dashboard').'/Carta/detalle/(:num)'] = "dashboard/Carta/detalle/$1";

$route[$this->config->item('path_dashboard').'/Reserva'] = "dashboard/Reserva/listar";
$route[$this->config->item('path_dashboard').'/Reserva/(:num)'] = "dashboard/Reserva/listar/$1";
$route[$this->config->item('path_dashboard').'/Reserva/buscar'] = "dashboard/Reserva/buscar";
$route[$this->config->item('path_dashboard').'/Reserva/buscar/(:num)'] = "dashboard/Reserva/buscar/$1";
$route[$this->config->item('path_dashboard').'/Reserva/detalle/(:num)'] = "dashboard/Reserva/detalle/$1";
$route[$this->config->item('path_dashboard').'/Reserva/horarios_reserva/(:num)'] = "dashboard/Reserva/horarios_reserva/$1";
$route[$this->config->item('path_dashboard').'/Reserva/revisar_fechas_reservas'] = "dashboard/Reserva/revisar_fechas_reservas";


$route[$this->config->item('path_dashboard').'/Horario_exclusion'] = "dashboard/Horario_exclusion/listar";
$route[$this->config->item('path_dashboard').'/Horario_exclusion/(:num)'] = "dashboard/Horario_exclusion/listar/$1";
$route[$this->config->item('path_dashboard').'/Horario_exclusion/buscar'] = "dashboard/Horario_exclusion/buscar";
$route[$this->config->item('path_dashboard').'/Horario_exclusion/buscar/(:num)'] = "dashboard/Horario_exclusion/buscar/$1";
$route[$this->config->item('path_dashboard').'/Horario_exclusion/agregar_view'] = "dashboard/Horario_exclusion/agregar_view";
$route[$this->config->item('path_dashboard').'/Horario_exclusion/detalle/(:num)'] = "dashboard/Horario_exclusion/detalle/$1";



##########################################################


################### frontend #############################

$route[$this->config->item('path_fronted')] = "frontend/inicio/index";
$route['home'] = "frontend/inicio/index";

// $route[$this->config->item('path_fronted').'historias'] = "frontend/historia/index";
$route['historias-perfectas'] = "frontend/historia/index";

#################### ADMIN ############################
// $route['admin'] = 'admin/dashboard';
$route['admin'] = 'admin/admin';
$route['admin/login'] = 'admin/admin/login'; 
$route['admin/logout'] = 'admin/admin/logout'; 
$route['admin/stories/export'] = 'admin/admin/exportStories'; 



##########################################################
