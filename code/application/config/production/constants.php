<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
define('EXIT_SUCCESS', 0); // no errors
define('EXIT_ERROR', 1); // generic error
define('EXIT_CONFIG', 3); // configuration error
define('EXIT_UNKNOWN_FILE', 4); // file not found
define('EXIT_UNKNOWN_CLASS', 5); // unknown class
define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
define('EXIT_USER_INPUT', 7); // invalid user input
define('EXIT_DATABASE', 8); // database error
define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code





#constantes generales

define('CORREO_PROTOCOL','smtp');
define('CORREO_HOST','ssl://smtp.gmail.com');
define('CORREO_PUERTO',465);
define('CORREO_NOMBRE','suregaloperfecto@gmail.com');
define('CORREO_CONTRASENIA','K@iser2006');

/*
define('CORREO_PROTOCOL','smtp');
define('CORREO_HOST','smtp.sendgrid.net');
define('CORREO_PUERTO',587);
define('CORREO_NOMBRE','noreply@adminbancoripley.com.pe');
define('CORREO_CONTRASENIA','$R1Pl3y653._-$');
*/

define('REMITENTE_NOMBRE','Su Regalo Perfecto');
define('CORREO_SUBJECT','Formulario de Contacto');
define('CORREO_DESTINO','gabriela.ramos@faber-castell.com.pe');

define('STRING_ESTADO_ACTIVO', 't');
define('STRING_ESTADO_INACTIVO', 't');

define('PLATAFORMA_WEB', '1');
define('PLATAFORMA_MOVIL', '2');

define('ESTADO_ACTIVO', 1);
define('ESTADO_INACTIVO', 0);
define('ESTADO_ELIMINADO', 2);

define('LOCAL_DEFAULT', 1);

define('ACEPTADO', 'Aceptado');
define('PENDIENTE', 'Pendiente');
define('RECHAZADO', 'Rechazado');
define('ASISTIDO', 'Asistido');

define('ESTADO_ACEPTADO', 1);
define('ESTADO_PENDIENTE', '0');
define('ESTADO_RECHAZADO', 2);
define('ESTADO_ASISTIDO', 3);

define('FOTO_LISTADO',1);
define('FOTO_DETALLE',2);


define('ESTADO_RESPUESTA_OK', '1');
define('ESTADO_RESPUESTA_ERROR', '0');

define('DESCRIPCION_ESTADO_ACTIVO', 'Activo');
define('DESCRIPCION_ESTADO_INACTIVO', 'Inactivo');
define('DESCRIPCION_ESTADO_ELIMINADO', 'Eliminado');

define('ESTADO_PETICION_OK', 200);
define('ESTADO_PETICION_ERROR_INTERNO_SERVIDOR', 500);
define('ESTADO_PETICION_ERROR_NO_AUTORIZADO', 401);
define('ESTADO_PETICION_ERROR_RECURSO_NO_ENCONTRADO', 404);


define('NOMBRE_METODO_POST', 'post');
define('NOMBRE_METODO_PUT', 'put');
define('NOMBRE_METODO_GET', 'get');
define('NOMBRE_METODO_DELETE', 'delete');


define('ACTIVAR_LOG', '1');
define('DESACTIVAR_LOG', '0');

define('DAY_WEEK', serialize(   
                            array(
                                array("id" =>1 , "dia" => "Lunes"),
                                array("id" =>2 , "dia" => "Martes"),
                                array("id" =>3 , "dia" => "Miercoles"),
                                array("id" =>4 , "dia" => "Jueves"),
                                array("id" =>5 , "dia" => "Viernes"),
                                array("id" =>6 , "dia" => "Sabado"),
                                array("id" =>7 , "dia" => "Domingo")
                                )
                            )
    );

define('MONTHS', serialize(
                   array(
                       array("id" =>1 , "mes" => "Enero"),
                       array("id" =>2 , "mes" => "Febrero"),
                       array("id" =>3 , "mes" => "Marzo"),
                       array("id" =>4 , "mes" => "Abril"),
                       array("id" =>5 , "mes" => "Mayo"),
                       array("id" =>6 , "mes" => "Junio"),
                       array("id" =>7 , "mes" => "Julio"),
                       array("id" =>8 , "mes" => "Agosto"),
                       array("id" =>9 , "mes" => "Setiembre"),
                       array("id" =>10 , "mes" => "Octubre"),
                       array("id" =>11 , "mes" => "Noviembre"),
                       array("id" =>12 , "mes" => "Diciembre"),
                   )
               )
);

define('DASHBOARD_ADMIN_COD', '1');
define('DASHBOARD_MOZO_COD', '0');

define('DASHBOARD_ADMIN_NOMBRE', 'ADMIN');
define('DASHBOARD_MOZO_NOMBRE', 'MOZO');

define('STATUS_ACCEPTED', 1);
define('STATUS_PENDING', 0);
define('STATUS_DENIED', 2);
define('STATUS_ASSISTED', 3);

define('STATUS_ALL_LOCALS', 0);
define('STATUS_SOME_LOCALS', 1);
define('STATUS_DELIVERY', 2);


/*NOTIFICATION*/
define('TYPE_NOTIFICATION_EMAIL','1');
define('TYPE_NOTIFICATION_PUSH','2');
define('TYPE_NOTIFICATION_EMAIL_ACTIVE','1');
define('TYPE_NOTIFICATION_PUSH_ACTIVE','1');

define('TYPE_NOTIFICATION_INFORMATIVE','1');
define('TYPE_NOTIFICATION_LINKED_APP','3');
define('TYPE_NOTIFICATION_LINKED_EXTERNAL','2');

define('TYPE_SEND_MASSIVE','1');
define('TYPE_SEND_PERSONALIZED','2');

define('INTERNAL_APP_TYPE_PROMOTION', '1');
define('INTERNAL_APP_TYPE_BENEFIT', '2');
define('INTERNAL_APP_TYPE_MENU', '3');
define('INTERNAL_APP_TYPE_PROFILE', '4');
define('INTERNAL_APP_TYPE_RESERVE', '5');

define('TYPE_BENEFIT', 'BENEFIT');
define('TYPE_MENU', 'MENU');
define('TYPE_PROMOTION', 'PROMOTION');

define('MAX_TOKEN_PUSH_POR_ENVIO_ANDROID',40);


define('SUCCESS_SEND_PUSH','ENVIADO');
define('RECEIVED_SEND_PUSH','RECIBIDO');
define('SEEN_SEND_PUSH','VISTO');
define('OTHER_SEND_PUSH','OTRO');
define('ERROR_SEND_PUSH','FALLIDO');

define('CODE_SUCCESS_SEND_PUSH','0');
define('CODE_RECEIVED_SEND_PUSH','2');
define('CODE_SEEN_SEND_PUSH','1');
define('CODE_OTHER_SEND_PUSH','4');
define('CODE_ERROR_SEND_PUSH','3');

define('NOTIFICATION_IOS','2');
define('NOTIFICATION_ANDROID','3');

define('NOTIFICATION_INFORMATIVE','Informativo');
define('NOTIFICATION_LINKED_APP','Enlazado app');
define('NOTIFICATION_LINKED_EXTERNAL','Enlazado externo');



define('API_KEY_FIREBASE',"AAAAizx6iC8:APA91bHpJznpY15fCIsp2LyS-n835UHJt4p4o0p99-G3Aad53zWIcpjJ7d_V2mhhV4jcbwx1q2kln4FDfYF19us0dVUAnLVes8MPsmpOZCv1HQ2ThPycQDdzVCJB49ICnVPfS4fQkAF9");

define('EVENT_SENDGRID_TYPE_STATUS_DELETE','dropped');
define('EVENT_SENDGRID_TYPE_STATUS_PROCESSED','processed');
define('EVENT_SENDGRID_TYPE_STATUS_REBOUND','bounce');
define('EVENT_SENDGRID_TYPE_STATUS_DELIVERED','delivered');
define('EVENT_SENDGRID_TYPE_STATUS_OPEN','open');
define('EVENT_SENDGRID_TYPE_STATUS_CLICK','click');
define('EVENT_SENDGRID_TYPE_STATUS_UNSUBSCRIBE','unsubscribe');
define('EVENT_SENDGRID_TYPE_STATUS_SPAM','spam report');
define('EVENT_SENDGRID_TYPE_STATUS_DEFERRED','deferred');

define('VALUE_EVENT_SENDGRID_TYPE_STATUS_DELETE','Eliminado');
define('VALUE_EVENT_SENDGRID_TYPE_STATUS_PROCESSED','Procesado');
define('VALUE_EVENT_SENDGRID_TYPE_STATUS_REBOUND','Rebote');
define('VALUE_EVENT_SENDGRID_TYPE_STATUS_DELIVERED','Entregado');
define('VALUE_EVENT_SENDGRID_TYPE_STATUS_OPEN','Abierto');
define('VALUE_EVENT_SENDGRID_TYPE_STATUS_CLICK','Click');
define('VALUE_EVENT_SENDGRID_TYPE_STATUS_UNSUBSCRIBE','Lista Negra');
define('VALUE_EVENT_SENDGRID_TYPE_STATUS_SPAM','Spam');
define('VALUE_EVENT_SENDGRID_TYPE_STATUS_DEFERRED','Diferido');
define('VALUE_EVENT_SENDGRID_TYPE_STATUS_UNIDENTIFIED','No identificado');
define('VALUE_EVENT_SENDGRID_TYPE_STATUS_FAILED','Fallido');
define('VALUE_EVENT_SENDGRID_TYPE_STATUS_PENDING','Pendiente');

define('VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_DELETE','5');
define('VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_PROCESSED','0');
define('VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_REBOUND','4');
define('VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_DELIVERED','1');
define('VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_OPEN','2');
define('VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_CLICK','3');
define('VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_UNSUBSCRIBE','6');
define('VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_SPAM','7');
define('VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_DEFERRED','8');
define('VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_UNIDENTIFIED','9');
define('VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_FAILED','10');
define('VALUE_CODE_EVENT_SENDGRID_TYPE_STATUS_PENDING','11');


//define('ESTADO_PETICION_ERROR_PAGINA_NO_ENCONTRADA', 404);



##############################CONTANTES API#################################
define('ACCION_LISTAR_PROMOCION','promocion/listar');
define('ACCION_DETALLE_PROMOCION','promocion/detalle');
define('ACCION_BUSQUEDA_PROMOCION','promocion/buscar');
define('ACCION_LISTAR_PROMOCION_POR_CATEGORIA','promocion/categoria/');

define('ACCION_LISTAR_BENEFICIO', 'twitter/listar');
define('ACCION_DETALLE_BENEFICIO', 'beneficio/detalle/');
define('ACCION_BUSQUEDA_BENEFICIO', 'beneficio/buscar?termino=');
define('ACCION_LISTAR_BENEFICIO_POR_CATEGORIA', 'beneficio/categoría/');

define('ACCION_LISTAR_PUNTOS','puntos/obtener_puntos');

define('ACCION_OBTENER_PUNTOS_USUARIO','puntos/obtener_puntos_usuario');

define('ACCION_LISTAR_CANJE','canje/obtener_canjes');

define('ACCION_LISTAR_CARTA','carta/listar');
define('ACCION_LISTAR_CARTA_CATEGORIA','carta/listar_por_categoria');
define('ACCION_DETALLE_CARTA','carta/detalle');
define('ACCION_BUSQUEDA_CARTA','carta/busar');


define('ACCION_REGISTRAR_USUARIO','usuario/registrar_usuario');
define('ACCION_INICIAR_SESION','usuario/iniciar_sesion');
define('ACCION_INICIAR_SESION_RED_SOCIAL','usuario/iniciar_sesion_red_social');

define('ACCION_LISTAR_SERVICIO_VIP', 'servicio/listar');
define('ACCION_DETALLE_SERVICIO_VIP', 'servicio/detalle/');

define('ACCION_LISTAR_PARTICIPANTE', 'participante/listar');
define('ACCION_DETALLE_PARTICIPANTE', 'participante/detalle/');


define('ACCION_OBTENER_PERFIL', 'usuario/perfil');
define('ACCION_ACTUALIZAR_PERFIL', 'usuario/actualizar-perfil');

define('ACCION_CERRAR_SESION','usuario/cerrar_sesion');
define('ACCION_ACTUALIZAR_USUARIO','usuario/actualizar');


define('ACCION_OBTENER_HORARIO','horario_reserva/obtener_horario');

define('ACCION_OBTENER_LOCALES','local/obtener_locales');
define('ACCION_OBTENER_LOCAL_PAQUETES','local/obtener_local_paquetes');

define('ACCION_REGISTRAR_SOLICITUD_RESERVA','reserva_solicitud/registrar_solicitud');

define('ACCION_REGISTRAR_TOKEN_PUSH_USUARIO', 'usuario/regitrar_token_push');

define('PUNTOS_INICIALES_REGISTRO','0');


define('ID_TIPO_IMAGEN_WEB_LISTADO', '1');
define('ID_TIPO_IMAGEN_WEB_DETALLE', '2');

define('URL_HOST_IMAGENES', 'http://volacomoelpresidente.preview.pe/gateway/');
define('IMAGEN_POR_DEFECTO_DETALLE', 'upload/imagenes_por_defecto/imagen_detalle.jpg');
define('IMAGEN_POR_DEFECTO_LISTADO', 'upload/imagenes_por_defecto/imagen_listado.jpg');
define('IMAGEN_POR_DEFECTO_MAPA_PIN', 'upload/imagenes_por_defecto/imagen_mapa_pin.jpg');

define('PAGINACION_PUNTOS', '10');
define('PAGINACION_CANJES', '10');
define('PAGINACION_BENEFICIOS', '8');
define('PAGINACION_PROMOCIONES', '8');
define('PAGINACION_CARTA','8');
define('PAGINACION_USUARIOS','10');
define('PAGINACION_MENSAJES','10');
############################################################################



########################### CONTANTES CMS ###################################

define('ID_TIPO_USUARIO_SUPER_ADMINISTRADOR', 1);
define('ID_TIPO_USUARIO_ADMINISTRADOR', 2);

define('DESCRIPCION_TIPO_USUARIO_SUPER_ADMINISTRADOR', "Super Administrador");
define('DESCRIPCION_TIPO_USUARIO_ADMINISTRADOR', "Administrador CMS");

define('ELEMENTOS_POR_PAGINA',10);
define('MAX_INSERT_BATCH', 100);

define('NOMBRE_CONTROLADOR_DASHBOARD', 'dashboard');
define('NOMBRE_CONTROLADOR_USUARIO', 'usuario');
define('BD_LOG_CMS', 'tbl_log_cms');
#################################################################################


########################### CONTANTES DASHBOARD #################################



define('ACCION_DASHBOARD_INICIAR_SESION','api-dashboard/usuario/iniciar_sesion');
define('ACCION_DASHBOARD_CERRAR_SESION','api-dashboard/usuario/cerrar_sesion');

define('ACCION_DASHBOARD_CONFIGURAR_SUPER_ADMIN','api-dashboard/usuario/configurar_local_super_admin');
define('ACCION_DASHBOARD_PRINCIPAL','api-dashboard/principal/obtener');

define('ACCION_DASHBOARD_LISTAR_PROMOCION','api-dashboard/promocion/listar');
define('ACCION_DASHBOARD_FILTRAR_PROMOCION','api-dashboard/promocion/filtrar');
define('ACCION_DASHBOARD_DETALLE_PROMOCION','api-dashboard/promocion/detalle');
define('ACCION_DASHBOARD_ELIMINAR_PROMOCION','api-dashboard/promocion/eliminar');
define('ACCION_DASHBOARD_LISTAR_PROMOCION_NOTIFICACION','api-dashboard/promocion/obtener_promociones_notificacion');

define('ACCION_DASHBOARD_LISTAR_BENEFICIO','api-dashboard/beneficio/listar');
define('ACCION_DASHBOARD_FILTRAR_BENEFICIO','api-dashboard/beneficio/filtrar');
define('ACCION_DASHBOARD_DETALLE_BENEFICIO','api-dashboard/beneficio/detalle');
define('ACCION_DASHBOARD_ELIMINAR_BENEFICIO','api-dashboard/beneficio/eliminar');
define('ACCION_DASHBOARD_LISTAR_BENEFICIOS_NOTIFICACION','api-dashboard/beneficio/obtener_beneficios_notificacion');

define('ACCION_DASHBOARD_OBTENER_CLIENTES','api-dashboard/cliente/obtener_clientes');
define('ACCION_DASHBOARD_BUSCAR_CLIENTE','api-dashboard/cliente/buscar_cliente');
define('ACCION_DASHBOARD_BUSCAR_CLIENTE_DNI','api-dashboard/cliente/buscar_cliente_dni');
define('ACCION_DASHBOARD_BUSCAR_CLIENTE_TARJETA','api-dashboard/cliente/buscar_cliente_tarjeta');
define('ACCION_DASHBOARD_DETALLE_AGREGAR_PUNTOS','api-dashboard/cliente/detalle_agregar_puntos');
define('ACCION_DASHBOARD_AGREGAR_PUNTOS','api-dashboard/cliente/agregar_puntos');
define('ACCION_DASHBOARD_DETALLE_CANJEAR_PUNTOS','api-dashboard/cliente/detalle_canjear_puntos');
define('ACCION_DASHBOARD_CANJEAR_PUNTOS','api-dashboard/cliente/canjear_puntos');
define('ACCION_DASHBOARD_OBTENER_HISTORIAL','api-dashboard/cliente/obtener_historial');
define('ACCION_DASHBOARD_FILTRAR_HISTORIAL','api-dashboard/cliente/filtrar_historial');
define('ACCION_DASHBOARD_DETALLE_DESCUENTO','api-dashboard/cliente/detalle_descuento');
define('ACCION_DASHBOARD_REALIZAR_DESCUENTO','api-dashboard/cliente/realizar_descuento');
define('ACCION_DASHBOARD_OBTENER_CLIENTE','api-dashboard/cliente/obtener_cliente');
define('ACCION_DASHBOARD_OBTENER_PUNTOS','api-dashboard/cliente/obtener_puntos');
define('ACCION_DASHBOARD_OBTENER_CANJES','api-dashboard/cliente/obtener_canje');
define('ACCION_DASHBOARD_OBTENER_BENEFICIOS','api-dashboard/cliente/obtener_beneficios');
define('ACCION_DASHBOARD_OBTENER_TIPO_PAGOS','api-dashboard/cliente/obtener_tipo_pagos');
define('ACCION_DASHBOARD_LISTAR_CLIENTE','api-dashboard/cliente/listar_clientes');
define('ACCION_DASHBOARD_DETALLE_CLIENTE','api-dashboard/cliente/detalle');
define('ACCION_DASHBOARD_FILTRAR_CLIENTE', 'api-dashboard/cliente/filtrar_clientes');
define('ACCION_DASHBOARD_GENERAR_EXCEL_CLIENTE', 'api-dashboard/cliente/generar_excel');
define('ACCION_DASHBOARD_OBTENER_LISTA_CLIENTES_NOTIFICACION','api-dashboard/cliente/obtener_clientes_notificacion');


define('ACCION_DASHBOARD_CARTA_OBTENER','api-dashboard/carta/listar');
define('ACCION_DASHBOARD_CARTA_OBTENER_POR_CATEGORIA','api-dashboard/carta/listar_por_categoria');
define('ACCION_DASHBOARD_CARTA_DETALLE','api-dashboard/carta/detalle');
define('ACCION_DASHBOARD_CARTA_BUSCAR','api-dashboard/carta/buscar');
define('ACCION_DASHBOARD_CARTA_OBTENER_LISTA_NOTIFICACION','api-dashboard/carta/obtener_cartas_notificacion');

define('ACCION_DASHBOARD_OBTENER_PRINCIPAL','api-dashboard/principal/obtener');


define('ACCION_DASHBOARD_OBTENER_RESERVAS','api-dashboard/reserva_solicitud/obtener_reservas');
define('ACCION_DASHBOARD_BUSCAR_RESERVA','api-dashboard/reserva_solicitud/filtrar_reservas');
define('ACCION_DASHBOARD_DETALLE_RESERVA','api-dashboard/reserva_solicitud/detalle');
define('ACCION_DASHBOARD_EDITAR_RESERVA','api-dashboard/reserva_solicitud/editar_reserva');
define('ACCION_DASHBOARD_EDITAR_HORARIO_RESERVA','api-dashboard/reserva_solicitud/editar_horario_reserva');
define('ACCION_DASHBOARD_FECHA_RESERVA','api-dashboard/reserva_solicitud/buscar_reserva_fecha');
define('ACCION_DASHBOARD_RECEPCION_RESERVA','api-dashboard/reserva_solicitud/recepcion_reserva');
define('ACCION_DASHBOARD_GENERAR_EXCEL_RESERVA','api-dashboard/reserva_solicitud/generar_excel');

define('ACCION_DASHBOARD_OBTENER_HORARIO_EXCLUSION','api-dashboard/horario_exclusion/obtener_horarios_exclusion');
define('ACCION_DASHBOARD_BUSCAR_HORARIO_EXCLUSION','api-dashboard/horario_exclusion/filtrar_horarios_exclusion');
define('ACCION_DASHBOARD_DETALLE_HORARIO_EXCLUSION','api-dashboard/horario_exclusion/detalle_horario_exclusion');
define('ACCION_DASHBOARD_DETALLE_LISTADO_LOCALES','api-dashboard/horario_exclusion/detalle_listado_locales');
define('ACCION_DASHBOARD_AGREGAR_HORARIO_EXCLUSION','api-dashboard/horario_exclusion/agregar_horarios_exclusion');
define('ACCION_DASHBOARD_EDITAR_HORARIO_EXCLUSION','api-dashboard/horario_exclusion/editar_horario_exclusion');
define('ACCION_DASHBOARD_REVISAR_HORARIO_EXCLUSION','api-dashboard/horario_exclusion/revisar_horarios_exclusion');


define('ACCION_DASHBOARD_OBTENER_MOZOS','api-dashboard/mozo/obtener_mozos');
define('ACCION_DASHBOARD_REGISTRAR_CLIENTE','api-dashboard/cliente/registrar_cliente');
define('ACCION_DASHBOARD_RESETEAR_CONTRASENIA','api-dashboard/cliente/resetear_contrasenia');
define('ACCION_DASHBOARD_ASIGNAR_CONTRASENIA','api-dashboard/cliente/asignar_contrasenia');
define('ACCION_DASHBOARD_ASIGNAR_TARJETA','api-dashboard/cliente/asignar_tarjeta');
define('ACCION_DASHBOARD_ELIMINAR_TARJETA','api-dashboard/cliente/eliminar_tarjeta');

define('ACCION_DASHBOARD_MENSAJE_REGISTRAR','api-dashboard/mensaje/registrar');
define('ACCION_DASHBOARD_MENSAJE_LISTADO','api-dashboard/mensaje/obtener_mensajes');
define('ACCION_DASHBOARD_MENSAJE_BUSQUEDA','api-dashboard/mensaje/filtros_mensajes');
define('ACCION_DASHBOARD_MENSAJE_DETALLE','api-dashboard/mensaje/detalle_mensaje');
define('ACCION_DASHBOARD_MENSAJE_GENERAR_EXCEL','api-dashboard/mensaje/general_excel_mensaje');
define('ACCION_DASHBOARD_MENSAJE_ESTADISTICA','api-dashboard/mensaje/estadistica_mensaje');



define('TIEMPO_LIMITE_CONTRASENIA',24); //Horas

#################################################################################

########################### CONTANTES CMS ###################################

define('url_servicios_dashoard', 'http://demo.elclubdebeneficios.com/gateway/index.php/api-dashboard/');
//define('url_servicios_dashoard', 'http://localhost/club-de-beneficios/index.php/api-dashboard/');
#############################################################################

