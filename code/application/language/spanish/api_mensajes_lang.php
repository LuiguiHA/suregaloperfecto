<?php
$lang['usuario_regristro'] = 'Se registro al usuario de manera exitosa.';
$lang['usuario_inicio_sesion'] = 'Ingreso exitoso.';
$lang['usuario_cerrar_sesion'] = 'Cerró sesión de manera exitosa.';

$lang['usuario_perfil'] = 'Se obtuvo el perfil de usuario de manera exitosa';
$lang['usuario_actualizar_sesion'] = 'Se actualizó la sesion del usuario';
$lang['usuario_actualizar_datos'] = 'Se actualizó los datos de manera exitosa.';

$lang['usuario_registro_token_push'] = 'Se registro el token push de  manera exitosa';

$lang['promocion_listar'] = 'Lista de promociones realizada de manera exitosa.';
$lang['promocion_buscar'] = 'Busqueda de promociones realizada de manera exitosa.';
$lang['promocion_detalle'] = 'Detalle de promocion realizada de manera exitosa.';
$lang['promocion_listar_categoria'] = 'Lista de promociones por categoría realizada de manera exitosa.';
$lang['beneficio_listar'] = 'Lista de beneficios realizada de manera exitosa.';
$lang['beneficio_buscar'] = 'Busqueda de beneficios realizada de manera exitosa.';
$lang['beneficio_detalle'] = 'Detalle de beneficios realizada de manera exitosa.';
$lang['beneficio_listar_categoria'] = 'Lista de beneficio por categoría realizada de manera exitosa.';
$lang['puntos_listar'] = 'Lista de puntos realizada de manera exitosa';
$lang['carta_listar'] = 'Listado de carta realizada de manera exitosa';
$lang['carta_listar_categoria'] = 'Listado por categoría realizada de manera exitosa';
$lang['carta_detalle'] = 'El detalle de la carta se realizó de manera exitosa';
$lang['obtener_listado_locales'] = 'Lista de locales realizada con éxito';
$lang['obtener_horario_ubicaciones_locales'] = 'Lista de horarios y ubicaciones  de locales realizada con éxito';
$lang['registro_solicitud_reserva'] = '¡Tu solicitud de reserva se ha enviado con éxito!';
$lang['obtener_puntos_usuario'] = 'Se obtuvo los puntos del usuario de manera exitosa';

$lang['mensaje_estado_actualizado'] = 'Se actualizo el mensaje de manera exitosa';

