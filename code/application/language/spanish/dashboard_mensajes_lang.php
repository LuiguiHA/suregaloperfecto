<?php
/**
 * Created by Luis Alberto Rosas Arce.
 */
$lang['mensaje_operacion_exito'] =  'Operación realizada con éxito';
$lang['inicio_obtener_locales'] = 'Lista de locales realizada con éxito';
$lang['cliente_obtener_cliente'] = 'El listado de clientes se realizó de manera exitosa';
$lang['cliente_busqueda_usuario'] = 'La búsqueda de usuarios se realizó de manera exitosa';
$lang['cliente_detalle_agregar_puntos'] = 'El detalle de agregar puntos se realizó de manera exitosa';
$lang['cliente_agregar_puntos'] = 'Felicidades. Has ganado ';
$lang['cliente_detalle_canjear_puntos'] = 'El detalle de canjerar puntos se realizó de manera exitosa';
$lang['cliente_canjear_puntos'] = 'Felicitaciones acabas de canjear';
$lang['cliente_obtener_historial'] = 'El listado de historial se realizó de manera exitosa';
$lang['cliente_obtener_puntos'] = 'El listado de puntos de usuario se realizó de manera exitosa';
$lang['cliente_obtener_canje'] = 'El listado de canje de usuario se realizó de manera exitosa';
$lang['cliente_filtar_historial'] = 'El filtro de historial se realizó de manera exitosa';
$lang['cliente_detalle_descuento'] = 'El detalle del descuento se realizó de manera exitosa';
$lang['cliente_realizar_descuento'] = 'Se realizó el descuento de manera exitosa';
$lang['cliente_listado'] = 'Lista de usuarios obtenida de manera exitosa';
$lang['cliente_detalle'] = 'Se obtuvo el detalle de manera exitosa';
$lang['cliente_filtro'] = 'Se filtró los usuarios de manera exitosa';

$lang['principal_obtener'] = 'Se realizó el servicio de obtener principal de manera exitosa';


$lang['cliente_buscar_detalle_cliente'] = 'El detalle de cliente se realizó de manera exitosa';
$lang['carta_listar'] = 'El listado de cartas se realizó con exito';
$lang['carta_listar_categoria'] = 'El listado de cartas por categoría se realizó de manera exitosa';
$lang['carta_detalle'] = 'El detalle de la carta se realizó de manera exitosa';
$lang['carta_busqueda'] = 'La búsqueda de la carta se realizó de manera exitosa';


$lang['cliente_obtener_reserva'] = 'El listado de reservas se realizó de manera exitosa';
$lang['cliente_actualizar_reserva'] = 'La reserva se ha actualizado de manera exitosa.';

$lang['cliente_obtener_horarios'] = 'El listado de horarios se realizó de manera exitosa';
$lang['cliente_agregar_horario_exclusion'] = 'El horario de exclusion se agrego de manera exitosa';
$lang['cliente_editar_horario_exclusion'] = 'El horario de exclusion se edito de manera exitosa';
$lang['cliente_revisar_horario_exclusion'] = 'Se reviso los  horarios de exclusion de manera exitosa';

$lang['cliente_busqueda_dni_usuario'] = 'Se encontro al usuario de manera exitosa';

$lang['cliente_obtener_beneficios'] = 'El listado de los beneficios se realizó de manera exitosa';

$lang['cliente_tipo_pagos'] = 'El listado de los tipo de pagos se realizó de manera exitosa';

$lang['cliente_obtener_mozos'] = 'El listado de los mozos se realizó de manera exitosa';

$lang['usuario_registro'] = 'Usuario registrado de manera exitosa';
$lang['contrasena_restablecida'] = "Se ha restablecido la contraseña, verificar su correo.";

$lang['cliente_recepcion_reserva_check'] = 'La reserva ya ha sido recepcionada';
$lang['cliente_recepcion_reserva_actualizar'] = 'La recepcion ha sido actualizada';

$lang['registro_mensaje_email'] = 'Se registro de manera exitosa el envio de email';
$lang['registro_mensaje_push'] = 'Se registro de manera exitosa el envio de push';
$lang['registro_mensaje'] = 'Se registro de manera exitosa el mensaje de push y email';

$lang['registro_tarjeta_cliente'] = 'Se asigno de manera exitosa la tarjeta al cliente';
$lang['registro_tarjeta_cliente_eliminar'] = 'Se desvinculo de manera exitosa la tarjeta al cliente';

