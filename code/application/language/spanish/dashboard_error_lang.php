<?php
/**
 * Created by Luis Alberto Rosas Arce.
 */

$lang['error_perfil'] = 'Su perfil no tiene los permisos suficientes para realizar la operación';
$lang['error_email_vacio'] = 'El email no puede ser vacio.';
$lang['error_correo_vacio'] = 'Correo no ingresado, debe ingresar el correo';
$lang['error_contrasenia_vacio'] = 'Contraseña no ingresada, debe ingresar la contraseña';
$lang['error_local_vacío'] = 'Local no ingresado, debe ingresar el local';
$lang['error_usurio_no_registrado']='Usuario no registrado';
$lang['error_contrasenia_incorrecta'] = 'Contraseña incorrecta';
$lang['error_local_no_encontrado'] = 'Local no encontrado';
$lang['error_local_listado'] = 'No se pudo obtener el listado de locales';

$lang['error_token_recibido']='Token no recibido';
$lang['error_token_encontrado']= 'Usuario no autorizado';
$lang['error_token_valido'] = "Token inválido";


$lang['error_empresa_encontrada'] = 'Empresa no encontradda';

$lang['error_cerrar_sesion']= 'No se pudo cerrar sesión, por favor vuelva a intentarlo';
$lang['error_estadisticas_usuarios'] = "No se pudo obtener las estadísticas de los usuarios";


$lang['error_parametro_url'] = "Parámetro obligatorio no recibido";
$lang['error_parametros'] = "Parámetro no recibido";

$lang['error_obtener_promociones'] = "No se pudo obtener las promociones";
$lang['error_obtener_beneficios'] = "No se pudo obtener los benefios";
$lang['error_obtener_detalle_beneficio'] = "No se pudo obtener el beneficio";
$lang['error_obtener_detalle_descripcion_canje'] = "No se pudo obtener la descripcion del canje";
$lang['error_obtener_detalle_contacto']= "No se pudo obtener el detalle del contacto";
$lang['error_obtener_detalle_informacion_adicional'] = "No se pudo obtener el detalle de la información adicional";
$lang['error_obtener_detalle_foto']="No se pudo obtener el detalle de la foto";
$lang['error_obtener_detalle_locales'] = "No se pudo obtener el detalle de los locales";
$lang['error_obtener_detalle_local'] = "No se pudo obtener el detalle del local";

$lang['error_eliminar_beneficio']= "No se pudo eliminar el beneficio";
$lang['error_eliminar_promocion'] = "No se pudo eliminar la promoción";

$lang['error_obtener_usuarios']= "No se pudo obtener los usuarios";
$lang['error_obtener_clientes']= "No se pudo obtener a los clientes";

$lang['error_obtener_tipo_pago'] = "No se pudo obtener los tipos de pago";
$lang['error_obtener_detalle_cliente'] = "No se pudo obtener el detalle del cliente";
$lang['error_obtener_detalle_puntps'] = "No se pudo obtener el detalle de los puntos";
$lang['error_obtener_detalle_canjes'] = "No se pudo obtener el detalle de los canjes";

$lang['error_guardar_puntos'] = "No se pudo guardar los puntos";
$lang['error_guardar_canje'] = "No se pudo guardar el canje";
$lang['error_guardar_descuento'] = "No se pudo guardar el descuento";

$lang['error_obtener_promociones'] = "No se pudo obtener las promociones";
$lang['error_obtener_promociones_canjeadas'] = "No se pudo obtener las promociones canjeadas";
$lang['error_obtener_detalle_promocion'] = "No se pudo obtener el detalle de la promocion";
$lang['error_detalle_cliente'] = 'No se pudo obtener el detalle del cliente';

$lang['error_obtener_tipo_pago'] = "No se pudo obtener los tipos de pago.";
$lang['error_obtener_detalle_cliente'] = "No se pudo obtener el detalle del cliente.";
$lang['error_obtener_detalle_puntps'] = "No se pudo obtener el detalle de los puntos.";
$lang['error_obtener_detalle_canjes'] = "No se pudo obtener el detalle de los canjes.";

$lang['error_guardar_puntos'] = "No se pudo guardar los puntos.";
$lang['error_guardar_canje'] = "No se pudo guardar el canje.";
$lang['error_guardar_descuento'] = "No se pudo guardar el descuento.";

$lang['error_obtener_promociones'] = "No se pudo obtener las promociones.";
$lang['error_obtener_promociones_canjeadas'] = "No se pudo obtener las promociones canjeadas.";
$lang['error_obtener_detalle_promocion'] = "No se pudo obtener el detalle de la promocion.";

$lang['error_obtener_detalle_cliente'] = 'No se pudo obtener el detalle del cliente.';
$lang['error_canje_insuficiente'] = 'Usted no tiene los puntos suficientes para realizar el canje.';

$lang['error_carta_obtener'] = 'No se pudo obtener el listado de cartas.';
$lang['error_carta_obtener_categoria'] = 'No se pudo obtener las categorias de las cartas.';
$lang['error_carta_obtener_categoria_parametro'] = 'Se debe ingresar el parámetro de categoría';
$lang['error_carta_detalle'] = 'No se pudo obtener el detalle de la carta';
$lang['error_carta_detalle_parametro'] = 'Se debe ingresar el parámetro de la carta';
$lang['error_carta_detalle_foto'] = 'No se pudo obtener el detalle de la foto.';
$lang['error_carta_detalle_empresa'] = 'No se pudo obtener el detalle de la empresa.';
$lang['error_carta_detalle_local'] = 'No se pudo obtener el detalle de los locales.';
$lang['error_carta_busqueda'] = 'No se pudo obtener la busqueda de la carta.';

$lang['error_obtener_puntos'] = 'No se pudo obtener los puntos del usuario';
$lang['error_obtener_canje'] = 'No se pudo obtener los canjes del usuario';


$lang['error_obtener_reservas']= "No se pudo obtener las reservas";
$lang['error_obtener_horarios']= "No se pudo obtener los horarios";
$lang['error_obtener_locales']= "No se pudo obtener los locales";
$lang['error_obtener_detalle_reserva'] = "No hay horarios disponibles para esta fecha";
$lang['error_obtener_listado_ubicacion'] = "No se pudo obtener el listado de la ubicaciones";
$lang['error_actualizar_reserva'] = "No se pudo actualizar la reserva";
$lang['error_obtener_detalle_horario_exclusion'] = "No se pudo obtener el detalle del horario exclusion";
$lang['error_agregar_horario_exclusion']= "No se agregar el horario";
$lang['error_editar_horario_exclusion']= "No se editar el horario";
$lang['error_revisar_horario_exclusion']= "No se revisar los horarios de exclusion";

$lang['error_buscar_dni_usuario']= "No se pudo encontrar al cliente";
$lang['error_buscar_tarjeta_usuario']= "La tarjeta no esta asociada a ningun cliente";

$lang['error_obtener_beneficios']= "No se pudo obtener los beneficios";

$lang['error_tipo_pagos']= "No se pudo obtener los tipo de pagos";

$lang['error_obtener_mozos']= "No se pudo obtener los mozos";


$lang['error_token_contrasenia'] = "No se pudo generar el token";
$lang['error_token_contrasenia_correo'] = "Ocurrió un error en el envio de correo";

$lang['error_formato_fecha'] = 'El formato de fecha es incorrecto';

$lang['error_persona_dni'] = 'EL DNI ingresado , ya se encuentra registrado';
$lang['error_persona_email'] = 'EL email ingresado , ya se encuentra registrado';
$lang['error_envio_mail'] = 'No se pudo enviar el mail';

$lang['error_envio_mail'] = 'No se pudo enviar el mail';
$lang['error_recepcion_reserva_actualizar'] = 'No se pudo actualizar la recepcion de la reserva';

$lang['error_obtener_reservas_grafico'] = "No se pudo obtener las reservas.";
$lang['error_obtener_acumulacion_grafico'] = "No se pudo obtener las el listado de acumulacion de puntos.";


$lang['error_envio_mail'] = 'No se pudo enviar el mail';
$lang['error_beneficio_listar_vacio'] = 'No se encontraron beneficios disponibles.';
$lang['error_promocion_listar_vacio'] = 'No se encontraron promociones disponibles.';

$lang['error_token_push_registro'] = 'No se pudo registrar el token push';
$lang['error_token_push_envio'] = 'Debe ingresar el parámetro token push';

$lang['error_register_message'] = 'No se pudo registrar el mensaje';
$lang['registro_mensaje_email'] = 'No se pudo registrar el mensaje de email';
$lang['error_register_message_detail'] = 'Hubo un error cuando se envio los emails';
$lang['error_register_message_update_date_end'] = 'No se pudo registrar la fecha fin';

$lang['error_register_message_push'] = 'No se pudo registrar el mensaje de push';
$lang['error_usuarios_token_push'] = 'No se le puedo enviar la notificación a los usuarios seleccionados';

$lang['error_obtener_mensajes'] = 'No se pudo obtener los mensajes';

$lang['error_obtener_estadistica_mensaje'] = 'No se pudo obtener la estadistica del mensaje';
$lang['error_obtener_detalle_mensaje'] = 'No se pudo obtener el detalle del  mensaje';
$lang['error_obtener_detalle_mensaje_procesando'] = 'El mensaje todavia se esta procesando';

$lang['error_asignar_tarjeta'] = 'No se pudo asignar la tarjeta al cliente';
$lang['error_asignar_tarjeta_eliminar'] = 'No se pudo disvincular la tarjeta al cliente';
$lang['error_asignar_tarjeta_vacio'] = 'Debe ingresar un codigo de tarjeta';
$lang['error_asignar_tarjeta_repetido'] = 'El codigo de tarjeta ingresado ya esta asignada a otro usuario';
$lang['error_asignar_tarjeta_repetido_dni'] = 'El usuario ya tiene asignado una tarjeta';
$lang['error_asignar_tarjeta_numerico'] = 'Ingrese un codigo de tarjeta valido';






