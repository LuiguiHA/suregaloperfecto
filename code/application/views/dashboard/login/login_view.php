<div class="login-box animated fadeInDown">
    <div style="margin-bottom: 10px;text-align: center;"><img src="<?php echo base_url('assets/backend/');?>/img/club-beneficios/logo.png" ></div>

    <div class="login-body">
        <div class="login-title"><strong>Bienvenido</strong>, Por favor ingrese sus datos</div>
        <form  class="form-horizontal" method="post" id="frm-login">
            <div class="form-group">
                <div class="col-md-12">
                    <input type="text" class="form-control" placeholder="Username" name="txtuser" id="txtuser"/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <input type="password" class="form-control" placeholder="Password" name="txtpassword" id="txtpassword"/>
                </div>
            </div>

         <!--   <div class="form-group" >
                <div class="col-md-12" >

                    <select class="form-control select" id="local" name="local"  >
                        <option value="">Seleccione</option>
                        <?php foreach ($locales as $local) { ?>
                            <option value="<?php echo $local["id"];?>">
                                <?php echo $local["nombre"]; ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
            </div>-->
            <div class="form-group">
                <div class="col-md-12">
                    <!--  <button class="btn btn-info btn-block">Log In</button>-->
                    <input type="submit" value="Ingresar" id="btnLogin" class="btn btn-info btn-block"/>
                </div>
            </div>
        </form>
    </div>
    <div class="login-footer">
        <div class="pull-left">
            &copy; 2016 | Club Beneficios
        </div>
      <!--  <div class="pull-right">
            <a href="#">Contact Us</a>
        </div>-->
    </div>
</div>

<script type="text/javascript">

    $("#frm-login").submit(function(){

        if($("#txtuser").val() != ""){
            if($("#txtpassword").val()!=""){
                $("#btnLogin").attr('disabled',true);
                $.post("<?php echo site_url('dashboard/Inicio/login/');?>",$(this).serialize(),
                    function(result){
                   
                        switch(result.cod) {
                            case 0:


                                fn_mostrarError(result.msg);
                                break;
                            case 1:
                                location.href="<?php echo site_url('dashboard/Inicio/');?>";
                                break;

                        }
                        $("#btnLogin").attr('disabled',false);

                    },"json");
            }else{
                $("#txtpassword").focus();
            }
            return false;
        }else{
            $("#txtuser").focus();
        }
        return false;

    });

    function fn_mostrarError(mensaje){
        alert(mensaje);
    }

</script>