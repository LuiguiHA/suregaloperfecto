<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">


    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal" id="formGuardar" enctype="multipart/form-data" method="POST">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Detalle de la Promoción</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-md-1 col-xs-12 control-label">Nombre</label>
                            <div class="col-md-11 col-xs-12">
                                <Label class="form-control"><?php echo $promocion["nombre"]; ?></Label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-1 col-xs-12 control-label">Descripción</label>
                            <div class="col-md-11 col-xs-12">
                                <Label class="form-control"><?php echo $promocion["descripcion"]; ?></Label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-1 col-xs-12 control-label">Fecha Inicio</label>
                            <div class="col-md-11 col-xs-12">
                                <Label
                                    class="form-control"><?php echo date('d/m/Y ', strtotime($promocion["fecha_inicio"])); ?></Label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-1 col-xs-12 control-label">Fecha Fin</label>
                            <div class="col-md-11 col-xs-12">
                                <Label
                                    class="form-control"><?php echo date('d/m/Y ', strtotime($promocion["fecha_fin"])); ?></Label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-1 col-xs-12 control-label">Restricciones</label>
                            <div class="col-md-11 col-xs-12">
                                <Label class="form-control"><?php echo $promocion["restricciones"]; ?></Label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-1 col-xs-12 control-label">Compartir en Redes</label>
                            <div class="col-md-11 col-xs-12">
                                <Label class="form-control">
                                    <?php

                                    if (strcasecmp($promocion["compartir_redes"], "1") == 0)
                                    {
                                        echo "Si";
                                    } else
                                    {
                                        echo "No";
                                    }
                                    ?>
                                </Label>
                            </div>
                        </div>
                        <?php
                        if (isset($foto_detalle) && count($foto_detalle) > 0)

                        {
                        ?>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h5>Foto Detalle</h5>

                                <div class="form-group">
                                    <img src="<?php echo base_url().$foto_detalle["foto"]?>"   width="100"/>
                                </div>


                            </div>
                        </div>
                        <?php }?>
                        <?php
                        if (isset($informacion_adicional) && count($informacion_adicional) > 0)

                        {
                        ?>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h5>Información Adicional</h5>
                                <?php foreach ($informacion_adicional as $info_adi)
                                {

                                    ?>
                                    <div class="form-group">
                                        <label class="col-md-2 col-xs-12 control-label"><?php echo $info_adi["campo"]?></label>
                                        <div class="col-md-10 col-xs-12">
                                            <Label class="form-control"><?php echo $info_adi["valor"]?></Label>
                                        </div>
                                    </div>
                                <?php } ?>


                            </div>
                        </div>
                        <?php }?>
                        <?php
                        if (isset($locales) && count($locales) > 0)

                        {
                            ?>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h5>Locales</h5>

                                <table class="table ">
                                    <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Dirección</th>
                                        <th>Latitud</th>
                                        <th>Longitud</th>
                                        <th>Email</th>
                                        <th>Teléfono</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php

                                        foreach ($locales as $local)
                                        {
                                            ?>
                                            <tr>
                                                <td><?php echo $local["nombre"]; ?></td>
                                                <td><?php echo $local["direccion"] ?></td>
                                                <td><?php echo $local["latitud"] ?></td>
                                                <td><?php echo $local["longitud"] ?></td>
                                                <td><?php echo $local["correo"] ?></td>
                                                <td><?php echo $local["telefono"] ?></td>
                                            </tr>
                                            <?php


                                    }
                                    ?>
                                    </tbody>
                                </table>


                            </div>
                        </div>

            <?php }?>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">

        </div>
    </div>


    <!-- END WIDGETS -->
</div>
<!-- END PAGE CONTENT WRAPPER -->       