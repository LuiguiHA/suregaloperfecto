<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Filtro de búsqueda</h3>
                </div>
                <div class="panel-body">
                    <form name="form_busqueda" id="form_busqueda" method="post"
                          action="<?php
                          echo site_url($this->config->item('path_dashboard') .'/Promocion/buscar');
                          ?>">
                    <div class="col-md-12 form-group">
                        <div class="col-md-6">
                            <label class="control-label">Nombre</label>
                            <input type="text" name="nombre" id="nombre" class="form-control" maxlength="200" value="<?php echo $nombre;?>"/>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Estado</label>
                            <select class="form-control select" id="estado" name="estado">
                                <option value="">Seleccione</option>
                                <option value="1" <?php if ($estado == "1") echo "selected";?> >Activo</option>
                                <option value="0" <?php if ($estado == "0") echo "selected";?>>Inactivo</option>

                            </select>
                        </div>
                    </div>
                    <div class="col-md-12 form-group">
                        <div class="col-md-6">
                            <label class="control-label">Fecha inicio</label>
                            <input type="text" name="fecha_inicio" id="fecha_inicio" class="form-control" maxlength="200" value="<?php echo $fecha_inicio;?>"/>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Fecha fin</label>
                            <input type="text" name="fecha_fin" id="fecha_fin" class="form-control" maxlength="200" value="<?php echo $fecha_fin;?>"/>
                        </div>
                    </div>
                    <div class="col-md-12 form-group">
                        <input type="submit" class="btn btn-primary pull-right" value="Buscar"/>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Listado</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered table-actions">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Fecha inicio</th>
                            <th>Fecha fin</th>
                            <th>Estado</th>
                            <th width="100">Acción</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php
                        if (isset($promociones) && count($promociones) > 0)
                        {

                            foreach ($promociones as $promocion)
                            {
                                ?>
                                <tr>
                                    <td><?php echo $promocion["nombre"]; ?></td>
                                    <td><span
                                            class="label label-primary label-form"><?php echo date('d/m/Y', strtotime($promocion["fecha_inicio"])); ?></span>
                                    </td>
                                    <td><span
                                            class="label label-primary label-form"><?php echo date('d/m/Y', strtotime($promocion["fecha_fin"])); ?></span>
                                    </td>
                                    <td>

                                        <?php
                                        if ($promocion["estado"] == "0")
                                        {

                                            ?>
                                            <span class="label label-danger label-form">

                                               <?php echo "Inactivo"; ?>

                                                </span>

                                            <?php
                                        } elseif ($promocion["estado"] == "1")
                                        {

                                            $fecha_actual = date("Y-m-d",time());
                                            $fecha_entrada =  date('Y-m-d',strtotime($promocion["fecha_fin"]));


                                            if ($fecha_entrada<$fecha_actual){
                                              ?>
                                                <span class="label label-warning label-form">
                                                <?php echo "Despublicado"; ?>
                                                      </span>
                                                <?php

                                            }else
                                            {

                                                ?>
                                                <span class="label label-success label-form">
                                                <?php echo "Activo"; ?>
                                                      </span>
                                                <?php
                                            }
                                        }
                                        ?>
                                        </span>
                                    </td>
                                    <td class="text-center">
                                        <a href="<?php echo site_url($this->config->item('path_dashboard') . '/Promocion/detalle/' . $promocion["id"]); ?>" data-toggle="tooltip" data-placement="bottom" title="Ver detalle"
                                           class="btn btn-info btn-condensed">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>
                                </tr>
                                <?php
                            }

                        }
                        ?>
                        </tbody>
                    </table>
                    <?php

                    if (isset($paginador))
                        echo $paginador;
                    ?>
                </div>
            </div>
        </div>
    </div>


    <!-- END WIDGETS -->
</div>
<!-- END PAGE CONTENT WRAPPER -->
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery(document).on('click','#paginador ul li a',function(c){
            c.preventDefault();
            var inicio = jQuery(this).attr('href');
            jQuery("#form_busqueda").attr("action",inicio);
            jQuery("#form_busqueda").submit();
        });
    });

    $("#fecha_inicio").datetimepicker({
        format: 'YYYY-MM-DD',
        locale: 'es'
    });

    $("#fecha_fin").datetimepicker({
        format: 'YYYY-MM-DD',
        locale: 'es'
    });
</script>
