<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">


    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal" id="formGuardar" enctype="multipart/form-data" method="POST">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Detalle de la Carta</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-md-1 col-xs-12 control-label">Nombre</label>
                            <div class="col-md-11 col-xs-12">
                                <Label class="form-control"><?php echo $carta["nombre"]; ?></Label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-1 col-xs-12 control-label">Categoría</label>
                            <div class="col-md-11 col-xs-12">
                                <Label class="form-control"><?php echo $carta["nombre_categoria"]; ?></Label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-1 col-xs-12 control-label">Precio</label>
                            <div class="col-md-11 col-xs-12">
                                <Label class="form-control"><?php echo $carta["precio"]; ?></Label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-1 col-xs-12 control-label">Descripción</label>
                            <div class="col-md-11 col-xs-12">
                                <Label
                                    class="form-control"><?php echo $carta["descripcion"]; ?></Label>
                            </div>
                        </div>




                        <?php
                        if (isset($foto) && count($foto) > 0)

                        {
                        ?>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h5>Foto </h5>

                                <div class="form-group">

                                    <img src="<?php echo $foto["nombre"]?>"   width="100" style="margin: 10px"/>

                                </div>


                            </div>
                        </div>
                        <?php }?>

                        <?php
                        if (isset($empresa) && count($empresa) > 0)

                        {
                            ?>
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <h5>Empresa</h5>

                                    <div class="form-group">
                                        <label class="col-md-1 col-xs-12 control-label">Nombre</label>
                                        <div class="col-md-11 col-xs-12">
                                            <Label class="form-control"><?php echo $empresa["nombre"]?></Label>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        <?php }?>

                        
                        <?php
                        if (isset($locales) && count($locales) > 0)

                        {
                            ?>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h5>Locales</h5>

                                <table class="table ">
                                    <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Dirección</th>
                                        <th>Latitud</th>
                                        <th>Longitud</th>
                                        <th>Email</th>
                                        <th>Teléfono</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php

                                        foreach ($locales as $local)
                                        {
                                            ?>
                                            <tr>
                                                <td><?php echo $local["nombre"]; ?></td>
                                                <td><?php echo $local["direccion"] ?></td>
                                                <td><?php echo $local["latitud"] ?></td>
                                                <td><?php echo $local["longitud"] ?></td>
                                                <td><?php echo $local["correo"] ?></td>
                                                <td><?php echo $local["telefono"] ?></td>
                                            </tr>
                                            <?php


                                    }
                                    ?>
                                    </tbody>
                                </table>


                            </div>
                        </div>

            <?php }?>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">

        </div>
    </div>


    <!-- END WIDGETS -->
</div>
<!-- END PAGE CONTENT WRAPPER -->       