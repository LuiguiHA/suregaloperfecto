<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Filtro de búsqueda</h3>
                </div>
                <div class="panel-body">
                    <form name="form_busqueda" id="form_busqueda" method="post"
                          action="<?php
                          echo site_url($this->config->item('path_dashboard') .'/Carta/buscar');
                          ?>">
                    <div class="col-md-12 form-group">
                        <div class="col-md-4">
                            <label class="control-label">Nombre</label>
                            <input type="text" name="nombre" id="nombre" class="form-control" maxlength="200" value="<?php echo  $nombre;?>" />
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Categoría</label>
                            <select class="form-control select" id="categoria"  name="categoria">
                                <option value="">Seleccione</option>
                                <?php foreach ($categorias as $categoria) { ?>
                                    <option value="<?php echo $categoria["id"];?>"  <?php if ($categoria_select == $categoria["id"]) echo "selected" ;?>>
                                        <?php echo $categoria["nombre"]; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Estado</label>
                            <select class="form-control select" id="estado" name="estado">
                                <option value="">Seleccione</option>
                                <option value="1" <?php if ($estado == "1") echo "selected";?> >Activo</option>
                                <option value="0" <?php if ($estado == "0") echo "selected";?>>Inactivo</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12 form-group">
                        <input type="submit" class="btn btn-primary pull-right"  value="Buscar" />
                    </div>
                        </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Listado</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered table-actions">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Categoría</th>
                            <th>Precio</th>
                            <th>Estado</th>
                            <th width="100">Acción</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php
                        if (isset($cartas) && count($cartas) > 0)

                        {

                            foreach ($cartas as $carta)
                            {
                                ?>
                                <tr>
                                    <td><?php echo $carta["nombre"]; ?></td>
                                    <td><span class="label label-success label-form"><?php echo $carta["categoria"]; ?></span></td>
                                    <td><span
                                            class="label label-primary label-form"><?php echo "S/. ".$carta["precio"]; ?></span>
                                    </td>

                                    <td>

                                        <?php
                                        if ($carta["estado"] == "0")
                                        {

                                            ?>
                                            <span class="label label-danger label-form">

                                               <?php echo "Inactivo"; ?>

                                                </span>

                                            <?php
                                        } elseif ($carta["estado"] == "1")
                                        {


                                                ?>
                                                <span class="label label-success label-form">
                                                <?php echo "Activo"; ?>
                                                      </span>
                                                <?php

                                        }

                                        ?>
                                        </span>
                                    </td>
                                    <td class="text-center">
                                        <a href="<?php echo site_url($this->config->item('path_dashboard') . '/Carta/detalle/' . $carta["id"]); ?>" data-toggle="tooltip" data-placement="bottom" title="Ver detalle" class="btn btn-info btn-condensed">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>
                                </tr>
                                <?php
                            }

                        }
                        ?>
                        </tbody>
                    </table>
                    <?php

                    if (isset($paginador))
                        echo $paginador;
                    ?>
                </div>
            </div>
        </div>
    </div>



    <!-- END WIDGETS -->
</div>
<!-- END PAGE CONTENT WRAPPER -->
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery(document).on('click','#paginador ul li a',function(c){
            c.preventDefault();
            var inicio = jQuery(this).attr('href');
            jQuery("#form_busqueda").attr("action",inicio);
            jQuery("#form_busqueda").submit();
        });
    });
</script>