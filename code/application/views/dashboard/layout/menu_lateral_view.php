<div class="page-sidebar">
    <!-- START X-NAVIGATION -->
    <ul class="x-navigation">
        <?php //if(isset($mostrarLogoMenuLateral) && $mostrarLogoMenuLateral === TRUE): ?>
    <!--    <li class="xn-logo">
            <a href="index.html">ATLANT</a>
            <a href="#" class="x-navigation-control"></a>
        </li>-->


        <?php
        // if(isset($mostrarPerfilMenuLateral) && $mostrarPerfilMenuLateral === TRUE):
        ?>

        <?php
        $local = $this->session->userdata('nom_local');
        $empresa = $this->session->userdata('nom_empresa');
        $logo = $this->session->userdata('logo');

        ?>
        <li class="xn-profile">
         <!--   <a href="#" class="profile-mini">
                <img src="<?php echo base_url('assets/backend/');?>/img/users/avatar.jpg" alt="John Doe"/>
            </a>-->
            <div class="profile">
               <!-- <div class="profile-image">
                    <img src="<?php echo base_url('assets/backend/');?>/img/users/avatar.jpg" alt="John Doe"/>
                </div>-->
                <div class="profile-data">
                    <div class="profile-data-name"><?php echo $empresa;?></div>
                    <div class="profile-data-title"><?php echo $local;?></div>
                </div>
                <!--<div class="profile-controls">
                    <a href="#" class="profile-control-left"><span class="fa fa-info"></span></a>
                    <a href="#" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                </div>-->
            </div>
        </li>
        <?php
        // endif;
        ?>

        <?php if(isset($mostrarTituloMenuLateral) && $mostrarTituloMenuLateral === TRUE): ?>
            <li class="xn-title">Navigation</li>
        <?php endif ?>


        <li <?php
        if($this->uri->segment(2)== "Principal" )
            echo  'class="active"';
        else
            echo  'class=""';
        ?>>
            <a href="<?php echo site_url($this->config->item('path_dashboard') . '/Principal'); ?>"><span class="fa fa-dashboard"></span> <span class="xn-text">Dashboard</span></a>

        </li>


        
        <li <?php
        if($this->uri->segment(2)== "Cliente" )
            echo  'class="active"';
        else
            echo  'class=""';
        ?>>
            <a href="<?php echo site_url($this->config->item('path_dashboard') . '/Cliente'); ?>"><span class="fa fa-users"></span> <span class="xn-text">Clientes</span></a>
        </li>
    <!--    <li class="xn-openable">
            <a href="#"><span class="fa fa-bar-chart-o"></span> <span class="xn-text">Reportes</span></a>
            <ul>
                <li><a href="reporte-usuarios.html"><span class="fa fa-user"></span>Por usuarios</a></li>
                <li><a href="reporte-puntos.html"><span class="fa fa-tags"></span>Por puntos</a></li>
            </ul>
        </li>-->

        <li <?php
        if($this->uri->segment(2)== "Promocion" || $this->uri->segment(2)== "Carta" || $this->uri->segment(2)== "Beneficio")
            echo  'class="xn-openable active"';
        else
            echo  'class="xn-openable"';
        ?>>
            <a href="#"><span class="fa fa-search"></span> <span class="xn-text">Consultas</span></a>
            <ul>
                <li <?php
                if( ($this->uri->segment(3)== "" || $this->uri->segment(3)== "buscar" || $this->uri->segment(3)== "listar" )
                    && ($this->uri->segment(2)== "Promocion" )  )
                    echo  'class="active"';
                ?>><a href="<?php echo site_url($this->config->item('path_dashboard') . '/Promocion'); ?>"><span class="fa fa-cut"></span>Promociones</a></li>
                <li <?php
                if( ($this->uri->segment(3)== "" || $this->uri->segment(3)== "buscar" || $this->uri->segment(3)== "listar" )
                    && ($this->uri->segment(2)== "Beneficio" )  )
                    echo  'class="active"';
                ?>><a href="<?php echo site_url($this->config->item('path_dashboard') . '/Beneficio'); ?>"><span class="fa fa-tag"></span>Canjes</a></li>
                <li
                    <?php
                    if( ($this->uri->segment(3)== "" || $this->uri->segment(3)== "buscar" || $this->uri->segment(3)== "listar" )
                        && ($this->uri->segment(2)== "Carta" )  )
                        echo  'class="active"';
                    ?>
                ><a href="<?php echo site_url($this->config->item('path_dashboard') . '/Carta'); ?>"><span class="glyphicon glyphicon-cutlery"></span>Carta</a></li>
            </ul>
        </li>

        <li <?php
        if($this->uri->segment(2)== "Reserva")
            echo  'class="active"';
        else
            echo  'class=""';
        ?>>
            <a href="<?php echo site_url($this->config->item('path_dashboard') . '/Reserva'); ?>"><span class="fa fa-file-text-o"></span> <span class="xn-text">Reservas</span></a>

        </li>
        <li <?php
        if($this->uri->segment(2)== "Horario_exclusion")
            echo  'class="active"';
        else
            echo  'class=""';
        ?>>
            <a href="<?php echo site_url($this->config->item('path_dashboard') . '/Horario_exclusion'); ?>"><span class="fa fa-times-circle"></span> <span class="xn-text">Exclusión</span></a>

        </li>

    </ul>
    <!-- END X-NAVIGATION -->
</div>