       

        <!-- START TEMPLATE -->
       <!-- <script type="text/javascript" src="<?php echo base_url('assets/backend/');?>/js/settings.js"></script> -->
        <script type="text/javascript" src="<?php echo base_url('assets/backend/');?>/js/plugins.js"></script>        
        <script type="text/javascript" src="<?php echo base_url('assets/backend/');?>/js/actions.js"></script>   
        <!-- END TEMPLATE -->
        <script type="text/javascript">
                <?php   $tipo_mensaje = $this->session->userdata('tipo_mensaje');
                $mensaje = $this->session->userdata('mensaje');
                if (isset($tipo_mensaje) && isset($mensaje)){
                ?>
                noty(
                    {
                            text: '<?php echo $mensaje?>',
                            layout: 'topRight',
                            type: '<?php echo $tipo_mensaje?>',
                            timeout: 5000
                    });
                <?php }
                $this->session->unset_userdata('tipo_mensaje');
                $this->session->unset_userdata('mensaje');
                ?>
        </script>