<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">


    <div class="row">
        <div class="col-md-12">
            <form action ="<?php echo site_url('dashboard/Reserva/editar_reserva/'.$reserva["id"])?>" class="form-horizontal" id="detalle_reserva" enctype="multipart/form-data" method="POST">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Datos de la Reserva</h3>
                    </div>
                    <div class="panel-body">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h5>Usuario</h5>

                                <div class="form-group">
                                    <label class="col-md-1 col-xs-12 control-label">Nombre</label>
                                    <div class="col-md-11 col-xs-12">
                                        <Label class="form-control"><?php echo $reserva["nombre"] ?></Label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-1 col-xs-12 control-label">Apellido</label>
                                    <div class="col-md-11 col-xs-12">
                                        <Label class="form-control"><?php echo $reserva["apellido"] ?></Label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-1 col-xs-12 control-label">DNI</label>
                                    <div class="col-md-11 col-xs-12">
                                        <Label class="form-control"><?php echo $reserva["dni"] ?></Label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-1 col-xs-12 control-label">Email</label>
                                    <div class="col-md-11 col-xs-12">
                                        <Label class="form-control"><?php echo $reserva["correo"] ?></Label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-1 col-xs-12 control-label">Teléfono</label>
                                    <div class="col-md-11 col-xs-12">
                                        <Label class="form-control"><?php echo $reserva["telefono"] ?></Label>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h5>Reserva</h5>

                                <a href="<?php echo site_url($this->config->item('path_dashboard').'/Reserva/horarios_reserva/'. $reserva["id_local"] . '/' .$reserva["meses"] . '/' . $reserva["id"]);?>" class="btn btn-default"  style="margin-bottom: 10px;"><i class="fa fa-plus"></i> Modificar Reserva</a>

                                <div class="form-group">
                                    <label class="col-md-1 col-xs-12 control-label">Local</label>
                                    <div class="col-md-11 col-xs-12">
                                        <Label class="form-control"><?php echo $reserva["nombre_local"] ?></Label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-1 col-xs-12 control-label">Ubicación</label>
                                    <div class="col-md-11 col-xs-12">
                                        <select class="form-control select" id="ubicacin" name="ubicacion">
                                            <option value="">Seleccione</option>
                                            <?php foreach ($ubicacion as $ubicacion){?>
                                            <option value="<?php echo $ubicacion["id"]?>"  <?php if ($ubicacion["id"] == $reserva["id_local_ubicacion"]) echo 'selected' ;?>><?php echo $ubicacion["nombre"] ?></option>
                                            <?php }?>
                                        </select>


                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-1 col-xs-12 control-label">Fecha</label>
                                    <div class="col-md-11 col-xs-12">

                                        <Label class="form-control"><?php echo $reserva["fecha"] ?></Label>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-1 col-xs-12 control-label">Día</label>
                                    <div class="col-md-11 col-xs-12">
                                        <?php
                                        $day_week = unserialize(DAY_WEEK);
                                        foreach ($day_week as $day) {

                                            if ($day["id"] == $reserva["dia"]){
                                                $dia  = $day["dia"];
                                            }
                                        } ?>

                                        <Label class="form-control" id="dia"  name="dia"><?php echo $dia;?></Label>
                                    </div>
                                </div>


                               <div class="form-group">
                                  <label class="col-md-1 col-xs-12 control-label">Hora</label>
                                    <div class="col-md-11 col-xs-12">
                                        <Label class="form-control"><?php
                                            $time_start= DateTime::createFromFormat( 'H:i:s', $reserva["hora"]);
                                            $hora = $time_start->format( 'h:i A');
                                            echo $hora;
                                          ?>

                                        </Label>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-md-1 col-xs-12 control-label">Cantidad</label>
                                    <div class="col-md-11 col-xs-12">
                                        <Label class="form-control"><?php echo $reserva["cantidad"] ?></Label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-1 col-xs-12 control-label">Estado</label>
                                    <div class="col-md-11 col-xs-12">
                                        <select class="form-control select" id="estado" name="estado">
                                            <option value="">Seleccione</option>
                                            <option value="1" <?php if ($reserva["estado"] == ESTADO_ACEPTADO) echo "selected"; ?>>Aceptado</option>
                                            <option value="2" <?php if ($reserva["estado"] == ESTADO_RECHAZADO) echo "selected"; ?> >Rechazado</option>
                                            <option value="0" <?php if ($reserva["estado"] == ESTADO_PENDIENTE) echo "selected"; ?>>Pendiente</option>
                                            <option value="3" <?php if ($reserva["estado"] == ESTADO_ASISTIDO) echo "selected"; ?>>Asistido</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="btn-group pull-right" style="margin-bottom: 10px ; margin-right: 10px;">
                        <a type="button" class="btn btn-primary" style="margin-right: .5em;"
                           href="<?php echo site_url('dashboard/Reserva/listar'); ?>">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="Guardar" name="agregar" id="agregar" />
                    </div>
                </div>


            </form>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">

        </div>
    </div>


    <!-- END WIDGETS -->
</div>

<script type="text/javascript">
    var jvalidate = $("#detalle_reserva").validate({
        ignore: [],
        rules: {
            ubicacion: {
                required: true
            },
            estado: {
                required: true
            }
        }
    });

</script>

<!-- END PAGE CONTENT WRAPPER -->       