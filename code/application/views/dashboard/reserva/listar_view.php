<div class="page-content-wrap">

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Filtros</h3>
                </div>
                <div class="panel-body">
                    <form name="form_busqueda" id="form_busqueda" method="post"
                          action="<?php
                          echo site_url($this->config->item('path_dashboard') . '/Reserva/buscar');
                          ?>">
                        <div class="col-md-12 form-group">
                            <div class="col-md-4">
                                <label class="control-label">Nombre</label>
                                <input type="text" name="nombre" id="nombre" class="form-control" maxlength="200"
                                       value="<?php echo $nombre; ?>"/>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">Apellido</label>
                                <input type="text" name="apellido" id="apellido" class="form-control" maxlength="200"
                                       value="<?php echo $apellido; ?>"/>
                            </div>

                            <div class="col-md-4">
                                <label class="control-label">DNI</label>
                                <input type="text" name="dni" id="dni" class="form-control" maxlength="8"
                                       value="<?php echo $dni; ?>"/>
                            </div>
                        </div>
                        <div class="col-md-12 form-group">
                            <div  <?php  if ($this->session->userdata('id_local')!= null ){
                                echo "class='col-md-6'";
                            }else{
                                echo "class='col-md-4'";
                            }?>
                            >
                                <label class="control-label">Día</label>
                                <select class="form-control select" id="dia" name="dia">
                                    <option value="">Seleccione</option>
                                    <?php
                                    $day_week = unserialize(DAY_WEEK);
                                    foreach ($day_week as $day)
                                    { ?>
                                        <option value="<?php echo $day["id"]; ?>"  <?php if($day["id"] == $dia) echo "selected"; ?>>
                                            <?php echo $day["dia"]; ?>
                                        </option>
                                    <?php } ?>

                                </select>
                            </div>

                            <?php if ($this->session->userdata('id_local')== null ){?>

                            <div class="col-md-4">
                                <label class="control-label">Local</label>
                                <select class="form-control select" id="local" name="local">
                                    <option value="">Seleccione</option>
                                    <?php

                                    foreach ($locales as $local)
                                    { ?>
                                        <option value="<?php echo $local["id"]; ?>"  <?php if($local["id"] == $local_select) echo "selected"; ?>>
                                            <?php echo $local["nombre"]; ?>
                                        </option>
                                    <?php } ?>

                                </select>
                            </div>
                            <?php }?>

                            <div  <?php  if ($this->session->userdata('id_local')!= null ){
                                 echo "class='col-md-6'";
                            }else{
                            echo "class='col-md-4'";
                             }?>
                            >
                                <label class="control-label">Estado</label>
                                <select class="form-control select" id="estado" name="estado">
                                    <option value="">Seleccione</option>
                                    <option value="1" <?php if ($estado == ESTADO_ACEPTADO) echo "selected"; ?>>Aceptado</option>
                                    <option value="2" <?php if ($estado == ESTADO_RECHAZADO) echo "selected"; ?> >Rechazado</option>
                                    <option value="0" <?php if ($estado == ESTADO_PENDIENTE) echo "selected"; ?>>Pendiente</option>
                                    <option value="3" <?php if ($estado == ESTADO_ASISTIDO) echo "selected"; ?>>Asistido</option>
                                </select>
                            </div>

                        </div>
                        <div class="col-md-12 form-group">
                            <input type="submit" class="btn btn-primary pull-right" value="Buscar"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <table class="table table-bordered table-actions">
                        <thead>
                        <tr>
                            <th>Local</th>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>DNI</th>
                            <th>Cantidad</th>
                            <th>Hora</th>
                            <th>Día</th>
                            <th>Fecha</th>
                            <th>Estado</th>
                            <th width="100">Acción</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if (isset($reservas) && count($reservas) > 0)
                        {

                            foreach ($reservas as $reserva)
                            {
                                ?>
                                <tr>
                                    <td><?php echo $reserva["local_nombre"]; ?></td>
                                    <td><?php echo $reserva["nombre"]; ?></td>
                                    <td><?php echo $reserva["apellido"]; ?></td>
                                    <td><?php echo $reserva["dni"]; ?></td>
                                    <td><?php echo $reserva["cantidad"]; ?></td>
                                    <td>
                                        <?php

                                        $time_start= DateTime::createFromFormat( 'H:i:s', $reserva["hora"]);
                                       $hora = $time_start->format( 'h:i A');

                                        echo $hora;


                                        ?>
                                    </td>
                                    <td><?php
                                        $day_week = unserialize(DAY_WEEK);

                                        for ($i = 0; $i < sizeof($day_week); $i++)
                                        {
                                            if ($reserva["dia"] == $day_week[$i]["id"])
                                            {
                                                $dia = $day_week[$i]["dia"];
                                            }
                                        }
                                        echo $dia;

                                        ?>
                                    </td>
                                    <td><?php echo $reserva["fecha"]; ?></td>
                                    <td>

                                        <?php
                                        if ($reserva["estado"] == ESTADO_PENDIENTE)
                                        {

                                            ?>
                                            <span class="label label-info label-form">

                                               <?php echo "Pendiente"; ?>

                                                </span>

                                            <?php
                                        } elseif ($reserva["estado"] == ESTADO_ACEPTADO)
                                        {

                                            ?>
                                            <span class="label label-warning label-form">

                                               <?php echo "Aceptado"; ?>

                                                </span>

                                            <?php
                                        } else if ($reserva["estado"] == ESTADO_RECHAZADO)
                                        {
                                            ?>
                                            <span class="label label-danger label-form">

                                               <?php echo "Rechazado"; ?>

                                                </span>

                                            <?php
                                        }else
                                        {
                                            ?>
                                            <span class="label label-success label-form">

                                               <?php echo "Asistido"; ?>

                                                </span>

                                            <?php
                                        }

                                        ?>

                                    </td>
                                    <td class="text-center">
                                        <a href="<?php echo site_url($this->config->item('path_dashboard') . '/Reserva/detalle/' . $reserva["id"]); ?>" data-toggle="tooltip" data-placement="bottom" title="Ver detalle"
                                           class="btn btn-info btn-condensed">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>

                                </tr>
                                <?php
                            }

                        }
                        ?>
                        </tbody>
                    </table>
                    <?php

                    if (isset($paginador)) echo $paginador;
                    ?>
                </div>
            </div>
        </div>

    </div>

    <!-- END WIDGETS -->
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery(document).on('click', '#paginador ul li a', function (c) {
            c.preventDefault();
            var inicio = jQuery(this).attr('href');
            jQuery("#form_busqueda").attr("action", inicio);
            jQuery("#form_busqueda").submit();
        });
    });

    $(document).ready(function () {
        $('#dni').keypress(validateNumber);

    });

    function validateNumber(event) {
        var key = window.event ? event.keyCode : event.which;

        if (event.keyCode === 8 || event.keyCode === 46
            || event.keyCode === 37 || event.keyCode === 39) {
            return true;
        }
        else if (key < 48 || key > 57) {
            return false;
        }
        else return true;
    }
</script>