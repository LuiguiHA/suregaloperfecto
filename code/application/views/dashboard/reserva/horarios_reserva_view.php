<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">


    <div class="row">
        <div class="col-md-12">
            <form action="<?php echo site_url('dashboard/Reserva/editar_horario_reserva/' . $reserva) ?>"
                  class="form-horizontal" id="horario_actualizar" enctype="multipart/form-data" method="POST">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Horarios</h3>
                    </div>
                    <div class="panel-body">

                        <div class="form-group">
                            <label class="col-md-1 col-xs-12 control-label">Fecha</label>
                            <div class="col-md-11 col-xs-12">
                                <input type="text" class="form-control"
                                       id="fecha" name="fecha" readonly>
                            </div>
                        </div>

                        <div id="div_dia">
                            <div class="form-group">
                                <label class="col-md-1 col-xs-12 control-label">Día</label>
                                <div class="col-md-11 col-xs-12">
                                    <Label class="form-control" id="dia"></Label>
                                </div>
                            </div>
                        </div>

                        
                        <div id="div_horario" style="margin-top: 10px;">
                            <div class="form-group">
                                <label class="col-md-1 col-xs-12 control-label">Horario</label>
                                <div class="col-md-11 col-xs-12">

                                    <select class="form-control" id="horario" name="horario">
                                        <option value="">Seleccione</option>
                                    </select>

                                </div>
                            </div>
                        </div>

                        <div id="div_ubicacion" style="margin-top: 10px;">
                            <div class="form-group">
                                <label class="col-md-1 col-xs-12 control-label">Ubicación</label>
                                <div class="col-md-11 col-xs-12">

                                    <select class="form-control" id="ubicacion" name="ubicacion">
                                        <option value="">Seleccione</option>
                                    </select>

                                </div>
                            </div>
                        </div>


                        <div id="div_cantidad" style="margin-top: 20px;">
                            <div class="form-group">
                                <label class="col-md-1 col-xs-12 control-label">Cantidad</label>
                                <div class="col-md-11 col-xs-12">
                                    <div id="div_input_cantidad"></div>
                                    <!--<input type="text" class="form-control spinner_default" value="1" id="cantidad"
                                           name="cantidad"  min="0" readonly/>-->
                                </div>
                            </div>
                        </div>


                    <div id="div_estado" style="margin-top: 20px;">
                    <div class="form-group">
                        <label class="col-md-1 col-xs-12 control-label">Estado</label>
                        <div class="col-md-11 col-xs-12">
                            <select class="form-control select" id="estado" name="estado">
                                <option value="">Seleccione</option>
                                <option value="1">Aceptado</option>
                                <option value="2">Rechazado</option>
                                <option value="0">Pendiente</option>
                                <option value="3">Asistido</option>
                            </select>
                        </div>
                    </div>
                </div>

        </div>
        <div class="btn-group pull-right" style="margin-bottom: 10px ; margin-right: 10px;">
            <a type="button" class="btn btn-primary" style="margin-right: .5em;"
               href="<?php echo site_url('dashboard/Reserva/detalle/'.$reserva); ?>">Cancelar</a>
            <input type="submit" class="btn btn-primary" value="Guardar" name="agregar" id="agregar"/>
        </div>
    </div>


    </form>
</div>
</div>


<div class="row">
    <div class="col-md-12">

    </div>
</div>
    
<!-- END WIDGETS -->
</div>

<script type="text/javascript" >

    var date_reserves = new Date();
    date_reserves.setMonth(date_reserves.getUTCMonth()+ <?php echo $meses?>);

    var date = new Date();
    date.setDate(date.getDate());
    $("#fecha").datetimepicker({
        format: 'YYYY-MM-DD',
        locale: 'es',
        defaultDate : new Date(),
        minDate : date,
        maxDate : date_reserves,
        ignoreReadonly: true
    });


    var jvalidate = $("#horario_actualizar").validate({
        ignore: [],
        rules: {
            fecha: {
                required: true
            },
            horario: {
                required: true
            },

            ubicacion: {
                required: true
            },

            cantidad: {
                required: true
            },

            estado: {
                required: true
            }
        }
    });


    $("#div_dia").hide();
    $("#div_horario").hide();
    $("#div_cantidad").hide();
    $("#div_estado").hide();
    $("#div_ubicacion").hide();

    $('#fecha').on('dp.change', function (e) {

        var fecha = $("#fecha").val();
        var f = new Date(fecha);
        var day = f.getDay() + 1;

        var diasemana = new Array(7);
        diasemana[1] = "Lunes";
        diasemana[2] = "Martes";
        diasemana[3] = "Miércoles";
        diasemana[4] = "Jueves";
        diasemana[5] = "Viernes";
        diasemana[6] = "Sábado";
        diasemana[7] = "Domingo";

        for (var i = 1; i <= diasemana.length; i++) {

            if (day == i) {
                var day_name = diasemana[i];
            }
        }

        $("#dia").text(day_name);

        $("#div_dia").hide();
        $("#div_horario").hide();
        $("#div_cantidad").hide();
        $("#div_estado").hide();
        $("#div_ubicacion").hide();

        $.ajax({
            url: '<?php echo site_url('dashboard/Reserva/revisar_fechas_reservas');?>',
            type: 'POST',
            data: {id_local: <?php echo $local;?>, fecha: fecha},
            dataType: 'json',
            success: function (data) {

                if (data.cod == "1") {

                    $('#horario').empty();

                    $("#horario").append('<option value="">' + 'Seleccione ' + '</option>');

                    $.each(data.horario, function (i, horario) {

                        var horarios = horario.lista_horas;

                        for (var j = 0; j < horarios.length; j++) {
                            $("#horario").append('<option value="' + horario.id_horario + '|' + horarios[j] + '">' + horarios[j] + '</option>');
                        }
                    });

                    $("#div_dia").show();
                    $("#div_horario").show();

                    $('#ubicacion').empty();

                    $("#ubicacion").append('<option value="">' + 'Seleccione ' + '</option>');
                    $.each(data.ubicacion, function (i, ubicacion) {
                        $("#ubicacion").append('<option value="' + ubicacion.id + '">' + ubicacion.nombre + '</option>');
                    });

                    $("#div_ubicacion").show();

                    $("#horario").on('change', function () {
                        var max_cantidad = 0;
                        var horario_select = $("#horario").val().split('|');

                        var id_horario = horario_select[0];

                        $("#cantidad").val("1");

                        for (var j = 0; j < data.horario.length; j++) {
                            if (id_horario == data.horario[j].id_horario) {
                                max_cantidad = data.horario[j].stock_final;
                            }
                        }

                        $('#div_input_cantidad').empty();
                        $("#div_input_cantidad").append('<input type="text" class="form-control spinner_default" value="1" id="cantidad" name="cantidad"  min="0" max="' + max_cantidad + '" readonly/>');

                        $(function () {
                            $(".spinner_default").spinner()
                        });

                        $("#div_cantidad").show();
                        $("#div_estado").show();
                        $("#div_ubicacion").show();
                    });

                    $("#cantidad").on('change', function () {
                        alert($("#cantidad").val());
                    })
                } else {
                    $("#div_horario").hide();
                    $("#div_cantidad").hide();
                    $("#div_estado").hide();
                    $("#div_ubicacion").hide();
                    alert(data.message);

                }
            }
        });


    });

    $("#fecha").datetimepicker({
        format: 'YYYY-MM-DD',
        locale: 'es'
    });


</script>

<!-- END PAGE CONTENT WRAPPER -->       