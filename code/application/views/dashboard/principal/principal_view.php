<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">

    <!-- START WIDGETS -->
    <div class="row">
        <div class="col-md-4">
            <?php if ($this->session->userdata('id_local')== null ){?>
            <!-- START WIDGET SLIDER -->
            <div class="widget widget-default widget-carousel">
                <div class="owl-carousel" id="owl-example">
                    <div>
                        <div class="widget-title">Total de usuarios</div>
                        <div class="widget-subtitle"></div>
                        <div class="widget-int"><?php echo $datos_usuarios['usuarios_totales']; ?></div>
                    </div>
                    <div>
                        <div class="widget-title">Usuarios recurrentes</div>
                        <div class="widget-subtitle"></div>
                        <div class="widget-int"><?php echo $datos_usuarios['usuarios_recurrentes']; ?></div>
                    </div>
                    <div>
                        <div class="widget-title">Usuarios nuevos</div>
                        <div class="widget-subtitle"></div>
                        <div class="widget-int"><?php echo $datos_usuarios['usuarios_nuevos']; ?></div>
                    </div>
                </div>
            </div>
            <!-- END WIDGET SLIDER -->
            <?php }else{?>

            <div class="widget widget-default widget-item-icon" >
                <div class="widget-item-left">
                    <span class="fa fa-user"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count"><?php echo $datos_usuarios['usuarios_recurrentes']; ?></div>
                    <div class="widget-title">Usuarios</div>
                    <div class="widget-subtitle">Recurrentes</div>
                </div>
            </div>
            <?php }?>

        </div>
        <div class="col-md-4">

            <!-- START WIDGET MESSAGES -->
            <div class="widget widget-default widget-item-icon" >
                <div class="widget-item-left">
                    <span class="fa fa-user"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count"><?php echo $datos_usuarios['usuarios_canje']; ?></div>
                    <div class="widget-title">Usuarios</div>
                    <div class="widget-subtitle">Canjearon puntos</div>
                </div>
            </div>
            <!-- END WIDGET MESSAGES -->

        </div>
        <div class="col-md-4">

            <!-- START WIDGET REGISTRED -->
            <div class="widget widget-default widget-item-icon" >
                <div class="widget-item-left">
                    <span class="fa fa-user"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count"><?php echo $datos_usuarios['usuarios_puntos']; ?></div>
                    <div class="widget-title">Usuarios</div>
                    <div class="widget-subtitle">Acumularon puntos</div>
                </div>
            </div>
            <!-- END WIDGET REGISTRED -->

        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Canjes</h3>
                        <span>Actividad de locales en la última semana</span>
                        <canvas id="canvas_canjes"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Últimos canjes</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table
                                <?php
                                if (isset($canjes) && count($canjes) > 0)
                                {
                                    ?>
                                    class="table datatable_simple"
                                <?php
                                }else{
                                    ?>
                                    class="table"
                                <?php
                                }
                                ?>
                        >
                        <thead>
                        <tr>
                            <th>Usuario</th>
                            <th>Local</th>
                            <th>Descripción</th>
                            <th>Puntos canjeados</th>
                            <th>Fecha</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if (isset($canjes) && count($canjes) > 0)
                        {
                            foreach ($canjes as $canje)
                            {
                                ?>
                                <tr>
                                    <td><?php echo $canje["usuario_nombre"]; ?></td>
                                    <td><?php echo $canje["local_nombre"]; ?></td>
                                    <td><?php echo $canje["beneficio"]; ?></td>
                                    <td><?php echo $canje["puntos_usuados"]; ?></td>
                                    <td> <?php echo date('d/m/Y g:i a', strtotime($canje["fecha"])); ?></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Reservas</h3>
                        <span>Actividad de locales en la última semana</span>
                        <canvas id="canvas_reservas"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Últimas Reservas</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table  <?php
                        if (isset($reservas) && count($reservas) > 0)
                        {
                            ?>
                            class="table datatable_simple"
                            <?php
                        }else{
                            ?>
                            class="table"
                            <?php
                        }
                        ?>
                        >
                        <thead>
                        <tr>
                            <th>Usuario</th>
                            <th>Local</th>
                            <th>Personas</th>
                            <th>Estado</th>
                            <th>Fecha</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if (isset($reservas) && count($reservas) > 0)
                        {

                            foreach ($reservas as $reserva)
                            {
                                ?>
                                <tr>
                                    <td><?php echo $reserva["usuario_nombre"]; ?></td>
                                    <td><?php echo $reserva["local_nombre"]; ?></td>
                                    <td><?php echo $reserva["cantidad"]; ?></td>
                                    <td><?php
                                        if($reserva["estado"] == STATUS_PENDING) {
                                        ?>
                                            <span class="label label-default">Pendiente</span>
                                            <?php
                                        }elseif ($reserva["estado"] == STATUS_ACCEPTED){
                                            ?>
                                            <span class="label label-primary">Aceptado</span>
                                            <?php
                                        }elseif ($reserva["estado"] == STATUS_DENIED){
                                            ?>
                                            <span class="label label-danger">Rechazado</span>
                                            <?php
                                        }else{
                                            ?>
                                            <span class="label label-success">Confirmado</span>
                                            <?php
                                        }

                                    ?>
                                    </td>
                                    <td><?php echo date('d/m/Y g:i a', strtotime($reserva["fecha"])); ?></td>

                                </tr>
                                <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Acumulación de Puntos</h3>
                        <span>Actividad de locales en la última semana</span>
                        <canvas id="canvas_acumulaciones"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Últimas acumulaciones</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table  <?php
                        if (isset($acumulaciones) && count($acumulaciones) > 0)
                        {
                            ?>
                            class="table datatable_simple"
                            <?php
                        }else{
                            ?>
                            class="table"
                            <?php
                        }
                        ?>
                        >
                        <thead>
                        <tr>
                            <th>Usuario</th>
                            <th>Local</th>
                            <th>Puntos Ganados</th>
                            <th>Tipo de pago</th>
                            <th>Fecha</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if (isset($acumulaciones) && count($acumulaciones) > 0)
                        {

                            foreach ($acumulaciones as $acumulacion)
                            {
                                ?>
                                <tr>
                                    <td><?php echo $acumulacion["usuario_nombre"]; ?></td>
                                    <td><?php echo $acumulacion["local_nombre"]; ?></td>
                                    <td><?php echo $acumulacion["puntos"]; ?></td>
                                    <td><?php echo $acumulacion["tipo_pago"]; ?></td>
                                    <td> <?php echo date('d/m/Y g:i a', strtotime($acumulacion["fecha"])); ?></td>

                                </tr>
                                <?php
                            }

                        }
                        ?>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- END WIDGETS -->
</div>

<script type="text/javascript">
    var randomColorFactor = function () {
        return Math.round(Math.random() * 255);
    };
    var randomColor = function (opacity) {
        return 'rgba(' + randomColorFactor() + ',' + randomColorFactor() + ',' + randomColorFactor() + ',' + (opacity || '.3') + ')';
    };

    $(document).ready(initialize);

    function initialize() {


        var config_canje = {
            type: 'line',
            data: {
                labels: [<?php echo $fecha;?>],
                datasets: [<?php echo $canjes_grafico;?>]
            },
            options: {
                responsive: true,
                title: {
                    display: false,
                    text: ''
                },
                tooltips: {
                    mode: 'label',
                    callbacks: {}
                },
                hover: {
                    mode: 'dataset'
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            show: true,
                            labelString: 'Día'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            show: true,
                            labelString: 'Valor'
                        },
                        ticks: {
                            suggestedMin: 0,
                            suggestedMax: <?php if ($canjes_total_max!= 0 ) echo $canjes_total_max;?>,
                            beginAtZero: true,
                            userCallback: function(label, index, labels) {
                                if (Math.floor(label) === label) {
                                    return label;
                                }

                            }
                        }
                    }]
                }
            }
        };

        var config_reserva = {
            type: 'line',
            data: {
                labels: [<?php echo $fecha;?>],
                datasets: [<?php echo $reservas_grafico;?>]
            },
            options: {
                responsive: true,
                title: {
                    display: false,
                    text: ''
                },
                tooltips: {
                    mode: 'label',
                    callbacks: {}
                },
                hover: {
                    mode: 'dataset'
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            show: true,
                            labelString: 'Día'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            show: true,
                            labelString: 'Valor'
                        },
                        ticks: {
                            suggestedMin: 0,
                            suggestedMax: <?php echo $reservas_total_max;?>,
                            beginAtZero: true,
                            userCallback: function(label, index, labels) {
                                if (Math.floor(label) === label) {
                                    return label;
                                }

                            }
                        }
                    }]
                }
            }
        };

        var config_acumulacion = {
            type: 'line',
            data: {
                labels: [<?php echo $fecha;?>],
                datasets: [<?php echo $acumulaciones_grafico;?>]
            },
            options: {
                responsive: true,
                title: {
                    display: false,
                    text: ''
                },
                tooltips: {
                    mode: 'label',
                    callbacks: {}
                },
                hover: {
                    mode: 'dataset'
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            show: true,
                            labelString: 'Día'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            show: true,
                            labelString: 'Valor'
                        },
                        ticks: {
                            suggestedMin: 0,
                            suggestedMax: <?php echo $acumulaciones_total_max;?>,
                            beginAtZero: true,
                            userCallback: function(label, index, labels) {
                                if (Math.floor(label) === label) {
                                    return label;
                                }

                            }
                        }
                    }]
                }
            }
        };

        $.each(config_canje.data.datasets, function (i, dataset) {
            dataset.borderColor = randomColor(0.4);
            dataset.backgroundColor = randomColor(0.5);
            dataset.pointBorderColor = randomColor(0.7);
            dataset.pointBackgroundColor = randomColor(0.5);
            dataset.pointBorderWidth = 1;
        });

        $.each(config_reserva.data.datasets, function (i, dataset_reserva) {
            dataset_reserva.borderColor = randomColor(0.4);
            dataset_reserva.backgroundColor = randomColor(0.5);
            dataset_reserva.pointBorderColor = randomColor(0.7);
            dataset_reserva.pointBackgroundColor = randomColor(0.5);
            dataset_reserva.pointBorderWidth = 1;
        });

        $.each(config_acumulacion.data.datasets, function (i, dataset_acumulacion) {
            dataset_acumulacion.borderColor = randomColor(0.4);
            dataset_acumulacion.backgroundColor = randomColor(0.5);
            dataset_acumulacion.pointBorderColor = randomColor(0.7);
            dataset_acumulacion.pointBackgroundColor = randomColor(0.5);
            dataset_acumulacion.pointBorderWidth = 1;
        });


        var ctx = document.getElementById("canvas_canjes").getContext("2d");
        window.myLine = new Chart(ctx, config_canje);
        var ctx2 = document.getElementById("canvas_reservas").getContext("2d");
        window.myLine = new Chart(ctx2, config_reserva);
        var ctx3 = document.getElementById("canvas_acumulaciones").getContext("2d");
        window.myLine = new Chart(ctx3, config_acumulacion);




    }
</script>
<!-- END PAGE CONTENT WRAPPER -->