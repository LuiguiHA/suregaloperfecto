<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <form action="<?php echo site_url('dashboard/Horario_exclusion/editar/' . $horario["id"]) ?>"
                  class="form-horizontal" id="editar_horario" enctype="multipart/form-data" method="POST">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Datos del Horario</h3>
                    </div>
                    <div class="panel-body">

                        <div class="form-group">
                            <label class="col-md-1 col-xs-12 control-label">Fecha</label>
                            <div class="col-md-11 col-xs-12">
                                <input type="text" class="form-control"
                                       value="<?php echo $horario["fecha"]; ?>" id="fecha" name="fecha"
                                     >
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-1 col-xs-12 control-label">Tipo</label>
                            <div class="col-md-11 col-xs-12">
                                <select class="form-control select" id="tipo" name="tipo">
                                    <option value="">Seleccione</option>
                                    <option value="1" <?php if ($horario["tipo"] == "1") echo "selected"; ?> >Horas
                                    </option>
                                    <option value="0" <?php if ($horario["tipo"] == "0") echo "selected"; ?>>Día
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div id="div_horario" style="margin-bottom: 10px;">
                            <div class="form-group">
                                <label class="col-md-1 col-xs-12 control-label">Horario</label>
                                <div class="col-md-11 col-xs-12">

                                    <select class="form-control" id="horario" name="horario">
                                        <option value="">Seleccione</option>
                                    </select>

                                </div>
                            </div>
                        </div>
                        <div id="div_hora" style="margin-bottom: 10px;">
                            <div class="form-group">
                                <label class="col-md-1 col-xs-12 control-label">Hora Inicio</label>
                                <div class="col-md-11 col-xs-12">

                                    <div id="div_hora_inicio">
                                    </div>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-1 col-xs-12 control-label">Hora Fin</label>
                                <div class="col-md-11 col-xs-12">

                                    <div id="div_hora_fin">
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-1 col-xs-12 control-label">Estado</label>
                            <div class="col-md-11 col-xs-12">
                                <select class="form-control select" id="estado" name="estado">
                                    <option value="">Seleccione</option>
                                    <option value="1" <?php if ($horario["estado"] == "1") echo "selected"; ?> >Activo
                                    </option>
                                    <option value="0" <?php if ($horario["estado"] == "0") echo "selected"; ?>>
                                        Inactivo
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="btn-group pull-right" style="margin-bottom: 10px ; margin-right: 10px;">
                        <a type="button" class="btn btn-primary" style="margin-right: .5em;"
                           href="<?php echo site_url('dashboard/horario_exclusion/listar'); ?>">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="Guardar" name="agregar" id="agregar"/>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">

        </div>
    </div>


    <!-- END WIDGETS -->
</div>

<script type="text/javascript">
    var validate_reserve = true;


    type_hour();



    var jvalidate = $("#editar_horario").validate({
        ignore: [],
        rules: {

            fecha: {
                required: true
            },
            estado: {
                required: true
            },
            tipo: {
                required: true
            }
        }
    });


    $('#fecha').on('dp.change', function (e) {
        if ($("#tipo").val() == 1) {
            change_data(true);
        } else {
            $("#tipo").on('change', function () {
                change_data(true);
            });
        }
    });
    $("#tipo").on('change', function () {
        if ($("#tipo").val() == 1) {
            if ($("#fecha").val() != "") {
                change_data(true);
            }
        } else {
            $("#div_horario").hide();
            $("#div_hora").hide();
        }
    });



    function change_data(type) {
        if ($("#tipo").val() == 1) {
            $.ajax({
                url: '<?php echo site_url('dashboard/Horario_exclusion/revisar_fechas_reservas');?>',
                type: 'POST',
                data: {id_local: <?php echo $horario["id_local"];?>, fecha: $("#fecha").val()},
                dataType: 'json',
                success: function (data) {
                    $("#div_horario").show();
                    $('#horario').empty();

                    $("#horario").append('<option value="">' + 'Seleccione ' + '</option>');

                    $.each(data.horario, function (i, horario) {
                        var horario_data = "";

                        <?php if ($horario["id_horario"]!= null || $horario["id_horario"] !=""){ ?>
                        horario_data = <?php echo $horario["id_horario"];?>;
                        <?php }?>

                        $("#horario").append('<option value="' + horario.id_horario + '"    >' + horario.hora_inicio + ' - ' + horario.hora_fin + '</option>');

                        if (horario_data == horario.id_horario){
                            $("#horario").val(horario.id_horario);
                        }
                    });


                    if ($("#horario").val() == "") {
                        $("#div_hora").hide();
                    }else{
                        show_hour(data,type);
                    }

                    $("#horario").on('change', function () {
                        show_hour(data,true);
                    });
                }
            });

        } else {
            $("#div_horario").hide();
            $("#div_hora").hide();
        }
    }


    function converthour(hora_12, type) {
        var time = hora_12;

        var hours = Number(time.match(/^(\d+)/)[1]);
        var minutes = Number(time.match(/:(\d+)/)[1]);
        var AMPM = time.match(/\s(.*)$/)[1];
        if (AMPM == "PM" && hours < 12) hours = hours + 12;
        if (AMPM == "AM" && hours == 12) hours = hours - 12;
        var sHours = hours.toString();
        var sMinutes = minutes.toString();
        if (hours < 10) sHours = "0" + sHours;
        if (minutes < 10) sMinutes = "0" + sMinutes;

        if (type) {
            return sMinutes;
        }
        else {
            return sHours;
        }
    }

    $("#editar_horario").submit(function () {

        if ($("#tipo").val() == 1) {
            if ($("#horario").val() != "") {

                var hora_inicio_ini = $("#hora_inicio").val();
                var hora_fin_ini = $("#hora_fin").val();

                var hora_inicio = converthour(hora_inicio_ini, false);
                var hora_fin = converthour(hora_fin_ini, false);

                var min_inicio = converthour(hora_inicio_ini, true);
                var min_fin = converthour(hora_fin_ini, true);



                if (hora_inicio < hora_fin) {

                    return true;
                } else if (hora_inicio == hora_fin) {
                    if (min_inicio == min_fin) {
                        error = "La hora de inicio es igual a la hora fin, cambie los horarios ";
                        alert(error);
                        return false;
                    } else {
                        return true;
                    }

                } else {
                    error = "La hora de inicio es mayor a la hora fin, cambie los horarios  ";
                    alert(error);
                    return false;
                }
            } else {
                error = "Seleccione un horario";
                alert(error);
                return false;
            }
        } else {
            return true
        }
    });


    function type_hour() {

        if ($("#tipo").val() == 1) {
            $("#div_horario").show();
            $("#div_hora").show();
            change_data(false);
        } else {
            $("#div_horario").hide();
            $("#div_hora").hide();
        }
    }


    function show_hour(data,type){

        if ($("#horario").val() == "") {
            $("#div_hora").hide();
        } else {

            $("#div_hora").show();
        }
        var hora_inicio = 0;
        var hora_fin = 0;
        var horario_select = $("#horario").val();


        for (var j = 0; j < data.horario.length; j++) {

            if (horario_select == data.horario[j].id_horario) {
                hora_inicio = data.horario[j].hora_inicio;
                hora_fin = data.horario[j].hora_fin;
            }
        }


        if (type == true){
            $("#div_hora_inicio").empty();
            $("#div_hora_inicio").append('<input type="text" class="form-control" value="'+hora_inicio+'" name="hora_inicio" id="hora_inicio" readonly/>');
            $("#hora_inicio").timepicker({
                timeFormat: 'h:mm p',
                interval: 30,
                minTime: hora_inicio,
                maxTime: hora_fin,
                defaultTime: hora_inicio,
                dynamic: false,
                dropdown: true,
                scrollbar: true
            });

            $("#div_hora_fin").empty();
            $("#div_hora_fin").append('<input type="text" class="form-control" value="'+hora_fin+'" name="hora_fin" id="hora_fin" readonly/>');
            $("#hora_fin").timepicker({
                timeFormat: 'h:mm p',
                interval: 30,
                minTime: hora_inicio,
                maxTime: hora_fin,
                defaultTime: hora_fin,
                dynamic: false,
                dropdown: true,
                scrollbar: true
            });
        }else{
            <?php  if ( ($horario["hora_inicio"] != null || $horario["hora_inicio"] !="")  &&  ($horario["hora_fin"] != null || $horario["hora_din"] !="")) { ?>


            $("#div_hora_inicio").empty();
            $("#div_hora_inicio").append('<input type="text" class="form-control" value="<?php $time_start= DateTime::createFromFormat( 'H:i:s',$horario["hora_inicio"]);$hora = $time_start->format( 'h:i A');echo $hora;?>" name="hora_inicio" id="hora_inicio" readonly/>');
            $("#hora_inicio").timepicker({
                timeFormat: 'h:mm p',
                interval: 30,
                minTime: hora_inicio,
                maxTime: hora_fin,
                //   defaultTime: <?php $time_start= DateTime::createFromFormat( 'H:i:s',$horario["hora_inicio"]);$hora = $time_start->format( 'h:i A');echo $hora;?>,
                dynamic: false,
                dropdown: true,
                scrollbar: true
            });

            $("#div_hora_fin").empty();
            $("#div_hora_fin").append('<input type="text" class="form-control" value="<?php $time_start= DateTime::createFromFormat( 'H:i:s',$horario["hora_fin"]);$hora = $time_start->format( 'h:i A');echo $hora;?>" name="hora_fin" id="hora_fin" readonly/>');
            $("#hora_fin").timepicker({
                timeFormat: 'h:mm p',
                interval: 30,
                minTime: hora_inicio,
                maxTime: hora_fin,
                //  defaultTime: <?php $time_start= DateTime::createFromFormat( 'H:i:s',$horario["hora_fin"]);$hora = $time_start->format( 'h:i A');echo $hora;?>,
                dynamic: false,
                dropdown: true,
                scrollbar: true
            });
            <?php }?>
        }

    }

    $("#editar_horario").submit(function () {


        var id_horario = $("#horario").val();
        var tipo = $("#tipo").val();
        var fecha = $("#fecha").val();
        var estado = $("#estado").val();

        if(estado == 1) {
            $.ajax({
                type: 'POST',
                dataType: "json",
                async: false,
                url: "<?php echo site_url('dashboard/Horario_exclusion/revisar_horario_exclusion');?>",
                data: {
                    id_horario_exclusion :<?php echo $horario["id"]; ?>,
                    id_local:<?php echo $horario["id_local"]; ?>,
                    id_horario: id_horario,
                    tipo: tipo,
                    fecha: fecha

                },
                success: function (result) {
                    switch (result.cod) {
                        case 0:
                            fn_check_reserve(result.cod);
                            break;
                        case 1:
                            fn_check_reserve(result.cod);
                            break;
                    }
                }
            });
            return validate_reserve;
        }else{
            return true;
        }


    });

    function fn_check_reserve(check) {

        if (check == 0) {
            validate_reserve = true;
        } else {
            var tipo = $("#tipo").val();
            if (tipo == 1){
                error = "El horario seleccionado en dicha fecha  ya esta excluida";
            }else{
                error = "La fecha ingresada ya esta excluida";
            }

            alert(error);
            console.log("error");
            validate_reserve = false;
        }

    }


    var validate_reserve = true;

    var date = new Date();
    $("#fecha").datetimepicker({
        format: 'YYYY-MM-DD',
        locale: 'es',
        defaultDate : new Date(),
        minDate : date
    });




</script>

<!-- END PAGE CONTENT WRAPPER -->       