<div class="page-content-wrap">

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Filtros</h3>
                </div>
                <div class="panel-body">
                    <form name="form_busqueda" id="form_busqueda" method="post"
                          action="<?php
                          echo site_url($this->config->item('path_dashboard') . '/Horario_exclusion/buscar');
                          ?>">
                        <div class="col-md-12 form-group">

                            <div  <?php  if ($this->session->userdata('id_local')!= null ){
                                echo "class='col-md-4'";
                            }else{
                                echo "class='col-md-3'";
                            }?>
                            >
                                <label class="control-label">Tipo</label>
                                <select class="form-control select" id="tipo" name="tipo">
                                    <option value="">Seleccione</option>
                                    <option value="1" <?php if ($tipo == "1") echo "selected"; ?> >Horas</option>
                                    <option value="0" <?php if ($tipo == "0") echo "selected"; ?>>Día</option>
                                </select>
                            </div>

                            <div  <?php  if ($this->session->userdata('id_local')!= null ){
                                echo "class='col-md-4'";
                            }else{
                                echo "class='col-md-3'";
                            }?>
                            >
                                <label class="control-label">Fecha</label>
                                <input type="text" class="form-control"
                                       value="<?php echo $fecha ; ?>" id="fecha" name="fecha">
                            </div>
                            <?php if ($this->session->userdata('id_local')== null ){?>
                            <div class="col-md-3">
                                <label class="control-label">Local</label>
                                <select class="form-control select" id="local" name="local">
                                    <option value="">Seleccione</option>
                                    <?php

                                    foreach ($locales as $local)
                                    { ?>
                                        <option
                                            value="<?php echo $local["id"]; ?>" <?php if ($local["id"] == $local_select) echo "selected"; ?>>
                                            <?php echo $local["nombre"]; ?>
                                        </option>
                                    <?php } ?>

                                </select>
                            </div>
                            <?php }?>

                            <div  <?php  if ($this->session->userdata('id_local')!= null ){
                                echo "class='col-md-4'";
                            }else{
                                echo "class='col-md-3'";
                            }?>
                            >
                                <label class="control-label">Estado</label>
                                <select class="form-control select" id="estado" name="estado">
                                    <option value="">Seleccione</option>
                                    <option value="1" <?php if ($estado == "1") echo "selected"; ?> >Activo</option>
                                    <option value="0" <?php if ($estado == "0") echo "selected"; ?>>Inactivo</option>
                                </select>
                            </div>

                        </div>
                        <div class="col-md-12 form-group">
                            <input type="submit" class="btn btn-primary pull-right" value="Buscar"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="<?php echo site_url($this->config->item('path_dashboard').'/Horario_exclusion/agregar_view');?>" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Agregar</a>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered table-actions">
                        <thead>
                        <tr>
                            <th>Local</th>
                            <th>Fecha</th>
                            <th>Hora Inicio</th>
                            <th>Hora Fin</th>
                            <th>Tipo</th>
                            <th>Estado</th>
                            <th width="100">Acción</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if (isset($horarios) && count($horarios) > 0)
                        {

                            foreach ($horarios as $horario)
                            {
                                ?>
                                <tr>
                                    <td><?php echo $horario["nombre_local"]; ?></td>
                                    <td><?php echo $horario["fecha"]; ?></td>
                                    <td>
                                        <?php

                                        if ($horario["hora_inicio"] !=null || $horario["hora_inicio"] !=""){
                                            $time_start= DateTime::createFromFormat( 'H:i:s',$horario["hora_inicio"]);
                                            $hora = $time_start->format( 'h:i A');

                                            echo $hora;
                                        }else{
                                            echo "";
                                        }

                                        ?>
                                    </td>
                                    <td><?php
                                        if ($horario["hora_fin"] !=null || $horario["hora_fin"] !=""){
                                            $time_start= DateTime::createFromFormat( 'H:i:s',$horario["hora_fin"]);
                                            $hora = $time_start->format( 'h:i A');

                                            echo $hora;
                                        }else{
                                            echo "";
                                        }?></td>
                                    <td>
                                        <?php
                                        if ($horario["tipo"] == "1")
                                        {

                                            ?>
                                            <span class="label label-info label-form">

                                               <?php echo "Horas"; ?>

                                                </span>

                                            <?php
                                        }  else
                                        {
                                            ?>
                                            <span class="label label-warning label-form">

                                               <?php echo "Día"; ?>

                                                </span>

                                            <?php
                                        }

                                        ?>

                                    </td>
                                    <td>
                                        <?php
                                        if ($horario["estado"] == ESTADO_ACTIVO)
                                        {

                                            ?>
                                            <span class="label label-success label-form">

                                               <?php echo "Activo"; ?>

                                                </span>

                                            <?php
                                        }  else
                                        {
                                            ?>
                                            <span class="label label-danger label-form">

                                               <?php echo "Inactivo"; ?>

                                                </span>

                                            <?php
                                        }

                                        ?>

                                    </td>
                                    <td class="text-center">
                                        <a href="<?php echo site_url($this->config->item('path_dashboard') . '/Horario_exclusion/detalle/' . $horario["id"]); ?>"
                                           data-toggle="tooltip" data-placement="bottom" title="Ver detalle"
                                           class="btn btn-info btn-condensed">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>

                                </tr>
                                <?php
                            }

                        }
                        ?>
                        </tbody>
                    </table>
                    <?php

                    if (isset($paginador)) echo $paginador;
                    ?>
                </div>
            </div>
        </div>

    </div>

    <!-- END WIDGETS -->
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery(document).on('click', '#paginador ul li a', function (c) {
            c.preventDefault();
            var inicio = jQuery(this).attr('href');
            jQuery("#form_busqueda").attr("action", inicio);
            jQuery("#form_busqueda").submit();
        });
    });

    $(document).ready(function () {
        $('#dni').keypress(validateNumber);

    });

    function validateNumber(event) {
        var key = window.event ? event.keyCode : event.which;

        if (event.keyCode === 8 || event.keyCode === 46
            || event.keyCode === 37 || event.keyCode === 39) {
            return true;
        }
        else if (key < 48 || key > 57) {
            return false;
        }
        else return true;
    }

    $("#fecha").datetimepicker({
        format: 'YYYY-MM-DD',
        locale: 'es'
    });
</script>