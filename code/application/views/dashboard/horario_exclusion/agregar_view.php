<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">


    <div class="row">
        <div class="col-md-12">
            <form
                action="<?php echo site_url('dashboard/Horario_exclusion/agregar/') ?>"
                class="form-horizontal" id="agregar_horario" enctype="multipart/form-data" method="POST">
                <div class="panel panel-default">

                    <div class="panel-body">
                        <?php if ($this->session->userdata('id_local')== null ) { ?>
                            <div class="form-group">
                                <label class="col-md-1 col-xs-12 control-label">Local</label>
                                <div class="col-md-11 col-xs-12">
                                    <select class="form-control select" name="local" id="local">
                                        <option value="">Seleccione</option>
                                        <?php foreach ($locales as $local) { ?>
                                            <option value="<?php echo $local["id"] ?>">
                                                <?php echo $local["nombre"]; ?>
                                            </option>
                                        <?php } ?>

                                    </select>
                                </div>
                            </div>
                            <?php
                        }?>

                        <div class="form-group">
                            <label class="col-md-1 col-xs-12 control-label">Fecha</label>
                            <div class="col-md-11 col-xs-12">
                                <input type="text" class="form-control"
                                       value="" id="fecha" name="fecha"  >
                            </div>
                        </div>
                        <div id="div_tipo" style="margin-bottom: 10px;">
                            <div class="form-group">
                                <label class="col-md-1 col-xs-12 control-label">Tipo</label>
                                <div class="col-md-11 col-xs-12">
                                    <select class="form-control select" id="tipo" name="tipo">
                                        <option value="">Seleccione</option>
                                        <option value="1" <?php if ($horario["tipo"] == "1") echo "selected"; ?> >Horas
                                        </option>
                                        <option value="0" <?php if ($horario["tipo"] == "0") echo "selected"; ?>>Día
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div id="div_horario" style="margin-bottom: 10px;">
                            <div class="form-group">
                                <label class="col-md-1 col-xs-12 control-label">Horario</label>
                                <div class="col-md-11 col-xs-12">

                                    <select class="form-control" id="horario" name="horario">
                                        <option value="">Seleccione</option>
                                    </select>

                                </div>
                            </div>
                        </div>


                        <div id="div_hora" style="margin-bottom: 10px;">
                            <div class="form-group">
                                <label class="col-md-1 col-xs-12 control-label">Hora Inicio</label>
                                <div class="col-md-11 col-xs-12">

                                    <div id="div_hora_inicio">
                                    </div>

                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-1 col-xs-12 control-label">Hora Fin</label>
                                <div class="col-md-11 col-xs-12">

                                    <div id="div_hora_fin">
                                    </div>

                                </div>
                            </div>
                        </div>

                        <!-- <div id="data_horario">
                            <div class="form-group">
                                <label class="col-md-1 control-label">Hora Inicio</label>
                                <div class="col-md-11">
                                    <div class="input-group bootstrap-timepicker">
                                        <input type="text" class="form-control timepicker" value="<?php
                        $date = new DateTime($horario["hora_inicio"]);
                        echo $date->format('h:i a ');
                        ?>" name="hora_inicio"
                                               id="hora_inicio" readonly/>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-1 control-label">Hora Fin</label>
                                <div class="col-md-11">
                                    <div class="input-group bootstrap-timepicker">
                                        <input type="text" class="form-control timepicker" value="<?php
                        $date = new DateTime($horario["hora_fin"]);
                        echo $date->format('h:i a ');
                        ?>" name="hora_fin"
                                               id="hora_fin" readonly/>
                                    </div>
                                </div>
                            </div>

                        </div>-->

                        <div class="form-group">
                            <label class="col-md-1 col-xs-12 control-label">Estado</label>
                            <div class="col-md-11 col-xs-12">
                                <select class="form-control select" id="estado" name="estado">
                                    <option value="">Seleccione</option>
                                    <option value="1" <?php if ($horario["estado"] == "1") echo "selected"; ?> >Activo
                                    </option>
                                    <option value="0" <?php if ($horario["estado"] == "0") echo "selected"; ?>>
                                        Inactivo
                                    </option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="btn-group pull-right" style="margin-bottom: 10px ; margin-right: 10px;">
                        <a type="button" class="btn btn-primary" style="margin-right: .5em;"
                           href="<?php echo site_url('dashboard/horario_exclusion/listar'); ?>">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="Guardar" name="agregar" id="agregar"/>
                    </div>
                </div>


            </form>
        </div>
    </div>


    <!-- END WIDGETS -->
</div>

<script type="text/javascript">

    var validate_reserve = true;


    var date = new Date();
    date.setDate(date.getDate());
    $("#fecha").datetimepicker({
        format: 'YYYY-MM-DD',
        locale: 'es',
        defaultDate : new Date(),
        minDate : date
    });



    <?php if ($this->session->userdata('id_local')== null ) { ?>
    var jvalidate = $("#agregar_horario").validate({
        ignore: [],
        rules: {

            local: {
                required: true
            },

            fecha: {
                required: true
            },
            estado: {
                required: true
            },
            tipo: {
                required: true
            }
        }
    });
    <?php }else{?>

    var jvalidate = $("#agregar_horario").validate({
        ignore: [],
        rules: {

            fecha: {
                required: true
            },
            estado: {
                required: true
            },
            tipo: {
                required: true
            }
        }
    });
    <?php }?>

    $("#div_horario").hide();
    $("#data_horario").hide();
    $("#div_hora").hide();
   // $("#div_tipo").hide();


    $("#fecha").on('dp.change', function (e) {
        $("#div_tipo").show();
        if ($("#tipo").val() == 1) {
            change_data();
        } else {
            $("#tipo").on('change', function () {
                change_data();
            });
        }

    });

    $("#local").on('change', function () {
        if ($("#fecha").val() != "") {
            change_data();
        }
    });

    $("#tipo").on('change', function () {
        change_data();
    });


    function change_data() {
        var id_local = "";
        <?php if ($this->session->userdata('id_local')!= null ) { ?>
            id_local =  "<?php echo $this->session->userdata('id_local')?>";
        <?php }else{?>
            id_local =  $("#local").val();
        <?php }?>
        if ($("#local").val() !="" && id_local !="") {
            if ($("#tipo").val() == 1) {
                $.ajax({
                    url: '<?php echo site_url('dashboard/Horario_exclusion/revisar_fechas_reservas');?>',
                    type: 'POST',
                    data: {id_local: id_local, fecha: $("#fecha").val()},
                    dataType: 'json',
                    success: function (data) {

                        if (data.cod == '1') {
                            $("#div_horario").show();
                            $('#horario').empty();

                            $("#horario").append('<option value="">' + 'Seleccione ' + '</option>');

                            $.each(data.horario, function (i, horario) {
                                $("#horario").append('<option value="' + horario.id_horario + '">' + horario.hora_inicio + ' - ' + horario.hora_fin + '</option>');
                            });

                            if ($("#horario").val() == "") {
                                $("#div_hora").hide();
                            }


                            $("#horario").on('change', function () {
                                if ($("#horario").val() == "") {
                                    $("#div_hora").hide();
                                } else {
                                    $("#div_hora").show();
                                }

                                var hora_inicio = 0;
                                var hora_fin = 0;

                                for (var j = 0; j < data.horario.length; j++) {
                                    if ($("#horario").val() == data.horario[j].id_horario) {
                                        hora_inicio = data.horario[j].hora_inicio;
                                        hora_fin = data.horario[j].hora_fin;
                                    }
                                }

                                $("#div_hora_inicio").empty();
                                $("#div_hora_inicio").append('<input type="text" class="form-control" value="' + hora_inicio + '" name="hora_inicio" id="hora_inicio" readonly/>');
                                $("#hora_inicio").timepicker({
                                    timeFormat: 'h:mm p',
                                    interval: 30,
                                    minTime: hora_inicio,
                                    maxTime: hora_fin,
                                    defaultTime: hora_inicio,
                                    dynamic: false,
                                    dropdown: true,
                                    scrollbar: true
                                });

                                $("#div_hora_fin").empty();
                                $("#div_hora_fin").append('<input type="text" class="form-control" value="' + hora_fin + '" name="hora_fin" id="hora_fin" readonly/>');
                                $("#hora_fin").timepicker({
                                    timeFormat: 'h:mm p',
                                    interval: 30,
                                    minTime: hora_inicio,
                                    maxTime: hora_fin,
                                    defaultTime: hora_fin,
                                    dynamic: false,
                                    dropdown: true,
                                    scrollbar: true
                                });


                            });
                        }else{
                            var error = "Hubo un error"
                            alert(error);
                        }
                    }
                });

            } else {
                $("#div_horario").hide();
                $("#data_horario").hide();
                $("#div_hora").hide();
            }
        }
    }


    function converthour(hora_12, type) {
        var time = hora_12;
        var hours = Number(time.match(/^(\d+)/)[1]);
        var minutes = Number(time.match(/:(\d+)/)[1]);
        var AMPM = time.match(/\s(.*)$/)[1];
        if (AMPM == "PM" && hours < 12) hours = hours + 12;
        if (AMPM == "AM" && hours == 12) hours = hours - 12;
        var sHours = hours.toString();
        var sMinutes = minutes.toString();
        if (hours < 10) sHours = "0" + sHours;
        if (minutes < 10) sMinutes = "0" + sMinutes;

        if (type) {
            return sMinutes;
        }
        else {
            return sHours;
        }
    }

    $("#agregar_horario").submit(function () {

        if ($("#tipo").val() == 1) {
            if ($("#horario").val() != "") {
                var hora_inicio_ini = $("#hora_inicio").val();
                var hora_fin_ini = $("#hora_fin").val();

                var hora_inicio = converthour(hora_inicio_ini, false);
                var hora_fin = converthour(hora_fin_ini, false);

                var min_inicio = converthour(hora_inicio_ini, true);
                var min_fin = converthour(hora_fin_ini, true);


                if (hora_inicio < hora_fin) {

                    return true;
                } else if (hora_inicio == hora_fin) {
                    if (min_inicio == min_fin) {
                        error = "La hora de inicio es igual a la hora fin, cambie los horarios ";
                        alert(error);
                        return false;
                    } else {
                        return true;
                    }

                } else {
                    error = "La hora de inicio es mayor a la hora fin, cambie los horarios  ";
                    alert(error);
                    return false;
                }
            } else {
                error = "Seleccione un horario";
                alert(error);
                return false;
            }
        } else {
            return true
        }


    });


    $("#agregar_horario").submit(function () {

        var id_local = $("#local").val();
        var id_horario = $("#horario").val();
        var tipo = $("#tipo").val();
        var fecha = $("#fecha").val();
        var estado = $("#estado").val();

        if(estado == 1){
            $.ajax({
                type: 'POST',
                dataType: "json",
                async: false,
                url: "<?php echo site_url('dashboard/Horario_exclusion/revisar_horario_exclusion');?>",
                data:  {
                    id_local: id_local,
                    id_horario: id_horario,
                    tipo: tipo,
                    fecha: fecha

                },
                success:
                    function(result) {
                        switch (result.cod) {
                            case 0:
                                fn_check_reserve(result.cod);
                                break;
                            case 1:
                                fn_check_reserve(result.cod);
                                break;
                        }
                    }
            });
            return validate_reserve;
        }else{
            return true;
        }


    });

    function fn_check_reserve(check) {

        if (check == 0) {
            validate_reserve = true;
        } else {
            var tipo = $("#tipo").val();
            if (tipo == 1){
                error = "El horario y/o fecha seleccionado  ya esta excluida";
            }else{
                error = "La fecha ingresada ya esta excluida";
            }

            alert(error);
            validate_reserve = false;
        }

    }

</script>

<!-- END PAGE CONTENT WRAPPER -->       