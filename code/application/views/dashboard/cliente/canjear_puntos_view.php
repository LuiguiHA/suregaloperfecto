<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Datos de canje</h3>
                </div>
                <div class="panel-body">
                    <form name="aplicar_puntos" id="aplicar_puntos" method="post"
                          action="<?php
                          echo site_url($this->config->item('path_dashboard') .'/Cliente/canjear_puntos/'.$detalle_cliente["id"]);
                          ?>">
                    <div class="col-md-12 form-group">
                        <div class="col-md-4">
                            <label class="control-label">Canjes</label>
                            <select data-live-search="true"  class="form-control select" onchange='puntos_requeridos();' id="canje" name="canje">
                                <option value="">Seleccione</option>
                                <?php foreach ($beneficios as $beneficio) { ?>
                                    <option value="<?php echo $beneficio["id"];?>"  >
                                        <?php echo $beneficio["nombre"]; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Puntos requeridos</label>
                            <Label class="form-control" id="puntos"></Label>

                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Monto Adicional</label>
                            <input type="text" name="monto" id="monto" class="form-control" maxlength="200" value="" />
                        </div>

                        <input class="hidden" type="text" name="punto" id="punto"/>
                    </div>
                    <div class="col-md-12 form-group">
                        <input type="submit" class="btn btn-primary pull-right"  value="Guardar" />
                    </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Datos de cliente</h3>
                </div>
                <div class="panel-body">
                    <div class="col-md-12 form-group">
                        <div class="col-md-4">
                            <label class="control-label">Nombre</label>
                            <Label class="form-control"><?php echo $detalle_cliente["nombre"];?></Label>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Apellidos</label>
                            <Label class="form-control"><?php echo $detalle_cliente["apellido"];?></Label>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Sexo</label>
                            <Label class="form-control">
                                <?php
                                if (strcasecmp($detalle_cliente["sexo"],"m") == 0)
                                {
                                    echo "Masculino";
                                }else
                                {
                                    echo "Femenino";
                                }
                                ?>
                            </Label>
                        </div>
                    </div>

                    <div class="col-md-12 form-group">
                        <div class="col-md-4">
                            <label class="control-label">Email</label>
                            <Label class="form-control"><?php echo $detalle_cliente["correo"];?></Label>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Telefono</label>
                            <Label class="form-control"><?php echo $detalle_cliente["telefono"];?></Label>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">DNI</label>
                            <Label class="form-control"><?php echo $detalle_cliente["dni"];?></Label>
                        </div>

                    </div>

                    <div class="col-md-12 form-group">
                        <div class="col-md-4">
                            <label class="control-label">Puntos acumulados</label>
                            <Label class="form-control"><?php echo $detalle_cliente["puntos_acumulados"];?></Label>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Puntos por vencer</label>
                            <Label class="form-control"><?php echo $detalle_cliente["puntos_por_vencer"];?></Label>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Puntos usados</label>
                            <Label class="form-control"><?php echo $detalle_cliente["puntos_usado"];?></Label>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Historial de canjes</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered table-actions">
                        <thead>
                        <tr>
                            <th>Fecha Registro</th>
                            <th>Beneficio</th>
                            <th>Local</th>
                            <th>Puntos usados</th>
                            <th>Puntos Totales</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php
                        if (isset($canjes) && count($canjes) > 0)
                        {

                            foreach ($canjes as $canje)
                            {
                                ?>
                                <tr>
                                    <td> <?php echo date('d/m/Y g:i a', strtotime($canje["fecha_registro"])); ?></td>
                                    <td><span class="label label-success label-form"><?php echo $canje["beneficio"];?></span></td>
                                    <td><?php echo $canje["local"];?></td>
                                    <td><span class="label label-primary label-form"><?php echo $canje["puntos_usados"];?></span></td>
                                    <td><span class="label label-primary label-form"><?php echo $canje["puntos_totales"];?></span></td>
                                </tr>
                                <?php
                            }

                        }
                        ?>
                        </tbody>
                    </table>
                    <?php

                    if (isset($paginador))
                        echo $paginador;
                    ?>
                </div>
            </div>
        </div>
    </div>



    <!-- END WIDGETS -->
</div>
<!-- END PAGE CONTENT WRAPPER -->

<script type="text/javascript">

function puntos_requeridos(){


    //document.all("puntos").innerText=document.getElementById('canje').value;

   var canje = document.getElementById('canje').value

        <?php foreach ($beneficios as $beneficio) { ?>
            if (canje != "") {
                if (canje == <?php echo $beneficio["id"]?>) {
                    document.all("puntos").innerText =<?php echo $beneficio["puntos"]?>;
                        document.getElementById("punto").value =<?php echo $beneficio["puntos"]?>;
                }
            }else{
                document.all("puntos").innerText="";
            }
    <?php } ?>


}


</script>