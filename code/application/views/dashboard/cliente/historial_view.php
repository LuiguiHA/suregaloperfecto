<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Filtros de búsqueda</h3>
                </div>
                <div class="panel-body">
                    <form name="form_busqueda" id="form_busqueda" method="post"
                          action="<?php
                          echo site_url($this->config->item('path_dashboard') .'/Cliente/historial_buscar/'.$detalle_cliente["id"]);
                          ?>">
                    <div class="col-md-12 form-group">
                        <div class="col-md-4">
                            <label class="control-label">Local</label>
                            <select class="form-control select" id="local" name="local">
                                <option value="">Seleccione</option>
                                <?php foreach ($locales as $local) { ?>
                                    <option value="<?php echo $local["id"];?>"  <?php if ($local_select == $local["id"]) echo "selected" ;?>>
                                        <?php echo $local["nombre"]; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Fecha inicio</label>
                            <input type="text" name="fecha_inicio" id="fecha_inicio" class="form-control" maxlength="200" value="<?php echo $fecha_inicio;?>" />
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Fecha fin</label>
                            <input type="text" name="fecha_fin" id="fecha_fin" class="form-control" maxlength="200" value="<?php echo $fecha_fin;?>" />
                        </div>
                    </div>
                    <div class="col-md-12 form-group">
                        <input type="submit" class="btn btn-primary pull-right"  value="Buscar" />
                    </div>
                        </form>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Datos de cliente</h3>
                </div>
                <div class="panel-body">
                    <div class="col-md-12 form-group">
                        <div class="col-md-4">
                            <label class="control-label">Nombre</label>
                            <Label class="form-control"><?php echo $detalle_cliente["nombre"];?></Label>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Apellidos</label>
                            <Label class="form-control"><?php echo $detalle_cliente["apellido"];?></Label>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Sexo</label>
                            <Label class="form-control">
                                <?php
                                if (strcasecmp($detalle_cliente["sexo"],"m") == 0)
                                {
                                    echo "Masculino";
                                }else
                                {
                                    echo "Femenino";
                                }
                                ?>
                            </Label>
                        </div>
                    </div>

                    <div class="col-md-12 form-group">
                        <div class="col-md-4">
                            <label class="control-label">Email</label>
                            <Label class="form-control"><?php echo $detalle_cliente["correo"];?></Label>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Telefono</label>
                            <Label class="form-control"><?php echo $detalle_cliente["telefono"];?></Label>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">DNI</label>
                            <Label class="form-control"><?php echo $detalle_cliente["dni"];?></Label>
                        </div>

                    </div>

                    <div class="col-md-12 form-group">
                        <div class="col-md-4">
                            <label class="control-label">Puntos acumulados</label>
                            <Label class="form-control"><?php echo $detalle_cliente["puntos_acumulados"];?></Label>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Puntos por vencer</label>
                            <Label class="form-control"><?php echo $detalle_cliente["puntos_por_vencer"];?></Label>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Puntos usados</label>
                            <Label class="form-control"><?php echo $detalle_cliente["puntos_usado"];?></Label>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default tabs">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Historial de puntos</a></li>
                    <li><a href="#tab-second" role="tab" data-toggle="tab">Historial de canjes</a></li>

                </ul>
                <div class="panel-body tab-content">
                    <div class="tab-pane active" id="tab-first">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <table class="table table-bordered table-actions">
                                    <thead>
                                    <tr>
                                        <th>Fecha Registro</th>

                                        <th>Tipo de pago</th>
                                        <th>Monto</th>
                                        <th>Puntos Ganados</th>
                                        <th>Puntos Totales</th>
                                        <th>Fecha caducidad</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if (isset($puntos) && count($puntos) > 0)
                                    {

                                        foreach ($puntos as $punto)
                                        {
                                            ?>
                                            <tr>
                                                <td> <?php echo date('d/m/Y g:i a', strtotime($punto["fecha_registro"])); ?></td>
                                                <td><span class="label label-success label-form"><?php echo $punto["tipo_pago"]; ?></span></td>
                                                <td><span class="label label-primary label-form"><?php echo "S/ .".$punto["monto"]; ?></span></td>
                                                <td><span class="label label-primary label-form"><?php echo "+ ".$punto["puntos_ganados"]; ?></span></td>
                                                <td><span class="label label-primary label-form"><?php echo $punto["puntos_totales"]; ?></span></td>
                                                <td> <?php echo date('d/m/Y', strtotime($punto["fecha_caducidad"])); ?></td>
                                            </tr>
                                            <?php
                                        }

                                    }
                                    ?>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-second">

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <table class="table table-bordered table-actions">
                                    <thead>
                                    <tr>
                                        <th>Fecha Registro</th>
                                        <th>Beneficio</th>
                                        <th>Local</th>
                                        <th>Puntos usados</th>
                                        <th>Puntos Totales</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if (isset($canjes) && count($canjes) > 0)
                                    {

                                        foreach ($canjes as $canje)
                                        {
                                            ?>
                                            <tr>
                                                <td> <?php echo date('d/m/Y g:i a', strtotime($canje["fecha_registro"])); ?></td>
                                                <td><span class="label label-success label-form"><?php echo $canje["beneficio"]; ?></span></td>
                                                <td><?php echo $canje["local"]; ?></td>
                                                <td><span class="label label-primary label-form"><?php echo $canje["puntos_usados"]; ?></span></td>
                                                <td><span class="label label-primary label-form"><?php echo $canje["puntos_totales"]; ?></span></td>

                                            </tr>
                                            <?php
                                        }

                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>


                    </div>
                </div>
            </div>


        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

        </div>
    </div>



    <!-- END WIDGETS -->
</div>
<!-- END PAGE CONTENT WRAPPER -->
<script>
    $("#fecha_inicio").datetimepicker({
        format: 'YYYY-MM-DD',
        locale: 'es'
    });

    $("#fecha_fin").datetimepicker({
        format: 'YYYY-MM-DD',
        locale: 'es'
    });
</script>