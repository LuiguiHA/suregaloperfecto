<div class="page-content-wrap">

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Filtros</h3>
                </div>
                <div class="panel-body">
                    <form name="form_busqueda" id="form_busqueda" method="post"
                          action="<?php
                          echo site_url($this->config->item('path_dashboard') . '/Cliente/buscar');
                          ?>">
                    <div class="col-md-12 form-group">
                        <div class="col-md-3">
                            <label class="control-label">Nombre</label>
                            <input type="text" name="nombre" id="nombre" class="form-control" maxlength="200" value="<?php echo $nombre;?>" />
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">Apellido</label>
                            <input type="text" name="apellido" id="apellido" class="form-control" maxlength="200" value="<?php echo $apellido;?>" />
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">DNI</label>
                            <input type="text" name="dni" id="dni" class="form-control" maxlength="200" value="<?php echo $dni;?>" />
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">Email</label>
                            <input type="text" name="correo" id="correo" class="form-control" maxlength="200" value="<?php echo $correo;?>" />
                        </div>
                    </div>
                    <div class="col-md-12 form-group">
                        <input type="submit" class="btn btn-primary pull-right"  value="Buscar" />
                    </div>
                        </form>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-body" >
                    <table class="table table-bordered table-actions" >
                        <thead >
                        <tr >
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>DNI</th>
                            <th>Email</th>
                            <th>Puntos</th>
                            <th width="250">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if (isset($clientes) && count($clientes) > 0)
                        {

                            foreach ($clientes as $cliente)
                            {
                                ?>
                                <tr>
                                    <td><?php echo $cliente["nombre"]; ?></td>
                                    <td><?php echo $cliente["apellido"]; ?></td>
                                    <td><?php echo $cliente["dni"]; ?></td>
                                    <td><?php echo $cliente["correo"]; ?></td>
                                    <td><?php echo $cliente["puntos"]; ?></td>
                                    <td class="text-center">
                                       <!--<a href="<?php echo site_url($this->config->item('path_dashboard') . '/Cliente/detalle_agregar_puntos/' . $cliente["id"]); ?>" data-toggle="tooltip" data-placement="bottom" title="Agregar puntos" class="btn btn-success btn-condensed">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                        <a href="<?php echo site_url($this->config->item('path_dashboard') . '/Cliente/detalle_canjear_puntos/' . $cliente["id"]); ?>" data-toggle="tooltip" data-placement="bottom" title="Canjear puntos" class="btn btn-danger btn-condensed">
                                            <i class="fa fa-minus"></i>
                                        </a>-->
                                        <a href="<?php echo site_url($this->config->item('path_dashboard') . '/Cliente/historial/' . $cliente["id"]); ?>" data-toggle="tooltip" data-placement="bottom" title="Historial" class="btn btn-info btn-condensed">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                      <!--  <a href="<?php echo site_url($this->config->item('path_dashboard') . '/Cliente/descuento/' . $cliente["id"]); ?>" data-toggle="tooltip" data-placement="bottom" title="Aplicar dscto" class="btn btn-info btn-condensed">
                                            <i class="fa fa-tag"></i>
                                        </a>-->
                                        <a href="<?php echo site_url($this->config->item('path_dashboard') . '/Cliente/detalle/' . $cliente["id"]); ?>" data-toggle="tooltip" data-placement="bottom" title="Ve detalle de usuario" class="btn btn-info btn-condensed">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>

                                </tr>
                                <?php
                            }

                        }
                        ?>
                        </tbody>
                    </table>
                    <?php

                    if (isset($paginador))
                        echo $paginador;
                    ?>
                </div>
            </div>
        </div>

    </div>

    <!-- END WIDGETS -->
</div>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery(document).on('click','#paginador ul li a',function(c){
            c.preventDefault();
            var inicio = jQuery(this).attr('href');
            jQuery("#form_busqueda").attr("action",inicio);
            jQuery("#form_busqueda").submit();
        });
    });

    $(document).ready(function(){
        $('#dni').keypress(validateNumber);


    });

    function validateNumber(event) {
        var key = window.event ? event.keyCode : event.which;

        if (event.keyCode === 8 || event.keyCode === 46
            || event.keyCode === 37 || event.keyCode === 39) {
            return true;
        }
        else if ( key < 48 || key > 57 ) {
            return false;
        }
        else return true;
    }
</script>