<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">



    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal" id="formGuardar" enctype="multipart/form-data"  method="POST" >
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Datos de cliente</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-md-1 col-xs-12 control-label">Nombre</label>
                            <div class="col-md-11 col-xs-12">
                                <Label class="form-control"><?php echo $cliente["nombre"];?></Label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-1 col-xs-12 control-label">Apellido</label>
                            <div class="col-md-11 col-xs-12">
                                <Label class="form-control"><?php echo $cliente["apellido"];?></Label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-1 col-xs-12 control-label">Sexo</label>
                            <div class="col-md-11 col-xs-12">
                                <Label class="form-control">
                                    <?php
                                    if (strcasecmp($cliente["sexo"],"m") == 0)
                                    {
                                        echo "Masculino";
                                    }else
                                    {
                                        echo "Femenino";
                                    }
                                    ?>
                                </Label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-1 col-xs-12 control-label">Fecha Nacimiento</label>
                            <div class="col-md-11 col-xs-12">
                                <Label class="form-control"><?php echo date('d/m/Y ', strtotime($cliente["fecha_nacimiento"])); ?></Label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-1 col-xs-12 control-label">Email</label>
                            <div class="col-md-11 col-xs-12">
                                <Label class="form-control"><?php echo $cliente["correo"];?></Label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-1 col-xs-12 control-label">Teléfono</label>
                            <div class="col-md-11 col-xs-12">
                                <Label class="form-control"><?php echo $cliente["telefono"];?></Label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-1 col-xs-12 control-label">Puntos acumulados</label>
                            <div class="col-md-11 col-xs-12">
                                <Label class="form-control"><?php echo $cliente["punto_acumulado"];?></Label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-1 col-xs-12 control-label">Puntos por vencer</label>
                            <div class="col-md-11 col-xs-12">
                                <Label class="form-control"><?php echo $cliente["puntos_por_vencer"];?></Label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-1 col-xs-12 control-label">Puntos usados</label>
                            <div class="col-md-11 col-xs-12">
                                <Label class="form-control"><?php echo $cliente["puntos_usado"];?></Label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-1 col-xs-12 control-label">Fecha registro</label>
                            <div class="col-md-11 col-xs-12">
                                <Label class="form-control"><?php echo date('d/m/Y ', strtotime($cliente["fecha_registro"])); ?></Label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-1 col-xs-12 control-label">Fecha último visita</label>
                            <div class="col-md-11 col-xs-12">
                                <Label class="form-control"><?php echo date('d/m/Y ', strtotime($cliente["ultima_visita"])); ?></Label>
                            </div>
                        </div>

                        <!--
                            <div class="form-group">
                                <label class="col-md-1 control-label">Nombre</label>
                                <div class="col-md-11">
                                    <input type="text" class="form-control" value="" name="nombre"  id="nombre" value="Juan Carlos" />
                                </div>
                            </div>

                            <div class="col-md-12 form-group">

                                <div class="col-md-4">
                                    <label class="control-label">Nombre</label>
                                    <input type="text" name="titulo" id="titulo" class="form-control" maxlength="200" value="Juan Carlos" />
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label">Apellidos</label>
                                    <Label class="form-control">Ferro Laura</Label>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label">Sexo</label>
                                    <Label class="form-control">Masculino</Label>
                                </div>
                            </div>

                            <div class="col-md-12 form-group">
                                <div class="col-md-4">
                                    <label class="control-label">Email</label>
                                    <Label class="form-control">juankferro@gmail.com</Label>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label">Telefono</label>
                                    <Label class="form-control">961787196</Label>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label">DNI</label>
                                    <Label class="form-control">42029488</Label>
                                </div>

                            </div>

                            <div class="col-md-12 form-group">
                                <div class="col-md-4">
                                    <label class="control-label">Puntos acumulados</label>
                                    <Label class="form-control">1500</Label>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label">Puntos por vencer</label>
                                    <Label class="form-control">50</Label>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label">Puntos usados</label>
                                    <Label class="form-control">250</Label>
                                </div>

                            </div>-->

                    </div>
                </div>
            </form>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">

        </div>
    </div>



    <!-- END WIDGETS -->
</div>
<!-- END PAGE CONTENT WRAPPER -->       