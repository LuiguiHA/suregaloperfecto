<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Datos de compra</h3>
                </div>
                <div class="panel-body">
                    <form name="agregar_puntos" id="agregar_puntos" method="post"
                          action="<?php
                          echo site_url($this->config->item('path_dashboard') .'/Cliente/agregar_puntos/'.$detalle_cliente["id"]);
                          ?>">
                    <div class="col-md-12 form-group">
                        <div class="col-md-6">
                            <label class="control-label">Tipo de pago</label>
                            <select class="form-control select" id="tipo_pago"  name="tipo_pago" required="true">
                                <option value="">Seleccione</option>
                                <?php foreach ($tipo_pagos as $tipo_pago) { ?>
                                    <option value="<?php echo $tipo_pago["id"];?>"  <?php if ($tipo_pago_select == $tipo_pago["id"]) echo "selected" ;?>>
                                        <?php echo $tipo_pago["nombre"]; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Monto</label>
                            <input type="text" name="monto" id="monto" class="form-control" maxlength="200" value="" />
                        </div>
                    </div>
                    <div class="col-md-12 form-group">
                        <input type="submit" class="btn btn-primary pull-right"  value="Guardar" />
                    </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Datos de cliente</h3>
                </div>
                <div class="panel-body">
                    <div class="col-md-12 form-group">
                        <div class="col-md-4">
                            <label class="control-label">Nombre</label>
                            <Label class="form-control"><?php echo $detalle_cliente["nombre"];?></Label>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Apellidos</label>
                            <Label class="form-control"><?php echo $detalle_cliente["apellido"];?></Label>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Sexo</label>
                            <Label class="form-control"><?php
                                if (strcasecmp($detalle_cliente["sexo"],"m") == 0)
                                {
                                    echo "Masculino";
                                }else
                                {
                                    echo "Femenino";
                                }
                                ?></Label>
                        </div>
                    </div>

                    <div class="col-md-12 form-group">
                        <div class="col-md-4">
                            <label class="control-label">Email</label>
                            <Label class="form-control"><?php echo $detalle_cliente["correo"];?></Label>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Telefono</label>
                            <Label class="form-control"><?php echo $detalle_cliente["telefono"];?></Label>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">DNI</label>
                            <Label class="form-control"><?php echo $detalle_cliente["dni"];?></Label>
                        </div>

                    </div>

                    <div class="col-md-12 form-group">
                        <div class="col-md-4">
                            <label class="control-label">Puntos acumulados</label>
                            <Label class="form-control"><?php echo $detalle_cliente["puntos_acumulados"];?></Label>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Puntos por vencer</label>
                            <Label class="form-control"><?php echo $detalle_cliente["puntos_por_vencer"];?></Label>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Puntos usados</label>
                            <Label class="form-control"><?php echo $detalle_cliente["puntos_usado"];?></Label>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Historial de puntos</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered table-actions">
                        <thead>
                        <tr>
                            <th>Fecha Registro</th>
                            <th>Tipo de pago</th>
                            <th>Monto</th>
                            <th>Puntos Ganados</th>
                            <th>Puntos Totales</th>
                            <th>Fecha caducidad</th>
                        </tr>
                        </thead>
                        <tbody>
                        
                        <?php
                        if (isset($puntos) && count($puntos) > 0)
                        {

                            foreach ($puntos as $punto)
                            {
                                ?>
                                <tr>
                                    <td> <?php echo date('d/m/Y g:i a', strtotime($punto["fecha_registro"])); ?></td>
                                    <td><span class="label label-success label-form"><?php echo $punto["tipo_pago"];?></span></td>
                                    <td><span class="label label-primary label-form"><?php echo "S/. ".$punto["monto"];?></span></td>
                                    <td><span class="label label-primary label-form"><?php echo "+ ".$punto["puntos_ganados"];?></span></td>
                                    <td><span class="label label-primary label-form"><?php echo $punto["puntos_totales"];?></span></td>
                                    <td><?php echo date('d/m/Y', strtotime($punto["fecha_caducidad"])); ?></td>
                                </tr>
                                <?php
                            }

                        }
                        ?>
                        </tbody>
                    </table>
                    <?php

                    if (isset($paginador))
                        echo $paginador;
                    ?>
                </div>
            </div>
        </div>
    </div>



    <!-- END WIDGETS -->
</div>
<!-- END PAGE CONTENT WRAPPER -->    