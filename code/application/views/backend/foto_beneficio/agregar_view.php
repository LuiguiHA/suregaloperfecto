<!-- PAGE CONTENT WRAPPER -->

<div class="page-content-wrap">

    <div class="container-fluid">
        <div class="col-md-12 panel-body">
            <div class="block" style="background-color:#EEEEEE;margin-bottom: 0px;padding-bottom:20px;">
                <form action="<?php echo site_url($this->config->item('path_backend') . '/Foto_Beneficio/agregar/'.$beneficio); ?>"
                      id="agregar_foto" class="form-horizontal" role="form" name="agregar_foto" method="post"
                      enctype="multipart/form-data" >


                    <div class="form-group">
                        <label class="col-md-1 control-label">Foto</label>
                        <div class="col-md-11">
                        
                            <input type="file" class="fileinput  btn-success"
                                   name="foto" id="foto" data-filename-placement="inside" title="Seleccione una Imagen"
                                   accept=".jpg,.png,.jpeg"/>
                            <label id="error_img" style="visibility: hidden ; color: #b64645 ; font-weight : normal">La imagen es necesaria. </label>
                        </div>

                        </div>


                    <div class="form-group">
                        <label class="col-md-1 control-label">Tipo</label>
                        <div class="col-md-11">
                            <select class="form-control select" name="tipo" id="tipo">
                                <option value="">Seleccione</option>
                                <option value="1">Principal</option>
                                <option value="2">Detalle</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Plataforma</label>
                        <div class="col-md-11">
                            <select class="form-control select" name="plataforma" id="plataforma">
                                <option value="">Seleccione</option>
                                <option value="1" >Web</option>
                                <option value="2" >Móvil</option>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-1 control-label">Estado</label>
                        <div class="col-md-11">
                            <select class="form-control select" name="estado" id="estado">
                                <option value="">Seleccione</option>
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                        </div>
                    </div>


                    <div class="btn-group pull-right">
                        <a type="button" class="btn btn-primary" style="margin-right: .5em;"
                           href="<?php echo site_url($this->config->item('path_backend') . '/Beneficio/editar/'.$beneficio.'/F'); ?>">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="Guardar" name="agregar" id="agregar"/>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT WRAPPER -->
<script type="text/javascript">



    var jvalidate = $("#agregar_foto").validate({
        ignore: [],
        rules: {
            estado: {
                required: true
            },
            tipo:{
                required: true
            },
            plataforma:{
                required: true
            }

        }
    });

    $("#agregar_foto").submit(function () {

        var foto = $("#foto").val();
        if(foto == "") {
            error_img.style.visibility='visible';
            return false;
        }else {
            error_img.style.visibility='hidden';
            return true;
        }
    });



</script>
