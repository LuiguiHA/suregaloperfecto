
<!-- PAGE CONTENT WRAPPER -->

<div class="page-content-wrap">

    <div class="container-fluid">
        <div class="col-md-12 panel-body">
            <div class="block" style="background-color:#EEEEEE;margin-bottom: 0px;padding-bottom:20px;">
                <form action="<?php echo site_url($this->config->item('path_backend').'/Categoria_Beneficio/agregar');?>" id="agregar_categoria" class="form-horizontal" role="form" name="agregar_categoria" method="post"
                      enctype="multipart/form-data" action="">


                    <div class="form-group">
                        <label class="col-md-1 control-label">Nombre</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="" name="nombre"  id="nombre" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Descripción</label>
                        <div class="col-md-11">

                            <textarea type="text" class="form-control" value="" name="descripcion" id="descripcion"></textarea>
                        </div>
                    </div>

                   <!-- <div class="form-group">
                        <label class="col-md-1 control-label" >Foto</label>
                        <div class="col-md-11">

                            <div class="input-group date col-md-6" id="dp-2">
                                <input type="file" class="fileinput btn-success" data-filename-placement="inside" name="foto" id="foto"
                                       title="Seleccione una Imagen" style="left: -83.9063px; top: 5px;" accept=".jpg,.png,.jpeg">
                                <span class="file-input-name "></span>
                            </div>

                        </div>

                    </div>-->


                    <div class="form-group">
                        <label class="col-md-1 control-label">Estado</label>
                        <div class="col-md-11">
                            <select class="form-control select" name="estado" id="estado" >
                                <option value="">Seleccione</option>
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                        </div>
                    </div>



                    <div class="btn-group pull-right">
                        <a type="button" class="btn btn-primary"  style="margin-right: .5em;" href="<?php echo site_url($this->config->item('path_backend').'/inicio');?>">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="Guardar" name="agregar" id="agregar" />
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT WRAPPER -->
<script type="text/javascript">


   var jvalidate = $("#agregar_categoria").validate({
       ignore: [],
       rules: {

           nombre: {
               required: true
           },

           estado: {
               required: true
           }
       }
   });



</script>
