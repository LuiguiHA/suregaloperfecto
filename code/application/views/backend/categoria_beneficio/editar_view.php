
<!-- PAGE CONTENT WRAPPER -->

<div class="page-content-wrap">

    <div class="container-fluid">
        <div class="col-md-12 panel-body">
            <div class="block" style="background-color:#EEEEEE;margin-bottom: 0px;padding-bottom:20px;">
                <form action="<?php echo site_url($this->config->item('path_backend').'/Categoria_Beneficio/editar/'.$categoria->id);?>" id="editar_categoria" class="form-horizontal" role="form" name="editar_categoria" method="post"
                      enctype="multipart/form-data" action="">


                    <div class="form-group">
                        <label class="col-md-1 control-label">Nombre</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="<?php echo $categoria->nombre?>" name="nombre"  id="nombre" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Descripción</label>
                        <div class="col-md-11">

                            <textarea type="text" class="form-control"  name="descripcion" id="descripcion"><?php echo $categoria->descripcion?></textarea>
                        </div>
                    </div>


                   <!-- <div class="form-group">
                        <label class="col-md-1 control-label" >Foto</label>
                        <div class="col-md-11">
                            <?php if ($categoria->foto ==null || $categoria->foto == ""){
                                ?>

                            <div class="input-group date col-md-6" id="dp-2">
                                <span class="file-input-name"> No cuenta con imagen </span>
                            </div>
                            <?php } else {?>
                            <div class="input-group date col-md-6" id="dp-2">
                                <input type="button" class="btn btn-primary" onclick="lightbox_open();" value="Ver Imagen" style=" left: -83.9063px; top: 5px; ";/>

                            </div>
                            <?php }?>
                            <div id="light" >
                                <a href="#" onclick="lightbox_close();"><img src="<?php echo base_url().$categoria->foto;
                                   ?> "width="100%" /></a>
                            </div>
                            <div id="fade" onClick="lightbox_close();"></div>
                            </br>
                            <div class="input-group date col-md-6" id="dp-2">
                                <input type="file" class="fileinput btn-success" name="foto" id="foto" data-filename-placement="inside"
                                       title="Seleccione una Imagen" style="left: -83.9063px; top: 5px;" accept=".jpg,.png,.jpeg">
                                <span class="file-input-name "></span>
                            </div>

                        </div>

                    </div>
                    <input class="hidden" type="text" value="<?php echo $categoria->foto;?>" name="foto_antiguo"/>-->

                    <div class="form-group">
                        <label class="col-md-1 control-label">Estado</label>
                        <div class="col-md-11">
                            <select class="form-control select" name="estado" id="estado" >
                                <option value="">Seleccione</option>
                                <option value="1" <?php if($categoria->estado == "1") echo "selected";?>>Activo</option>
                                <option value="0" <?php if($categoria->estado == "0") echo "selected";?>>Inactivo</option>
                            </select>
                        </div>
                    </div>



                    <div class="btn-group pull-right">
                        <a type="button" class="btn btn-primary"  style="margin-right: .5em;" href="<?php echo site_url($this->config->item('path_backend').'/inicio');?>">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="Guardar" name="agregar" id="agregar" />
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT WRAPPER -->
<script type="text/javascript">


     var jvalidate = $("#editar_categoria").validate({
     ignore: [],
     rules: {

     nombre: {
     required: true
     },

     estado: {
     required: true
     }
     }
     });

</script>
