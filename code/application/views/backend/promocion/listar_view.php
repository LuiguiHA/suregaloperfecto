<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap" style="padding: 15px">
    <div class="container-fluid" style="padding-left: 0px !important;padding-right: 0px !important;">
        <div class="panel panel-default" style="border-top-color: #DAD4D4 ; border-top-width : 2px">
            <div class="panel-body">
                <div class="panel-body">


                    <form name="form_busqueda" id="form_busqueda" method="post"
                          action="<?php
                          echo site_url($this->config->item('path_backend') . '/Promocion/buscar');
                          ?>">
                        <div class="col-md-12 form-group">

                            <div class="col-md-4">
                                <label class="control-label"> Nombre</label>
                                <input id="nombre" class="form-control input" type="text" name="nombre"
                                       placeholder="Nombre" value="<?php echo $nombre; ?>" class="input-small"/>
                            </div>

                            <div class="col-md-4">
                                <label class="control-label">Empresa</label>

                                <select class="form-control select" name="empresa" id="empresa">
                                    <option value="">Seleccione</option>
                                    <?php foreach ($empresas as $empresas)
                                    { ?>
                                        <option
                                            value="<?php echo $empresas->id; ?>" <?php if ($empresa == $empresas->id) echo "selected"; ?>>
                                            <?php echo $empresas->nombre; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="col-md-4">
                                <label class="control-label">Categoría</label>
                                <select class="form-control select" name="categoria" id="categoria">
                                    <option value="">Seleccione</option>
                                    <?php foreach ($categorias as $categoria)
                                    { ?>
                                        <option
                                            value="<?php echo $categoria->id; ?>" <?php if ($categoria->id == $categoria_select)
                                        {
                                            echo "selected";
                                        } ?>>
                                            <?php echo $categoria->nombre; ?>
                                        </option>
                                    <?php } ?>
                                </select>

                            </div>
                        </div>
                        <div class="col-md-12 form-group">

                            <div class="col-md-4">
                                <label class="control-label">Fecha de Inicio</label>
                                <input type="text" class="form-control" value="<?php echo $fecha_inicio?>" name="fecha_inicio" id="fecha_inicio" />

                            </div>

                            <div class="col-md-4">
                                <label class="control-label">Fecha de Finalización</label>
                                <input type="text" class="form-control" value="<?php echo $fecha_fin?>" name="fecha_fin" id="fecha_fin" />

                            </div>

                            <div class="col-md-4">
                                <label class="control-label">Estado</label>
                                <select class="form-control select" name="estado" id="estado">
                                    <option value="">Seleccione</option>
                                    <option value="1" <?php if ($estado == "1") echo "selected"; ?>>Activo</option>
                                    <option value="0" <?php if ($estado == "0") echo "selected"; ?>>Inactivo</option>
                                </select>

                            </div>

                        </div>
                        <div class="col-md-12 form-group">
                            <input type="hidden" name="buscar" id="buscar"
                                   value=""/>

                        </div>
                        <div class="col-md-12 form-group">
                            <div class="col-md-4">

                            </div>
                            <div class="col-md-8">
                                <input type="submit" class="btn btn-primary pull-right" value="Buscar"/>
                            </div>
                        </div>

                    </form>


                </div>

            </div>
        </div>
    </div>
    <div class="panel panel-default" style="border-top-color: #DAD4D4 ; border-top-width : 2px">
        <div class="panel-heading">
            <h3 class="panel-title">Promociones</h3>
            <a id="export_promocion" class="btn btn-danger dropdown-toggle pull-right" style="margin-left: 5px;"><img
                    src="<?php echo base_url('assets/backend/'); ?>/img/icons/xls.png" width="16"/> Exportar</a>
            <a href="<?php echo site_url($this->config->item('path_backend') . '/Promocion/agregar'); ?>"
               class="btn btn-default pull-right"><i class="fa fa-plus"></i> Agregar</a>


        </div><?php


        ?>
        <div class="panel-body">
            <table class="table table-hover table-cms">
                <thead>
                <tr>
                    <td colspan="7" align="center" valign="top"></td>
                </tr>
                <tr>
                    <th>Nombre</th>
                    <th>Descuento</th>
                    <th>Categoría</th>
                    <th>Fecha de Inicio</th>
                    <th>Fecha de Finalización</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>

                <?php
                if (isset($promociones) && count($promociones) > 0)
                {

                    foreach ($promociones as $promocion)
                    {
                        ?>
                        <tr>
                            <td><?php echo $promocion->nombre; ?></td>
                            <td><?php echo $promocion->descuento; ?></td>
                            <td><?php echo $promocion->nombre_categoria; ?></td>
                            <td><?php echo date('Y-m-d', strtotime($promocion->fecha_inicio)); ?></td>
                            <td><?php echo date('Y-m-d', strtotime($promocion->fecha_fin)); ?></td>
                            <td>
                                <?php
                                if ($promocion->estado == 1) echo '<span class="label label-success label-form" style="width:70px; font-weight:bolder;">Activo</span>'; else
                                    echo '<span class="label label-danger label-form" style="width:70px; font-weight:bolder;">Inactivo</span>';
                                ?>
                            </td>
                            <td>

                                <a href="<?php echo site_url($this->config->item('path_backend') . '/Promocion/editar/' . $promocion->id); ?>"
                                   class="btn btn-info btn-condensed" title="Editar"><i class="fa fa-edit"></i></a>
                            </td>

                        </tr>
                        <?php
                    }

                }
                ?>

                </tbody>
            </table>
            <?php


            if (isset($paginador)) echo $paginador;
            ?>

        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery(document).on('click', '#paginador ul li a', function (c) {
            c.preventDefault();
            var inicio = jQuery(this).attr('href');
            jQuery("#form_busqueda").attr("action", inicio);
            jQuery("#form_busqueda").submit();
        });
    });
    $('#export_promocion').click(function () {
        // e.preventDefault();

        var url = "<?php echo site_url($this->config->item('path_backend') . '/Promocion/export');?>";
        var nombre = $("#nombre").val();
        var categoria = $("#categoria").val();
        var empresa = $("#empresa").val();
        var fecha_inicio = $("#fecha_inicio").val();
        var fecha_fin = $("#fecha_fin").val();
        var estado = $("#estado").val();
        window.location.href = url + "?nombre=" + nombre + "&empresa=" + empresa + "&fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin + "&categoria=" + categoria + "&estado=" + estado;


    });

    $("#fecha_inicio").datetimepicker({
        format: 'YYYY-MM-DD',
        locale: 'es'
    });

    $("#fecha_fin").datetimepicker({
        format: 'YYYY-MM-DD',
        locale: 'es'
    });
</script>


<!-- END PAGE CONTENT WRAPPER -->