
<!-- PAGE CONTENT WRAPPER -->

<div class="page-content-wrap">

    <div class="container-fluid">
        <div class="col-md-12 panel-body">
            <div class="block" style="background-color:#EEEEEE;margin-bottom: 0px;padding-bottom:20px;">
                <form action="<?php echo site_url($this->config->item('path_backend').'/Usuario/agregar');?>" id="agregar_usuario" class="form-horizontal" role="form" name="agregar_usuario" method="post"
                      enctype="multipart/form-data" action="">

                    <div class="form-group">
                        <label class="col-md-2 control-label">Nombre</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" value="" name="nombre"  id="nombre" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Apellido</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" value="" name="apellido"  id="apellido" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">DNI</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" value="" name="dni"  id="dni" maxlength="8" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Sexo</label>
                        <div class="col-md-10">
                            <select class="form-control select" name="sexo" id="sexo" >
                                <option value="">Seleccione</option>
                                <option value="M">Masculino</option>
                                <option value="F">Femenino</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Fecha de Nacimiento</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" value="" name="fecha_nacimiento" id="fecha_nacimiento" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Empresa</label>
                        <div class="col-md-10">
                            <select class="form-control select" name="empresa" id="empresa" >
                                <option value="">Seleccione</option>
                                <?php foreach ($empresas as $empresa) { ?>
                                    <option value="<?php echo $empresa->id;?>">
                                        <?php echo $empresa->nombre; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-2 control-label">Correo</label>
                        <div class="col-md-10">

                            <input type="email" class="form-control" value="" name="correo" id="correo"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Contraseña</label>
                        <div class="col-md-10">
                            <input type="password" class="form-control" value="" name="contrasenia" id="contrasenia"/> 
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Puntos Acumulados</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" value="" name="puntos" id="puntos" />
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-2 control-label">Estado</label>
                        <div class="col-md-10">
                            <select class="form-control select" name="estado" id="estado" >
                                <option value="">Seleccione</option>
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                        </div>
                    </div>

                    

                    <br/>
                    <div class="btn-group pull-right">
                        <a type="button" class="btn btn-primary"  style="margin-right: .5em;" href="<?php echo site_url($this->config->item('path_backend').'/Usuario');?>">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="Guardar" name="agregar" id="agregar" />
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT WRAPPER -->
<script type="text/javascript">

   var jvalidate = $("#agregar_usuario").validate({
       ignore: [],
       rules: {
           nombre: {
               required: true
           },
           apellido: {
               required: true

           },
           dni: {
               required: true

           },
           sexo: {

               required: true
           },
           estado: {
               required: true

           },
           empresa: {
               required: true

           },
           correo: {
               required: true,
               email : true

           },
           contrasenia: {
                required: true
           },
           puntos: {
               required: true
           }

       }
   });
   $(document).ready(function () {
       $('#dni').keypress(validateNumber);
       $('#puntos').keypress(validateNumber);

   });

   function validateNumber(event) {
       var key = window.event ? event.keyCode : event.which;

       if (event.keyCode === 8 || event.keyCode === 46
           || event.keyCode === 37 || event.keyCode === 39) {
           return true;
       }
       else if (key < 48 || key > 57) {
           return false;
       }
       else return true;
   }

   $("#fecha_nacimiento").datetimepicker({
       format: 'YYYY-MM-DD',
       locale: 'es'
   });




</script>
