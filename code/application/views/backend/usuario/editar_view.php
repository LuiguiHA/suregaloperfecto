<!-- PAGE CONTENT WRAPPER -->

<div class="panel panel-default tabs">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab1" data-toggle="tab">Datos Personales</a></li>
        <li><a href="#tab2" data-toggle="tab" id="lista_usuario_empresa">Empresa</a></li>


    </ul>


    <div class="tab-content">
        <div class="tab-pane panel-body active" id="tab1">

            <form action="<?php echo site_url($this->config->item('path_backend').'/Usuario/editar/'.$usuario->id);?>" id="editar_usuario" class="form-horizontal"
                  role="form" name="editar_usuario" method="post"
                  enctype="multipart/form-data" >

                <div class="form-group">
                    <label class="col-md-2 control-label">Nombre</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" value="<?php echo $usuario->nombre;?>" name="nombre"  id="nombre" readonly="true" style = "color:#656d78; font-weight:600"  />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Apellido</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" value="<?php echo $usuario->apellido;?>" name="apellido"  id="apellido" readonly="true" style = "color:#656d78; font-weight:600"  />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">DNI</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" value="<?php echo $usuario->dni;?>" name="dni"  id="dni" maxlength="8" minlength="8" readonly="true" style = "color:#656d78; font-weight:600"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Sexo</label>
                    <div class="col-md-10">
                    <?php 
                         if(strtolower($usuario->sexo) == "m") $sexo = "Masculino";
                         if(strtolower($usuario->sexo) == "F") $sexo = "Femenino";
                    ?>
                     <input type="text" class="form-control" value="<?php echo $sexo;?>" name="sexo"  id="sexo" readonly="true" style = "color:#656d78; font-weight:600"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Fecha de Nacimiento</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" value="<?php echo date('Y-m-d', strtotime($usuario->fecha_nacimiento)); ?>" name="fecha_nacimiento" id="fecha_nacimiento" readonly="true" 
                        style = "color:#656d78; font-weight:600"/>
                    </div>
                </div>
                

                <div class="form-group">
                    <label class="col-md-2 control-label">Estado</label>
                    <div class="col-md-10">
                        <select class="form-control select" name="estado" id="estado" >
                            <option value="">Seleccione</option>
                            <option value="1" <?php if($usuario->estado == "1") echo "selected";?>>Activo</option>
                            <option value="0" <?php if($usuario->estado == "0") echo "selected";?>>Inactivo</option>
                        </select>
                    </div>
                </div>



                <div class="btn-group pull-right">
                    <a type="button" class="btn btn-primary"  style="margin-right: .5em;" href="<?php echo site_url($this->config->item('path_backend').'/Usuario');?>">Cancelar</a>
                    <input type="submit" class="btn btn-primary" value="Guardar" name="agregar" id="agregar" />
                </div>


            </form>

        </div>
        <div class="tab-pane panel-body " id="tab2">
            <div class="page-content-wrap">
                <div class="container-fluid">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="panel-body">


                                <form name="form_busqueda" id="form_busqueda" method="post" action="">
                                    <div class="col-md-12 form-group">

                                        <div class="col-md-12">
                                            <label class="control-label"> Nombre</label>
                                            <input id="nombre_empresa" class="form-control input" type="text" name="nombre_empresa"
                                                   placeholder="Nombre" value="<?php echo $nombre; ?>" class="input-small"/>
                                        </div>



                                        <input type="hidden" name="buscar" id="buscar"
                                               value=""/>

                                    </div>
                                    <div class="col-md-12 form-group">
                                        <div class="col-md-4">

                                        </div>
                                        <div class="col-md-8">

                                            <a href="#" class="btn btn-primary pull-right" id="buscar_usuario_empresa">Buscar</a>
                                        </div>

                                    </div>

                                </form>


                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Empresas</h3>
                        <a   id="export_empresa_persona" class="btn btn-danger dropdown-toggle pull-right"  style="margin-left: 5px;" ><img src="<?php echo base_url('assets/backend/');?>/img/icons/xls.png" width="16"/> Exportar</a>


                    </div>
                    <div id="tabla_usuario_empresa" class="table-cms"></div>

                </div>


            </div>
        </div>

    </div>
</div>
<!-- END PAGE CONTENT WRAPPER -->
<script type="text/javascript">
    var jvalidate = $("#editar_usuario").validate({
        ignore: [],
        rules: {
            nombre: {
                required: true
            },
            apellido: {
                required: true

            },
            dni: {
                required: true
            },
            sexo: {

                required: true
            },
            estado: {
                required: true

            }

        }
    });
    $(document).ready(function(){
        $('#dni').keypress(validateNumber);


    });

    function validateNumber(event) {
        var key = window.event ? event.keyCode : event.which;

        if (event.keyCode === 8 || event.keyCode === 46
            || event.keyCode === 37 || event.keyCode === 39) {
            return true;
        }
        else if ( key < 48 || key > 57 ) {
            return false;
        }
        else return true;
    }


    $("#lista_usuario_empresa").click(function () {

        $.post("<?php echo site_url('backend/Usuario/listar_empresa');?>", {id_usuario : <?php echo $usuario->id;?>},
            function (result) {

                $('#tabla_usuario_empresa').html(result);


            }, "html");

        return true;
    });

    $("#buscar_usuario_empresa").click(function () {


        $.post("<?php echo site_url('backend/Usuario/buscar_empresa');?>",
            {id_usuario : <?php echo $usuario->id;?> , nombre : $("#nombre_empresa").val()},
            function (result) {
                $('#tabla_usuario_empresa').html(result);

            }, "html");

        return true;
    });






    <?php
    switch($this->uri->segment(5)){
    case 'F' :
    ?>
    $(document).ready(function () {
        $('#lista_beneficio_foto').trigger('click');
    });
    <?php  break;
    case 'I' :
    ?>
    $(document).ready(function () {
        $('#lista_beneficio_info').trigger('click');
    });
    <?php  break;
    case 'L' :
    ?>
    $(document).ready(function () {
        $('#lista_beneficio_local').trigger('click');
    });
    <?php
    break;
    }
    ?>


    $('#export_empresa_persona').click(function(e){
         e.preventDefault();

        var url = "<?php echo site_url($this->config->item('path_backend').'/Usuario/export_user_empresa');?>";
        var id_usuario = <?php echo $usuario->id;?>;
        var nombre = $("#nombre_empresa").val();

            window.location.href= url+"?id_usuario="+id_usuario+"&nombre="+nombre;

    });

</script>
