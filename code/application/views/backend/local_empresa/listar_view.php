<!-- PAGE CONTENT WRAPPER -->



        
        <div class="panel-body" >
            <table class="table table-hover table-cms" >
                <thead>
                <tr>
                    <td colspan="6" align="center" valign="top"></td>
                </tr>
                <tr>
                    <th>Nombre</th>
                    <th>Dirección</th>
                    <th>Localidad</th>
                    <th>Telefono</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>


                <?php
                     

                if (isset($local_empresa) && count($local_empresa) > 0) {

                    foreach ($local_empresa as $local) {
                        ?>
                        <tr>

                            <td><?php echo $local->nombre; ?></td>
                            <td><?php echo $local->direccion; ?></td>
                            <td><?php echo $local->localidad; ?></td>
                            <td><?php echo $local->telefono; ?></td>
                            <td>
                                <?php
                                if ($local->estado == 1)
                                    echo '<span class="label label-success label-form" style="width:70px; font-weight:bolder;">Activo</span>';
                                else
                                    echo '<span class="label label-danger label-form" style="width:70px; font-weight:bolder;">Inactivo</span>'
                                ?>
                            </td>
                            <td>

                                <a href="<?php echo site_url($this->config->item('path_backend') . '/Local_Empresa/editar/'.$empresa.'/'.$local->id); ?>"
                                   class="btn btn-info btn-condensed" title="Editar"><i class="fa fa-edit"></i></a>
                            </td>

                        </tr>
                        <?php
                    }

                }
                ?>

                </tbody>
            </table>
            <?php
            
            if (isset($paginador))
                echo $paginador;
            ?>
        </div>



<script>

    $(document).on("click", "#paginador ul li a", function(e){
        e.preventDefault();
        var href = $(this).attr("href");

        // alert(href);
        $.post(href, {id_empresa : <?php echo $empresa;?> , nombre :$("#nombre_local").val() ,
                direccion : $("#direccion_local").val(),estado : $("#estado_local").val()},
            function (result) {

                $('#tabla_empresa_local').html(result);


            }, "html");

        return true;
    });

</script>
<!-- END PAGE CONTENT WRAPPER -->