<!-- PAGE CONTENT WRAPPER -->

<div class="page-content-wrap">

    <div class="container-fluid">
        <div class="col-md-12 panel-body">
            <div class="block" style="background-color:#EEEEEE;margin-bottom: 0px;padding-bottom:20px;">
                <form action="<?php echo site_url($this->config->item('path_backend') . '/Foto_Promocion/editar/'.$promocion.'/'.$foto->id); ?>"
                      id="editar_foto" class="form-horizontal" role="form" name="editar_foto" method="post"
                      enctype="multipart/form-data" >



                    <div class="form-group">
                        <label class="col-md-1 control-label" >Foto</label>
                        <div class="col-md-11">

                                <div class="input-group date col-md-6" id="dp-2">
                                    <input type="button" class="btn btn-primary" onclick="lightbox_open();" value="Ver Imagen" style=" left: -83.9063px; top: 5px; ";/>

                                </div>

                            <div id="light" >
                                <a href="#" onclick="lightbox_close();"><img src="<?php echo base_url().$foto->foto;?>"  width="100%" /></a>
                            </div>
                            <div id="fade" onClick="lightbox_close();"></div>
                            </br>
                            <div class="input-group date col-md-6" id="dp-2">
                                <input type="file" class="fileinput  btn-success"
                                       name="foto" id="foto" data-filename-placement="inside" title="Seleccione una Imagen"
                                       accept=".jpg,.png,.jpeg"/>
                                
                                <span class="file-input-name "></span>
                            </div>

                        </div>

                    </div>

                    <input class="hidden" type="text" value="<?php echo $foto->foto?>" name="foto_antigua"/>


                    <div class="form-group">
                        <label class="col-md-1 control-label">Tipo</label>
                        <div class="col-md-11">
                            <select class="form-control select" name="tipo" id="tipo">
                                <option value="">Seleccione</option>
                                <option value="1" <?php if($foto->tipo == "1") echo "selected";?>>Principal</option>
                                <option value="2"<?php if($foto->tipo == "2") echo "selected";?>>Detalle</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Plataforma</label>
                        <div class="col-md-11">
                            <select class="form-control select" name="plataforma" id="plataforma">
                                <option value="">Seleccione</option>
                                <option value="1" <?php if($foto->plataforma == "1") echo "selected";?>>Web</option>
                                <option value="2"<?php if($foto->plataforma == "2") echo "selected";?>>Móvil</option>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-1 control-label">Estado</label>
                        <div class="col-md-11">
                            <select class="form-control select" name="estado" id="estado">
                                <option value="">Seleccione</option>
                                <option value="1"<?php if($foto->estado == "1") echo "selected";?>>Activo</option>
                                <option value="0"<?php if($foto->estado == "0") echo "selected";?>>Inactivo</option>
                            </select>
                        </div>
                    </div>


                    <div class="btn-group pull-right">
                        <a type="button" class="btn btn-primary" style="margin-right: .5em;"
                           href="<?php echo site_url($this->config->item('path_backend') . '/Promocion/editar/'.$promocion.'/F'); ?>">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="Guardar" name="agregar" id="agregar"/>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT WRAPPER -->
<script type="text/javascript">



    var jvalidate = $("#editar_foto").validate({
        ignore: [],
        rules: {

            estado: {
                required: true
            },
            tipo:{
                required: true
            },
            plataforma:{
                required: true
            }

        }
    });


</script>
