
<!-- PAGE CONTENT WRAPPER -->

<div class="panel panel-default tabs">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab1" data-toggle="tab">Carta</a></li>
        <li><a href="#tab2" data-toggle="tab" id="lista_carta_foto">Foto</a></li>
    </ul>


    <div class="tab-content">
        <div class="tab-pane panel-body active" id="tab1">

                <form action="<?php echo site_url($this->config->item('path_backend').'/Carta/editar/'.$carta->id);?>" id="editar_carta" class="form-horizontal" role="form" name="editar_carta" method="post"
                      enctype="multipart/form-data" >

                    <div class="form-group">
                        <label class="col-md-1 control-label">Categoría</label>
                        <div class="col-md-11">
                            <select class="form-control select" name="categoria" id="categoria" >
                                <option value="">Seleccione</option>
                                <?php foreach ($categorias as $categoria) { ?>
                                    <option value="<?php echo $categoria->id;?>"  <?php if ($carta->id_categoria_carta == $categoria->id) {
                                        echo "selected";
                                    } ?>
                                    >
                                        <?php echo $categoria->nombre; ?>
                                    </option>
                                <?php } ?>

                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Local</label>
                        <div class="col-md-11">
                            <select class="form-control select" name="local" id="local" >
                                <option value="">Seleccione</option>
                                <?php foreach ($local as $local) { ?>
                                    <option value="<?php echo $local->id;?>"  <?php if($carta->id_local== $local->id){ echo "selected";}?>>
                                        <?php echo $local->nombre; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Nombre</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="<?php echo $carta->nombre;?>" name="nombre"  id="nombre" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Precio</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="<?php echo $carta->precio;?>" name="precio"  id="precio" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Descripción</label>
                        <div class="col-md-11">

                            <textarea type="text" class="form-control" name="descripcion" id="descripcion"><?php echo $carta->descripcion;?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Estado</label>
                        <div class="col-md-11">
                            <select class="form-control select" name="estado" id="estado" >
                                <option value="">Seleccione</option>
                                <option value="1" <?php if ($carta->estado == "1") echo "selected"; ?>>Activo</option>
                                <option value="0" <?php if ($carta->estado == "0") echo "selected"; ?>>Inactivo</option>
                            </select>
                        </div>
                    </div>



                    <div class="btn-group pull-right">
                        <a type="button" class="btn btn-primary"  style="margin-right: .5em;" href="<?php echo site_url($this->config->item('path_backend').'/Carta');?>">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="Guardar" name="agregar" id="agregar" />
                    </div>


                </form>
        </div>
        <div class="tab-pane panel-body " id="tab2">
            <div class="page-content-wrap">
                <div class="container-fluid">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="panel-body">


                                <form name="form_busqueda" id="form_busqueda" method="post" action="">
                                    <div class="col-md-12 form-group">

                                        <div class="col-md-4">
                                            <label class="control-label">Tipo</label>
                                            <select class="form-control select" name="tipo_foto" id="tipo_foto">
                                                <option value="">Seleccione</option>
                                                <option value="1" <?php if ($tipo == "1") echo "selected"; ?>>Principal
                                                </option>
                                                <option value="2" <?php if ($tipo == "2") echo "selected"; ?>>
                                                    Detalle
                                                </option>
                                            </select>

                                        </div>

                                        <div class="col-md-4">
                                            <label class="control-label">Plataforma</label>
                                            <select class="form-control select" name="plataforma_foto" id="plataforma_foto">
                                                <option value="">Seleccione</option>
                                                <option value="1" <?php if ($plataforma == "1") echo "selected"; ?>>Web
                                                </option>
                                                <option value="2" <?php if ($plataforma == "2") echo "selected"; ?>>
                                                    Móvil
                                                </option>
                                            </select>

                                        </div>


                                        <div class="col-md-4">
                                            <label class="control-label">Estado</label>
                                            <select class="form-control select" name="estado_foto" id="estado_foto">
                                                <option value="">Seleccione</option>
                                                <option value="1" <?php if ($estado == "1") echo "selected"; ?>>Activo
                                                </option>
                                                <option value="0" <?php if ($estado == "0") echo "selected"; ?>>
                                                    Inactivo
                                                </option>
                                            </select>

                                        </div>
                                        <input type="hidden" name="buscar" id="buscar"
                                               value=""/>

                                    </div>
                                    <div class="col-md-12 form-group">
                                        <div class="col-md-4">

                                        </div>
                                        <div class="col-md-8">
                                            <!--  <input type="submit" class="btn btn-primary pull-right" value="Buscar"
                                                    id="buscar_foto"/>-->
                                            <a href="#" class="btn btn-primary pull-right" id="buscar_carta_foto">Buscar</a>
                                        </div>

                                    </div>

                                </form>


                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Fotos</h3>
                        <a   id="export_foto_carta" class="btn btn-danger dropdown-toggle pull-right"  style="margin-left: 5px;" ><img src="<?php echo base_url('assets/backend/');?>/img/icons/xls.png" width="16"/> Exportar</a>

                        <a href="<?php echo site_url($this->config->item('path_backend') . '/Foto_Carta/agregar/' . $carta->id); ?>"
                           class="btn btn-default pull-right"><i class="fa fa-plus"></i> Agregar</a>

                    </div>
                    <div id="tabla_carta_foto"></div>

                </div>


            </div>
        </div>
       
    </div>
    </div>
<!-- END PAGE CONTENT WRAPPER -->
<script type="text/javascript">



    var jvalidate = $("#editar_carta").validate({
        ignore: [],
        rules: {
            categoria: {
                required: true
            },

            nombre: {
                required: true
            },

            precio: {
                required: true

            },

            estado: {
                required: true
            }
        }
    });
    $(document).ready(function(){

        $('#precio').keypress(validateNumberDouble);

    });



    function validateNumberDouble(event) {
        var code = (event.which) ? event.which : event.keyCode;
        if(code==8 || code ==45 || code == 46)
        {
            //backspace
            return true;
        }
        else if(code>=48 && code<=57)
        {
            //is a number
            return true;
        }
        else
        {
            return false;
        }
    }


    $("#lista_carta_foto").click(function () {

        $.post("<?php echo site_url('backend/Foto_carta/listar');?>",{id_carta : <?php echo $carta->id;?>},
            function (result) {

                $('#tabla_carta_foto').html(result);


            }, "html");

        return true;
    });

    $("#buscar_carta_foto").click(function () {


        $.post("<?php echo site_url('backend/Foto_carta/buscar');?>",
            {id_carta : <?php echo $carta->id;?>,tipo : $("#tipo_foto").val(),estado : $("#estado_foto").val(),plataforma:$("#plataforma_foto").val()},
            function (result) {
                $('#tabla_carta_foto').html(result);

            }, "html");

        return true;
    });

    <?php
    switch($this->uri->segment(5)){
    case 'F' :
    ?>
    $(document).ready(function () {
        $('#lista_carta_foto').trigger('click');
    });

    <?php
    break;
    }
    ?>

    $('#export_foto_carta').click(function(){
        // e.preventDefault();

        var url = "<?php echo site_url($this->config->item('path_backend').'/Foto_Carta/export');?>";
        var id_carta = <?php echo $carta->id;?>;
        var tipo=$("#tipo_foto").val();
        var estado=$("#estado_foto").val();
        var plataforma= $("#plataforma_foto").val();

        window.location.href= url+"?id_carta="+id_carta+"&tipo="+tipo+"&estado="+estado+"&plataforma="+plataforma;


    });


</script>
