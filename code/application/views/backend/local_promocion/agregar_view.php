<!-- PAGE CONTENT WRAPPER -->

<div class="page-content-wrap">

    <div class="container-fluid">
        <div class="col-md-12 panel-body">
            <div class="block" style="background-color:#EEEEEE;margin-bottom: 0px;padding-bottom:20px;">
                <form
                    action="<?php echo site_url($this->config->item('path_backend') . '/Local_Promocion/agregar/'.$promocion); ?>"
                    id="agregar_local" class="form-horizontal" role="form" name="agregar_local" method="post"
                    enctype="multipart/form-data" >

                    <div class="form-group">
                        <label class="col-md-1 control-label">Local</label>
                        <div class="col-md-11">
                            <select class="form-control select" name="local" id="local" >
                                <option value="">Seleccione</option>
                                <?php foreach ($local as $local) { ?>
                                    <option value="<?php echo $local->id;?>">
                                        <?php echo $local->nombre; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="btn-group pull-right">
                        <a type="button" class="btn btn-primary" style="margin-right: .5em;"
                           href="<?php echo site_url($this->config->item('path_backend') . '/Promocion/editar/'.$promocion.'/L'); ?>">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="Guardar" name="agregar" id="agregar"/>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT WRAPPER -->
<script type="text/javascript">

    /* jQuery.validator.messages.required = 'Por favor completar este campo.';

     $("#agregar_categoria").validate({
     highlight: function (element) {
     $(element).closest('.form-group').addClass('has-error');
     },
     unhighlight: function (element) {
     $(element).closest('.form-group').removeClass('has-error');
     },
     errorElement: 'span',
     errorClass: 'help-block',
     errorPlacement: function (error, element) {
     if (element.parent('.input-group').length) {
     error.insertAfter(element.parent());
     } else {
     error.insertAfter(element);
     }


     }
     });*/


    var jvalidate = $("#agregar_local").validate({
        ignore: [],
        rules: {
            local: {
                required: true,
            },
        }
    });



</script>
