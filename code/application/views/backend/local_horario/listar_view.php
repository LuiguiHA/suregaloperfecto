<!-- PAGE CONTENT WRAPPER -->


<div class="panel-body">
    <table class="table table-hover table-cms">
        <thead>
        <tr>
            <td colspan="7" align="center" valign="top"></td>
        </tr>
        <tr>
            <th>Dia</th>
            <th>Hora Inicio</th>
            <th>Hora Fin </th>
            <th>Stock</th>
            <th>Fecha Registro</th>
            <th>Estado</th>
            <th>Acciones</th>
        </tr>
        </thead>
        <tbody>


        <?php


        if (isset($local_horario) && count($local_horario) > 0)
        {

            foreach ($local_horario as $horario)
            {
                ?>
                <tr>

                    <td><?php
                        $day_week = unserialize (DAY_WEEK);

                        for ($i=0 ; $i<sizeof($day_week) ; $i++ ){
                            if ($horario->dia == $day_week[$i]["id"]){
                                $dia = $day_week[$i]["dia"];
                            }
                        }
                        echo $dia;
                        ?>
                    </td>
                    <td><?php
                        $date = new DateTime($horario->hora_inicio);
                        echo $date->format('h:i a ');
                        ?>
                    </td>
                    <td><?php
                        $date = new DateTime($horario->hora_fin);
                        echo $date->format('h:i a ');
                        ?>
                    </td>
                    <td><?php echo $horario->stock; ?></td>
                    <td><?php echo date('Y-m-d g:i a', strtotime($horario->fecha_registro)); ?></td>
                    <td>
                        <?php
                        if ($horario->estado == 1) echo '<span class="label label-success label-form" style="width:70px; font-weight:bolder;">Activo</span>'; else
                            echo '<span class="label label-danger label-form" style="width:70px; font-weight:bolder;">Inactivo</span>'
                        ?>
                    </td>
                    <td>

                        <a href="<?php echo site_url($this->config->item('path_backend') . '/Local_horario/editar_view/' .$empresa.'/'. $local . '/' . $horario->id); ?>"
                           class="btn btn-info btn-condensed" title="Editar"><i class="fa fa-edit"></i></a>
                    </td>

                </tr>
                <?php
            }

        }
        ?>

        </tbody>
    </table>
    <?php

    if (isset($paginador)) echo $paginador;
    ?>
</div>


<script>

    $(document).on("click", "#paginador ul li a", function (e) {
        e.preventDefault();
        var href = $(this).attr("href");

        // alert(href);
        $.post(href, {
                id_local: <?php echo $local;?> ,
                id_empresa: <?php echo $empresa;?> ,
                estado: $("#estado_local_horario").val()
            },
            function (result) {

                $('#tabla_local_horario').html(result);


            }, "html");

        return true;
    });

</script>
<!-- END PAGE CONTENT WRAPPER -->