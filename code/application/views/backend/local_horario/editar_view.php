<!-- PAGE CONTENT WRAPPER -->

<div class="page-content-wrap">

    <div class="container-fluid">
        <div class="col-md-12 panel-body">
            <div class="block" style="background-color:#EEEEEE;margin-bottom: 0px;padding-bottom:20px;">
                <form
                    action="<?php echo site_url($this->config->item('path_backend') . '/Local_horario/editar/'.$empresa.'/'.$local.'/'.$local_horario->id); ?>"
                    id="editar_local_horario" class="form-horizontal" role="form" name="editar_local_horario" method="post"
                    enctype="multipart/form-data" >

                    <div class="form-group">
                        <label class="col-md-1 control-label">Dia</label>
                        <div class="col-md-11">
                            <select class="form-control select" name="dia" id="dia">
                                <option value="">Seleccione</option>
                                <?php
                                $day_week = unserialize(DAY_WEEK);
                                foreach ($day_week as $day)
                                { ?>
                                    <option value="<?php echo $day["id"]; ?>" <?php if($local_horario->dia == $day["id"]) echo "selected";?>>
                                        <?php echo $day["dia"]; ?>
                                    </option>
                                <?php } ?>

                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Stock</label>
                        <div class="col-md-11">

                            <input type="text" class="form-control spinner_default" value="<?php echo $local_horario->stock ; ?>" id="stock" name="stock"  min="1"  readonly/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Hora Inicio</label>
                        <div class="col-md-11">
                            <div class="input-group bootstrap-timepicker">
                                <input type="text" class="form-control" value="<?php
                                $date = new DateTime($local_horario->hora_inicio);
                                echo $date->format('h:i a ');
                                  ?>" name="hora_inicio"
                                       id="hora_inicio" readonly/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Hora Fin</label>
                        <div class="col-md-11">
                            <div class="input-group bootstrap-timepicker">
                                <input type="text" class="form-control" value="<?php

                                $date = new DateTime($local_horario->hora_fin);
                                echo $date->format('h:i a ');

                                ?>" name="hora_fin"
                                       id="hora_fin" readonly/>
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-1 control-label">Estado</label>
                        <div class="col-md-11">
                            <select class="form-control select" name="estado" id="estado">
                                <option value="">Seleccione</option>
                                <option value="1"  <?php if($local_horario->estado == "1") echo "selected";?>>Activo</option>
                                <option value="0"  <?php if($local_horario->estado == "0") echo "selected";?>>Inactivo</option>
                            </select>
                        </div>
                    </div>


                    <div class="btn-group pull-right">
                        <a type="button" class="btn btn-primary" style="margin-right: .5em;"
                           href="<?php echo site_url($this->config->item('path_backend').'/Local_Empresa/editar/'.$empresa.'/'.$local.'/H'); ?>">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="Guardar" name="agregar" id="agregar"/>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT WRAPPER -->
<script type="text/javascript">

    $(function () {
        $(".spinner_default").spinner()
    });

    var validate_hour = true;


    var jvalidate = $("#editar_local_horario").validate({
        ignore: [],
        rules: {

            dia: {
                required: true,
            },

            stock: {
                required: true,
            },
            hora_inicio: {

                required: true,
            },
            hora_fin: {
                required: true,
            },

            estado: {
                required: true
            }
        }
    });

    $(document).ready(function () {


        $('#stock').keypress(validateNumber);


    });

    function validateNumber(event) {
        var key = window.event ? event.keyCode : event.which;

        if (event.keyCode === 8 || event.keyCode === 46
            || event.keyCode === 37 || event.keyCode === 39) {
            return true;
        }
        else if (key < 48 || key > 57) {
            return false;
        }
        else return true;
    }

    function converthour(hora_12, type) {
        var time = hora_12;
        var hours = Number(time.match(/^(\d+)/)[1]);
        var minutes = Number(time.match(/:(\d+)/)[1]);
        var AMPM = time.match(/\s(.*)$/)[1];
        if (AMPM == "PM" && hours < 12) hours = hours + 12;
        if (AMPM == "AM" && hours == 12) hours = hours - 12;
        var sHours = hours.toString();
        var sMinutes = minutes.toString();
        if (hours < 10) sHours = "0" + sHours;
        if (minutes < 10) sMinutes = "0" + sMinutes;

        if (type == 1) {
            return sHours + ":" + sMinutes + ":" + "00";
        }else if(type == 2){
            return sMinutes;
        }
        else {
            return sHours;
        }
    }

    $("#editar_local_horario").submit(function () {

        var hora_inicio_ini = $("#hora_inicio").val();
        var hora_fin_ini = $("#hora_fin").val();

        var  hora_inicio = converthour(hora_inicio_ini, 0);
        var   hora_fin = converthour(hora_fin_ini, 0);

        var  min_inicio = converthour(hora_inicio_ini,2);
        var min_fin = converthour(hora_fin_ini, 2);

        if (hora_inicio < hora_fin) {
            // check_hour();
            return true;
        } else if (hora_inicio == hora_fin) {
            if (min_inicio == min_fin) {
                error = "La hora de inicio es igual a la hora fin, cambie los horarios ";
                alert(error);
                return false;
            } else {
                return true;
            }
        } else {
            error = "La hora de inicio es mayor a la hora fin, cambie los horarios  ";
            alert(error);
            return false;
        }

    });

    $("#editar_local_horario").submit(function () {

        // function check_hour() {


        var hora_inicio = $("#hora_inicio").val();
        var hora_fin = $("#hora_fin").val();

        hora_inicio = converthour(hora_inicio, 1);
        hora_fin = converthour(hora_fin, 1);


        $.ajax({
            type: 'POST',
            dataType: "json",
            async: false,
            url: "<?php echo site_url('backend/Local_horario/check_hour');?>",
            data:  {
                id_horario: <?php echo $local_horario->id; ?>,
                id_local: <?php echo $local; ?>,
                id_dia: $("#dia").val(),
                hora_inicio: hora_inicio,
                hora_fin: hora_fin,
                estado: $("#estado").val()
            },
            success:
                function(result) {
                    switch (result.cod) {
                        case 0:
                            fn_check_hour(result.cod);
                            break;
                        case 1:
                            fn_check_hour(result.cod);
                            break;
                    }
                }
        });

        //  }
        console.log(validate_hour);
        return validate_hour;
    });

    function fn_check_hour(check) {

        if (check == 0) {
            validate_hour = true;
        } else {
            error = "El horario ingresado se cruzan con otros";
            alert(error);
            console.log("error");
            validate_hour = false;
        }

    }

    $("#hora_inicio").timepicker({
        interval: 30,
    });

    $("#hora_fin").timepicker({
        interval: 30,
    });


</script>

