
<!-- PAGE CONTENT WRAPPER -->

<div class="page-content-wrap">

    <div class="container-fluid">
        <div class="col-md-12 panel-body">
            <div class="block" style="background-color:#EEEEEE;margin-bottom: 0px;padding-bottom:20px;">
                <form action="<?php echo site_url($this->config->item('path_backend').'/Formula_Puntos/agregar');?>" id="agregar_formula" class="form-horizontal" role="form" name="agregar_formula" method="post"
                      enctype="multipart/form-data" >


                    <div class="form-group">
                        <label class="col-md-1 control-label">Nombre</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="" name="nombre"  id="nombre" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Monto</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="" name="monto"  id="monto" />
                        </div>
                    </div>



                    <div class="form-group">
                        <label class="col-md-1 control-label">Punto</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="" name="punto" id="punto" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Estado</label>
                        <div class="col-md-11">
                            <select class="form-control select" name="estado" id="estado" >
                                <option value="">Seleccione</option>
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                        </div>
                    </div>



                    <div class="btn-group pull-right">
                        <a type="button" class="btn btn-primary"  style="margin-right: .5em;" href="<?php echo site_url($this->config->item('path_backend').'/Formula_Puntos');?>">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="Guardar" name="agregar" id="agregar" />
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT WRAPPER -->
<script type="text/javascript">
    $.validator.addMethod('decimal', function(value, element) {
        return this.optional(element) || /^\d+(\.\d{0,2})?$/.test(value);
    }, "Ingrese solo con dos decimales : 1233123.12");
    
   var jvalidate = $("#agregar_formula").validate({
       ignore: [],
       rules: {
           nombre: {
               required: true
           },
          monto: {
               required: true,
              decimal:true
           },
           punto: {
               required: true

           },
           estado: {
               required: true
           }
       }
   });
   $(document).ready(function(){


       $('#monto').keypress(validateNumberDouble);
       $('#punto').keypress(validateNumber);
        
   });

   function validateNumber(event) {
       var key = window.event ? event.keyCode : event.which;

       if (event.keyCode === 8 || event.keyCode === 46
           || event.keyCode === 37 || event.keyCode === 39) {
           return true;
       }
       else if ( key < 48 || key > 57 ) {
           return false;
       }
       else return true;
   }

   function validateNumberDouble(event) {
       var code = (event.which) ? event.which : event.keyCode;
       if(code==8 || code ==45 || code == 46)
       {
           return true;
       }
       else if(code>=48 && code<=57)
       {
           return true;
       }
       else
       {
           return false;
       }
   }





</script>
