<!-- PAGE CONTENT WRAPPER -->



        
        <div class="panel-body" >
            <table class="table table-hover table-cms" >
                <thead>
                <tr>
                    <td colspan="6" align="center" valign="top"></td>
                </tr>
                <tr>
                    <th>Campo</th>
                    <th>Valor</th>
                    <th>Orden</th>
                    <th>Fecha de Registro</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>


                <?php
                if (isset($info_beneficio) && count($info_beneficio) > 0) {

                    foreach ($info_beneficio as $info) {
                        ?>
                        <tr>

                            <td><?php echo $info->campo; ?></td>

                            <td><?php echo $info->valor; ?></td>
                            <td><?php echo $info->orden; ?></td>
                            <td><?php echo date('Y-m-d g:i a', strtotime($info->fecha_registro)); ?></td>
                            <td>
                                <?php
                                if ($info->estado == 1)
                                    echo '<span class="label label-success label-form" style="width:70px; font-weight:bolder;">Activo</span>';
                                else
                                    echo '<span class="label label-danger label-form" style="width:70px; font-weight:bolder;">Inactivo</span>'
                                ?>
                            </td>
                            <td>
                                <div style="width: 100px">
                                    <a href="<?php echo site_url($this->config->item('path_backend') . '/Info_Adicional_Beneficio/editar/'.$beneficio.'/'.$info->id); ?>"
                                       class="btn btn-info btn-condensed" title="Editar"><i class="fa fa-edit"></i></a>

                                    <a onclick="confirmarEliminar(<?php echo $info->id?>,<?php echo $info->id_beneficio?>)" class="btn btn-danger btn-condensed" title="Eliminar"><i class="fa fa-trash-o"></i></a>

                                </div>

                            </td>

                        </tr>
                        <?php
                    }

                }
                ?>

                </tbody>
            </table>
            <?php
            
            if (isset($paginador))
                echo $paginador;
            ?>
        </div>



<div class="message-box message-box-danger animated fadeIn" id="message-box-default">
    <div class="mb-container" id="content">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-trash-o"></span>Eliminar</div>
            <div class="mb-content">
                <p>¿Esta seguro de eliminar esta información adicional?</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close" id="closeDialog" style="margin-left: 10px">No</button>
                <button class="btn btn-default btn-lg pull-right mb-control-close" id="delete">Si</button>
            </div>
        </div>
    </div>
    <div id="loader_content" class="loader" style="visibility: hidden;display: none"></div>
</div>


<script>
    function confirmarEliminar(id_info_add,id_beneficio) {
        $('#message-box-default').show();

        $('#closeDialog').click(function () {
            $('#message-box-default').hide();
        });

        $('#delete').click(function () {
            eliminarInformacion(id_info_add,id_beneficio);
        });
    }

    function eliminarInformacion(id_info_add,id_beneficio) {
        $('#content').hide();
        show_loader();
        $.ajax({
            url: "<?php echo site_url($this->config->item('path_backend') . '/Info_adicional_beneficio/eliminar');?>",
            type: "POST",
            data : {id_info_add : id_info_add},
            dataType: 'json',
            success: function (response) {
                var message = "";
                var type = "";
                if(response.cod == "1"){
                    message = response.mensaje;
                    type =  "success";
                }else{
                    message = response.mensaje;
                    type =  "warning";
                }
                $('#message-box-default').hide();
                hidden_loader();
                show_message(type,message,id_beneficio)
            },
            error: function (xhr, status) {
                $('#message-box-default').hide();
                hidden_loader();
                $('#content').show();
                var text = "No se ha podido conectar con el servidor";
                show_message('warning',text,"")
            }
        });

    }


    function show_message(type,message,id_beneficio) {

        swal({
            title: "",
            text: message,
            type: type,
            showCancelButton: false,
            confirmButtonColor: '#21A053',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Aceptar',
            allowOutsideClick: false,
        }).then(function () {
            if (type == "success"){
                var segments = $(location).attr('href').split('/',9);
                window.location.href = segments.join('/') + "/I";
            }
        })
    }

    $(document).on("click", "#paginador ul li a", function(e){
        e.preventDefault();
        var href = $(this).attr("href");

        // alert(href);
        $.post(href, {id_beneficio : <?php echo $beneficio;?> , campo : $("#campo_info").val() , valor : $("#valor_info").val()
                , estado : $("#estado_info").val()},
            function (result) {

                $('#tabla_beneficio_info').html(result);


            }, "html");

        return true;
    });

    function show_loader() {
        $('#loader_content').css({"visibility":"visible"});
        $('#loader_content').css({"display":"block"});
    }

    function hidden_loader() {
        $('#loader_content').css({"visibility":"hidden"});
        $('#loader_content').css({"display":"none"});
    }
   
</script>
<!-- END PAGE CONTENT WRAPPER -->