<!-- PAGE CONTENT WRAPPER -->

<div class="page-content-wrap">

    <div class="container-fluid">
        <div class="col-md-12 panel-body">
            <div class="block" style="background-color:#EEEEEE;margin-bottom: 0px;padding-bottom:20px;">
                <form
                    action="<?php echo site_url($this->config->item('path_backend') . '/Info_Adicional_Beneficio/editar/'.$beneficio.'/'.$info->id); ?>"
                    id="editar_info" class="form-horizontal" role="form" name="editar_info" method="post"
                    enctype="multipart/form-data" >

                    <div class="form-group">
                        <label class="col-md-1 control-label">Campo</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="<?php echo $info->campo;?>" name="campo" id="campo" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Valor</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="<?php echo $info->valor;?>" name="valor" id="valor" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Orden</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="<?php echo $info->orden;?>" name="orden" id="orden" maxlength="1"/>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-1 control-label">Estado</label>
                        <div class="col-md-11">
                            <select class="form-control select" name="estado" id="estado">
                                <option value="">Seleccione</option>
                                <option value="1" <?php if($info->estado == "1") echo "selected";?>>Activo</option>
                                <option value="0" <?php if($info->estado == "0") echo "selected";?>>Inactivo</option>
                            </select>
                        </div>
                    </div>


                    <div class="btn-group pull-right">
                        <a type="button" class="btn btn-primary" style="margin-right: .5em;"
                           href="<?php echo site_url($this->config->item('path_backend') .'/Beneficio/editar/'.$beneficio.'/I'); ?>">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="Guardar" name="agregar" id="agregar"/>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT WRAPPER -->
<script type="text/javascript">

    /* jQuery.validator.messages.required = 'Por favor completar este campo.';

     $("#agregar_categoria").validate({
     highlight: function (element) {
     $(element).closest('.form-group').addClass('has-error');
     },
     unhighlight: function (element) {
     $(element).closest('.form-group').removeClass('has-error');
     },
     errorElement: 'span',
     errorClass: 'help-block',
     errorPlacement: function (error, element) {
     if (element.parent('.input-group').length) {
     error.insertAfter(element.parent());
     } else {
     error.insertAfter(element);
     }


     }
     });*/


    var jvalidate = $("#agregar_info").validate({
        ignore: [],
        rules: {
            campo: {
                required: true,
            },
            valor: {
                required: true,
            },

            estado: {
                required: true
            }
        }
    });

    $(document).ready(function(){

        $('#orden').keypress(validateNumber);


    });

    function validateNumber(event) {
        var key = window.event ? event.keyCode : event.which;

        if (event.keyCode === 8 || event.keyCode === 46
            || event.keyCode === 37 || event.keyCode === 39) {
            return true;
        }
        else if ( key < 48 || key > 57 ) {
            return false;
        }
        else return true;
    }



</script>
