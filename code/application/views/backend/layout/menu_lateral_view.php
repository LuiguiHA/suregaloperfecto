<div class="page-sidebar">
    <!-- START X-NAVIGATION -->
    <ul class="x-navigation">
        <?php if(isset($mostrarLogoMenuLateral) && $mostrarLogoMenuLateral === TRUE): ?>
        <li class="xn-logo" >

            <a href="<?php echo site_url($this->config->item('path_backend') . '/Beneficio'); ?>" style="background-color: #21a053;!important;"></a>
            <a href="#" class="x-navigation-control" style="background-color: #21a053;!important;"></a>
        </li>
        <?php endif; ?>
        
        <?php 
        if(isset($mostrarPerfilMenuLateral) && $mostrarPerfilMenuLateral === TRUE):
        ?>
        <li class="xn-profile">
            <a href="#" class="profile-mini">
                <img src="assets/images/users/avatar.jpg" alt="John Doe"/>
            </a>            
            <div class="profile">
                <div class="profile-image">
                    <img src="assets/images/users/avatar.jpg" alt="John Doe"/>
                </div>
                <div class="profile-data">
                    <div class="profile-data-name">John Doe</div>
                    <div class="profile-data-title">Web Developer/Designer</div>
                </div>
                <div class="profile-controls">
                    <a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a>
                    <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                </div>
            </div>                                                                        
        </li>
        <?php 
        endif;
        ?>
        
        <?php if(isset($mostrarTituloMenuLateral) && $mostrarTituloMenuLateral === TRUE): ?>
        <li class="xn-title">Navigation</li>
        <?php endif ?>


        
        <li  <?php
        if($this->uri->segment(2)== "inicio" || $this->uri->segment(2)== "Twitter")
           echo  'class="xn-openable active"';
        else
            echo  'class="xn-openable"';
        ?>>
            <a href="#"><span  class="fa fa-suitcase"></span> <span class="xn-text">Twitter</span></a>
            <ul>
                <li <?php
                if( ($this->uri->segment(3)== "" || $this->uri->segment(3)== "buscar" || $this->uri->segment(3)== "listar_beneficio" || $this->uri->segment(3)== "buscar_beneficio")
                    && ($this->uri->segment(2)== "inicio" || $this->uri->segment(2)== "Twitter")  )
                    echo  'class="active"';
                ?> ><a href="<?php echo site_url($this->config->item('path_backend') . '/Twitter'); ?>"><span class="fa fa-list"></span> Listar</a></li>
                <li <?php
                if($this->uri->segment(3)== "agregar" &&  ($this->uri->segment(2)== "inicio" || $this->uri->segment(2)== "Twitter") )
                    echo  'class="active"';

                ?>><a href="<?php echo site_url($this->config->item('path_backend') . '/Twitter/agregar'); ?>"><span class="fa fa-plus"></span> Agregar</a></li>

            </ul>
        </li>


        
    </ul>
    <!-- END X-NAVIGATION -->
</div>