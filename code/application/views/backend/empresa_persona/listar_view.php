<!-- PAGE CONTENT WRAPPER -->



        
        <div class="panel-body" >
            <table class="table table-hover" >
                <thead>
                <tr>
                    <td colspan="4" align="center" valign="top"></td>
                </tr>
                <tr>
                    <th>Nombre</th>

                    <th>Correo</th>
                    <th>Puntos Acumulados</th>
                    <th>Fecha de Registro</th>
                </tr>
                </thead>
                <tbody>


                <?php
                     

                if (isset($empresa_persona) && count($empresa_persona) > 0) {

                    foreach ($empresa_persona as $empresa) {
                        ?>
                        <tr>

                            <td><?php echo $empresa->nombre; ?></td>
                            <td><?php echo $empresa->correo; ?></td>
                            <td><?php echo $empresa->punto_acumulado; ?></td>
                            <td><?php echo date('Y-m-d g:i a', strtotime($empresa->fecha_registro)); ?></td>
                            

                        </tr>
                        <?php
                    }

                }
                ?>

                </tbody>
            </table>
            <?php
            
            if (isset($paginador))
                echo $paginador;
            ?>
        </div>
<script>

    $(document).on("click", "#paginador ul li a", function(e){
        e.preventDefault();
        var href = $(this).attr("href");

        // alert(href);
        $.post(href, {id_usuario : <?php echo $usuario;?> , nombre : $("#nombre_empresa").val()},
            function (result) {

                $('#tabla_usuario_empresa').html(result);


            }, "html");

        return true;
    });

</script>



<!-- END PAGE CONTENT WRAPPER -->