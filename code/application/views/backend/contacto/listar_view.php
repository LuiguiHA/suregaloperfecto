<!-- PAGE CONTENT WRAPPER -->



        
        <div class="panel-body" >
            <table class="table table-hover table-cms" >
                <thead>
                <tr>
                    <td colspan="6" align="center" valign="top"></td>
                </tr>
                <tr>
                    <th>DNI</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Sexo</th>
                    <th>Email</th>
                    <th>Fecha de registro</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>


                <?php
                     

                if (isset($contacto) && count($contacto) > 0) {

                    foreach ($contacto as $contacto) {
                        ?>
                        <tr>
                        
                            <td><?php echo $contacto->dni; ?></td>
                            <td><?php echo $contacto->nombre; ?></td>
                            <td><?php echo $contacto->apellido; ?></td>
                            <td>
                              <?php 
                              if ( strtolower($contacto->sexo)=="m")
                                echo "Masculino";
                              else
                                echo "Femenino";
                              ?>
                                
                            </td>
                            <td><?php echo $contacto->correo; ?></td>
                            <td><?php echo date('Y-m-d g:i a', strtotime($contacto->fecha_registro)); ?></td>
                            <td>
                                <?php
                                if ($contacto->estado == 1)
                                     echo '<span class="label label-success label-form" style="width:70px; font-weight:bolder;">Activo</span>';
                                else
                                    echo '<span class="label label-danger label-form" style="width:70px; font-weight:bolder;">Inactivo</span>';
                                ?>
                            </td>
                            <td>

                                <a href="<?php echo site_url($this->config->item('path_backend') . '/Contacto/editar/'.$empresa.'/'.$contacto->id); ?>"
                                   class="btn btn-info btn-condensed" title="Editar"><i class="fa fa-edit"></i></a>
                            </td>

                        </tr>
                        <?php
                    }

                }
                ?>

                </tbody>
            </table>
            <?php
            
            if (isset($paginador))
            echo $paginador;
            ?>
        </div>



<script>
    
       $(document).on("click", "#paginador ul li a", function(e){
e.preventDefault();
var href = $(this).attr("href");

// alert(href);
$.post(href,   {id_empresa : <?php echo $empresa;?> , nombre :$("#nombre_contacto").val() ,
apellido : $("#apellido_contacto").val(),dni : $("#dni_contacto").val(),estado : $("#estado_contacto").val()},
function (result) {

$('#tabla_empresa_contacto').html(result);


}, "html");

return true;
});

</script>
<!-- END PAGE CONTENT WRAPPER -->