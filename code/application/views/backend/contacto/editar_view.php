<!-- PAGE CONTENT WRAPPER -->

<div class="page-content-wrap">

    <div class="container-fluid">
        <div class="col-md-12 panel-body">
            <div class="block" style="background-color:#EEEEEE;margin-bottom: 0px;padding-bottom:20px;">
                <form
                    action="<?php echo site_url($this->config->item('path_backend') . '/Contacto/editar/'.$empresa.'/'.$contacto->id); ?>"
                    id="editar_contacto" class="form-horizontal" role="form" name="editar_contacto" method="post"
                    enctype="multipart/form-data" >



                    <div class="form-group">
                        <label class="col-md-1 control-label">DNI</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="<?php echo $contacto->dni;?>" name="dni" id="dni" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Nombre</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="<?php echo $contacto->nombre;?>" name="nombre" id="nombre" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Apellido</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="<?php echo $contacto->apellido;?>" name="apellido" id="apellido" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Sexo</label>
                        <div class="col-md-11">
                            <select class="form-control select" name="sexo" id="sexo">
                                <option value="">Seleccione</option>
                                <option value="M" <?php if (strtolower($contacto->sexo) == "m") {echo "selected";} ?>>Masculino</option>
                                <option value="F"<?php if (strtolower($contacto->sexo) == "f") {echo "selected";} ?>>Femenino</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Fecha de Nacimiento</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="<?php echo date('Y-m-d', strtotime($contacto->fecha_nacimiento)); ?>" name="fecha_nacimiento" id="fecha_nacimiento"  />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Email</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="<?php echo $contacto->correo;?>" name="email" id="email" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Roles</label>
                        <div class="col-md-11">


                            <select class="form-control select" name="roles[]" id="roles"  multiple  required title="Seleccione">
                                <option value="dashboard" <?php   $roles=json_decode($contacto->perfil);for ($i=0;$i<sizeof($roles);$i++){if($roles[$i]->dashboard) echo "selected";}?>>Dashboard</option>
                                <option value="clientes" <?php   $roles=json_decode($contacto->perfil);for ($i=0;$i<sizeof($roles);$i++){if($roles[$i]->clientes) echo "selected";}?>>Clientes</option>
                                <option value="reportes" <?php   $roles=json_decode($contacto->perfil);for ($i=0;$i<sizeof($roles);$i++){if($roles[$i]->reportes) echo "selected";}?>>Reportes</option>
                                <option value="consultas" <?php   $roles=json_decode($contacto->perfil);for ($i=0;$i<sizeof($roles);$i++){if($roles[$i]->consultas) echo "selected";}?>>Consultas</option>
                                *
                            </select>



                            <div id="opciones-roles" style="margin-top: 1px;">

                                <div class="form-group hidden" id="rol-contacto">
                                    <label class="col-md-2 control-label">Consultas</label>
                                    <div class="col-md-10">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <div class="col-md-4">
                                                        <?php   $roles=json_decode($contacto->perfil);
                                                        for ($i=0;$i<sizeof($roles);$i++){
                                                            if($roles[$i]->consultas){
                                                                $tipos=$roles[$i]->consultas[0];
                                                                $pos = strpos($tipos, ",");
                                                                if ($pos === true) {
                                                                    $consultas = explode(",", $tipos);

                                                                }
                                                            }
                                                        }
                                                        ?>
                                                        <label class="check"><input type="checkbox" id="promociones" name="promociones"
                                                                                    class="icheckbox"   /> Promociones</label>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="check"><input type="checkbox" id="canje" name="canje"
                                                                                    class="icheckbox"/>
                                                            Canje</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Estado</label>
                        <div class="col-md-11">
                            <select class="form-control select" name="estado" id="estado">
                                <option value="">Seleccione</option>
                                <option value="1" <?php if ($contacto->estado == "1") {echo "selected";} ?>>Activo</option>
                                <option value="0" <?php if ($contacto->estado == "0") {echo "selected";} ?>>Inactivo</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Generar Contraseña</label>
                        <div class="col-md-4">
                            <input type="checkbox" class="icheckbox" name="contraseña" id="contraseña" value="1"/>
                        </div>
                    </div>


                    <div class="btn-group pull-right">
                        <a type="button" class="btn btn-primary" style="margin-right: .5em;"
                           href="<?php echo site_url($this->config->item('path_backend').'/Empresa/editar/'.$empresa.'/C'); ?>">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="Guardar" name="agregar" id="agregar"/>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT WRAPPER -->
<script type="text/javascript">



    var jvalidate = $("#editar_contacto").validate({
        ignore: [],
        rules: {
            dni: {
                required: true,

            },
            nombre: {
                required: true,
            },
            apellido: {
                required: true,
            },
            sexo: {
                required: true,
            },
            email: {
                required: true,
                email:true,
            },

            estado: {
                required: true
            }
        }


    });

    $(document).ready(function(){
        $('#dni').keypress(validateNumber);


    });

    function validateNumber(event) {
        var key = window.event ? event.keyCode : event.which;

        if (event.keyCode === 8 || event.keyCode === 46
            || event.keyCode === 37 || event.keyCode === 39) {
            return true;
        }
        else if ( key < 48 || key > 57 ) {
            return false;
        }
        else return true;
    }


    $("#editar_contacto").change(function(){

        var rol = $('#roles').val();

        if (rol == "consultas") {
            document.getElementById("opciones-roles").setAttribute('style', 'visibility:visible');
            $("#rol-contacto").attr("class", "form-group");

        }


    });


    $("#fecha_nacimiento").datetimepicker({
        format: 'YYYY-MM-DD',
        locale: 'es'
    });




</script>
