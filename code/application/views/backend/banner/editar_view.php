<!-- PAGE CONTENT WRAPPER -->

<div class="page-content-wrap">

    <div class="container-fluid">
        <div class="col-md-12 panel-body">
            <div class="block" style="background-color:#EEEEEE;margin-bottom: 0px;padding-bottom:20px;">
                <form
                    action="<?php echo site_url($this->config->item('path_backend') . '/Banner/editar/'.$empresa.'/'.$banner->id); ?>"
                    id="editar_banner" class="form-horizontal" role="form" name="editar_banner" method="post"
                    enctype="multipart/form-data" >

                    <div class="form-group">
                        <label class="col-md-1 control-label">Recurso</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="<?php echo $banner->recurso;?>" name="recurso" id="recurso" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Nombre</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="<?php echo $banner->nombre;?>" name="nombre" id="nombre" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Descripción</label>
                        <div class="col-md-11">

                            <textarea type="text" class="form-control" value="" name="descripcion" id="descripcion"><?php echo $banner->descripcion;?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Enlace</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="<?php echo $banner->enlace;?>" name="enlace" id="enlace" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Target</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="<?php echo $banner->target;?>" name="target" id="target" />
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-1 control-label">Tipo</label>
                        <div class="col-md-11">
                            <select class="form-control select" name="tipo" id="tipo">
                                <option value="">Seleccione</option>
                                <option value="1" <?php if ($banner->tipo == 1) {echo "selected";} ?>>Principal</option>
                                <option value="2" <?php if ($banner->tipo == 2) {echo "selected";} ?>>Detalle</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Estado</label>
                        <div class="col-md-11">
                            <select class="form-control select" name="estado" id="estado">
                                <option value="">Seleccione</option>
                                <option value="1" <?php if ($banner->estado == "1") {echo "selected";} ?>>Activo</option>
                                <option value="0" <?php if ($banner->estado == "0") {echo "selected";} ?>>Inactivo</option>
                            </select>
                        </div>
                    </div>


                    <div class="btn-group pull-right">
                        <a type="button" class="btn btn-primary" style="margin-right: .5em;"
                           href="<?php echo site_url($this->config->item('path_backend').'/Empresa/editar/'.$empresa.'/B'); ?>">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="Guardar" name="agregar" id="agregar"/>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT WRAPPER -->
<script type="text/javascript">



    var jvalidate = $("#editar_banner").validate({
        ignore: [],
        rules: {
            recurso: {
                required: true,
            },
            nombre: {
                required: true,
            },
            
            estado: {
                required: true
            }
        }
    });

    

</script>
