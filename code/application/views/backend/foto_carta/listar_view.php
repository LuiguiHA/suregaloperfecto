<!-- PAGE CONTENT WRAPPER -->



        
        <div class="panel-body" >
            <table class="table table-hover table-cms" >
                <thead>
                <tr>
                    <td colspan="6" align="center" valign="top"></td>
                </tr>
                <tr>
                    <th>Foto</th>
                    <th>Tipo</th>
                    <th>Plataforma</th>
                    <th>Peso</th>
                    <th>Fecha de Registro</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>


                <?php
                     

                if (isset($foto_carta) && count($foto_carta) > 0) {

                    foreach ($foto_carta as $foto) {
                        ?>
                        <tr>
                            <td> <img src="<?php echo base_url().$foto->foto;?>" width="auto" height="70 px"/></td>
                            <td><?php  if ($foto->tipo == 1)
                                    echo "Principal";
                                else
                                    echo "Detalle";
                                
                                
                                ?>
                            </td>
                            <td><?php  if ($foto->plataforma == 1)
                                    echo "Web";
                                else
                                    echo "Móvil";
                                ?>
                            </td>

                            <td><?php echo $foto->peso; ?></td>
                            <td><?php echo date('Y-m-d g:i a', strtotime($foto->fecha_registro)); ?></td>

                            <td>
                                <?php
                                if ($foto->estado == 1)
                                   echo '<span class="label label-success label-form" style="width:70px; font-weight:bolder;">Activo</span>';
                                else
                                    echo '<span class="label label-danger label-form" style="width:70px; font-weight:bolder;">Inactivo</span>'
                                ?>
                            </td>
                            <td>



                                <div style="width: 100px">
                                    <a href="<?php echo site_url($this->config->item('path_backend') . '/Foto_Carta/editar/'.$carta.'/'.$foto->id); ?>"
                                       class="btn btn-info btn-condensed" title="Editar"><i class="fa fa-edit"></i></a>

                                    <a onclick="confirmarEliminar(<?php echo $foto->id?>)" class="btn btn-danger btn-condensed" title="Eliminar"><i class="fa fa-trash-o"></i></a>

                                </div>

                            </td>

                        </tr>
                        <?php
                    }

                }
                ?>

                </tbody>
            </table>
            <?php
            
            if (isset($paginador))
            echo $paginador;
            ?>
        </div>

<div class="message-box message-box-danger animated fadeIn" id="message-box-default">
    <div class="mb-container" id="content">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-trash-o"></span>Eliminar</div>
            <div class="mb-content">
                <p>¿Esta seguro de eliminar esta foto?</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close" id="closeDialog" style="margin-left: 10px">No</button>
                <button class="btn btn-default btn-lg pull-right mb-control-close" id="delete">Si</button>
            </div>
        </div>
    </div>
    <div id="loader_content" class="loader" style="visibility: hidden;display: none"></div>
</div>



<script>

    function confirmarEliminar(id_foto) {
        $('#message-box-default').show();

        $('#closeDialog').click(function () {
            $('#message-box-default').hide();
        });

        $('#delete').click(function () {
            eliminarFoto(id_foto);
        });
    }

    function eliminarFoto(id_foto) {
        $('#content').hide();
        show_loader();
        $.ajax({
            url: "<?php echo site_url($this->config->item('path_backend') . '/Foto_carta/eliminar');?>",
            type: "POST",
            data : {id_foto : id_foto},
            dataType: 'json',
            success: function (response) {
                var message = "";
                var type = "";
                if(response.cod == "1"){
                    message = response.mensaje;
                    type =  "success";
                }else{
                    message = response.mensaje;
                    type =  "warning";
                }
                $('#message-box-default').hide();
                hidden_loader();
                show_message(type,message)
            },
            error: function (xhr, status) {
                $('#message-box-default').hide();
                hidden_loader();
                $('#content').show();
                var text = "No se ha podido conectar con el servidor";
                show_message('warning',text)
            }
        });

    }


    function show_message(type,message) {

        swal({
            title: "",
            text: message,
            type: type,
            showCancelButton: false,
            confirmButtonColor: '#21A053',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Aceptar',
            allowOutsideClick: false,
        }).then(function () {
            if (type == "success"){
                var segments = $(location).attr('href').split('/',9);
                window.location.href = segments.join('/') + "/F";
            }
        })
    }

    function show_loader() {
        $('#loader_content').css({"visibility":"visible"});
        $('#loader_content').css({"display":"block"});
    }

    function hidden_loader() {
        $('#loader_content').css({"visibility":"hidden"});
        $('#loader_content').css({"display":"none"});
    }
    
       $(document).on("click", "#paginador ul li a", function(e){
           e.preventDefault();
           var href = $(this).attr("href");

          // alert(href);
           $.post(href,{id_carta : <?php echo $carta;?>,tipo : $("#tipo_foto").val(),estado : $("#estado_foto").val(),plataforma:$("#plataforma_foto").val()},
               function (result) {

                 $('#tabla_carta_foto').html(result);


               }, "html");

           return true;
       });

</script>
<!-- END PAGE CONTENT WRAPPER -->