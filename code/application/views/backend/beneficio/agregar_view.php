
<!-- PAGE CONTENT WRAPPER -->

<div class="page-content-wrap">

    <div class="container-fluid">
        <div class="col-md-12 panel-body">
            <div class="block" style="background-color:#EEEEEE;margin-bottom: 0px;padding-bottom:20px;">
                <form action="<?php echo site_url($this->config->item('path_backend').'/Beneficio/agregar');?>" id="agregar_beneficio" class="form-horizontal" role="form" name="agregar_beneficio" method="post"
                      enctype="multipart/form-data" action="">

                    <div class="form-group">
                        <label class="col-md-2 control-label">Categoría</label>
                        <div class="col-md-10">
                            <select class="form-control select" name="categoria" id="categoria" required>
                                <option value="">Seleccione</option>
                                <?php foreach ($categorias as $categoria) { ?>
                                    <option value="<?php echo $categoria->id;?>">
                                        <?php echo $categoria->nombre; ?>
                                    </option>
                                <?php } ?>

                            </select>
                        </div>
                    </div>
                    <?php
                    
                    ?>
                        <?php if (sizeof($empresas) != 1){?>
                        <div class="form-group" style="visibility: visible">
                            <label class="col-md-2 control-label">Empresa</label>
                            <div class="col-md-10">
                                <select class="form-control select" name="empresa" id="empresa" required>
                                    <option value="">Seleccione</option>
                                    <?php foreach ($empresas as $empresa) { ?>
                                        <option value="<?php echo $empresa->id; ?>">
                                            <?php echo $empresa->nombre; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    <?php
                    }
                    ?>

                    <?php if ($empresas != null && sizeof($empresas) == 1){
                        ?>
                        <input type="hidden" name="empresa" value="<?php  echo $empresas[0]->id?>" />
                    <?php }
                    ?>




                    <div class="form-group">
                        <label class="col-md-2 control-label">Nombre</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" value="" name="nombre"  id="nombre" required/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Descripción</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" value="" name="descripcion"  id="descripcion" required/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Restricción</label>
                        <div class="col-md-10">

                            <textarea type="text" class="form-control" value="" name="restriccion" id="restriccion"></textarea>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-2 control-label">Tipo</label>
                        <div class="col-md-10">
                            <select class="form-control select" name="tipo" id="tipo" required>
                                <option value="">Seleccione</option>
                                <option value="1">Canje por puntos</option>
                                <option value="2">Mixto</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Puntos</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" value="" name="puntos" id="puntos" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Adicional</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" value="" name="monto_adicional" id="monto_adicional" />
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-2 control-label">Disponible en todos los locales </label>
                        <div class="col-md-10">
                            <select class="form-control select" name="tipo_locales" id="tipo_locales" required>
                                <option value="">Seleccione</option>
                                <option value="<?php echo STATUS_ALL_LOCALS?>">Si</option>
                                <option value="<?php echo STATUS_SOME_LOCALS?>">Algunos</option>
                                <option value="<?php echo STATUS_DELIVERY?>">Delivery</option>
                            </select>
                        </div>
                    </div>


                    <div id="div_some_locals" style="margin-bottom: 15px " >
                        <div class="form-group">
                            <label class="col-md-2 control-label">Locales</label>
                            <div class="col-md-10">
                                <select  multiple class="form-control select" name="locales[]" id="locales">
                                    <?php if ($locales != null && sizeof($locales) > 0) { ?>
                                        <?php foreach ($locales as $local) { ?>
                                                <option value="<?php echo $local->id; ?>" <?php if ($local->tipo == LOCAL_DEFAULT) echo "selected"?>>
                                                    <?php echo $local->nombre; ?>
                                                </option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div id="div_delivery" style="margin-bottom: 30px " >
                        <div class="form-group">
                            <label class="col-md-2 control-label">Dirección</label>
                            <div class="col-md-10">

                                <input type="text" class="form-control" value="" name="direccion" id="direccion" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Telefono</label>
                            <div class="col-md-10">
                                <input type="number" class="form-control" value="" name="telefono" id="telefono"/>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-2 control-label">Correo</label>
                            <div class="col-md-10">
                                <input type="email" class="form-control" value="" name="correo" id="correo"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-10">
                                <div id="map" style="height: 400px;width: 100%"></div>
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Coordenadas  </label>
                            <div class="col-md-10">
                                <div style="margin-bottom: 10px">
                                    <label class="control-label">Latitud :</label>
                                    <input type="number" class="form-control"  name="latitud" id="latitud" readonly/>
                                </div>
                                <div>
                                    <label class="control-label">Longitud :</label>
                                    <input type="number" class="form-control"  name="longitud" id="longitud" readonly/>
                                </div>
                            </div>
                        </div>

                    </div>



                    <div class="form-group">
                        <label class="col-md-2 control-label">Orden</label>
                        <div class="col-md-10">
                            <input type="number" class="form-control spinner_default" min="0" value="0" name="orden" id="orden" readonly/>
                        </div>
                    </div>





                    <div class="form-group">
                        <label class="col-md-2 control-label">Fecha de inicio</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" value="" name="fecha_inicio" id="fecha_inicio" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Fecha de finalización</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" value="" name="fecha_fin" id="fecha_fin" />
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-2 control-label">Estado</label>
                        <div class="col-md-10">
                            <select class="form-control select" name="estado" id="estado" required>
                                <option value="">Seleccione</option>
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                        </div>
                    </div>



                    <div class="btn-group pull-right">
                        <a type="button" class="btn btn-primary"  style="margin-right: .5em;" href="<?php echo site_url($this->config->item('path_backend').'/inicio');?>">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="Guardar" name="agregar" id="agregar" />
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT WRAPPER -->



<script type="text/javascript">
        $(document).ready(initMap);

    var map;
    var lastMarkerLat="";
    var lastMarkerLng="";
    var markes = [];

    function initMap() {

        var mapOptions = {
            zoom: 15,
            center: {lat: -12.135817, lng: -77.0135569},
            zoomControl: true,
            scaleControl: true,
            streetViewControl: false,
            mapTypeId: 'satellite'
        };

        map = new google.maps.Map(document.getElementById('map'), mapOptions);



        google.maps.event.addListener(map, 'click', function(event) {

            if(markes.length > 0){
                markes[0].setMap(null);
                markes = [];
            }

            placeMarker(event.latLng);

        });

    }

        function placeMarker(location) {
            var marker = new google.maps.Marker({
                position: location,
                map: map
            });

            $('#latitud').val(marker.getPosition().lat());
            $('#longitud').val(marker.getPosition().lng());

            markes.push(marker);
        }


    $(".spinner_default").spinner();
    $("#div_delivery").hide();
    $("#div_some_locals").hide();

    $("#tipo_locales").change(function () {
        $("#div_delivery").hide();
        $("#div_some_locals").hide();
            if ($("#tipo_locales").val() == "<?php echo STATUS_DELIVERY?>"){
                $("#div_delivery").show();
                $("#telefono").attr("required", "true");
                $("#correo").attr("required", "true");
                google.maps.event.trigger(map, 'resize')

            }else if($("#tipo_locales").val() == "<?php echo STATUS_SOME_LOCALS?>"){
                $("#div_some_locals").show();
                $("#locales").attr("required", "true");
            }else{
                $("#telefono").removeAttr('required');
                $("#correo").removeAttr('required');
                $("#locales").removeAttr('required');
                $("#telefono").val("");
                $("#correo").val("");
                $("#direccion").val("");
                $("#longitud").val("");
                $("#latitud").val("");
            }

    });

        $("#locales").selectpicker({
            noneSelectedText:''
        });


   var jvalidate = $("#agregar_beneficio").validate({
       ignore: [],
       rules: {
           categoria: {
               required: true
           },
           <?php if ( sizeof($empresas) != 1){?>
           empresa: {
               required: true

           },
           <?php  }?>
           nombre: {
               required: true
           },

           tipo: {
               required: true
           },
           puntos: {
               required: true
           },

           fecha_inicio: {
               required: true
           },
           fecha_fin: {
               required: true
           },
           estado: {
               required: true
           }
       }
   });

   $(document).ready(function(){
       $('#telefono').keypress(validateNumber);
       $('#puntos').keypress(validateNumber);
       $('#orden').keypress(validateNumber);

   });

   function validateNumber(event) {
       var key = window.event ? event.keyCode : event.which;

       if (event.keyCode === 8 || event.keyCode === 46
           || event.keyCode === 37 || event.keyCode === 39) {
           return true;
       }
       else if ( key < 48 || key > 57 ) {
           return false;
       }
       else return true;
   }

   function validateNumberDouble(event) {
       var code = (event.which) ? event.which : event.keyCode;
       if(code==8 || code ==45 || code == 46)
       {
           //backspace
           return true;
       }
       else if(code>=48 && code<=57)
       {
           //is a number
           return true;
       }
       else
       {
           return false;
       }
   }


    $("#fecha_inicio").datetimepicker({

        format: 'YYYY-MM-DD',
        locale: 'es'
    });

    $("#fecha_fin").datetimepicker({
        format: 'YYYY-MM-DD',
        locale: 'es'
    });



</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyASbnMbZgifMOqhzVF2RreWxOlzHlQeG1c&callback=initMap"
></script>
