<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap" style="padding: 15px">
    <div class="container-fluid" style="padding-left: 0px !important;padding-right: 0px !important;">
        <div class="panel panel-default" style="border-top-color: #DAD4D4 ; border-top-width : 2px">
            <div class="panel-body">
                <div class="panel-body">


                    <form name="form_busqueda" id="form_busqueda" method="post"
                          action="<?php
                          echo site_url($this->config->item('path_backend') . '/Beneficio/buscar');
                          ?>">


                        <div class="col-md-12 form-group">

                            <div class="col-md-4">
                                <label class="control-label"> Nombre</label>
                                <input id="nombre" class="form-control input" type="text" name="nombre"
                                       placeholder="Nombre" value="<?php echo $nombre; ?>" class="input-small"/>
                            </div>

                            <div class="col-md-4">
                                <label class="control-label">Categoría</label>
                                <select class="form-control select" name="categoria" id="categoria">
                                    <option value="">Seleccione</option>
                                    <?php foreach ($categorias as $categoria)
                                    { ?>
                                        <option
                                            value="<?php echo $categoria->id; ?>" <?php if ($categoria->id == $categoria_select)
                                        {
                                            echo "selected";
                                        } ?>>
                                            <?php echo $categoria->nombre; ?>
                                        </option>
                                    <?php } ?>
                                </select>

                            </div>

                            <!-- <div class="col-md-2" >
                                <label class="control-label">Empresa</label>
                                <select class="form-control select" name="empresa" id="empresa">
                                    <option value="">Seleccione</option>
                                    <?php foreach ($empresas as $empresa)
                            { ?>
                                        <option value="<?php echo $empresa->id; ?>" <?php if ($empresa->id == $empresa_select)
                            {
                                echo "selected";
                            } ?>>
                                            <?php echo $empresa->nombre; ?>
                                        </option>
                                    <?php } ?>
                                </select>

                            </div>-->

                            <div class="col-md-4">
                                <label class="control-label">Tipo</label>
                                <select class="form-control select" name="tipo" id="tipo">
                                    <option value="">Seleccione</option>
                                    <option value="1" <?php if ($tipo == "1") echo "selected"; ?> >Caje por puntos
                                    </option>
                                    <option value="2" <?php if ($tipo == "2") echo "selected"; ?> >Mixto</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 form-group">
                            <div class="col-md-4">
                                <label class="control-label">Fecha de Inicio</label>
                                <input type="text" class="form-control" value="<?php echo $fecha_inicio?>" name="fecha_inicio" id="fecha_inicio" />

                            </div>

                            <div class="col-md-4">
                                <label class="control-label">Fecha de Finalización</label>
                                <input type="text" class="form-control" value="<?php echo $fecha_fin?>" name="fecha_fin" id="fecha_fin" />

                            </div>

                            <div class="col-md-4">
                                <label class="control-label">Estado</label>
                                <select class="form-control select" name="estado" id="estado">
                                    <option value="">Seleccione</option>
                                    <option value="1" <?php if ($estado == "1") echo "selected"; ?>>Activo</option>
                                    <option value="0" <?php if ($estado == "0") echo "selected"; ?>>Inactivo</option>
                                </select>

                            </div>
                        </div>
                        <div class="col-md-12 form-group">
                            <input type="hidden" name="buscar" id="buscar"
                                   value=""/>

                        </div>
                        <div class="col-md-12 form-group">
                            <div class="col-md-4">

                            </div>
                            <div class="col-md-8">
                                <input type="submit" class="btn btn-primary pull-right" value="Buscar"/>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <div class="panel panel-default" style="border-top-color: #DAD4D4 ; border-top-width : 2px">
        <div class="panel-heading">
            <h3 class="panel-title">Beneficios</h3>
            <a id="export_beneficio" class="btn btn-danger dropdown-toggle pull-right" style="margin-left: 5px;"><img
                    src="<?php echo base_url('assets/backend/'); ?>/img/icons/xls.png" width="16"/> Exportar</a>
            <a href="<?php echo site_url($this->config->item('path_backend') . '/Beneficio/agregar'); ?>"
               class="btn btn-default pull-right"><i class="fa fa-plus"></i> Agregar</a>

        </div>
        <div class="panel-body">
            <table class="table table-hover table-cms">
                <thead>
                <tr>
                    <td colspan="4" align="center" valign="top"></td>
                </tr>
                <tr>
                    <th>Nombre</th>
                    <th>Teléfono</th>
                    <th>Correo</th>
                    <th>Categoría</th>
                    <th>Tipo</th>
                    <th>Orden</th>
                    <th>Fecha de Inicio</th>
                    <th>Fecha de Finalización</th>
                    <th style="text-align:center;">Estado</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>

                <?php
                if (isset($beneficios) && count($beneficios) > 0)
                {

                    foreach ($beneficios as $beneficio)
                    {
                        ?>
                        <tr>
                            <td><?php echo $beneficio->nombre; ?></td>
                            <td><?php echo $beneficio->telefono; ?></td>
                            <td><?php echo $beneficio->correo; ?></td>
                            <td><?php echo $beneficio->nombre_categoria; ?></td>
                            <td><?php
                                if ($beneficio->tipo == 1)
                                {
                                    echo "Caje por puntos";
                                } else
                                {
                                    echo "Mixto";
                                }
                                ?>
                            </td>
                            <td><?php echo $beneficio->orden; ?></td>
                            <td><?php echo date('Y-m-d', strtotime($beneficio->fecha_inicio)); ?></td>
                            <td><?php echo date('Y-m-d', strtotime($beneficio->fecha_fin)); ?></td>
                            <td style="text-align:center;">
                                <?php
                                if ($beneficio->estado == 1) echo '<span class="label label-success label-form" style="width:70px; font-weight:bolder;">Activo</span>'; else
                                    echo '<span class="label label-danger label-form" style="width:70px; font-weight:bolder;">Inactivo</span>'
                                ?>
                            </td>
                            <td>

                                <a href="<?php echo site_url($this->config->item('path_backend') . '/Beneficio/editar/' . $beneficio->id); ?>"
                                   class="btn btn-info btn-condensed" title="Editar"><i class="fa fa-edit"></i></a>
                            </td>

                        </tr>
                        <?php
                    }

                }
                ?>

                </tbody>
            </table>
            <?php


            if (isset($paginador)) echo $paginador;
            ?>

        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery(document).on('click', '#paginador ul li a', function (c) {
            c.preventDefault();
            var inicio = jQuery(this).attr('href');
            jQuery("#form_busqueda").attr("action", inicio);
            jQuery("#form_busqueda").submit();
        });
    });


    $('#export_beneficio').click(function () {
        // e.preventDefault();

        var url = "<?php echo site_url($this->config->item('path_backend') . '/Beneficio/export');?>";
        var nombre = $("#nombre").val();
        var categoria = $("#categoria").val();
        var fecha_inicio = $("#fecha_inicio").val();
        var fecha_fin = $("#fecha_fin").val();
        var tipo = $("#tipo").val();
        var estado = $("#estado").val();
        //console.log (url+"?nombre="+nombre+"&categoria="+categoria+"&empresa="+empresa+"&tipo="+tipo+"&estado="+estado );

        window.location.href = url + "?nombre=" + nombre + "&categoria=" + categoria + "&fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin +"&tipo=" + tipo + "&estado=" + estado;

        /*
         $.get(url+"?nombre="+nombre+"&categoria="+categoria+"&empresa="+empresa+"&tipo="+tipo+"&estado="+estado,null,
         function(resultado){
         window.open(resultado);
         },"html"
         );
         */
    });


    $("#fecha_inicio").datetimepicker({
        format: 'YYYY-MM-DD',
        locale: 'es'
    });

    $("#fecha_fin").datetimepicker({
        format: 'YYYY-MM-DD',
        locale: 'es'
    });

</script>


<!-- END PAGE CONTENT WRAPPER -->