<!-- PAGE CONTENT WRAPPER -->

<div class="page-content-wrap">

    <div class="container-fluid">
        <div class="col-md-12 panel-body">
            <div class="block" style="background-color:#EEEEEE;margin-bottom: 0px;padding-bottom:20px;">

             <form action="<?php echo site_url($this->config->item('path_backend').'/Twitter/editar/'.$beneficio->id);?>" id="editar_beneficio" class="form-horizontal" role="form" name="editar_beneficio" method="post"
                      enctype="multipart/form-data" action="">


                
                <div class="form-group">
                    <label class="col-md-2 control-label">Título</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" value="<?php echo $beneficio->nombre; ?>" name="nombre"
                               id="nombre"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Contenido</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" value="<?php echo $beneficio->contenido; ?>"
                               name="contenido" id="contenido"/>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-2 control-label">Foto Actual</label>
                    <div class="col-md-10">

                      <img src="<?php echo base_url().$beneficio->imagen;?>" width="auto" height="70px"/>

                    </div>

                </div>
                
                <div class="form-group">
                    <label class="col-md-2 control-label">Foto</label>
                    <div class="col-md-10">

                        <input type="file" class="fileinput  btn-success"
                               name="foto" id="foto" data-filename-placement="inside" title="Seleccione una Imagen"
                               accept=".jpg,.png,.jpeg"/>
                        <label id="error_img" style="visibility: hidden ; color: #b64645 ; font-weight : normal">La imagen es necesaria. </label>
                    </div>

                </div>

                
                <div class="form-group">
                    <label class="col-md-2 control-label" >Estado</label>
                    <div class="col-md-10">
                        <select class="form-control select" name="activo" id="activo">
                            <option value="">Seleccione</option>
                            <option value="1" <?php if ($beneficio->activo == 1) {
                                echo "selected";
                            } ?> >Activo
                            </option>
                            <option value="0" <?php if ($beneficio->activo == 0) {
                                echo "selected";
                            } ?>>Inactivo
                            </option>
                        </select>
                    </div>
                </div>
                
                <input class="hidden" type="text" value="<?php echo $beneficio->imagen?>" name="foto_antiguo"/>


                <div class="btn-group pull-right">
                    <a type="button" class="btn btn-primary" style="margin-right: .5em;"
                       href="<?php echo site_url($this->config->item('path_backend') . '/inicio'); ?>">Cancelar</a>
                    <input type="submit" class="btn btn-primary" value="Guardar" name="agregar" id="agregar"/>
                </div>


            </form>
          </div>
        </div>
        
        
        
    </div>
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
    <script type="text/javascript">
        

        var jvalidate = $("#editar_beneficio").validate({
            ignore: [],
            rules: {
               
                nombre: {
                    required: true
                },

                contenido: {
                    required: true

                },
                activo: {
                    required: true
                }
            }
        });


    </script>
