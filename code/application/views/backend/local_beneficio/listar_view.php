<!-- PAGE CONTENT WRAPPER -->



        
        <div class="panel-body" >
            <table class="table table-hover table-cms" >
                <thead>
                <tr>
                    <td colspan="4" align="center" valign="top"></td>
                </tr>
                <tr>
                    <th>Local</th>

                    <th>Fecha de Registro</th>
                    <th>Estado</th>
                    <?php if ($beneficio->tipo_locales != STATUS_ALL_LOCALS){?>
                        <th>Acciones</th>
                    <?php }?>
                </tr>
                </thead>
                <tbody>


                <?php
                     

                if (isset($local_beneficio) && count($local_beneficio) > 0) {

                    foreach ($local_beneficio as $local) {
                        ?>
                        <tr>

                            <td><?php echo $local->nombre; ?></td>
                            <td><?php echo date('Y-m-d g:i a', strtotime($local->fecha_registro)); ?></td>

                            <td>
                                <?php
                                if ($local->estado == 1)
                                    echo '<span class="label label-success label-form" style="width:70px; font-weight:bolder;">Seleccionar</span>';
                                else
                                    echo '<span class="label label-danger label-form" style="width:70px; font-weight:bolder;">Quitar</span>'
                                ?>
                            </td>
                            <?php if ($beneficio->tipo_locales != STATUS_ALL_LOCALS){?>
                            <td>


                                <div style="width: 100px">
                                    <!--<a href="<?php echo site_url($this->config->item('path_backend') . '/Local_Beneficio/editar/'.$beneficio->id.'/'.$local->id); ?>"
                                       class="btn btn-info btn-condensed" title="Editar"><i class="fa fa-edit"></i></a>-->
                                    <?php if ($local->estado == 1){?>
                                        <a onclick="confirmarQuitar(<?php echo $local->id?>)" class="btn btn-danger btn-condensed" title="Quitar"><i class="fa fa-trash-o"></i></a>
                                    <?php }else{?>
                                        <a onclick="confirmarSeleccionar(<?php echo $local->id?>)" class="btn btn-success btn-condensed" title="Seleccionar"><i class="fa fa-check"></i></a>
                                    <?php }?>
                                </div>

                            </td>
                        <?php }?>

                        </tr>
                        <?php
                    }

                }
                ?>

                </tbody>
            </table>
            <?php
            
            if (isset($paginador))
                echo $paginador;
            ?>
        </div>


<div class="message-box message-box-danger animated fadeIn" id="message-box-delete">
    <div class="mb-container" id="content">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-trash-o"></span>Quitar</div>
            <div class="mb-content">
                <p>¿Esta seguro de quitar este local?</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close" id="closeDialog" style="margin-left: 10px">No</button>
                <button class="btn btn-default btn-lg pull-right mb-control-close" id="confirm">Si</button>
            </div>
        </div>
    </div>
    <div id="loader_content" class="loader" style="visibility: hidden;display: none"></div>
</div>

<div class="message-box message-box-success animated fadeIn" id="message-box-select">
    <div class="mb-container" id="contentSelect">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-check"></span>Seleccionar</div>
            <div class="mb-content">
                <p>¿Esta seguro de seleccionar este local?</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close" id="closeDialogSelect" style="margin-left: 10px">No</button>
                <button class="btn btn-default btn-lg pull-right mb-control-close" id="confirmSelect">Si</button>
            </div>
        </div>
    </div>
    <div id="loader_content_select" class="loader" style="visibility: hidden;display: none"></div>
</div>


<script>

    function confirmarQuitar(id_local) {
        $('#message-box-delete').show();

        $('#closeDialog').click(function () {
            $('#message-box-delete').hide();
        });

        $('#confirm').click(function () {
            modificarLocal(id_local,<?php echo ESTADO_INACTIVO?>);
        });
    }

    function confirmarSeleccionar(id_local) {
        $('#message-box-select').show();

        $('#closeDialogSelect').click(function () {
            $('#message-box-select').hide();
        });

        $('#confirmSelect').click(function () {
            modificarLocal(id_local,<?php echo ESTADO_ACTIVO?>);
        });
    }


    function modificarLocal(id_local,estado) {

        if (estado == "1") {

            $('#contentSelect').hide();
            show_loader_select();
        }else{
            $('#content').hide();
            show_loader();
        }
        $.ajax({
            url: "<?php echo site_url($this->config->item('path_backend') . '/Local_beneficio/editar_estado');?>",
            type: "POST",
            data : {id_local : id_local,
                estado : estado,
                id_beneficio : "<?php echo $beneficio->id?>"},
            dataType: 'json',
            success: function (response) {
                var message = "";
                var type = "";
                if(response.cod == "1"){
                    message = response.mensaje;
                    type =  "success";
                }else{
                    message = response.mensaje;
                    type =  "warning";
                }
                if (estado == "1") {

                    hidden_loader_select();
                    $('#message-box-select').hide();
                }else{
                    $('#message-box-delete').hide();
                    hidden_loader();
                }


                show_message(type,message)
            },
            error: function (xhr, status) {
                if (estado == "1") {

                    hidden_loader_select();
                    $('#contentSelect').show();
                    $('#message-box-select ').hide();
                }else{
                    $('#message-box-delete').hide();
                    hidden_loader();
                    $('#content').show();
                }


                var text = "No se ha podido conectar con el servidor";
                show_message('warning',text)
            }
        });

    }


    function show_message(type,message) {

        swal({
            title: "",
            text: message,
            type: type,
            showCancelButton: false,
            confirmButtonColor: '#21A053',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Aceptar',
            allowOutsideClick: false,
        }).then(function () {
            if (type == "success"){
                var segments = $(location).attr('href').split('/',9);
                window.location.href = segments.join('/') + "/L";
            }
        })
    }

    function show_loader() {
        $('#loader_content').css({"visibility":"visible"});
        $('#loader_content').css({"display":"block"});
    }

    function hidden_loader() {
        $('#loader_content').css({"visibility":"hidden"});
        $('#loader_content').css({"display":"none"});
    }

    function show_loader_select() {
        $('#loader_content_select').css({"visibility":"visible"});
        $('#loader_content_select').css({"display":"block"});
    }

    function hidden_loader_select() {
        $('#loader_content_select').css({"visibility":"hidden"});
        $('#loader_content_select').css({"display":"none"});
    }

    $(document).on("click", "#paginador ul li a", function(e){
        e.preventDefault();
        var href = $(this).attr("href");

        // alert(href);
        $.post(href,{id_beneficio : <?php echo $beneficio->id;?> , local : $("#local_nombre").val(), estado : $("#estado_local").val()},
            function (result) {

                $('#tabla_beneficio_local').html(result);


            }, "html");

        return true;
    });

</script>
<!-- END PAGE CONTENT WRAPPER -->