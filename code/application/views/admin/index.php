<body class="admin-body">
	<div class="admin-content">
		<nav class="navbar navbar-dark bg-dark">
		  <div class="container d-flex justify-content-between">
          <a href="#" class="navbar-brand d-flex align-items-center">
            <strong>Administrador</strong>
          </a>
          <?php echo anchor('admin/logout', 'Cerrar sesión', array("class" => "admin-logout"))?>
        </div>
		</nav>

		<div class="container">

			<div class="row">
				<div class="col-12">
					<h3 class="admin-section-title">Lista de historias</h3>
				</div>
			</div>

			<div class="row">
				<div class="col-12">
					<?php echo anchor('admin/stories/export', 'Descargar todas las historias', array("class" => "btn btn-primary admin-download-stories"))?>
				</div>
			</div>

			<table cellpadding=0 cellspacing=10 class="admin-stories-table table">
				<tr>
					<th>Nombres</th>
					<th>Apellidos</th>
					<th>DNI</th>
					<th>Email</th>
					<th>Teléfono</th>
					<th>Historia</th>
					<th>Fecha de registro</th>
				</tr>
				<?php foreach ($stories as $story):?>
					<tr>
			            <td><?php echo htmlspecialchars($story->nombres,ENT_QUOTES,'UTF-8');?></td>
			            <td><?php echo htmlspecialchars($story->apellidos,ENT_QUOTES,'UTF-8');?></td>
			            <td><?php echo htmlspecialchars($story->dni,ENT_QUOTES,'UTF-8');?></td>
			            <td><?php echo htmlspecialchars($story->email,ENT_QUOTES,'UTF-8');?></td>
			            <td><?php echo htmlspecialchars($story->phone,ENT_QUOTES,'UTF-8');?></td>
			            <td><?php echo htmlspecialchars($story->historia,ENT_QUOTES,'UTF-8');?></td>
			            <td><?php echo htmlspecialchars($story->fecha_registro,ENT_QUOTES,'UTF-8');?></td>
					</tr>
				<?php endforeach;?>
			</table>

			<div class="row justify-content-center">
				<nav aria-label="Page navigation example">
				  <ul class="pagination">
				  	<?php for ($i = 0; $i < $total_pages; $i++):?>
				    <li class="page-item"><a class="page-link" href="/admin?page=<?php echo $i + 1; ?>"><?php echo $i + 1; ?></a></li>
				    <?php endfor;?>
				  </ul>
				</nav>
			 </div>
		</div>
	</div>
</body>