<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>El Regalo Perfecto</title>
		<!--[if (gte mso 9)|(IE)]>
		<style type="text/css">
		table {border-collapse: collapse;}
		</style>
		<![endif]-->
	</head>
	
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="min-width: 100%; background: #ffffff; margin: 0; padding: 0;">

  <style type="text/css">

      @-ms-viewport {
          width: device-width;
      }
      @media screen and (max-width: 400px) {
          .one-column .column{
              max-width: 100% !important;
          }
      }
      @media screen and (min-width: 401px) and (max-width: 620px) {
          .two-column .column {
              max-width: 50% !important;
          }
      }

      .full-width-image {
          width: 100%;
      }
  </style>
  
  <center class="wrapper" style="width: 100%; table-layout: fixed; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
   
    <table width="600" bgcolor="#ffffff" align="center" border="0" cellpadding="0" cellspacing="0">

      <tr>
        <td style="background: #05301c;text-align: center">
          <img src="<?php echo base_url();?>assets/css/images/logo_fabercastell.jpg">
        </td>
      </tr>

      <tr>
        <td valign="top">
               
          <table width="600" border="0" cellspacing="0" cellpadding="0">

                <tr>
                    <td width="500" valign="top" bgcolor="#fff" style="padding: 15px;">

                        <p style="padding:0; border:0; margin:0; font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; line-height:18px; color:#000;">
                            Mensaje enviado por <?php echo $nombre; ?>.
                        </p><br />
                        
    
                        <p style="padding:0; border:0; margin:0; font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; line-height:18px; color:#000;">
                            Correo electrónico: <a href="mailto:<?php echo $correo; ?>"><?php echo $correo; ?></a>.
                        </p><br />
                        
                        <p style="padding:0; border:0; margin:0; font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; line-height:18px; color:#000;">
                            Mensaje:
                        </p><br />

                        <p style="padding:0; border:0; margin:0; font-family:Arial; font-size:14px; color:#000; line-height:18px;">
                            <?php echo $mensaje?>
                        </p><br/>
                    </td>
                        
                </tr>

            </table>
        </td>
      </tr>
      <tr>
          <td height="30" valign="top" bgcolor="#fff" style="text-align:center"><span style="color: #808080;font-family:Arial; font-size:11px;"> © 1761-<?php echo date('Y');?> Faber-Castell. </span></td>
      </tr>
    </table>

  </center>

</body>
</html>
