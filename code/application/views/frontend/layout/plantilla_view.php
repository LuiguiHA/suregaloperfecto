<!doctype html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Faber-Castell - Su regalo perfecto</title>
<link href="<?php echo base_url() ?>assets/css/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, max-scale=1.0">

<meta name="robots" content="index, follow" />
<meta name="keywords" content="Faber-Castell, Faber Castell, Su Regalo Perfecto<?php
  if (isset($categorias) && count($categorias) > 0) {
    foreach ($categorias as $categoria) {
      echo ', '.$categoria->nombre;
    }
  }

  if (isset($productos) && count($productos) > 0) {
    foreach ($productos as $producto) {
      $nrp = str_replace('<br>', ' ', $producto->nombre);
      echo ', '.$nrp;
    }
  }
?>
" />
<meta content="Conoce nuestra línea de escritura fina para encontrar el regalo perfecto para esa persona tan especial. Sólo cuéntanos un poco sobre lo que buscas y te damos la mejor recomendación" name="description" />


<meta property="og:title" content="Faber-Castell - Su Regalo Perfecto" />
<meta property="og:description" content="Conoce nuestra línea de escritura fina para encontrar el regalo perfecto para esa persona tan especial. Sólo cuéntanos un poco sobre lo que buscas y te damos la mejor recomendación" />
<meta property="og:url" content="http://www.suregaloperfecto.com" />
<meta property="og:image" content="<?php echo base_url() ?>assets/images/fabercastell_facebook.jpg" />

<?php echo $this->minify->deploy_css(); ?>
<link href="<?php echo base_url() ?>assets/slick/slick.css" rel="stylesheet" type="text/css">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="js/html5shiv.min.js"></script>
<![endif]-->

<script>
  var PATH_BASE = '<?php echo base_url() ?>';
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-112403330-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-112403330-1');
</script>

</head>

<body>

<?php
  $this->load->view('frontend/layout/menu_view',TRUE);

  if(isset($vista) && $vista !="") $this->load->view($vista);

  $this->load->view('frontend/layout/footer_view',TRUE);
?>

<!--POPUP: INFO -->
<div class="popupInfo" style="display: none;">


	<div class="content">
	<!--CLOSE	-->
	<div class="close"></div>
	<!--CONTENT	-->
		<!--GALLERY -->
		<div class="gallery">
			<!--<div class="thumbs">
				<a href="#"><img src="<?php echo base_url() ?>assets/images/galeria/sku0001/pic01_small.jpg" alt=""></a>
				<a href="#"><img src="<?php echo base_url() ?>assets/images/galeria/sku0001/pic02_small.jpg" alt=""></a>
			</div>-->
			<div class="photo">
				<img src="<?php echo base_url() ?>assets/images/catalogo/default.jpg" alt="">
			</div>
		</div>
		<!--DETAIL -->
		<div class="detail">
			<h3>Lápicero 325698</h3>
			<span>SKU: 2356987456</span>
			<p>Descripción</p>
			<p class="desc_pro">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error commodi quae, facilis ab dolorem rem nulla corporis assumenda aut quod fuga, quidem nemo dolore optio culpa hic, quo eius odit.</p>
			<div class="group">
			</div>
		</div>
	</div>

</div>

<!--POPUP: REGALO -->
<div class="popupRegalo" style="display: none;">


	<!--CONTENT	-->
	<div class="content">
	  <!--CLOSE	-->
	<div class="close"></div>

		<h3>Su regalo perfecto es:</h3>
		<img src="<?php echo base_url() ?>assets/images/catalogo/default-large.jpg" alt="">

		<h4>Portaminas Pocket Plata</h4>
		<span>SKU: 247242742</span>
		<p>Pequeño instrumento de escritura de bolsillo, cuerpo pulido y de alto brillo con un capuchón en brillante.</p>
		<div class="group">
			<a href="#">Retail</a>
			<a href="#">Retail</a>
			<a href="#">Obtenlo aquí</a>
		</div>

	</div>

</div>


<!--POPUP: TÉRMINOS -->
<div class="popupTerminos" style="display: none;">


	<!--CONTENT	-->
	<div class="content">
	  <!--CLOSE	-->
	<div class="close"></div>

		<h3>Términos y Condiciones</h3>
		<div class="sb-container container-example1">
		  <p>Descargo de responsabilidad </p>
		  <h4>1. Contenido</h4>
		  <p>El autor se reserva el derecho de no ser responsable de la actualidad, corrección, integridad o calidad de la información proporcionada. Por lo tanto, se rechazarán las reclamaciones de responsabilidad por daños causados por el uso de cualquier información proporcionada, incluido cualquier tipo de información que sea incompleta o incorrecta. Todas las ofertas no son vinculantes y sin compromiso. El autor puede ampliar, cambiar o eliminar parcial o completamente partes de las páginas o la publicación completa, incluidas todas las ofertas e información, sin previo aviso. </p>

		  <h4>2. Referencias y enlaces</h4>
		  <p>El autor no es responsable de ningún contenido vinculado o mencionado en sus páginas, a menos que tenga pleno conocimiento de los contenidos ilegales y pueda evitar que los visitantes de su sitio vean esas páginas. Si se produce algún daño mediante el uso de la información presentada allí, solo el autor de las páginas respectivas puede ser responsable, no el que ha vinculado a estas páginas. Además, el autor no se hace responsable de las publicaciones o mensajes publicados por los usuarios de los foros de debate, libros de visitas o listas de correo proporcionadas en su página. </p>

		  <h4>3. Derechos de autor</h4>
		  <p>El autor intentó no utilizar ningún material protegido por derechos de autor para la publicación o, si no es posible, indicar los derechos de autor del objeto respectivo. Los derechos de autor de cualquier material creado por el autor están reservados. No se permite ninguna duplicación o uso de objetos tales como diagramas, sonidos o textos en otras publicaciones electrónicas o impresas sin el consentimiento del autor.</p>

		  <h4>4. Política de privacidad</h4>
		  <p>Si se da la oportunidad para la entrada de datos personales o comerciales (direcciones de correo electrónico, nombre, direcciones), la entrada de estos datos se realiza de forma voluntaria. El uso y el pago de todos los servicios ofrecidos están permitidos, si y hasta el momento es técnicamente posible y razonable, sin especificar ningún dato personal o bajo especificación de datos anónimos o un alias. Se prohíbe el uso de direcciones postales publicadas, números de teléfono oax y direcciones de correo electrónico para fines de marketing, los delincuentes que envíen mensajes no deseados de spam serán castigados ".</p>

		  <p>Este sitio web utiliza Google Analytics, un servicio de análisis web proporcionado por Google, Inc. ("Google"). Google Analytics utiliza "cookies", que son archivos de texto colocados en su computadora, para ayudar al sitio web a analizar cómo los usuarios usan el sitio. La información generada por la cookie sobre su uso del sitio web (incluida su dirección IP) será transmitida y almacenada por Google en servidores en los Estados Unidos. ? En caso de activación de la anonimización de IP, Google truncará / anonimizará el último octeto de la dirección IP para los Estados miembros de la Unión Europea, así como para otras partes en el Acuerdo sobre el Espacio Económico Europeo. ? Solo en casos excepcionales, los servidores de Google en EE. UU. Envían y acortan la dirección IP completa. En nombre del proveedor del sitio web, Google utilizará esta información con el fin de evaluar su uso del sitio web, compilar informes sobre la actividad del sitio web para los operadores del sitio web y proporcionar otros servicios relacionados con la actividad del sitio web y el uso de Internet al proveedor del sitio web. ? Google no asociará su dirección IP con ningún otro dato en poder de Google. ? Puede rechazar el uso de cookies seleccionando la configuración adecuada en su navegador. Sin embargo, tenga en cuenta que si hace esto, es posible que no pueda usar la funcionalidad completa de este sitio web. ? Además, puede evitar que Google recopile y use datos (cookies y direcciones IP) descargando e instalando el complemento del navegador disponible en <a href="https://tools.google.com/dlpage/gaoptout?hl=en-GB" target="_blank">https://tools.google.com/dlpage/gaoptout?hl=en-GB</a>. Más información se puede encontrar en <a href="http://www.google.com/analytics/terms/gb.html" target="_blank">http://www.google.com/analytics/terms/gb.html</a> (Términos de servicio y privacidad de Google Analytics). ? ? Puede rechazar el uso de cookies seleccionando la configuración adecuada en su navegador. Sin embargo, tenga en cuenta que si hace esto, es posible que no pueda usar la funcionalidad completa de este sitio web. ? Además, puede evitar que Google recopile y use datos (cookies y direcciones IP) descargando e instalando el complemento del navegador disponible en <a href="https://tools.google.com/dlpage/gaoptout?hl=en-GB" target="_blank">https://tools.google.com/dlpage/gaoptout?hl=en-GB</a>. Más información se puede encontrar en <a href="http://www.google.com/analytics/terms/gb.html" target="_blank">http://www.google.com/analytics/terms/gb.html</a> (Términos de servicio y privacidad de Google Analytics). Puede rechazar el uso de cookies seleccionando la configuración adecuada en su navegador. Sin embargo, tenga en cuenta que si hace esto, es posible que no pueda usar la funcionalidad completa de este sitio web. ? Además, puede evitar que Google recopile y use datos (cookies y direcciones IP) descargando e instalando el complemento del navegador disponible en <a href="https://tools.google.com/dlpage/gaoptout?hl=en-GB" target="_blank">https://tools.google.com/dlpage/gaoptout?hl=en-GB</a>. Más información se puede encontrar en <a href="http://www.google.com/analytics/terms/gb.html" target="_blank">http://www.google.com/analytics/terms/gb.html</a> (Términos de servicio y privacidad de Google Analytics). Puede encontrar más información en <a href="http://www.google.com/analytics/terms/gb.html" target="_blank">http://www.google.com/analytics/terms/gb.html</a> (Términos de servicio y privacidad de Google Analytics). </p>

		  <p>Tenga en cuenta que en este sitio web, el código de Google Analytics se complementa con "gat._anonymizeIp ();" para garantizar una colección anónima de direcciones IP (llamado enmascaramiento de IP). </p>

		  <h4>5. Validez legal de este descargo de responsabilidad </h4>
		  <p>Este descargo de responsabilidad se debe considerar como parte de la publicación en Internet de la que se le remitió. Si las secciones o los términos individuales de esta declaración no son legales o correctos, el contenido o la validez de las otras partes no se ven afectados por este hecho.</p>

		</div>
	</div>

</div>

<!--POPUP: ENCUENTRANOS -->
<div class="popupEncuentranos" style="display: none;">


	<!--CONTENT	-->
	<div class="content">
	<!--CLOSE	-->
	<div class="close"></div>
		<h3>Encuéntranos</h3>

		<!--FILTER MOVIL -->
		<select id="select3" class="movFilter" name="select"></select>

		<!--MENU -->
		<div class="group"></div>

		<!--PLACES -->
		<div class="places"></div>

	</div>

	<!--NAV	-->
	<!--<span class="aPrev"></span>
	<span class="aNext"></span>-->

</div>

<script src="<?php echo base_url() ?>assets/js/jquery-1.12.4.min.js"></script>
<script src="<?php echo base_url() ?>assets/slick/slick.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/sweetalert.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/service.ddlist.jquery.min.js"></script>

<!--<script src="https://www.google.com/recaptcha/api.js?onload=loadCaptcha&render=explicit" async defer></script>-->
<script>
    var Path = "<?php echo base_url() ?>index.php/api/";
</script>
<?php echo $this->minify->deploy_js(TRUE, 'auto'); ?>

</body>
</html>
