<!--FOOTER -->
<footer class="footer">
	<ul>
		<li><a href="#home">Inicio</a></li>
		<li><a href="#regalo-perfecto">Su regalo perfecto</a></li>
		<li><a href="#linea-escritura-fina">Línea de escritura fina</a></li>
		<li><a href="#contacto">Encuéntranos</a></li>
	</ul>
	<div class="social">
	  <a href="https://www.facebook.com/FaberCastellPeru/" target="_blank"><img src="<?php echo base_url() ?>assets/images/facebook.png" alt=""></a>
	  <a href="https://www.instagram.com/fabercastellperu/" target="_blank"><img src="<?php echo base_url() ?>assets/images/instagram.png" alt=""></a>
	  <a href="https://www.youtube.com/user/FaberCastellPeru" target="_blank"><img src="<?php echo base_url() ?>assets/images/youtube.png" alt=""></a>
	</div>
	<p>© 1761-<?php echo date('Y'); ?> Faber-Castell</p>
</footer>