<!--PAGE: HOME -->
<!doctype html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Faber-Castell - Su regalo perfecto</title>
<link href="<?php echo base_url() ?>assets/css/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" >
<meta name="viewport" content="width=device-width, initial-scale=1.0, max-scale=1.0">

<meta name="robots" content="index, follow" />
<meta content="Conoce nuestra línea de escritura fina para encontrar el regalo perfecto para esa persona tan especial. Sólo cuéntanos un poco sobre lo que buscas y te damos la mejor recomendación" name="description" />


<meta property="og:title" content="Faber-Castell - Su Regalo Perfecto" />
<meta property="og:description" content="Conoce nuestra línea de escritura fina para encontrar el regalo perfecto para esa persona tan especial. Sólo cuéntanos un poco sobre lo que buscas y te damos la mejor recomendación" />
<meta property="og:url" content="http://www.suregaloperfecto.com" />
<meta property="og:image" content="<?php echo base_url() ?>assets/images/fabercastell_facebook.jpg" />

<?php echo $this->minify->deploy_css(); ?>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="js/html5shiv.min.js"></script>
<![endif]-->

<script>
  var PATH_BASE = '<?php echo base_url() ?>';
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-112403330-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-112403330-1');
</script>

</head>
<body>
<!-- <nav class="menu" style="display: none">
  <a class="icologo" href="#home">Faber-Castell</a>
  <a class="ico02" href="#regalo-perfecto">Su Regalo Perfecto</a>
  <a class="ico01" href="#linea-escritura-fina">Línea de Escritura Fina</a>
  <a class="ico03" href="#contacto">Encuéntranos</a>
</nav> -->
<section class="pageHome history" id="home">
	<div class="menu-history">
		<a class="icologo" href="/home">Faber-Castell</a>
		<a class="ico02" href="/home#regalo-perfecto">Su Regalo Perfecto</a>
		<a class="ico01" href="/home#linea-escritura-fina">Línea de Escritura Fina</a>
		<a class="ico03" href="/home#contacto">Encuéntranos</a>
	</div>
	<!-- <h1>Faber-Castell - Su regalo perfecto</h1> -->
	
	<!-- <div class="intro" anim>
      <a class="ico02" href="#regalo-perfecto">Su Regalo Perfecto</a>
      <a class="ico01" href="#linea-escritura-fina">Línea de Escritura Fina</a>
      <a class="ico03" href="#contacto">Encuéntranos</a>
	</div> -->
</section>
<!--PAGE: HISTORIA -->
<section class="pageHistory" id="history">
	
	<div class="content">
		<div class="modHistory" anim>
			<img src="assets/images/landing_historias.png" class="title">
			<p class="subtitle">Cuéntanos tu historia aquí</p>
			<!--FORM -->
			<div class="container-form">
				<form id="formHistory" name="form2" method="post">
					<input class="itCampo" type="text" name="name" id="name" placeholder="Nombre">
					<input class="itCampo" type="text" name="lastName" id="lastName" placeholder="Apellido">
					<input class="itCampo" type="email" name="userEmail" id="email" placeholder="E-mail">
					<div class="two-fields">
						<input class="itCampo dni" type="text" name="dni" id="dni" placeholder="DNI" maxlength="8">
						<input class="itCampo" type="text" name="phone" id="phone" placeholder="Celular/Fijo" maxlength="9">
					</div>
					<textarea class="itArea" name="historyText" id="historyText" placeholder="Tu historia"></textarea>
					<div class="terms info">
						<p>He leído y <a href="#" class="protection">acepto la cláusula de consentimiento de Protección de Datos Personales</a>.</p>
						<p class="checkbox"><input type="radio" name="info" id="info" value="SI">SI <input type="radio" name="info" id="info" value="NO"> NO</p>
					</div>
					<p class="terms conditions"><a href="#" class="terminos">Acepto los términos y condiciones del sorteo.</a></p>
					<div class="inErrorHistory">
						<p></p>
					</div>
					<input class="btn" type="submit" name="submit2" id="submit2" value="Enviar">
					<div class="div-line">
						<div class="line"><hr></div>
						<div class="clic"><i class="fas fa-circle"></i></div>
						<div class="line"><hr></div>
					</div>
					<button class="btn-fb"><i class="fab fa-facebook-f"></i>&nbsp;Conectar con facebook</button>
				</form>
			</div>
		</div>
	</div>
	
</section>


<div class="popupTerminos history-terms-modal" id="terms" style="display: none;">
	<!--CONTENT	-->
	<div class="content">
		<!--CLOSE	-->
		<div class="close"></div>

			<h3>Términos y Condiciones</h3>
			<div class="sb-container container-example1 history-terms">
				<!-- <p>Descargo de responsabilidad </p> -->
				<h4>1. DENOMINACIÓN:</h4>
				<p>"Historias perfectas"</p>

				<h4>2. AMBITO DE APLICACIÓN:</h4>
				<p>Promoción válida en todo el territorio Nacional (Perú).</p>

				<h4>3. DURACIÓN DE LA PROMOCIÓN</h4>
				<p style="margin-bottom: 10px;">14/12/2018 – 23/12/2018</p>
				<ul>
					<li><strong>Fecha de inicio de la promoción:</strong> Del 14 de Diciembre de 2018 a las 15:00 horas.</li>
					<li><strong>Fecha máxima de recepción de obras:</strong> Viernes 21 de Diciembre de 2018 hasta las 23:59 horas</li>
					<li><strong>Fecha de evaluación y elección de ganadores:</strong> 26 de Diciembre de 2018.</li>
				</ul>

				<h4>4. ALCANCES DE LA PROMOCIÓN</h4>
				<p>Se sorteará un ganador.</p>

				<h4>5. OBJETIVO Y MECÁNICA DE LA PROMOCIÓN</h4>
				<ul>
					<li>Los participantes deberán ingresar en el landing de “Historias perfectas” y compartir la historia más emotiva que han tenido a lo largo de sus vidas.</li>
					<li>El texto ingresado deberá ser inédito y no vulnerar el derecho de autor o algún otro propiedad de terceros.</li>
					<li>Las historias podrán ser -a criterio y selección de FABER-CASTELL- colgadas en sus redes sociales y plataformas.</li>
					<li>Se podrá desarrollar -a consideración y decisión de FABER-CASTELL- un video o material audiovisual de carácter promocional en base a la historia ganadora y/o de cualquier historia participante.</li>
					<li>FABER-CASTELL garantiza el irrestricto respeto por la autoría de las historias participantes.</li>
					<li>La participación de los participantes supone la aceptación expresa y sin reservas de las presentes bases.</li>
				</ul>

				<h4>6. DETALLE SOBRE LOS PREMIOS</h4>
				<ul>
					<li>
						<strong>Premio:</strong>
						<ul class="check">
							<li> 1 Set Bolígrafo GvFC Anello Set His & Hers</li>
						</ul>
					</li>
				</ul>

				<h4>7. ENTREGAS DE LOS PREMIOS</h4>
				<ul>
					<li>El premio será entregado a partir del 30 de Diciembre de 2018.</li>
					<li>El ganador(a) deberá presentar su documento de identidad vigente al momento de recoger el premio. En caso de menor de edad, deberá venir con su padre, madre, tutor o apoderado, portando los intervinientes su documento de identidad y cualquier otro que acredite el parentesco o la condición de tutor o apoderado.</li>
					<li>El ganador será contactado por correo electrónico o teléfono.</li>
					<li>El participante ganador será publicado en el fanpage e Instagram Faber-Castell Perú.</li>
				</ul>

				<h4>8. RESTRICCIONES</h4>

				<ul>
					<li>Válido una historia por participante.</li>
					<li>El premio es personal e intransferible, su naturaleza y características no podrán cambiarse, ni siquiera a pedido del propio ganador. Tampoco podrán ser cambiados por dinero en efectivo.</li>
					<li>No podrán participar en la promoción los trabajadores de A.W. Faber-Castell Peruana S.A., sus ascendientes, descendientes y parientes en línea colateral hasta el cuarto grado de consanguinidad y segundo de afinidad.</li>
					<li>De no cumplir con los términos y condiciones señaladas, automáticamente la participación será declarada inválida.</li> 
				</ul>

				<h4>9. ASPECTOS LEGALES ADICIONALES</h4>
				<p>Las condiciones aquí contenidas serán de aplicación subsidiaria a cualquier otra que sobre la misma materia se establezca con carácter especial y así se ponga en conocimiento de los participantes, por ejemplo, a través de formularios de registro, bases reguladoras de concursos o promociones y/o condiciones de los servicios particulares, quedando las presentes políticas como complementarias de las anteriores en aquello que no se contradiga.<br>Los datos personales brindados a A.W. Faber Castell Peruana S.A. quedan incorporados a sus bancos de datos y serán empleados para la gestión y realización del concurso denominado “Historias perfectas”, incluyendo el procesamiento de datos, envío de mensajería electrónica, entre otros. El titular de los datos personales o sus representantes y/o apoderados legales, podrán revocar la autorización para el tratamiento de sus datos personales en cualquier momento. A.W. Faber Castell Peruana S.A. garantiza el adecuado uso de los datos personales proporcionados por los participantes.</p>
			</div>
		</div>

	</div>
</div>

<div class="popupTerminos .popupProteccion history-terms-modal"  id="protection" style="display: none;">
	<!--CONTENT	-->
	<div class="content">
		<!--CLOSE	-->
		<div class="close"></div>

			<h3>Protección de datos</h3>
			<div class="sb-container container-example1 history-terms">
				<p>Nos permites mantener actualizada tu información y nos autorizas a incorporarla en la base de datos de "Clientes".</p><br>
				<p>Te informamos que, de acuerdo a la Ley N° 29733 - Ley de Protección de Datos Personales, sus modificatorias y ampliatorias, estamos autorizados a dar tratamiento a los datos personales que nos brindas, los mismos que serán almacenados en nuestra base durante 20 años desde su registro.</p><br>
				<p>Tus datos serán usados con la finalidad de optimizar nuestros servicios, así como para nuestra gestión comercial, estadística y administrativa, lo cual autorizas expresamente.</p><br>
				<p>En este sentido, nos comprometemos a mantener la confidencialidad de la información, utilizando estándares de seguridad conforme a Ley, a fin de evitar su alteración, pérdida, tratamiento o acceso no autorizado en nuestra base de datos.</p><br>
				<p>Te recordamos que, podrás acceder a esta información, rectificarla, cancelarla, oponerte a su uso o revocar tu consentimiento, dirigiéndote a las oficinas de la Faber Castell Perú de manera presencial en el horario establecido para la atención.</p>
			</div>
		</div>

	</div>
</div>

<script src="<?php echo base_url() ?>assets/js/jquery-1.12.4.min.js"></script>
<script src="<?php echo base_url() ?>assets/slick/slick.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/sweetalert.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.alphanum.js"></script>
<!--<script src="https://www.google.com/recaptcha/api.js?onload=loadCaptcha&render=explicit" async defer></script>-->
<script>
    var Path = "<?php echo base_url() ?>index.php/api/";
</script>

<?php echo $this->minify->deploy_js(); ?>

</body>

<!--FOOTER -->
<footer class="footer">
	<ul>
		<li><a href="/home#regalo-perfecto">Su regalo perfecto</a></li>
		<li><a href="#linea-escritura-fina">Obsequios corporativos</a></li>
		<li><a href="#contacto">Catálogo de productos</a></li>
		<li><a href="#contacto">Puntos de venta</a></li>
		<li><a href="#contacto">Contacto</a></li>
		<li><a href="#contacto">Newsletter</a></li>
	</ul>
	<p>© 1761-<?php echo date('Y'); ?> Faber-Castell</p>
</footer>