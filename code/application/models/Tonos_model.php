<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class Tonos_model extends MY_Model{

    public function __construct(){

        parent::__construct();

        $this->table = "tonos";
        $this->table_id = "Id";

    }

}