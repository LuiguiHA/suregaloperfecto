<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class Punto_model extends MY_Model{

    public function __construct(){

        parent::__construct();

        $this->table = "punto";
        $this->table_id = "id";

    }

}