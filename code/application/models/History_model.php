<?php

if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class History_model extends MY_Model{

    public function __construct(){

        parent::__construct();

        $this->table = "historia";
        $this->table_id = "id";

    }

    public function getPage($page = 1, $maxPerPage = 30)
    {
        $query = $this->db->get('historia', $maxPerPage, ($maxPerPage * ($page - 1)));
        return $query->result();
    }

    public function getTotalPages($maxPerPage = 30)
    {
        $totalPages = $this->db->count_all_results('historia');
        return ceil($totalPages/$maxPerPage);
    }



}