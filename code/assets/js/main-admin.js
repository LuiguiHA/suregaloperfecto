$(document).on('ready', function(){
    eventos();
});

function login(){
    var params = {
        email: $('#email').val(),
        password: $('#password').val(),
    };
    // Post(params,url,token,function (res) {

    //     if(res.resultado){
    //     $('#name').val("");
    //     $('#lastName').val("");
    //     $('#email').val("");
    //     $('#historyText').val("");
    //     $('#dni').val("");
    //     $('#phone').val("");
    //     $("#formHistory input[name='info']:checked").removeAttr('checked');

    //     show_message("Muy bien!",res.mensaje,"success");

    //     }else{
    //     show_message("UPSS","Ha ocurrido un error a tratar de enviar la información.","error");
    //     }
    // });
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": Path + 'admin/login',
        "method": "POST",
        "headers": {
            "content-type": "application/x-www-form-urlencoded"
        },
        "data": params
    };

    $.ajax(settings)
    .success(function (response) {
        // callback(response,status,xhr);
        if(response.resultado){
            location.reload();
        }
    })
    .fail(function (err) {
        callback(err);
    });
}

function logout(){
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": Path + 'admin/logout',
        "method": "POST",
        "headers": {
            "content-type": "application/x-www-form-urlencoded"
        }
    };

    $.ajax(settings)
    .success(function (response) {
        // callback(response,status,xhr);
        if(response.resultado){
            location.reload();
        }
    })
    .fail(function (err) {
        callback(err);
    });
}

function eventos(){
    $('#login').click(function(e){
        e.preventDefault();
    
        // gtag('event', 'click-button', { 'event_category': 'Click en botón', 'event_label': 'Ingresar al administrador' });
    
        // if (validate.history()) {
        //   login();
        // }
        login();
    });

    $('#logout').click(function(e){
        e.preventDefault();
    
        // gtag('event', 'click-button', { 'event_category': 'Click en botón', 'event_label': 'Ingresar al administrador' });
    
        // if (validate.history()) {
        //   login();
        // }
        logout();
    });
}
