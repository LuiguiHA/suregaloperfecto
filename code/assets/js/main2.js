var winHeight=($(window).outerHeight()/3)*2, primeraCarga=true;
// var Path = "http://www.suregaloperfecto.com/index.php/api/";
//var Path = "http://suregaloperfecto.preview.pe/index.php/api/";
//var Path = "http://localhost/suregaloperfecto/index.php/api/";

var captchaContainer = null;
var re_captcha = true;

var puntosVenta = {};
var lugaresVenta = {};
var productosPorPagina = 6;
var idTono = 0;

/*var loadCaptcha = function () {
    captchaContainer = grecaptcha.render('captcha_container', {
        'sitekey': '6LfvoT8UAAAAANgJpvMFB7LOE7JWWA6p0VAO2Li1',
        'callback': function (response) {
            re_captcha = true;
        }
    });
};*/

$(document).on('ready', function(){

  $('.menu').fadeOut(0);

  $('#cboTono').ddlist({
    width: '95%',
    selectionIndex: 0,
    onSelected: function (index, value, text) {
      // Do something with selected option
      idTono = value;
      if(index != 0){
        $('.ddListContainer > a').addClass('ddListPaddingClose');
        $('.ddListContainer > a').removeClass('ddListPaddingOpen');
      }else{
        $('.ddListContainer > a').addClass('ddListPaddingOpen');
        $('.ddListContainer > a').removeClass('ddListPaddingClose');
      }
    },
    onSelectedOnInit : function(){
      $('.ddListContainer > a').addClass('ddListPaddingOpen');
    }
  });



  $(window).on('scroll', function(){


    anim();
    if($(window).scrollTop() >= $('#home').height() - 20){
      $('.menu').fadeIn(250);
    }else{
      $('.menu').fadeOut(250);
    }
  });
});

$(window).on('load',function(){

  eventos();

  sliderProductos();
  Services.getPuntosAtencion();

  if($(window).outerWidth()<=640){
    productosPorPagina = 4;
    Services.getProductos(0);
  }
  cargarImagenes();
  anim();
  $(".sb-container").scrollBox();

});

function cargarPuntosAtencion(id){

  puntosVenta.map((punto, i, a) => {

    if(punto.lugar_id == id && punto.online == 0){
      $('.popupEncuentranos .content .places').append('<div class="item">' +
				'<h4>'+punto.nombre+'</h4>'+
				'<p>'+punto.direccion+'</p>'+
				'<a class="email" href="mailto:'+punto.email+'">'+punto.email+'</a>'+
				'<div class="btns">'+
					'<div class="phone">'+punto.telefono+'</div>'+
					'<a href="'+punto.query+'">Maps</a>'+
				'</div>'+
			'</div');
    }else if(punto.lugar_id == id && punto.online == 1){
      $('.popupEncuentranos .content .places').append('<div class="item">' +
				'<h4>'+punto.nombre+'</h4>'+
				'<a class="email" href="'+punto.direccion+'" target="_blank">'+punto.direccion+'</a>'+
			'</div');
    }

  });

  $('.popupEncuentranos .content .places .item .btns a').click(function(e){
    e.preventDefault();

    $('.pageContacto .content .modPuntos .map iframe').attr('src','https://www.google.com/maps/embed/v1/place?key=AIzaSyBG7NQYnacAuqE0ZxELdXAdVBSq1LJ2Vvk&q='+$(this).attr('href'));
    $('.popupEncuentranos').fadeOut();
  });
}

function cargarLugares(){
  lugaresVenta.map((lugar, i, a) => {
    var classActive = "";
    if(i == 0) classActive = ' class="active"';
    $('.popupEncuentranos .content .group').append('<a href="'+lugar.id+'"'+classActive+'>'+lugar.nombre+'</a>');

    $('.popupEncuentranos .content .movFilter').append('<option value="'+lugar.id+'">'+lugar.nombre+'</option>');

  });

  $('.popupEncuentranos .content .group a').click(function(e){
    e.preventDefault();
    var idLugar = $(this).attr('href');

    gtag('event', 'click-button', { 'event_category': 'Click en lugares de atención', 'event_label': $(this).text() });

    $('.popupEncuentranos .content .group a').removeClass('active');

    $(this).addClass('active');

    $('.popupEncuentranos .content .places').fadeOut(500,function(){

      $('.popupEncuentranos .content .places').html('');
      cargarPuntosAtencion(idLugar);
      $('.popupEncuentranos .content .places').fadeIn();
    });

  });

  $('.popupEncuentranos .content .movFilter').change(function(){
    var idLugar = $(this).val();

    $('.popupEncuentranos .content .places').fadeOut(500,function(){

      $('.popupEncuentranos .content .places').html('');
      cargarPuntosAtencion(idLugar);
      $('.popupEncuentranos .content .places').fadeIn();
    });
  });
}

function cargarImagenes(){
  $('.pageLinea .content .catalog .listProducts .group .item img').each(function(){
    var sku = $(this).attr('data');
    $(this).fadeOut(0);
    $(this).attr('src',PATH_BASE+'assets/images/catalogo/detalle/'+sku+'.png');

    $(this).one('load', function() {
      $(this).parent().css('min-height','auto');
      $(this).fadeIn();
    });
  });
}

function eventos(){

  $('.menu a, .intro a, .footer ul li a').click(function(e){
    e.preventDefault();

    gtag('event', 'click-menu', { 'event_category': 'Navegación por menu', 'event_label': this.hash });

    $("html,body").animate({scrollTop: $(this.hash).offset().top}, 500);

  });

  $('.filter li a').click(function(e){
    e.preventDefault();

    var categoria = $(this).attr('href');
    Services.getProductos(categoria);

  });

  $('.pageLinea .content .catalog .movFilter').change(function(e){

    var categoria = $(this).val();
    Services.getProductos(categoria);
  });

  $('.listProducts .slider .item a').click(function(e){
    e.preventDefault();
    var producto = $(this).attr('href');
    Services.getDetalle(producto);
  });


  $('a.terminos').click(function(e){
    e.preventDefault();
    gtag('event', 'click-button', { 'event_category': 'Click en botón', 'event_label': 'Ver términos y condiciones' });
    $('.popupTerminos').fadeIn();
  });

  $('.popupTerminos .close').click(function(){
    $('.popupTerminos').fadeOut();
  });

  $('.popupInfo .close').click(function(){
    $('.popupInfo').fadeOut();
  });

  $('.pageContacto .content .modPuntos .btn').click(function(e){
    e.preventDefault();
    gtag('event', 'click-button', { 'event_category': 'Click en botón', 'event_label': 'Ver mapas' });
    $('.popupEncuentranos').fadeIn();
  });

  $('.popupEncuentranos .close').click(function(){
    $('.popupEncuentranos').fadeOut();
  });

  $('.pageRegalo .content .formBuscar form .col .btn').click(function(e){
    e.preventDefault();
    gtag('event', 'click-button', { 'event_category': 'Click en botón', 'event_label': 'Buscar el regalo' });
    Services.getRegalo();
  });

  $('.popupRegalo .close').click(function(){
    $('.popupRegalo').fadeOut();
  });

  $('.pageContacto .content .modContacto form .btn').click(function(e){
    e.preventDefault();

    gtag('event', 'click-button', { 'event_category': 'Click en botón', 'event_label': 'Enviar mensaje de contacto' });

    if(re_captcha){
        if (validate.contacUs()) {
          Services.sendContactUs();
        }
    }else{
        $('.inError p').html("* Debe hacer clic en el reCAPTCHA.");
    }
  });
}

var Services = {
  init: function () {
      this.getTweets();
  },
  getProductos: function(categoria) {

    var self = this;
    var url = "catalogo/obtener_categoria/"+categoria;
    var token = self.getToken('token');

    $('.listProducts .slider').fadeOut();

    Get(url,token,function(res,status,xhr) {
        token = xhr.getResponseHeader("token");
        self.setToken(token);

      var itemsProducto = '<div class="group">';
      var contador = 0;
      res.productos.map((producto, i, a) => {

        itemsProducto += '<div class="item">' +
                              '<a href="'+producto.id+'" class="btnImg"><img src="'+PATH_BASE+'assets/images/catalogo/default.jpg" alt="'+producto.nombre+'" data="'+producto.sku+'"></a>' +
                              '<h3>'+producto.nombre+'</h3>' +
                              '<p>SKU: '+producto.sku+'</p>' +
                              '<a class="btn" href="'+producto.id+'">Ver más</a>' +
                          '</div>';
        contador++;

        if(contador == productosPorPagina){
          itemsProducto += '</div><div class="group">';
          contador = 0;
        }

      });

      itemsProducto += '</div>';

      $('.listProducts .slider').removeClass('slick-initialized slick-slider').html(itemsProducto).fadeIn();

      sliderProductos();
      cargarImagenes();
      $('.listProducts .slider .item a').click(function(e){
        e.preventDefault();
        var producto = $(this).attr('href');
        Services.getDetalle(producto);
      });

    });
  },

  getDetalle: function(producto) {

    var self = this;
    var url = "catalogo/detalle_producto/"+producto;
    var token = self.getToken('token');
    Get(url,token,function(res,status,xhr) {
      token = xhr.getResponseHeader("token");
      self.setToken(token);

      var nom = res.producto.nombre.toString();
      nom = nom.replace('<br>',' ');

      gtag('event', 'vista-producto', { 'event_category': 'Línea Escritura Fina', 'event_label': nom });

      $('.popupInfo .detail h3').html(nom);
      $('.popupInfo .detail span').html('SKU:'+res.producto.sku);
      $('.popupInfo .detail .desc_pro').html(res.producto.descripcion);
      $('.popupInfo .content .gallery .photo img').attr('src',PATH_BASE+'assets/images/catalogo/large/'+res.producto.sku+'.png');
      $('.popupInfo .content .detail .group').html('');
      if(res.producto.url_saga != ''){
        $('.popupInfo .content .detail .group').append('<a href="'+res.producto.url_saga+'" class="urlSaga" target="_blank">Obtenlo en Saga Falabella</a>');
      }
      $('.popupInfo .content .detail .group').append('<a href="#" class="goPuntoVenta">Venta presencial en nuestras tiendas</a>');
      $('.popupInfo').fadeIn();

      $('.goPuntoVenta').click(function(e){
        e.preventDefault();
        $('.popupInfo').fadeOut();
        $("html,body").animate({scrollTop: $('.modPuntos').offset().top}, 500);
      });

    });
  },

  getPuntosAtencion: function() {

    var self = this;
    var url = "punto/obtener_puntos_venta";
    var token = self.getToken('token');
    Get(url,token,function(res,status,xhr) {
      token = xhr.getResponseHeader("token");
      self.setToken(token);

      //console.log(res);
      if(res.lugares){
        lugaresVenta = res.lugares;
        puntosVenta = res.puntos;
        cargarLugares();
        if(puntosVenta.length){
          cargarPuntosAtencion(lugaresVenta[0].id);
        }

      }

    });
  },

  getRegalo: function() {

    if(validarBusqueda()){
      var self = this;
      var url = "catalogo/obtener_regalo/";
      var token = self.getToken('token');

      var params = {
          sexo: $('#cboSexo').val(),
          edad: $('#cboEdad').val(),
          tipo: $('#cboTipo').val(),
          personalidad: $('#cboPersonalidad').val(),
          tono: idTono
          /*tono: $('#cboTono').val()*/
      };
      Post(params,url,token,function (res) {

        if(res.producto){
          console.log(res);

          var nom = res.producto.nombre.toString();
          nom = nom.replace('<br>',' ');

          gtag('event', 'regalo-encontrado', { 'event_category': 'Regalo Encontrado', 'event_label': nom });


          $('.popupRegalo .content img').hide();
          $('.popupRegalo .content h4').html(nom);
          $('.popupRegalo .content span').html('SKU:'+res.producto.sku);
          $('.popupRegalo .content p').html(res.producto.descripcion);
          $('.popupRegalo .content img').attr('src',PATH_BASE + 'assets/images/catalogo/detalle/'+res.producto.sku+'.png').load(function(){
            $(this).show();
          });

          $('.popupRegalo .content .group').html('');
          if(res.producto.url_saga != ''){
            $('.popupRegalo .content .group').append('<a href="'+res.producto.url_saga+'" class="urlSaga" target="_blank">Obtenlo en Saga Falabella</a>');
          }
          $('.popupRegalo .content .group').append('<a href="#" class="goPuntoVenta">Obtenlo aquí</a>');

          $('.goPuntoVenta').click(function(e){
            e.preventDefault();
            $('.popupRegalo').fadeOut();
            $("html,body").animate({scrollTop: $('.modPuntos').offset().top}, 500);
          });

          $('.popupRegalo').fadeIn();
        }else{
          show_message("UPSS","Te pedimos disculpa. No hemos podido encontrar un regalo para tu combinación","info");
          gtag('event', 'regalo-no-encontrado', { 'event_category': 'Regalo No Encontrado', 'event_label': 'No se encontró un producto para la combinación' });
        }


      });
    }
  },

  sendContactUs: function() {

    if(validarBusqueda()){
      var self = this;
      var url = "contacto/registro_contacto";
      var token = self.getToken('token');

      var params = {
          names: $('#userNames').val(),
          email: $('#userEmail').val(),
          message: $('#userMessage').val(),
          terminos: ($('#userData').is(':checked')? 1:0),
          info: ($('#userData2').is(':checked')? 1:0)
      };
      Post(params,url,token,function (res) {

        if(res.resultado){
          $('#userNames').val("");
          $('#userEmail').val("");
          $('#userMessage').val("");
          $('#userData').attr('checked', false);
          $('#userData2').attr('checked', false);

          show_message("Muy bien!",res.mensaje,"success");

        }else{
          show_message("UPSS","Ha ocurrido un error a tratar de enviar la información.","error");
        }


      });
    }
  },

  getToken: function () {
    var token = "";
    if(localStorage.getItem('token')){
        token = localStorage.getItem('token')
    }
    return token;
  },
  setToken:function (token) {
    if(localStorage.getItem('token')){
        localStorage.removeItem('token');
    }
    localStorage.setItem('token',token);
  }
};

var validate = {
    contacUs: function () {
        var Valid = {
            names: true,
            email: true,
            message: true,
            data: true/*,
            data2: true*/
        };
        var names = $('#userNames'),
            email = $('#userEmail'),
            message = $('#userMessage'),
            data = $('#userData')/*,
            data2 = $('#userData2')*/;

        if (!data.is(':checked')) {
            data.addClass('validInput');
            $('.inError p').html('Debe de seleccionar la opción: Acepto términos y condiciones.');
            Valid.data = false;
        } else {
            $('.inError p').html('');
        }

        if (message.val() == undefined || message.val() == '') {
            $('.inError p').html('Debe de ingresar el mensaje que desea comunicar.');
            message.addClass('validInput');
            Valid.message = false;
        } else {
            message.removeClass('validInput');
        }

        if (email.val() == undefined || email.val() == '') {
            $('.inError p').html('Debe de ingresar el email.');
            email.addClass('validInput');
            Valid.email = false;
        } else {

            var expression = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;

            if (expression.test(email.val())) {
              email.removeClass('validInput');
            }
            else {
              $('.inError p').html('Debe de ingresar un formato correcto para el email.');
              email.addClass('validInput');
              Valid.email = false;
            }

        }

        if ((names.val() == undefined || names.val() == '')) {
            $('.inError p').html('Debe de ingresar su nombre completo');
            names.addClass('validInput');
            Valid.names = false;
        } else {
            names.removeClass('validInput');
        }

        return (Valid.names && Valid.email && Valid.message && Valid.data);
    }
};

function validarBusqueda(){
  return true;
}

function sliderProductos(){
  $('.slider').slick(
    {
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      infinite: false,
      prevArrow: $('.aPrev'),
      nextArrow: $('.aNext')
    }
  );
}

function anim(){
	var win =$(window).scrollTop()+winHeight,
	 	sHome = posi('#home'),
	 	sLinea = posi('#linea-escritura-fina'),
	 	sRegalo = posi('#regalo-perfecto'),
	 	sContacto = posi('#contacto');

	 	if(sHome!==false && win >= sHome){
	 		entrar($('#home'), 'entrar', 60);
        }

	 	if(sLinea && win >= sLinea){
          entrar($('#linea-escritura-fina'), 'entrar', 250);
        }

	 	if(sRegalo && win >= sRegalo)
	 		entrar($('#regalo-perfecto'), 'entrar', 250);

	 	if(sContacto && win >= sContacto)
	 		entrar($('#contacto'), 'entrar', 250);

}

function entrar(key, anim, time){

	var keys = key.find('[anim]'), todos = keys.length-1, i = 0, loop = '';

	loop = setInterval(function(){
		if(i<=todos){ keys.eq(i).addClass(anim); i++; }else{ clearInterval(loop); }
	},time);
}

function posi(key) { return $(key).length ? $(key).offset().top : false; }


function Get(url,token,callback) {
    if (typeof callback == "function") {
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": Path + url,
            "method": "GET",
            "headers": {
                "content-type": "application/x-www-form-urlencoded",
                "token": token
            }
        };

        $.ajax(settings).success(function (response,status,xhr) {
            callback(response,status,xhr);
        }).fail(function (err) {
            callback(err);
        });
    }
}

function Post(params, url, token, callback) {
    if (typeof callback == "function") {
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": Path + url,
            "method": "POST",
            "headers": {
                "content-type": "application/x-www-form-urlencoded",
                "token": token
            },
            "data": params
        };

        $.ajax(settings).success(function (response,status,xhr) {
            callback(response,status,xhr);
        }).fail(function (err) {
            callback(err);
        });
    }
}

function show_message(title,text,icon) {
  swal({
    title: title,
    text: text,
    icon: icon,
    button: "Aceptar",
  });

}
