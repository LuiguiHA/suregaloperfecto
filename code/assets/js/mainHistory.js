var winHeight=($(window).outerHeight()/3)*2, primeraCarga=true;
$(document).on('ready', function(){
    // CONNECT FACEBOOK
    // show_message("Muy bien!","<p>sdasdasdas</p>","success");
  window.fbAsyncInit = function() {
    FB.init({
      appId            : '509907432854401',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v3.2'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

   $('#dni').alphanum({
    allowNumeric: true,
    allowUpper: false,
    allowLower: false,
    allowSpace: false
  });

  $('#phone').alphanum({
    allowNumeric: true,
    allowUpper: false,
    allowLower: false,
    allowSpace: false
  });

  $('#email').alphanum({
    allowSpace: false,
    allow: '@.-_',
  });

  $('#name').alphanum({
    allowNumeric: false,
  });

  $('#lastName').alphanum({
    allowNumeric: false,
  });
});
$(window).on('load',function(){

  eventos();
  anim();
  $("#terms .sb-container").scrollBox();
  $("#protection .sb-container").scrollBox();

});
var services = {
  sendHistory: function(){
    var self = this;
    var url = "history/registro_historia";
    var token = ''
    var params = {
        name: $('#name').val(),
        last_name: $('#lastName').val(),
        email: $('#email').val(),
        history: $('#historyText').val(),
        dni: $('#dni').val(),
        phone: $('#phone').val(),
        info: $("#formHistory input[name='info']:checked").val()
    };
    Post(params,url,token,function (res) {

      if(res.resultado){
        $('#name').val("");
        $('#lastName').val("");
        $('#email').val("");
        $('#historyText').val("");
        $('#dni').val("");
        $('#phone').val("");
        $("#formHistory input[name='info']:checked").removeAttr('checked');

        show_message("Muy bien!",res.mensaje,"success");

      }else{
        show_message("UPSS","Ha ocurrido un error a tratar de enviar la información.","error");
      }


    });
  }
}
var validate = {
  history: function () {
    var Valid = {
      name: true,
      last_name: true,
      email: true,
      dni: true,
      phone: true,
      historyText: true,
      info: true
    };
    var name = $('#name'),
        last_name = $('#lastName'),
        email = $('#email')
        historyText = $('#historyText'),
        dni = $('#dni'),
        phone = $('#phone'),
        info = $("#formHistory input[name='info']:checked")
    
    
    if (info.val() == undefined) {
      info.addClass('validInput');
      $('.inErrorHistory p').html('Todos los campos son obligatorios.');
      Valid.info = false;
    } else {
        if(info.val() == 'SI'){
          $('.inErrorHistory p').html('');
        }
    }

    if (historyText.val() == undefined || historyText.val() == '') {
      $('.inErrorHistory p').html('Todos los campos son obligatorios.');
      historyText.addClass('validInput');
      Valid.historyText = false;
    } else {
      historyText.removeClass('validInput');
    }

    if (phone.val() == undefined || phone.val() == '') {
      $('.inErrorHistory p').html('Todos los campos son obligatorios.');
      phone.addClass('validInput');
      Valid.phone = false;
    } else {
      if(phone.val().length < 7){
        $('.inErrorHistory p').html('El campo Celular/Fijo debe contener mínimo 7 caracteres.');
        phone.addClass('validInput');
        Valid.phone = false;
      } else{
        phone.removeClass('validInput');
      }
    }

    if (dni.val() == undefined || dni.val() == '') {
      $('.inErrorHistory p').html('Todos los campos son obligatorios.');
      dni.addClass('validInput');
      Valid.dni = false;
    } else {
      if(dni.val().length != 8){
        $('.inErrorHistory p').html('El campo DNI debe contener 8 caracteres.');
        dni.addClass('validInput');
        Valid.dni = false;
      } else{
        dni.removeClass('validInput');
      }
    }
    
    if (email.val() == undefined || email.val() == '') {
      $('.inErrorHistory p').html('Todos los campos son obligatorios.');
      email.addClass('validInput');
      Valid.email = false;
    } else {
      var expression = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;

      if (expression.test(email.val())) {
        email.removeClass('validInput');
      }
      else {
        $('.inErrorHistory p').html('Debe de ingresar un formato correcto para el email.');
        email.addClass('validInput');
        Valid.email = false;
      }
    }

    if (last_name.val() == undefined || last_name.val() == '') {
      $('.inErrorHistory p').html('Todos los campos son obligatorios.');
      last_name.addClass('validInput');
      Valid.last_name = false;
    } else {
      last_name.removeClass('validInput');
    }

    if (name.val() == undefined || name.val() == '') {
      $('.inErrorHistory p').html('Todos los campos son obligatorios.');
      name.addClass('validInput');
      Valid.name = false;
    } else {
      name.removeClass('validInput');
    }

    return (Valid.name && Valid.email && Valid.last_name && Valid.info && Valid.historyText && Valid.dni && Valid.phone);
  }
}
function eventos(){
  $('a.terminos').click(function(e){
    e.preventDefault();
    // gtag('event', 'click-button', { 'event_category': 'Click en botón', 'event_label': 'Ver términos y condiciones' });
    $('#terms').fadeIn();
  });

  $('#terms .close').click(function(){
    $('#terms').fadeOut();
  });

  $('a.protection').click(function(e){
    e.preventDefault();
    // gtag('event', 'click-button', { 'event_category': 'Click en botón', 'event_label': 'Ver términos y condiciones' });
    $('#protection').fadeIn();
  });

  $('#protection .close').click(function(){
    $('#protection').fadeOut();
  });

  $('.pageHistory .content .modHistory form .btn').click(function(e){
    e.preventDefault();

    gtag('event', 'click-button', { 'event_category': 'Click en botón', 'event_label': 'Enviar mensaje de historia' });

    if (validate.history()) {
      services.sendHistory();
    }
  });

  $('.pageHistory .content .modHistory form .btn-fb').click(function(e){
    e.preventDefault();

    gtag('event', 'click-button', { 'event_category': 'Click en botón', 'event_label': 'Conectarse a facebook' });

    FB.login(response => {
      if (response.authResponse) {
        var userId = response.authResponse.userID
        var url = "/" + userId + "/"
        FB.api(url,
              {fields: 'email,first_name,last_name,name'},
              function (response) {
                if (response && !response.error) {
                  $('#name').val(response.first_name)
                  $('#lastName').val(response.last_name)
                  $('#email').val(response.email)
                  $('#name').removeClass('validInput');
                  $('#lastName').removeClass('validInput');
                  $('#email').removeClass('validInput');
                }
              }
            );
      } 
      else {
       console.log('User cancelled login or did not fully authorize.');
      }
    }, {scope: 'email'});
  });
}
function anim(){
	var win =$(window).scrollTop()+winHeight,
	 	sHome = posi('#home'),
	 	sLinea = posi('#linea-escritura-fina'),
	 	sRegalo = posi('#regalo-perfecto'),
    sContacto = posi('#contacto');
    sHistory= posi('#history');

	 	if(sHome!==false && win >= sHome){
	 		entrar($('#home'), 'entrar', 60);
        }
    
    if(sHistory!==false && win >= sHistory){
      entrar($('#history'), 'entrar', 120);
    }

	 	if(sLinea && win >= sLinea){
          entrar($('#linea-escritura-fina'), 'entrar', 250);
        }

	 	if(sRegalo && win >= sRegalo)
	 		entrar($('#regalo-perfecto'), 'entrar', 250);

	 	if(sContacto && win >= sContacto)
	 		entrar($('#contacto'), 'entrar', 250);

}

function entrar(key, anim, time){

	var keys = key.find('[anim]'), todos = keys.length-1, i = 0, loop = '';

	loop = setInterval(function(){
		if(i<=todos){ keys.eq(i).addClass(anim); i++; }else{ clearInterval(loop); }
	},time);
}

function posi(key) { return $(key).length ? $(key).offset().top : false; }

function show_message(title,text,icon) {
  swal({
    title: title,
    text: text,
    icon: icon,
    button: "Aceptar",
  });

}

function Get(url,token,callback) {
  if (typeof callback == "function") {
      var settings = {
          "async": true,
          "crossDomain": true,
          "url": Path + url,
          "method": "GET",
          "headers": {
              "content-type": "application/x-www-form-urlencoded",
              "token": token
          }
      };

      $.ajax(settings).success(function (response,status,xhr) {
          callback(response,status,xhr);
      }).fail(function (err) {
          callback(err);
      });
  }
}

function Post(params, url, token, callback) {
  if (typeof callback == "function") {
      var settings = {
          "async": true,
          "crossDomain": true,
          "url": Path + url,
          "method": "POST",
          "headers": {
              "content-type": "application/x-www-form-urlencoded",
              "token": token
          },
          "data": params
      };

      $.ajax(settings).success(function (response,status,xhr) {
          callback(response,status,xhr);
      }).fail(function (err) {
          callback(err);
      });
  }
}